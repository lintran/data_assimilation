
from numpy import dot, array

def adv_model(x, t, parms, deriv, grad = None):
    """
    Linear state transition
        x_t = mean + Fx_t-1
    """
    out = [x] * 2
    out[1] = parms["mean"] + dot(parms["F"], x)
    return array(out)

def deriv(y, t, parms):
    return parms["F"]














