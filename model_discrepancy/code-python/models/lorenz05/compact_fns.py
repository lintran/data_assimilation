
from __future__ import division
from numpy import finfo, floor, cos, pi, tan, ceil, logical_or, logical_not, exp

def tricube(dist, cutoff):
    r = abs(dist / cutoff)
    return 70 / 81 * (1-r**3)**3 * float(r <= 1)

def wendland(dist, cutoff, tau, q):
    """
    dist: distance matrix/vector
    cutoff: range parameter
    tau: shape parameter
    q: cov fn is 2*q-times continuously differentiable, thus GP is q-times mean-square differentiable
    """

    # if type(dim) is not int: raise ValueError("'dim' must be an integer.")
    # if type(q) is not int: raise KeyError("'q' must be an integer.")

    # if (dist < 0).any(): raise ValueError("'dist' must be nonnegative.")
    # if dim < 0: raise KeyError("'dim' must be nonnegative.")
    # if q < 0: raise ValueError("'q' must be nonnegative.")

    if cutoff <= 0: raise ValueError("'cutoff' must be greater than 0.")
    if tau < 2.*(q+1): raise ValueError("'tau' must be greater than or equal to 2*('q'+1).")

    dist = dist / cutoff
    a = fplus(1-dist)**tau

    if   q == 0: num = 1.
    elif q == 1: num = 1. + tau*dist
    elif q == 2: num = 3. + 3.*tau*dist + (tau**2 - 1) * dist**2
    else:        raise KeyError("'q' can only take integer values between 0 and 2, inclusive.")

    den = 1.
    if q == 2: den = 3.

    return a * num / den

def wendlandcos(dist, cutoff, tau, q, period_frac, n, dim = 1):
    if period_frac < 0 or period_frac > 1: raise ValueError("'period_frac' must be between 0 and 1, inclusive.")
    if n < 0: raise ValueError("'n' must be an integer greater than or equal to 0.")

    w = wendland(dist, cutoff, tau, q)

    dist = 2*pi* n*period_frac * dist/cutoff
    tau = 1/tan(pi/(2*dim)) if dim > 1 else 0
    c = exp(-tau*dist) * cos(dist)

    return w*c

def fplus(a):
    return a*(a>0)

def gasparicohn(dist, halfwidth):
    out = dist / halfwidth

    idx1 = dist >= halfwidth*2
    idx2 = dist <= halfwidth
    idx3 = logical_not(logical_or(idx1, idx2))

    out[idx1] = 0
    out[idx2] = ( ( ( -0.25*out[idx2] +0.5 )*out[idx2] +0.625 )*out[idx2] -5.0/3.0 )*out[idx2]**2 + 1.0
    out[idx3] = ( ( ( ( out[idx3]/12.0 -0.5 )*out[idx3] +0.625 )*out[idx3] +5.0/3.0 )*out[idx3] -5.0 )*out[idx3] + 4.0 - 2.0 / (3.0 * out[idx3])

    return out
