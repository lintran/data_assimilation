
! adapted from DART: kodiak/models/lorenz_04/model_mod.f90

subroutine comp_dt(z, dt, model_number, model_size, K, K2, K4, H, forcing, &
                   smooth_steps, ss2, space_time_scale, sts2, coupling, &
                   alpha_minus_betai)
!------------------------------------------------------------------
! subroutine comp_dt(z, dt)
!
! Computes the time tendency of the lorenz 2004 model given current state.
!
! The model equations are given by
!
! Model 2 (II)
!      dX_i
!      ---- = [X,X]_{K,i} -  X_i + F
!       dt
!
!
! Model 3 (III)
!      dZ_i
!      ---- = [X,X]_{K,i} + b^2 (-Y_{i-2}Y_{i-1} + Y_{i-1}Y_{i+1})
!       dt                +  c  (-Y_{i-2}X_{i-1} + Y_{i-1}X_{i+1})
!                         -  X_i - b Y_i + F,
!
! where
!
!     [X,X]_{K,i} = -W_{i-2K}W_{i-K}
!                 +  sumprime_{j=-(K/2)}^{K/2} W_{i-K+j}X_{i+K+j}/K,
!
!      W_i =  sumprime_{j=-(K/2)}^{K/2} X_{i-j}/K,
!
! and sumprime denotes a special kind of summation where the first
! and last terms are divided by 2.
!
! NOTE: The equations above are only valid for K even.  If K is odd,
! then sumprime is replaced by the traditional sum, and the K/2 limits
! of summation are replaced by (K-1)/2. THIS CODE ONLY IMPLEMENTS THE
! K EVEN SOLUTION!!! -- now code also implements K odd solution
!
! The variable that is integrated is X (model II) or Z (model III),
! but the integration of Z requires
! the variables X and Y.  For model III they are obtained by
!
!      X_i = sumprime_{j= -J}^{J} a_j Z_{i+j}
!      Y_i = Z_i - X_i.
!
! The "a" coefficients are given by
!
!      a_j = alpha - beta |j|,
!
! where
!
!      alpha = (3J^2 + 3)/(2J^3 + 4J)
!      beta  = (2J^2 + 1)/(1J^4 + 2J^2).
!
! This choice of alpha and beta ensures that X_i will equal Z_i
! when Z_i varies quadratically over the interval 2J.   This choice
! of alpha and beta means that sumprime a_j = 1 and
! sumprime (j^2) a_j = 0.
!
! Note that the impact of this filtering is to put large-scale
! variations into the X variable, and small-scale variations into
! the Y variable.
!
! The parameter names above are based on those that appear in
! Lorenz 04.  To map to the code below, set:
!
!       F = forcing
!       b = space_time_scale
!       c = coupling
!       K = K
!       J = smooth_steps

implicit none

integer,          parameter          :: pr = 8
double precision, intent( in)        ::  z(model_size), sts2, coupling, space_time_scale, forcing, alpha_minus_betai(ss2+1)
double precision, intent( out)       :: dt(model_size)
integer,          intent( in)        :: model_number, model_size, K, K2, K4, H, smooth_steps, ss2
double precision, dimension(size(z)) :: x, y
double precision                     :: xwrap(- K4:model_size + K4)
double precision                     :: ywrap(- K4:model_size + K4)
double precision                     ::    wx(- K4:model_size + K4)
double precision                     :: xx, den
integer                              :: i, j

! could branch this differently for more effecient model II

if ( model_number == 3 ) then
   ! Decompose z into x and y
   call z2xy(z,x,y, model_size, smooth_steps, ss2, alpha_minus_betai)
elseif ( model_number == 2 ) then
   x = z
   y = 0.0_pr   ! just a dummy
endif

! Deal with cyclic boundary conditions using buffers
do i = 1, model_size
   xwrap(i) = x(i)
   ywrap(i) = y(i)
end do

! Fill the xwrap and ywrap buffers
do i = 1, K4
   xwrap(- K4 + i)       = xwrap(model_size - K4 +i)
   xwrap(model_size + i) = xwrap(i)
   ywrap(- K4 + i)       = ywrap(model_size - K4 +i)
   ywrap(model_size + i) = ywrap(i)
end do

if ( modulo(model_size, 2) == 0 ) then ! if N even
   den = 2.0_pr
else ! if N odd
   den = 1.0_pr
endif

! Calculate the W's
do i = 1, model_size
   !wx(i) = xwrap(i - (-H))/2.0_pr
   wx(i) = xwrap(i - (-H))/den
   do j = - H + 1, H - 1
      wx(i) = wx(i) + xwrap(i - j)
   end do
   !wx(i) = wx(i) + xwrap(i - H)/2.0_pr
   wx(i) = wx(i) + xwrap(i - H)/den
   wx(i) = wx(i)/K
end do

! Fill the W buffers
do i = 1, K4
   wx(- K4 + i)       = wx(model_size - K4 + i)
   wx(model_size + i) = wx(i)
end do

! Generate dz/dt
do i = 1, model_size
   !xx = wx(i - K + (-H))*xwrap(i + K + (-H))/2.0_pr
   xx = wx(i - K + (-H))*xwrap(i + K + (-H))/den
   do j = - H + 1, H - 1
      xx = xx + wx(i - K + j)*xwrap(i + K + j)
   end do
   !xx = xx + wx(i - K + H)*xwrap(i + K + H)/2.0_pr
   xx = xx + wx(i - K + H)*xwrap(i + K + H)/den
   xx = - wx(i - K2)*wx(i - K) + xx/K

   if ( model_number == 3 ) then
     dt(i) = xx + (sts2)*( - ywrap(i - 2)*ywrap(i - 1) &
         + ywrap(i - 1)*ywrap(i + 1)) + coupling*( - ywrap(i - 2)*xwrap(i - 1) &
         + ywrap(i - 1)*xwrap(i + 1)) - xwrap(i) - space_time_scale*ywrap(i) &
         + forcing
   else ! must be model II
     dt(i) = xx - xwrap(i) + forcing
   endif

end do

end subroutine comp_dt

subroutine z2xy(z,x,y, model_size, smooth_steps, ss2, alpha_minus_betai)
!------------------------------------------------------------------
! subroutine z2xy(z,x,y)
!
! Decomposes z into x and y for L2k4

implicit none

integer,          parameter   :: pr = 8
integer                       :: i, j, ia, model_size, smooth_steps, ss2, I2
double precision, intent( in) :: z(model_size), alpha_minus_betai(ss2+1)
double precision, intent(out) :: x(model_size)
double precision, intent(out) :: y(model_size)
double precision              :: zwrap(- ss2:model_size + ss2)

! Fill zwrap
do i = 1, model_size
   zwrap(i) = z(i)
end do
zwrap( - ss2) = zwrap(model_size - ss2)
do i = 1, ss2
   zwrap( - ss2 + i) = zwrap(model_size - ss2 + i)
   zwrap(model_size + i) = zwrap(i)
end do

! Generate the x variables
do i = 1, model_size
   ia = 1
   x(i) = alpha_minus_betai(ia)*zwrap(i - ( - smooth_steps))/2.0_pr
   do j = - smooth_steps + 1, smooth_steps - 1
      ia = ia + 1
      x(i) = x(i) + alpha_minus_betai(ia)*zwrap(i - j)
   end do
   ia = ia + 1
   x(i) = x(i) + alpha_minus_betai(ia)*zwrap(i - smooth_steps)/2.0_pr
end do

! Generate the y variables
do i = 1, model_size
   y(i) = z(i) - x(i)
end do

end subroutine z2xy


subroutine linearize_dt(x, M, model_size, K, K2, K4, Ksq, H)

implicit none

integer,          parameter          ::  pr = 8
double precision, intent( in)        ::  x(model_size), Ksq
double precision, intent(out)        :: M(model_size,model_size)
integer,          intent( in)        :: model_size, K, K2, K4, H
double precision                     :: xx, sumprime
integer, dimension(K+1)              :: nX, n
integer                              :: JJ(K+1), Kp1a(K-1), Kp1b(2)
integer                              :: nM, i, j, a

JJ   = [(i, i = -H, H)]
Kp1a = [(i, i =  2, K)]
Kp1b = [1, K+1]

do nM = 1, model_size ! start loop over rows in Jacobian
   ! addend 1: sum'_j [ I(m = n-K-j) sum'_i X_{n-2K-i} ]
   nX = nM-K -JJ
   n  = nM-K2-JJ
   call wrap(nX, model_size, K+1)
   call wrap(n, model_size, K+1)
   xx = sumprime(x,n,K,model_size,K+1)
   call addto_elements_in_M(M, -xx,          nM, nX(Kp1a), model_size, 1, K-1)
   call addto_elements_in_M(M, -xx / 2.0_pr, nM, nX(Kp1b), model_size, 1, 2)

   ! addend 2: sum'_j [ I(m = n-2K-j) sum'_i X_{n-K-i} ]
   nX = nM-K2-JJ
   n  = nM-K -JJ
   call wrap(nX, model_size, K+1)
   call wrap(n, model_size, K+1)
   xx = sumprime(x,n,K,model_size,K+1)
   call addto_elements_in_M(M, -xx,          nM, nX(Kp1a), model_size, 1, K-1)
   call addto_elements_in_M(M, -xx / 2.0_pr, nM, nX(Kp1b), model_size, 1, 2)

   ! addend 3: sum'_j [ I(m = n+K+j) sum'_i X_{m-2K-i} ]
   nX = nM+K+JJ
   call wrap(nX, model_size, K+1)
   do j = 1,K+1
      do i = -H,H
         a  = nM-K+JJ(j)-i
         call wrap(a, model_size, 1)
         xx = x(a)

         if ( (i == -H) .or. (i == H)   ) xx = xx / 2.0_pr
         if ( (j == 1)  .or. (j == K+1) ) xx = xx / 2.0_pr
         M(nM,nX(j)) = M(nM,nX(j)) + xx
      enddo
   enddo

   ! addend 4: sum'_j sum'_i [ I(m = n-K+j-i) X_{m+2K+i} ] = sum'_i X_{m+2K+i} sum'_j I(m = n-K+j-i)
   do i = -H,H

      nX = nM-K+JJ-i
      call wrap(nX, model_size, K+1)

      do j = 1,K+1
         a  = nM-K+JJ(j)
         call wrap(a, model_size, 1)
         xx = x(a)

         if ( (i == -H) .or. (i == H)   ) xx = xx / 2.0_pr
         if ( (j == 1)  .or. (j == K+1) ) xx = xx / 2.0_pr
         M(nM,nX(j)) = M(nM,nX(j)) + xx
      end do
   end do

end do ! end loop over rows in Jacobian

! finalize
M(:,:) = M(:,:) / Ksq
do i = 1,model_size
   M(i,i) = M(i,i) - 1.0_pr
enddo

end subroutine linearize_dt

subroutine wrap(i, n, i_len)
   implicit none ! tells compiler that you won't allow implicit types
   integer, intent(in)    :: i_len, n
   integer, intent(inout) :: i(i_len)
   integer                :: j

   do j = 1,i_len
      i(j) = modulo(i(j)-1,n) + 1
   enddo
end subroutine wrap

double precision function sumprime(x,n,K,x_len,n_len)
   implicit none ! tells compiler that you won't allow implicit types

   integer,          parameter   :: pr = 8
   integer,          intent(in)  :: x_len, n_len, K
   double precision, intent(in)  :: x(x_len)
   integer,          intent(in)  :: n(n_len)
   integer                       :: i
   double precision              :: output

   output = 0.0_pr

   do i = 2, n_len-1
      output = output + x(n(i))
   enddo

   if ( modulo(K,2) == 0 ) then
      output = output + ( x(n(1)) + x(n(n_len)) ) / 2.0_pr
   else
      output = output + ( x(n(1)) + x(n(n_len)) )
   endif

   sumprime = output
return
end function sumprime

subroutine addto_elements_in_M(M, add_val, n1, n2, model_size, n1_len, n2_len)
   implicit none
   integer,          intent(in)    :: model_size, n1_len, n2_len, n1(n1_len), n2(n2_len)
   double precision, intent(in)    :: add_val
   double precision, intent(inout) :: M(model_size, model_size)
   integer                         :: i,j

   do i = 1, n1_len
      do j = 1, n2_len
         M(n1(i),n2(j)) = M(n1(i),n2(j)) + add_val
      enddo
   enddo
end subroutine

subroutine comp_dt_linear(z, dt, model_number, model_size, K, K2, K4, H, forcing, &
                          smooth_steps, ss2, space_time_scale, sts2, coupling, &
                          alpha_minus_betai, L, b)

   implicit none

   double precision, intent( in)        ::  z(model_size), sts2, coupling, space_time_scale, forcing, alpha_minus_betai(ss2+1)
   double precision, intent( in)        :: L(model_size,model_size), b(model_size)
   double precision, intent( out)       :: dt(model_size)
   integer,          intent( in)        :: model_number, model_size, K, K2, K4, H, smooth_steps, ss2

   call comp_dt(z, dt, model_number, model_size, K, K2, K4, H, &
      forcing, smooth_steps, ss2, space_time_scale, sts2, &
      coupling, alpha_minus_betai)
   dt = dt + matmul(L, z) + b

end subroutine comp_dt_linear
