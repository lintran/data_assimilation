
from __future__ import division
from numpy import array, fill_diagonal, zeros, in1d, diff, isnan, empty, dot, ones, isnan
from numpy.fft import fft, ifft
from scipy.integrate import odeint
from scipy.linalg import cho_factor, cho_solve
from lorenz05 import *
from compact_fns import wendlandcos

def adv_model(x, t, parms, deriv, grad = None):
    try:
        return adv_model_rk(x, t, parms, deriv, grad)
    except:
        return adv_model_lsode(x, t, parms, deriv, grad)

def adv_model_lsode(x, t, parms, deriv, grad = None):
    out = odeint(deriv, x, t, args = (parms,), Dfun = grad)
    success = not isnan(out).any()
    return (out, success)

def adv_model_rk(x, t, parms, deriv, grad = None):
    out = rk4(deriv, parms).adv_model(x, t, parms["dt"])
    success = not isnan(out).any()
    return (out, success)

def adv_model_euler(x, t, parms, deriv, grad = None):
    out = [x] * 2
    out[1] = x + diff(t) * deriv(x, t, parms)
    return array(out)

def deriv(y, t, parms):
    #dt = [0. for x in range(parms["N"])]
    # subroutine comp_dt(z, dt, model_number, model_size, K, K2, K4, H, forcing, &
    #               smooth_steps, ss2, space_time_scale, sts2, coupling, &
    #               alpha_minus_betai)
    dx = \
    comp_dt(y,                     # z
            #dx,                   # dx
            parms["model_number"], # model_number
            #parms["N"],           # model_size
            parms["K"],            # K
            parms["K2"],           # K2
            parms["K4"],           # K4
            parms["J"],            # H
            parms["F"],            # forcing
            parms["I"],            # smooth_steps
            #parms["I2"],          # ss2
            parms["b"],            # space_time_scale
            parms["b_sq"],         # sts2
            parms["c"],            # coupling
            parms["alpha_minus_betai"])#, # alpha_minus_betai
            #parms["N"], parms["I2"])

    return dx

def deriv_plus_Lb(y, t, parms):
    dx = deriv(y, t, parms)
    dx += fft( parms["c_L_fft"] * ifft(y) ).real + parms["Fplus"]
    return dx

def deriv_plus_L(y, t, parms):
    dx = deriv(y, t, parms)
    dx += fft( parms["c_L_fft"] * ifft(y) ).real
    return dx

def deriv_plus_Lb_old(y, t, parms):
    dx = \
    comp_dt_linear(y,                     # z
            #dx,                   # dx
            parms["model_number"], # model_number
            #parms["N"],           # model_size
            parms["K"],            # K
            parms["K2"],           # K2
            parms["K4"],           # K4
            parms["J"],            # H
            parms["F"],            # forcing
            parms["I"],            # smooth_steps
            #parms["I2"],          # ss2
            parms["b"],            # space_time_scale
            parms["b_sq"],         # sts2
            parms["c"],            # coupling
            parms["alpha_minus_betai"], # alpha_minus_betai
            #parms["N"], parms["I2"],
            parms["L"], parms["Fplus"])

    return dx

def deriv_wrong(y, t, parms):
    #dt = [0. for x in range(parms["N"])]
    # subroutine comp_dt(z, dt, model_number, model_size, K, K2, K4, H, forcing, &
    #               smooth_steps, ss2, space_time_scale, sts2, coupling, &
    #               alpha_minus_betai)
    dx = \
    comp_dt_wrong(y,                     # z
            #dx,                   # dx
            parms["model_number"], # model_number
            #parms["N"],           # model_size
            parms["K"],            # K
            parms["K2"],           # K2
            parms["K4"],           # K4
            parms["J"],            # H
            parms["F"],            # forcing
            parms["I"],            # smooth_steps
            #parms["I2"],          # ss2
            parms["b"],            # space_time_scale
            parms["b_sq"],         # sts2
            parms["c"],            # coupling
            parms["alpha_minus_betai"])#, # alpha_minus_betai
            #parms["N"], parms["I2"])

    if isnan(dx).any(): return ValueError("Returned 'nan'.")

    return dx

def grad(x, dt, parms):
    return \
    linearize_dt(x,            # x
               #M,           # M
               #parms["N"],   # model_size
               parms["K"],   # K
               parms["K2"],  # K2
               parms["K4"],  # K4
               parms["Ksq"], # Ksq
               parms["J"])   # H

def calc_consts(parms):
    parms["model_number"] = int(parms["model_number"])
    parms["N"] = int(parms["N"])
    parms["K"] = int(parms["K"])
    parms["F"] = float(parms["F"])

    if parms["model_number"] == 2:
        # just dummy variables
        parms["I"] = 1
        parms["b"] = 1
        parms["c"] = 1
    elif parms["model_number"] != 3:
        return ValueError("Invalid 'model' number.")

    # if parms["K"] % 2 != 0: return ValueError("Code has only been implemented for 'K' even.")

    parms["I"] = int(parms["I"])
    parms["b"] = float(parms["b"])
    parms["c"] = float(parms["c"])

    parms["J"] = parms["K"]/2 if parms["K"] % 2 == 0 else (parms["K"]-1)/2 # integer
    parms["K2"] = 2*parms["K"]  # integer
    parms["K4"] = 4*parms["K"]  # integer
    parms["Ksq"] = parms["K"]**2  # integer
    parms["I2"] = 2*parms["I"]  # integer
    parms["alpha"] = (3.*parms["I"]**2 + 3.) / (2.*parms["I"]**3 + 4.*parms["I"])
    parms["beta"] = (2.*parms["I"]**2 + 1.) / (parms["I"]**4. + 2.*parms["I"]**2)
    parms["alpha_minus_betai"] = [parms["alpha"] - parms["beta"] * abs(x) for x in range(-parms["I"], parms["I"]+1)]
    parms["b_sq"] = parms["b"]**2
    parms["consts_calculated"] = True

    return parms

def calc_b(parms):
    parms["b_sq"] = parms["b"]**2
    return parms

def calc_c(parms):
    parms["c"] = parms["b_sq"] * parms["cprime"]
    return parms

def calc_K(parms):
    parms["J"] = parms["K"]/2 if parms["K"] % 2 == 0 else (parms["K"]-1)/2 # integer
    parms["K2"] = 2*parms["K"]  # integer
    parms["K4"] = 4*parms["K"]  # integer
    parms["Ksq"] = parms["K"]**2  # integer
    return parms

def calc_I(parms):
    parms["I2"] = 2*parms["I"]  # integer
    parms["alpha"] = (3.*parms["I"]**2 + 3.) / (2.*parms["I"]**3 + 4.*parms["I"])
    parms["beta"] = (2.*parms["I"]**2 + 1.) / (parms["I"]**4. + 2.*parms["I"]**2)
    parms["I"] = int(parms["I"])
    parms["alpha_minus_betai"] = [parms["alpha"] - parms["beta"] * abs(x) for x in range(-parms["I"], parms["I"]+1)]
    return parms

def calc_ab(parms):
    parms["alpha_minus_betai"] = [parms["alpha"] - parms["beta"] * abs(x) for x in range(-parms["I"], parms["I"]+1)]
    return parms

def calc_Lb(parms):
    # L = a Sigma_dx Sigma_xx^-1 = a (Sigma_xx^-1 Sigma_xd)^T
    # b = mu_d - L mu_x
    # parameters: a, (period_xx, cutoff_xx), (period_dx, cutoff_dx), mu_d, mu_x
    c_dx = wendlandcos(parms["distvec"],
                       cutoff = parms["cutoff_dx"], tau = parms["tau_dx"], q = parms["q_dx"],
                       period_frac = parms["period_dx"], n = parms["n_wavelengths"])
    c_xx = wendlandcos(parms["distvec"],
                       cutoff = parms["cutoff_xx"], tau = parms["tau_xx"], q = parms["q_xx"],
                       period_frac = parms["period_xx"], n = parms["n_wavelengths"])
    c_L_fft = parms["multiplier"] * fft(c_dx) / fft(c_xx)
    parms["c_L_fft"] = c_L_fft # L = circulant(ifft(c_L_fft).real)
    parms["Fplus"] = parms["mu_d"]*ones(parms["N"]) - fft( c_L_fft * ifft(parms["mu_x"]) ).real
    return parms

def calc_Lb_old(parms):
    # L = a Sigma_dx Sigma_xx^-1 = a (Sigma_xx^-1 Sigma_xd)^T
    # b = mu_d - L mu_x
    # parameters: a, (period_xx, cutoff_xx), (period_dx, cutoff_dx), mu_d, mu_x
    Sigma_dx = \
        wendlandcos(parms["distmat"],
                    cutoff = parms["cutoff_dx"], tau = parms["tau_dx"], q = parms["q_dx"],
                    period_frac = parms["period_dx"], n = parms["N"])
    Sigma_xx = \
        wendlandcos(parms["distmat"],
                    cutoff = parms["cutoff_xx"], tau = parms["tau_xx"], q = parms["q_xx"],
                    period_frac = parms["period_xx"], n = parms["N"])
    parms["L"] = parms["multiplier"] * cho_solve(cho_factor(Sigma_xx), Sigma_dx).T
    parms["Fplus"] = parms["mu_d"]*ones(parms["N"]) - dot(parms["L"], parms["mu_x"])
    return parms

def calc_L(parms):
    # L = a Sigma_dx Sigma_xx^-1 = a (Sigma_xx^-1 Sigma_xd)^T
    # b = mu_d - L mu_x
    # parameters: a, (period_xx, cutoff_xx), (period_dx, cutoff_dx), mu_d, mu_x
    c_dx = wendlandcos(parms["distvec"],
                       cutoff = parms["cutoff_dx"], tau = parms["tau_dx"], q = parms["q_dx"],
                       period_frac = parms["period_dx"], n = parms["n_wavelengths"])
    c_xx = wendlandcos(parms["distvec"],
                       cutoff = parms["cutoff_xx"], tau = parms["tau_xx"], q = parms["q_xx"],
                       period_frac = parms["period_xx"], n = parms["n_wavelengths"])
    c_L_fft = parms["multiplier"] * fft(c_dx) / fft(c_xx)
    parms["c_L_fft"] = c_L_fft # L = circulant(ifft(c_L_fft).real)
    return parms

class rk4(object):
    def __init__(self, deriv, parms):
        self.deriv = deriv
        self.parms = parms

    def adv_1step(self, x, dt):
    # This is how DART integrates the Lorenz 2005 model, cf subroutine adv_1step in DART/Kodiak/models/lorenz_04/model_mod.f90
    # DART's comment:
    #     Does single time step advance for lorenz 04 model
    #     using four-step rk time step

        # Compute the first intermediate step
        dx = self.deriv(x, 0, parms = self.parms)
        x1    = dt * dx
        inter = x + x1 / 2.

        # Compute the second intermediate step
        dx = self.deriv(inter, 0, parms = self.parms)
        x2    = dt * dx
        inter = x + x2 / 2.

        # Compute the third intermediate step
        dx = self.deriv(inter, 0, parms = self.parms)
        x3    = dt * dx
        inter = x + x3

        # Compute fourth intermediate step
        dx = self.deriv(inter, 0, parms = self.parms)
        x4 = dt * dx

        # Compute new value for x
        dxt = x1/6. + x2/3. + x3/3. + x4/6.

        return x + dxt

    def adv_model(self, x, t, dt, verbose = False):
        # Wrapper for adv.1step. The output matches the structure of the 'ode' function of the 'deSolve' package
        T = t.size

        # initialize output
        out = zeros((T, x.size))
        out[0,:] = x
        y = x

        # number of integration steps
        int_steps = zeros(T)
        int_steps[1:] = diff(t) / dt
        int_steps = int_steps.round().astype(int)

        # do the integration
        for i in range(1, T):
          if verbose:
            if i % 100 == 0: print i + " / " + "T"
          for j in range(int_steps[i]):
            y = self.adv_1step(x = y, dt = dt)
            #if sum(isnan(y)) > 1: embed()
            out[i,:] = y

        return out



