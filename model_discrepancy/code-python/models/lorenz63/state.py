
from numpy import array, fill_diagonal, zeros, diff, isnan, sqrt, dot
from scipy.integrate import odeint
from IPython import embed

def adv_model(x, t, parms, deriv, grad = None):
    return (odeint(deriv, x, t, args = (parms,), Dfun = grad), True)

def adv_model_euler(x, t, parms, deriv, grad = None):
    out = [x] * 2
    out[1] = x + diff(t) * deriv(x, t, parms)
    return (array(out), True)

def adv_model_rk(x, t, parms, deriv, grad = None):
    out = rk2(deriv, parms).adv_model(x, t, parms["dt"])
    success = not isnan(out).any()
    return (out, success)

def deriv_right(y, t, parms):
    dX = parms["sigma"] * (y[1] - y[0])
    dY = - y[2] * y[0] + parms["rho"] * y[0] - y[1]
    dZ = y[0] * y[1] - parms["beta"] * y[2]
    return array([dX, dY, dZ])

def grad_right(x, dt, parms):
    jac = zeros((3,3))
    jac[0,:] = array((  -parms["sigma"],    parms["sigma"],       0        ))
    jac[1,:] = array(( parms["rho"] - x[2],     -1,            -x[0]       ))
    jac[2,:] = array((     x[1],                x[0],       -parms["beta"] ))
    return jac

def deriv_wrong(y, t, parms):
    dX = parms["sigma"] * (y[1] - y[0])
    dY = - y[2] * y[0] + parms["rho"] * y[0] - y[1]
    dZ = parms["alpha"] * y[0] * y[1] - parms["beta"] * y[2]
    out = array([dX, dY, dZ])
    out += parms["F"]
    return out

def deriv_wrong_linearcorrection(y, t, parms):
    dX = parms["sigma"] * (y[1] - y[0])
    dY = - y[2] * y[0] + parms["rho"] * y[0] - y[1]
    dZ = parms["alpha"] * y[0] * y[1] - parms["beta"] * y[2]
    out = array([dX, dY, dZ])
    # embed()
    # raise Exception()
    out += (dot(parms["L"], y[:,None]) + parms["b"])[:,0]
    return out

def grad_wrong(x, dt, parms):
    jac = zeros((3,3))
    jac[0,:] = array((  -parms["sigma"],       parms["sigma"],         0        ))
    jac[1,:] = array(( parms["rho"] - x[2],        -1,              -x[0]       ))
    jac[2,:] = array(( parms["alpha"]*x[1], parms["alpha"]*x[0], -parms["beta"] ))
    return jac

def hess_wrong(f, x, dt, parms):
    hess = zeros((3,3))
    if f == 2:
        hess[0,1] = parms["alpha"]
        hess[1,0] = parms["alpha"]
    elif f < 0 or f > 2:
        raise KeyError("'f' needs to be equal to 0, 1, or 2 (corresponds to x, y, or z).")

    return dt * hess

class rk2(object):
    def __init__(self, deriv, parms):
        self.deriv = deriv
        self.parms = parms

    def adv_1step(self, x, dt, fract = 1):
    # This is how DART integrates the Lorenz 1963 model, cf subroutine adv_single in DART/Kodiak/models/lorenz_63/model_mod.f90
    # DART's comment:
    #     does single time step advance for lorenz convective 3 variable model
    #     using two step rk time step

        # Compute the first intermediate step
        dx = self.deriv(x, 0, parms = self.parms)
        x1 = x + fract * dt * dx

        # Compute the second intermediate step
        dx = self.deriv(x1, 0, parms = self.parms)
        x2 = x1 + fract * dt * dx

        #  new value for x is average of original value and second intermediate
        out = (x + x2) / 2.

        return out

    def adv_model(self, x, t, dt, verbose = False):
        # Wrapper for adv.1step. The output matches the structure of the 'ode' function of the 'deSolve' package
        T = t.size

        # initialize output
        out = zeros((T, x.size))
        out[0,:] = x
        y = x

        # number of integration steps
        int_steps = zeros(T)
        int_steps[1:] = diff(t) / dt
        int_steps = int_steps.round().astype(int)

        # do the integration
        for i in range(1, T):
          if verbose:
            if i % 100 == 0: print i + " / " + "T"
          for j in range(int_steps[i]):
            y = self.adv_1step(x = y, dt = dt)
            #if sum(isnan(y)) > 1: embed()
            out[i,:] = y

        return out



















