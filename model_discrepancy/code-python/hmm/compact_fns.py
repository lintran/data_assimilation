
from __future__ import division
from numpy import finfo, floor, cos, pi, logical_or, logical_not

def tricube(dist, cutoff):
    r = abs(dist / cutoff)
    return 70 / 81 * (1-r**3)**3 * float(r <= 1)

def wendland(dist, cutoff, q, dim = 1):
    """
    dist: distance matrix
    dim: number of dimensions
    q: cov fn is 2*q-times continuously differentiable, thus GP is q-times mean-square differentiable
    """

    # if type(dim) is not int: raise ValueError("'dim' must be an integer.")
    # if type(q) is not int: raise KeyError("'q' must be an integer.")

    # if (dist < 0).any(): raise ValueError("'dist' must be nonnegative.")
    # if dim < 0: raise KeyError("'dim' must be nonnegative.")
    # if q < 0: raise ValueError("'q' must be nonnegative.")

    if cutoff == 0: cutoff = finfo(float).eps
    dist = dist / cutoff
    j = float(floor(dim/2) + q + 1)
    a = fplus(1-dist)**(j+q)

    if   q == 0: num = 1
    elif q == 1: num = (j+1) * dist + 1
    elif q == 2: num = (j**2 + 4*j + 3) * dist**2 + (3*j + 6) * dist + 3
    elif q == 3: num = (j**3 + 9*j**2 + 23*j + 15) * dist**3 + (6*j**2 + 36*j + 45) * dist**2 + (15*j + 45) * dist + 15
    else:        raise KeyError("'q' can only take integer values between 0 and 3, inclusive.")

    den = 1
    if q == 2:   den = 3
    elif q == 3: den = 15

    return a * num / den

def wendlandsph(dist, cutoff, q, dim = 1):
    """
    dist: distance matrix
    dim: number of dimensions
    q: cov fn is 2*q-times continuously differentiable, thus GP is q-times mean-square differentiable
    """

    # if type(dim) is not int: raise ValueError("'dim' must be an integer.")
    # if type(q) is not int: raise KeyError("'q' must be an integer.")

    # if (dist < 0).any(): raise ValueError("'dist' must be nonnegative.")
    # if dim < 0: raise KeyError("'dim' must be nonnegative.")
    # if q < 0: raise ValueError("'q' must be nonnegative.")

    if cutoff == 0: cutoff = finfo(float).eps
    dist = dist / cutoff
    j = float(floor(dim/2) + q + 1)
    a = fplus(1-dist)**(j+q)

    if   q == 1: num = j*dist + 1
    elif q == 2: num = (j**2 + 1) * dist**2 + 3*j*dist + 3
    else:        raise KeyError("'q' can only take integer values between 0 and 3, inclusive.")

    den = 1
    if q == 2:   den = 3

    return a * num / den

def wendlandcos(dist, cutoff, q, period_frac, n, dim = 1):
    # period_frac is relative to the cutoff.
    #      cos(2*pi* dist * n) with dist \in [0,1]
    # n controls the number of wavelengths on the distance domain of [0,1]
    #      cos[2*pi * (dist*cutoff) * (n/cutoff)]
    # Now: period_frac = n/cutoff = number of wavelengths on the domain of [0, cutoff]
    #      with dist*cutoff \in [0, cutoff]
    if period_frac < 0 or period_frac > 1: raise ValueError("'period_frac' must be between 0 and 1.")
    w = wendland(dist, cutoff, q, dim)
    c = cos(2*pi * dist * period_frac * n)
    return w*c

def wendlandcossph(dist, cutoff, q, period_frac, n, dim = 1):
    if period_frac < 0 or period_frac >= 1: raise ValueError("'period_frac' must be between 0 and 1")
    w = wendlandsph(dist, cutoff, q, dim)
    c = cos(2*pi * dist * period_frac * n)
    return w*c

def fplus(a):
    return a*(a>0)

def gasparicohn(dist, halfwidth):
    out = dist / halfwidth

    idx1 = dist >= halfwidth*2
    idx2 = dist <= halfwidth
    idx3 = logical_not(logical_or(idx1, idx2))

    out[idx1] = 0
    out[idx2] = ( ( ( -0.25*out[idx2] +0.5 )*out[idx2] +0.625 )*out[idx2] -5.0/3.0 )*out[idx2]**2 + 1.0
    out[idx3] = ( ( ( ( out[idx3]/12.0 -0.5 )*out[idx3] +0.625 )*out[idx3] +5.0/3.0 )*out[idx3] -5.0 )*out[idx3] + 4.0 - 2.0 / (3.0 * out[idx3])

    return out
