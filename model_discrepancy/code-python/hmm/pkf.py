
from pf import *
from numpy.linalg import svd

class pkf(pf):
    def __init__(self, *args, **kwargs):
        self.filter_type = "pkf"
        self.uniform_weights = kwargs.pop("uniform_weights", False)
        super(pkf, self).__init__(*args, **kwargs)
        if self.ess_threshold is None: self.ess_threshold = self.M+1
        if self.post_reg_type is not None:
            logging.warning("Post-regularization does not apply to particle Kalman filters. 'post_reg_type' will be ignored.")
            self.post_reg_type = None
        if self.pre_reg_type is None:
            logging.warning("Particle Kalman filter requires is a type of pre-regularization filter. Since 'pre_reg_type' was not specified, will set it to be 'diag'.")
            self.pre_reg_type = "diag"

    def update_ti0(self, verbose = True):
        super(pkf, self).update_ti0(verbose)
        ti = 0
        if self.pre_reg_type is not "diag":
            Z_f = self.Zf_sample(ti)
            mu_f = self.ensemble.mean["forecast"][ti,:]
            self.calc_KG_and_Sigma_a(Z_f, mu_f)
            # FIXME: these lines of code are done AFTER inflation/perturbation, which is incorrect. should be done before.
        else:
            Z_f = diag(self.reg_multiplier * ones(self.N))
            mu_f = self.ensemble.mean["forecast"][ti,:]
            self.calc_KG_and_Sigma_a(Z_f, mu_f)

    def resample_t(self, ti):
        log_wt0, yes = self.resample_or_not(ti)
        m, log_wt0 = self.resample(log_wt0, yes)
        if ti != self.T-1: self.ensemble.update_parent("forecast", ti+1, m, yes, log_wt0)
        if yes: self.resample_mixturedist(ti, m)
        return m, log_wt0

    def forecast_t(self, ti, m, log_wt0, parms = None, inflate = None, perturb = None):
        self.propagate(ti, m = None, logwt = log_wt0, parms = parms, inflate = inflate, perturb = perturb)
        if self.pre_reg_type is not "diag":
            Z_f = self.Zf_sample(ti)
            mu_f = self.ensemble.mean["forecast"][ti,:]
            self.calc_KG_and_Sigma_a(Z_f)
        else:
            Z_f = diag(self.reg_multiplier * ones(self.N))
            mu_f = self.ensemble.mean["forecast"][ti,:]
            self.calc_KG_and_Sigma_a(Z_f, mu_f)
        return log_wt0

    def update_t(self, ti, log_wt0):
        return self.update(ti, log_wt0)

    def resample_mixturedist(self, ti, m):
        if (self.pre_reg_type is "diag" or self.pre_reg_type is "sample_diag") and self.hmm.measurement_model.identity:
            add = dot(sqrt(self.Sigma_a), normal().rvs(size = (self.N, self.M)))
        else:
            add = mvnorm(cov = self.Sigma_a).rvs(self.M).T

        self.ensemble.ens["analysis"][:,:,ti] = self.ensemble.ens["analysis"][:,m,ti] + add
        #self.ensemble.calc_stats("analysis", ti-1, logwt = -log(self.M)*ones(self.M))

    def mif_resample_t(self, ti):
        log_wt0, yes = self.resample_or_not(ti) if not self.uniform_weights else (-log(self.M)*ones(self.M), True)
        m, log_wt0 = self.resample(log_wt0, yes)
        if ti != self.T-1: self.ensemble.update_parent("forecast", ti+1, m, yes, log_wt0)
        parms = self.mif_resample_mixturedist(ti, m) if yes else self.ensemble.ens["forecast_param"][:,m,ti]
        self.ensemble.ens["analysis_param"][:,:,ti] = parms
        self.ensemble.calc_stats("analysis_param", ti, logwt = log_wt0)
        return log_wt0, parms

    def mif_forecast_t(self, ti, m, log_wt0, M_subset, parms_dict, parms_vec = None, inflate = None, perturb = None):
        return self.forecast_t(ti, m, log_wt0, parms_dict, sd_x)

    def mif_update_t(self, ti, log_wt0):
        return self.update_t(ti, log_wt0)

    def mif_resample_mixturedist(self, ti, m):
        self.resample_mixturedist(ti, m)
        out = self.ensemble.ens["param"][:,m,ti]
        self.ensemble.ens["param"][:,:,ti] = out
        return out
        # if self.pre_reg_type is "diag":
        #     D = self.ensemble.mean["param"].shape[1]
        #     add = self.reg_multiplier * normal().rvs(size = (D, self.M))
        #     out = self.ensemble.ens["param"][:,m,ti] + add
        #     self.ensemble.ens["param"][:,:,ti] = out
        #     return out
        # else:
        #     raise KeyError("Pre-regularization type %s has not been implemented for MIF with PKF." % self.pre_reg_type)

    def init_reg(self):
        self.calc_reg_opt()

        if self.pre_reg_type is "diag":
            Z_f = diag(self.reg_multiplier * ones(self.N))
        else:
            Z_f = self.Zf_sample(0)

        self.calc_KG_and_Sigma_a(Z_f)

    def update(self, ti, log_wt0):
        H    = self.hmm.measurement_model.H
        x_f  = self.ensemble.ens["forecast"][:,:,ti]
        y    = self.hmm.y[ti,:][:,newaxis]
        diff = y - dot(H, x_f)

        # calculate particle's marginal likelihood, g( y_t | x_t^(i) )
        log_gt1 = mvnorm.logpdf(diff.T, mean = None, cov = self.Sigma_y)
        self.ensemble.ancestry["filter"]["loglik"][ti,:] = log_gt1

        # calculate marginal likelihood, sum w_t-1^(i) g( y_t | x_t^(i) )
        log_wt = log_wt0 + log_gt1
        self.loglik_t["filter"][ti] = sum_weight(log_wt)

        # update weight
        log_wt = norm_weight(log_wt)
        self.ensemble.logwt["analysis"][ti,:] = log_wt

        # calculate analysis means
        self.ensemble.ens["analysis"][:,:,ti] = \
            x_f + crossprod(self.KG_t, diff)

        mean = self.ensemble.calc_mean("analysis", ti, log_wt)
        self.ensemble.mean["analysis"][ti,:] = mean
        self.calc_analysis_sd(ti, log_wt, mean)

        return log_wt

    def calc_analysis_sd(self, ti, logwt, mu):
        """
        Calculates the sd of a mixture distribution
        http://en.wikipedia.org/wiki/Mixture_distribution#Moments
        """
        key = "analysis"
        wt = exp(logwt)

        mu_i  = self.ensemble.ens[key][:,:,ti] # N x M
        #mu    = self.ensemble.mean[key][ti,:][:,newaxis]
        mu    = mu[:,newaxis]
        var_i = diag(self.Sigma_a)[:,newaxis]
        var   = average((mu_i - mu)**2. + var_i, axis = 1, weights = wt)
        logv1 = 1 #sum_weight(logwt)
        logv2 = sum_weight(2*logwt)
        den = 1 - exp(logv2 - 2*logv1)
        self.ensemble.sd[key][ti,:] = sqrt(var / den)

    def Zf_sample(self, ti):
        if self.pre_reg_type is "sample":
            mu_f = self.ensemble.mean["forecast"][ti,:][:,newaxis]
            U_f = self.ensemble.ens["forecast"][:,:,ti] - mu_f
            Z_f = 1/sqrt(self.M-1) * U_f # Sigma.f = Z.f %*% t(Z.f) = cov(t(forecast.ensemble))

        elif self.pre_reg_type is "sample_diag":
            Z_f = diag(self.ensemble.sd["forecast"][ti,:])

        elif self.pre_reg_type is "tlm_mean":
            if ti > 1:
                x0 = self.ensemble.mean[ti-1,:]
                dt = self.hmm.ty[ti] - self.hmm.ty[ti-1]
                Z_f = self.hmm.forward_model.tlm(x0, dt)
            else:
                Z_f = diag(ones(self.N))

        elif self.pre_reg_type is "svd":
            U, d, V = svd(self.ensemble.ens["forecast"][:,:,ti])
            d_sq = d**2
            idx = where(cumsum(d_sq/d_sq.sum()) > .95)[0][0] + 1
            d[range(idx,self.M)] = 0
            U_f = U[:,range(idx)] * d[newaxis,:]
            Z_f = 1/sqrt(self.M-1) * U_f

        elif self.pre_reg_type is "rodeo":
            a = self.ensemble.ens["forecast"][:,:,ti].T
            b = global_rodeo_bw(a, c0 = self.ensemble.sd["forecast"][ti,:].max()*10)
            Z_f = diag(b.bw)
            Z_f /= self.reg_multiplier

        return self.reg_multiplier * Z_f

    def calc_KG_and_Sigma_a(self, Z_f, mu_f = None):
        # TODO: make this go faster if both Sigma_f and Sigma_o is diagonal
        H_Zf = dot(self.hmm.measurement_model.H, Z_f) # H %*% Z.f
        H_Sigma_f = tcrossprod(H_Zf, Z_f)
        self.Sigma_y = tcrossprod(H_Zf) + self.hmm.measurement_model.Sigma.Sigma # H Sigma_f H + Sigma_0
        self.KG_t = solve(self.Sigma_y, H_Sigma_f) # transpose of Kalman gain = [H Sigma_f H + Sigma_0]^-1 H Sigma_f
        self.Sigma_a = tcrossprod(Z_f) - crossprod(self.KG_t, H_Sigma_f)



