
from numpy import inf, append, cov
from numpy.random import choice
from scipy.io import savemat
from pf_param import *
from IPython import embed

class pmcmc(pf_param):
    def __init__(self, log_prior, *args, **kwargs):

        self.log_prior = log_prior

        # extra kwargs
        self.param_adaptive = kwargs.pop("param_adaptive", False)
        self.lap_steps = kwargs.pop("lap_steps", 1)
        self.lap_b_start = kwargs.pop("lap_b_start", 1)
        self.lap_c0 = kwargs.pop("lap_c0", 1)
        self.lap_c1 = kwargs.pop("lap_c1", .8)
        self.lap_ropt = kwargs.pop("lap_ropt", .234)
        self.lap_update_log = kwargs.pop("lap_update_log", True)
        self.keep_x = kwargs.pop("keep_x", False)
        super(pmcmc, self).__init__(*args, **kwargs)

        if self.D_dict["obs"] > 0: raise ValueError("PMCMC has not been implemented to estimate observation model parameters.")

    def init_cov(self):
        super(pmcmc, self).init_cov()
        if self.param_adaptive:
            self.lap_gamma1 = 1
            self.lap_gamma2 = self.lap_c0 * self.lap_gamma1

            self.lap_sigma_sq = 2.4**2/self.D
            self.lap_mean = zeros(self.D)
            self.lap_cov = self.param_cov

            self.lap_step_count = 0

    def init_sample(self):
        super(pmcmc, self).init_sample()

        # create record keeping arrays
        self.records["keep_new"] = empty(self.B, dtype = bool)
        if self.keep_x: self.records["x"] = nones((self.B, self.pf.N, self.pf.T))

        # init old params
        self.param_old = self.records["param"][0,:]
        self.reset_filter()
        self.run_filter()
        self.accept_reject()
        self.loglik_old = self.loglik()
        self.logp_old   = self.loglik_old + self.log_prior(self.param_old)
        if self.keep_x: self.x_old = self.pf.sample_x()

        self.records["loglik"][0]   = self.loglik_old
        self.records["param"][0,:]  = self.param_old
        self.records["keep_new"][:] = False
        if self.keep_x: self.records["x"][0,:,:] = self.x_old

        # print initial progress report
        b = self.b-1
        d = self.D_print
        print_me = "%i lik: %.2f initial: " % (b, self.loglik_old)
        print_me += reduce(lambda x,y: x+y, ["%.2f " % x for x in self.param_old[:d]])
        if self.param_adaptive: print_me += "lap: %.2f" % (self.lap_sigma_sq)
        print print_me

    def sample(self):
        # sample parameters
        self.param_new = self.sample_param(self.param_old)
        param_new_dict = self.param_vec_to_dict_state(self.param_new)
        self.logp_param_new = self.log_prior(self.param_new)

        # if parameters are ok, sample hidden states
        if self.check_param():
            self.change_param(self.param_new)
            self.reset_filter()
            self.run_filter()
            self.loglik_new = self.loglik()
            if self.keep_x: self.x_new = self.pf.sample_x()
        else:
            self.loglik_new = -inf

        # decide to accept/reject
        self.accept_reject()
        self.print_progress()
        self.b += 1

    def print_progress(self):
        b = self.b
        d = self.D_print
        acc_rate = self.records["keep_new"][range(b+1)].mean()
        result = "accepted" if self.records["keep_new"][b] else "rejected"

        print_me = "%i lik: %.2f prob: %.3f %s: " % (b, self.loglik_new, self.accept_prob, result)
        print_me += reduce(lambda x,y: x+y, ["%.2f " % x for x in self.param_new[:d]])
        print_me += "acc: %.3f " % (acc_rate)
        if self.param_adaptive and self.lap_step_count == 0: print_me += "lap: %.2f" % (self.lap_sigma_sq)

        print print_me

    def sample_param(self, mean):
        # TODO: make this more general (not just MVN)
        out = mvnorm(mean, self.param_cov).rvs()
        if out.size == 1: out = array([out])
        if self.param_int.any():
            for i in range(self.D):
                if self.param_int[i]: out[i] = round(out[i])
        return out

    def check_param(self):
        """
        Checks for immediate rejection of parameter.
        """
        ok_sample_x = True

        # check range constraints
        if self.check_param_range.any():
            ok_sample_x = array([self.param_new >= self.param_min, self.param_new <= self.param_max]).all()

        # check prior probability
        if self.logp_param_new == -inf:
            ok_sample_x = False

        return ok_sample_x

    def accept_reject(self):
        b = self.b
        self.calc_accept_prob()
        accept_new = choice([True, False], size = 1, p = [self.accept_prob, 1-self.accept_prob])
        self.records["keep_new"][b] = accept_new

        if accept_new:
            self.records["loglik"][b] = self.loglik_new
            self.records["param"][b,:] = self.param_new
            if self.keep_x: self.records["x"][b,:,:] = self.x_new

            self.loglik_old = self.loglik_new
            self.param_old  = self.param_new
            self.logp_old   = self.logp_new
            if self.keep_x: self.x_old = self.x_new
        else:
            self.records["loglik"][b] = self.loglik_old
            self.records["param"][b,:] = self.param_old
            if self.keep_x: self.records["x"][b,:,:] = self.x_old

        if self.param_adaptive: self.update_cov()

    def calc_accept_prob(self, log_out = False):
        if self.loglik_new == -inf:
            self.logp_new = -inf
            log_p = -inf
        else:
            self.logp_new = self.loglik_new + self.logp_param_new
            log_p = self.logp_new - self.logp_old
            log_p = append(0, log_p).min()
        self.accept_prob = exp(log_p)

    def update_cov(self):
        self.lap_step_count += 1

        if self.lap_step_count >= self.lap_steps and self.b >= self.lap_b_start:
            self.lap_step_count = 0
            self.lap_gamma1 = 1/self.b**self.lap_c1
            self.lap_gamma2 = self.lap_c0 * self.lap_gamma1

            if self.lap_steps == 1:
                diff = self.accept_prob - self.lap_ropt
                self.lap_cov += self.lap_gamma1 * (tcrossprod(self.param_new - self.lap_mean) - self.lap_cov)
                self.lap_mean += self.lap_gamma1 * (self.param_new - self.lap_mean)
            else:
                idx = [self.b-x for x in range(self.lap_steps)]
                r = self.records["keep_new"][idx].mean()
                Sigma = cov(self.records["param"][idx,:].T)
                diff = r - self.lap_ropt
                self.lap_cov += self.lap_gamma1 * (Sigma - self.lap_cov)

            if self.lap_update_log:
                self.lap_sigma_sq = exp(log(self.lap_sigma_sq) + self.lap_gamma2 * diff)
            else:
                self.lap_sigma_sq += self.lap_gamma2 * diff

            self.param_cov = self.lap_sigma_sq * self.lap_cov

    def savemat(self, filename):
        """ Save as MATLAB-style .mat file. """
        savemat(filename,
            {'lap_cov': self.lap_cov, 'lap_sigma_sq': self.lap_sigma_sq,
             'param_cov': self.param_cov, 'param_names': self.param_names,
             'param_sizes': self.param_sizes, 'records': self.records})















