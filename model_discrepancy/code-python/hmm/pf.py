
from numpy import exp, log, newaxis, unique, in1d, floor, repeat, arange, empty, isnan, isinf
from numpy.random import choice
from math import pi
from scipy.special import gammaln

from IPython import embed

from hmm import *

class pf(hmm_sampler):
    # TODO: test why this is slower for 1d?
    # FIXME: deal with inflation / perturbation for PF class
    def __init__(self, hmm, ics,
                 resample_type = "multinomial",
                 post_reg_type = None,
                 pre_reg_type = None,
                 reg_C = 1.,
                 resample_ess = True,
                 ess_threshold = None,
                 **kwargs):

        # record inputs
        self.resample_type = resample_type
        self.post_reg_type = post_reg_type
        self.pre_reg_type = pre_reg_type
        self.reg_C = float(reg_C)
        self.resample_ess = resample_ess
        self.ess_threshold = ess_threshold

        # check filter and resample types
        ok_resample_type = ["multinomial", "residual", "systematic"]
        ok_post_reg_type = ["sample", "diag"]
        ok_pre_reg_type  = ["sample", "sample_diag", "diag", "tlm_mean", "svd", "rodeo"]
        if not in1d(resample_type, ok_resample_type) and resample_type is not None:
            raise KeyError("Don't recognize that `resample_type`.")
        if not in1d(post_reg_type, ok_post_reg_type) and post_reg_type is not None:
            raise KeyError("Don't recognize that `post_reg_type`.")
        if not in1d(pre_reg_type, ok_pre_reg_type) and pre_reg_type is not None:
            raise KeyError("Don't recognize that `pre_reg_type`.")
        if pre_reg_type is not None and post_reg_type is not None:
            raise KeyError("Can't pre- and post-regularize at the same time, i.e. only 'pre_reg_type' or 'post_reg_type' can be specified.")

        # init
        super(pf, self).__init__(hmm, ics, **kwargs)
        self.ensemble = ensemble_pf(self.N, self.T, self.M)
        self.init_filter()
        self.init_reg()

    def init_reg(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def filter(self, verbose = False):
        ti = self.ti["filter"]
        m, logwt = self.resample_t(ti-1)
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti-1) + "}"
        logwt = self.forecast_t(ti, m, logwt)
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti) + "}"
        self.update_t(ti, logwt)
        self.ti["filter"] += 1

    def resample_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def forecast_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def update_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def mif_resample_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def mif_forecast_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def mif_update_t(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def init_filter(self):
        self.ti["filter"] = 1
        self.ensemble.ens["analysis"][:,:,0] = self.ics.ens
        self.ensemble.logwt["analysis"][0,:] = -log(self.M) * ones(self.M)
        self.ensemble.calc_stats("analysis", 0)

    def update_ti0(self, verbose = True):
        if verbose: print "Sampling x" + str(0) + " | y0"
        ti = 0
        log_wt0 = -log(self.M)*ones(self.M)

        # forecast
        self.ensemble.ens["forecast"][:,:,0] = self.ics.ens
        self.ensemble.logwt["forecast"][0,:] = log_wt0
        self.ensemble.calc_stats("forecast", ti, log_wt0)

        # update
        self.update(ti, log_wt0)

    def resample_or_not(self, ti):
        ess, log_wt0 = self.ensemble.ess(ti)
        choice = self.ess_check(ess) if self.resample_ess else True

        # overrides: don't resample if...
        # (1) updating ti0 (initial conditions)
        # (2) there are uniform weights
        # (3) choose not to resample
        check1 = ti < 0
        check2 = log_wt0[0] == -log(self.M)
        check3 = self.resample_type is None
        if check1 or check2 or check3: choice = False

        # check if weights are ok
        if isnan(log_wt0).any() or isinf(log_wt0).any():
            self.filter_error = True
            self.filter_error_msg = "Weights are 'nan' or 'inf'."

        return log_wt0, choice

    def ess_check(self, ess):
        return ess <= self.ess_threshold

    def propagate_nonoise(self, ti, m = None, parms = None):
        if m is None: m = range(self.M)
        m_uniq = unique(m, return_inverse = True)
        x0 = self.reg_x(ti-1, m_uniq[0], "analysis")
        x1, success = self.multi_advance(x0, self.hmm.ty[ti-1:ti+1], no_noise = True, parms = parms)
        return x1[:, m_uniq[1]]

    def reg_x(self, ti, m, key = "analysis"):
        x = self.ensemble.ens[key][:,m,ti]

        if self.post_reg_type is not None:
            if self.post_reg_type == "diag":
                x += normal(0., self.reg_multiplier).rvs(x.shape)

            else:
                Sigma_a = self.reg_multiplier**2 * cov(self.ensemble.ens[key][:,:,ti])
                x += mvnorm(cov = Sigma_a).rvs(x.shape[1]).T

            #self.ensemble.ens[key][:,:,ti] = x

        return x

    def propagate(self, ti, m = None, logwt = None, parms = None, sd_x = None, inflate = None, perturb = None):
        x1 = self.propagate_nonoise(ti, m, parms)

        # add in noise manually
        fm = self.hmm.forward_model
        if fm.Sigma is not None:
            dt  = diff(self.hmm.ty[ti-1:ti+1]) if fm.Sigma_dt else 1.
            add = fm.Sigma.rvs(x1.shape, sd_multiply = dt) # NxM
            x1 += add
        self.ensemble.ens["forecast"][:,:,ti] = x1

        # inflate and perturb
        if inflate is not None:
            self.inflation_t[ti,:] = array([parms[m][inflate] for m in range(self.M)])
        if perturb is not None:
            self.perturbation_t[ti,:] = array([parms[m][perturb] for m in range(self.M)])
        if self.inflateperturb and self.inflateperturb_forecast:
            self.ensemble.ens["forecast"][:,:,ti] = self.inflate_and_perturb(ti, "forecast")

        self.ensemble.calc_stats("forecast", ti, logwt)
        if self.post_reg_type is not None: self.ensemble.forecast_analysis_same_ti(ti)

    def calc_reg_opt(self, N_add = 0):
        power = 1./(self.N + N_add + 4)
        log_A = log(4.) - log(self.N + N_add + 2)
        self.reg_opt = exp(power * (log_A - log(self.M)))
        #self.reg_opt = exp(-power * log(self.M))
        self.reg_multiplier = self.reg_C * self.reg_opt

    def update(self, ti, log_wt0, x = None): # TODO: generalize for arbitrary measurement prob
        """
        Calculates marginal log likelihood
                         [ g( y_t | x_t^(i) ) f( x_t^(i) | x_t-1^(i) ) ]
            w_t-1^(i) log[ ------------------------------------------- ]
                         [         q( x_t^(i) | x_t-1^(i), y_t )       ]
        assuming q( x_t^(i) | x_t-1^(i), y_t ) = f( x_t^(i) | x_t-1^(i) ) (bootstrap filter assumption) and g is normal.
        Therefore, it really calculates log [ w_t-1^(i) g( y_t | x_t^(i) ) ].

        log_wt0 = w_t-1^(i)
        """
        H = self.hmm.measurement_model.H
        y = self.hmm.y[ti,:][:,newaxis]
        if x is None: x = self.ensemble.ens["forecast"][:,:,ti]
        diff = y - dot(H,x)

        # calculate particle's marginal likelihood, g( y_t | x_t^(i) )
        log_gt1 = self.hmm.measurement_model.Sigma.logpdf(diff.T)
        self.ensemble.ancestry["filter"]["loglik"][ti,:] = log_gt1

        # calculate marginal likelihood, sum w_t-1^(i) g( y_t | x_t^(i) )
        log_wt = log_wt0 + log_gt1
        self.loglik_t["filter"][ti] = sum_weight(log_wt)

        # update weight
        log_wt = norm_weight(log_wt)
        self.ensemble.logwt["analysis"][ti,:] = log_wt

        self.ensemble.calc_stats("analysis", ti, log_wt)

        return log_wt

    def calc_loglik_t(self, ti, key = "filter", wt_key = "forecast", add = 0):
        """
        Calculates log p(y_t | y_{1:t-1}), where
            p(y_t | y_{1:t-1}) = sum_m wt
        and wt is calculated using 'calc_weight'.
        """
        logwt = self.ensemble.logwt[wt_key][ti,:]
        loglik = self.ensemble.ancestry["filter"]["loglik"][ti,:]
        self.loglik_t[key][ti] = sum_weight(logwt + loglik) + add

    def sample_x(self, key = "analysis"):
        wt = self.ensemble.norm_weight(-1, log_out = False)
        m = choice(self.M, size = 1, p = wt)
        a = self.ensemble.get_ancestry(self.T, m)
        return self.ensemble.ens[key][:, a, range(self.T)]

    def smooth(self, verbose = False):
        fm = self.hmm.forward_model
        if fm.Sigma is None: return KeyError("Cannot smooth when state transitions are deterministic.")

        ti = self.ti["smoother"]
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti) + "}"

        log_wt0 = self.ensemble.logwt["analysis"][ti,:]
        log_wt1 = self.ensemble.logwt["smoother"][ti+1,:]
        x1 = self.propagate_nonoise(ti+1)

        m = choice(self.M, 1, p = exp(log_wt1))[0]
        x1_check = self.ensemble.ens["analysis"][:,m,ti+1]

        diff = x1_check[:,newaxis] - x1
        log_wt0 = norm_weight(log_wt0 + fm.Sigma.logpdf(diff.T))
        self.ensemble.logwt["smoother"][ti,:] = log_wt0
        self.calc_loglik_t(ti, key = "smoother", wt_key = "smoother")
        self.ensemble.calc_stats("smoother", ti, log_wt0)

        self.ti["smoother"] -= 1
