
from numpy import in1d, mean, std, empty, uint16, ones, isnan, average, sqrt, logical_not
from scipy.io import savemat
from helper import *

class ensemble(object):
    # TODO: add functionality to init with netCDF file
    def __init__(self, N, T, M):
        self.N = N
        self.T = T
        self.M = M

        self.keys = ["forecast", "analysis"]
        self.ens = dict(forecast = None, analysis = None)
        self.mean = dict()
        self.sd = dict()
        self.reset()

    def reset(self):
        for key in self.keys:
            self.mean[key] = nones((self.T, self.N))
            self.sd[key] = nones((self.T, self.N))

class ensemble_sampler(ensemble):
    def __init__(self, *args):
        super(ensemble_sampler, self).__init__(*args)
        self.type = "sampler"
        self.init_logwt()

    def reset(self):
        super(ensemble_sampler, self).reset()
        for key in self.keys: self.ens[key] = nones((self.N, self.M, self.T), order="F")
        self.init_logwt()

    def init_smoother(self):
        if not in1d("smoother", self.keys): self.keys.append("smoother")

        self.mean["smoother"] = nones(self.mean[self.keys[0]].shape)
        self.sd["smoother"]   = nones(self.sd[self.keys[0]].shape)

        self.mean["smoother"][-1,:]  = self.mean["analysis"][-1,:]
        self.sd["smoother"][-1,:]    = self.sd["analysis"][-1,:]

    def init_logwt(self):
        self.logwt = \
            dict(forecast = -log(self.M)*ones((self.T, self.M)),
                 analysis = -log(self.M)*ones((self.T, self.M)))

    def init_params(self, ens, mean, sd):
        D = mean.size
        for key in ["forecast_param", "analysis_param"]:
            self.ens[key]   = nones((D, self.M, self.T), order="F")
            self.mean[key]  = nones((self.T, D))
            self.sd[key]    = nones((self.T, D))

            self.ens[key][:,:,0] = ens
            self.mean[key][0,:]  = mean
            self.sd[key][0,:]    = sd

    def calc_stats(self, key, ti, logwt = None, success = None):
        # FIXME: add functionality to change which weights to use
        if logwt is None: logwt = norm_weight(self.logwt[key.split("_")[0]][ti,:], log_out = True)
        if success is None: success = array([True] * self.M)
        wt = exp(logwt)
        self.mean[key][ti,:] = self.calc_mean(key, ti, logwt, wt, success)
        self.sd[key][ti,:]   = self.calc_sd(key, ti, logwt, wt, success)
        return self.mean[key][ti,:], self.sd[key][ti,:]

    def calc_mean(self, key, ti, logwt = None, wt = None, success = None):
        if wt is None:
            if logwt is None: logwt = norm_weight(self.logwt[key.split("_")[0]][ti,:], log_out = True)
            wt = exp(logwt)
        if success is None: success = array([True] * self.M)
        out = average(self.ens[key][:,success,ti], axis = 1, weights = wt[success])
        return out

    def calc_sd(self, key, ti, logwt = None, wt = None, success = None):
        if logwt is None and wt is None: # both not specified
            logwt = self.logwt[key.split("_")[0]][ti,:] # no need to normalize because var_weighted takes care of normalization
            wt = exp(logwt)
        if success is None: success = array([True] * self.M)

        var = var_weighted(self.ens[key][:,success,ti], logwt = logwt[success], wt = wt[success], mean = self.mean[key][ti,:], marginal = True)
        return sqrt(var)

class ensemble_enkf(ensemble_sampler):
    def __init__(self, *args):
        super(ensemble_enkf, self).__init__(*args)
        self.type = "enkf"

    def reset(self):
        super(ensemble_enkf, self).reset()
        if in1d("smoother", self.keys): self.init_smoother()

    def init_logwt(self):
        super(ensemble_enkf, self).init_logwt()
        self.logwt["forecast"] = self.logwt["analysis"]

    def init_smoother(self):
        super(ensemble_enkf, self).init_smoother()
        self.ens["smoother"]  = nones(self.ens[self.keys[0]].shape, order="F")
        self.ens["smoother"][:,:,-1] = self.ens["analysis"][:,:,-1]

    def savemat(self, filename, **kwargs):
        """ Save as MATLAB-style .mat file. """
        savemat(filename,
            {'ens': self.ens,
             'mean': self.mean,
             'sd': self.sd,
             'logwt': self.logwt},
            **kwargs)

class ensemble_pf(ensemble_sampler):
    def __init__(self, *args):
        super(ensemble_pf, self).__init__(*args)
        self.type = "pf"
        self.init_ancestry()

    def init_smoother(self):
        super(ensemble_pf, self).init_smoother()
        self.ens["smoother"] = self.ens["analysis"]
        self.logwt["smoother"] = nones((self.T, self.M))
        self.logwt["smoother"][-1,:] = self.logwt["analysis"][-1,:]

    def init_ancestry(self):
        self.ancestry = dict(filter = None)
        self.ancestry["filter"] = \
            dict(loglik = nones((self.T, self.M)),
                 parent = array([65535*ones(self.M) for x in range(self.T)], dtype = uint16),
                 resampled = empty(self.T, dtype = bool),
                 ess = nones(self.T))

    def reset(self):
        super(ensemble_pf, self).reset()
        self.init_ancestry()

    def forecast_analysis_same_ti(self, ti):
        self.ens["analysis"][:,:,ti] = self.ens["forecast"][:,:,ti]

    def forecast_analysis_same_all(self):
        self.ens["forecast"] = self.ens["analysis"]

    def get_ancestry(self, ti, m, key = "filter"):
        parent = empty(ti, dtype = uint16)
        parent[0] = m
        for x in range(1,ti):
            m = self.ancestry[key]["parent"][ti-x,m]
            parent[x] = m
        return parent[::-1]

    def ess(self, ti, logwt = None, key = "analysis"):
        """
        Calculates effective sample size: ESS = 1 / sum wt^2
        """
        if logwt is None: logwt = self.logwt[key][ti,:]

        log_sum_wt = sum_weight(logwt)
        log_ess = -sum_weight(2*logwt) + 2*log_sum_wt
        self.ancestry["filter"]["ess"][ti] = exp(log_ess)

        logwt = norm_weight(logwt, log_sum_wt = log_sum_wt)

        return self.ancestry["filter"]["ess"][ti], logwt

    def update_parent(self, key, ti, parent, resampled, logwt = None):
        self.ancestry["filter"]["parent"][ti,:] = parent
        self.ancestry["filter"]["resampled"][ti-1] = resampled
        if logwt is not None: self.logwt[key][ti,:] = logwt

    def get(self, type, key = None, ti = None, m = None, n = None):
        if type is "mean":
            if key is None: key = "forecast"
            out = self.mean[key][ti,n]
            if len(out.shape) == 4: out = out[0,0]
            return out

        elif type is "sd":
            if key is None: key = "forecast"
            out = self.sd[key][ti,n]
            if len(out.shape) == 4: out = out[0,0]
            return out

        elif type is "logwt":
            if key is None: key = "forecast"
            out = self.logwt[key][ti,m]
            if len(out.shape) == 4: out = out[0,0]
            return out

        elif type is "ess":
            if key is None: key = "filter"
            out = self.ancestry[key]["ess"][ti]
            if len(out.shape) == 2: out = out[0]
            return out

        elif type is "resampled":
            if key is None: key = "filter"
            out = self.ancestry[key]["resampled"][ti]
            if len(out.shape) == 2: out = out[0]
            return out

        elif type is "parent":
            if key is None: key = "filter"
            out = self.ancestry[key]["parent"][ti,m]
            if len(out.shape) == 4: out = out[0,0]
            return out

        else:
            return ValueError("Don't recognize that 'type'.")

    def savemat(self, filename, **kwargs):
        """ Save as MATLAB-style .mat file. """
        savemat(filename,
            {'ens': self.ens,
             'mean': self.mean,
             'sd': self.sd,
             'logwt': self.logwt,
             'ancestry': self.ancestry},
            **kwargs)

class ensemble_kf(ensemble):
    def __init__(self, *args):
        super(ensemble_kf, self).__init__(*args)
        self.type = "kf"
        self.init_Sigma()

    def init_Sigma(self):
        self.Sigma = dict()
        for key in self.keys: self.Sigma[key] = [ None ] * self.T
        self.Sigma["ti"] = None

    def reset(self):
        super(ensemble_kf, self).reset()
        self.init_Sigma()


