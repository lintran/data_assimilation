
from __future__ import division
from numpy import isnan, maximum, ceil, nan, nansum, vstack, dstack, nanmean, in1d, diag_indices_from, repeat, median, tile
from scipy.linalg import cho_solve, cho_factor, eigh
from scipy.stats import truncnorm
from scipy.io import savemat
from copy import deepcopy
from pf_param import *
from sys import stdout
from IPython import embed

class IteratedFilter(pf_param):
    def __init__(self, *args, **kwargs):
        """
        Parameters
        ----------
        stateforecast_prev : boolean
            [x_t | x_t-1, theta] is replaced with [x_t | x_t-1, theta_t-1] if True, else [x_t | x_t-1, theta_t]
        update_type : string
            How to update parameters when using EnKF for filtering. Valid choices:
                "pf"        = particle filter update
                "original"  = original enkf
                "perturbed" = enkf with perturbed observations
                "etkf"      = deterministic square-root filters
        tune_cov_eps : scalar
            Tune parameter covariance matrix ala Lindstrom (2013) "Tuned Iterated Filtering". Can be between 0 and 1, where 1 indicates no tuning. Paper suggested 1e-2.
        """
        self.stateforecast_prev = kwargs.pop("stateforecast_prev", False)
        self.update_type = kwargs.pop("update_type", None)
        self.resample_type = kwargs.pop("resample_type", None)
        self.tune_cov_eps = kwargs.pop("tune_cov_eps", 1)
        #self.b_mle = kwargs.pop("b_mle", 5)
        super(IteratedFilter, self).__init__(*args, **kwargs)

        if self.reset_ics is False:
            logging.warning("'reset_ics' cannot be False for 'IteratedFilter' class; changed to True.")
            self.reset_ics = True

        if self.pf.filter_type is "enkf":
            enkf_update_type = ["original", "perturbed", "etkf"]
            pf_update_type   = ["apf", "pf"]
            if not in1d(self.update_type, enkf_update_type + pf_update_type): raise KeyError("Don't recognize that `update_type`. If didn't specify `update_type`, must specify one since using EnKF.")
            if in1d(self.update_type, pf_update_type):
                if self.resample_type is None: raise KeyError("Since you choose to filter with EnKF and update parameters with PF, must specify `resample_type`.")
                self.pf.resample_type = self.resample_type
            if self.update_type == "apf":
                e = self.pf.ensemble
                e.ens["forecast_old"]  = nones((e.N, e.M, e.T), order="F")
                e.mean["forecast_old"] = nones((e.T, e.N))
                e.sd["forecast_old"]   = nones((e.T, e.N))

    def init_params(self, *args, **kwargs):
        super(IteratedFilter, self).init_params(*args, **kwargs)

        # initialize list of parameter dictionaries
        M = self.pf.M
        parms = self.pf.hmm.forward_model.parms

        # TODO: make following line faster by copying only elements that are needed for parameter estimation
        self.state_parms_dict = [deepcopy(parms) for m in range(M)]

        if self.param_mean_analysis is not None:
            for m in xrange(M): self.state_parms_dict[m][self.param_mean_analysis] = self.pf.hmm.forward_model.parms[self.param_mean_analysis]
        if self.param_mean_diff is not None:
            self.pf.hmm.forward_model.parms[self.param_mean_diff] = zeros(self.pf.hmm.forward_model.parms["N"])
            for m in xrange(M): self.state_parms_dict[m][self.param_mean_diff] = self.pf.hmm.forward_model.parms[self.param_mean_diff]

    def init_params_more(self, mean, ens):
        sd = sqrt(self.param_cov.diagonal()) # doesn't matter
        self.pf.ensemble.init_params(ens, mean, sd)

    def init_cov(self, *args, **kwargs):
        if self.param_sd.size != self.D: raise KeyError("Length of 'param_sd' does not match the sizes of the parameters.")

        self.param_sd_trans = self.param_sd.copy()
        # for idx in self.idx_param_ulbdd:
        #     self.param_sd_trans[idx] = \
        #         logit2(self.param_sd_trans[idx], self.param_min[idx], self.param_max[idx])
        self.param_var_trans = self.param_sd_trans**2

        self.param_cov = diag(self.param_var_trans)
        self.param_cov_chol = diag(self.param_sd_trans)
        self.param_cov_diag = True
        #if self.tune_cov_eps < 1: self.fisher_info = self.param_cov

    def sample(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def print_progress(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def run_filter(self, calc_Vt):
        # copied from 'hmm_sampler' and 'hmm_getstate' classes
        if not isnan(self.pf.hmm.y[0,:]).all(): self.pf.update_ti0(False)
        for t in xrange(1, self.pf.T):
            self.filter(calc_Vt)
            if self.pf.filter_error:
                logging.warning("Filter error! Stopped at ti = %i. Error msg: %s" % (self.pf.ti["filter"], self.pf.filter_error_msg))
                break

        # resampling is not done at time T, so do it now:
        # NOTE: the following only resamples the parameters and not the states. does it matter?
        ti = self.pf.T-1
        self.pf.mif_resample_t(ti)

    def filter(self, calc_Vt):
        """
        Parameters
        ----------
        calc_Vt : boolean
            Calculate the parameter's forecast covariance?
        """
        # skeleton copied from 'pf' class
        pf = self.pf
        fm = pf.hmm.forward_model
        ti = pf.ti["filter"]

        # resample
        logwt0, parms0 = pf.mif_resample_t(ti-1)
        if ti == 1 and isnan(logwt0).any(): logwt0 = -log(pf.M)*ones(pf.M)

        # forecast
        m = None
        for i in range(20): # will move on to updating when at least one of the forecasted states/parameters are good
            # forecast parameters
            parms, idx_obs_parms = self.forecast_param(parms0, logwt0, calc_Vt)

            # forecast hidden states
            # print pf.ensemble.logwt["forecast"][ti,:].min(), pf.ensemble.logwt["forecast"][ti,:].max()
            logwt1, m = pf.mif_forecast_t(ti, None, logwt0, self.state_parms_dict, self.update_type, parms_vec = parms, idx_inflation = self.idx_inflation, idx_halfwidth = self.idx_halfwidth, verbose = self.filter_verbose)
            # print pf.ensemble.logwt["forecast"][ti,:].min(), pf.ensemble.logwt["forecast"][ti,:].max()
            # if m is None: print parms1.min(), parms1.max()
            if m is not None: break
        if m is None: raise Exception("Tried %i times to forecast state/parameters to no success." % (i+1))

        # update
        logwt1 = pf.mif_update_t(ti, m, logwt0, parms, self.update_type, idx_obs_parms = idx_obs_parms)

        if self.filter_verbose:
            d = self.D_print
            print_me = "%i-%i param: " % (self.b, ti)
            print_me += reduce(lambda x,y: x+y, ["%.05f " % x for x in median(pf.ensemble.ens["analysis_param"][:d,:,ti], axis=1)])
            print print_me
            stdout.flush()

        pf.ti["filter"] += 1

    def forecast_param(self, parms0, logwt0, calc_Vt):
        pf = self.pf
        fm = pf.hmm.forward_model
        ti = pf.ti["filter"]

        # forecast parameters
        parms1 = self.sample_param(parms0)
        pf.ensemble.ens["forecast_param"][:,:,ti] = parms1
        if calc_Vt: self.Vt_chol[ti] = cho_factor(var_weighted(parms1, logwt0, marginal = False))
        if self.param_mean_analysis is not None:
            n = self.param_mean_analysis_n
            mean = fm.parms[self.param_mean_analysis]
            mean = 1/(n+1) * ( n*mean + pf.ensemble.mean["analysis"][ti-1,:] )
            fm.parms[self.param_mean_analysis][:] = mean
            self.param_mean_analysis_n += 1
        if self.param_mean_diff is not None and ti > 1:
            n = self.param_mean_diff_n
            mean = fm.parms[self.param_mean_diff]
            a = pf.ensemble.mean["analysis"][ti-1,:]
            f = pf.ensemble.mean["forecast"][ti-1,:]
            dt = pf.hmm.ty[ti-1] - pf.hmm.ty[ti-2]
            mean = 1/(n+1) * ( n*mean + (a-f)/dt )
            fm.parms[self.param_mean_diff][:] = mean
            self.param_mean_diff_n += 1

        # choose parameters to forecast hidden states
        parms = parms0 if self.stateforecast_prev else parms1
        goodparms = logical_or(parms >= self.param_min[:,None],
                               parms <= self.param_max[:,None]).any(axis=0)
        badparms = logical_not(goodparms)
        pf.ensemble.logwt["forecast"][ti,:] = logwt0
        pf.ensemble.logwt["forecast"][ti,badparms] = -inf
        for m in where(goodparms)[0]: self.state_parms_dict[m] = self.param_vec_to_dict_state(parms[:,m], self.state_parms_dict[m])
        idx_obs_parms = None if self.D_dict["obs"] == 0 else self.param_start["obs"][0]

        pf.ensemble.calc_stats("forecast_param", ti)

        return parms, idx_obs_parms

    def run(self):
        for x in xrange(self.b, self.B+1):
            self.sample()
            if self.pf.filter_error: break

    def sample_param(self, mean):
        return self.sample_param_truncnorm(mean)
        #return self.sample_param_norm(mean)

    def sample_param_truncnorm(self, mean):
        if self.param_cov_diag: return self.sample_param_truncnorm_uncorrelated(mean)
        else:                   return self.sample_param_truncnorm_correlated(mean)

    def sample_param_truncnorm_correlated(self, mean):
        D,M = mean.shape
        out = mean.copy() # DxM
        reject = array([self.param_bdd.any()] * M)
        n_reject = reject.sum()

        i = 0
        while n_reject > 0:
            out[:,reject] = mean[:,reject]
            add = normal.rvs(size = (D,n_reject))
            out[:,reject] += dot(self.param_cov_chol, add)
            if self.param_int.any():
                idx_reject = where(reject)[0]
                idx = (repeat(self.idx_param_int, reject.sum()), tile(idx_reject, self.idx_param_int.size))
                out[idx] = out[idx].round()

            reject = logical_or(out[self.param_bdd,:] < self.param_min[:,None],
                                out[self.param_bdd,:] > self.param_max[:,None])
            reject = reject.any(axis = 0)
            n_reject = reject.sum()

            i += 1
            if i > 1000: raise Exception("Tried rejection sampling from truncated normal %i times to no success." % i)

        return out

    def sample_param_truncnorm_uncorrelated(self, mean):
        out = empty(mean.shape) # DxM
        D = mean.shape[0]
        M = mean.shape[1]
        sd = self.param_cov_chol.diagonal()

        if self.param_real.any():
            add = normal().rvs(size = (self.param_real.sum(), M))
            add *= sd[self.idx_param_real][:,None]
            out[self.idx_param_real,:] = mean[self.idx_param_real,:] + add
        if self.param_bdd.any():
            for d in self.idx_param_bdd:
                a = (self.param_min[d] - mean[d,:]) / sd[d]
                b = (self.param_max[d] - mean[d,:]) / sd[d]
                for m in range(M): out[d,m] = truncnorm.rvs(a[m], b[m], loc = mean[d,m], scale = sd[d])
                out[d,out[d,:] == inf] = self.param_max[d]
                out[d,out[d,:] == -inf] = self.param_min[d]

        if self.param_int.any(): out[self.idx_param_int,:] = out[self.idx_param_int,:].round()

        return out

    def sample_param_norm(self, mean):
        out = self.param_transform_real(mean.copy()) # DxM
        add = normal.rvs(size = out.shape)
        out += dot(self.param_cov_chol, add)
        self.param_transform_inv(out)
        return out

    def param_transform_real(self, params):
        # params, array with shape (D,M)
        # constrained on both sides
        for idx in self.idx_param_ulbdd:
            params[idx,:] = \
                logit2(params[idx,:], self.param_min[idx], self.param_max[idx])

        # constrained on one side
        for idx in self.idx_param_lbdd:
            params[idx,:] = log(params[idx,:] - self.param_min[idx])

        for idx in self.idx_param_ubdd:
            params[idx,:] = log(self.param_max[idx] - params[idx,:])

        return params

    def param_transform_inv(self, params):
        # params, array with shape (D,M)
        # constrained on both sides
        for idx in self.idx_param_ulbdd:
            params[idx,:] = \
                invlogit2(params[idx,:], self.param_min[idx], self.param_max[idx])

        # constrained on one side
        for idx in self.idx_param_lbdd:
            params[idx,:] = exp(params[idx,:]) + self.param_min[idx]

        for idx in self.idx_param_ubdd:
            params[idx,:] = self.param_max[idx] - exp(params[idx,:])

        # integer
        for idx in self.idx_param_int:
            params[idx,:] = params[idx,:].round()

        return params

    def tune_covariance(self, sigma_sq, tau_sq = 1):
        pf = self.pf
        T  = pf.T
        pt = self.pf.ensemble.mean["analysis_param"] # same pt as in calc_mle method
        # pt = self.param_transform_real(pt.copy().T).T

        diff       = pt[1:T,:] - pt[0:(T-1),:]
        diff      /= sqrt(sigma_sq)
        diff[0,:] *= sqrt(sigma_sq / (sigma_sq + tau_sq))

        Sigma_b1  = array([tcrossprod(diff[i,:][:,None]) for i in range(T-1)]).sum(axis=0)
        Sigma_b1 /= (T-1)
        Sigma_b1_2norm = eigh(Sigma_b1, eigvals_only = True, eigvals = (self.D-1, self.D-1))
        Sigma_0_2norm  = self.param_var_trans.max()
        Sigma_b2 = Sigma_b1 * Sigma_0_2norm / Sigma_b1_2norm

        out = (1-self.tune_cov_eps)*Sigma_b2
        out[diag_indices_from(out)] += self.tune_cov_eps*self.param_var_trans

        return out

    def tune_covariance2(self, sigma_sq, tau_sq = 1):
        pf = self.pf
        T  = pf.T
        pt = self.pf.ensemble.mean["analysis_param"] # same pt as in calc_mle method
        diff = pt[1:T,:] - pt[0:(T-1),:]

        if self.b > self.b_mle:
            addme = array([cho_solve(self.Vt_chol[i+1], tcrossprod(diff[i,:][:,None])) for i in range(T-1)])
            addme = symmetrize(dot(self.param_cov, addme.sum(axis=0)))
            self.fisher_info = (self.fisher_info + addme)/2

        return self.fisher_info

    def savemat(self, filename, **kwargs):
        """ Save as MATLAB-style .mat file. """
        out_add = kwargs.pop("dict_add", dict())
        out = {'records': self.records,
               'param_names': self.param_names,
               'param_min': self.param_min,
               'param_max': self.param_max,
               'param_sd': self.param_sd}
        out.update(out_add)
        savemat(filename, out, **kwargs)

class IteratedFilter1(IteratedFilter):
    def __init__(self, *args, **kwargs):
        self.delta = kwargs.pop("delta")
        self.b_mle = kwargs.pop("b_mle", 5)
        self.M_max = kwargs.pop("M_max", inf)
        super(IteratedFilter1, self).__init__(*args, **kwargs)

    def sample(self):
        # initialize constants
        b   = self.b
        b2  = 1+1/b
        tausq_multiplier = 1/b2              # tau_{b+1}^2 = (1+1/b)^-1 tau_b^2 = b^-1
        csq_multiplier   = b2**(-self.delta) #   c_{b+1}^2 = (1+1/b)^-delta c_b^2 = b^-delta
        # sigma^2 = c^2*tau^2

        M = max(self.M_min, ceil(b**(1/2.+self.delta)))
        M = min(self.M_max, M)
        M = int(M)

        # reset filter
        self.reset_filter(M = M)

        # initialize parameters
        self.param_cov *= tausq_multiplier
        self.param_cov_chol *= sqrt(tausq_multiplier)
        mean = self.records["param"][b-1,:]
        ens = self.sample_param(repeat(mean[:,None], M, axis = 1))
        self.init_params_more(mean, ens)

        # prepare parameter covariance
        self.param_cov *= csq_multiplier
        self.param_cov_chol *= sqrt(csq_multiplier)
        self.Vt_chol = [None] * self.pf.T

        # sample parameters (and hidden states)
        self.run_filter(calc_Vt = b > self.b_mle)
        self.records["loglik"][b-1] = self.loglik()

        # update MLE
        self.update_mle()
        self.print_progress()

        # tune covariance matrix
        if self.tune_cov_eps < 1:
            tau_sq   = 1/b
            c_sq     = b**(-self.delta)
            sigma_sq = c_sq*tau_sq

            out = self.tune_covariance(sigma_sq, tau_sq)
            self.param_cov = sigma_sq * out
            self.param_cov_chol = sqrt(sigma_sq) * cholesky(out)
            self.param_cov_diag = False

        self.b += 1

    def update_mle(self):
        T = self.pf.T
        b = self.b

        pt = self.pf.ensemble.mean["analysis_param"]

        if b <= self.b_mle:
            # per recommendation of Ionides et al (2006), see equation 17
            mle = pt.mean(axis=0) # FIXME: check
        else:
            diff = pt[1:T,:] - pt[0:(T-1),:]
            addme = array([cho_solve(self.Vt_chol[i+1], diff[i,:]) for i in range(T-1)])
            addme = dot(self.param_cov, addme.T).sum(axis=1) # Vt0 = self.param_cov

            # following code to test one parameter
            # self.Vt = vstack(self.Vt)
            # addme = diff[1:(T-1)]/self.Vt[1:(T-1)]
            # addme = (self.Vt[1]*addme).sum()
            # print self.Vt[1] * ((1/self.Vt[1:(T-1)] - 1/self.Vt[2:]).sum() + 1/self.Vt[-1]) # should equal 1

            mle = self.records["param"][b-1,:] + addme

        self.records["param"][b,:] = mle

    def print_progress(self):
        b = self.b-1
        d = self.D_print
        print_me = "%i-done mle: " % b
        print_me += reduce(lambda x,y: x+y, ["%.2f " % x for x in self.records["param"][b,:d]])
        print_me += "loglik: %.2f" % self.records["loglik"][b]
        print print_me

class IteratedFilter2(IteratedFilter):
    def __init__(self, *args, **kwargs):
        self.alpha = kwargs.pop("alpha", .95)
        super(IteratedFilter2, self).__init__(*args, **kwargs)
        init = self.records["param"][0,:]
        self.records["param"] = nones((self.B+1, init.size, self.pf.M))
        self.records["param"][0,:,:] = init[:,None]
        self.records["loglik"] = nones(self.B+1)

    def sample(self):
        # initialize constants
        b = self.b

        # initialize parameters
        if b > 1:
            self.param_cov *= self.alpha
            self.param_cov_chol *= sqrt(self.alpha)
            # if self.param_inflate is not None:
            #     self.param_cov[self.idx_inflation] /= self.alpha
            #     self.param_cov_chol[self.idx_inflation] /= sqrt(self.alpha)
        ens = self.sample_param(self.records["param"][b-1,:,:])
        mean = ens.mean(axis=1)
        self.reset_filter()
        self.init_params_more(mean, ens)
        # if self.tune_cov_eps < 1: self.Vt_chol = [None] * self.pf.T

        # sample parameters (and hidden states)
        # self.run_filter(calc_Vt = self.tune_cov_eps < 1)
        self.run_filter(calc_Vt = False)
        self.records["loglik"][b] = self.loglik()
        self.records["param"][b,:,:] = self.pf.ensemble.ens["analysis_param"][:,:,-1]
        self.print_progress()

        # tune covariance matrix
        if self.tune_cov_eps < 1:
            sigma_sq = self.alpha**(b-1)
            out = self.tune_covariance(sigma_sq)
            self.param_cov = sigma_sq * out
            self.param_cov_chol = sqrt(sigma_sq) * cholesky(out)
            self.param_cov_diag = False
            print self.param_cov

        self.b += 1

    def print_progress(self):
        b = self.b
        d = self.D_print
        print_me = "%i-done bayes map: " % b
        print_me += reduce(lambda x,y: x+y, ["%.5f " % x for x in median(self.records["param"][b,:d,:], axis=1)])
        print_me += "loglik: %.2f" % self.records["loglik"][b]
        print print_me

    def trace(self, param_name, burnin = 0):
        idx = where(self.param_names == param_name)[0][0]
        start = self.param_start[idx]
        end = self.param_end[idx]
        plt.plot(self.records["param"][:,idx,burnin:])

    def hist(self, param_name, burnin = 0):
        pass
