from pf import *

class bf(pf):
    def __init__(self, *args, **kwargs):
        self.filter_type = "bf"
        super(bf, self).__init__(*args, **kwargs)
        if self.ess_threshold is None: self.ess_threshold = self.M/2.
        if self.pre_reg_type is not None:
            logging.warning("Pre-regularization has not been implemented for bootstrap filter. 'pre_reg_type' will be ignored.")
            self.pre_reg_type = None

    def init_reg(self):
        # regularization?
        if self.post_reg_type is None:
            self.ensemble.forecast_analysis_same_all()
        else:
            self.calc_reg_opt()

    def resample_t(self, ti):
        log_wt0, yes = self.resample_or_not(ti)
        m, log_wt0 = self.resample(log_wt0, yes)
        if ti != self.T-1: self.ensemble.update_parent("forecast", ti+1, m, yes, log_wt0)
        return m, log_wt0

    def forecast_t(self, ti, m, log_wt0, parms = None):
        self.propagate(ti, m, logwt = log_wt0, parms = parms)
        return log_wt0

    def update_t(self, ti, log_wt0):
        return self.update(ti, log_wt0, x = self.perturb(ti, "forecast") if self.perturbation_factor != 0 else None)


