
from numpy import eye, in1d, array, dot, diag, asarray, where, nansum, newaxis, sqrt, cov, allclose, tril, ones, diff, append, logical_and
from numpy import interp, arange
from numpy.random import choice, rand
from numpy.random import seed as set_seed
from numpy.linalg import cholesky, slogdet, svd

from scipy import nanmean
from scipy.stats import multivariate_normal as mvnorm
from scipy.stats import norm as normal
from scipy.io import savemat

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from multiprocessing import Pool
from copy import deepcopy

from helper import *
from ensemble import *
from compact_fns import *

import logging

from IPython import embed

class hmm(object):
    def __init__(self, forward_model, measurement_model,
                 x0, ty, tx = None,
                 x = None, y = None,
                 seed_x = None, seed_y = None):
        """
        Initializes the hidden Markov model (HMM). x is the state space and y is the observed state.

        Parameters
        ----------
        forward_model : forward_model
            Advances the state space with initial conditions, x0, to the times in t. See class definition for more information.
        measurement_model : measurement_model
            Advances the state space to observation model. See class definition for more information.
        x0 : array
            Initial conditions, x0.
        ty : array
            A sequence of time points at which y is observed.
        tx : array, optional
            A sequence of time points at which x is observed. If None, will be set to be equal to ty.
        x : array, optional, shape (len(tx), len(x0))
            Unobserved hidden state. Look at `y` for more info.
        y : array, optional, shape (len(ty), len(x0))
            Observed values of the state space. If `x` and `y` are both `None`, `forward_model` will be used to integrate the state space forward with initial conditions `x0` and observed values will be generated with the output and `measurement_model`. If `x` is specified and `y` is not, observed values will be generated with the specified `x` and `measurement_model`. If `y` is specified and `x` is not, observed values will take on `y` and `x` will be `None`.
        seed_x and seed_y: int or array_like, optional
            Set the seed of the generation of `x` and `y`, respectively.
        """
        self.forward_model = forward_model
        self.measurement_model = measurement_model

        self.x0 = x0
        self.N = x0.size

        self.x = x
        self.y = y
        self.x_mean = None

        self.seed_x = seed_x
        self.seed_y = seed_y

        self.ty = ty
        self.T = ty.size

        # sequence of time points for state space
        if tx is None:
            self.tx = ty
            self.tmatch = xrange(self.T)
        else:
            if tx.size < ty.size: raise ValueError("Length of tx is less than ty.")
            self.tx = tx
            self.tmatch = where(in1d(tx.round(10), ty.round(10)))[0] # TEST: why do we need to round for values to match up?

        self.init_stateobs()

    def init_stateobs(self):
        # inits true state and observed state
        if self.x is None and self.y is None: # neither x nor y are specified
            self.x, self.x_mean = self.forward_model.advance(self.x0, self.tx, seed = self.seed_x)
            self.y = self.measurement_model.obs(self.x[self.tmatch,:], seed = self.seed_y)
            self.y[0,:] = None

        elif self.x is not None and self.y is None: # only x is specified
            self.y = self.measurement_model.obs(self.x[self.tmatch,:], seed = self.seed_y)
            self.y[0,:] = None

        elif self.x is None and self.y is not None: # only y is specified
            self.x = None

    def plot1d(self, ni, t_start = None, t_end = None):
        n_len = len(ni)

        if t_start is None: t_start = self.ty.min()
        if t_end is None: t_end = self.ty.max()

        # TODO: do this better
        txi = range(where(self.tx - t_start == 0)[0][0],
                    where(self.tx - t_end == 0)[0][0] + 1)
        tyi = range(where(self.ty - t_start == 0)[0][0],
                    where(self.ty - t_end == 0)[0][0] + 1)

        fig, subfig = plt.subplots(n_len, 1, sharex=True)
        plt.subplots_adjust(hspace=0.05)

        if n_len == 1: subfig = array((subfig,))

        for n in xrange(n_len):
            # true x
            subfig[n].plot(self.tx[txi], self.x[txi,ni[n]], c="DarkGreen", zorder = 2)
            if self.x_mean is not None and self.forward_model.Sigma is not None:
                mean = self.x_mean[txi,ni[n]]
                sd = self.forward_model.Sigma.sigma[ni[n]]
                lower = mean - 1.96*sd
                upper = mean + 1.96*sd
                subfig[n].fill_between(self.tx[txi], lower, upper, facecolor="Green", alpha=0.5, edgecolor=None, linewidth=0, zorder = 2)

            # y
            obs = self.measurement_model.H.sum(axis=0)
            if obs[n] == 1:
                idx = obs[:ni[n]].sum()
                subfig[n].scatter(self.ty[tyi], self.y[tyi,idx], s=1, zorder=3)

            # other
            rng = [self.ty[tyi].min(), self.ty[tyi].max()]
            subfig[n].set_xlim(rng)

        if n_len == 1: subfig = subfig[0]

        return fig, subfig

    def plot3d(self, ti, plot_x = True, plot_y = True):
        if self.N == 3:
            # set up plot
            fig = plt.figure()
            subfig = fig.add_subplot(111, projection='3d')

            # plot stuff
            if self.x is not None: subfig.plot(self.x[:,0], self.x[:,1], self.x[:,2], c='LightGrey')
            if plot_x:
                x = self.x[self.tmatch[ti],:]
                subfig.text(x[0], x[1], x[2], "x")
            if plot_y: subfig.text(self.y[ti,0], self.y[ti,1], self.y[ti,2], "y")

            return fig, subfig
        else:
            raise ValueError("This function can only deal with a 3d state space (N = 3).")

    def savemat(self, filename):
        """ Save as MATLAB-style .mat file. """
        savemat(filename,
            {'x': self.x, 'y': self.y,
            'x_mean': self.x_mean, 'x0': self.x0,
            'tx': self.tx, 'ty': self.ty, 'tmatch': self.tmatch,
            'H': self.measurement_model.H,
            'Sigma_y_chol': self.measurement_model.Sigma.Sigma_chol,
            'parms': self.forward_model.parms})

class forward_model(object):
    """
        Advances state space.

        Parameters
        ----------
        deriv : callable(x, t, parms)
            Computes the derivative of x at t.
        adv_fn : callable(x0, t, parms, deriv)
            Advances the state space with initial conditions, x0, to the times in t. Basically an integrator for 'deriv'.
        parms : dict
            Minimal amount of parameters of `adv_fn` model. See 'fparms' for what I mean by "minimal".
        fparms : callable(parms)
            Uses 'parms' to precompute other parameters used in 'deriv'.
        """
    def __init__(self, adv_fn, parms,
                 deriv = None, grad = None, hess_fn = None,
                 fparms = None,
                 Sigma = None, adv_fn_vectorized = False,
                 Sigma_dt = False, n_cpus = None):
        # TODO: generalize for noise model to be arbitrary (not just normal)
        self.adv_fn = adv_fn
        self.adv_fn_vectorized = adv_fn_vectorized

        self.parms = fparms(parms) if fparms is not None else parms
        self.fparms = fparms if fparms is not None else self.fparms_none

        self.deriv = deriv
        self.grad = grad
        self.hess_fn = hess_fn

        self.Sigma = Sigma
        self.Sigma_dt = Sigma_dt

        self.n_cpus = n_cpus
        self.parallel = n_cpus is not None
        self.pool = Pool(self.n_cpus) if self.parallel else None

    def fparms_none(self, parms):
        return parms

    def advance(self, x0, t, seed = None, no_noise = False):
        """
        Advances the state space for many time steps. This function is most likely only to be used to advance the perfect model. Keep in mind:
            - Return statement is different when t is of size 2 vs size >2.
            - This is different from the 'multi_advance' in the sense that it does not allow for multiple parameters.

        Parameters
        ----------
        x0 : array
            Initial conditions of size N
        t : array
            A sequence of time points for which to solve for x. The first value is the time of the initial conditions.

        Returns
        -------
        array, shape (len(t), len(x0)) or (1, len(x0))
            Array containing the value of x for each desired time in t. The first row will contain the initial value x0 in the first row ONLY if len(t) > 2.
        """
        if t.size <= 1: raise ValueError("'t' must have size of at least 2, where the first time is the initial conditions.")
        t = array(t)

        if self.Sigma is None or no_noise: # deterministic state transitions
            x, success = self.adv_fn(x0, t, self.parms, self.deriv, self.grad)
            if t.size == 2: x = x[1,:]
            mean = x
        else: # random state transitions
            if t.size == 2:
                dt = diff(t) if self.Sigma_dt else 1.
                out = self.adv_fn(x0, t, self.parms, self.deriv, self.grad)
                mean, success = out[0][1,:], out[1]
                x = mean + self.Sigma.rvs(mean.shape, sd_multiply = dt, seed = seed)
            else:
                mean = array([x0] * t.size)
                x = mean.copy()
                seeds = rand_seed(t.size, seed = seed)
                n = x0.shape

                for ti in xrange(1, t.size):
                    dt = diff(t[ti-1:ti+1]) if self.Sigma_dt else 1.
                    out = self.adv_fn(x[ti-1], t[ti-1:ti+1], self.parms, self.deriv, self.grad)
                    mean[ti,:], success = out[0][1,:], out[1]
                    x[ti,:] = mean[ti] + self.Sigma.rvs(n, sd_multiply = dt, seed = seeds[ti])

        return x, mean

    def multi_advance(self, x0, t, no_noise = False, parms = None):
        """
        Advances the state space for only one time step, but for multiple initial conditions x0. Unlike the 'advance' method, each set of initial conditions can have its own parms, as noted by the keyword argument 'parms'.

        Parameters
        ----------
        x0 : array, shape (N, M)
            M different initial conditions of size N
        t : array
            A sequence of time points of size 2 for which to solve for x. The first value is the time of the initial conditions.

        Returns
        -------
        array, shape (N, M)
            Array of the integrated model.
        """
        # deal with t
        if t.size != 2: raise ValueError("Function only works for stepping forward once, i.e. t of size 2.")
        t = array(t)

        # advance the mean of each particle
        if self.adv_fn_vectorized and parms is None:
            x, success = self.multi_advance_vect(x0, t, self.parms)
        else:
            M = x0.shape[1]
            if parms is None:     parms = [self.parms for m in range(M)]
            if not self.parallel: x, success = self.multi_advance_loop(x0, t, parms)
            else:                 x, success = self.multi_advance_looppar(x0, t, parms)

        # add in noise
        if not (self.Sigma is None or no_noise):
            dt = diff(t) if self.Sigma_dt else 1.
            x += self.Sigma.rvs(x.shape, sd_multiply = dt)

        return (x, success)

    def multi_advance_vect(self, x0, t, parms = None):
        out = self.adv_fn(x0, t, parms, self.deriv, self.grad)
        x, success = out[0][1,:], out[1]
        return (x, success)

    def multi_advance_loop(self, x0, t, parms = None):
        M = x0.shape[1]
        x = nones(x0.shape)
        success = array([True] * M)
        for m in xrange(M):
            out = self.adv_fn(x0[:,m], t, parms[m], self.deriv, self.grad)
            x[:,m], success[m] = out[0][1,:], out[1]
        return (x, success)

    def multi_advance_looppar(self, x0, t, parms = None):
        M = x0.shape[1]

        processes = [self.pool.apply_async(self.adv_fn, (x0[:,m], t, parms[m], self.deriv, self.grad)) for m in range(M)]
        results = [p.get() for p in processes]

        x = [r[0] for r in results]
        x = array(x)[:,1,:].T
        success = array([r[1] for r in results])
        return (x, success)

    def tlm(self, x, dt):
        jac = self.grad(x, t, self.parms)
        M = dt * jac
        fill_diagonal(M, M.diagonal() + 1.0)
        return M

    def hess(self, f, x, t):
        return self.hess_fn(f, x, t, self.parms)

    def param_dict_to_vec(self, param_dict, param_names, param_start, param_end):
        out = nones(param_end[-1]) # D
        for i in range(param_names.size):
            start = param_start[i]
            end = param_end[i]
            key = param_names[i]
            out[start:end] = param_dict[key]
        return out

    def param_vec_to_dict(self, param_vec, param_names, param_start, param_end, param_dict = None, apply_fparms = True):
        if param_dict is None: param_dict = deepcopy(self.parms)
        for i in range(param_names.size):
            start = param_start[i]
            end = param_end[i]
            key = param_names[i]
            param_dict[key] = param_vec[start:end]
            if end - start == 1: param_dict[key] = param_dict[key][0]
        if apply_fparms: param_dict = self.fparms(param_dict)
        return param_dict

    def multi_param_vec_to_dict(self, param_vec, param_names, param_start, param_end, param_dict = None):
        M = param_vec.shape[1]
        if param_dict is None: param_dict = [None] * M
        out = [self.param_vec_to_dict(param_vec[:,m], param_names, param_start, param_end, param_dict[m], not self.parallel) for m in range(M)]

        if self.parallel:
            processes = [self.pool.apply_async(self.fparms, (out[m])) for m in range(M)]
            out = [p.get() for p in processes]

        return out

    def __del__(self):
        if self.pool is not None: self.pool.close()

class measurement_model(object):
    """
        y_t = H h(x_t) + e_t, where e_t ~ Normal(0, Sigma)

        Parameters
        ----------
        H : array, optional
            Linear map from state space to observation space. If None, will be identity matrix.
    """

    def __init__(self, Sigma, H = None, loc_x = None, loc_y = None, halfwidth = None, num_points = None):
        self.Sigma = Sigma
        self.H = H
        self.loc_x = loc_x
        self.loc_y = loc_y
        self.halfwidth = halfwidth
        self.num_points = num_points

        self.identity = False
        if H is None:
            if loc_x is None:
                self.H = eye(self.Sigma.N)
                self.identity = True
            elif not (loc_x is None and loc_y is None and halfwidth is None and num_points is not None):
                self.H = self.construct_weighting_matrix()
        self.linear = True

    def obs(self, x, seed = None):
        y = dot(self.H, x.T).T # TODO: generalize H to change with time
        if len(y.shape) == 1: y = y.reshape((y.size,1))
        y += self.Sigma.rvs(y.shape, seed = seed)
        return y

    def construct_weighting_matrix_prelims(self, halfwidth = None):
        if halfwidth is None: halfwidth = self.halfwidth

        # code taken from obs_def/obs_def_1d_state_mod.f90 > get_expected_1d_integral
        Nx = self.loc_x.size
        Ny = self.loc_y.size

        # Figure out the total range of the integrated funtion (1 is max)
        rng = 4.*halfwidth
        if rng > 1: rng = 1

        # Compute the bottom and top of the range
        bottom = self.loc_y - rng / 2
        bottom[bottom < 0] += 1

        # Next figure out where to put all the points for interpolation
        dg = rng / (self.num_points - 1)
        vec_np = arange(self.num_points)
        g = array([b + vec_np*dg for b in bottom])
        g[g > 1] -= 1

        # compute distance between locations and interpolating points
        dist = abs(self.loc_y[:,None] - g)
        dist[dist > .5] = 1 - dist[dist > .5]
        gp_dist = gasparicohn(dist, halfwidth)

        # taken from lorenz_04/model_model.f90 > model_interpolate
        lctn = g*Nx
        lower_index = lctn.astype(int)
        upper_index = lower_index + 1
        lower_index[lower_index > Nx-1] -= Nx
        upper_index[upper_index > Nx-1] -= Nx
        lctnfrac = lctn - floor(lctn)
        #obs_val = (1.0 - lctnfrac) * (lower_index == 1) + lctnfrac * (upper_index == 1)

        return (Nx, Ny, lower_index, upper_index, lctnfrac * gp_dist, gp_dist)

    def construct_weighting_matrix(self, halfwidth = None):
        Nx, Ny, lower_index, upper_index, LtG, gp_dist = self.construct_weighting_matrix_prelims(halfwidth)

        # calculate H matrix
        # H = ones((Ny, Nx))
        # for n in range(Nx):
        #     wt1 = (1.0 - lctnfrac) * (lower_index == n) + \
        #                 (lctnfrac) * (upper_index == n)
        #     wt2 = wt1 * gp_dist
        #     H[:,n] = wt2.sum(axis=1)
        x = diag(ones(Nx))
        sh = list(LtG.shape)
        sh.append(x.shape[1])
        add = (gp_dist - LtG)[:,:,None] * x[lower_index.reshape(-1),:].reshape(sh) + \
                          LtG[:,:,None] * x[upper_index.reshape(-1),:].reshape(sh)
        H = add.sum(axis=1)
        # H = H / H.sum(axis=1)[:,None]
        H /= gp_dist[0,:].sum()
        return H

    def apply_weighting_matrix(self, x, halfwidth = None):
        Nx, Ny, lower_index, upper_index, LtG, gp_dist = self.construct_weighting_matrix_prelims(halfwidth)

        convert_back = False
        if x.size == Nx:
            x = x[:,None]
            convert_back = True
        # Hx = zeros((Ny, x.shape[1]))
        # for n in range(Nx):
        #     wt1 = (1.0 - lctnfrac) * (lower_index == n) + (lctnfrac) * (upper_index == n)
        #     wt2 = wt1 * gp_dist
        #     Hx += (wt2.sum(axis=1)[:,None] * x[n,:])
        sh = list(LtG.shape)
        sh.append(x.shape[1])
        add = (gp_dist - LtG)[:,:,None] * x[lower_index.reshape(-1),:].reshape(sh) + \
                          LtG[:,:,None] * x[upper_index.reshape(-1),:].reshape(sh)
        Hx = add.sum(axis=1)
        Hx /= gp_dist[0,:].sum()
        if convert_back: Hx = Hx[:,0]

        return Hx

class covmat(object):
    """
    Creates covariance matrix. Can specify arbitrary covariance matrix with `Sigma`. Otherwise, specifying both `sigma` and `N` creates a covariance matrix with no correlation.
    """

    def __init__(self, sigma = None, N = None, Sigma = None, Sigma_chol = None, calc_chol = True):
        self.N = N
        self.Sigma = Sigma
        self.Sigma_chol = Sigma_chol
        self.calc_chol = calc_chol

        if Sigma_chol is not None: # Sigma_chol is specified
            if sigma is not None or N is not None or Sigma is not None: logging.warning("Since `Sigma_chol` is specified, `sigma`, `N`, and `Sigma` are ignored.")
            if not allclose(Sigma_chol, tril(Sigma_chol)): raise KeyError("`Sigma_chol` is not lower triangular.")
            self.N = Sigma_chol.shape[0]
            self.Sigma = tcrossprod(self.Sigma_chol)
            self.calc_chol = True

        if sigma is not None and N is not None: # no correlation
            if Sigma is not None: logging.warning("Since `sigma` and `N` were both specified, `Sigma` is ignored.")

            sigma = asarray(sigma)
            if   sigma.size == 1: sigma = array([sigma] * N)
            elif sigma.size != N: raise KeyError("`sigma` must be a scalar or have length `N`.")

            self.Sigma_chol = diag(sigma)
            self.Sigma = tcrossprod(self.Sigma_chol)
            self.calc_chol = True

        elif Sigma is not None: # arbitrary covariance matrix
            if sigma is not None or N is not None: logging.warning("Since `Sigma` was specified, `sigma` set to None.")
            self.N = Sigma.shape[0]
            self.Sigma_chol = cholesky(Sigma) if self.calc_chol else None

        else:
            raise ValueError("Must specify both `sigma` and `N` (no correlation) or `Sigma` (arbitrary covariance matrix) or `Sigma_chol` (Cholesky decomp of Sigma) ")

        self.mvnorm = mvnorm(mean = None, cov = self.Sigma)
        self.sigma = sqrt(self.Sigma.diagonal())

        if self.calc_chol:
            self.logdet = 2*log(self.Sigma_chol.diagonal()).sum()
            self.lognormalizingconstant = -.5*( self.N*log(2*pi) + self.logdet )

    def rvs(self, size, mu = 0., sd_multiply = 1., seed = None):
        if self.Sigma_chol is None: return KeyError("Choleksky has not been calculated because 'calc_chol' is False.")

        mu = array(mu)
        if mu.size == 1:        mu = ones(self.N) * mu
        elif mu.size == self.N: pass
        else:                   raise KeyError("`mu` must be a scalar or vector of length `self.N`, otherwise `None` (vector of zeros).")

        Sigma_chol = sd_multiply * self.Sigma_chol

        if seed is not None: set_seed(seed)
        if isinstance(size, tuple):
            if len(size) == 1:
                out = mu + dot(Sigma_chol, normal().rvs(size))
            elif len(size) == 2:
                if size[0] == self.N:
                    out = mu[:, newaxis] + dot(Sigma_chol, normal().rvs(size))
                else:
                    out = mu[newaxis, :] + dot(normal().rvs(size), Sigma_chol)
            else:
                raise ValueError("If `size` is of class `tuple`, should be of length 1 or 2.")
        elif isinstance(size, int):
            out = mu[:, newaxis] + dot(Sigma_chol, normal().rvs((self.N, size)))
        else:
            raise ValueError("`size` should be of class `int` or `tuple`.")
        if seed is not None: set_seed(None)

        return out

    def pdf(self, x, mean = None):
        return exp(self.logpdf(x, mean))

    def logpdf(self, x, mean = None):
        #return mvnorm.logpdf(x, mean = mean, cov = self.Sigma)
        # x : array with shape (d, n), where d is the dimensions of the random vector
        if self.Sigma_chol is None: return KeyError("Choleksky has not been calculated because 'calc_chol' is False.")
        if x.shape[0] != self.N: return ValueError("First dimension of 'x' needs to match up with Sigma's dimensions.")
        if mean is not None: x -= mean[:,None]
        La = solve(self.Sigma_chol, x)
        out = self.lognormalizingconstant -.5*(La**2.).sum(axis=0)
        return out

class init_cond(object):
    """ Initial conditions """
    def __init__(self, M, samples = None, mean = None, Sigma = None, seed = None):
        # TODO: generalize to arbitrary distributions
        self.M = M
        self.mean = mean
        self.Sigma = Sigma
        self.seed = seed
        self.samples = samples # N x Mbig
        if self.samples is not None: self.samplesize = self.samples.shape[1]
        self.init_ens()

    def init_ens(self):
        if self.samples is not None:
            m = choice(self.samplesize, self.M, replace = False)
            self.ens = self.samples[:,m]
        else:
            self.M = int(self.M)
            self.mean = self.mean.astype("float")
            self.ens = self.Sigma.rvs(self.M, mu = self.mean, seed = self.seed)

    def savemat(self, filename):
        """ Save as MATLAB-style .mat file. """
        savemat(filename,
            {'M': M, 'mean': mean, 'Sigma': Sigma, 'ens': ens})

class hmm_getstate(object):
    def __init__(self, hmm, ics):
        """
        Get HMM state values.

        Parameters
        ----------
        hmm : hmm
        ics : init_cond
        """
        self.N = hmm.N
        self.M = ics.M
        self.T = hmm.T

        self.hmm = hmm
        self.ics = ics

        self.ti = dict(filter = 1, smoother = hmm.T - 2)
        self.loglik = dict(filter = None)
        self.loglik_t = dict(filter = nones(self.T))

        self.filter_error = False

    def run_filter(self, verbose=False):
        for t in xrange(1, self.T):
            self.filter(verbose)
            if self.filter_error:
                logging.warning("Filter error! Stopped at ti = %i. Error msg: %s" % (self.ti["filter"], self.filter_error_msg))
                break
        self.sum_loglik("filter", verbose = verbose)

    def reset(self):
        self.ensemble.reset()
        self.init_filter()

    def init_loglik(self, key):
        self.loglik[key] = None
        self.loglik_t[key] = nones(self.T)

    def run_loglik(self, key, *args, **kwargs):
        self.init_loglik(key)
        for ti in range(1, self.T): self.calc_loglik_t(ti, key, *args, **kwargs)
        self.sum_loglik(key)

    def sum_loglik(self, key = "filter", ti0 = 1, ti1 = None, verbose = False):
        if ti1 is None: ti1 = self.T
        self.loglik[key] = nansum(self.loglik_t[key][ti0:ti1])
        return self.loglik[key]

    def plot3d(self, ti, name):
        if self.hmm.N == 3:
            fig, subfig = self.hmm.plot3d(ti)
            self.plot3d_postx(ti, name, subfig)
            for key in self.ensemble.keys:
                subfig.text(self.ensemble.mean[key][ti,0], self.ensemble.mean[key][ti,1], self.ensemble.mean[key][ti,2], key[0])
            return fig, subfig
        else:
            raise ValueError("This function can only deal with a 3d state space (N = 3).")

    def plot1d(self, ni, name, t_start = None, t_end = None, plot_ensemble = True, col = "Gray", alpha = .5):
        n_len = len(ni)

        if t_start is None: t_start = self.hmm.ty.min()
        if t_end is None: t_end = self.hmm.ty.max()

        # TODO: do this better
        txi = range(where(self.hmm.tx - t_start == 0)[0][0],
                    where(self.hmm.tx - t_end == 0)[0][0] + 1)
        tyi = range(where(self.hmm.ty - t_start == 0)[0][0],
                    where(self.hmm.ty - t_end == 0)[0][0] + 1)

        fig, subfig = self.hmm.plot1d(ni, t_start, t_end)
        if n_len == 1: subfig = array((subfig,))

        # x | data
        for n in xrange(n_len): self.plot1d_postx(ni[n], name, subfig[n], tyi, plot_ensemble, col, alpha)

        if n_len == 1: subfig = subfig[0]

        return fig, subfig

    def plot1d_postx(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def plot3d_postx(self):
        raise NotImplementedError("Subclass must implement abstract method.")

    def rmse(self, ti, **kwargs):
        truth_type = kwargs.pop("truth_type", "x")
        key = kwargs.pop("key", "forecast")
        if truth_type == "x":
            return self.rmse_x(ti, key, **kwargs)
        elif truth_type == "y":
            return self.rmse_y(ti, key)

    def rmse_y(self, ti, key):
        y = self.hmm.y[ti,:][:,None]
        Hx = dot(self.hmm.measurement_model.H, self.ensemble.ens[key][:,:,ti])
        wt = 1./self.M * ones(self.M) if self.ensemble.type == "enkf" else exp(self.ensemble.logwt[key][ti,:])
        diff = norm(y - Hx, axis = 0)
        return sqrt(average(diff**2, weights = wt))

    def rmse_x(self, ti, key, recalculate = False):
        if self.hmm.x_mean is None:
            raise KeyError("Cannot calculate RMSE when 'hmm.x_mean' is None.")

        if recalculate:
            if not in1d(self.ensemble.type, ["pf", "pkf"]): raise KeyError("'recalculate' can be 'True' only for particle filters.")
            logwt = self.ensemble.logwt["analysis"][-1,:]
            pred = array([self.ensemble.calc_mean(key, ti, logwt) for ti in range(ti_start, self.T)])

        else:
            pred = self.ensemble.mean[key][ti,:]

        truth = self.hmm.x_mean[self.hmm.tmatch[ti],:]
        diff = pred - truth
        return norm(diff)

    def spread(self, ti, key = "forecast", assume_independent = False, pca_threshold = 1):
        if assume_independent:
            logsd = log(self.ensemble.sd[key][ti,:])
            spread = exp(1./self.N * logsd.sum())
        else:
            C = cov(self.ensemble.ens[key][:,:,ti], ddof = 1)

            if pca_threshold < 1 and pca_threshold > 0:
                U, d, V = svd(C)
                idx = where(cumsum(d/d.sum()) > pca_threshold)[0][0]
                spread = exp(1/(2.*self.N) * log(d[:idx]).sum())
            elif pca_threshold == 1:
                sign, logdet = slogdet(C)
                spread = sign * exp(1/(2.*self.N) * logdet)
            else:
                raise KeyError("0 < pca_threshold <= 1")

        return spread

    def crps(self, ti, truth_type = "x", key = "forecast"):
        if truth_type == "y":
            truth = self.hmm.y[ti,:][:,None]
            pred = dot(self.hmm.measurement_model.H, self.ensemble.ens[key][:,:,ti])
        else:
            truth = self.hmm.x_mean[self.hmm.tmatch[ti],:]
            pred = self.ensemble.ens[key][:,:,ti]
        wt = 1./self.M * ones(self.M) if self.ensemble.type == "enkf" else exp(self.ensemble.logwt[key][ti,:])

        truth_minus_pred = average(norm(truth[:,None] - pred, axis = 0), weights = wt)
        pred_minus_pred = [average(norm(pred[:,m][:,newaxis] - pred, axis = 0), weights = wt) for m in range(self.M)]
        pred_minus_pred = average(pred_minus_pred, weights = wt)

        out = truth_minus_pred - .5 * pred_minus_pred
        return out

    def uni_rank_hist(self, key = "forecast", ti_start = 1):
        ens = append(self.hmm.y[ti_start:,:].T[:,newaxis,:], self.ensemble.ens[key][:,:,ti_start:], axis = 1)
        ranks = ens.argsort(axis = 1)[:,0,:] + 1
        return ranks

    def mult_rank_hist(self, key = "forecast", ti_start = 1, ni = 0):
        # WARNING: code has not been tested!!!!!!!
        if type(ni) is int: ni = array([ni])
        ens = append(self.hmm.y[ti_start:,ni].T[:,newaxis,:], self.ensemble.ens[key][ni,:,ti_start:], axis = 1)
        preranks = array([[sum((ens[:,:,ti] <= ens[:,m,ti][:,newaxis]).sum(axis = 0) == ni.size) for m in range(M)] for ti in range(ens.shape[2])])
        ranks = (prerank <= prerank[:,0][:,newaxis]).sum(axis = 1)
        return ranks1

class hmm_sampler(hmm_getstate):
    # FIXME: add the ability to scale perturbation by dt
    def __init__(self, hmm, ics,
                 inflate_forecast = False,
                 inflate_ics = False,
                 inflate_random = False,
                 inflation_factor = 1.,
                 inflate_first = True,
                 perturb_forecast = False,
                 perturbation_factor = 0.,
                 inflateperturb_obs = False):
        """
        Samples states in HMM.

        Parameters
        ----------
        inflate_forecast : boolean, optional
            Is inflation part of the forecasting model (True), i.e. count the inflation of ensemble members as your forecast values, or a mechanism just before updating (False)?
        inflate_ics : boolean, optional
            If True, make the sd of the ensemble at least the sd of the initial conditions; else (default), do nothing.
        inflate_random : boolean, optional
            If True, randomly generate new ensemble with the mean and covariance of the ensemble.
        inflation_factor : float, optional
            Multiplier (inflation_factor^2) to the covariance. Assuming that the ensemble has mean zero, ens_new = inflation_factor * ens_old, hence cov(ens_new) = inflation_factor^2 * cov(ens_old).
        inflate_first : boolean, optional
            If True (default), inflation is done first and then perturbation; else, v.v.
        perturb_forecast : boolean, optional
            See inflate_forecast.
        perturbation_factor : float, optional
            Jitter the ensemble by this value.
        inflateperturb_obs : boolean, optional
            Inflate/perturb only observed states.
        """
        super(hmm_sampler, self).__init__(hmm, ics)
        self.inflate_forecast = inflate_forecast
        self.inflate_ics = inflate_ics
        self.inflate_random = inflate_random
        self.inflation_factor = float(inflation_factor)
        self.inflate_first = inflate_first
        self.perturb_forecast = perturb_forecast
        self.perturbation_factor = float(perturbation_factor)
        self.inflateperturb_obs = inflateperturb_obs

        self.inflation = inflation_factor != 1
        self.perturbation = perturbation_factor != 0
        self.inflateperturb = self.inflation or self.perturbation
        self.inflateperturb_forecast = self.inflate_forecast or self.perturb_forecast

        self.inflation_t = self.inflation_factor * ones((self.T, self.N, self.M))
        self.perturbation_t = self.perturbation_factor * ones((self.T, self.N, self.M))

    def init_smoother(self):
        if not in1d("smoother", self.ensemble.keys):
            self.ensemble.init_smoother()
            self.ti["smoother"] = self.T - 2
            self.loglik["smoother"] = 0
            self.loglik_t["smoother"] = nones(self.T)
            self.loglik_t["smoother"][-1] = self.loglik_t["filter"][-1]

    def run_filter(self, verbose=False):
        if not isnan(self.hmm.y[0,:]).all(): self.update_ti0(verbose)
        super(hmm_sampler, self).run_filter(verbose)

    def run_smoother(self, verbose=False):
        self.init_smoother()
        for t in range(self.T-1, 0, -1): self.smooth(verbose)
        print self.sum_loglik("smoother")

    def multi_advance(self, *args, **kwargs):
        return self.hmm.forward_model.multi_advance(*args, **kwargs)

    def inflate_and_perturb(self, ti, key, success = None):
        if self.inflateperturb_obs:
            idx = self.hmm.measurement_model.H.sum(axis=0) == 0
            self.perturbation_t[ti,idx,:] = 0.
            self.inflation_t[ti,idx,:] = 1.

        out = self.ensemble.ens[key][:,:,ti]
        if self.perturbation and not self.inflate_first:
            add = normal().rvs((self.N, self.M))
            out += self.perturbation_t[ti,:,:] * add
            self.ensemble.ens[key][:,:,ti] = out

        if self.inflation and self.inflate_ics:
            self.calc_inflate_ics(ti, key)

        if self.inflation:
            out = self.inflate(ti, key, success = success)

        if self.inflation and self.inflate_first and self.perturbation:
            add = normal().rvs((self.N, self.M))
            out += self.perturbation_t[ti,:,:] * add

        return out

    def inflate(self, ti, key, success = None):
        mean = self.ensemble.mean[key][ti,:][:,None]
        if isnan(mean).any(): mean = self.ensemble.calc_mean(key, ti, success = success)[:,None]
        if self.inflate_random:
            Sigma = cov(self.ensemble.ens[key][:,:,ti])
            add = mvnorm.rvs(0, Sigma, self.M).T
        else:
            add = self.ensemble.ens[key][:,:,ti] - mean
        out = mean + self.inflation_t[ti,:,:] * add
        return out

    def perturb(self, ti, key):
        out = self.ensemble.ens[key][:,:,ti]
        add = normal().rvs((self.N, self.M))
        out += self.perturbation_t[ti,:,:] * add
        return out

    def calc_inflate_ics(self, ti, key):
        mu, sd = self.ensemble.calc_stats(key, ti)
        mu = mu[:,newaxis]
        min_sd_ratio = sd / self.ics.Sigma.sigma
        out = self.inflation_t[ti,:,:] / min_sd_ratio
        idx = where(min_sd_ratio < 1)[0]
        self.inflation_t[ti,:,idx] = out[idx]

    def resample(self, logwt, resample_choice = True):
        # resample
        if resample_choice:
            m = self.resample_chooser(logwt)
            logwt = -log(self.M) * ones(self.M)
        else:
            m = arange(self.M)

        return m, logwt

    def resample_chooser(self, log_wt):
        try:
            M = log_wt.size
            if self.resample_type is "multinomial":
                return self.resample_mult(log_wt)
            elif log_wt[0] == -log(M):
                return range(M)
            elif self.resample_type is "residual":
                return self.resample_res(log_wt)
            elif self.resample_type is "systematic":
                return self.resample_sys(log_wt)
        except:
            self.filter_error = True
            self.filter_error_msg = "Resampling issues."

    def resample_mult(self, log_wt):
        # multinomial resampling
        m = choice(log_wt.size, size = self.M, p = exp(log_wt))
        return m

    def resample_res(self, log_wt):
        # residual resampling
        wt = exp(log_wt)
        Mi = self.M * wt
        Mi_floored = floor(self.M * wt)
        res = self.M - sum(Mi_floored)

        res_wt = (Mi - Mi_floored) / res
        Mi_res = multinomial(res, res_wt, 1)[0]
        Mi = Mi_floored + Mi_res
        return repeat(arange(self.M), Mi.astype(int))

    def resample_sys(self, log_wt):
        # systematic resampling
        if (log_wt == -log(self.M)).all():
            Mi = arange(self.M)
        else:
            wt = exp(log_wt)
            n = log_wt.size

            U = rand(1) / self.M
            Ui = U + [(i-1.)/self.M for i in range(1, self.M+1)]

            b = cumsum(wt)
            a = array([0] + b[range(n-1)].tolist())

            Mi = array([where(logical_and(Ui[i] >= a, Ui[i] <= b))[0][0] for i in range(self.M)])

        return Mi

    def plot3d_postx(self, ti, name, subfig):
        subfig.scatter(self.ensemble.ens[name][0,:,ti], self.ensemble.ens[name][1,:,ti], self.ensemble.ens[name][2,:,ti], c='Green', s=5, linewidth=0)

    def plot1d_postx(self, ni, name, subfig, tyi, plot_ensemble, col, alpha):
        if plot_ensemble:
            if self.ensemble.M > 500: M = choice(self.ensemble.M, 500)
            else:                     M = xrange(self.ensemble.M)
            for m in M: subfig.plot(self.hmm.ty[tyi], self.ensemble.ens[name][ni,m,tyi], c="LightGrey")

        else:
            mean  = self.ensemble.mean[name][tyi,ni]
            sd    = self.ensemble.sd[name][tyi,ni]
            lower = mean - 1.96*sd
            upper = mean + 1.96*sd
            subfig.plot(self.hmm.ty[tyi], mean, color=col, alpha=alpha)
            subfig.fill_between(self.hmm.ty[tyi], lower, upper, facecolor=col, alpha=alpha, edgecolor=None, linewidth=0, zorder=2)









