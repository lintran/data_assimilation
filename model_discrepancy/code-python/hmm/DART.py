

import os
from shutil import rmtree


def DART_filter(DART_path, truth_folder, output_folder, namelist,
                overwrite_existing = False,
                cluster = False, cluster_name = None):

  if DART_path[-1] != "/": DART_path += "/"

  # change directory
  output_folder = DART_path + output_folder
  if os.path.exists(output_folder):
    if overwrite_existing: rmtree(output_folder)
    else:                  raise Exception("'output_folder' exists. You specified not to overwrite.")
  os.makedirs(output_folder)
  os.chdir(output_folder)

  # copy symlinks to dart program
  folder = DART_path + "work"
  for f in ["filter", "filter_ics"]: os.symlink("%s/%s" % (folder, f), f)

  # copy symlink to perfect observations
  folder = DART_path + truth_folder
  for f in ["obs_seq.out"]: os.symlink("%s/%s" % (folder, f), f)

  # write namelist
  f = open("input.nml", "w")
  for line in namelist: f.write(line)
  f.close()

  # run filter
  if cluster:
    systemcall = "echo './filter' | qsub "
    if cluster_name is not None: systemcall += "-N %s -o %s.out -e %s.err" % (cluster_name, cluster_name, cluster_name)
  else:
    systemcall = "./filter"
  os.system(systemcall)

