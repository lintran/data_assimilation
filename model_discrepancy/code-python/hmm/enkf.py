
from numpy import eye, in1d, array, dot, fill_diagonal, asarray, sqrt, cov, newaxis, log, multiply, pi, repeat, ceil, inf, allclose, median, unique, round, size
from numpy.linalg import solve, eigh

from helper import *
from hmm import *
from compact_fns import gasparicohn

class enkf(hmm_sampler):
    # TODO: add functionality to init with netCDF file
    # TODO: test why this is slower for 1d?
    def __init__(self, hmm, ics,
                 predict_hessian = False,
                 predict_type = "original",
                 update_type = "etkf",
                 localization_hw = 1000000,
                 localization_distmat = None,
                 smooth_type = "linearize",
                 tlm_use_mean = False,
                 **kwargs):
        """
        Creates Ensemble Kalman Filter object.

        Parameters
        ----------
        hmm : hmm
        ics : init_cond
        predict_type : str, optional
            Only choice is the original EnKF method ("original", default).
        update_type : str, optional
            Either Ensemble transform Kalman filter ("etkf", default), perturbed observations ("perturbed"), or original method ("original", like "perturbed" but without perturbing the y obs).
        smooth_type : str, optional
            Either using the tangent linear model for backcasting ("linearize", default) or use the ensemble to calculate the covariance between the analysis at time t and forecast of time t+1 ("ensemble"). If "linearize", then `tlm_use_mean` (below) kicks in.
        tlm_use_mean : boolean, optional
            True means use analysis mean to calculate tangent linear model else use analysis ensemble.

        """

        self.filter_type = "enkf"

        # record inputs
        self.predict_hessian = predict_hessian
        self.predict_type = predict_type
        self.update_type = update_type
        self.localization_hw = localization_hw
        self.localization_distmat = localization_distmat
        self.smooth_type = smooth_type
        self.tlm_use_mean = tlm_use_mean

        # check filter and resample types
        ok_predict_type = ["original"]
        ok_update_type  = ["original", "perturbed", "etkf"]
        ok_smooth_type  = ["linearize", "ensemble"]
        if not in1d(predict_type, ok_predict_type): raise KeyError("Don't recognize that `predict_type`.")
        if not in1d(update_type, ok_update_type):   raise KeyError("Don't recognize that `update_type`.")
        if not in1d(smooth_type, ok_smooth_type):   raise KeyError("Don't recognize that `smooth_type`.")
        # TODO: create children based on `update` type

        # init
        super(enkf, self).__init__(hmm, ics, **kwargs)
        self.ensemble = ensemble_enkf(self.N, self.T, self.M)
        self.init_filter()
        self.init_localization()

    def init_filter(self):
        self.ti["filter"] = 1
        self.ensemble.ens["forecast"][:,:,0] = self.ics.ens
        self.ensemble.ens["analysis"][:,:,0] = self.ics.ens
        self.ensemble.calc_stats("analysis", 0)

    def update_ti0(self, verbose = True):
        if verbose: print "Sampling x" + str(0) + " | y0"
        ti = 0
        self.ensemble.ens["forecast"][:,:,ti] = self.ics.ens
        self.ensemble.calc_stats("forecast", ti)
        self.calc_loglik_t(ti)
        self.update(ti)

    def filter(self, verbose=False):
        ti = self.ti["filter"]
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti-1) + "}"
        self.predict(ti)
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti) + "}"
        self.update(ti)
        self.ti["filter"] += 1

    def predict(self, ti, parms = None, inflation = None): # TODO: generalize to have noise
        fm = self.hmm.forward_model
        ts = self.hmm.ty[ti-1:ti+1]
        x0 = self.ensemble.ens["analysis"][:,:,ti-1]
        goodparms = self.ensemble.logwt["forecast"][ti,:] > -inf

        # forecast ensemble members
        x1 = nones(x0.shape)
        success = array([False] * self.M)
        x1[:,goodparms], success[goodparms] = \
            self.multi_advance(x0[:,goodparms], ts, parms = None if parms is None else array(parms)[goodparms])
        self.ensemble.ens["forecast"][:,:,ti] = x1
        self.ensemble.logwt["forecast"][ti,logical_not(success)] = -inf
        self.ensemble.logwt["forecast"][ti,:] = norm_weight(self.ensemble.logwt["forecast"][ti,:])

        if success.any():
            # if estimating inflation and perturbation parameters, inflate and perturb forecasts so that weights can be calculated properly
            if self.inflate_forecast:
                if inflation is not None: self.inflation_t[ti,:,success] = inflation.T[success,:]
                self.ensemble.ens["forecast"][:,:,ti] = self.inflate_and_perturb(ti, "forecast", success = success)
            # idx = self.hmm.measurement_model.H.sum(axis=0) > 0
            # out = self.inflate_and_perturb(ti, "forecast", success = success)
            # self.ensemble.ens["forecast"][idx,:,ti] = out[idx,:]

            # wrap up forecasting
            self.ensemble.calc_stats("forecast", ti, success = success)
            self.calc_Sigmay(ti, inflateperturb = False)

            # checks if forecasting went well
            if isnan(self.Sigma_y).any(): success = array([False] * self.M)
            try: self.Sigma_y_chol = cholesky(self.Sigma_y)
            except: success = array([False] * self.M)

            if success.any(): self.calc_loglik_t(ti, calc_Sigmay = False, success = success)

        return success

    def update(self, ti, param_update_type = None):
        inflateperturb = self.inflateperturb and not self.inflateperturb_forecast
        ens_state, mu_state, U_state, Z_state, HZ_state, logwt = self.calc_mean_sqrtcov(ti, "forecast", inflateperturb)
        if not allclose(logwt, -log(self.M)*ones(self.M)):
            print logwt
            raise ValueError("All M states were not successfully integrated. Still need to figure out what to do in this case... (See note in 'calc_mean_sqrtcov' method.)")

        if inflateperturb: self.calc_Sigmay(ti, Z_state = Z_state)
        HP = dot(self.hmm.measurement_model.H, self.localization_mat * tcrossprod(Z_state))
        KG_t = solve(self.Sigma_y, HP) # transpose of Kalman gain = [H Sigma_f H + Sigma_0]^-1 H Sigma_f

        if self.update_type != "etkf": self.update_perturbed(ti, KG_t)
        else:                          self.update_etkf(ti, KG_t, mu_state, HZ_state, U_state)

        self.ensemble.calc_stats("analysis", ti, logwt)

        if param_update_type is not None:
            ens_parm, mu_parm, U_parm, Z_parm, HZ_parm, logwt = self.calc_mean_sqrtcov(ti, "forecast_param", False)
            KG_t = solve(self.Sigma_y, tcrossprod(HZ_state, Z_parm))
            # print KG_t[:,0]

            if param_update_type != "etkf": self.update_perturbed(ti, KG_t, True)
            else:                           self.update_etkf(ti, KG_t, mu_parm, HZ_state, U_parm, True)

    def update_perturbed(self, ti, KG_t, param_update = False):
        """ Perturbed observations """
        x_f = self.ensemble.ens["forecast"][:,:,ti]
        y_perturbed = asarray([self.hmm.y[ti,:]] * self.M).T
        if self.update_type == "perturbed": y_perturbed += self.hmm.measurement_model.Sigma.rvs(y_perturbed.shape)
        # TODO: generalize for arbitrary Sigma_0

        # update ensemble members: x_a = x_f + KG (y.perturbed - H x_f)
        if not param_update:
            self.ensemble.ens["analysis"][:,:,ti] = \
                x_f + crossprod(KG_t, y_perturbed - dot(self.hmm.measurement_model.H, x_f))
        else:
            self.ensemble.ens["analysis_param"][:,:,ti] = \
                self.ensemble.ens["forecast_param"][:,:,ti] + crossprod(KG_t, y_perturbed - dot(self.hmm.measurement_model.H, x_f))

    def update_etkf(self, ti, KG_t, mu_f, H_Zf, U_f, param_update = False):
        mean_increment = self.hmm.y[ti,:] - dot(self.hmm.measurement_model.H, self.ensemble.mean["forecast"][ti,:])
        mu_a = mu_f + crossprod(KG_t, mean_increment)[:,newaxis]
        Z_a = self.Z_a(H_Zf, U_f)
        key = "analysis" if not param_update else "analysis_param"
        self.ensemble.ens[key][:,:,ti] = mu_a + Z_a

    def calc_mean_sqrtcov(self, ti, key = "forecast", inflateperturb = False):
        # FIXME: figure out how to do localization with ensemble square root filter (seem that I need to switch to EAKF)
        # key = "forecast" or "forecast_param"
        success = self.ensemble.logwt["forecast"][ti,:] > -inf
        M = success.sum()
        logwt = array([-inf] * self.M)
        logwt[success] = -log(M)

        ens = self.ensemble.ens[key][:,:,ti] if not inflateperturb else self.inflate_and_perturb(ti, "forecast")
        ens = ens[:,success]
        mu = self.ensemble.mean[key][ti,:][:,newaxis]
        H = self.hmm.measurement_model.H

        U = ens - mu
        Z = 1/sqrt(M-1) * U # Sigma = Z %*% t(Z) = cov(t(ens))
        HZ = dot(H, Z) if key == "forecast" else None # H %*% Z

        # NOTE: if M < self.M, then ens, U, Z, HZ will be of size M (and any other dimension) and analysis update will throw an error

        return (ens, mu, U, Z, HZ, logwt)

    def calc_Sigmay(self, ti, Z_state = None, inflateperturb = False):
        if Z_state is None: Z_state = self.calc_mean_sqrtcov(ti, "forecast", inflateperturb)[3]
        HPHt = tcrossprod(self.hmm.measurement_model.H, b = self.localization_mat * tcrossprod(Z_state), b_weight = True)
        self.Sigma_y = HPHt + self.hmm.measurement_model.Sigma.Sigma # H Sigma_f H + Sigma_0

    def Z_a(self, H_Zf, U_f):
        """
        Ensemble transform KF (ETKF), a type of square root filter (see equation 16 of Tipping et al, 2003, "Ensemble Square Root Filters") with correction made by Wang et al 2004 as discussed in Livings et al, 2008, "Unbiased ensemble square root filter", Equation 27)
        """
        A = crossprod(a = H_Zf, b = self.hmm.measurement_model.Sigma.Sigma, b_weight = True, inv_b = True) # Z_f^T H^T Sigma_0^-1 H Z_f, shape M x M
        eig_val, eig_vec = eigh(A)

        B = eye(self.M)
        fill_diagonal(B, 1.0/sqrt(eig_val+1))

        C = tcrossprod(a = eig_vec, b = B, b_weight = True)

        Z_a = dot(U_f, C) # Sigma_a = Z_a Z_a^T

        return(Z_a)

    def init_localization(self):
        self.localization_mat = gasparicohn(self.localization_distmat, self.localization_hw) if self.localization_distmat is not None else 1.

    def smooth(self, verbose=False):
        ti = [self.ti["smoother"] + x for x in [0,1]]
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(self.M) + "}"
        if ti[0] < 1:  raise ValueError("Last smoothing was at ty[1]; can't smooth anymore.")

        diff = self.ensemble.ens["smoother"][:,:,ti[1]] - self.ensemble.ens["forecast"][:,:,ti[1]]

        if   self.smooth_type == "linearize": self.smooth_lin(ti, diff)
        elif self.smooth_type == "ensemble":  self.smooth_ens(ti, diff)
        else: raise KeyError("Don't recognize that `smooth_type`.")

        self.ensemble.calc_stats("smoother", ti[0])
        self.ti["smoother"] -= 1

    def smooth_lin(self, ti, diff):
        Sigma_f = cov(self.ensemble.ens["forecast"][:,:,ti[1]])
        Sigma_a = cov(self.ensemble.ens["analysis"][:,:,ti[0]])

        if self.tlm_use_mean:
            mu_a = self.ensemble.ens["analysis"][:,:,ti[0]]
            M = self.hmm.forward_model.tlm(mu_a, self.hmm.ty[ti])
            B_t = solve(Sigma_f, dot(M, Sigma_a))
            for m in xrange(self.M):
                xa = self.ensemble.ens["analysis"][:,m,ti[0]]
                self.ensemble.ens["smoother"][:,m,ti[0]] = xa + crossprod(B_t, diff[:,m])
        else:
            for m in xrange(self.M):
                xa = self.ensemble.ens["analysis"][:,m,ti[0]]
                M = self.hmm.forward_model.tlm(xa, self.hmm.ty[ti])
                B_t = solve(Sigma_f, dot(M, Sigma_a))
                self.ensemble.ens["smoother"][:,m,ti[0]] = xa + crossprod(B_t, diff[:,m])

    def smooth_ens(self, ti, diff):
        mu_a = self.ensemble.mean["analysis"][ti[0],:][:,newaxis]
        U_a  = self.ensemble.ens["analysis"][:,:,ti[0]] - mu_a
        Z_a  = 1/sqrt(self.M-1) * U_a # Sigma.a = Z.a %*% t(Z.a) = cov(t(analysis.ensemble))

        mu_f = self.ensemble.mean["forecast"][ti[1],:][:,newaxis]
        U_f  = self.ensemble.ens["forecast"][:,:,ti[1]] - mu_f
        Z_f  = 1/sqrt(self.M-1) * U_f # Sigma.f = Z.f %*% t(Z.f) = cov(t(forecast.ensemble))

        Sigma_f  = tcrossprod(Z_f)
        Sigma_fa = tcrossprod(Z_f, Z_a)

        B_t = solve(Sigma_f, Sigma_fa) # cov(forecast, analysis) = tcrossprod(Z.f, Z.a)
        self.ensemble.ens["smoother"][:,:,ti[0]] = \
            self.ensemble.ens["analysis"][:,:,ti[0]] + crossprod(B_t, diff)

    def calc_loglik_t(self, ti, key = "filter", calc_Sigmay = True, success = None):
        """
        Calculates log p(y_t | y_{1:t-1}), where
            p(y_t | y_{1:t-1}) = 1/M sum_m N(y_t; H x_t^(m), Sigma_0 + H Sigma_f H^T)
        """
        if success is None: success = array([True]*self.M)
        H      = self.hmm.measurement_model.H
        x_f    = self.ensemble.ens["forecast"][:,:,ti]
        if calc_Sigmay:
            self.calc_Sigmay(ti, inflateperturb = False)
            self.Sigma_y_chol = cholesky(self.Sigma_y)

        diff   = self.hmm.y[ti,:][:,newaxis] - dot(H, x_f)
        log_pm = mvnorm_logpdf(diff, Sigma_chol = self.Sigma_y_chol)
        self.ensemble.logwt["analysis"][ti,:] = self.ensemble.logwt["forecast"][ti,:] + log_pm
        self.ensemble.logwt["analysis"][ti,logical_not(success)] = -inf
        self.loglik_t[key][ti] = mean_weight(log_pm)

    def mif_resample_t(self, ti):
        parms = self.ensemble.ens["analysis_param"][:,:,ti]
        logwt = -log(self.M)*ones(self.M)
        return logwt, parms

    def mif_forecast_t(self, ti, m, log_wt0, state_parms_dict, param_update_type,
        parms_vec = None, idx_inflation = None, idx_halfwidth = None, verbose = False):

        self.mif_mean_obs_param(ti, parms_vec, idx_halfwidth)
        inflation = parms_vec[idx_inflation,:][None,:] if idx_inflation is not None else None
        success = self.predict(ti, parms = state_parms_dict, inflation = inflation)
        self.mif_update_obs_param(ti, parms_vec, idx_inflation, idx_halfwidth)

        if success.any():
            if param_update_type == "apf":
                self.ensemble.ens["forecast_old"][:,:,ti] = self.ensemble.ens["forecast"][:,:,ti]
                self.ensemble.mean["forecast_old"][ti,:]  = self.ensemble.mean["forecast"][ti,:]
                self.ensemble.sd["forecast_old"][ti,:]    = self.ensemble.sd["forecast"][ti,:]
                m, success = self.mif_apf(ti, m, log_wt0, state_parms_dict, parms_vec, idx_inflation, verbose)
            else:
                m = where(success)[0]

        fail = logical_not(success)
        if fail.any(): m = None # resample parameters and re-forecast if there are any failures
        return None, m

    def mif_mean_obs_param(self, ti, parms_vec, idx_halfwidth):
        # replaces measurement model with mean halfwidth parameter
        if idx_halfwidth is not None:
            hw_hat = median(parms_vec[idx_halfwidth,:])
            mm = self.hmm.measurement_model
            mm.halfwidth = hw_hat
            mm.H = mm.construct_weighting_matrix(hw_hat)

    def mif_apf(self, ti, m, log_wt0, parms_dict, parms_vec = None, idx_inflation = None, verbose = False):
        # returns the indices of the resampled parameters

        # pre-sample parameters
        logwt = self.ensemble.logwt["analysis"][ti,:]
        logwt[isnan(logwt)] = -inf
        logwt = norm_weight(logwt)

        if verbose:
            log_sum_wt = sum_weight(logwt)
            log_ess = -sum_weight(2*logwt) + 2*log_sum_wt
            print "ess: %.02f" % (exp(log_ess)) ,

        m, logwt = self.resample(logwt)
        self.ensemble.ens["analysis_param"][:,:,ti] = parms_vec[:,m]
        # self.ensemble.logwt["forecast"][ti,:] = self.ensemble.logwt["analysis"][ti,:]
        self.ensemble.logwt["forecast"][ti,:] = -log(self.M)*ones(self.M)

        # sample states and correct for the bias in pre-sampling parameters
        parms_dict = [parms_dict[mi] for mi in m]
        inflation = parms_vec[idx_inflation,m][None,:] if idx_inflation is not None else None
        if not self.inflate_forecast: self.inflation_t[ti,:,:] = inflation
        success = self.predict(ti, parms = parms_dict, inflation = inflation)

        return m, success

    def mif_update_obs_param(self, ti, parms_vec, idx_inflation, idx_halfwidth):
        if (not self.inflate_forecast and idx_inflation is not None) or (idx_halfwidth is not None):
            mm = self.hmm.measurement_model
            y  = self.hmm.y[ti,:][:,newaxis]
            x  = self.ensemble.ens["forecast"][:,:,ti]
            diff_old  = y - dot(mm.H, x)
            logwt_old = mm.Sigma.logpdf(diff_old)

            if idx_inflation is not None:
                self.inflation_t[ti,:,:] = parms_vec[idx_inflation,:]
                x = self.inflate_and_perturb(ti, "forecast", success = None)

            if idx_halfwidth is not None: # estimating halfwidth parameter
                hw = parms_vec[idx_halfwidth,:]
                Hx_new = array([mm.apply_weighting_matrix(x[:,mi], hw[mi]) for mi in range(self.M)]).T
            elif idx_inflation is not None: # not estimating halfwidth parameter but estimating inflation in observation space
                Hx_new = dot(mm.H, x)
            # hw = parms_vec[idx_obs_parms,:]
            # hw_rounded = floor(hw*self.N)
            # blah, m_unique, m_convertback = unique(hw_rounded, return_index = True, return_inverse = True)
            # H_new_unique = array([mm.construct_weighting_matrix(hwi) for hwi in hw[m_unique]]) # resampled
            # Hx_new = array([dot(H_new_unique[m_convertback[mi]], x[:,mi]) for mi in range(self.M)]).T
            # allclose(Hx_new, Hx_new2)
            diff_new = y - Hx_new
            logwt_new = mm.Sigma.logpdf(diff_new)

            self.ensemble.logwt["analysis"][ti,:] += logwt_new - logwt_old

    def mif_update_t(self, ti, m, log_wt0, parms_vec, param_update_type, idx_obs_parms = None):
        enkf_update_type  = ["original", "perturbed", "etkf"]
        if in1d(param_update_type, enkf_update_type): # enkf update
            self.update(ti, param_update_type)
        else:                                         # pf update
            self.update(ti, None)
            if param_update_type == "pf": self.mif_pf(ti, log_wt0)
        self.ensemble.calc_stats("analysis_param", ti)
        return None

    def mif_pf(self, ti, log_wt0):
        # resample states and parameters
        logwt_param = norm_weight(self.ensemble.logwt["analysis"][ti,:])
        logwt_state = self.ensemble.logwt["forecast"][ti,:]
        logwt_paramstate = repeat(logwt_param - logwt_state, self.M)
        m, logwt = self.resample(logwt_paramstate)

        m += 1
        m_param = ceil(m*1./self.M).astype(int)
        m_state = m - self.M*(m_param-1)
        m_param -= 1
        m_state -= 1

        self.ensemble.ens["analysis"][:,:,ti] = self.ensemble.ens["analysis"][:,m_state,ti]
        self.ensemble.ens["analysis_param"][:,:,ti] = self.ensemble.ens["forecast_param"][:,m_param,ti]

        self.ensemble.logwt["analysis"][ti,:] = logwt
        self.ensemble.calc_stats("analysis", ti)
