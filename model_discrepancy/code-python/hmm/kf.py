
from numpy import in1d, dot, inf, exp, mean
from numpy.linalg import solve

from scipy.stats import norm
from scipy.integrate import quad

from statsmodels.tools.tools import ECDF

from copy import deepcopy

from hmm import *

class kf(hmm_getstate):
    def __init__(self, hmm, ics, save_ti = None):
        super(kf, self).__init__(hmm, ics)
        self.save_ti = save_ti
        self.ensemble = ensemble_kf(self.N, self.T, self.M)
        self.init_filter()

    def init_filter(self):
        self.ti["filter"] = 1
        self.ensemble.mean["analysis"][0,:] = self.ics.mean
        self.ensemble.sd["analysis"][0,:] = self.ics.Sigma.sigma
        self.ensemble.Sigma["ti"] = self.ics.Sigma.Sigma

    def filter(self, verbose=False):
        ti = self.ti["filter"]
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti-1) + "}"
        self.predict(ti)
        if verbose: print "Sampling x" + str(ti) + " | y{0:" + str(ti) + "}"
        self.update(ti)
        self.ti["filter"] += 1

    def predict(self, ti):
        mu_a = self.ensemble.mean["analysis"][ti-1]
        Sigma_a = self.ensemble.Sigma["ti"]

        mean = self.hmm.forward_model.parms["mean"]
        F = self.hmm.forward_model.parms["F"]
        Sigma_x = self.hmm.forward_model.Sigma.Sigma

        self.ensemble.mean["forecast"][ti,:] = mean + dot(F, mu_a)
        self.ensemble.Sigma["ti"] = tcrossprod(dot(F, Sigma_a), F) + Sigma_x
        self.ensemble.sd["forecast"][ti,:] = sqrt(self.ensemble.Sigma["ti"].diagonal().copy())
        self.copy_ti(ti, "forecast")

    def update(self, ti):
        mu_f = self.ensemble.mean["forecast"][ti]
        Sigma_f = self.ensemble.Sigma["ti"]

        H = self.hmm.measurement_model.H
        Sigma_y = self.hmm.measurement_model.Sigma.Sigma
        H_Sigmaf = dot(H, Sigma_f)

        HSfH_p_Sy = tcrossprod(H_Sigmaf, H) + Sigma_y
        KG_t = solve(HSfH_p_Sy, H_Sigmaf) # transpose of Kalman gain = [H Sigma_f H + Sigma_0]^-1 H Sigma_f
        mean_increment = self.hmm.y[ti,:] - dot(H, mu_f)

        self.ensemble.mean["analysis"][ti,:] = mu_f + crossprod(KG_t, mean_increment)
        self.ensemble.Sigma["ti"] = Sigma_f - crossprod(KG_t, H_Sigmaf)
        self.ensemble.sd["analysis"][ti,:] = sqrt(self.ensemble.Sigma["ti"].diagonal().copy())
        self.copy_ti(ti, "forecast")

        self.calc_loglik_t(ti, Sigma = HSfH_p_Sy, diff = mean_increment)

    def calc_loglik_t(self, ti, Sigma, diff, key = "filter"):
        """
        Calculates log p(y_t | y_{1:t-1}), where
            p(y_t | y_{1:t-1}) = N(y_t; H x_t^(m), Sigma_0 + H Sigma_f H^T)
        """
        self.loglik_t[key][ti] = mvnorm.logpdf(diff, mean = None, cov = Sigma)

    def copy_ti(self, ti, key):
        if in1d(ti, self.save_ti): self.ensemble.Sigma[key][ti] = self.ensemble.Sigma["ti"].copy()

    def crps(self, ti, hmm_sampler, key_true, key_est):
        if self.N > 1:
            raise ValueError("Function only works for N == 1.")
        else:
            x_mean = self.ensemble.mean[key_true][ti,0]
            x_sd = self.ensemble.sd[key_true][ti,0]
            F = norm(loc = x_mean, scale = x_sd).cdf

            x_b = hmm_sampler.ensemble.ens[key_est][0,:,ti]
            if hmm_sampler.ensemble.type == "pf":
                wgts = norm_weight(log_wt = hmm_sampler.ensemble.logwt[key_est][ti,:], log_out = False)
                F_hat = ECDF_weighted(x_b, weights = wgts)
                def integrand(x): return (F(x) - F_hat(x))**2

                # deal with discontinuities
                x_b.sort()
                out = nones((3,2))
                out[0,:] = quad(integrand, -inf, x_b[0])
                out[1,:] = quad(integrand, x_b[0], x_b[-1], points = x_b)
                out[2,:] = quad(integrand, x_b[-1], inf)
                return tuple(out.sum(axis = 0))

            elif hmm_sampler.ensemble.type == "enkf":
                # TODO: 3.44 s to do this one time. can this be optimized?
                h = hmm_sampler.ensemble.sd[key_est][ti,0]
                def F_hat(x, mu): return norm(loc = mu, scale = h).cdf(x)
                def integrand(x): return mean([F(x) - F_hat(x, mu) for mu in x_b])**2
                return quad(integrand, -inf, inf)

    def plot3d_postx(self, ti, name, subfig):
        pass

    def plot1d_postx(self, ni, name, subfig, tyi):
        mean = self.ensemble.mean[name][tyi,ni]
        sd = self.ensemble.sd[name][tyi,ni]
        lower = mean - 1.96*sd
        upper = mean + 1.96*sd
        subfig.fill_between(self.hmm.ty[tyi], lower, upper, facecolor="LightGrey", edgecolor="LightGrey")







