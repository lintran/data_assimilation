
from __future__ import division
from numpy import diag, empty, inf, in1d, logical_or, logical_not, logical_and, isinf
from scipy.stats import norm as normal
from scipy.stats import multivariate_normal as mvnorm
import matplotlib.pyplot as plt
import logging
from helper import *
from IPython import embed

class pf_param(object):
    def __init__(self, B,
                 pf,
                 param_names,
                 param_sd,

                 param_min = None,
                 param_max = None,
                 param_int = None,

                 param_mean_analysis = None,
                 param_mean_diff = None,
                 param_inflate = None,
                 param_perturb = None,

                 D_print = 3,

                 filter_verbose = False,
                 reset_ics = True,
                 loglik_ti0 = 1,
                 loglik_key = "filter"):

        """
        param_names, dict: Contains two dictionaries ('state' and 'obs'). Each will have a string vector specifying the names of the parameters. The ordering will specify the ordering of param_sd, param_min, param_max, param_int, if specified, where the 'state' variables are first and then 'obs' parameters
        param_sd, float

        param_mean_analysis, string: Name of state parameter keyword name controlling mean of the analysis states. If None (default), then it is not calculated.
        param_inflate, string: Name of state parameter keyword name controlling inflation. If None (default), then inflation factors are not estimated.
        param_perturb, string: Name of state parameter keyword name controlling perturbation. If None (default), then perturbation factors are not estimated.
        """

        self.B = B
        self.pf = pf

        self.param_names = param_names
        if not in1d("state", self.param_names.keys()): self.param_names["state"] = zeros(0)
        if not in1d("obs",   self.param_names.keys()): self.param_names["obs"]   = zeros(0)
        for key in ["state", "obs"]: self.param_names[key] = array(self.param_names[key])

        self.param_sd   = param_sd.astype("float")
        self.param_var  = param_sd**2
        self.param_min  = param_min.astype("float")
        self.param_max  = param_max.astype("float")
        self.param_int  = param_int

        self.param_mean_analysis = param_mean_analysis
        self.param_mean_analysis_n = 1.
        self.param_mean_diff = param_mean_diff
        self.param_mean_diff_n = 1.

        self.D_print = D_print
        if self.D_print < 1: raise KeyError("'D_print' must be >= 1.")

        self.filter_verbose = filter_verbose

        self.reset_ics = reset_ics
        self.loglik_ti0 = loglik_ti0
        self.loglik_key = loglik_key

        self.b = 1
        self.init_params()
        self.init_cov()
        self.init_sample()
        self.M_min = self.pf.M

    def init_params(self):
        self.init_param_sizes()
        self.init_param_startend()
        self.init_param_constraints()

        if self.D_dict["obs"] > 0:
            bad = logical_not(in1d(self.param_names["obs"], ["halfwidth", "inflation"])).any()
            if bad: raise ValueError("Observation model parameter estimation has only been implemented for 'halfwidth' and 'inflation'.")

    def init_param_sizes(self):
        # get parameter sizes
        self.param_sizes = dict(state = array([]), obs = array([]))
        fm_parms         = self.pf.hmm.forward_model.parms

        self.param_sizes["state"] = array([array(fm_parms[key]).size for key in self.param_names["state"]])
        self.param_sizes["obs"]   = ones(self.param_names["obs"].size, dtype = int)

        self.D_dict = {key: int(self.param_sizes[key].sum()) for key in ["state", "obs"]}
        self.D      = self.D_dict["state"] + self.D_dict["obs"]

        self.D_print = min(self.D_print, self.D)
        if self.D_print < self.D: logging.warning("Progress reports will only show the first %i parameters." % self.D_print)

    def init_param_startend(self):
        # since parameters are specified as a dictionary, this allows for conversion from dictionary to vector and vv
        self.param_start = dict()
        self.param_end   = dict()

        for key in ["state", "obs"]:
            self.param_start[key] = empty(self.param_sizes[key].size, dtype = int)
            self.param_end[key]   = empty(self.param_sizes[key].size, dtype = int)

        start = 0
        end = 0
        self.idx_inflation = None
        self.idx_halfwidth = None
        for key in ["state", "obs"]:
            for i in range(self.param_sizes[key].size):
                self.param_start[key][i] = start
                end = start + self.param_sizes[key][i]
                self.param_end[key][i] = end
                if i == where(self.param_names[key] == "inflation")[0]:
                    self.idx_inflation = range(start, end) if (end-start)>1 else start
                    self.pf.inflation = True
                    self.pf.inflateperturb = True
                    self.pf.inflate_forecast = (key == "state")
                    self.pf.inflateperturb_forecast = self.pf.inflate_forecast
                if i == where(self.param_names["obs"] == "halfwidth")[0]:
                    self.idx_halfwidth = start
                start = end

    def init_param_constraints(self):
        # deal with vector indicating limits of the parameters
        if self.param_min is None: self.param_min = -inf*ones(self.D)
        if self.param_max is None: self.param_max =  inf*ones(self.D)
        if self.param_int is None: self.param_int =  array([False] * self.D)
        if self.param_min.size != self.D: raise KeyError("Length of 'param_min' does not match the sizes of the parameters.")
        if self.param_max.size != self.D: raise KeyError("Length of 'param_max' does not match the sizes of the parameters.")
        if self.param_min.size != self.D: raise KeyError("Length of 'param_int' does not match the sizes of the parameters.")
        self.param_real  = logical_and(isinf(self.param_min), isinf(self.param_max))
        self.param_bdd   = logical_not(self.param_real)
        self.param_ulbdd = logical_and(self.param_min > -inf, self.param_max < inf)
        self.param_ubdd  = logical_and(isinf(self.param_min), self.param_max < inf)
        self.param_lbdd  = logical_and(self.param_min > -inf, isinf(self.param_max))
        self.idx_param_real   = where(self.param_real)[0]
        self.idx_param_bdd    = where(self.param_bdd)[0]
        self.idx_param_ulbdd  = where(self.param_ulbdd)[0]
        self.idx_param_ubdd   = where(self.param_ubdd)[0]
        self.idx_param_lbdd   = where(self.param_lbdd)[0]
        self.idx_param_int    = where(self.param_int)[0]

    def find_startend(self, key, name):
        i     = where(name == self.param_names[key])[0][0]
        start = self.param_start[key][i]
        end   = self.param_end[key][i]
        return range(start, end)

    def init_cov(self):
        if self.param_sd.size != self.D: raise KeyError("Length of 'param_sd' does not match the sizes of the parameters.")
        self.param_cov = diag(self.param_var)

    def init_sample(self):
        # create record keeping arrays
        self.records = dict(loglik = nones(self.B),
                            param = nones((self.B, self.D)))

        param_dict = dict()
        if self.D_dict["state"] != 0: param_dict["state"] = self.pf.hmm.forward_model.parms
        if self.D_dict["obs"]   != 0:
            param_dict["obs"] = dict()
            if self.idx_halfwidth is not None: param_dict["obs"]["halfwidth"] = self.pf.hmm.measurement_model.halfwidth
            if not self.pf.inflate_forecast and self.idx_inflation is not None: param_dict["obs"]["inflation"] = 1.
        self.records["param"][0,:] = self.param_dict_to_vec(param_dict)

    def change_param(self, param_vec):
        self.change_param_state(param_vec)
        self.change_param_obs(param_vec)

    def change_param_state(self, param_vec):
        if self.D_dict["state"] == 0: pass
        parms = self.pf.hmm.forward_model.parms
        for i in range(self.param_names["state"].size):
            start = self.param_start["state"][i]
            end = self.param_end["state"][i]
            key = self.param_names["state"][i]
            parms[key] = param_vec[start:end]

    def change_param_obs(self, param_vec):
        if self.D_dict["obs"] == 0: pass
        if in1d("halfwidth", self.param_names["obs"]):
            mm = self.pf.hmm.measurement_model
            mm.halfwidth = param_vec[self.find_startend("obs", "halfwidth")]
            mm.construct_weighting_matrix()

    def param_dict_to_vec(self, param_dict):
        out = empty(self.D)
        if self.D_dict["state"] > 0:
            out[:self.D_dict["state"]] = self.param_dict_to_vec_state(param_dict["state"])
        if self.D_dict["obs"] > 0:
            if self.idx_halfwidth is not None: out[self.idx_halfwidth] = param_dict["obs"]["halfwidth"]
            if not self.pf.inflate_forecast and self.idx_inflation is not None: out[self.idx_inflation] = param_dict["obs"]["inflation"]
        return out

    def param_vec_to_dict(self, param_vec):
        out = dict(state = self.param_vec_to_dict_state(param_vec),
                   obs = self.param_vec_to_dict_obs(param_vec))
        return out

    def param_vec_to_dict_obs(self, param_vec):
        out = dict()
        if self.D_dict["obs"] > 0:
            if self.idx_halfwidth is not None: out["halfwidth"] = param_vec[self.idx_halfwidth]
            if not self.pf.inflate_forecast and self.idx_inflation is not None: out["inflation"] = param_vec[self.idx_inflation]
        return out

    def multi_param_vec_to_dict(self, param_vec):
        out_state = self.multi_param_vec_to_dict_state(param_vec)
        out = [dict(state = out_state[i], obs = self.param_vec_to_dict_obs(param_vec[:,m])) for m in self.pf.M]
        return out

    def param_dict_to_vec_state(self, param_dict):
        return self.pf.hmm.forward_model.param_dict_to_vec(param_dict, self.param_names["state"], self.param_start["state"], self.param_end["state"])

    def param_vec_to_dict_state(self, param_vec, param_dict = None):
        return self.pf.hmm.forward_model.param_vec_to_dict(param_vec, self.param_names["state"], self.param_start["state"], self.param_end["state"], param_dict)

    def multi_param_vec_to_dict_state(self, param_vec, param_dict = None):
        return self.pf.hmm.forward_model.multi_param_vec_to_dict(param_vec, self.param_names["state"], self.param_start["state"], self.param_end["state"], param_dict)

    def trace(self, param_type, param_name, burnin = 0):
        plt.plot(self.records["param"][burnin:,self.find_startend(param_type, param_name)])

    def hist(self, param_type, param_name, burnin = 0):
        plt.hist(self.records["param"][burnin:,self.find_startend(param_type, param_name)])

    def pftrace(self, param_type, param_name, burnin = 0, key = "analysis"):
        key = key + "_param"
        idx = self.find_startend(param_type, param_name)[0]
        plt.plot(self.pf.ensemble.ens[key][idx,:,burnin:].T)

    def run(self):
        for x in xrange(self.b, self.B): self.sample()

    def sample(self):
        self.reset_filter()
        self.run_filter(self.filter_verbose)
        self.records["loglik"][self.b] = self.loglik()
        self.print_progress()
        self.b += 1

    def print_progress(self):
        print_me = "%i lik: %.2f lik_sd: %.2s" % (self.b, self.loglik_new, std(self.records["loglik"]))
        print print_me

    def loglik(self):
        return self.pf.sum_loglik(self.loglik_key, self.loglik_ti0)

    def reset_filter(self, M = None):
        if M is not None:
            # this should only occur for MIF method. MIF requires filter to be reinitialized at every iterations
            self.pf.ics.M = M
            self.pf.ics.init_ens()
            self.pf.M = M
            self.pf.ensemble.M = M
        else:
            if self.reset_ics: self.pf.ics.init_ens()
        self.pf.reset()

    def run_filter(self):
        self.pf.run_filter()

    def sample_param(self):
        raise NotImplementedError("Subclass must implement abstract method.")












