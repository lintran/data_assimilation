from pf import *

class apf(pf):
    """
    Auxiliary particle filter.
    James McNames, "Auxiliary Particle Filters ECE 510 State Space Tracking" (lecture given on April 30, 2012 at Portland State University), http://moodle.cecs.pdx.edu/pluginfile.php/2222/mod_resource/content/0/Lecture_Slides/Auxiliary_Particle_Filters.pdf
    Pitt et al (2010), "Auxiliary particle filtering within adaptive Metropolis-Hastings sampling"
    """

    def __init__(self, *args, **kwargs):
        self.filter_type = "apf"
        super(apf, self).__init__(*args, **kwargs)
        if self.ess_threshold is None: self.ess_threshold = self.M/2.
        if self.post_reg_type is not None or self.pre_reg_type is not None:
            logging.warning("Regularization has not been implemented for Auxiliary PF. 'post_reg_type', 'pre_reg_type', and 'reg_C' will be ignored.")
            self.post_reg_type = None
            self.pre_reg_type = None

    def init_reg(self):
        # no regularization
        self.ensemble.forecast_analysis_same_all()

    def resample_t(self, ti):
        return None, None

    def forecast_t(self, ti, m, log_wt0, parms = None):
        fm = self.hmm.forward_model

        # propagate just the means (no noise): p( y_t | mu_t^k )
        log_wt0 = self.ensemble.logwt["analysis"][ti-1,:] # log w_t-1^(i)
        x1 = self.propagate_nonoise(ti, parms = parms) # mu_t^(i)

        # pretend we're actually forecasting (so we haven't seen data yet). get forecast mean and sd
        self.ensemble.ens["forecast"][:,:,ti] = x1
        self.ensemble.logwt["forecast"][ti,:] = log_wt0
        if fm.Sigma is not None:
            e1 = fm.Sigma.rvs((self.N, self.M))
            self.ensemble.ens["forecast"][:,:,ti] += e1

        self.ensemble.calc_stats("forecast", ti, log_wt0)

        # update...

        # pre-sample parents
        # first-stage weights, unnormalized: v_t^k = w_t-1^k p( y_t | mu_t^k )
        log_vt0 = self.update(ti, log_wt0) # first-stage weights, normalized
        self.ensemble.logwt["analysis"][ti,:] = log_vt0
        m, add = self.resample(log_vt0, True)

        # propagate
        self.ensemble.ens["analysis"][:,:,ti] = x1[:,m]
        if fm.Sigma is not None: self.ensemble.ens["analysis"][:,:,ti] += e1
        if self.perturbation_factor != 0: self.ensemble.ens["analysis"][:,:,ti] = self.perturb(ti, "analysis")
        self.ensemble.update_parent("analysis", ti, m, True) #, log_vt0)

        # correct for bias in pre-sampling
        # second-stage weights, unnormalized: w_t^k = w_t-1^k / v_t-1^k p( y_t^k | x_t^k )
        log_wt1 = norm_weight(log_wt0[m] - log_vt0[m]) # second-stage weights, normalized
        if isnan(log_wt1).any() or isinf(log_wt1).any():
            self.filter_error = True
            self.filter_error_msg = "Weights are 'nan' or 'inf'."
        return log_wt1

    def update_t(self, ti, log_wt1):
        return self.update(ti, log_wt1)
