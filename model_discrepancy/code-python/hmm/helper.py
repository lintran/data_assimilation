
from __future__ import division
from numpy import zeros, dot, log, exp, pi
from numpy import array, linspace, argsort, cumsum
from numpy import empty, newaxis, where, cov, sqrt, ones, average
from numpy.linalg import solve, cholesky
from numpy.random import random_integers, choice
from numpy.random import seed as set_seed

# from statsmodels.distributions.empirical_distribution import StepFunction

from IPython import embed

def nones(*args, **kwargs):
    out = zeros(*args, **kwargs)
    out[:] = None
    return out

def crossprod(a, b = None, b_weight = False, inv_b = False):
    if b is not None:
        if not b_weight:
            return dot(a.T, b)
        else:
            if not inv_b: return dot(a.T, dot(b, a))
            else:         return dot(a.T, solve(b, a))
    else:
        return dot(a.T, a)

def tcrossprod(a, b = None, b_weight = False, inv_b = False):
    if b is not None:
        if not b_weight:
            return dot(a, b.T)
        else:
            if not inv_b: return dot(a, dot(b, a.T))
            else:         return dot(a, solve(b, a.T))
    else:
        return dot(a, a.T)

def sum_weight(log_wt, log_out = True):
    """
    Calculates sum of weights, sum(wt), but specialized for really small weights:

        log(sum wt) = mp + log [ sum exp( log_wt - mp ) ],

    where mp = max log_wt. Source: http://blog.smola.org/post/987977550/log-probabilities-semirings-and-floating-point
    """
    mp = log_wt.max()
    inner_sum = exp(log_wt - mp).sum()
    out = mp + log(inner_sum)
    if log_out: return out
    else:       return exp(out)

def mean_weight(log_wt, log_out = True):
    """
    Calculates mean of weights, 1/n sum(wt)
    """
    out = -log(log_wt.size) + sum_weight(log_wt)
    if log_out: return out
    else:       return exp(out)

def norm_weight(log_wt, log_out = True, log_sum_wt = None):
    """
    Normalize weights: wt / sum(wt)
    """
    if log_sum_wt is None: log_sum_wt = sum_weight(log_wt)
    out = log_wt - log_sum_wt
    if log_out: return out
    else:       return exp(out)

def var_weighted(x, logwt = None, wt = None, mean = None, diff = False, marginal = True):
    """
    x : (dimension of random vector) x (sample size)
    https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_variance
    https://en.wikipedia.org/wiki/Weighted_arithmetic_mean#Weighted_sample_covariance
    diff : boolean
        If diff is True, then x is a centered difference, i.e. x already has the mean subtracted from it.
    """
    if logwt is not None and wt is None: # only logwt is specified
        wt = exp(logwt)
        M = len(logwt)
    elif logwt is None and wt is not None: # only wt is specified
        logwt = log(wt)
        M = len(wt)

    if diff:
        if mean is None: mean = average(x, axis = 1, weights = wt)
        x -= mean[:,newaxis]

    # calculate normalizing constant for an unbiased estiamtor
    logv1 = sum_weight(logwt)
    logv2 = sum_weight(2*logwt)
    den   = exp(logv1) - exp(logv2 - logv1)

    # calculate variance without normalizing constant
    if marginal:
        var, wt_sum = average(x**2., axis = 1, weights = wt, returned = True)
        var *= wt_sum
    else:
        wt_sqrt = exp(.5*logwt)
        diff_wt = wt_sqrt[None,:] * x
        var     = tcrossprod(diff_wt)

    return var/den

def rand_seed(size, seed = None):
    # numpy.random.seed can only take values between 0 and 4294967295
    set_seed(seed)
    return random_integers(0, 4294967295, size)

def norm(diff, ord = 2, **kwargs):
    ord = float(ord)
    return (diff**ord).mean(**kwargs)**(1/ord)

def mvnorm_logpdf(x, Sigma = None, Sigma_chol = None):
    if Sigma_chol is None: Sigma_chol = cholesky(Sigma)
    La = solve(Sigma_chol, x)
    logdet = log(Sigma_chol.diagonal()).sum()
    d = Sigma_chol.shape[0]
    out = -.5*( d*log(2*pi) + 2*logdet + (La * La).sum(axis = 0) )
    return out

def symmetrize(a):
    return (a + a.T)/2

def logit(p, eps = 1e-4):
    # p \in (0,1)
    p = array(p)
    p[p == 0] += eps
    p[p == 1] -= eps
    return log(p) - log(1-p)

def invlogit(alpha):
    # alpha \in reals
    return 1/(1+exp(alpha))

def logit2(p, m, M):
    # p \in [m, M]
    rng = M-m
    return logit((p-m)/rng)

def invlogit2(x, m, M):
    # x \in reals
    rng = M-m
    return rng*invlogit(x) + m

# class ECDF_weighted(StepFunction):
#     """
#     Modified from `statsmodels.tools.tools.ECDF` / `statsmodels.distributions.empirical_distribution.ECDF`.
#     """
#     def __init__(self, x, side='right', weights=None):
#         x = array(x, copy=True)
#         nobs = len(x)

#         if weights is None:
#             x.sort()
#             y = linspace(1./nobs,1,nobs)
#         else:
#             if sum(weights).round(10) != 1: raise KeyError("`weights` don't add up to 1.")
#             order = argsort(x)
#             x = x[order]
#             weights = weights[order]
#             y = cumsum(weights)

#         super(ECDF_weighted, self).__init__(x, y, side=side, sorted=True)



class global_rodeo_bw(object):
    def __init__(self, data, beta = .9, c0 = 1, M = None):
        self.data = data
        self.beta = beta
        self.c0 = c0

        self.N, self.D = data.shape
        self.cn = log(self.N)

        self.bw = (self.c0 / log(self.cn)) * ones(self.D)

        A = range(self.D)
        if M is None: self.M = self.N
        self.M_set = choice(self.N, self.M, replace = True)
        self.calc_ts()

        while len(A) != 0:
            A_sub = A
            for d in A_sub:
                if abs(self.T_d[d]) > self.lambda_d[d]:
                    self.bw[d] *= self.beta
                    self.calc_ts()
                else:
                    A.remove(d)

    def Z(self, x):
        num = (x[newaxis,:] - self.data)**2.                     # N x D
        bw_sq = self.bw**2                                       # D
        log_b = -(num/(2*bw_sq[newaxis,:])).sum(axis = 1)        # N
        Z_nd  = (num - bw_sq[newaxis,:]) * exp(log_b[:,newaxis]) # N x D
        return Z_nd

    def calc_ts(self):
        Z_ndm = empty((self.N, self.D, self.M)) # N x D x M
        for m in set(self.M_set): Z_ndm[:,:, m == self.M_set] = self.Z(self.data[m,:])[:,:,newaxis]
        Z_dm = Z_ndm.mean(axis = 0) # D x M
        self.T_d = (Z_dm**2).mean(axis = 1) # D

        mu_d = Z_ndm.mean(axis = 0) # D x M
        self.sd_d = empty(self.D)
        for d in range(self.D):
            C_d = cov(Z_ndm[:,d,:].T) / self.N # M x M
            C_d_tr = (dot(C_d, C_d)).diagonal().sum()
            self.sd_d[d] = 1./self.M * sqrt(2.*C_d_tr + 4.*crossprod(mu_d[d,:], C_d, b_weight = True, inv_b = False))
        self.lambda_d = self.sd_d**2 + 2*self.sd_d * sqrt(log(self.N * self.cn))

    """
    from numpy.random import multivariate_normal as mvrnorm
    from numpy.random import uniform, normal
    from numpy import ones, diag, zeros

    a = uniform(0, 1, (100, 30))
    for i in range(5): a[:,i] = normal(0.5, (0.02*(i+1))**2, 100)
    b = global_rodeo_bw(a, c0 = a.std(axis=0).max()*10)
    print cov(a.T).diagonal()
    print b.bw
    print b.T_d
    print b.lambda_d
    """


