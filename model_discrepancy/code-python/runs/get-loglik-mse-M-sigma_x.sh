#!/bin/bash

# $1 model name
# $2 use cluster?

# set up depending on model name
if [ "$1" = "linear" ]; then
    Mvec=(10 100 500 1000 2000)
    sx=(.1 1)
    N=1
    T=500
    opts1="-run_kf -run_perturbed -C .1 1 -sigma_x .1"
    opts2="-run_kf -run_perturbed -C .1 1 -sigma_x 1"

elif [ "$1" == "lorenz63" ]; then
    Mvec=(2 3 10 100 500)
    sx=(.1 1)
    N=3
    T=300
    opts1="-run_etkf -sigma_x .1"
    opts2="-run_etkf -sigma_x 1"

elif [ "$1" == "lorenz05" ]; then
    Mvec=(30 60 120 240 480)
    sx=(.1 1)
    N=240
    T=100
    opts1="-runs 50 -run_etkf -sigma_x .1"
    opts2="-runs 50 -run_etkf -sigma_x 1"

else
    echo "ERROR: Don't recognize that model type."
    exit 1 # terminate and indicate error
fi

# runs
for M in "${Mvec[@]}"
do
    name1="$1-M${M}-sx${sx[0]}-diffics"
    filename1="M${M}-sx${sx[0]}.mat"

    name2="$1-M${M}-sx${sx[1]}-diffics"
    filename2="M${M}-sx${sx[1]}.mat"

    if [ "$2" == "true" ]; then # use cluster
        echo "using cluster"

        echo $name1
        echo "python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename1} ${opts1}" | qsub -o outfiles/${name1}.out -e errfiles/${name1}.err -N ${name1}
        sleep 2s

        echo $name2
        echo "python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename2} ${opts2}" | qsub -o outfiles/${name2}.out -e errfiles/${name2}.err -N ${name2}
        sleep 2s

    elif [ "$2" == "false" ]; then # don't use cluster
        echo "not using cluster"

        echo $name1
        nohup python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename1} ${opts1} > outfiles/${name1}.out &
        sleep 2s

        echo $name2
        nohup python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename2} ${opts2} > outfiles/${name2}.out &
        sleep 2s

    else
        echo "ERROR: Second argument must be 'true' or 'false'."
    fi
done
