
# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("est", action = "store", type = str)
parser.add_argument("update_type", action = "store", type = str)

# optional arguments
parser.add_argument("-est_hw", action = "store_true", default = False)
parser.add_argument("-M", action = "store", type = int, default = 100)
parser.add_argument("-B", action = "store", type = int, default = 25)
parser.add_argument("-ncpus", action = "store", type = int, default = None)
parser.add_argument("-runone", action = "store_true", default = False)
parser.add_argument("-save", action = "store_true", default = False)
parser.add_argument("-savename", action = "store", type = str, default = "")
parser.add_argument("-saveidx", action = "store", type = int, default = None)
# parser.add_argument("-obs", action = "store", type = str, default = "integral-random-480")
parser.add_argument("-obs", action = "store", type = str, default = "identity-other-480")
parser.add_argument("-inflation", action = "store_true", default = False)
parser.add_argument("-inflation_value", action = "store", type = float, default = None)
parser.add_argument("-dt", action = "store", type = int, default = 48)
parser.add_argument("-lsode", action = "store_true", default = False)
parser.add_argument("-sd_y", action = "store", type = float, default = 1)
parser.add_argument("-tune_cov_eps", action = "store", type = float, default = 1)
parser.add_argument("-dart", action = "store_true", default = False)


# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["KF", "apf", "-est_hw", "-saveidx", "2", "-M", "200", "-obs", "integral-random-480", "-dt", "24", "-sd_y", "1", "-inflation", "-inflation_value", "1.5"])
# args = parser.parse_args(["KF", "apf", "-saveidx", "2", "-M", "101", "-obs", "integral-random-480", "-dt", "24", "-inflation", "-sd_y", "1"])#, "-inflation_value", "2"])
print args

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz05 import *

# TRUE MODEL (DATA GENERATED EVERY 6 HOURS) ----------------------------------------
if args.dart:
  # match up with DART
  args.dartfolder = "default-%s/dt06/perfect" % args.obs
  DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_04/"
  inits = loadmat(DARTdir + "work/inits.mat")
  obs = loadmat(DARTdir + args.dartfolder + "/obs.mat")
  truth = netcdf.netcdf_file(DARTdir + args.dartfolder + "/True_State.nc", "r")

  T = (1*365*4) # equivalent to 1 year
  x = truth.variables["state"][:T,0,:].copy()
  y = obs["obs"][:,:T].T
  dt_convert = dict(days = truth.time_step_days.copy(), seconds = truth.time_step_seconds.copy())

  parms = dict(N = x.shape[1],                    # 960
               K = truth.K.copy(),                # 32
               I = truth.smooth_steps.copy(),     # 12
               F = truth.model_forcing.copy(),    # 15
               b = truth.space_time_scale.copy(), # 10
               c = truth.coupling.copy(),         # 2.5
               model_number = 3 if truth.model_scale == "2-scale" else 2,
               dt = truth.model_delta_t.copy())

  x0 = x[0,:]
  ty_real = truth.variables["time"][:T].copy()
  ty = parms["dt"]*(24*60*60*ty_real / (24*60*60*dt_convert["days"] + dt_convert["seconds"]))
  print ty.size
  locx = truth.variables["loc1d"][:].copy()

  truth.close()

  # forward model
  fm = forward_model(adv_model_lsode if args.lsode else adv_model_rk, parms, deriv, fparms = calc_consts, n_cpus = args.ncpus)
  fm.parms = calc_consts(fm.parms)

  # observation model
  import re
  obstype1 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\2", args.dartfolder)
  obstype2 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\3", args.dartfolder)
  Sigma_y = covmat(sigma = sqrt(obs["var"]), N = obs["obs"].shape[0])

  if obstype1 == "identity":
    H = diag([1]*parms["N"])
    if obstype2 == "other": H = H[arange(0,parms["N"],parms["N"]/obs["obs"].shape[0]),:]
    om = measurement_model(Sigma = Sigma_y, H = H)
  elif obstype1 == "linear":
    H = zeros((obs["obs"].shape[0], parms["N"]))
    for i in range(obs["obs"].shape[0]):
      idx_less = where(locx < obs["loc"][i])[0][-1]
      H[i,idx_less] = (obs["loc"][i] - locx[idx_less])
      H[i,idx_less+1] = (locx[idx_less+1] - obs["loc"][i])
    H /= (1./parms["N"])
    om = measurement_model(Sigma = Sigma_y, H = H)
  elif obstype1 == "integral":
    om = \
        measurement_model(Sigma = Sigma_y,
            loc_x = locx,
            loc_y = obs["loc"],
            halfwidth = obs["halfwidth"][0],
            num_points = obs["num_points"][0])

  a = dot(om.H, x.T)
  b = obs["truth"][:,:T]
  if not allclose(a,b): raise Exception("Observation model doesn't match up with DART's.")

  # hmm
  l05hmm = hmm(fm, om, x0, ty, x = x, y = y)

else: # model 2 ---------------------------------------------------------------------------
  # kinda match up with dart
  args.dartfolder = "default-%s/dt06/perfect" % args.obs
  DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_04/"
  inits = loadmat(DARTdir + "work/inits.mat")
  obs = loadmat(DARTdir + args.dartfolder + "/obs.mat")
  truth = netcdf.netcdf_file(DARTdir + args.dartfolder + "/True_State.nc", "r")

  T = (1*365*4) # equivalent to 1 year
  dt_convert = dict(days = truth.time_step_days.copy(), seconds = truth.time_step_seconds.copy())

  parms = dict(N = truth.variables["state"].shape[2], # 960
               K = truth.K.copy(),                    # 32
               F = truth.model_forcing.copy(),        # 15
               model_number = 2,
               dt = truth.model_delta_t.copy())

  x0 = inits["perfect"]
  ty_real = truth.variables["time"][:T].copy()
  ty = parms["dt"]*(24*60*60*ty_real / (24*60*60*dt_convert["days"] + dt_convert["seconds"]))
  print ty.size
  locx = truth.variables["loc1d"][:].copy()

  truth.close()

  # forward model
  fm = forward_model(adv_model_lsode if args.lsode else adv_model_rk, parms, deriv, fparms = calc_consts, n_cpus = args.ncpus)
  fm.parms = calc_consts(fm.parms)

  # observation model
  import re
  obstype1 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\2", args.dartfolder)
  obstype2 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\3", args.dartfolder)
  Sigma_y = covmat(sigma = args.sd_y, N = obs["obs"].shape[0])

  if obstype1 == "identity":
    H = diag([1]*parms["N"])
    if obstype2 == "other": H = H[arange(0,parms["N"],parms["N"]/obs["obs"].shape[0]),:]
    om = measurement_model(Sigma = Sigma_y, H = H)
  elif obstype1 == "linear":
    H = zeros((obs["obs"].shape[0], parms["N"]))
    for i in range(obs["obs"].shape[0]):
      idx_less = where(locx < obs["loc"][i])[0][-1]
      H[i,idx_less] = (obs["loc"][i] - locx[idx_less])
      H[i,idx_less+1] = (locx[idx_less+1] - obs["loc"][i])
    H /= (1./parms["N"])
    om = measurement_model(Sigma = Sigma_y, H = H)
  elif obstype1 == "integral":
    om = \
        measurement_model(Sigma = Sigma_y,
            loc_x = locx,
            loc_y = obs["loc"],
            halfwidth = obs["halfwidth"][0],
            num_points = obs["num_points"][0])

  # hmm
  l05hmm = hmm(fm, om, x0, ty, seed_y = 12345)

# FILTER INITIALIZATION --------------------------------------------------
# hmm with data generated every 'dt' hours
idx = arange(0, ty.size, args.dt/6)
x = l05hmm.x
y = l05hmm.y
l05hmm = hmm(fm, om, x0, ty[idx], x = x, tx = ty, y = y[idx,:])
T = idx.size

# initial conditions
# ics = init_cond(M = args.M, samples = inits["perturbed"][range(0,960,960/parms["N"]),:], seed = args.saveidx)
# ics.ens = ics.samples[:,range(args.M)]
ics = init_cond(M = args.M, mean = x0, Sigma = covmat(sigma = 1, N = parms["N"]))

# localization
distvec = locx
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T
maxdist = distvec.max()

# initialize EnKF
l05enkf = enkf(l05hmm, ics,
               update_type = "etkf",
               inflation_factor = args.inflation_value if args.inflation and args.inflation_value is not None else 1.,
               inflate_forecast = True,
               localization_hw = 5/960.,
               localization_distmat = distmat)

# PARAMETER INITIALIZATION -----------------------------------------------
print "parms right", fm.parms

def calc_params(parms):
  if args.est.find("K") >= 0:
    parms = calc_K(parms)
  if args.est.find("I") >= 0:
    parms = calc_I(parms)
  if args.est.find("a") >= 0 or args.est.find("B") >= 0:
    parms = calc_ab(parms)
  if args.est.find("b") >= 0:
    parms = calc_b(parms)
  # if args.est.find("b") >= 0 and args.est.find("c") >= 0:
    # parms = calc_c(parms)
  return parms

def calc_params2(parms):
  parms = calc_c(parms)
  parms = calc_consts(parms)
  return parms

if args.est != "KFIbc":
  fm.fparms = calc_params
# else:
#   fm.fparms = calc_params2

param_names = array([])
param_min   = array([])
param_max   = array([])
param_sd    = array([])
param_int   = array([])

set_seed(args.saveidx)
if args.est.find("K") >= 0:
  param_names = append(param_names, "K")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, 100)
  param_sd    = append(param_sd, .5)
  param_int   = append(param_int, True)
  fm.parms["K"] += round(normal.rvs(scale = 2*param_sd[-1]))

if args.est.find("F") >= 0:
  param_names = append(param_names, "F")
  param_min   = append(param_min, -50)
  param_max   = append(param_max, 50)
  param_sd    = append(param_sd, .5)
  param_int   = append(param_int, False)
  fm.parms["F"] += normal.rvs(scale = 2*param_sd[-1])

if args.est.find("I") >= 0:
  param_names = append(param_names, "I")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, 100)
  param_sd    = append(param_sd, .5)
  param_int   = append(param_int, True)
  fm.parms["I"] += round(normal.rvs(scale = 2*param_sd[-1]))

if args.est.find("a") >= 0:
  param_names = append(param_names, "alpha")
  param_min   = append(param_min, 0)
  param_max   = append(param_max, 1)
  param_sd    = append(param_sd, .001)
  param_int   = append(param_int, False)
  a, b = (param_min[-1] - fm.parms["alpha"]) / (2*param_sd[-1]), (param_max[-1] - fm.parms["alpha"]) / (2*param_sd[-1])
  fm.parms["alpha"] = truncnorm.rvs(a, b, loc = fm.parms["alpha"], scale = 2*param_sd[-1])

if args.est.find("B") >= 0:
  param_names = append(param_names, "beta")
  param_min   = append(param_min, 0)
  param_max   = append(param_max, .1)
  param_sd    = append(param_sd, .0001)
  param_int   = append(param_int, False)
  a, b = (param_min[-1] - fm.parms["beta"]) / (2*param_sd[-1]), (param_max[-1] - fm.parms["beta"]) / (2*param_sd[-1])
  fm.parms["beta"] = truncnorm.rvs(a, b, loc = fm.parms["beta"], scale = 2*param_sd[-1])

if args.est.find("b") >= 0:
  param_names = append(param_names, "b")
  param_min   = append(param_min, 0)
  param_max   = append(param_max, 20)
  param_sd    = append(param_sd, .5)
  param_int   = append(param_int, False)
  fm.parms["b"] += normal.rvs(scale = 2*param_sd[-1])

if args.est.find("c") >= 0:
  # if args.est.find("b") >= 0:
  #   param_names = append(param_names, "cprime")
  #   param_min   = append(param_min, 0)
  #   param_max   = append(param_max, 100)
  #   param_sd    = append(param_sd, .001)
  #   param_int   = append(param_int, False)
  #   fm.parms["cprime"] = fm.parms["c"] / fm.parms["b_sq"]
  #   fm.parms["cprime"] += normal.rvs(scale = 2*param_sd[-1])
  # else:
    param_names = append(param_names, "c")
    param_min   = append(param_min, 0)
    param_max   = append(param_max, 20)
    param_sd    = append(param_sd, .05)
    param_int   = append(param_int, False)
    fm.parms["c"] += normal.rvs(scale = 2*param_sd[-1])

fm.parms = fm.fparms(fm.parms)

if args.inflation and args.inflation_value is None:
  fm.parms["inflation"] = 1.
  param_names = append(param_names, "inflation")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, 20)
  param_sd    = append(param_sd,  .6)
  param_int   = append(param_int, False)

param_names = dict(state = param_names)
if args.est_hw:
  param_names["obs"] = array(["halfwidth"])
  param_min   = append(param_min, 0)
  param_max   = append(param_max, distvec.max()/2.)
  param_sd    = append(param_sd, 1/960.)
  param_int   = append(param_int, False)
  a, b = (param_min[-1] - om.halfwidth) / (2*param_sd[-1]), (param_max[-1] - om.halfwidth) / (2*param_sd[-1])
  om.halfwidth = truncnorm.rvs(a, b, loc = om.halfwidth, scale = 2*param_sd[-1])
  om.H = om.construct_weighting_matrix()

print "parms init", fm.parms
# set_seed(None)
print "names", param_names
print "min", param_min
print "max", param_max
print "sd", param_sd

# RUN PARAMETER ESTIMATION ---------------------------------------------
if2 = IteratedFilter2(
        args.B, l05enkf,
        filter_verbose = True,
        param_names = param_names,
        param_sd  = param_sd,
        param_min = param_min,
        param_max = param_max,
        param_int = param_int,
        param_inflate = "inflation" if args.inflation and args.inflation_value is None else None,
        D_print = param_sd.size,
        loglik_ti0 = 0,
        update_type = args.update_type,
        # resample_type = "systematic",
        resample_type = "multinomial",
        tune_cov_eps = args.tune_cov_eps)

if args.runone:
  pr.enable()
  if2.sample()
  pr.disable()
  ts = time.time()
  ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M-%S')
  dr = path + "/profiling/lorenz05/est-param/"
  if not os.path.exists(dr): os.makedirs(dr)
  filename = dr + "%02i" % (1 if args.ncpus is None else args.ncpus)
  filename += "-" + ts + ".out"
  pr.dump_stats(filename)
  # cProfilev (insert filename)
  # localhost:4000
else:
  from time import time
  begin = time()
  if2.run()
  end = time()
  print "Elapsed time: ", ((end - begin) / 60)
  # if2.sample()

if False:
  l05enkf.plot1d(xrange(3), "forecast")
  plt.show()

  if2.pftrace("state", "b"); plt.show()

if args.save:
  if args.saveidx is None:
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d-%H-%M-%S")
  else:
    ts = "%03i" % args.saveidx
  if args.savename != "": ts = args.savename + "-" + ts
  args.dartfolder = re.sub("dt06", "dt%02i" % (args.dt), args.dartfolder)
  dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/thesis/lorenz05/param-est/"
  dr += args.dartfolder
  dr += ("" if args.dart else "_wrong")  + "/"
  dr += "lsode/" if args.lsode else "rk/"

  if not args.inflation:             inf10 = 10
  elif args.inflation_value is None: inf10 = 0
  else:                              inf10 = args.inflation_value * 10

  #dr += "infmin%03i/" % (args.inflation_min)
  if not os.path.exists(dr): os.makedirs(dr)
  filename = "%s/%s-%s-inf%02i-M%03i-B%03i-%s" % (dr, args.est, args.update_type, inf10, args.M, args.B, ts)


  # import cPickle
  # f = file(filename + ".pyobj", "wb")
  # l63enkf.hmm.forward_model = None
  # cPickle.dump(if2, f, protocol = cPickle.HIGHEST_PROTOCOL)
  # f.close()

  args = vars(args)
  args = {key: value if value is not None else [] for key, value in args.iteritems()}
  if2.savemat(filename + ".mat", dict_add = dict(args = args, time = (end - begin) / 60))

