#!/bin/bash
high=0
est="KF"
B=25

mkdir -p outfiles/${est}

for update_type in "apf" "etkf"
do
  for M in 51 101 201
  do
    for i in $(seq 1 20)
    do
      ncpus=1
      # if [ "$M" == "201" ]; then ncpus=2; fi

      python_ncpus=""
      qsub_ncpus=""
      if [ "${ncpus}" -gt "1" ]; then
        python_ncpus="-ncpus ${ncpus}"
        qsub_ncpus="-pe smp ${ncpus} -R y"
      fi

      cmd="python est-param.py ${est} ${update_type} ${python_ncpus} \
            -dt 24 \
            -B $B \
            -M $M \
            -sd_y 1 \
            -save -saveidx $i"
      qsub_command="qsub ${qsub_ncpus} -j y" #"-j y -js 1"
      # if [ "$i" -le "${high}" ] && [ "${M}" == 101 ]; then qsub_command="${qsub_command} -q high.q"; fi
      # if [ "${update_type}" == "apf" ]; then qsub_command="${qsub_command} -js 1"; fi

      # # "easy" observation model ----------------------------------------------------------
      # # ...without inflation
      # name=$(printf all-dt024-${update_type}-inf10-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-all-960" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # ...with adaptive inflation
      # name=$(printf all-dt024-${update_type}-infad-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-all-960 -inflation" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # ...with fixed inflation
      # name=$(printf all-dt024-${update_type}-inf15-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-all-960 -inflation -inflation_value 1.5" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # # "hard" observation model ----------------------------------------------------------
      # # # ...without inflation
      # name=$(printf other-dt024-${update_type}-inf10-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-other-480" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # # ...with adaptive inflation
      # name=$(printf other-dt024-${update_type}-infad-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-other-480 -inflation" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # # ...with fixed inflation
      # name=$(printf other-dt024-${update_type}-inf15-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs identity-other-480 -inflation -inflation_value 1.5" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # estimate halfwidth parameter -------------------------------------------------------
      # doesn't work without inflation
      if [ "${update_type}" == "apf" ]; then cmd="${cmd} -est_hw"; fi

      # # ...without inflation
      name=$(printf integral-dt024-${update_type}-inf10-M%03i-B%03i-%03i $M $B $i)
      if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      echo "${cmd} -obs integral-random-480" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      sleep .5

      # # ...with adaptive inflation
      # name=$(printf integral-dt024-${update_type}-infad-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs integral-random-480 -inflation" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5

      # # ...with fixed inflation
      # name=$(printf integral-dt024-${update_type}-inf15-M%03i-B%03i-%03i $M $B $i)
      # if [ -e "outfiles/${est}/${name}.out" ]; then rm outfiles/${est}/${name}.out; fi
      # echo "${cmd} -obs integral-random-480 -inflation -inflation_value 1.5" | ${qsub_command} -N ${name} -o outfiles/${est}/${name}.out -e errfiles/${est}/${name}.err
      # sleep .5
    done
  done
done

