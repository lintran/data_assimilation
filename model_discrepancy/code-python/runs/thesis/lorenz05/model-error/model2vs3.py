
# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("model_type", action = "store", type = str)
parser.add_argument("dt", action = "store", type = int)

# optional arguments
parser.add_argument("-obs", action = "store", default = "identity-other-480")
parser.add_argument("-Treal", action = "store", type = int, default = (365*2)*24)
parser.add_argument("-Tcheck", action = "store", type = int, default = (365*3/2)*24)
parser.add_argument("-seed", action = "store", type = int, default = 12345)
parser.add_argument("-M", action = "store", type = int, default = 200)
parser.add_argument("-ncpus", action = "store", type = int, default = None)
parser.add_argument("-inflation", action = "store", type = float, default = 0)

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["estL020", "36"])
# args = parser.parse_args(["plugin-estL000", "36"])
print args

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz05 import *

# TRUE MODEL (DATA GENERATED EVERY 6 HOURS) ----------------------------------------
# kinda match up with dart
args.dartfolder = "default-%s/dt06/perfect" % ("identity-all-960") #args.obs
DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_04/"
inits = loadmat(DARTdir + "work/inits.mat")
obs = loadmat(DARTdir + args.dartfolder + "/obs.mat")
truth = netcdf.netcdf_file(DARTdir + args.dartfolder + "/True_State.nc", "r")

parms = dict(N = 960, K = 32, I = 12, F = 15, b = 10, c = 2.5, model_number = 3, dt = .001)
locx = truth.variables["loc1d"][:].copy()
truth.close()

# forward model
fm = forward_model(adv_model_rk, parms, deriv, fparms = calc_consts, n_cpus = args.ncpus)
fm.parms = calc_consts(fm.parms)

# observation model
import re
obstype1 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\2", args.dartfolder)
obstype2 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\3", args.dartfolder)
Sigma_y = covmat(sigma = 1., N = obs["obs"].shape[0])

if obstype1 == "identity":
  H = diag([1]*parms["N"])
  if obstype2 == "other": H = H[arange(0,parms["N"],parms["N"]/obs["obs"].shape[0]),:]
  om = measurement_model(Sigma = Sigma_y, H = H)
elif obstype1 == "linear":
  H = zeros((obs["obs"].shape[0], parms["N"]))
  for i in range(obs["obs"].shape[0]):
    idx_less = where(locx < obs["loc"][i])[0][-1]
    H[i,idx_less] = (obs["loc"][i] - locx[idx_less])
    H[i,idx_less+1] = (locx[idx_less+1] - obs["loc"][i])
  H /= (1./parms["N"])
  om = measurement_model(Sigma = Sigma_y, H = H)
elif obstype1 == "integral":
  om = \
      measurement_model(Sigma = Sigma_y,
          loc_x = locx,
          loc_y = obs["loc"],
          halfwidth = obs["halfwidth"][0],
          num_points = obs["num_points"][0])

# hmm
x0 = inits["perfect"]
tx = arange(0, args.Treal*parms["dt"], parms["dt"]*6)
l05hmm = hmm(fm, om, x0, tx, seed_y = args.seed)

# initial conditions
# ics = init_cond(M = args.M, samples = inits["perturbed"][range(0,960,960/parms["N"]),:], seed = args.saveidx)
# ics.ens = ics.samples[:,range(args.M)]
ics = init_cond(M = args.M, mean = x0, Sigma = covmat(sigma = 1, N = parms["N"]))

# WRONG MODEL ----------------------------------------------------------------------
# get data every dt
idx = range(0, tx.size, args.dt/6)
x = l05hmm.x
y = l05hmm.y
l05hmm = hmm(fm, om, x0, tx[idx], x = x[idx,:], y = y[idx,:])

# localization
distvec = locx
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T
maxdist = distvec.max()

dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/"

print fm.parms

if args.model_type != "right": # wrong model
  fm.parms["model_number"] = 2

  if args.model_type[:10] == "plugin-svd": # plug in svd estimated from truth
    f = "%s/truth/svd%s.mat" % (dr, args.model_type[-3:])
    print f
    linear = loadmat(f)
    fm.deriv = deriv_plus_Lb_old
    fm.parms["L"] = linear["L"]
    fm.parms["Fplus"] = linear["b"]

  elif args.model_type != "wrong": # linear correction
    fm.deriv = deriv_plus_Lb

    if args.model_type[:11] == "plugin-optL": # plug in parameters estimated from truth
      fm.deriv = deriv_plus_L
      f = "%s/truth/optL%s.mat" % (dr, args.model_type[-7:])
      linear = loadmat(f)
      fm.parms["c_L_fft"] = linear["c.L.fft"]
      # fm.parms["Fplus"] = linear["b"]

    elif args.model_type[:10] == "plugin-est": # plug in parameters estimated from batch mode
      import re
      temp = re.sub("plugin\\-(.*)", '\\1', args.model_type)
      f = "%s/est/%s-dt%03i-M200-B025-%05i.mat" % (dr, temp, args.dt, args.seed)
      linear = loadmat(f)
      param = median(linear["records"][0,0][1][-1,:,:], axis = 1)

      n = int(re.sub("estL([\d]{3}).*", '\\1', temp))
      q_specified = re.search("q", temp) is not None
      if q_specified:
        if n == 0:
          cutoffDX = param[1]
          cutoffXX = param[2]
          qDX = int(re.sub("estL[\d]{3}q([\d]{1})[\d]{1}", '\\1', temp))
          qXX = int(re.sub("estL[\d]{3}q[\d]{1}([\d]{1})", '\\1', temp))
          tauDX = 2*(qDX+1)
          tauXX = 2*(qXX+1)
          periodDX = 0
          periodXX = 0
        else:
          cutoffDX = param[1]
          periodDX = param[2]
          cutoffXX = param[3]
          periodXX = param[4]
          qDX = int(re.sub("estL[\d]{3}q([\d]{1})[\d]{1}", '\\1', temp))
          qXX = int(re.sub("estL[\d]{3}q[\d]{1}([\d]{1})", '\\1', temp))
          tauDX = param[2]
          tauXX = 2*(qXX+1)
      else:
        if n == 0:
          cutoffDX = param[1]
          qDX = param[2]
          cutoffXX = param[3]
          qXX = param[4]
          tauDX = 2*(qDX+1)
          tauXX = 2*(qXX+1)
          periodDX = 0
          periodXX = 0
        else:
          cutoffDX = param[1]
          qDX = param[2]
          periodDX = param[3]
          cutoffXX = param[4]
          qXX = param[5]
          periodXX = param[6]
          tauDX = 2*(qDX+1)
          tauXX = 2*(qXX+1)

      parms_add = dict(multiplier = param[0], #mu_d = 0, mu_x = zeros(parms["N"]),
                       distvec = distvec, n_wavelengths = n,
                       cutoff_dx = cutoffDX, tau_dx = 2*(qDX+1), period_dx = 0, q_dx = qDX,
                       cutoff_xx = cutoffXX, tau_xx = 2*(qXX+1), period_xx = 0, q_xx = qXX)
      fm.parms.update(parms_add)
      fm.parms = calc_L(fm.parms)
      fm.fparms = calc_L
      fm.deriv = deriv_plus_L

    elif args.model_type[:4] == "estL": # estimate parameters in an online fashion
      fm.deriv = deriv_plus_L

      n = float(args.model_type[4:7])
      q_specified = args.model_type[7:8] == "q"
      qDX = float(args.model_type[8:9]) if q_specified else 0
      qXX = float(args.model_type[9:10]) if q_specified else 0

      parms_add = dict(multiplier = 0, #mu_d = 0, mu_x = zeros(parms["N"]),
                       distvec = distvec, n_wavelengths = n,
                       cutoff_dx = distvec[1], tau_dx = 2*(qDX+1), period_dx = 0, q_dx = qDX,
                       cutoff_xx = distvec[1], tau_xx = 2*(qXX+1), period_xx = 0, q_xx = qXX)
      fm.parms.update(parms_add)
      fm.parms = calc_L(fm.parms)
      fm.fparms = calc_L

      param_names = array(["multiplier",
                     "cutoff_dx", "q_dx", "period_dx",
                     "cutoff_xx", "q_dx", "period_xx"])
      param_min   = array([  -50,
                               0, 0, 0,
                               0, 0, 0])
      param_max   = array([        50,
                              maxdist, 2, 1,
                              maxdist, 2, 1])
      param_int   = array([False]*param_names.size)
      param_sd    = array([        .1,
                           distvec[1], .1, .01,
                           distvec[1], .1, .01])

      if parms_add["n_wavelengths"] == 0:
        idx = where(array([x[:6] for x in param_names]) != "period")[0]
        param_names = param_names[idx]
        param_min = param_min[idx]
        param_max = param_max[idx]
        param_int = param_int[idx]
        param_sd = param_sd[idx]

      if q_specified: # don't estimate q
        idx = where(array([x[:1] for x in param_names]) != "q")[0]
        param_names = param_names[idx]
        param_min = param_min[idx]
        param_max = param_max[idx]
        param_int = param_int[idx]
        param_sd = param_sd[idx]
      else:
        idx = where(array([x[:1] for x in param_names]) == "q")[0]
        param_int[idx] = True

print fm.parms

# FILTER INITIALIZATION --------------------------------------------------
# initialize EnKF
l05enkf = enkf(l05hmm, ics,
               update_type = "etkf",
               inflation_factor = args.inflation,
               inflate_forecast = True,
               localization_hw = 5/960.,
               localization_distmat = distmat)

# RUN FILTER AND SAVE -----------------------------------------------
if args.inflation != 0:
  l05enkf.run_filter(True)
else:
  fm.parms["inflation"] = 1.
  if args.model_type[:3] != "est":
    def calc_params(parms): return parms
    fm.fparms = calc_params
    param_names = array(["inflation"])
    param_min   = array([1])
    param_max   = array([20])
    param_sd    = array([.6])
    param_int   = array([False])
  else:
    param_names = append(param_names, "inflation")
    param_min   = append(param_min, 1)
    param_max   = append(param_max, 20)
    param_sd    = append(param_sd,  .6)
    param_int   = append(param_int, False)

  print "names", param_names
  print "min", param_min
  print "max", param_max
  print "sd", param_sd
  param_names = dict(state = param_names)

  set_seed(args.seed)
  if2 = IteratedFilter2(
          1, l05enkf,
          filter_verbose = True,
          param_names = param_names,
          param_sd  = param_sd,
          param_min = param_min,
          param_max = param_max,
          param_int = param_int,
          param_inflate = "inflation",
          param_mean_analysis = "mu_x" if args.model_type[:5] == "estLb" else None,
          D_print = param_sd.size,
          loglik_ti0 = 0,
          update_type = "apf",
          # resample_type = "systematic",
          resample_type = "multinomial",
          tune_cov_eps = 1)

  from time import time
  begin = time()
  if2.run()
  end = time()
  print "Elapsed time: ", ((end - begin) / 60)

if False:
  l05enkf.plot1d(xrange(10,13), "forecast")
  plt.show()

ts = "%05i" % (args.seed)
dr += "plugin/"
# dr += args.obs + "/"
if not os.path.exists(dr): os.makedirs(dr)
filename = "%s/%s-dt%03i-M%03i-%s" % \
             (dr, args.model_type, args.dt, args.M, ts)

l05hmm.x_mean = l05hmm.x
tstart = where(l05hmm.ty > parms["dt"]*args.Tcheck)[0][0] # between 1 year and 1.5 years
rmse = array([l05enkf.rmse(ti, truth_type = "x", key = "forecast_old") for ti in range(tstart, l05hmm.ty.size)])
crps = array([l05enkf.crps(ti, truth_type = "x", key = "forecast_old") for ti in range(tstart, l05hmm.ty.size)])

from numpy import percentile
print "rmse", percentile(rmse, [10, 50, 90])
print "crps", percentile(crps, [10, 50, 90])
savemat(filename + ".mat", {'rmse': rmse, 'crps': crps})

#"""