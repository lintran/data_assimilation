
# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("dt", action = "store", type = int)

# optional arguments
parser.add_argument("-B", action = "store", type = int, default = 25)
parser.add_argument("-obs", action = "store", default = "identity-other-480")
parser.add_argument("-Treal", action = "store", type = int, default = (365*2)*24)
parser.add_argument("-Tcheck", action = "store", type = int, default = (365*3/2)*24)
parser.add_argument("-seed", action = "store", type = int, default = 12345)
parser.add_argument("-M", action = "store", type = int, default = 200)
parser.add_argument("-ncpus", action = "store", type = int, default = None)
parser.add_argument("-inflation", action = "store", type = float, default = 0)
parser.add_argument("-est", action = "store", type = int, default = 1)
parser.add_argument("-n", action = "store", type = int, default = 10)
parser.add_argument("-qXX", action = "store", type = int, default = 0)
parser.add_argument("-qDX", action = "store", type = int, default = 0)
parser.add_argument("-estq", action = "store_true", default = False)

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["36"])
print args

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz05 import *

# TRUE MODEL (DATA GENERATED EVERY 6 HOURS) ----------------------------------------
# kinda match up with dart
args.dartfolder = "default-%s/dt06/perfect" % ("identity-all-960") #args.obs
DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_04/"
inits = loadmat(DARTdir + "work/inits.mat")
obs = loadmat(DARTdir + args.dartfolder + "/obs.mat")
truth = netcdf.netcdf_file(DARTdir + args.dartfolder + "/True_State.nc", "r")

parms = dict(N = 960, K = 32, I = 12, F = 15, b = 10, c = 2.5, model_number = 3, dt = .001)
locx = truth.variables["loc1d"][:].copy()
truth.close()

# forward model
fm = forward_model(adv_model_rk, parms, deriv, fparms = calc_consts, n_cpus = args.ncpus)
fm.parms = calc_consts(fm.parms)

# observation model
import re
obstype1 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\2", args.dartfolder)
obstype2 = re.sub("(\w+)\-(\w+)\-(\w+)\-(\w+).*", "\\3", args.dartfolder)
Sigma_y = covmat(sigma = 1., N = obs["obs"].shape[0])

if obstype1 == "identity":
  H = diag([1]*parms["N"])
  if obstype2 == "other": H = H[arange(0,parms["N"],parms["N"]/obs["obs"].shape[0]),:]
  om = measurement_model(Sigma = Sigma_y, H = H)
elif obstype1 == "linear":
  H = zeros((obs["obs"].shape[0], parms["N"]))
  for i in range(obs["obs"].shape[0]):
    idx_less = where(locx < obs["loc"][i])[0][-1]
    H[i,idx_less] = (obs["loc"][i] - locx[idx_less])
    H[i,idx_less+1] = (locx[idx_less+1] - obs["loc"][i])
  H /= (1./parms["N"])
  om = measurement_model(Sigma = Sigma_y, H = H)
elif obstype1 == "integral":
  om = \
      measurement_model(Sigma = Sigma_y,
          loc_x = locx,
          loc_y = obs["loc"],
          halfwidth = obs["halfwidth"][0],
          num_points = obs["num_points"][0])

# hmm
x0 = inits["perfect"]
tx = arange(0, args.Treal*parms["dt"], parms["dt"]*6)
l05hmm = hmm(fm, om, x0, tx, seed_y = args.seed)

# initial conditions
# ics = init_cond(M = args.M, samples = inits["perturbed"][range(0,960,960/parms["N"]),:], seed = args.saveidx)
# ics.ens = ics.samples[:,range(args.M)]
ics = init_cond(M = args.M, mean = x0, Sigma = covmat(sigma = 1, N = parms["N"]))

# get data every dt
idx = where(tx <= parms["dt"]*args.Tcheck)[0] # between 1 year and 1.5 years
idx = idx[range(0, idx.size, args.dt/6)]
x = l05hmm.x
y = l05hmm.y
l05hmm = hmm(fm, om, x0, tx[idx], x = x[idx,:], y = y[idx,:])

# localization
distvec = locx
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T
maxdist = distvec.max()

# FILTER INITIALIZATION --------------------------------------------------
# initialize EnKF
l05enkf = enkf(l05hmm, ics,
               update_type = "etkf",
               inflation_factor = args.inflation,
               inflate_forecast = True,
               localization_hw = 5/960.,
               localization_distmat = distmat)

# PARAMETER INITIALIZATION -----------------------------------------------
parms_wrong = parms
parms_wrong = dict(N = parms["N"], K = parms["K"], F = parms["F"],
                   model_number = 2, dt = parms["dt"],
                   multiplier = 0, #mu_d = 0, mu_x = zeros(parms["N"]),
                   distvec = distvec, n_wavelengths = args.n,
                   cutoff_dx = distvec[1], tau_dx = 2*(args.qDX+1), period_dx = 0, q_dx = args.qDX,
                   cutoff_xx = distvec[1], tau_xx = 2*(args.qXX+1), period_xx = 0, q_xx = args.qXX)
parms_wrong2 = calc_consts(parms_wrong)
parms_wrong2 = calc_L(parms_wrong2)
fm.parms = parms_wrong2
fm.fparms = calc_L
fm.deriv = deriv_plus_L

param_names = array(["multiplier",
               "cutoff_dx", "q_dx", "period_dx",
               "cutoff_xx", "q_dx", "period_xx"])
param_min   = array([  -50,
                         0, 0, 0,
                         0, 0, 0])
param_max   = array([        50,
                        maxdist, 2, 1,
                        maxdist, 2, 1])
param_int   = array([False]*param_names.size)
param_sd    = array([        .1,
                     distvec[1], .1, .01,
                     distvec[1], .1, .01])

if parms_wrong2["n_wavelengths"] == 0:
  idx = where(array([x[:6] for x in param_names]) != "period")[0]
  param_names = param_names[idx]
  param_min = param_min[idx]
  param_max = param_max[idx]
  param_int = param_int[idx]
  param_sd = param_sd[idx]

if not args.estq: # don't estimate q
  idx = where(array([x[:1] for x in param_names]) != "q")[0]
  param_names = param_names[idx]
  param_min = param_min[idx]
  param_max = param_max[idx]
  param_int = param_int[idx]
  param_sd = param_sd[idx]
else:
  idx = where(array([x[:1] for x in param_names]) == "q")[0]
  param_int[idx] = True

if args.inflation == 0:
  fm.parms["inflation"] = 1.
  param_names = append(param_names, "inflation")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, 20)
  param_sd    = append(param_sd,  .6)
  param_int   = append(param_int, False)

param_names = dict(state = param_names)

# RUN PARAMETER ESTIMATION ---------------------------------------------
set_seed(args.seed)
if2 = IteratedFilter2(
        args.B, l05enkf,
        filter_verbose = True,
        param_names = param_names,
        param_sd  = param_sd,
        param_min = param_min,
        param_max = param_max,
        param_int = param_int,
        param_inflate = "inflation",
        # param_mean_analysis = "mu_x",
        # param_mean_diff = "mu_d",
        D_print = param_sd.size,
        loglik_ti0 = 0,
        update_type = "apf",
        # resample_type = "systematic",
        resample_type = "multinomial",
        tune_cov_eps = 1)

from time import time
begin = time()
if2.run()
# if2.sample()
end = time()
print "Elapsed time: ", ((end - begin) / 60)

if False:
  l05enkf.plot1d(xrange(3), "forecast")
  plt.show()

ts = "%05i" % (args.seed)
dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/"
dr += "est/"
# dr += args.obs + "/"
if not os.path.exists(dr): os.makedirs(dr)
filename = "%s/estL%03i%s-dt%03i-M%03i-B%03i-%s" % \
             (dr, args.n, ("q%i%i" % (args.qDX, args.qXX)) if not args.estq else "", args.dt, args.M, args.B, ts)
if2.savemat(filename + ".mat")





