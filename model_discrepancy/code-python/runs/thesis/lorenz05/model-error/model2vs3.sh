#!/bin/bash
ncpus=2
high=-1

python_ncpus=""
qsub_ncpus=""
if [ "${ncpus}" -gt "1" ]; then
  python_ncpus="-ncpus ${ncpus}"
  qsub_ncpus="-pe smp ${ncpus}"
fi

mkdir -p outfiles/plugin

for model_type in "plugin-estL000" "plugin-estL010"
# "plugin-svd095" "plugin-svd100"
# "plugin-optL000q002" "plugin-optL000q200" "plugin-optL000q202" "plugin-optL000q220"
# "estL000" "estL010" "estL020" "estL100"
# "estL000q00" "estL010q00" "estL020q00" "estL100q00"
# "estL000q20" "estL010q20" "estL020q20" "estL100q20"
# "estL000q22" "estL010q22" "estL020q22" "estL100q22"
# "plugin-estL000" "plugin-estL000q00" "plugin-estL010"
do
  for dt in $(echo `seq 6 6 24` `seq 36 12 48` `seq 72 24 120`)
  do
    cmd="python model2vs3.py ${model_type} ${dt} ${python_ncpus}"
    name=$(printf ${model_type}-dt%03i-M%03i ${dt} 200)
    qsub_command="qsub ${qsub_ncpus} -j y" #"-j y -js 1"
    # if [ "$i" -le "${high}" ]; then qsub_command="${qsub_command} -q high.q"; fi
    if [ -e "outfiles/plugin/${name}.out" ]; then rm outfiles/plugin/${name}.out; fi
    echo "export OMP_NUM_THREADS=${ncpus}; ${cmd}" | ${qsub_command} -o outfiles/plugin/${name}.out -e outfiles/plugin/${name}.err -N ${name}
    sleep .5
  done
done
