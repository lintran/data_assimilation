#!/bin/bash
ncpus=2
high=-1

python_ncpus=""
qsub_ncpus=""
if [ "${ncpus}" -gt "1" ]; then
  python_ncpus="-ncpus ${ncpus}"
  qsub_ncpus="-pe smp ${ncpus}"
fi

mkdir -p outfiles/est

for dt in $(echo `seq 6 6 24` `seq 36 12 48` `seq 72 24 120`)
do
  for n in 0 10 20 100
  do
    qsub_command="qsub ${qsub_ncpus} -j y" #"-j y -js 1"

    cmd="python est-param.py ${dt} ${python_ncpus} -n ${n} -estq"
    name=$(printf estL%03i-dt%03i-M%03i-B%03i ${n} ${dt} 200 25)
    # if [ "$i" -le "${high}" ]; then qsub_command="${qsub_command} -q high.q"; fi
    if [ -e "outfiles/est/${name}.out" ]; then rm outfiles/est/${name}.out; fi
    echo "export OMP_NUM_THREADS=${ncpus}; ${cmd}" | ${qsub_command} -o outfiles/est/${name}.out -e outfiles/est/${name}.err -N ${name}
    sleep .5

    cmd="python est-param.py ${dt} ${python_ncpus} -n ${n}"
    name=$(printf estL%03iq00-dt%03i-M%03i-B%03i ${n} ${dt} 200 25)
    # if [ "$i" -le "${high}" ]; then qsub_command="${qsub_command} -q high.q"; fi
    if [ -e "outfiles/est/${name}.out" ]; then rm outfiles/est/${name}.out; fi
    echo "export OMP_NUM_THREADS=${ncpus}; ${cmd}" | ${qsub_command} -o outfiles/est/${name}.out -e outfiles/est/${name}.err -N ${name}
    sleep .5
  done
done
