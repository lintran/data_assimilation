#!/bin/bash

mkdir -p outfiles

for alpha in 1.1 1.5 2 3 5 8
do
  for wrong_type in wrong wrongLb # wrongb
  do
    for dt in $(echo `seq 6 6 24` `seq 36 12 72` `seq 96 24 168`)
    do
      for i in 1
      do
        alpha100=$(printf "%.0f" $(echo "(${alpha}*100)" | bc -l))
        name=$(printf beta20-alpha%03i-${wrong_type}-dt%03i-%03i ${alpha100} ${dt} ${i})
        # echo $name
        if [ -e "outfiles/${name}.out" ]; then rm outfiles/${name}.out; fi
        if [ -e "errfiles/${name}.err" ]; then rm errfiles/${name}.err; fi
        echo "python plugin-true-linear.py ${alpha} ${wrong_type} ${dt} -seed $i" | qsub -j y -N ${name} -o outfiles/${name}.out -e errfiles/${name}.err
        sleep .5
      done
    done
  done
done

alpha=1
wrong_type="right"
i=1
for dt in $(echo `seq 6 6 24` `seq 36 12 72` `seq 96 24 168`)
do
  alpha100=$(printf "%.0f" $(echo "(${alpha}*100)" | bc -l))
  name=$(printf beta20-alpha%03i-${wrong_type}-dt%03i-%03i ${alpha100} ${dt} ${i})
  # echo $name
  if [ -e "outfiles/${name}.out" ]; then rm outfiles/${name}.out; fi
  if [ -e "errfiles/${name}.err" ]; then rm errfiles/${name}.err; fi
  echo "python plugin-true-linear.py ${alpha} ${wrong_type} ${dt} -seed $i" | qsub -j y -N ${name} -o outfiles/${name}.out -e errfiles/${name}.err
  sleep .5
done

