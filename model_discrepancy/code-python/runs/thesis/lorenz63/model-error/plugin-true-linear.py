

# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("alpha", action = "store", type = float)
parser.add_argument("wrong_type", action = "store", type = str)
parser.add_argument("dt", action = "store", type = int)
# parser.add_argument("obs", action = "store", type = float)

# optional arguments
parser.add_argument("-beta", action = "store", type = float, default = 2)
parser.add_argument("-Treal", action = "store", type = int, default = (365*3)*24)
parser.add_argument("-Tcheck", action = "store", type = int, default = (365*2)*24)
parser.add_argument("-M", action = "store", default = 48, type = int)
parser.add_argument("-seed", action = "store", default = 999, type = int)
parser.add_argument("-inflation", action = "store", type = float, default = 0)
# parser.add_argument("-trainfolder", action = "store", type = str, default = "perfect")
# parser.add_argument("-lsode", action = "store_true", default = False)

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["1.1", "wrongLb", "24"])
print args

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz63 import *

# TRUE MODEL ----------------------------------------------------------------------
# match up with DART
DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_63/"
inits = loadmat(DARTdir + "work/inits.mat")
# obs = loadmat(DARTdir + "%s/obs%02i" % (args.trainfolder, args.obs) + "/obs.mat")
# truth = netcdf.netcdf_file(DARTdir + "%s/obs%02i" % (args.trainfolder, args.obs) + "/True_State.nc", "r")

# forward model
parms = dict(sigma = 10, rho = 28, beta = 8/3., alpha = 1, F = [0,0,0], dt = 0.01)
fm = forward_model(adv_model, parms, deriv_right)

# observation model
if False:
  H = array([[0,0,1],[0,0,0]], dtype = float)
  idx = array([floor(args.obs/10), ceil(args.obs/10)], dtype = int)
  idx[idx > 2] = 0
  H[1,idx] = 1
  if idx[0] != idx[1]: H[1,:] /= 2
  Sigma_y = covmat(sigma = 1, N = 2)
  om = measurement_model(Sigma = Sigma_y, H = H)
Sigma_y = covmat(sigma = 1, N = 3)
om = measurement_model(Sigma = Sigma_y)

# test to see if observation model matches up with DART
# a = dot(om.H, x.T)
# b = obs["truth"][:,:args.T]
# if not allclose(a,b): raise Exception("Observation model doesn't match up with DART's.")

# hmm
x0 = inits["perfect"]
tx = arange(0, args.Treal*parms["dt"], parms["dt"])
l63hmm = hmm(fm, om, x0, tx, seed_y = args.seed)
# T = args.T/args.forecast_lead_time if not args.Tnumber else args.T*args.forecast_lead_time
# tx = arange(0, T, parms["dt"])
# ty = arange(0, T, args.forecast_lead_time)
# l63hmm = hmm(fm, om, x0, ty, seed_y = args.seed)
# l63hmm.plot1d(range(3)); plt.show()

# initial conditions
ics = init_cond(M = args.M, mean = x0, Sigma = covmat(sigma = Sigma_y.sigma[0], N = 3))
# ics = init_cond(M = args.M, samples = inits["perturbed"])
# ics.ens = ics.samples[:,range(args.M)]

# WRONG MODEL ----------------------------------------------------------------------
# get data every dt
idx = range(0, tx.size, args.dt)
x = l63hmm.x
y = l63hmm.y
l63hmm = hmm(fm, om, x0, tx[idx], x = x[idx,:], y = y[idx,:])

dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/thesis/lorenz63/model-error/"

if args.wrong_type != "right":
  fm.deriv = deriv_wrong_linearcorrection
  # fm.parms["sigma"] = 12
  # fm.parms["rho"] = 25
  fm.parms["beta"] = args.beta
  fm.parms["alpha"] = args.alpha

  f = "%s/true-linear-beta%02i-alpha%03i.mat" % (dr, args.beta*10, args.alpha*100)
  linear = loadmat(f)

  if args.wrong_type == "wrong":
    fm.parms["L"] = zeros((3,3))
    fm.parms["b"] = zeros((3,1))

  elif args.wrong_type == "wrongb":
    fm.parms["L"] = zeros((3,3))
    fm.parms["b"] = linear["b0"]

  elif args.wrong_type == "wrongLb":
    fm.parms["L"] = linear["L"]
    fm.parms["b"] = linear["b"]


# INITIALIZE FILTER ----------------------------------------------------------------
# if not args.lsode: fm.adv_fn = adv_model_rk

# localization
distvec = arange(0, 3) / 3.
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T

# initialize EnKF
l63enkf = enkf(l63hmm, ics,
               # inflateperturb_obs = True,
               update_type = "etkf",
               localization_hw = 0.00001,
               inflation_factor = args.inflation,
               inflate_forecast = True,
               localization_distmat = distmat)

if args.inflation != 0:
  l63enkf.run_filter(True)
else:
  def calc_params(parms): return parms

  fm.parms["inflation"] = 1.
  param_names = array(["inflation"])
  param_min   = array([1])
  param_max   = array([5])
  param_sd    = array([.2])
  param_int   = array([False])
  param_names = dict(state = param_names)

  set_seed(args.seed)
  if2 = IteratedFilter2(
          1, l63enkf,
          filter_verbose = True,
          param_names = param_names,
          param_sd  = param_sd,
          param_min = param_min,
          param_max = param_max,
          param_int = param_int,
          param_inflate = "inflation",
          D_print = param_sd.size,
          loglik_ti0 = 0,
          update_type = "apf",
          # resample_type = "systematic",
          resample_type = "multinomial",
          tune_cov_eps = 1)
  if2.run()

if False:
  l63enkf.plot1d(xrange(3), "forecast"); plt.show()

ts = "%03i" % (args.seed)
# dr += args.obs + "/"
if not os.path.exists(dr): os.makedirs(dr)
filename = "%s/true-plugin-beta%02i-alpha%03i-%s-dt%03i-%s" % \
             (dr, args.beta*10, args.alpha*100, args.wrong_type, args.dt, ts)

l63hmm.x_mean = l63hmm.x
tstart = where(l63hmm.ty > parms["dt"]*args.Tcheck)[0][0] # between 1 year and 1.5 years
rmse = array([l63enkf.rmse(ti, truth_type = "x", key = "forecast_old") for ti in range(tstart, l63hmm.ty.size)])
crps = array([l63enkf.crps(ti, truth_type = "x", key = "forecast_old") for ti in range(tstart, l63hmm.ty.size)])

# diff = l63enkf.ensemble.mean["forecast"][tstart:,:] - l63hmm.x[tstart:,:]
# rmse_x = sqrt(mean((diff)**2))

# diff = dot(om.H, l63enkf.ensemble.mean["forecast"][tstart:,:].T) - dot(om.H, l63hmm.x[tstart:,:].T)
# rmse_y = sqrt(mean((diff)**2))

# idx = om.H.sum(axis=0) == 0
# diff = l63enkf.ensemble.mean["forecast"][tstart:,idx] - l63hmm.x[tstart:,idx]
# rmse_x_notobs = sqrt(mean((diff)**2))

# idx = logical_not(idx)
# diff = l63enkf.ensemble.mean["forecast"][tstart:,idx] - l63hmm.x[tstart:,idx]
# rmse_x_obs = sqrt(mean((diff)**2))

# sd_mean = l63enkf.ensemble.sd["forecast"][tstart:,].mean()

# loglik_sum = l63enkf.sum_loglik(ti0 = tstart)
# loglik_mean = l63enkf.loglik_t["filter"][tstart:].mean()

# out = [rmse_x, rmse_y, rmse_x_obs, rmse_x_notobs, sd_mean, loglik_sum, loglik_mean]
from numpy import percentile
print "rmse", percentile(rmse, [10, 50, 90])
print "crps", percentile(crps, [10, 50, 90])
savemat(filename, {'rmse': rmse, 'crps': crps})


