

# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# optional arguments
parser.add_argument("-perfect_folder", action = "store", type = str, default = "perfect")
parser.add_argument("-T", action = "store", type = str, default = "600")
parser.add_argument("-dt", action = "store", type = int, default = 19)
parser.add_argument("-cluster", action = "store_true", default = False)
parser.add_argument("-overwrite_existing", action = "store_true", default = False)

args = parser.parse_args()
# args = parser.parse_args([""])
print args


import os
import shutil
import subprocess
import numpy as np
import re
from math import floor

path = os.path.expanduser("~") + "/data_assimilation/DART/Kodiak/models/lorenz_63"

obsfiles = []
for f in os.listdir(path + "/obs"):
  if re.match(".*\.out", f) and re.match(".*one.*", f) is None: obsfiles.append(re.sub("\.out", "", f))

for obs in obsfiles:
  # change directory
  folder = "%s/%s/%s" % (path, args.perfect_folder, obs)
  if os.path.exists(folder):
    if args.overwrite_existing: shutil.rmtree(folder)
    else:                       continue
  os.makedirs(folder)
  os.chdir(folder)

  # copy symlinks to dart program
  folder = path + "/work"
  for f in ["create_fixed_network_seq", "perfect_model_obs"]:
    os.symlink("%s/%s" % (folder, f), f)

  # copy perfect_ics
  os.symlink("%s/%s/perfect_ics" % (path, args.perfect_folder), "perfect_ics")

  # copy namelist
  shutil.copyfile("%s/input/perfect.nml" % path, "input.nml")

  # copy empty obs
  shutil.copyfile(path + "/obs/%s.out" % obs, obs + ".out")

  # generate empty obs time sequence
  day = args.dt/24
  seconds = (args.dt - day*24)*3600
  userinput  = obs + ".out\n"                       # in file
  userinput += "1\n"                                # regularly repeating time sequence
  userinput += args.T + "\n"                         # number of observation times
  userinput += "0 0\n"                              # initial time in days and seconds
  userinput += str(day) + " " + str(seconds) + "\n" # period of obs in days and seconds
  userinput += "obs_seq.in\n"                       # out file

  p = subprocess.Popen(["./create_fixed_network_seq"],
                       stdin=subprocess.PIPE, stdout=subprocess.PIPE)
  stdout, stderr = p.communicate(input=userinput)
  print stdout
  print stderr

  # run data-generating process
  systemcall = "./perfect_model_obs" if not args.cluster else "echo './perfect_model_obs' | qsub -N perfect-%s-%s-dt%02i" % (param_type, obs_type, dt)
  os.system(systemcall)





