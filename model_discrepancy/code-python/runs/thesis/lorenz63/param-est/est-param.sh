#!/bin/bash
high=5
outfolder="rk"

mkdir -p outfiles/${outfolder}
# mkdir -p errfiles/${outfolder}

for i in $(seq 1 30)
do
  for obs in 25 0 5 10 15 #$(seq 0 5 25)
  do
    for B in 50 #100 25
    do
      for M in 96 #6 12 24 48 96
      do
        for update_type in "etkf" "apf"
        do
          cmd="python est-param.py ${update_type} ${obs}\
                -M $M -B $B \
                -save -saveidx $i"
          qsub_command="qsub -j y" # -js 1"
          if [ "$i" -le "${high}" ]; then qsub_command="${qsub_command} -q high.q"; fi
          # if [ "${M}" -lt 90 ]; then qsub_command="${qsub_command} -js 1"; fi
          # if [ "$i" -le "${high}" ] && [ "${M}" -lt 90 ]; then qsub_command="${qsub_command} -q high.q"; fi

          # without inflation
          # name=$(printf obs%02i-%s-inf10-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          # if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          # if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          # echo "${cmd}" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          # sleep .5

          # with adaptive inflation
          name=$(printf obs%02i-%s-infmax20sd60-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          echo "${cmd} -inflation" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          sleep .5

          name=$(printf obs%02i-%s-infmax20sd20-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          echo "${cmd} -inflation -inflation_sd .2" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          sleep .5

          name=$(printf obs%02i-%s-infmax05sd20-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          echo "${cmd} -inflation -inflation_sd .2 -inflation_max 5" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          sleep .5

          name=$(printf obs%02i-%s-infmax05sd60-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          echo "${cmd} -inflation -inflation_sd .6 -inflation_max 5" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          sleep .5

          name=$(printf obs%02i-%s-infmax05sd10-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          echo "${cmd} -inflation -inflation_sd .1 -inflation_max 5" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          sleep .5

          # with fixed inflation
          # name=$(printf obs%02i-%s-inf15-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          # if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          # if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          # echo "${cmd} -inflation -inflation_value 1.5" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          # sleep .5

          # name=$(printf obs%02i-%s-inf20-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          # if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          # if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          # echo "${cmd} -inflation -inflation_value 2" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          # sleep .5

          # name=$(printf obs%02i-%s-inf25-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          # if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          # if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          # echo "${cmd} -inflation -inflation_value 2.5" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          # sleep .5

          # name=$(printf obs%02i-%s-inf30-B%03i-M%03i-%04i ${obs} ${update_type} $B $M $i)
          # if [ -e "outfiles/${outfolder}/${name}.out" ]; then rm outfiles/${outfolder}/${name}.out; fi
          # if [ -e "errfiles/${outfolder}/${name}.err" ]; then rm errfiles/${outfolder}/${name}.err; fi
          # echo "${cmd} -inflation -inflation_value 3" | ${qsub_command} -N ${name} -o outfiles/${outfolder}/${name}.out -e errfiles/${outfolder}/${name}.err
          # sleep .5
        done
      done
    done
  done
done
