

# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("update_type", action = "store", type = str)
parser.add_argument("obs", action = "store", type = float)

# optional arguments
parser.add_argument("-T", action = "store", type = int, default = 300)
parser.add_argument("-M", action = "store", default = 10, type = int)
parser.add_argument("-B", action = "store", default = 50, type = int)
parser.add_argument("-save", action = "store_true", default = False)
parser.add_argument("-savename", action = "store", default = "", type = str)
parser.add_argument("-saveidx", action = "store", default = None, type = int)
parser.add_argument("-inflation", action = "store_true", default = False)
parser.add_argument("-inflation_max", action = "store", type = float, default = 20)
parser.add_argument("-inflation_sd", action = "store", type = float, default = .6)
parser.add_argument("-param", action = "store", type = str, default = "sigma")
parser.add_argument("-trainfolder", action = "store", type = str, default = "perfect")
parser.add_argument("-lsode", action = "store_true", default = False)
parser.add_argument("-inflation_value", action = "store", type = float, default = None)
parser.add_argument("-inflate_forecast", action = "store_true", default = True)

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["apf", "0", "-M", "48", "-B", "50", "-saveidx", "1", "-inflation_sd", ".6", "-inflation_max", "100", "-inflation"])#, "-inflation_value", "2"])
print args

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz63 import *

# TRUE MODEL ----------------------------------------------------------------------
# match up with DART
DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_63/"
inits = loadmat(DARTdir + "work/inits.mat")
obs = loadmat(DARTdir + "%s/obs%02i" % (args.trainfolder, args.obs) + "/obs.mat")
truth = netcdf.netcdf_file(DARTdir + "%s/obs%02i" % (args.trainfolder, args.obs) + "/True_State.nc", "r")

if args.T is None: args.T = truth.variables["state"].shape[0]
x = truth.variables["state"][:args.T,0,:].copy()
y = obs["obs"][:,:args.T].T #if args.saveidx is None else None

dt_convert = dict(days = 0, seconds = 3600)
ty_real = truth.variables["time"][:args.T].copy()
ty = truth.model_deltat.copy()*(24*60*60*ty_real / (24*60*60*dt_convert["days"] + dt_convert["seconds"]))

truth.close()

# forward model
parms = dict(sigma = 10, rho = 28, beta = 8/3., alpha = 1, F = [0,0,0], dt = truth.model_deltat.copy())
fm = forward_model(adv_model, parms, deriv_right)

# observation model
H = array([[0,0,1],[0,0,0]], dtype = float)
idx = array([floor(args.obs/10), ceil(args.obs/10)], dtype = int)
idx[idx > 2] = 0
H[1,idx] = 1
if idx[0] != idx[1]: H[1,:] /= 2
Sigma_y = covmat(sigma = 1, N = 2)
om = measurement_model(Sigma = Sigma_y, H = H)

# test to see if observation model matches up with DART
a = dot(om.H, x.T)
b = obs["truth"][:,:args.T]
if not allclose(a,b): raise Exception("Observation model doesn't match up with DART's.")

# hmm
x0 = inits["perfect"]
l63hmm = hmm(fm, om, x0, ty, x = x, y = y)
# T = args.T/args.forecast_lead_time if not args.Tnumber else args.T*args.forecast_lead_time
# tx = arange(0, T, parms["dt"])
# ty = arange(0, T, args.forecast_lead_time)
# l63hmm = hmm(fm, om, x0, ty, seed_y = args.saveidx)
# l63hmm.plot1d(range(3)); plt.show()

# initial conditions
ics = init_cond(M = args.M, mean = x0, Sigma = covmat(sigma = Sigma_y.sigma[0], N = 3))
# ics = init_cond(M = args.M, samples = inits["perturbed"])
# ics.ens = ics.samples[:,range(args.M)]

# INITIALIZE FILTER ----------------------------------------------------------------
if not args.lsode: fm.adv_fn = adv_model_rk

# localization
distvec = arange(0, 3) / 3.
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T

# initialize EnKF
l63enkf = enkf(l63hmm, ics,
               # inflateperturb_obs = True,
               update_type = "etkf",
               localization_hw = 0.00001,
               # localization_hw = 0.2,
               inflation_factor = args.inflation_value if args.inflation and args.inflation_value is not None else 1.,
               inflate_forecast = True,
               localization_distmat = distmat)
if False:
  l63enkf.run_filter()
  l63enkf.plot1d(xrange(3), "forecast"); plt.show()

  l63grpf = pkf(l63hmm, ics, pre_reg_type = "sample_diag", reg_C = 1, resample_ess = True)
  l63grpf.run_filter()
  l63grpf.plot1d(xrange(3), "forecast"); plt.show()

# PARAMETER ESTIMATION --------------------------------------------------------------
print "parms right", fm.parms

# initialize parameter constraints, etc
param_names = array([args.param])
param_min   = array([0])
param_max   = array([50])
param_sd    = array([.25])

set_seed(args.saveidx)
fm.parms[args.param] += normal.rvs(scale = 2*param_sd[-1])
# set_seed(None)

param_names = dict(state = param_names)
if not args.inflate_forecast: param_names["obs"] = []

if args.inflation and args.inflation_value is None:
  key = "state" if args.inflate_forecast else "obs"
  if args.inflate_forecast: fm.parms["inflation"] = 1
  param_names[key] = append(param_names[key], "inflation")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, args.inflation_max)
  param_sd    = append(param_sd, args.inflation_sd)
  # fm.parms["inflation"] = [1.]*3
  # param_names = append(param_names, "inflation")
  # param_min   = append(param_min, [.9,1,.9])#[.9]*3)
  # param_max   = append(param_max, [20,1,20])#[20]*3)
  # param_sd    = append(param_sd, [1,0,1])#[1]*3)

print "parms init", fm.parms
# set_seed(None)
print "names", param_names
print "min", param_min
print "max", param_max
print "sd", param_sd

# run parameter estimation
if2 = IteratedFilter2(
        args.B, l63enkf,

        param_names = param_names,
        param_sd  = param_sd,
        param_min = param_min,
        param_max = param_max,
        param_inflate = "inflation" if args.inflation and args.inflation_value is None else None,
        loglik_ti0 = 0,

        # filter_verbose = True,
        update_type = args.update_type,
        resample_type = "multinomial")
#if2.sample()

from time import time
begin = time()
if2.run()
end = time()
print "Elapsed time: ", ((end-begin)/60)

if False:
  if2.pftrace("state", args.param); plt.show()

  param_est = median(if2.records["param"][-1,:,:], axis=1)
  fm.parms[args.param] = param_est[0]
  # fm.parms[args.param] = 10
  l63enkf = enkf(l63hmm, ics, update_type = "etkf",
                 inflation_factor = param_est[1],
                 localization_hw = 0.00001,
                 localization_distmat = distmat)#param_est[1] if args.inflation else 1)
  l63enkf.run_filter()
  l63enkf.plot1d(xrange(3), "forecast"); plt.show()

if args.save:
  if not args.inflation:             infcode = "100000"
  elif args.inflation_value is None: infcode = "00%02i%02i" % (args.inflation_max, args.inflation_sd*100)
  else:                              infcode = "%02i0000" % (args.inflation_value * 10)

  if args.saveidx is None:
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d-%H-%M-%S")
  else:
    ts = "%04i" % args.saveidx
  if args.savename != "": ts = args.savename + "-" + ts
  dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/thesis/lorenz63/param-est/avg-"
  dr += "lsode/" if args.lsode else "rk/"
  if not os.path.exists(dr): os.makedirs(dr)

  filename = "%s/obs%02i-%s-inf%s-M%03i-B%03i-%s" % (dr, args.obs, args.update_type, infcode, args.M, args.B, ts)

  # import cPickle
  # f = file(filename + ".pyobj", "wb")
  # l63enkf.hmm.forward_model = None
  # cPickle.dump(if2, f, protocol = cPickle.HIGHEST_PROTOCOL)
  # f.close()

  args = vars(args)
  args = {key: value if value is not None else [] for key, value in args.iteritems()}
  if2.savemat(filename + ".mat", dict_add = dict(args = args, time = (end - begin) / 60))



