

# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# positional arguments
parser.add_argument("update_type", action = "store", type = str)

# optional arguments
parser.add_argument("-M", action = "store", nargs = "+", type = int, default = [4,5,6,12,24])
parser.add_argument("-B", action = "store", default = 50, type = int)
parser.add_argument("-inflation", action = "store_true", default = False)
parser.add_argument("-param", action = "store", type = str, default = "sigma")
parser.add_argument("-cluster", action = "store_true", default = False)
parser.add_argument("-overwrite_existing", action = "store_true", default = False)

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["etkf", "-cluster", "-overwrite_existing"])
# args = parser.parse_args(["truth", "-cluster", "-overwrite_existing"])
# args = parser.parse_args(["apf", "-cluster", "-overwrite_existing"])
print args

# LOAD LIBRARIES -------------------------------------------------------------------
import os, sys
import numpy as np
import re
from scipy.io import loadmat

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm.DART import *

# SET FOLDERS ----------------------------------------------------------------------
dart_path = os.path.expanduser("~") + "/data_assimilation/DART/Kodiak/models/lorenz_63"
param_path = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/lorenz63/thesis/param-est/"

param_folder = args.update_type + "/" if args.update_type != "truth" else "etkf/"
param_folder += "inf/" if args.inflation else "noinf/"

# GET MLEs -------------------------------------------------------------------------
files = []
for f in os.listdir(param_path + param_folder):
  if re.search(".mat", f): files.append(re.sub("\.mat", "", f))
files = np.array(files)

# subset files
pattern = "obs([\d]+)\-B([\d]+)\-M([\d]+)\-([\d]+)"
runinfo = dict(B = np.array([int(re.sub(pattern, "\\2", f)) for f in files]),
               M = np.array([int(re.sub(pattern, "\\3", f)) for f in files]),
               obs = np.array([float(re.sub(pattern, "\\1", f)) for f in files]))
keep = np.logical_and(runinfo["B"] == args.B, np.in1d(runinfo["M"], args.M))
files = files[keep]
for key in runinfo.keys(): runinfo[key] = runinfo[key][keep]

# NAMELIST --------------------------------------------------------------------------
# get namelist and find lines that control parameter and ensemble size
input_files = []
names_input_files = ["noinf", "inf"]

with open("%s/input/perfect.nml" % dart_path) as f: input_files.append(f.readlines())
with open("%s/input/adinfvarying.nml" % dart_path) as f: input_files.append(f.readlines())

param_line = []
ens_line = []
for i in range(len(input_files[0])):
  if re.match(".*%s.*" % args.param, input_files[0][i]): param_line.append(i)
for i in range(len(input_files[0])):
  if re.match(".*ens_size.*", input_files[0][i]): ens_line.append(i)
param_line = param_line[0]
ens_line = ens_line[0]

if args.update_type == "truth":
  # PLUG IN TRUTH ---------------------------------------------------------------------
  for obs in np.unique(runinfo["obs"]):
    for M in np.unique(runinfo["M"]):
      for j in range(2):

        # copy namelist and adjust ensemble size
        namelist = list(input_files[j]) # copy namelist
        namelist[ens_line] = re.sub("[\d]+", "%i" % M, namelist[ens_line])

        # run enkf
        output_folder = "truth/obs%02i-M%03i/%s" % (obs, M, names_input_files[j])
        truth_folder = "perfect/obs%02i" % (obs)
        cluster_name = "truth-obs%02i-M%03i-%s" % (obs, M, names_input_files[j])
        DART_filter(dart_path, truth_folder, output_folder, namelist, args.overwrite_existing, args.cluster, cluster_name)
else:
  # PLUG IN MLE ------------------------------------------------------------------------
  for i in range(files.size):
    # get mle
    mle = loadmat(param_path + param_folder + "/" + files[i])
    param = mle["records"]["param"][0][0][-1,0,:].mean()

    for j in range(2):
      # copy namelist and adjust ensemble size and MLE
      namelist = list(input_files[j]) # copy namelist
      namelist[param_line] = re.sub("[\d\.]+", "%.15f" % param, namelist[param_line])
      namelist[ens_line] = re.sub("[\d]+", "%i" % runinfo["M"][i], namelist[ens_line])

      # run enkf
      output_folder = "%s/%s/%s" % (param_folder, files[i], names_input_files[j])
      truth_folder = "perfect/obs%02i" % (runinfo["obs"][i])
      cluster_name = "%s-%s-%s" % (args.update_type, files[i], names_input_files[j])
      DART_filter(dart_path, truth_folder, output_folder, namelist, args.overwrite_existing, args.cluster, cluster_name)








