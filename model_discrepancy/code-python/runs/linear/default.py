
from numpy import arange, array, dot
from numpy.random import uniform

# add module location to python path and import
import os
import sys
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.linear import *

def run_hmm(N, M, T,
            x0 = 0.,
            sigma_x = sqrt(.01),
            pool_parallel = None):

    x0 = array([x0] * N)
    ty = arange(0,T)

    Sigma_x = covmat(sigma = sigma_x, N = N) if sigma_x != 0 else None
    Sigma_y = covmat(sigma = 1, N = N)

    #parms = dict(F = eye(N), mean = normal(.5, .1, N), state_minus_mean = True)
    #set_seed(12345)
    #fill_diagonal(parms["F"], uniform(size = N))
    #parms["mean"] -= dot(parms["F"], parms["mean"])
    parms = dict(F = 0.6 * eye(N), mean = normal(0, .1, N))

    fm = forward_model(adv_model, parms, deriv, Sigma = Sigma_x, adv_fn_vectorized = True)
    om = measurement_model(Sigma = Sigma_y)

    return hmm(fm, om, x0, ty, tx = ty)
