#!/usr/bin/python

# import packages ----------------------------------------------------------------
from __future__ import print_function
import os
import sys
import argparse

from numpy import nan
from numpy.random import randint
from time import time
import cProfile

#from IPython import embed

# set parameters -----------------------------------------------------------------
parser = argparse.ArgumentParser(description = "Program to get loglik and MSE.")

# positional arguments
parser.add_argument("model", metavar = "model", action = "store", type = str, nargs = 1)
parser.add_argument("N", metavar = "N", action = "store", type = int, nargs = 1,
                    help = "state dimensions")
parser.add_argument("M", metavar = "M", action = "store", type = int, nargs = 1,
                    help = "number of particles")
parser.add_argument("T", metavar = "T", action = "store", type = int, nargs = 1,
                    help = "number of time points")
parser.add_argument("filename", metavar = "filename", action = "store", type = str, nargs = 1)

# optional arguments

parser.add_argument("-runs", "-n_runs", action = "store", default = 100, type = int)
parser.add_argument("-save_every", action = "store", default = 10000, type = int)
parser.add_argument("-sigma_x", action = "store", default = .1, type = float)
parser.add_argument("-sigma_ics", action = "store", default = 1, type = float)
parser.add_argument("-inflation", action = "store", default = 1., type = float)
parser.add_argument("-perturbation", action = "store", default = 0., type = float)
parser.add_argument("-ti_start", action = "store", default = 10, type = int)

parser.add_argument("-run_same_ics", action = "store_true", default = False,
                    help = "run with the same seed for initial conditions")
parser.add_argument("-run_etkf", action = "store_true",
                    help = "run enkf with update_type = etkf")
parser.add_argument("-run_perturbed", action = "store_true",
                    help = "run enkf with perturbed observations")
parser.add_argument("-run_post_reg", action = "store_true",
                    help = "run post-regularization for bootstrap filter")
parser.add_argument("-run_kf", action = "store_true",
                    help = "run Kalman filter")
parser.add_argument("-run_bf", action = "store_true",
                    help = "run bootstrap filter")
parser.add_argument("-run_apf", action = "store_true",
                    help = "run auxiliary particle filter")
parser.add_argument("-pkf_C", action = "store", default = [1], type = float, nargs = "+")
parser.add_argument("-bf_C", action = "store", default = [1], type = float, nargs = "+")

parser.add_argument("-right_param_name", action = "store", type = str, nargs = "+")
parser.add_argument("-right_param_value", action = "store", type = float, nargs = "+")
parser.add_argument("-wrong_param_name", action = "store", type = str, nargs = "+")
parser.add_argument("-wrong_param_value", action = "store", type = float, nargs = "+")

parser.add_argument("-parallel", action = "store_true")

#results = parser.parse_args()
#results = parser.parse_args(["lorenz63", "3", "10", "300", "test.mat", "-run_etkf", "-sigma_x", ".5", "-runs", "20"])
results = parser.parse_args(\
            ["lorenz05", "240", "480", "310", "test.mat", \
             "-pkf_C", "1", #"2", "5", "7.5", "10",
             "-run_etkf", "-inflation", "10", \
             #"-run_bf", "-run_post_reg", "-bf_C", ".5", "1", \
             "-run_apf", #"-perturbation", "1", \
             "-sigma_x", "0", \
             "-runs", "1", \
             "-parallel", \
             "-wrong_param_name", "model_number", "-wrong_param_value", "2"])
#results = parser.parse_args(["quad", "1", "20", "100", "test.mat", "-run_etkf", "-sigma_x", ".1", "-C", ".5"])
#results = parser.parse_args(["linear", "1", "10", "100", "../../text/2015-02-15 BSTARS/figures/linear.mat", "-run_kf", "-sigma_x", ".1", "-runs", "100"])
#results = parser.parse_args(["cappe", "1", "20", "100", "test.mat", "-run_etkf", "-sigma_x", ".1", "-C", "1", "-runs", "1"])

model = results.model[0]
filename = results.filename[0]

N = results.N[0]
M = results.M[0]
T = results.T[0]

n_runs = results.runs
save_every = results.save_every
sigma_x = results.sigma_x
sigma_ics = results.sigma_ics
inflation = results.inflation
perturbation = results.perturbation
ti_start = results.ti_start

same_ics = results.run_same_ics
run_etkf = results.run_etkf
run_perturbed = results.run_perturbed
run_post_reg = results.run_post_reg
run_kf = results.run_kf
run_bf = results.run_bf
run_apf = results.run_apf
pkf_C = results.pkf_C
bf_C = results.bf_C

right_param_name = results.right_param_name
right_param_value = results.right_param_value
wrong_param_name = results.wrong_param_name
wrong_param_value = results.wrong_param_value

parallel = results.parallel

# savemat doesn't deal with Nones properly
if right_param_name is not None and right_param_value is not None:
    if len(right_param_name) != len(right_param_value): raise KeyError("Lengths of 'right_param_name' and 'right_param_value' must equal.")
else:
    results.right_param_name = nan
    results.right_param_value = nan

if wrong_param_name is not None and wrong_param_value is not None:
    if len(wrong_param_name) != len(wrong_param_value): raise KeyError("Lengths of 'wrong_param_name' and 'wrong_param_value' must equal.")
else:
    results.wrong_param_name = nan
    results.wrong_param_value = nan

# import model -------------------------------------------------------------------
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python/runs/' + model
sys.path.append(path)
from default import *

# parallelize? -------------------------------------------------------------------
if parallel:
    import multiprocessing as mp
    n_cores = mp.cpu_count() - 1
    print("# cpus: %i" % n_cores)
    pool = mp.Pool(n_cores)
else:
    pool = None

# run true model ------------------------------------------------------------------
if model == "lorenz63": N = 3
if model == "lorenz63": parms = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1, Sigma_dt = True)
if model == "lorenz05": parms = dict(N = N, K = 32, I = 12, F = 15, b = 10, c = 2.5, model_number = 3)

if right_param_name is not None:
    for i in range(len(right_param_name)):
        parms[right_param_name[i]] = right_param_value[i]

hmm_run = run_hmm(N, M, T, sigma_x = sigma_x, parms = parms, pool_parallel = pool, Sigma_dt = False)

# replace true model with wrong model ---------------------------------------------
if wrong_param_name is not None:
    for i in range(len(wrong_param_name)):
        hmm_run.forward_model.parms[wrong_param_name[i]] = wrong_param_value[i]

# set up initial conditions -------------------------------------------------------
if same_ics:
    ics = init_cond(M = M, mean = x0, Sigma = covmat(sigma = sigma_ics, N = N), seed = 123)
else:
    set_seed(123)
    ics_seed = randint(0, 4294967295+1, n_runs)

# initialize output ---------------------------------------------------------------
filter_names = []
if run_kf: filter_names += ["kf"]
if run_etkf and run_perturbed:
    filter_names += ["enkf_" + x for x in ["etkf", "perturbed"]]
elif run_etkf or run_perturbed:
    filter_names += ["enkf"]
filter_names += ["pkf_" + "%.1f" % x for x in pkf_C] if len(pkf_C) > 1 else ["pkf"]
if run_bf: filter_names += ["bf_" + "%.1f" % x for x in bf_C] if len(bf_C) > 1 and run_post_reg else ["bf"]
if run_apf: filter_names += ["apf"]

n_filters = len(filter_names)
check_resampled = [in1d(f[0:2], ["pk", "bf", "ap"])[0] for f in filter_names]
filters = empty(n_filters, dtype = object)
out = dict(run_arg = vars(results),
           filter_names = filter_names,
           #resampled = zeros((n_runs, n_filters), dtype = uint16),
           #success = zeros((n_runs, n_filters)),
           loglik = zeros((n_runs, n_filters)),
           rmse = zeros((n_runs, n_filters)),
           norm = zeros((n_runs, n_filters)),
           crps = zeros((n_runs, n_filters)),
           spread = zeros((n_runs, n_filters)),
           spread_rmse = zeros((n_runs, n_filters)))

#filename = "../data/%s/%s/N%03d-T%03d-M%04d.mat" % (model, subfolder, N, T, M)
if filename[-4:] != ".mat": raise argparse.ArgumentTypeError("'filename' suffix must be '.mat'.")
print(os.getcwd() + "/" + filename)

# run ------------------------------------------------------------------------------
pr = cProfile.Profile()
pr.enable()

if run_kf:
    fi = 0
    ics = init_cond(M = M, mean = hmm_run.x0, Sigma = covmat(sigma = sigma_ics, N = N), seed = ics_seed[0])
    filters[fi] = kf(hmm_run, ics)
    filters[fi].run_filter()

    rmse = array([filters[fi].rmse(ti) for ti in range(ti_start, T)])
    spread = array([filters[fi].spread(ti, assume_independent = True) for ti in range(ti_start, T)])

    out["loglik"][:,fi] = filters[fi].sum_loglik(ti0 = ti_start)
    out["rmse"][:,fi] = mean(rmse)
    out["norm"][:,fi] = mean([filters[fi].norm_mean(ti) for ti in range(ti_start, T)])
    out["crps"][:,fi] = mean([filters[fi].crps(ti) for ti in range(ti_start, T)])
    out["spread"][:,fi] = mean(spread)
    out["spread_rmse"][:,fi] = mean(spread/rmse)

a = time()
for i in xrange(n_runs):
    # run initial conditions
    if not same_ics:
        ics = init_cond(M = M, mean = hmm_run.x0, Sigma = covmat(sigma = sigma_ics, N = N), seed = ics_seed[i])

    # EnKF
    fi = int(run_kf)
    if run_etkf:
        filters[fi] = \
            enkf(hmm_run, ics,
                inflation_factor = inflation,
                    update_type = "etkf")
        fi += 1

    if run_perturbed:
        filters[fi] = \
            enkf(hmm_run, ics,
                inflation_factor = inflation,
                update_type = "perturbed")
        fi += 1

    # PKF
    for ci in range(len(pkf_C)):
        filters[fi] = \
            pkf(hmm_run, ics,
                #pre_reg_type = "sample_diag",
                pre_reg_type = "diag",
                reg_C = pkf_C[ci],
                resample_ess = True)
        fi += 1

    # BF
    if run_bf:
        if run_post_reg:
            for ci in range(len(bf_C)):
                filters[fi] = \
                    bf(hmm_run, ics,
                        resample_ess = True,
                        post_reg_type = "diag", reg_C = bf_C[ci])
                fi += 1
        else:
            filters[fi] = \
                bf(hmm_run, ics,
                   resample_ess = True,
                   perturbation_factor = perturbation)
            fi += 1

    # APF
    if run_apf:
        filters[fi] = \
            apf(hmm_run, ics,
                resample_ess = True,
                perturbation_factor = perturbation)

    # record output
    print("%5i" % (i), end = " | ")
    for fi in range(n_filters):
        if not (fi == 0 and run_kf):
            filters[fi].run_filter()
            #if check_resampled[fi]: out["resampled"][i,fi] = filters[fi].ensemble.ancestry["filter"]["resampled"].sum()

            rmse = array([filters[fi].rmse(ti) for ti in range(ti_start, T)])
            spread = array([filters[fi].spread(ti, assume_independent = True) for ti in range(ti_start, T)])

            out["loglik"][i,fi] = filters[fi].sum_loglik(ti0 = ti_start)
            out["rmse"][i,fi] = mean(rmse)
            out["norm"][i,fi] = mean([filters[fi].norm_mean(ti) for ti in range(ti_start, T)])
            out["crps"][i,fi] = mean([filters[fi].crps(ti) for ti in range(ti_start, T)])
            out["spread"][i,fi] = mean(spread)
            out["spread_rmse"][i,fi] = mean(spread/rmse)
        print("%s %8.1f %5.3f" % (filter_names[fi], out["loglik"][i,fi], out["norm"][i,fi]), end = " | ")
        sys.stdout.flush()
    print("")

    # save
    if i % save_every == 0 and i > 0: savemat(filename, out)

savemat(filename, out)

b = time()
print("runtime = %.2f m" % ((b-a)/60.))

pr.disable()
pr.dump_stats(path + "/diags.out")

if parallel: pool.close()




"""

for fi in range(n_filters): filters[fi].plot1d(range(3), "forecast", plot_ensemble = False); plt.show()

hmm_run.savemat(path + "/../text/2015-02-15 BSTARS/figures/lorenz05-hmm.mat")
filters[1].ensemble.savemat(path + "/../text/2015-02-15 BSTARS/figures/lorenz05-pkf.mat")
filters[3].ensemble.savemat(path + "/../text/2015-02-15 BSTARS/figures/lorenz05-apf.mat")
"""






