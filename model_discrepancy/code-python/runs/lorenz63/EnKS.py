
from os import chdir
from os.path import expanduser

from numpy import arange, array, random
from copy import deepcopy

path = expanduser('~') + '/Desktop/Grad school/Research/data_assimilation/model_discrepancy/code-python'
chdir(path)

from hmm import *
from lorenz63 import *

seed = 12345
n_wrong = 2

# set up
print "Setting up EnKFs..."
x0 = array([.01] * 3)
tx = arange(0,30,0.01)
ty = arange(0,30,0.1)
Sigma_0 = covmat(sigma = 3., N = 3)
ics = init_cond(M = 10, mean = array([4.] * 3), sigma = 1)

parms_right = dict(sigma = 10, rho = 28, beta = 8/3)
parms_wrong = [[]] * n_wrong

random.seed(seed+1)
parms_wrong[0] = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1, F = array([normal(0, 100, 1)[0], 0, 0]))
print "F_wrong[0]: "
print parms_wrong[0]["F"]

random.seed(seed+2)
parms_wrong[1] = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1, F = normal(0, 100, 3))
print "F_wrong[1]: "
print parms_wrong[1]["F"]

l63_fm_right = forward_model(adv_model, parms_right, deriv_right, tlm_right)
l63_hmm_right = hmm(l63_fm_right, x0, Sigma_0, ty, tx = tx)
l63_right = enkf(l63_hmm_right, ics, inflation_factor = 2)

l63_fm_wrong = [[]] * n_wrong
l63_hmm_wrong = [[]] * n_wrong
l63_wrong = [[]] * n_wrong

for n in xrange(n_wrong):
    l63_fm_wrong[n] = forward_model(adv_model, parms_wrong[n], deriv_wrong, tlm_wrong)
    l63_hmm_wrong[n] = deepcopy(l63_hmm_right)
    l63_hmm_wrong[n].forward_model = l63_fm_wrong[n]
    l63_wrong[n] = enkf(l63_hmm_wrong[n], ics, inflation_factor = 2)

# run EnKF
print "Running EnKF on right model..."
l63_right.run_filter()
l63_right.run_smoother()
l63_right.run_marg_lik()
print l63_right.log_marg_lik[0]

for n in xrange(n_wrong):
    print "Running EnKF on wrong model #" + str(n) + "..."
    l63_wrong[n].run_filter()
    l63_wrong[n].run_smoother()
    l63_wrong[n].run_marg_lik()
    print l63_wrong[n].log_marg_lik[0]

l63_right.plot1d(xrange(3), "forecast")
plt.show()

l63_wrong[1].plot1d(xrange(3), "forecast")
plt.show()

l63_right.plot1d(xrange(3), "analysis")
plt.show()

l63_wrong[1].plot1d(xrange(3), "analysis")
plt.show()

l63_right.plot1d(xrange(3), "smoother")
plt.show()

l63_wrong[1].plot1d(xrange(3), "smoother")
plt.show()


