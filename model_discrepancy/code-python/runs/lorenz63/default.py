
from numpy import arange, array, dot
from numpy.random import uniform

# add module location to python path and import
import os
import sys
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz63 import *

def run_hmm(N, M, T,
            x0 = 0,
            sigma_x = sqrt(.01),
            parms = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1),
            Sigma_dt = True,
            pool_parallel = None):

    x0 = array([.01] * 3)
    ty = arange(0,T*.1,0.1)

    Sigma_x = covmat(sigma = sigma_x, N = N) if sigma_x != 0 else None
    Sigma_y = covmat(sigma = 1, N = 3)

    fm = forward_model(adv_model, parms, deriv_right, grad_right, Sigma = Sigma_x, Sigma_dt = Sigma_dt)
    om = measurement_model(Sigma = Sigma_y)

    return hmm(fm, om, x0, ty, tx = ty)
