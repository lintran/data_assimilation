
from os import chdir
from os.path import expanduser

from numpy import arange, array, random, linspace
import statsmodels.api as sm
from copy import deepcopy

from matplotlib.backends.backend_pdf import PdfPages

path = expanduser('~') + '/Desktop/Grad school/Research/data_assimilation/model_discrepancy/code-python'
chdir(path)

from hmm import *
from lorenz63 import *

M = 10

x0 = array([.01] * 3)
tx = arange(0,30,0.01)
ty = arange(0,30,0.1)
Sigma_0 = covmat(sigma = 3., N = 3)

parms = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1, F = array([0, 0, 0]))

l63_fm = forward_model(adv_model, parms, deriv_wrong, tlm_wrong)
l63_hmm = hmm(l63_fm, x0, Sigma_0, ty, tx = tx)
ics = init_cond(M = M, mean = array([4.] * 3), sigma = 1)

# EnKF
l63_enkf = enkf(l63_hmm, ics, inflation_factor = 2)
l63_enkf.run_filter(verbose = True)
l63_enkf.run_marg_lik()
l63_enkf.log_marg_lik

# PF
l63_pf = pf(l63_hmm, ics, perturbation_factor = 2, filter_type = "auxiliary")
l63_pf.run_filter(verbose = True)
l63_pf.run_marg_lik()
l63_pf.log_marg_lik

# likelihood grid
spacing = 21
fx = parms["F"][0] + linspace(-10, 10, spacing)
#fx = parms["rho"] + linspace(-3, 3, spacing)
log_lik = nones((spacing, 2))
log_lik[spacing/2, 0] = l63_enkf.log_marg_lik
log_lik[spacing/2, 1] = l63_pf.log_marg_lik

for i in xrange(spacing):
    print fx[i]
    if i == spacing/2: continue

    l63_enkf.hmm.forward_model.parms["F"][0] = fx[i]
    #l63_enkf.hmm.forward_model.parms["rho"] = fx[i]
    l63_enkf.reset()
    l63_enkf.run_filter()
    l63_enkf.run_marg_lik()
    log_lik[i,0] = l63_enkf.log_marg_lik

    l63_pf.hmm.forward_model.parms["F"][0] = fx[i]
    #l63_enkf.hmm.forward_model.parms["rho"] = fx[i]
    l63_pf.reset()
    l63_pf.run_filter()
    l63_pf.run_marg_lik()
    log_lik[i,1] = l63_pf.log_marg_lik

lowess = sm.nonparametric.lowess
z = [lowess(log_lik[:,i], fx)[:,1] for i in xrange(2)]

#pp = PdfPages("../plots/lorenz63/lik/Fx.pdf")

fig, subfig = plt.subplots(1, 2)
for i in xrange(2):
    subfig[i].plot(fx, z[i])
    subfig[i].scatter(fx, log_lik[:,i])
    subfig[i].axvline(x=fx[spacing/2])
    subfig[i].set_title(["enkf", "pf"][i])
    subfig[i].set_xlabel("Fx")
plt.show()

#pp.savefig(fig)
#plt.close()
#pp.close()
