#!/usr/bin/python

from __future__ import print_function
import sys
import os
import datetime

from numpy import arange, array, random, linspace

# add module location to python path and import
path = os.path.expanduser('~') + '/Desktop/Grad school/Research/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)

from hmm import *
from lorenz63 import *

# change directory to save file
path = os.path.expanduser('~') + '/Desktop/Grad school/Research/data_assimilation/model_discrepancy/data/lorenz63/likelihood-variance'
if not os.path.exists(path): os.makedirs(path)
os.chdir(path)

# arguments
if len(sys.argv) == 4:
    Fx = float(sys.argv[1])
    M = int(sys.argv[2])
    ics_mean = float(sys.argv[3])
else:
    Fx = float(0)
    M = int(10)
    ics_mean = float(.01)

n_runs = 1000
seed = 12345

# open outfile
print(path)
print(sys.argv)

# initialize filters
x0 = array([.01] * 3)
tx = arange(0,30,0.01)
ty = arange(0,30,0.1)
Sigma_0 = covmat(sigma = 3., N = 3)

parms = dict(sigma = 10, rho = 28, beta = 8/3, alpha = 1, F = [0, 0, 0])
fm = forward_model(adv_model, parms, deriv_wrong, tlm_wrong)
l63_hmm = hmm(fm, x0, Sigma_0, ty, tx = tx, seed = seed)
filename = "state-obs-seed%d.mat" % (seed)
l63_hmm.savemat(filename)

# run likelihood under new parameters
l63_hmm.forward_model.parms["F"][0] = Fx
out = dict(t0t100 = nones((n_runs, 4)),
           t100t200 = nones((n_runs, 4)),
           t200t300 = nones((n_runs, 4)))
out = dict(before = deepcopy(out),
           after = deepcopy(out))

#now = datetime.datetime.now().strftime("%Y-%m-%d-%H%M")
filename = "M%03i-ics%.02f-F%03i-seed%i.mat" % (M, ics_mean, Fx, seed)
for i in xrange(n_runs):
    print(str(i), end = " ")

    # EnKF
    print("enkf... ", end = "")
    ics = init_cond(M = M, mean = array([ics_mean] * 3), sigma = 1)
    l63_enkf = enkf(l63_hmm, ics, inflation_factor = 2, linear = True)
    l63_enkf.run_filter()
    l63_enkf.run_loglik()
    l63_enkf.run_loglik(key = "after_linear", linear = True)

    # Auxiliary particle filter
    print("apf... ", end = "")
    ics = init_cond(M = M, mean = array([ics_mean] * 3), sigma = 1)
    l63_apf = pf(l63_hmm, ics, perturbation_factor = 2, filter_type = "auxiliary")
    l63_apf.run_filter()
    l63_apf.run_loglik()

    # Bootstrap filter
    print("bf... ", end = "")
    ics = init_cond(M = M, mean = array([ics_mean] * 3), sigma = 1)
    l63_bf = pf(l63_hmm, ics, perturbation_factor = 2, filter_type = "bootstrap")
    l63_bf.run_filter()
    l63_bf.run_loglik()

    # output
    print("calculating output... ")
    for ba in ("before", "after"):
        for ti in range(0, 300, 100):
            t = ti + array([0, 100])
            name = "t" + str(t[0]) + "t" + str(t[1])
            out[ba][name][i,0] = l63_enkf.sum_loglik(ba, t[0], t[1])
            out[ba][name][i,1] = l63_enkf.sum_loglik(ba + "_linear", t[0], t[1])
            out[ba][name][i,2] = l63_apf.sum_loglik(ba, t[0], t[1])
            out[ba][name][i,3] = l63_bf.sum_loglik(ba, t[0], t[1])

    # save
    if i % 100 == 0: savemat(filename, {'out': out})

if i % 100 != 0: savemat(filename, {'out': out})


