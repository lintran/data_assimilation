#!/bin/bash
for M in 10 100
do
    for ics in .01 .1 1 4
    do
        for Fx in $(seq -10 10)
        do
            printf -v text "M%03i-ics%.02f-F%03i" "${M}" "${ics}" "${Fx}"
            echo "python lik-var.py ${Fx} ${M} ${ics}"  | \
                qsub -o outfiles/${text}.out \
                     -e errfiles/${text}.err \
                     -N ${text}
        done
    done
done
