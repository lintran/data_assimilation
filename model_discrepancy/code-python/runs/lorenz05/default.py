
from numpy import arange, array, dot
from numpy.random import uniform

# add module location to python path and import
import os
import sys
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz05 import *

def run_hmm(N, M, T,
            x0 = 0,
            sigma_x = sqrt(.01),
            parms = dict(K = 32, I = 12, F = 15, b = 10, c = 2.5, model_number = 3),
            Sigma_dt = True,
            pool_parallel = None):

    parms["N"] = N
    parms = calc_consts(parms)

    x0 = uniform(0, 1, N)
    x0 = odeint(deriv, x0, [0, 2*12*30*24/120.], args = (parms,), mxstep = 10**6)[1,:]
    #tx = 2*12*30*24/120. + arange(0, T*6/120., 1/120.)
    ty = 2*12*30*24/120. + arange(0, T*6/120., 6/120.)

    Sigma_x = covmat(sigma = sigma_x, N = N) if sigma_x != 0 else None
    Sigma_y = covmat(sigma = 1, N = N)

    fm = forward_model(adv_model, parms, deriv, grad, Sigma = Sigma_x, Sigma_dt = Sigma_dt, pool_parallel = pool_parallel)
    om = measurement_model(Sigma = Sigma_y)

    return hmm(fm, om, x0, ty)#, tx = ty)
