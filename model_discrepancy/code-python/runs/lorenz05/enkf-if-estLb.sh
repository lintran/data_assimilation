#!/bin/bash
# $1 saveidx
# $2 B
# $3 number of cpus
# $4- any other arguments
echo "python enkf-if-estLb.py -saveidx $1 -savename $2 -B $2 -ncpus $3 ${*:4}"  | qsub -pe smp $3 -R y -o outfiles/estLb-$2-$1.out -e errfiles/estLb-$2-$1.err -N estLb-$2-$1
