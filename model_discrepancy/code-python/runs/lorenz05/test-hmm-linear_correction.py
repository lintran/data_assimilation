
import os
import sys

from numpy import arange, array, append, sign, abs
from numpy.random import rand, uniform
from scipy.io import loadmat
from scipy.integrate import odeint
from time import time
from copy import deepcopy
import cProfile


# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)

from hmm import *
from models.lorenz05 import *

N = 960
M = 30
T = 50 #2.5*30*24/6.

use_wrong_model = True
# F = 15
# K = 10
F = 15
K = 32
model_number = 2

sigma_x = 0
sigma_y = 1.

C = 1
inflation = 10.
perturbation = 0 #.5 if sigma_x == 0 else 0

seed = 12345
plot = False
verbose = True
profile = False
parallel = False

obs_all = True
use_dart = False

# parallelize?
if parallel:
    import multiprocessing as mp
    n_cpus = mp.cpu_count() - 1
    print("# cpus: %i" % n_cpus)
    pool = mp.Pool(2)
else:
    pool = None

# Create H
if obs_all:
    H_bool = array([True for x in range(N)])
    H = None
else:
    H_bool = choice([True, False], size = N, p = [.5, .5])
    H = diag(H_bool.astype(float))[H_bool,:]

# forward model
parms = dict(N = N, K = 32, I = 12, F = 15, b = 10, c = 2.5, model_number = 3)
#parms = dict(N = N, K = 32, I = 12, F = 15, b = 10, c = 5, model_number = 3)
Sigma_x = covmat(sigma = sigma_x, N = N) if sigma_x != 0 else None
l05_fm = forward_model(adv_model, parms, deriv, grad, Sigma = Sigma_x, Sigma_dt = False, pool_parallel = pool)
l05_fm.parms = calc_consts(l05_fm.parms)

# observation model
Sigma_y = covmat(sigma = sigma_y, N = H_bool.sum())
#def h_fn(x): return x**2./10.
#def h_grad(x): return 1./5.*x
#l05_om = measurement_model(Sigma = Sigma_y, H = H, h_fn = h_fn, h_grad = h_grad)
l05_om = measurement_model(Sigma = Sigma_y)

# hmm
if use_dart:
    DART_inits = loadmat(path + "/../data/lorenz05/inits.mat")
    DART_obsinits = loadmat(path + "/../data/lorenz05/lorenz05.mat")
    x0 = DART_inits["perfect"][0:N]
    tx = DART_obsinits["times"][0:T]
    ty = DART_obsinits["times"][0:T]
    l05_hmm = hmm(l05_fm, l05_om, x0, ty, tx = tx, x = DART_obsinits["x"][0:T,0:N], y = DART_obsinits["y"][0:T,H_bool])
else:
    set_seed(seed)
    x0 = uniform(0, 1, N)
    x0 = odeint(deriv, x0, [0, 2*12*30*24/120.], args = (parms,), mxstep = 10**6)[1,:]
    tx = 2*12*30*24/120. + arange(0, T*6/120., 1/120.)
    ty = 2*12*30*24/120. + arange(0, T*6/120., 6/120.)
    l05_hmm = hmm(l05_fm, l05_om, x0, ty, tx = tx)

if plot: l05_hmm.plot1d(range(3)); plt.show()

# initial conditions
ics = init_cond(M = M, mean = x0, Sigma = covmat(sigma = 1, N = N), seed = seed)
#ics = init_cond(M = M, mean = x0 + normal(50,1,N), Sigma = covmat(sigma = 1, N = N))
#ics = init_cond(M = M, ens = DART_inits["perturbed"][:,range(M)])
#ics = init_cond(M = M, mean = DART_obsinits["y"][0,:], Sigma = covmat(sigma = 1, N = N))

if use_wrong_model:
    parms_wrong = dict(N = N, K = K, F = F, model_number = model_number, dt = 0.001)
    linear_info = loadmat(path + "/../data/lorenz05/linear/param-bconst-cv1-N960-K32-I12-b10-c3-F15.mat")
    parms_wrong["L"] = linear_info["L"]
    parms_wrong["Fplus"] = linear_info["b"]
    if parms_wrong["Fplus"].shape[0] == 1: parms_wrong["Fplus"] = parms_wrong["Fplus"]*ones(N)

    l05_fm.parms = calc_consts(parms_wrong)
    #l05_fm.deriv = deriv #deriv_plus_linear
    #l05_fm.adv_fn = adv_model_rk
    #l05_fm.grad = None

#"""
l05_fm.deriv = deriv
wrong = pkf(l05_hmm, ics,
            pre_reg_type = "diag", reg_C = C,
            resample_type = "multinomial",
            resample_ess = True)
wrong.run_filter(verbose = verbose)

mean(array([wrong.rmse(ti) for ti in range(10, l05_hmm.T)]))

if plot:
    wrong.plot1d(xrange(3), "forecast", t_start = ty[0], plot_ensemble = False)
    plt.show()

    wrong.plot1d(xrange(3), "analysis", t_start = ty[0], plot_ensemble = False)
    plt.show()
#"""


#"""
l05_fm.deriv = deriv_plus_linear
corrected = pkf(l05_hmm, ics,
                pre_reg_type = "diag", reg_C = C,
                resample_type = "multinomial",
                resample_ess = True)
corrected.run_filter(verbose = verbose)

mean(array([corrected.rmse(ti) for ti in range(10, l05_hmm.T)]))

if plot:
    corrected.plot1d(xrange(3), "forecast", t_start = ty[0], plot_ensemble = False)
    plt.show()

    corrected.plot1d(xrange(3), "analysis", t_start = ty[0], plot_ensemble = False)
    plt.show()
#"""


