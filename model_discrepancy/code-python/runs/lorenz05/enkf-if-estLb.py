
# PARSE ARGUMENTS -----------------------------------------------------------------
import argparse
parser = argparse.ArgumentParser()

# optional arguments
parser.add_argument("-M", action = "store", default = 50, type = int)
parser.add_argument("-B", action = "store", default = 50, type = int)
parser.add_argument("-ncpus", action = "store", default = None, type = int)
parser.add_argument("-runone", action = "store_true", default = False)
parser.add_argument("-save", action = "store_true", default = False)
parser.add_argument("-savename", action = "store", default = "", type = str)
parser.add_argument("-saveidx", action = "store", default = None, type = int)
parser.add_argument("-dartfolder", action = "store", type = str, nargs = 1, default = "right-full-default-dt12")

# parse arguments
args = parser.parse_args()
# args = parser.parse_args(["-ncpus", "3"])
# args = parser.parse_args(["-ncpus", "3", "-runone", "-saveidx", "3"])

# SETUP ---------------------------------------------------------------------------
import os, sys
import time, datetime
from numpy import arange, array, append, linspace
from scipy.io import loadmat, netcdf
from scipy.linalg import circulant
from cProfile import Profile
pr = Profile()

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)
from hmm import *
from models.lorenz05 import *

# TRUE MODEL ----------------------------------------------------------------------
# match up with DART
DARTdir = os.path.expanduser('~') + "/data_assimilation/DART/Kodiak/models/lorenz_04/"
inits = loadmat(DARTdir + "work/inits.mat")
obs = loadmat(DARTdir + args.dartfolder + "/obs.mat")
truth = netcdf.netcdf_file(DARTdir + args.dartfolder + "/True_State.nc", "r")

x = truth.variables["state"][:,0,:].copy()
y = obs["obs"].T

dt_convert = dict(days = truth.time_step_days.copy(), seconds = truth.time_step_seconds.copy())
T = x.shape[0]

parms = dict(N = x.shape[1],
             K = truth.K.copy(),
             I = truth.smooth_steps.copy(),
             F = truth.model_forcing.copy(),
             b = truth.space_time_scale.copy(),
             c = truth.coupling.copy(),
             model_number = 3 if truth.model_scale == "2-scale" else 2,
             dt = truth.model_delta_t.copy())

x0 = x[0,:]
ty_real = truth.variables["time"][:].copy()
ty = parms["dt"]*(24*60*60*ty_real / (24*60*60*dt_convert["days"] + dt_convert["seconds"]))
print ty.size
locx = truth.variables["loc1d"][:].copy()

truth.close()

# forward model
l05_fm = forward_model(adv_model_rk, parms, deriv, fparms = calc_consts, n_cpus = args.ncpus)
l05_fm.parms = calc_consts(l05_fm.parms)

# observation model
import re
partial = re.search("partial", args.dartfolder) is not None
Sigma_y = covmat(sigma = sqrt(obs["var"]), N = y.shape[1])
if not partial:
  l05_om = measurement_model(Sigma = Sigma_y)
else:
  l05_om = \
      measurement_model(Sigma = Sigma_y,
          loc_x = locx,
          loc_y = obs["loc"],
          halfwidth = obs["halfwidth"][0],
          num_points = obs["num_points"][0])
  a = dot(l05_om.H, x.T)
  b = obs["truth"]
  c = abs(a - b)
  print c.min(), c.max()

# hmm
l05_hmm = hmm(l05_fm, l05_om, x0, ty)

# initial conditions
ics = init_cond(M = args.M, samples = inits["perturbed"][range(0,960,960/parms["N"]),:])
ics.ens = ics.samples[:,range(args.M)]

# WRONG MODEL ----------------------------------------------------------------------
distvec = locx
distvec = distvec - distvec[0]
distvec[distvec > 0.5] = 1 - distvec[distvec > 0.5]
distmat = circulant(distvec).T
maxdist = distvec.max()

parms_wrong = parms
parms_wrong = dict(N = parms["N"], K = parms["K"], F = parms["F"], model_number = 2, dt = parms["dt"],
                   multiplier = 0, mu_d = 0, mu_x = zeros(parms["N"]),
                   distvec = distvec,
                   cutoff_dx = distvec[1], period_dx = 0, q_dx = 1,
                   cutoff_xx = distvec[1], period_xx = 0, q_xx = 1)
parms_wrong2 = calc_consts(parms_wrong)
parms_wrong2 = calc_Lb(parms_wrong2)
l05_fm.parms = parms_wrong2
l05_fm.fparms = calc_Lb
l05_fm.deriv = deriv_plus_linear

# FILTER ---------------------------------------------------------------------------

l05_enkf = enkf(l05_hmm, ics,
                update_type = "etkf")

param_names = array(["multiplier", "mu_d",
                     "cutoff_dx", "period_dx",
                     "cutoff_xx", "period_xx",
                     "inflation"])
param_min   = array([ -inf, -inf,
                         0,    0,
                         0,    0,
                         1])
param_max   = array([       inf,  inf,
                     maxdist/2.,    1,
                     maxdist/2.,    1,
                             10])
param_sd    = array([         1,    1,
                     distvec[1], .001,
                     distvec[1], .001,
                              1])
if2 = IteratedFilter2(
        args.B, l05_enkf,
        param_names = dict(state = param_names),
        param_sd  = param_sd,
        param_min = param_min,
        param_max = param_max,
        param_mean_analysis = "mu_x",
        param_inflate = "inflation",
        D_print = param_names.size,
        loglik_ti0 = 0,
        update_type = "apf",
        resample_type = "systematic")

if args.runone:
  pr.enable()
  if2.sample()
  pr.disable()
  ts = time.time()
  ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M-%S')
  filename = path + "/if2-%02i" % (1 if args.ncpus is None else args.ncpus)
  filename += "-" + ts + ".out"
  pr.dump_stats(filename)
  # cProfilev (insert filename)
  # localhost:4000
else:
  if2.run()

if args.save:
  if args.saveidx is None:
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d-%H-%M-%S")
  else:
    ts = "%03i" % args.saveidx
  if args.savename != "": ts = args.savename + "-" + ts
  dr = os.path.expanduser('~') + "/data_assimilation/model_discrepancy/data/lorenz05/model-error-pf/" + args.dartfolder
  if not os.path.exists(dr): os.makedirs(dr)

  # filename = "%s/if2-%s.mat" % (dr, ts)
  # if2.savemat(filename)
  # filename = "%s/enkf-%s.mat" % (dr, ts)
  # l05_enkf.ensemble.savemat(filename)

  import cPickle
  filename = "%s/if2-%s.pyobj" % (dr, ts)
  f = file(filename, "wb")
  l05_fm.pool.close()
  l05_fm.pool = None
  cPickle.dump(if2, f, protocol = cPickle.HIGHEST_PROTOCOL)
  f.close()

if False:
  l05_enkf.plot1d(xrange(3), "forecast")
  plt.show()

  for key in param_names:
    if2.pftrace(key); plt.show()



