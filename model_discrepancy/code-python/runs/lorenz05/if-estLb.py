
import os
import sys

from numpy import arange, array, append
from numpy.random import rand, uniform
from scipy.io import loadmat
from scipy.integrate import odeint
from scipy.spatial.distance import pdist
import scipy.stats as sp
from copy import deepcopy

# add module location to python path and import
path = os.path.expanduser('~') + '/data_assimilation/model_discrepancy/code-python'
sys.path.append(path)

from hmm import *
from models.lorenz05 import *

N = 960
M = 50
T = 100
B = 100

sigma_y = 1.
param_sd = 1.

use_dart = True
seed = 12345

perturb = False
inflate = True
obs_all = True
parallel = False
profile = True
dt_onehour = 0.001
#dt_onehour = 1/120.

# parallelize?
n_cpus = None
if parallel:
    from multiprocessing import cpu_count
    #n_cpus = 15
    #n_cpus = cpu_count() - 1
    n_cpus = 2
    # os.environ["OMP_NUM_THREADS"] = "1"
    # print("# cpus: %i, # threads" % n_cpus)
    # os.system("echo $OMP_NUM_THREADS")

# profile?
if profile: from cProfile import Profile

# Create H
if obs_all:
    H_bool = array([True for x in range(N)])
    H = None
else:
    H_bool = choice([True, False], size = N, p = [.8, .2])
    H = diag(H_bool.astype(float))[H_bool,:]

# observation model
Sigma_y = covmat(sigma = sigma_y, N = H_bool.sum())
l05_om = measurement_model(Sigma = Sigma_y, H = H)

# TRUE MODEL ----------------------------------------------------------------------
# forward model
parms = dict(N = N, K = 32, I = 12, F = 15, b = 10, c = 3, model_number = 3)
l05_fm = forward_model(adv_model_lsode, parms, deriv, fparms = calc_consts, n_cpus = n_cpus)
l05_fm.parms = calc_consts(l05_fm.parms)

# hmm
if use_dart:
    DART_inits = loadmat(path + "/../data/lorenz05/inits.mat")
    DART_obsinits = loadmat(path + "/../data/lorenz05/lorenz05.mat")
    x0 = DART_inits["perfect"][0:N]
    tx = DART_obsinits["times"][0:T]
    ty = DART_obsinits["times"][0:T]
    l05_hmm = hmm(l05_fm, l05_om, x0, ty, tx = tx, x = DART_obsinits["x"][0:T,0:N], y = DART_obsinits["y"][0:T,H_bool])
else:
    set_seed(seed)
    x0 = uniform(0, 1, N)
    x0 = odeint(deriv, x0, [0, 2*12*30*24*dt_onehour], args = (parms,), Dfun = grad)[1,:]
    tx = 2*12*30*24*dt_onehour + arange(0, T*6*dt_onehour, dt_onehour)
    ty = 2*12*30*24*dt_onehour + arange(0, T*6*dt_onehour, 6*dt_onehour)
    l05_hmm = hmm(l05_fm, l05_om, x0, ty, tx = tx)

# initial conditions
ics = init_cond(M = M, mean = x0, Sigma = covmat(sigma = 1, N = N))

# WRONG MODEL ----------------------------------------------------------------------

distmat = pdist(array(range(N))[:,None])
distmat[distmat > N/2.] = N - distmat[distmat > N/2.]
distmat = squareform(distmat)
distmat[0,:]

parms_wrong = dict(N = N, K = 32, F = 15, model_number = 2,
                   multiplier = 0, distmat = distmat,
                   mu_d = 0, mu_x = 0,
                   cutoff_dx = 1, period_dx = 0,
                   cutoff_xx = 1, period_xx = 0,
                   dt = (dt_onehour)/2)
if perturb: parms_wrong["perturb"] = 0.
if inflate: parms_wrong["inflate"] = 1.
parms_wrong2 = calc_consts(parms_wrong)
parms_wrong2 = calc_Lb(parms_wrong2)

l05_fm.parms = parms_wrong2
l05_fm.fparms = calc_Lb
l05_fm.deriv = deriv_plus_linear
#l05_fm.adv_fn = adv_model
l05_fm.adv_fn = adv_model_rk
#l05_fm.adv_fn = adv_model_lsode


# FILTER ---------------------------------------------------------------------------
l05_enkf = enkf(l05_hmm, ics,
                update_type = "etkf",
                inflation_first = True)

if perturb:
    l05_enkf.perturbation_forecast = True
    l05_enkf.perturbation_t[:] = parms_wrong["perturb"]
if inflate:
    l05_enkf.inflation_forecast = True
    l05_enkf.inflation_t[:] = parms_wrong["inflate"]


param_names = array(["multiplier", "mu_d", "cutoff_dx", "period_dx", "cutoff_xx", "period_xx"])
param_min   = array([ -inf, -inf,    0,   0,   0,   0])
param_max   = array([  inf,  inf,  240,   1, 240,   1])
param_sd    = array([  .25,  .25,   .5, .05,   1, .05])

if perturb:
  param_names = append(param_names, "perturb")
  param_min   = append(param_min, 0)
  param_max   = append(param_max, inf)
  param_sd    = append(param_sd, 1)

if inflate:
  param_names = append(param_names, "inflate")
  param_min   = append(param_min, 1)
  param_max   = append(param_max, inf)
  param_sd    = append(param_sd, 1)


if2 = IteratedFilter2(B, l05_enkf,
                      param_sd = param_sd,
                      param_names = param_names,
                      param_min = param_min,
                      param_max = param_max,
                      D_print = param_names.size,

                      parm_mean_analysis = "mu_x",
                      parm_perturb = "perturb" if perturb else None,
                      parm_inflate = "inflate" if inflate else None,
                      #M_subset = 5,

                      filter_verbose = True,
                      loglik_ti0 = 0)

if profile:
    pr = Profile()
    pr.enable()

#if2.run()
if2.sample()

if profile:
    pr.disable()
    pr.dump_stats(path + "/profile.out")
    # cProfilev (insert filename)
    # localhost:4000

"""
for key in param_names:
  if2.pftrace(key)
  plt.suptitle(key)
  plt.show()

for key in param_names:
  if2.trace(key)
  plt.suptitle(key)
  plt.show()
"""
