#!/bin/bash

# $1 model name
# $2 use cluster?

# set up depending on model name
if [ "$1" == "lorenz05" ]; then
    Mvec=(30 60 120 240 480) # 960)
    Pvec=(14 16 13 17 12 18 11 19 10 20)
    N=240
    T=310

    which_param="wrong"
    param_name="F"
    param_name_other="model_number"
    param_value_other="2"

    par_threshold=10000
    #par_threshold=240
    n_cpus=10

    opts_all="-pkf_C .5 1 2 5 7.5 10 \
              -run_etkf -inflation 10 \
              -sigma_x 0"
    opts=
    for mi in $(seq ${#Mvec[@]}); do
        if [ ${Mvec[${mi}-1]} -le 60 ]; then
            opts[${mi}-1]="${opts_all} -runs 20"
        else
            opts[${mi}-1]="${opts_all} -runs 10"
        fi

        if [ ${Mvec[${mi}-1]} -ge ${par_threshold} ]; then opts[${mi}-1]="${opts[${mi}-1]} -parallel"; fi
    done
else
    echo "ERROR: Don't recognize that model type."
    exit 1 # terminate and indicate error
fi

# runs
if [ "$2" == "true" ]; then # use cluster
    echo "using cluster"
elif [ "$2" == "false" ]; then # don't use cluster
    echo "not using cluster"
fi

for mi in $(seq ${#Mvec[@]}); do
    M=${Mvec[${mi}-1]}
    opts_run=${opts[${mi}-1]}
    par_command=""
    if [ ${M} -ge ${par_threshold} ]; then par_command="-pe mpi ${n_cpus} -R y"; fi

    for param_value in ${Pvec[@]}; do
        name="$1-M${M}-${param_name}${param_value}-diffics"
        filename="M${M}-${param_name}${param_value}.mat"

        if [ "$2" == "true" ]; then # use cluster
            echo $name
            echo \
                "python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename} ${opts_run} \
                 -${which_param}_param_name ${param_name} ${param_name_other}\
                 -${which_param}_param_value ${param_value} ${param_value_other}" \
                 | qsub -o outfiles/${name}.out -e errfiles/${name}.err -N ${name} ${par_command}
            sleep 1s

        elif [ "$2" == "false" ]; then # don't use cluster
            echo $name
            nohup \
                python get-loglik-mse.py $1 ${N} ${M} ${T} ../../data/$1/diffics/${filename} ${opts_run} \
                 -${which_param}_param_name ${param_name} ${param_name_other}\
                 -${which_param}_param_value ${param_value} ${param_value_other}
                 > outfiles/${name}.out &
            sleep 1s

        else
            echo "ERROR: Second argument must be 'true' or 'false'."
        fi
    done
done
