
# Code adapted from CGK's file:
#  ~/Dropbox/Grad school/Research/model_error/toy_models/LorenzModels.particle.R

# One step in particle filter
pf.step = function(particles, y, dt, sigma, tau, 
                   model, parms, N = NULL, M = NULL) {
  
  if(is.null(N)) N <- nrow(particles)
  if(is.null(M)) M <- ncol(particles)
  
  # Prediction step - perturb particles and step forward
  particles <- particles + tau * rnorm(M)
  particles <- apply(particles, 2, function(particle) {
    lsoda(y = particle, times = c(0, dt),
          func = model, parms = parms)[2,2:4]})
  
  pred.mean <- apply(particles, 1, mean)
  pred.var <- apply(particles, 1, var)
  
  # Calculate the likelihoods
  log.weights <- apply(particles, 2, function(x) {
    prod(dnorm(y, mean = x, sd = sigma, log = TRUE))})
  
  # Resample according to the weights
  index <- sample.int(M, M, replace = TRUE, prob = exp(log.weights))
  particles <- particles[,index]
  
  return(list(particles = particles, 
              pred.mean = pred.mean, 
              pred.var = pred.var,
              log.weights = log.weights))
}
