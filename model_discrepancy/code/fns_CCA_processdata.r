#
# Functions to handle data processing of CCA
#

data.bias = function(t, bias, analysis, bibfa = NULL) {
  if(is.null(bibfa)) {
    # If there is no BIBFA model, there is no bias
    bias$ensemble[,,t] = 0
    bias$mean[t,] = 0
    bias$var[t,] = 0
  } else {
    # If there is a BIBFA model
    new.input = list(discrepancy = NULL, 
                     analysis = t(analysis$ensemble[,,t-1] - bibfa$mean$analysis))
    prediction = GFApred(c(0,1), new.input, bibfa, sample = TRUE, nSample = 1)$sam$Y[[1]][1,,]
  
    bias$ensemble[,,t] = t(prediction) + bibfa$mean$discrepancy
    bias$mean[t,] = rowMeans(bias$ensemble[,,t])
    bias$var[t,] = apply(bias$ensemble[,,t], 1, var)
  }
  
  return(bias)
}

data.forecast.bias = function(t, forecast.bias, forecast, bias, inflation.factor = 1) {
  forecast.bias$ensemble[,,t] = inflate(forecast$ensemble[,,t] + bias$ensemble[,,t], inflation.factor = inflation.factor)
  forecast.bias$mean[t,] = rowMeans(forecast.bias$ensemble[,,t])
  forecast.bias$var[t,] = apply(forecast.bias$ensemble[,,t], 1, var)
  
  return(forecast.bias)
}

bibfa.update = function(t, y, forecast, analysis, opts) {
  Y.input = 
    list(discrepancy = t(do.call("cbind", lapply(2:t, function(t) y[t,] - forecast$ensemble[,,t]))),
         analysis = t(do.call("cbind", lapply(2:t, function(t) analysis$ensemble[,,t-1]))))
  Y.input = lapply(Y.input, scale, scale = FALSE)
  
  bibfa = GFAexperiment(Y = Y.input, K = opts$K, opts = opts, Nrep = opts$Nrep)
  mean = list(mean = lapply(Y.input, function(x) attr(x, "scaled:center")))
  
  return(c(bibfa, mean))
}

plot.tau = function(bibfa.results) {
  tau = sapply(bibfa.results, function(x) if(!is.null(x)) x$tau else rep(NA,2))
  if(!all(is.na(tau))) {
    par(mar = c(2, 2, 0, 0))
    matplot(t(1/sqrt(tau)), type = "l", col = c("blue", "red"), lty = "solid", lwd = 2)
    legend("bottomleft", legend = c("discrepancy", "analysis"), col = c("blue", "red"), lwd = 2, cex = .75)
  }
}

