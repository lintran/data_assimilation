
source("model_discrepancy/code/lorenz63/lorenz63.r")

multiplier = 3
parms = c(sigma = 10, rho = 28, beta = 8/3) # theta
times = seq(0, 30, by = 0.01)

plot.setup <- function(mat, theta = 40, phi = -5){
  persp(z = matrix(0, nrow = 2, ncol = 2), xlim = range(mat[,1]),
        ylim = range(mat[,2]), zlim = range(mat[,3]),
        theta = theta, phi = phi, zlab = "Z")
}

col.transparent = function(col, alpha = 100) {
  col.rgb = col2rgb(col)
  apply(col.rgb, 2, function(x) rgb(x[1], x[2], x[3], alpha, maxColorValue = 255))
}

library(RColorBrewer)
col = brewer.pal(4, "Set1")[c(1,2,4)]
col = col.transparent(col, 90)
col.trans = col.transparent(col, 75)

# solutions --------------------------------------------------------------------
start1 = rep(1,3)
start2 = start1 + sample(c(-1,1), 3, replace = TRUE)*.1

sol1 = adv.model(y = start1, times = times, func = deriv, parms = parms)[,2:4]
sol2 = adv.model(y = start2, times = times, func = deriv, parms = parms)[,2:4]
solw = adv.model(y = start1, times = times, func = deriv2, parms = parms)[,2:4]

# different initial conditions -------------------------------------------------
par(mar = rep(0, 4))
pmat <- plot.setup(rbind(sol1, sol2))
newcoords <- with(list(pmat = pmat), function(mat) { # For adding to the plot
  trans3d(x = mat[,1], y = mat[,2], z = mat[,3], pmat = pmat)
})

png("model_discrepancy/plots/lorenz63/diff_inits/image%d.png")
for(t in seq(1, length(times), 10)) {
  plot.setup(rbind(sol1,sol2))
  lines(newcoords(sol1[1:t,,drop = FALSE]), col = col.trans[1], lwd = multiplier/2)
  points(newcoords(sol1[t,,drop = FALSE]), col = col[1], pch = 15, cex = multiplier)
  lines(newcoords(sol2[1:t,,drop = FALSE]), col = col.trans[2], lwd = multiplier/2)
  points(newcoords(sol2[t,,drop = FALSE]), col = col[2], pch = 16, cex = multiplier)
}
dev.off()

# right vs wrong ---------------------------------------------------------------
par(mar = rep(0, 4))
pmat <- plot.setup(rbind(sol1, solw))
newcoords <- with(list(pmat = pmat), function(mat) { # For adding to the plot
  trans3d(x = mat[,1], y = mat[,2], z = mat[,3], pmat = pmat)
})


png("model_discrepancy/plots/lorenz63/wrong_model/image%d.png")
for(t in seq(1, length(times), 10)) {
  plot.setup(rbind(sol1,solw))
  lines(newcoords(sol1[1:t,,drop = FALSE]), col = col.trans[3], lwd = multiplier/2)
  points(newcoords(sol1[t,,drop = FALSE]), col = col[3], pch = 15, cex = multiplier)
  lines(newcoords(solw[1:t,,drop = FALSE]), col = col.trans[1], lwd = multiplier/2)
  points(newcoords(solw[t,,drop = FALSE]), col = col[1], pch = 16, cex = multiplier)
  legend("topleft", legend = c("right", "wrong"), lwd = rep(multiplier/2, 2), col = col[c(3,1)], pch = c(15,16), box.lwd = 0)
}
dev.off()






