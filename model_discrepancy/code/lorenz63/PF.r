
# Model description ------------------------------------------------------------
#             X_0 ~ MVN(0_3, I_3)
#   X_t | X_{t-1} ~ Phi_t[ MVN(X_{t-1}, tau I_3), theta ]
#       Y_t | X_t ~ MVN(X_t, sigma I_3)
# where Phi_t(cdot, theta) is the physical model.
#
# Code adapted from CGK's file:
#  ~/Dropbox/Grad school/Research/model_error/toy_models/LorenzModels.particle.R
#

# Load libraries and functions -------------------------------------------------
library(deSolve)
library(mvtnorm)

source("model_discrepancy/code/fns_particle_filter.r")
source("model_discrepancy/code/lorenz63/lorenz63.r")
load("model_discrepancy/data/lorenz63/lorenz63.rdata")

# Particle filter; correct model -----------------------------------------------

M <- 100 # Number of particles
tau <- 1 # sd for perturbation in forward propogation step
samps <- matrix(rnorm(M*3), ncol = M) # Initialize the particles
pred <- predvar <- matrix(NA, ncol = length(times), nrow = 3)

for(i in 1:length(times)){
  print(i)
  
  # One step in particle filter
  pf.output = pf.step(particles = samps, y = y[i,], dt = times[2],
                      sigma = sigma, tau = tau, model = deriv, parms = parms)
  samps = pf.output$samps
  pred[i,] = pf.output$pred.mean
  predvar[i,] = pf.output$pred.var
  
  # Plot
  plot.setup(sol)
  lines(newcoords(sol), col = "gray")
  points(newcoords(x[i,,drop = FALSE]), pch = 16)
  points(newcoords(samps), col = "blue", pch = ".")
  points(newcoords(pred[i, , drop = FALSE]), pch = 16, col = "blue")
  #dev.print(pdf, file = paste("~/Desktop/stills/", i, ".pdf", sep = "", height = 6, width = 6)
  #pause()
}

par(mfrow = c(3, 1))
for(i in 1:3) {
  plot(x[,i], type = "l")
  lines(pred[,i], col = "blue")
}
par(mfrow = c(1,1))
  

# Particle filter; wrong model -------------------------------------------------
  
m <- 100 # Number of particles
tau <- 1 # sd for perturbation in forward propogation step
samps <- rmvnorm(n = m, mean = rep(0, 3)) # Initialize the particles
pred <- predvar <- matrix(NA, nrow = length(times), ncol = 3)

for(i in 1:length(times)) {
  print(i)
  
  # One step in particle filter
  pf.output = pf.step(samps = samps, sigma = sigma, tau = tau, model = deriv2, parms = parms2)
  samps = pf.output$samps
  pred[i,] = pf.output$pred.mean
  predvar[i,] = pf.output$pred.var
  
  # Plot
  plot.setup(sol)
  lines(newcoords(sol), col = "gray")
  points(newcoords(x[i,,drop = FALSE]), pch = 16)
  points(newcoords(samps), col = "blue", pch = ".")
  points(newcoords(pred[i, , drop = FALSE]), pch = 16, col = "blue")
  #dev.print(pdf, file = paste("~/Desktop/stills/", i, ".pdf", sep = "", height = 6, width = 6)
  #pause()
}

par(mfrow = c(3, 1))
for(i in 1:3){
  plot(x[,i], type = "l")
  lines(pred[,i], col = "blue")
}
par(mfrow = c(1,1))

# Compare error ----------------------------------------------------------------
errors <- pred - x
colnames(errors) <- paste(colnames(errors), "err", sep = "")
pairs(cbind(x, errors))
pairs(cbind(pred, errors))
