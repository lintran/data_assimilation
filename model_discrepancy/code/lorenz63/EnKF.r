
# Model description ------------------------------------------------------------
#             X_0 ~ MVN(0_N, I_N)
#   X_t | X_{t-1} ~ w_t( X_t-1, parms )
#       Y_t | X_t ~ MVN(X_t, sigma^2 I_N)
# where w_t(cdot, parms) is Lorenz 2005 Model 2.
#

right = FALSE

# Load libraries and functions -------------------------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz63/lorenz63.r")

# Initialization ---------------------------------------------------------------
# data and parameters
load("model_discrepancy/data/lorenz63/lorenz63.rdata")

# output info
forecast <- analysis <- array(dim = c(N, M+2, T))
dimnames(forecast) <- dimnames(analysis) <-
  list(state = NULL,
       ensemble = c(sprintf("e%d", 1:M), "mean", "var"),
       time = NULL)

analysis[,1:M,1] <- ensemble.ics # Initialize the particles
analysis[,M+1,1] = rowMeans(analysis[,1:M,1])
analysis[,M+2,1] = apply(analysis[,1:M,1], 1, var)

# arguments for forecast and update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "ETKF")

# objects to save
data.filename = sprintf("model_discrepancy/data/lorenz63/EnKF_%s.rdata", ifelse(right, "right", "wrong"))
plot.filename = sprintf("model_discrepancy/plots/lorenz63/EnKF_%s/image%%d.png", ifelse(right, "right", "wrong"))
save.list = c("forecast", "analysis",
              "args.update",
              "parms", "N", "M", "T", "t",
              "data.filename", "plot.filename")

# Run EnKF ---------------------------------------------------------------------
for(t in 2:T) {
  print(t)
  
  # Forecast: Propagate the ensemble members forward
  forecast[,,t] = data.forecast(analysis.ensemble = analysis[,1:M,t-1], 
                                adv.model = adv.model, times = times[1:2], 
                                func = if(right) deriv else deriv2, parms = parms,
                                inflation.factor = 2,
                                N = N, M = M)
  
  # Update: Obtain analysis distribution
  analysis[,,t] = data.update(forecast.ensemble = forecast[,1:M,t], 
                              y = y[t,,drop = FALSE], 
                              args = args.update, N = N, M = M)
}

save(list = save.list, file = data.filename)
