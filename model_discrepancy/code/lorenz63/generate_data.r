
# Code adapted from CGK's file:
#  ~/Dropbox/Grad school/Research/model_error/toy_models/LorenzModels.particle.R

multiplier = 3

library(mvtnorm)
source("model_discrepancy/code/lorenz63/lorenz63.r")

parms = c(sigma = 10, rho = 28, beta = 8/3) # theta
start = rep(.01,3)
times = seq(0, 30, by = 0.01)

# The true solution, X_t - high temporal resolution ----------------------------
#    Output: sol, newcoord

names(start) <- c("X", "Y", "Z")
sol <- adv.model(y = start, times = times, func = deriv, parms = parms)[,2:4]

plot.setup <- function(mat, theta = 40, phi = -5){
  persp(z = matrix(0, nrow = 2, ncol = 2), xlim = range(mat[,1]),
        ylim = range(mat[,2]), zlim = range(mat[,3]),
        theta = theta, phi = phi, zlab = "Z")
}

par(mar = rep(0, 4))
pmat <- plot.setup(sol)
newcoords <- with(list(pmat = pmat), function(mat) { # For adding to the plot
  trans3d(x = mat[,1], y = mat[,2], z = mat[,3], pmat = pmat)
})

png("model_discrepancy/plots/lorenz63/lorenz63_true.png")
plot.setup(sol)
lines(newcoords(sol), col = "gray", lwd = multiplier/2)
#points(newcoords(matrix(start,ncol=3)), pch = 19)
dev.off()

# Generate the data, Y_t - lower temporal resolution ---------------------------
#    Output: sigma, x
sigma = 3 # Standard deviation of the observations in each coordinate
sigma.sq = sigma^2
Sigma.0 = diag(sigma.sq, 3)
Sigma.0.chol = chol(Sigma.0)
Sigma.0.inv = chol2inv(Sigma.0.chol)

times <- seq(0, 30, by = 0.1)
x <- sol[seq(1, nrow(sol), by = 10),]
y <- x + matrix(rnorm(prod(dim(x)), sd = sigma), ncol = 3) # Observations

png("model_discrepancy/plots/lorenz63/lorenz63_obs.png")
plot.setup(sol)
lines(newcoords(sol), col = "gray", lwd = multiplier/2)
points(newcoords(y), cex = 1, pch = 19, col = rgb(0,0,0,.3))
dev.off()

# Save ------------------------------------------------------------------------
M = 10
T = length(times)
N = 3

mean = 4
ensemble.ics = matrix(rnorm(M*N,mean), nrow = N, ncol = M)

png("model_discrepancy/plots/lorenz63/lorenz63_ics.png")
plot.setup(sol)
lines(newcoords(sol), col = "gray")
points(newcoords(matrix(start,ncol=3)), pch = 19)
points(newcoords(t(ensemble.ics)), pch = 19, col = "blue", cex = .2)
dev.off()

save(x, y, Sigma.0, Sigma.0.inv, Sigma.0.chol, sigma,
     times, ensemble.ics, N, M, T, parms, plot.setup, sol, newcoords,
     file = "model_discrepancy/data/lorenz63/lorenz63.rdata")



