################################################################################
#
# Create "empty notebook" for DART to fill in times and observations
#  https://www.image.ucar.edu/DAReS/DART/DART_Observations.php#obs_seq_overview
#  https://www.image.ucar.edu/DAReS/DART/DART_Observations.php#obs_synthetic
#
# 1) Run "./create_obs_sequence" to create the first state.  Output: set_def.out.
#    "set_def.out" should look like (the numbers before each line is the line
#    number:
#      1 obs_sequence
#      2 obs_kind_definitions
#      3 0
#      4 num_copies:            0  num_qc:            0
#      5 num_obs:            1  max_num_obs:            1
#      6 first:            1  last:            1
#      7 OBS            1
#      8 -1          -1          -1
#      9 obdef
#     10 loc1d
#     11 0.0000000000000000
#     12 kind
#     13 -1
#     14 0          0
#     15 1.0000000000000000     
#
# 2) Run this R file to fill in the other states.
# 3) ./create_fixed_network_sequence
# 4) ./perfect_model_obs
#
################################################################################

obs = 2.5
var = 1

locname = c("x", "y", "z")
locx = (0:2)/3
loc = c(locx[3], obs/3)

N = length(loc)

# Read in set_def.out ----------------------------------------------------------
raw.data = readLines("~/data_assimilation/DART/Kodiak/models/lorenz_63/obs/one_obs.out")
output = raw.data

# Change the number of observations --------------------------------------------

# Make sure the leading zeroes match up
N.txt = formatC(1, format = "d", width = nchar(N))

# Replace
idx = grep("num_obs", raw.data)
output[idx] = gsub(N.txt, N, raw.data[idx])

idx = grep("last", raw.data)
output[idx] = gsub(paste("(*last:[[:blank:]]{1,})", N.txt, sep = ""), paste("\\1", N, sep = ""), raw.data[idx])

# Add observation blocks -------------------------------------------------------
idx = grep("OBS", raw.data)
obs.block = raw.data[idx:length(raw.data)]
obs.block[length(obs.block)] = formatC(var, format = "f", width = 21, digits = 16)

output = c(list(output[1:(idx-1)]), lapply(1:N, function(x) obs.block))

for(n in 1:N) {
  # Change observation number
  n.txt = formatC(1, format = "d", width = nchar(n))
  output[[n+1]][1] = 
    gsub(paste("(*OBS[[:blank:]]{1,})", n.txt, sep = ""), 
         paste("\\1", n, sep = ""), 
         obs.block[1])
  
  # Change which observations go before and after this particular observation
  output[[n+1]][3] = 
    gsub("([[:blank:]]{1,})-1([[:blank:]]{1,})-1*", 
         paste("\\1", ifelse(n == 1, -1, n-1), "\\2", ifelse(n == N, -1, n+1)), 
         obs.block[3])
  
  # Change location of observation
  output[[n+1]][6] = formatC(loc[n], format = "f", width = 21, digits = 16)    
}

output = unlist(output)
fileCon = file(sprintf("~/data_assimilation/DART/Kodiak/models/lorenz_63/obs/obs%02i.out", as.integer(obs*10)))
writeLines(output, fileCon)
close(fileCon)


