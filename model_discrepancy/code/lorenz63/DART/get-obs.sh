#!/bin/bash

for obs in $(seq -w 00 05 25)
do
  if [ "${obs}" -eq 20 ]; then continue; fi
  ./get-obs.r perfect/obs${obs}
done
