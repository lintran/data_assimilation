#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("folder", action="store", type = "character")
args = parser$parse_args()
# args = parser$parse_args(c("perfect/obs25"))

################################################################################
#
# Converts DART's ASCII files to obtain observations and the initial conditions
# of the ensemble.  ASCII files of interest:
#   input.nml     : contains Lorenz 05 model parameters
#   obs_seq.out   : contains observations (with measurment error)
#   perfect_ics   : contains the initial conditions
#   True_State.nc : ditto
#   filter_ics    : contains the initial conditions of the ensemble members,
#
################################################################################

DARTdir = paste0("~/data_assimilation/DART/Kodiak/models/lorenz_63/", args$folder, "/")

# functions and constants ------------------------------------------------------
N = 3
days.to.sec = 24*60*60
sec.to.days = 1/days.to.sec

DARTtime.regex = "^[[:blank:]]{1,}[[:digit:]]{1,}[[:blank:]]{1,}[[:digit:]]{1,}[[:blank:]]{0,}$"

convert.DARTtime = function(idx, text) {
  temp = lapply(strsplit(text[idx], "[[:blank:]]{1,}"), as.numeric)
  sapply(temp, function(x) x[2]*sec.to.days + x[3])
}

eval.parse = function(text) eval(parse(text = text))

loc.convert = function(loc, N) loc*N+1

extract.number = function(idx, text) {
  # extracts number from raw text. only works for text with one value to be extracted
  as.numeric(gsub("[^[:digit:]\\.-]", "", text[idx]))
}

find.and.extract.number = function(find, text) extract.number(grep(find, text), text)

# get observations: obs_seq.out ------------------------------------------------
raw.data = readLines(paste(DARTdir, "obs_seq.out", sep = ""))

# Get number of variables. Usually observations, truth, quality control.
idx = grep("num_copies", raw.data)
nvar = eval.parse(gsub("[[:blank:]]{1,}num_copies:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}num_qc:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1+\\2", raw.data[idx]))

# Get number of observations
idx = grep("num_obs", raw.data)
nobs = as.numeric(gsub("[[:blank:]]{1,}num_obs:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}max_num_obs:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1", raw.data[idx]))

# Create data matrix
obs = matrix(nrow = nobs, ncol = nvar+7)
colnames(obs) = c("days", "seconds", "loc.raw", "loc", "H.raw", "H", "var", gsub(" ", "", raw.data[(idx+1):(idx+nvar)]))
obs.idx = 7 + 1:nvar

# Fill in variables
idx = grep("OBS", raw.data)
if(length(idx) != nobs) stop("Number of 'OBS' do not equal the number of observations as stated by 'num_obs'")
for(i in 1:nvar) obs[,obs.idx[i]] = as.numeric(gsub(" ", "", raw.data[idx+i]))

# Get locations
idx = grep("loc1d", raw.data)
if(length(idx) != nobs) stop("Number of 'loc1d' do not equal the number of observations as stated by 'num_obs'")
obs[,"loc.raw"] = as.numeric(gsub(" ", "", raw.data[idx+1]))
obs[,"loc"] = loc.convert(obs[,"loc.raw"], N)

# Get time
partial = length(grep("partial", DARTdir)) > 0
idx = grep("^kind", raw.data)
if(length(idx) != nobs) stop("Number of 'kind' do not equal the number of observations as stated by 'num_obs'")
if(partial) {
  temp = strsplit(raw.data[idx[1]+2], "[[:blank:]]{1,}")[[1]]
  halfwidth = as.numeric(temp[2])
  num_points = as.numeric(temp[3])
  idx = idx + 2
} else {
  halfwidth = 0
  num_points = 0
}
temp = lapply(strsplit(raw.data[idx+2], "[[:blank:]]{1,}"), as.numeric)
obs[,"days"] = sapply(temp, function(x) x[3])
obs[,"seconds"] = sapply(temp, function(x) x[2])

# Get variance
obs[,"var"] = as.numeric(gsub(" ", "", raw.data[idx+3]))

# Get H
obs[,"H.raw"] = as.numeric(gsub(" ", "", raw.data[idx+1]))
obs[,"H"] = as.numeric(obs[,"H.raw"] < 0)

# convert obs and truth --------------------------------------------------------
obs = cbind(t = obs[,"seconds"]*sec.to.days + obs[,"days"], obs)
#obs = obs[order(obs[,"t"], obs[,"loc.raw"]),]

N = length(unique(obs[,"loc.raw"]))
T = length(unique(obs[,"t"]))
obs.mat = matrix(obs[,"observations"], nrow = N, ncol = T)
truth.mat = matrix(obs[,"truth"], nrow = N, ncol = T)

# save -------------------------------------------------------------------------
library(R.matlab)
save(obs, obs.mat, file = paste0(DARTdir, "obs.rdata"))
writeMat(paste0(DARTdir, "obs.mat"), 
         obs = obs.mat,
         truth = truth.mat,
         var = obs[obs[,"t"] == obs[1,"t"],"var"],
         loc = unique(obs[,"loc.raw"]),
         halfwidth = halfwidth,
         num_points = num_points,
         verbose = -1)


