################################################################################
#
# Converts DART's ASCII files to obtain observations and the initial conditions
# of the ensemble.  ASCII files of interest:
#   input.nml     : contains Lorenz 05 model parameters
#   obs_seq.out   : contains observations (with measurment error)
#   perfect_ics   : contains the initial conditions
#   True_State.nc : ditto
#   filter_ics    : contains the initial conditions of the ensemble members,
#
################################################################################

DARTdir = "~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_63/work/"

# functions and constants ------------------------------------------------------
extract.number = function(idx, text) {
  # extracts number from raw text. only works for text with one value to be extracted
  as.numeric(gsub("[^[:digit:]\\.-]", "", text[idx]))
}

find.and.extract.number = function(find, text) extract.number(grep(find, text), text)

# get model characteristics: input.nml ---------------------------------------------
input = readLines(paste(DARTdir, "input.nml", sep = ""))
input.parms = list()

nml.start = grep("^\\&[[:alpha:]_]+", input)
nml.end = c(nml.start[-1]-1, length(input))
names(nml.start) <- names(nml.end) <- input[nml.start]

model.nml = input[(nml.start["&model_nml"]+1):nml.end["&model_nml"]]
perfect.nml = input[(nml.start["&perfect_model_obs_nml"]+1):nml.end["&perfect_model_obs_nml"]]
filter.nml = input[(nml.start["&filter_nml"]+1):nml.end["&filter_nml"]]

N = 3

# get perfect initial conditions: perfect_ics and True_State.nc ----------------
inits = list(perfect = NULL,
             perturbed = NULL)

# one source: get it from True_State.nc
# library(RNetCDF)
# nc = open.nc(paste(DARTdir, "True_State.nc", sep = ""))
# truth = read.nc(nc)
# close.nc(nc)

# second source: get it from perfect_ics. data is in row-major order.
raw.data = readLines(paste(DARTdir, "perfect_ics", sep = ""))
con = textConnection(gsub("D", "e", raw.data[-1]))
perfect = as.numeric(t(read.fwf(file = con, widths = rep(24,1))))
close(con)

# check if both sources give the same answer
# all.equal(truth$state[,1], perfect) # TRUE

# fill in inits$perfect
inits$perfect = perfect

# get time of initial conditions: check &perfect_model_obs_nml > init_time_days
# and init_time_seconds.  If the values are nonnegative, then use those values
# for time of initial conditions, else use the values in perfect_ics.
ics.time = find.and.extract.number("init_time_(days|seconds)", perfect.nml)
inits$day = ics.time[1]
inits$seconds = ics.time[2]

# get initial conditions of each ensemble member: filter_ics -------------------
raw.data = readLines(paste(DARTdir, "filter_ics", sep = ""))
init.time = raw.data[1]

# Get starting index of each ensemble member
start = grep(init.time, raw.data)
end = c(start[-1]-1, length(raw.data))
Mbig = length(start)

# create data
inits$perturbed = matrix(ncol = Mbig, nrow = N)

# get values of ensemble members. data is in row-major order.
file.create("temp.txt")
con = file("temp.txt")
writeLines(gsub("D", "e", raw.data[1:end[Mbig]][-start[1:Mbig]]), con)
close(con)

vals = read.fwf("temp.txt", widths = rep(24,1))
nr = nrow(vals)/Mbig
for(m in 1:Mbig) {
  start = (m-1)*nr+1
  end = m*nr
  inits$perturbed[,m] = as.numeric(t(vals[start:end,]))
}

file.remove("temp.txt")
rm(raw.data); gc()

# save -------------------------------------------------------------------------
library(R.matlab)
save(inits, file = paste0(DARTdir, "inits.rdata"))
writeMat(paste0(DARTdir, "inits.mat"), perfect = inits$perfect, perturbed = inits$perturbed)
