
# Code adapted from CGK's file:
#  ~/Dropbox/Grad school/Research/model_error/toy_models/LorenzModels.particle.R

library(deSolve)

# Lorenz Model 1963 ------------------------------------------------------------
# http://en.wikipedia.org/wiki/Lorenz_system
#   dx/dt = sigma (y-x)
#   dy/dt = x(rho - z) - y
#   dz/dt = xy - beta*z

# Creating Phi_true and Phi_wrong ----------------------------------------------

# Phi_true: Lorenz 63 model
deriv <- function(t, y, parms = c(sigma = 10, rho = 28, beta = 8/3)) { 
  dX <- parms["sigma"] * (y[2] - y[1])
  dY <- - y[3] * y[1] + parms["rho"] * y[1] - y[2]
  dZ <- y[1] * y[2] - parms["beta"] * y[3]
  return(list(c(dX = dX, dY = dY, dZ = dZ)))
}

# Phi_wrong
deriv2 <- function(t, y, parms = c(sigma = 10, rho = 28, beta = 8/3, alpha = 1)) {
  dX <- parms["sigma"] * (y[2] - y[1])
  dY <- - y[3] * y[1] + parms["rho"] * y[1] - y[2]
  dZ <- parms["alpha"] * y[1] * y[2] - parms["beta"] * y[3]
  return(list(c(dX = dX, dY = dY, dZ = dZ)))
}

adv.model = function(y, times, func, parms) {
  lsoda(y = y, times = times, func = func, parms = parms)
}

# Jacobian of Phi_wrong. Output: M_ij = \partial M_i / partial x_j ( \mu_t^a), where M is Phi_wrong and \mu_t^a is the analysis mean
deriv2.jac <- function(y, parms) {
  out <- matrix(nrow = 3, ncol = 3)
  out[1, ] <- c(  -parms["sigma"],    parms["sigma"],       0        )
  out[2, ] <- c( parms["rho"] - y[3],     -1,            -y[1]       )
  out[3, ] <- c(     3*y[2],            3*y[1],       -parms["beta"] )
  return(out)
}




