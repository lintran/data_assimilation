
sampleW = function(m, old, data, consts) {
  precW = diag(1/old$alpha[m,])
  for(n in 1:consts$N) precW = precW + old$tau[m] * tcrossprod(old$y[n,])
  R = chol(precW)
  
  mu.T = apply(data[[m]], 2, function(xd) colSums(xd * old$y))
  # This line in words: multiply each row y[n,] with data[[m]][n,d] and then sum across columns.
  # Note: Klami's code has it such that mu = crossprod(data[[m]], old$y). Not true.
  mu.T = solve(precW, old$tau[m] * mu.T)
  
  t(mu.T + solve(R, matrix(rnorm(consts$K * consts$D[m]), ncol = consts$D[m], nrow = consts$K)))
}

sampleY = function(old, consts, data) {
  precY = diag(1, consts$K)
  for(m in 1:consts$M) precY = precY + old$tau[m] * crossprod(old$W[[m]])
  R = chol(precY)
  
  mu = old$tau[1] * data[[1]] %*% old$W[[1]]
  for(m in 2:consts$M) mu = mu + old$tau[m] * data[[m]] %*% old$W[[m]]
  mu.T = solve(precY, t(mu))
  
  t(mu.T + solve(R, matrix(rnorm(consts$K * consts$N), ncol = consts$N, nrow = consts$K)))
}

sampleTau = function(m, old, data, consts, prior) {
  Wy = apply(old$y, 1, function(y) old$W[[m]] %*% y)
  yWWy = sum(apply(Wy, 2, crossprod))
  xWy = sum(data[[m]] * t(Wy))
  
  beta = prior$tau$beta + 0.5 * (consts$XX[m] - 2*xWy + yWWy)
  rgamma(1, shape = consts$post.alpha$tau[m], rate = beta)
}

sampleAlpha = function(m, old, prior) {
  beta = prior$alpha$beta + 0.5 * apply(old$W[[m]], 2, crossprod)
  sapply(beta, function(b) rgamma(1, shape = consts$post.alpha$alpha[m], rate = b))
}

parmsPostPredSamp = function(pred, post.params, consts) {
  # Co-opted from fns_onlineCCA.r / GFApred
  
  tr = which(pred==1)  # The observed data sets
  pr = which(pred==0)  # The data sets that need to be predicted
  
  nSample = dim(post.params$y)[1]
  sam = lapply(1:nSample, function(sample) list())
  
  for(i in 1:nSample) {
    # Get W and tau
    sam[[i]]$W = lapply(1:consts$M, function(m) post.params$W[[m]][i,,])
    sam[[i]]$tau = post.params$tau[i,]
    
    # Get (Sigma[tr] + W[tr] W[tr]^T)^{-1} W[tr] = tau[tr] (I + tau[tr] W[tr] W[tr]^T)^{-1} W[tr]
    Wpred = vector("list", consts$M)
    for(m in tr) Wpred[[m]] = solve(diag(1/sam[[i]]$tau[m], consts$D[m]) + tcrossprod(sam[[i]]$W[[m]]), sam[[i]]$W[[m]])
    
    # Calculate Var(Y | X[tr]) = I - Sum_{i in tr} I - Wi^t (Sigmai + WiWi^t)^-1 Wi
    varYgiventr = diag(consts$K)
    for(m in tr) varYgiventr = varYgiventr - crossprod(sam[[i]]$W[[m]], Wpred[[m]])
    sam[[i]]$cholvarYgiventr = chol(varYgiventr)
    
    # new W for training data
    for(m in tr) sam[[i]]$W[[m]] = Wpred[[m]]
  }
  
  return(
    list(params = list(nSample = nSample, tr = tr, pr = pr, D = consts$D, K = consts$K, mean = consts$mean), 
         sam = sam)
  )
}

postPredSampler = function(Xnew, params, sam) {
  N = nrow(Xnew[[params$tr[1]]])
  idx.sample = sample(length(sam), N, replace = TRUE)
  
  out = lapply(Xnew, function(m) list())
  for(m in params$pr) out[[m]] = matrix(nrow = N, ncol = params$D[m])
  
  for(n in 1:N) {
    i = idx.sample[n]
    
    # Sample from Y | X[tr]
    Y = sam[[i]]$cholvarYgiventr %*% rnorm(params$K)
    for(m in params$tr) Y = Y + crossprod(sam[[i]]$W[[m]], Xnew[[m]][n,] - params$mean[[m]])
    
    # Sample from X[pr] | X[tr]
    for(m in params$pr) out[[m]][n,] = params$mean[[m]] + sam[[i]]$W[[m]] %*% Y + rnorm(params$D[m], 0, 1/sqrt(sam[[i]]$tau[m]))
  }
  
  return(out)
}

calcL = function(pred, post.params, consts, idx.sample = NULL) {
  tr = which(pred==1)  # The observed data sets
  pr = which(pred==0)  # The data sets that need to be predicted
  
  if(is.null(idx.sample)) {
    nSample = dim(post.params$y)[1]
    idx.sample = 1:nSample
  } else {
    nSample = length(idx.sample)
  }
  
  L = lapply(1:consts$M, function(pr) lapply(1:consts$M, function(tr) list()))
  for(p in pr) for(t in tr) L[[p]][[t]] = array(dim = c(nSample, consts$D[p], consts$D[t]))
  
  b = vector("list", consts$M)
  for(p in pr) b[[p]] = t(matrix(consts$mean[[p]], nrow = consts$D[p], ncol = nSample))
  
  Sigma = vector("list", consts$M)
  for(p in pr) Sigma[[p]] = array(0, dim = c(nSample, consts$D[p], consts$D[p]))
  Sigma.pr <- Sigma.tr <- Sigma
  
  for(i in 1:nSample) {
    # Get W and tau
    W = lapply(1:consts$M, function(m) post.params$W[[m]][idx.sample[i],,])
    tau = post.params$tau[idx.sample[i],]
    
    # Get (Sigma[tr] + W[tr] W[tr]^T)^{-1} W[tr] = tau[tr] (I + tau[tr] W[tr] W[tr]^T)^{-1} W[tr]
    Wpred = vector("list", consts$M)
    for(m in tr) Wpred[[m]] = solve(diag(1/tau[m], consts$D[m]) + tcrossprod(W[[m]]), W[[m]])
    
    # Get L = W[pr] sum_tr W[tr]^T (Sigma[tr] + W[tr] W[tr]^T)^{-1} W[tr] = tau[tr] (I + tau[tr] W[tr] W[tr]^T)^{-1}
    for(p in pr) {
      Sigma.pr[[p]][i,,] = diag(1/tau[p], consts$D[p]) + tcrossprod(W[[p]])
      
      for(t in tr) {
        L[[p]][[t]][i,,] = tcrossprod(W[[p]], Wpred[[t]])
        b[[p]][i,] = b[[p]][i,] - L[[p]][[t]][i,,] %*% consts$mean[[t]]
        Sigma.tr[[p]][i,,] = Sigma.tr[[p]][i,,] + L[[p]][[t]][i,,] %*% tcrossprod(W[[t]], W[[p]])
      }
      
      Sigma[[p]][i,,] = Sigma.pr[[p]][i,,] + Sigma.tr[[p]][i,,]
    }    
  }
  
  for(p in pr) L[[p]][[p]] = NULL
  for(t in tr) L[[t]] <- b[[t]] <- Sigma.pr[[t]] <- Sigma.tr[[t]] <- Sigma[[t]] <- NULL
  
  return(list(pr = pr, tr = tr, 
              L = L, b = b, 
              Sigma = Sigma, Sigma.pr = Sigma.pr, Sigma.tr = Sigma.tr))
}


