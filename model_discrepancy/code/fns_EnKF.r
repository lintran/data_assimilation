#
# Used algorithm outlined in "Data Assimilation" by Nychka and Anderson,
# published in Gelfand et al., pg. 484
#

require(deSolve)
require(mvtnorm)
require(spam)

# Update step, analysis distribution

enkf.update = function(forecast.ensemble, y, y.perturbed = NULL,
                       H = NULL, Sigma.0, Sigma.0.inv = NULL,
                       method = "ETKF") {
  d = length(drop(y))
  n = ncol(forecast.ensemble)
  if(is.null(H)) H = diag(x = 1, nrow = d, ncol = d)
  
  U.f = t(scale(t(forecast.ensemble), center = TRUE, scale = FALSE))
  mu.f = attr(U.f, "scaled:center")
  Z.f = 1/sqrt(n-1) * U.f # Sigma.f = Z.f %*% t(Z.f) = cov(t(forecast.ensemble))
  H.Zf = H %*% Z.f
  KG.t = solve(tcrossprod(H.Zf) + Sigma.0, tcrossprod(H.Zf, Z.f)) # transpose of Kalman gain = [H Sigma_f H + Sigma_0]^-1 H Sigma_f
  
  # Method 1: perturbed observations
  if(method == "perturbed") {
    if(is.null(y.perturbed)) y.perturbed = t(rmvnorm(n = n, mean = y, sigma = Sigma.0))
    output = forecast.ensemble + crossprod(KG.t, y.perturbed - H %*% forecast.ensemble) # analysis ensemble, x_a = x_f + KG (y.perturbed - H x_f)
  } else 
  
  # Method 2: ensemble transform KF (ETKF), a type of square root filter (see equation 16 of Tipping et al, 2003, "Ensemble Square Root Filters") with correction made by Wang et al 2004 as discussed in Livings et al, 2008, "Unbiased ensemble square root filter", Equation 27)
  if(method == "ETKF") {
    mu.a = mu.f + crossprod(KG.t, matrix(y, ncol = 1) - H %*% mu.f)
    if(is.null(Sigma.0.inv)) Sigma.0.inv = solve(Sigma.0)
    eigen.decomp = eigen(t(H.Zf) %*% Sigma.0.inv %*% H.Zf)
    A = eigen.decomp$vectors %*% tcrossprod(diag(1/sqrt(eigen.decomp$values+1)), eigen.decomp$vectors)
    output = mu.a[,rep(1,n)] + U.f %*% A # analysis ensemble
  } else
  
  # Method 3: ensemble adjustment KF (EAKF), a type of square root filter (see equation 20 of Tipping et al, 2003, "Ensemble Square Root Filters")
  if(method == "EAKF") {
    mu.a = mu.f + crossprod(KG.t, matrix(y, ncol = 1) - H %*% mu.f)
    if(is.null(Sigma.0.inv)) Sigma.0.inv = solve(Sigma.0)
    eigen.decomp1 = eigen(t(H.Zf) %*% Sigma.0.inv %*% H.Zf)
    eigen.decomp2 = eigen(tcrossprod(Z.f))
    A = eigen.decomp1$vectors %*% tcrossprod(diag(1/sqrt(eigen.decomp1$values+1)), diag(1/sqrt(eigen.decomp2$values)) %*% tcrossprod(eigen.decomp2$vectors, Z.f))
    output = mu.a[,rep(1,n)] + U.f %*% A # analysis ensemble
  } else {
    stop("Don't recognize that particular 'method'.")
  }
  
  # Error checking
  if(FALSE) {
    I.VDV = diag(n) - crossprod(H.Zf, solve(tcrossprod(H.Zf) + Sigma.0, H.Zf))
    all.equal(cov(t(output)), Sigma.a)
  }
  
  return(output)
}

enkf.forecast = function(analysis.ensemble, 
                         adv.model, times, func, parms,
                         inflation.factor = 1, ...) {
  output = apply(analysis.ensemble, 2, function(member) {
    adv.model(y = member, times = times, func = func, parms = parms, ...)[2,-1]})
  
  output = inflate(output, inflation.factor)
  
  return(output)
}

inflate = function(data, inflation.factor = 1) {
  mu = rowMeans(data)
  apply(data, 2, function(x) sqrt(inflation.factor) * (x - mu) + mu)
}

enks = function(analysis.ensemble, smoothed.ensemble, forecast.ensemble, 
                method = "linearize", jacobian = NULL, dt = NULL, parms = NULL,
                taper.Sigma.f = NULL, taper.Sigma.fa = NULL,
                error.checking = FALSE) {
  n = ncol(forecast.ensemble)
  
  Z.a = 1/sqrt(n-1) * t(scale(t(analysis.ensemble), center = TRUE, scale = FALSE)) # Sigma.a = Z.a %*% t(Z.a) = cov(t(analysis.ensemble))
  mu.a = attr(Z.a, "scaled:center")
  
  Z.f = 1/sqrt(n-1) * t(scale(t(forecast.ensemble), center = TRUE, scale = FALSE))
  Sigma.f = tcrossprod(Z.f)
  if(!is.null(taper.Sigma.f)) Sigma.f = taper.Sigma.f * tcrossprod(Z.f)
  
  if(method == "linearize") {
    if(is.null(lin.model) | is.null(parms)) stop("Both 'lin.model' and 'parms' need to be defined if method == 'linearize'.")
    
    M = lin.model(y = mu.a, jacobian = jacobian, dt = dt, parms = parms)
    
    if(is.null(taper.Sigma.fa)) {
      B.t = solve(Sigma.f, M %*% tcrossprod(Z.a)) 
    } else {
      B.t = solve(Sigma.f, taper.Sigma.fa * M %*% tcrossprod(Z.a))
    }
  } else if(method == "ensemble") {
    # note: cov(forecast, analysis) = tcrossprod(Z.f, Z.a)
    if(is.null(taper.Sigma.fa)) {
      B.t = solve(Sigma.f, tcrossprod(Z.f, Z.a))
    } else {
      B.t = solve(Sigma.f, taper.Sigma.fa * tcrossprod(Z.f, Z.a))
    }
  } else {
    stop("Don't recognize that 'method'.")
  }

  # smoothed_t = analysis_t + B (smoothed_t+1 - forecast_t+1)
  if(error.checking) {
    diff = smoothed.ensemble - forecast.ensemble
    addon = crossprod(B.t, diff)
    output = analysis.ensemble + addon
    
    cat("  B     ", range(B.t), "\n")
    cat("    s-f ", range(diff), "\n")
    cat("  B(s-f)", range(addon), "\n")
    cat("    s-a", range(smoothed.ensemble - analysis.ensemble), "\n")
  } else {
    output = analysis.ensemble + crossprod(B.t, smoothed.ensemble - forecast.ensemble)
  }
  
  return(output)
}

lin.model <- function(y, jacobian, dt, parms) {
  M = dt * jacobian(y = y, parms = parms)
  diag(M) = diag(M) + 1
  return(M)
}

