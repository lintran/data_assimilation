
# MH update --------------------------------------------------------------------
#

library(mvtnorm)  # for dmvnorm
library(MCMCpack) # for dinvgamma

library(foreach)
library(fields)

p.mh = function(param, data, prior) {
  param = param.convert(param = param, data = data)
  
  immediate.fail = any(unlist(param[-1]) <= 0)
  
  if(immediate.fail) {
    output = -Inf
  } else {
    log.lik = lik(param = param, data = data)
    
    log.bs = c(beta = dmvnorm(param$beta, prior$beta$m, prior$beta$V, log = TRUE),
               sigma.sq = log(dinvgamma(param$sigma.sq, prior$sigma.sq$a, prior$sigma.sq$b)))
    
    log.X = c(rho = log(dinvgamma(param$X$rho, prior$X$rho$a, prior$X$rho$b)))
    
    log.t = c(c = log(dinvgamma(param$t$c, prior$t$c$a, prior$t$c$b)),
              rho = log(dinvgamma(param$t$rho, prior$t$rho$a, prior$t$rho$b)))
    
    output = list(lik = log.lik, 
                  other = list(sum(log.bs), sum(log.X), sum(log.t)))
  }
  
  return(output)
}

lik.slow = function(param, data) {
  K.X = corr.mat(corr.type = "matern", X = data$X, 
                 delta = param$X$rho, corr.args = list(smoothness = 3/2, reparam = TRUE))
  K.t = corr.mat(corr.type = "matern", X = data$t, 
                 delta = param$t$rho, corr.args = list(smoothness = 3/2, nugget = param$t$c, reparam = TRUE))
  K.stuff = K.extra(K = K.X %x% K.t, 
                    return.chol = TRUE, model.matrix = data$model.matrix)
  
  n = length(data$y)
  b2 = backsolve.chol(K.stuff$K.chol, data$y)
  a = crossprod(b2)
  if(!is.null(K.stuff$b1)) a = a - 2*crossprod(param$beta, crossprod(K.stuff$b1, b2)) + crossprod(param$beta, K.stuff$tH.Kinv.H) %*% param$beta
  
  log.exp = - 1/(2*param$sigma.sq) * a
  log.det = n * log(param$sigma.sq) + K.stuff$K.logdet
  log.lik = drop(- .5 * log.det + log.exp)
  
  return(log.lik)
}


lik = function(param, data) {
  K = list(
    X = corr.mat(corr.type = "matern", X = data$X, 
                 delta = param$X$rho, corr.args = list(smoothness = 3/2, reparam = TRUE)),
    t = corr.mat(corr.type = "matern", X = data$t, 
                 delta = param$t$rho, corr.args = list(smoothness = 3/2, nugget = param$t$c, reparam = TRUE)))
  K.chol = lapply(K, chol)
  K.logdet = sapply(K.chol, function(M) 2*sum(log(diag(M))))
  
  K.logdet = nrow(K.chol[[2]]) * K.logdet[1] + nrow(K.chol[[1]]) * K.logdet[2] # log(|K|)
  K.chol = K.chol[["X"]] %x% K.chol[["t"]] # R, where K = R^T R
  b1 = backsolve.chol(K.chol, data$model.matrix)
  tH.Kinv.H = crossprod(b1) # H^T K^-1 H
  
  n = length(data$y)
  b2 = backsolve.chol(K.chol, data$y)
  a = crossprod(b2)
  if(!is.null(b1)) a = a - 2*crossprod(param$beta, crossprod(b1, b2)) + crossprod(param$beta, tH.Kinv.H) %*% param$beta
  
  log.exp = - 1/(2*param$sigma.sq) * a
  log.det = n * log(param$sigma.sq) + K.logdet
  log.lik = drop(- .5 * log.det + log.exp)
  
  return(log.lik)
}

param.convert = function(param, data, convert.matrix = FALSE) {
  p = ncol(data$model.matrix)
  p2 = p+4
  
  fail = if(is.array(param)) dim(param)[3] != p2 else length(param) != p2
  if(fail) stop("Length of 'param' does not match with the dimensions given by 'model.matrix'.")
  
  list(beta = slice(1:p, param, convert.matrix),
       sigma.sq = slice(p+1, param, convert.matrix),
       X = list(rho = slice(p+2, param, convert.matrix)),
       t = list(c = slice(p+3, param, convert.matrix),
                rho = slice(p+4, param, convert.matrix)))
}


# Gibbs helper -----------------------------------------------------------------
MH.step = function(g, K, 
                   p.fn, param.old, data, prior, other.args = NULL,
                   proposal, LAP) {
  
  args = c(list(param = param.old, data = data, prior = prior), other.args)
  log.den = do.call(p.fn, args = args)
  
  sample = matrix(nrow = K, ncol = length(drop(param.old)))
  jump = rep(NA, K)
  lik = matrix(nrow = K, ncol = length(log.den$lik))
  
  for(t in 1:K) {
    # Sample candidate parameters
    param.cand = rmvnorm.chol(mu = param.old, Sigma.chol = sqrt(proposal$sigma.sq) * proposal$Sigma.chol)
    args$param = param.cand
    
    # Accept params?
    log.num = do.call(p.fn, args = args)
    
    #r = MH.ratio(unlist(log.num), unlist(log.den))
    #jump[t] = as.numeric(r > runif(1))
    r = unlist(log.num) - unlist(log.den) #; print(r)
    jump[t] = as.numeric(sum(r) > log(runif(1)))
    
    # for next iteration
    if(jump[t] == 1) {
      sample[t,] <- param.old <- param.cand
      log.den = log.num
      lik[t,] = log.num$lik
    } else {
      sample[t,] <- param.old
      lik[t,] = log.den$lik
    }
  }
  
  if(LAP) {
    proposal[1:2] = LAP.update(b = g, jumps = jump, sample = sample, h.sigma.sq.old = proposal$sigma.sq, h.Sigma.old = proposal$Sigma, c0 = ifelse(is.null(proposal$c0), 1, proposal$c0))
    proposal$Sigma.chol = chol(proposal$Sigma)
  }
  
  list(sample = sample, jump = jump, lik = lik, proposal = proposal)
}

MH.ratio = function(num, den, log = TRUE) {
  log.num = if(log) num else log(num)
  log.den = if(log) den else log(den)
  r = if(any(log.num == -Inf)) 0 else exp(sum(log.num) - sum(log.den))
  return(r)
}

rmvnorm.chol = 
function(
  n = 1, # number of random vectors desired
  mu, # mean vector (numeric vector)
  Sigma = NULL, # covariance matrix (matrix, length(mu) x length(mu))
  Sigma.chol = NULL, # chol(Sigma), an upper triangular matrix
  seed = NULL) # seed number
{
  # Sample from the multivariate normal distribution. (Code taken from STAT 260, HW #1)
  # Output: matrix of dimension: n x length(mu)
  
  if(is.null(Sigma) & is.null(Sigma.chol))
    stop("Either `Sigma' or `Sigma.chol' must be specified.")
  if(!is.null(Sigma) && length(mu) != nrow(Sigma)) 
    stop("Dimensions of `mu' and `Sigma' don't match.")
  if(!is.null(Sigma.chol) && length(mu) != nrow(Sigma.chol)) 
    stop("Dimensions of `mu' and `Sigma' don't match.")
  
  if(is.null(Sigma.chol)) {
    d = nrow(Sigma)
    Sigma.chol = chol(Sigma)
  } else {
    d = nrow(Sigma.chol)
  }
  
  if(!is.null(seed)) set.seed(seed)
  Z = matrix(rnorm(n*d), nrow = d)
  drop(t(mu + t(Sigma.chol) %*% Z))
}


LAP.update = 
function(
  b, # iteration number
  jumps, # numeric vector indicating jumps. 0 = no jump, 1 = jump
  sample, # MH samples done
  h.sigma.sq.old, # sigma_m(t-1)
  h.Sigma.old = NULL, # Sigma_m(t-1)
  c0 = 1, c1 = .8,
  r.opt = .234) {
  # Based off of Algorithm 2 of Ben Shaby's technical report, "Exploring an Adaptive Metropolis Algorithm".
  # According to Wayne, gamma1 < 1, so that won't get h.Sigma.new with rank < ncol(sample), hence added some nonnegative value of 1 to b
  
  LAP.k = nrow(sample)
  r.hat = sum(jumps)/LAP.k
  if(!is.null(h.Sigma.old)) h.Sigma.hat = cov(sample)
  gamma1 = 1/(b+1)^c1
  gamma2 = c0*gamma1
  
  h.sigma.sq.new = exp( log(h.sigma.sq.old) + gamma2 * (r.hat-r.opt) )
  h.Sigma.new = if(!is.null(h.Sigma.old)) h.Sigma.old + gamma1 * (h.Sigma.hat - h.Sigma.old) else NULL
  
  output = list(sigma.sq = h.sigma.sq.new, Sigma = h.Sigma.new)
  return(output)
}

rep.matrix = function(matrix, i = NULL, ...) {
  if(is.null(i)) {
    i = 1:nrow(matrix)
  } else {
    if(is.logical(i)) {
      if(length(i) != nrow(matrix)) stop("Length of 'i' does not equal number of rows in 'matrix'.")
      i = which(i)
    } else if(is.numeric(i)) {
      fail = any(i < 1) | any(i > nrow(matrix))
      if(fail) stop("'i' doesn't correspond to indices or number of rows in 'matrix'.")
    } else {
      stop("Don't recognize class of 'i'.")
    }
  }
  
  i.repeated = do.call("rep", list(x = i, ...))
  matrix[i.repeated,]
}

slice = function(indices, param, convert.matrix = FALSE) {
  if(is.array(param)) {
    output = param[,,indices,drop = FALSE]
    if(convert.matrix) {
      return(matrix(output, ncol = length(indices)))
    } else {
      return(output)
    }
  } else {
    return(param[indices])
  }
}

traceplot = function(g, K, gibbs.sample, data,
                     burn.in = 0,
                     n.pts = 3000, file) {
  
  trace = param.convert(param = gibbs.sample$sample, 
                        data = data,
                        convert.matrix = TRUE)
  
  x.idx = (burn.in*K+1):(g*K)
  if(length(x.idx) > n.pts) x.idx = sort(sample(x.idx, n.pts))
  x.lab = x.idx #+ burn.in*K
  
  plot.fn = function(y, name = "", line.proposal.changes = FALSE) {
    plot(x.lab, y, type = "n", xlab = "", ylab = name, main = name, las = 3)
    if(line.proposal.changes) abline(v = seq(0, g*K, K), col = "lightgray")
    points(x.lab, y, type = "l")
  }
  
  pdf(file, width = 11, height = 8.5)
  
  # beta
  if(ncol(trace$beta) > 1) {
    par(mfrow = c(2,4))
    sapply(1:ncol(trace$beta), function(col) plot.fn(trace$beta[x.idx,col], name = bquote(.(colnames(data$model.matrix)[col]))))
    par(mfrow = c(2,3))
  } else {
    par(mfrow = c(2,4))
    plot.fn(trace$beta[x.idx,], name = bquote(.(colnames(data$model.matrix)[1])))
  }
  
  # sigma.sq
  plot.fn(trace$sigma.sq[x.idx,], name = expression(sigma^2))
  
  # tau.sq
  plot.fn(trace$sigma.sq[x.idx,] * trace$t$c[x.idx,], name = expression(tau^2 == sigma^2 * c))
  plot.fn(trace$t$c[x.idx,], name = "c")
  
  # rho
  plot.fn(trace$X$rho[x.idx,], name = expression(rho[theta]))
  plot.fn(trace$t$rho[x.idx,], name = expression(rho[t]))
  
  # lik
  plot.fn(gibbs.sample$lik[x.idx], name = "lik")
  
  dev.off()
}

corr.mat = function(corr.type, X, delta, corr.args = NULL) {
  if(!corr.type %in% c("exp", "gauss", "matern", "wendland1d")) stop("I don't recognize that 'corr.type'.")
  fn = paste(corr.type, ".corr.mat", sep = "")
  args = c(list(x = X, corr.length = delta), corr.args)
  return(do.call(fn, args = args))
}

Matern.reparam = function(x, range, smoothness, reparam = FALSE) {
  # Code adapted from 'Matern' function from the fields package.  Made it amenable to matrices.
  d = dist(x) / range
  if(reparam) d = 2 * sqrt(smoothness) * d
  #d[d == 0] = 1e-10
  
  const = (2^(smoothness - 1)) * gamma(smoothness)
  const = 1/const
  
  const * (d^smoothness) * besselK(d, smoothness)
}

matern.corr.mat = function(x, corr.length, smoothness, nugget = 0, reparam = FALSE, product = TRUE, do.parallel = FALSE, verbose = FALSE)  {
  
  result = corr.error.check(x = x, corr.length = corr.length, smoothness = smoothness)
  if(product) {
    corr.length = result$corr.length
    smoothness = result$smoothness
  } else {
    if( length(corr.length) != 1 & length(smoothness) != 1 ) stop("Invalid 'corr.length' and/or 'smoothness': must have a length of 1 when product == FALSE.")
  }
  
  `%dopar_switch%` = if(product && do.parallel) `%dopar%` else `%do%`
  
  if( is.null(nugget) ) nugget = 0
  n = nrow(x)
  
  if( all(corr.length == Inf) ) { # If corr.length is all Inf, then the correlation matrix should be all 1s
    output = matrix(1, nrow = n, ncol = n)
    
    # if corr.length is not all Inf...
  } else {
    cols = if(product) which(corr.length != Inf) else 1
    
    output =
      foreach(col = cols, .combine = "*", .inorder = FALSE, .final = as.matrix) %dopar_switch% {
        if(verbose) cat(col, " "); flush.console()
        Matern.reparam(x = if(product) x[,col] else x, 
                       range = corr.length[col], 
                       smoothness = smoothness[col], 
                       reparam = reparam)
      }
    if(verbose) cat("\n"); flush.console()
  }
  
  diag(output) = 1 + nugget
  
  return(output)
}

backsolve.chol = function(X, Y) {
  if(!is.spam(X) & !is.spam(Y)) {
    return(backsolve(X, Y, transpose = TRUE))
  } else {
    return(forwardsolve(X, Y))
  }
}

corr.error.check = function(x, corr.length, smoothness = NULL, product = TRUE) {
  d = ncol(x)
  
  if( !is.matrix(x) ) stop("`x' must be a matrix.")
  
  if( length(corr.length) == 1 ) {
    warning(paste("'corr.length' is", corr.length, "in every dimension."))
    corr.length = rep(corr.length, d)
  }
  if( d != length(corr.length) ) stop("Number of parameters in 'corr.length' does not match up with the number of columns in 'x'.")
  if( any(corr.length < 0 | is.na(corr.length)) ) stop("'corr.length' must be a nonnegative real number.")
  
  if(!is.null(smoothness)) {
    if( length(smoothness) == 1 ) {
      warning(paste("'smoothness' is", smoothness, "in every dimension."))
      smoothness = rep(smoothness, d)
    }
    if( d != length(smoothness) ) stop("Number of parameters in 'smoothness' does not match up with 'x'.")
    if( any(smoothness < 0 | is.na(smoothness)) ) stop("'smoothness' must be a nonnegative real number.")
  }
  
  return(list(corr.length = corr.length, smoothness = smoothness))
}


