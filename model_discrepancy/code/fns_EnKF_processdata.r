#
# Functions to handle data processing of EnKF
#

source("model_discrepancy/code/fns_EnKF.r")

data.forecast = function(analysis.ensemble, adv.model, times, func, parms,
                         inflation.factor, N, M, ...) {
  # Initialization
  forecast = matrix(nrow = N, ncol = M+2)
  
  # Forecast: Propagate the ensemble members forward
  forecast[,1:M] = enkf.forecast(analysis.ensemble = analysis.ensemble, 
                                 adv.model = adv.model, times = times,
                                 func = func, parms = parms,
                                 inflation.factor = inflation.factor, ...)
  forecast[,M+1] = rowMeans(forecast[,1:M])
  forecast[,M+2] = apply(forecast[,1:M], 1, var)
  
  if(any(is.na(forecast[,M+1]))) stop("Something went wrong with 'enkf.forecast'. Obtained NA(s).")
  
  return(forecast)
}

data.update = function(forecast.ensemble, y, y.perturbed = NULL, dim, args, N, M) {
  # Initialization
  analysis = matrix(nrow = N, ncol = M+2)
  
  # Update: Obtain analysis distribution
  analysis[,1:M] = 
    do.call("enkf.update",
            c(list(forecast.ensemble = forecast.ensemble, 
                   y = y, y.perturbed = y.perturbed), 
              args))
  analysis[,M+1] = rowMeans(analysis[,1:M])
  analysis[,M+2] = apply(analysis[,1:M], 1, var)
  
  if(any(is.na(analysis[,M+1]))) stop("Something went wrong with 'enkf.update'. Obtained NA(s).")
  
  return(analysis)
}

data.smooth = function(analysis.ensemble, smoothed.ensemble, forecast.ensemble, args, N, M, range.tol = 100000) {
  # Initialization
  smoothed = matrix(nrow = N, ncol = M+2)
  
  # Forecast: Propagate the ensemble members forward
  smoothed[,1:M] = 
    do.call("enks",
            c(list(analysis.ensemble = analysis.ensemble, 
                   smoothed.ensemble = smoothed.ensemble,
                   forecast.ensemble = forecast.ensemble), 
              args))
  smoothed[,M+1] = rowMeans(smoothed[,1:M])
  smoothed[,M+2] = apply(smoothed[,1:M], 1, var)
  
  if(any(is.na(smoothed[,M+1]))) stop("Something went wrong with 'enks'. Obtained NA(s).")
  if(any(abs(smoothed[,M+1]) > range.tol)) stop("Something went wrong with 'enks'. Some of the mean values are greater than 'range.tol' (in absolute value).")
  
  return(smoothed)
}

plot.forecast.ensemble = function(n, M, T, forecast, x, y, 
                                  t.end = NULL, t.window.length = NULL,
                                  date.fn = function(x) x,
                                  mar = c(2, 2, 0, 0), 
                                  xlab = "", ylab = "", 
                                  ylim = NULL,
                                  cex = 0.5, lwd.multiplier = 1,
                                  plot = TRUE,
                                  ...) {
  # Fill in NULLs
  if(is.null(t.end)) t.end = T
  if(is.null(t.window.length)) t.window.length = T
  
  # Get window
  t.right = floor(t.window.length/3) # do not plot forecasts for the last third of the window
  t.window.start = min(max(1, t.end - (t.window.length-t.right)), T-t.window.length+1)
  t.window.end = min(max(t.window.length+1, t.end+t.right), T)
  
  t.window = t.window.start:t.window.end
  t.window.short = t.window.start:t.end
  
  date.window = date.fn(t.window)
  date.window.short = date.fn(t.window.short)
  
  if(plot) {
    ylim.range = if(is.null(ylim)) range(forecast[n,1:M,-(1:5)], x[,n], y[,n], na.rm = TRUE) else ylim
    
    # Plots the forecast ensemble with the truth and observations.
    par(mar = mar)
    
    plot(date.window, x[t.window,n],
         col = "green4", type = "l",
         ylim = ylim.range, 
         xlab = xlab, ylab = ylab, ...)
    for(m in 1:M) lines(date.window.short, forecast[n,m,t.window.short], col = "lightgray") # forecast ensemble
    lines(date.window, x[t.window,n], lwd = 1.2*lwd.multiplier, col = "green4") # the true solution
    points(date.window, y[t.window,n], cex = cex, pch = "O") # observations
  }
  
  invisible(list(t.window = t.window, t.window.short = t.window.short))
}

# Plot densities
plot.den = function(den, point = NULL, rug = NULL, legend = NULL) {
  par(mar = c(2, 2, 0, 0))
  
  plot(den[[1]], xlim = den$xlim, ylim = den$ylim, sub = "", main = "", 
       col = den$col[1])
  for(i in 2:(length(den)-3)) lines(den[[i]], col = den$col[i])
  
  if(!is.null(point)) {
    points(point$x, point$y, col = point$col, type = "h", lwd = 2)
  }
  
  if(!is.null(rug)) {
    for(i in 1:length(rug)) rug(rug[[i]], col = rug$col[i])
  }
  
  if(!is.null(legend)) {
    do.call("legend", legend)
  }
}

# Plot forecast, analysis, observation densities
plot.den.assim = function(n, M, t, sd, analysis, forecast, x, y) {
  den = list(forecast = density(forecast[n,1:M,t]),
             obs = NULL,
             analysis = density(analysis[n,1:M,t]))
  xlim.input = range(forecast[n,1:M,t], analysis[n,1:M,t], x[t,n] + c(-4,4)*sd)
  den$obs = list(x = seq(xlim.input[1], xlim.input[2], length.out = 100),
                 y = NULL)
  den$obs$y = dnorm(den$obs$x, mean = x[t,n], sd = sd)
  
  ylim.input = range(sapply(den, function(x) range(x$y)))
  
  plot.den(
    den = c(den,
            list(xlim = xlim.input, ylim = ylim.input, 
                 col = c("darkgray", "green4", "blue"))),
    point = list(x = y[t,n], 
                 y = dnorm(y[t,n], mean = x[t,n], sd = sd), 
                 col = "green4"),
    rug = list(forecast[n,1:M,t], y[t,n], analysis[n,1:M,t], 
               col = c("darkgray", "green4", "blue")),
    legend = list(x = "topright", 
                  legend = c("truth", "forecast", "analysis"),
                  col = c("green4", "gray", "blue"),
                  lwd = 2, bg = "white", cex = .8))
}