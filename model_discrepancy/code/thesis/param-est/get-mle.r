
get.mle.batch = function(run.folder, param.names, folder.form, names.form, subset = NULL, num.form = NULL) {
  if(is.null(num.form)) num.form = rep(FALSE, length(names.form))
  
  # Set folder and get info 
  runs = dir(run.folder)
  runs = runs[grep("\\.mat", runs)]
  run.info = sapply(1:length(names.form), function(i) gsub(folder.form, paste0("\\", i), runs))
  run.info = data.frame(run.info)
  names(run.info) = names.form
  for(i in 1:length(num.form)) {
    if(num.form[i]) run.info[,i] = as.numeric(levels(run.info[,i])[unclass(run.info[,i])])
  }
  
  # Subset
  idx.keep = rep(TRUE, length(runs))
  if(!is.null(subset)) for(type in names(subset)) idx.keep = idx.keep & run.info[,type] %in% subset[[type]]
  runs = runs[idx.keep]
  run.info = run.info[idx.keep,]
  
  # Get data from files and put into form for plotting
  files = paste0(run.folder, "/", runs)
  if(length(files) > 0) {
    out = lapply(1:length(files), function(i) get.mle(files[i], param.names, run.info[i,]))
    out = do.call(rbind, out)
    return(out)
  } else {
    return(NULL)
  }
}

get.mle = function(file, param.names, run.info) {
  # TODO: can make this faster by melting in batch mode
  require(reshape2)
  require(R.matlab)
  if2 = try(readMat(file), silent = FALSE)
  
  if(class(if2) == "try-error") {
    return(NULL)
  } else {
    mle = apply(if2$records[[2]], c(1,2), median)
    colnames(mle) = param.names[1:ncol(mle)]
    out = melt(mle, value.name = "mle")
    colnames(out)[1:2] = c("b", "param")
    out[,names(run.info)] = run.info
    return(out)
  }
}


