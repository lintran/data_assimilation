#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("B", type = "integer")
parser$add_argument("-apfinf", action="store_true", default = FALSE)
parser$add_argument("-etkfinf", action="store_true", default = FALSE)
parser$add_argument("-Nruns", type = "integer", default = 20)
parser$add_argument("-M", type = "integer", nargs = "+", default = c(4,5,6,12,24)) #, default = 0)
parser$add_argument("-folder", type = "character", default = "param-est-review")
args = parser$parse_args()
# args = parser$parse_args(c("50"))

if(args$M == 0) args$M = NA

library(R.matlab)
library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/param-est/get-rmse.r")

# Get RMSEs -----------------------------------------------------------------------------------
truth.folder = "~/data_assimilation/DART/Kodiak/models/lorenz_63/perfect/"
t = list(1:300, 301:600)

# ...from plugging in MLEs
update.types = c("apf", "etkf")
inf.types = ifelse(c(args$apfinf, args$etkfinf), "inf", "noinf")
run.folders = sprintf("~/data_assimilation/DART/Kodiak/models/lorenz_63/%s/%s", 
                      update.types, inf.types)
folder.form = "obs([[:digit:]]+)\\-B([[:digit:]]+)\\-M([[:digit:]]+)\\-([[:digit:]]+).*"
names = c("obs", "B", "M", "run")
subset = NULL
if(!is.na(args$M)) subset = c(subset, list(M = args$M))
if(!is.na(args$B)) subset = c(subset, list(M = args$B))

mle = lapply(run.folders, get.rmse.batch, truth.folder, folder.form, names, subset, t, na.sd0 = TRUE)
for(i in 1:length(update.types)) mle[[i]]$update.type = ifelse(i == 1, "APF", "EnKF")
mle = do.call(rbind, mle)
mle = subset(mle, direction == "all")
mle$obs = factor(mle$obs, levels = c(25, 0, 15, 10, 5), labels = c("xz", "x", "yz", "y", "xy"))
mle$rmse[mle$rmse > 15] = NA
summary(mle)

# ....from plugging in truth
run.folder = "~/data_assimilation/DART/Kodiak/models/lorenz_63/truth/"
folder.form = "obs([[:digit:]]+)\\-M([[:digit:]]+)"
names = c("obs", "M")

truth = get.rmse.batch(run.folder, truth.folder, folder.form, names, t = t, na.sd0 = TRUE)
truth = subset(truth, direction == "all")
truth$obs = factor(truth$obs, levels = c(25, 0, 15, 10, 5), labels = c("xz", "x", "yz", "y", "xy"))
summary(truth)

# get number of successful runs
win = subset(mle, !is.na(rmse))[,!colnames(mle) %in% c("rmse")]
win = unique(win)
win$run = 1
colnames(win)[colnames(win) == "run"] = "win"
win = aggregate(win ~ ., data = win, sum)
win$win = win$win/args$Nruns
win$fail = 1 - win$win

temp1 = aggregate(rmse ~ ., mle[,c("obs", "inflation", "rmse")], min, na.rm = TRUE)
colnames(temp1)[colnames(temp1) == "rmse"] = "min"
temp2 = aggregate(rmse ~ ., mle[,c("obs", "inflation", "rmse")], max, na.rm = TRUE)
colnames(temp2)[colnames(temp2) == "rmse"] = "max"
temp = merge(temp1, temp2)
temp$pos = temp$min - .1*(temp$max - temp$min)
win = merge(win, temp)

# Plot distribution of RMSEs ---------------------------------------------------
dir = sprintf("~/data_assimilation/model_discrepancy/text/phd-thesis/%s/illustrations/lorenz63", args$folder)
filename = sprintf("%s/rmse-compare-%s-B%03i.pdf", dir, paste(ifelse(inf.types == "inf", "i", "n"), collapse = ""), args$B)
width = 11-3
height = 8.5-3
#if(!is.na(args$B)) height = height/2
s = 600

pdf(filename, width = width, height = height)
for(inf in unique(mle$inflation)) {
  p = ggplot(subset(mle, inflation == inf & t == s), aes(update.type, rmse))
  p = p + geom_hline(data = subset(truth, inflation == inf & t == s), aes(yintercept = rmse), color = "darkgray")
  p = p + geom_boxplot(outlier.size = 1, outlier.shape = 1)
  p = p + geom_text(data = subset(win, inflation == inf & t == s), aes(update.type, pos, label = fail*100), 
                    angle = 90, size = 2, hjust = 0, fontface = "bold")
  p = p + facet_grid(obs ~ M, scales = "free_y")
  p = p + ylab("RMSE") + xlab("")
  p = p + ggtitle(sprintf("%s", inf))
  
  p2 = p + theme_bw() +
    theme(text = element_text(size = 10),
          #axis.text.x = element_text(angle = 90, vjust = .5),
          strip.text.y = element_text(angle = 0),
          plot.margin=unit(c(0,0,0,0),"mm"))
  p2
  print(p2)
}
dev.off()

