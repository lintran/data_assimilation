#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("update.type", action="store", type = "character")
parser$add_argument("B", type = "integer")
parser$add_argument("-inflation", action="store_true")
parser$add_argument("-Nruns", type = "integer", default = 20)
parser$add_argument("-M", type = "integer", nargs = "+", default = c(4,5,6,12,24)) #, default = 0)
parser$add_argument("-folder", type = "character", default = "param-est-review")
args = parser$parse_args()
# args = parser$parse_args(c("apf", "50"))
# args = parser$parse_args(c("etkf", "50"))

if(args$M == 0) args$M = NA

library(R.matlab)
library(grid)
library(ggplot2)
source("~/data_assimilation/model_discrepancy/code/thesis/param-est/get-rmse.r")

# Get RMSEs --------------------------------------------------------------------
truth.folder = "~/data_assimilation/DART/Kodiak/models/lorenz_63/perfect/"
t = list(1:300, 301:600)

# ...from plugging in MLEs
run.folder = sprintf("~/data_assimilation/DART/Kodiak/models/lorenz_63/%s/%s", 
                     args$update.type, ifelse(args$inflation, "inf", "noinf"))
folder.form = "obs([[:digit:]]+)\\-B([[:digit:]]+)\\-M([[:digit:]]+)\\-([[:digit:]]+).*"
names = c("obs", "B", "M", "run")
subset = NULL
if(!is.na(args$M)) subset = c(subset, list(M = args$M))
if(!is.na(args$B)) subset = c(subset, list(M = args$B))

mle = get.rmse.batch(run.folder, truth.folder, folder.form, names, subset, t, na.sd0 = TRUE)
mle$obs = factor(mle$obs, levels = c(25, 0, 15, 10, 5), labels = c("xz", "x", "yz", "y", "xy"))
mle$direction = factor(mle$direction)
mle$rmse[mle$rmse > 100] = NA
summary(mle)

# ....from plugging in truth
run.folder = "~/data_assimilation/DART/Kodiak/models/lorenz_63/truth/"
folder.form = "obs([[:digit:]]+)\\-M([[:digit:]]+)"
names = c("obs", "M")

truth = get.rmse.batch(run.folder, truth.folder, folder.form, names, t = t, na.sd0 = TRUE)
truth$obs = factor(truth$obs, levels = c(25, 0, 15, 10, 5), labels = c("xz", "x", "yz", "y", "xy"))
truth$direction = factor(truth$direction)
summary(truth)

# get number of successful runs
win = subset(mle, !is.na(rmse))[,!colnames(mle) %in% c("rmse")]
win = unique(win)
win$run = 1
colnames(win)[colnames(win) == "run"] = "win"
win = aggregate(win ~ ., data = win, sum)
win$win = win$win/args$Nruns
win$fail = 1 - win$win

temp1 = aggregate(rmse ~ ., mle[,c("direction", "inflation", "rmse")], min, na.rm = TRUE)
colnames(temp1)[colnames(temp1) == "rmse"] = "min"
temp2 = aggregate(rmse ~ ., mle[,c("direction", "inflation", "rmse")], max, na.rm = TRUE)
colnames(temp2)[colnames(temp2) == "rmse"] = "max"
temp = merge(temp1, temp2)
temp$pos = temp$min - .1*(temp$max - temp$min)
win = merge(win, temp)

# Plot distribution of RMSEs ---------------------------------------------------
dir = sprintf("~/data_assimilation/model_discrepancy/text/phd-thesis/%s/illustrations/lorenz63", args$folder)
filename = sprintf("%s/rmse-%s-%s%s.pdf", 
                   dir, args$update.type, 
                   ifelse(args$inflation, "inf", "noinf"), 
                   ifelse(!is.na(args$B), sprintf("-B%03i", args$B), ""))
width = 11-3
height = 8.5-3
#if(!is.na(args$B)) height = height/2
pdf(filename, width = width, height = height)

for(inf in unique(mle$inflation)) {
  for(s in unique(mle$t)) {
    p = ggplot(subset(mle, inflation == inf & t == s), aes(obs, rmse)) +
      geom_boxplot(outlier.size = 1, outlier.shape = 1)
    p = p + geom_point(data = subset(truth, inflation == inf & t == s), shape = 23, fill = "darkgreen")
    p = p + geom_text(data = subset(win, inflation == inf & t == s), aes(obs, pos, label = fail*100), 
                      angle = 90, size = 2, hjust = 0, fontface = "bold")
    p = p + facet_grid(direction ~ M, scales = "free_y", space = "free_y")
    p = p + ylab("RMSE") + xlab("")
    p = p + ggtitle(sprintf("%s, t%s", inf, s))
    
    p2 = p + theme_bw() +
      theme(text = element_text(size = 10),
            #axis.text.x = element_text(angle = 90, vjust = .5),
            strip.text.y = element_text(angle = 0),
            plot.margin=unit(c(0,0,0,0),"mm"))
    p2
    print(p2)
  }
}

dev.off()

# filename = sprintf("%s/blah.pdf", dir)
# pdf(filename, width = 8.5, height = 11)
# truth = get.ncdata(paste0(truth.folder, "l000/True_State.nc"))
# forecast = get.ncdata(paste0(run.folder, "l000-M012/noinf/Prior_Diag.nc"))
# M = 12
# par(mfrow = c(3,1))
# for(n in 1:3) {
#   plot(forecast[n,3,], type = "l", col = "gray", ylim = range(forecast[n,3:(M+2),], na.rm = TRUE))
#   for(m in 2:M) points(forecast[n,m+2,], type = "l", col = "gray")
#   points(truth[n,], type = "l", col = "darkgreen")
# }
# dev.off()
# 
