#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("savename", type = "character")
parser$add_argument("-x.facet", type = "character", default = "obs")
parser$add_argument("-y.facet", type = "character", default = "inf")
parser$add_argument("-filter", type = "character", default = "")
parser$add_argument("-inf", type = "character", nargs = "+", default = 100)
parser$add_argument("-M", type = "integer", nargs = "+", default = 0)
parser$add_argument("-B", type = "integer", default = 0)
parser$add_argument("-Nruns", type = "integer", default = 30)
parser$add_argument("-folder", type = "character", default = "")
parser$add_argument("-param.min", type = "double", default = 0)
parser$add_argument("-param.max", type = "double", default = 50)
parser$add_argument("-long.width", action = "store_true")
parser$add_argument("-scale.height", type = "double", default = 1)
parser$add_argument("-scale.width", type = "double", default = 1)
args = parser$parse_args()
# args = parser$parse_args(c("test", "-filter", "etkf", "-inf", "002060", "100000", "150000"))

if(args$filter == "") args$filter = NA
if(args$inf == 100) args$inf = NA
if(args$M == 0) args$M = NA
if(args$B == 0) args$B = NA

library(R.matlab)
library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/param-est/get-mle.r")

# Get MLEs -----------------------------------------------------------------------------------
run.folder = "~/data_assimilation/model_discrepancy/data/thesis/lorenz63/param-est/avg-rk"
folder.form = "obs([[:digit:]]+)\\-([[:alpha:]]+)\\-inf([[:digit:]]+)\\-M([[:digit:]]+)\\-B([[:digit:]]+)\\-([[:digit:]]+).*"
names.form = c("obs", "filter", "infcode", "M", "B", "run")
num.form = c(TRUE, FALSE, FALSE, TRUE, TRUE, TRUE)

param.names = c("sigma", "inflation")
subset = NULL
if(!is.na(args$filter)) subset = c(subset, list(filter = args$filter))
if(!is.na(args$inf)) subset = c(subset, list(infcode = args$inf))
if(!is.na(args$B)) subset = c(subset, list(B = args$B))
if(!is.na(args$M)) subset = c(subset, list(M = args$M))

# get MLEs
mle = get.mle.batch(run.folder, param.names, folder.form, names.form, subset = subset, num.form = num.form)
summary(mle)
mle = subset(mle, b == B)
mle$filter = factor(as.character(mle$filter), levels = c("apf", "etkf"), labels = c("EnKF-APF", "EnKF"))
mle$obs = factor(mle$obs, levels = c(25, 0, 15, 10, 5), 
                 labels = sprintf("atop('%s', i == %i)", c("xz", "x", "yz", "y", "xy"), 1:5))
summary(mle)

# get unique ids
uni = lapply(mle[,!colnames(mle) %in% c("mle", "run")], unique)
# uni$run = 1:args$Nruns
mle.all = expand.grid(uni)
mle.all$inf.value = as.numeric(substring(mle.all$infcode, 1, 2))
mle.all$inf.max = as.numeric(substring(mle.all$infcode, 3, 4))
mle.all$inf.sd = as.numeric(substring(mle.all$infcode, 5, 6))
mle.all$inf = NA
idx = mle.all$inf.value > 0
if(sum(idx) > 0) mle.all$inf[idx] = sprintf("paste(lambda == %.1f)", mle.all$inf.value[idx]/10)
if(sum(!idx) > 0) mle.all$inf[!idx] = sprintf("paste('max = %2i, sd = %.1f')", mle.all$inf.max[!idx], mle.all$inf.sd[!idx]/100)
mle.all$inf = factor(mle.all$inf)

# calculate percentage of failures
fails = aggregate(run ~ ., unique(mle[,!colnames(mle) %in% c("mle", "param")]), length)
colnames(fails)[colnames(fails) == "run"] = "success"
fails = merge(fails, mle.all, all.y = TRUE)
fails$success[is.na(fails$success)] = 0
fails$perc = round((1 - fails$success/max(mle$run, na.rm = TRUE))*100)
fails$mle = args$param.min-5
summary(fails)

mle = merge(mle, mle.all, all.y = TRUE)

# get mles that are outside min/max
outliers = subset(mle, mle > args$param.max | mle < args$param.min)

# aggregate(mle ~ ., subset(mle, obs %in% c("x", "xz") & M == 24 & param == "sigma")[,colnames(mle) != "run"], quantile, c(.05, .5, .95))

# Plot distribution of MLEs --------------------------------------------------------------------
p = ggplot(subset(mle, param == "sigma"), aes(factor(M), mle, group = factor(M)))
p = p + geom_violin(scale = "width")
  # geom_boxplot(outlier.size = 1.5, outlier.colour = alpha("black", .5)) +
p = p + geom_hline(yintercept = c(args$param.min, 10, args$param.max), color = c("red3", "green3", "red3"))
p = p + geom_point(data = subset(outliers, param == "sigma"), color = "red", alpha = .5, size = 2, position = position_jitter(w = .25, h = .1))
p = p + facet_grid(paste(args$y.facet, "~", args$x.facet), labeller = label_parsed)
p = p + geom_text(data = subset(fails, param == "sigma"), aes(label = perc), angle = 90, size = 2)
p = p + ylab(expression(hat(sigma))) + xlab("M")
p = p + coord_cartesian(ylim=c(-10, 55))
# p = p + ggtitle(sprintf("%s, %s", args$update.type, ifelse(args$inflation, "inf", "noinf")))

p2 = p + theme_bw() +
  theme(text = element_text(size = 10),
        axis.title.y = element_text(angle = 0),
        #axis.text.x = element_text(angle = 90, vjust = .5),
        plot.margin=unit(c(0,0,0,0),"mm"))
print(p2)
dev.off()

pdf.dims = c((8.5-3), (11-3)) # width x height
if(args$long.width) pdf.dims = rev(pdf.dims)
pdf.dims[1] = args$scale.width*pdf.dims[1]
pdf.dims[2] = args$scale.height*pdf.dims[2]

dir = sprintf("~/data_assimilation/model_discrepancy/text/phd-thesis/param-est-review/illustrations/lorenz63/%s", args$folder)
dir.create(dir, showWarnings = FALSE, recursive = TRUE)
filename = sprintf("%s/mle-%s.pdf", dir, args$savename)
pdf(filename, width = pdf.dims[1], height = pdf.dims[2])
print(p2)
dev.off()

