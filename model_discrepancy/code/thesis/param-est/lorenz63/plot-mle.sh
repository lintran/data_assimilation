#!/bin/bash

add_cmd=""
nohup ./plot-mle.r apf-fix-big -filter apf -inf 100000 150000 200000 250000 300000 ${add_cmd} &> apf-fix-big.out &
nohup ./plot-mle.r apf-est-big -filter apf -inf 000510 000520 000560 002020 002060 ${add_cmd} &> apf-est-big.out &
nohup ./plot-mle.r etkf-fix-big -filter etkf -inf 100000 150000 200000 250000 300000 ${add_cmd} &> etkf-fix-big.out &
nohup ./plot-mle.r etkf-est-big -filter etkf -inf 000510 000520 000560 002020 002060 ${add_cmd} &> etkf-est-big.out &

nohup ./plot-mle.r apf-fix -filter apf -inf 100000 150000 ${add_cmd} -scale.height .35 &> apf-fix.out &
nohup ./plot-mle.r apf-est -filter apf -inf 000520 002060 ${add_cmd} -scale.height .35 &> apf-est.out &
nohup ./plot-mle.r etkf-fix -filter etkf -inf 100000 150000 ${add_cmd} -scale.height .35 &> etkf-fix.out &
nohup ./plot-mle.r etkf-est -filter etkf -inf 000520 002060 ${add_cmd} -scale.height .35 &> etkf-est.out &

nohup ./plot-mle.r apf-etkf-fix -y.facet filter -inf 150000 ${add_cmd} -scale.height .35 &> apf-etkf-fix.out &
nohup ./plot-mle.r apf-etkf-est -y.facet filter -inf 000520 ${add_cmd} -scale.height .35 &> apf-etkf-est.out &
