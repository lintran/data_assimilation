

get.rmse.batch = function(run.folder, truth.folder, folder.form, names, subset = NULL, t = NULL, na.sd0 = TRUE) {
  # Set folder and get info 
  runs = dir(run.folder)
  run.info = sapply(1:length(names), function(i) as.numeric(gsub(folder.form, paste0("\\", i), runs)))
  run.info = data.frame(run.info)
  names(run.info) = names
  
  # Subset
  idx.keep = rep(TRUE, length(runs))
  if(!is.null(subset)) for(type in names(subset)) idx.keep = idx.keep & run.info[[type]] %in% subset[[type]]
  run = runs[idx.keep]
  run.info = lapply(run.info, function(l) l[idx.keep])
  run.info = data.frame(run.info)
  
  # get truth 
  obs = unique(run.info$obs)
  truth = lapply(sprintf("%s/obs%02i/True_State.nc", truth.folder, as.integer(obs)), get.ncdata)
  
  # Get data from files and put into form for plotting
  inflation.types = dir(paste0(run.folder, "/", runs[1]))
  run.info = do.call(rbind, lapply(inflation.types, function(inf) data.frame(run.info, inflation = inf)))
  forecast.files = lapply(inflation.types, function(inf) sprintf("%s/%s/%s/Prior_Diag.nc", run.folder, runs, inf))
  forecast.files = unlist(forecast.files)
  out = lapply(1:length(forecast.files), function(i) {
    idx = which(run.info$obs[i] == obs)
    get.rmse(forecast.files[i], run.info[i,], truth[[idx]], t = t, na.sd0 = na.sd0)
  })
  out = do.call(rbind, out)
  out
}

get.rmse = function(file, run.info, truth, t = NULL, na.sd0 = TRUE) {
  data = data.frame(matrix(ncol = length(run.info)+3, nrow = 4))
  colnames(data) = c(names(run.info), "t", "direction", "rmse")
  data[,names(run.info)] = run.info
  data$direction = c("x", "y", "z", "all")
  
  forecast = get.ncdata(file)
  
  t.list = list(1:dim(forecast)[3])
  if(!is.null(t)) t.list = c(t.list, t)
  t.name = sapply(t.list, function(x) x[length(x)])
  t.name[1] = -1
  
  out = lapply(1:length(t.list), function(i) {
    forecast.mean = forecast[,1,t.list[[i]]]
    forecast.sd = forecast[,2,t.list[[i]]]
    if(na.sd0) forecast.mean[forecast.sd == 0] = NA
    data$rmse = c(calc.rmse(forecast.mean, truth[,t.list[[i]]], margin = 1),
                  calc.rmse(forecast.mean, truth[,t.list[[i]]], margin = NA))
    data$t = t.name[i]
    data
  })
  
  do.call(rbind, out)
}

calc.rmse = function(pred, truth, margin = 1) {
  if(!is.na(margin)) {
    out = sqrt(apply((pred - truth)^2, margin, mean))
  } else {
    out = sqrt(mean((pred - truth)^2, na.rm = TRUE))
  }
  out
}

get.ncdata = function(file) {
  require(ncdf4)
  nc = nc_open(file)
  data = ncvar_get(nc)
  nc_close(nc)
  data
}


