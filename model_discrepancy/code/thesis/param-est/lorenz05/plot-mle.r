#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("savename", type = "character")
parser$add_argument("-est", type = "character", default = "KF")
parser$add_argument("-x.facet", type = "character", default = "obs2")
parser$add_argument("-y.facet", type = "character", default = "param2")
parser$add_argument("-filter", type = "character", default = "")
parser$add_argument("-inf", type = "integer", nargs = "+", default = 100)
parser$add_argument("-M", type = "integer", nargs = "+", default = 0) #, default = 0)
parser$add_argument("-B", type = "integer", default = 0)
parser$add_argument("-param", type = "character", nargs = "+", default = "")
parser$add_argument("-Nruns", type = "integer", default = 20)
parser$add_argument("-folder", type = "character", default = "param-est-review")
parser$add_argument("-long.width", action = "store_true")
parser$add_argument("-scale.height", type = "double", default = 1)
parser$add_argument("-scale.width", type = "double", default = 1)
parser$add_argument("-plotmaxmin", action = "store_true")
args = parser$parse_args()
# args = parser$parse_args(c("apf-inf15-zoomed", "-filter", "apf", "-inf", 15, "-scale.height", .375))
# args = parser$parse_args(c("apf-inf00-zoomed", "-filter", "apf", "-inf", 0, "-scale.height", .375))
# args = parser$parse_args(c("K-inf00-zoomed", "-inf", 0, "-param", "K", "-y.facet", "filter", "-scale.height", .375, "-scale.width", .66))
# args = parser$parse_args(c("F-inf00-zoomed", "-inf", 0, "-param", "F", "-y.facet", "filter", "-scale.height", .375, "-scale.width", .66))
# args = parser$parse_args(c("K-inf15-zoomed", "-inf", 15, "-param", "K", "-y.facet", "filter", "-scale.height", .375, "-scale.width", .66))
# args = parser$parse_args(c("F-inf15-zoomed", "-inf", 15, "-param", "F", "-y.facet", "filter", "-scale.height", .375, "-scale.width", .66))

if(args$filter == "") args$filter = NA
if(args$inf == 100) args$inf = NA
if(args$M == 0) args$M = NA
if(args$B == 0) args$B = NA

library(R.matlab)
library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/param-est/get-mle.r")

# Get MLEs -----------------------------------------------------------------------------------
obs = c("identity-all-960", "identity-other-480", "integral-random-480")
run.folders = sprintf("~/data_assimilation/model_discrepancy/data/thesis/lorenz05/param-est/default-%s/dt24/perfect_wrong/rk/", obs)
folder.form = "([[:alpha:]]+)\\-([[:alpha:]]+)\\-inf([[:digit:]]+)\\-M([[:digit:]]+)\\-B([[:digit:]]+)\\-([[:digit:]]+).*"
names.form = c("est", "filter", "inf.value", "M", "B", "run")
num.form = c(FALSE, FALSE, TRUE, TRUE, TRUE, TRUE)

param.names = c("K", "F", "inflation", "halfwidth")
subset = NULL
if(!is.na(args$filter)) subset = c(subset, list(filter = args$filter))
if(!is.na(args$inf)) subset = c(subset, list(inf.value = args$inf))
if(!is.na(args$B)) subset = c(subset, list(B = args$B))
if(!is.na(args$M)) subset = c(subset, list(M = args$M))

# get MLEs
mle = lapply(1:length(obs), function(i) get.mle.batch(run.folders[i], param.names, folder.form, names.form, subset = subset, num.form = num.form))
for(i in 1:length(obs)) if(!is.null(mle[[i]])) mle[[i]]$obs = obs[i]
lapply(mle, summary)
mle = do.call(rbind, mle)
mle = subset(mle, b == B)
mle$filter = factor(as.character(mle$filter), levels = c("apf", "etkf"), labels = c("EnKF-APF", "EnKF"))
mle$obs = factor(mle$obs, levels = obs, labels = c("full", "other", "integral"))
mle$param = as.character(mle$param)
mle$param[mle$obs == "integral" & mle$inf != 0 & mle$param == "inflation"] = "halfwidth"
mle$param = factor(mle$param, levels = param.names)
summary(mle)

if(args$param != "") mle = subset(mle, param %in% args$param & obs != "integral")

# get unique ids
uni = lapply(mle[,!colnames(mle) %in% c("mle", "run")], unique)
uni$obs = levels(mle$obs)
# uni$run = 1:args$Nruns
mle.all = expand.grid(uni)
mle.all$inf = paste("paste('fixed inflation, ', lambda == ", mle.all$inf.value/10, ")")
mle.all$inf[mle.all$inf.value == 0] = "paste('estimated inflation')"
mle.all$inf[mle.all$inf.value == 10] = "paste('no inflation')"
mle.all$inf = factor(mle.all$inf)
param2.names = sprintf("'%s'", param.names)
param2.names[param2.names == "'halfwidth'"] = "gamma[3]"
mle.all$param2 = factor(param2.names[unclass(mle.all$param)], levels = param2.names)
mle.all$obs2 = factor(mle.all$obs, levels = levels(mle.all$obs),
                      labels = sprintf("atop('%s', i == %i)", c("full", "other", "integral"), 1:3))

if(args$param != "") {
  mle.all = subset(mle.all, obs != "integral")
  mle.all$obs = mle.all$obs[, drop = TRUE]
  mle.all$obs2 = mle.all$obs2[, drop = TRUE]
}

# calculate percentage of failures
fails = aggregate(run ~ ., unique(mle[,!colnames(mle) %in% c("mle", "param")]), length)
colnames(fails)[colnames(fails) == "run"] = "success"
fails = merge(fails, mle.all, all.y = TRUE)
fails$success[is.na(fails$success)] = 0
fails$perc = round((1 - fails$success/max(mle$run, na.rm = TRUE))*100)
fails$mle = 0
summary(fails)

# create data on parameter min, max, and truth
file = grep("inf00.*\\.mat", dir(run.folders[3], full.names = TRUE), value = TRUE)[1]
temp = readMat(file)
# gsub(" ", "", as.character(c(temp$param.name[1,,]$state, temp$param.name[2,,]$obs)))
maxmin =
  data.frame(param = rep(param.names, 3), #  c("K", "F", "inflation", "halfwidth")
             yintercept = c(temp$param.min, temp$param.max, 32, 15, 1, 5/960),
             type = rep(c("min", "max", "truth"), each = length(param.names)))
maxmin = merge(maxmin, aggregate(mle ~ param, mle, min, na.rm = TRUE))
colnames(maxmin)[colnames(maxmin) == "mle"] = "min.mle"
maxmin = merge(maxmin, aggregate(mle ~ param, mle, max, na.rm = TRUE))
colnames(maxmin)[colnames(maxmin) == "mle"] = "max.mle"
maxmin = subset(maxmin, param %in% unique(mle$param))
maxmin = merge(maxmin, unique(mle.all[,c("param", "param2")]), all.x = TRUE)

# merge min/max info into mle data
temp = reshape(maxmin[,c("param", "type", "yintercept")], direction = "wide", idvar = "param", timevar = "type")
colnames(temp) = gsub("yintercept\\.", "", colnames(temp))
mle.all = merge(mle.all, temp, all.x = TRUE)
mle = merge(mle, mle.all, all.y = TRUE)

# other
if(!args$plotmaxmin) maxmin = subset(maxmin, yintercept >= min.mle & yintercept <= max.mle)

# get mles that are outside min/max
outliers = subset(mle, mle > max | mle < min)

# Plot distribution of MLEs --------------------------------------------------------------------
p = ggplot(subset(mle, param != "inflation"), aes(factor(M), mle))
p = p + geom_violin(scale = "width")
p = p + geom_hline(data = subset(maxmin, param != "inflation"), aes(yintercept = yintercept, color = type))
p = p + geom_point(data = subset(outliers, param != "inflation"), color = "red", alpha = .5, size = 2, position = position_jitter(w = .25, h = .1))
p = p + facet_grid(paste(args$y.facet, "~", args$x.facet), scales = ifelse(args$param == "", "free", "fixed"), drop = TRUE, labeller = label_parsed)
p = p + scale_colour_manual(values = c("min" = "red3", "max" = "red3", "truth" = "green3"))
p = p + ylab("") + xlab("")

if(args$param == "K") p = p + coord_cartesian(ylim=c(30, 34))
if(args$param == "F") p = p + coord_cartesian(ylim=c(10, 20))

p2 = p + theme_bw() +
  theme(text = element_text(size = 10),
        axis.title.y = element_text(angle = 0),
        #axis.text.x = element_text(angle = 90, vjust = .5),
        plot.margin=unit(c(0,0,0,0),"mm"))
# print(p2)
# dev.off()

pdf.dims = c((8.5-3), (11-3)) # width x height
if(args$long.width) pdf.dims = rev(pdf.dims)
pdf.dims[1] = args$scale.width*pdf.dims[1]
pdf.dims[2] = args$scale.height*pdf.dims[2]

dir = sprintf("~/data_assimilation/model_discrepancy/text/phd-thesis/%s/illustrations/lorenz05", args$folder)
filename = sprintf("%s/mle-%s.pdf", dir, args$savename)
pdf(filename, width = pdf.dims[1], height = pdf.dims[2])
print(p2)
dev.off()
