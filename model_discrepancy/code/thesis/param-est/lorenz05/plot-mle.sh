#!/bin/bash

# for update_type in apf etkf
# do
#   for inf in 00 10 15
#   do
#     add_cmd="-filter ${update_type} -inf ${inf}"
#     if [ "${inf}" == "00" ]; then add_cmd="${add_cmd} -scale.height .75"; else add_cmd="${add_cmd} -scale.height .5"; fi
#     name="${update_type}-inf${inf}"
#     nohup ./plot-mle.r ${name}-zoomed ${add_cmd} &> ${name}-zoomed.out &
#     nohup ./plot-mle.r ${name} ${add_cmd} -plotmaxmin &> ${name}.out &
#   done
# done

# nohup ./plot-mle.r apf-inf00-zoomed -filter apf -inf 00 -scale.height .75 &> apf-inf00-zoomed.out &
# nohup ./plot-mle.r apf-inf10-zoomed -filter apf -inf 10 -scale.height .5625 &> apf-inf10-zoomed.out &
# nohup ./plot-mle.r apf-inf15-zoomed -filter apf -inf 15 -scale.height .5625 &> apf-inf15-zoomed.out &
# nohup ./plot-mle.r etkf-inf00-zoomed -filter etkf -inf 00 -scale.height .5625 &> ektf-inf00-zoomed.out &
# nohup ./plot-mle.r etkf-inf10-zoomed -filter etkf -inf 10 -scale.height .375 &> etkf-inf10-zoomed.out &
# nohup ./plot-mle.r etkf-inf15-zoomed -filter ektf -inf 15 -scale.height .375 &> etkf-inf15-zoomed.out &
# nohup ./plot-mle.r apf-inf00 -filter apf -inf 00 -scale.height .75 -plotmaxmin &> apf-inf00.out &
# nohup ./plot-mle.r apf-inf10 -filter apf -inf 10 -scale.height .5625 -plotmaxmin &> apf-inf10.out &
# nohup ./plot-mle.r apf-inf15 -filter apf -inf 15 -scale.height .5625 -plotmaxmin &> apf-inf15.out &
# nohup ./plot-mle.r etkf-inf00 -filter etkf -inf 00 -scale.height .5625 -plotmaxmin &> ektf-inf00.out &
# nohup ./plot-mle.r etkf-inf10 -filter etkf -inf 10 -scale.height .375 -plotmaxmin &> etkf-inf10.out &
# nohup ./plot-mle.r etkf-inf15 -filter ektf -inf 15 -scale.height .375 -plotmaxmin &> etkf-inf15.out &

nohup ./plot-mle.r apf-inf00-zoomed -filter apf -inf 00 -scale.height .375 &> apf-inf00-zoomed.out &
nohup ./plot-mle.r apf-inf15-zoomed -filter apf -inf 15 -scale.height .375 &> apf-inf00-zoomed.out &
nohup ./plot-mle.r etkf-inf00-zoomed -filter etkf -inf 00 -scale.height .375 &> etkf-inf00-zoomed.out &
nohup ./plot-mle.r etkf-inf15-zoomed -filter etkf -inf 15 -scale.height .375 &> etkf-inf00-zoomed.out &

nohup ./plot-mle.r K-inf00-zoomed -inf 0 -param K -y.facet filter -scale.height .375 -scale.width .66 &> K-inf00-zoomed.out &
nohup ./plot-mle.r F-inf00-zoomed -inf 0 -param F -y.facet filter -scale.height .375 -scale.width .66 &> F-inf00-zoomed.out &
nohup ./plot-mle.r K-inf15-zoomed -inf 15 -param K -y.facet filter -scale.height .375 -scale.width .66 &> K-inf15-zoomed.out &
nohup ./plot-mle.r F-inf15-zoomed -inf 15 -param F -y.facet filter -scale.height .375 -scale.width .66 &> F-inf15-zoomed.out &

