

library(fields)

image.plot2 = function(plot.me, center.scale = FALSE, z.max = NULL, nlevels = 64, useRaster = TRUE, ...) {
  plot.me = t(plot.me)[,nrow(plot.me):1]
  
  if(center.scale) {
    col = colorRampPalette(c("blue", "white", "red"))(nlevels)
    if(is.null(z.max)) z.max = max(abs(range(plot.me)))
    zrange = z.max * c(-1,1)
    int = seq(zrange[1], zrange[2], length.out = nlevels + 1)
    image(x = 1:nrow(plot.me), y = 1:ncol(plot.me), z = plot.me, zlim = zrange, col = col, breaks = int, axes =
            FALSE, xlab = "", ylab = "", useRaster = useRaster, ...) 
  } else {
    col = tim.colors(nlevels)
    image(x = 1:nrow(plot.me), y = 1:ncol(plot.me), z = plot.me, col = col, axes = FALSE, xlab = "", ylab = "", useRaster = useRaster, ...)
  }
  
  box()
  inc = axTicks(2)
  axis(2, at = (ncol(plot.me):1)[inc], labels = (1:ncol(plot.me))[inc], las = 2)
  axis(3)
  
  if(center.scale) {
    image.plot(z = plot.me, legend.only = TRUE, horizontal = TRUE, zlim = zrange, col = col, breaks = int)
  } else {
    image.plot(z = plot.me, legend.only = TRUE, horizontal = TRUE, col = col)
  }
  
}


