

get.rmse = function(file, stat.names, run.info) {
  require(R.matlab)
  results = readMat(file)
  out = t(sapply(stat.names, function(type) quantile(results[[type]], c(.1, .5, .9), na.rm = TRUE)))
  colnames(out) = c("p10", "p50", "p90")
  out = data.frame(stat = stat.names, out)
  out[,names(run.info)] = run.info
  out
}

get.rmse.batch = function(run.folder, stat.names, folder.form, names.form, subset = NULL, num.form = NULL) {
  if(is.null(num.form)) num.form = rep(FALSE, length(names.form))

  # Set folder and get info
  runs = dir(run.folder)
  runs = runs[grep(sprintf("%s\\.mat", folder.form), runs)]
  run.info = sapply(1:length(names.form), function(i) gsub(folder.form, paste0("\\", i), runs))
  run.info = data.frame(run.info)
  names(run.info) = names.form
  for(i in 1:length(num.form)) {
    if(num.form[i]) run.info[,i] = as.numeric(levels(run.info[,i])[unclass(run.info[,i])])
  }

  # Subset
  idx.keep = rep(TRUE, length(runs))
  if(!is.null(subset)) for(type in names(subset)) idx.keep = idx.keep & run.info[,type] %in% subset[[type]]
  runs = runs[idx.keep]
  run.info = run.info[idx.keep,]

  # Get data from files and put into form for plotting
  files = paste0(run.folder, "/", runs)
  if(length(files) > 0) {
    out = lapply(1:length(files), function(i) get.rmse(files[i], stat.names, run.info[i,]))
    out = do.call(rbind, out)
    return(out)
  } else {
    return(NULL)
  }
}