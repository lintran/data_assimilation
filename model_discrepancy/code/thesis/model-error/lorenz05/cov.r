

# library(parallel)
# ncores = detectCores() - 1
# options(mc.cores = ncores)

source("~/data_assimilation/model_discrepancy/code/thesis/model-error/lorenz05/calc-linear-fun.r")
dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"
load(sprintf("%s/data.rdata", dr))

cov = lapply(data, cov.fast)
cor = lapply(cov, cov2cor)
cov$diff.x0 = cov.fast(data$diff, data$x0)
cor$diff.x0 = cov.fast(data$diff, data$x0, cor = TRUE)

save(cov, cor, file = sprintf("%s/cov.rdata", dr))

