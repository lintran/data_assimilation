#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("-long.width", action = "store_true")
parser$add_argument("-scale.height", type = "double", default = .5)
parser$add_argument("-scale.width", type = "double", default = 1)
# args = parser$parse_args()
args = parser$parse_args(c("-long.width"))

library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/get-rmse.r")

# Get rmses -----------------------------------------------------------------------------------
run.folder = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/plugin/"
folder.form = "([[:alnum:]\\-]+)\\-dt([[:digit:]]+)\\-M([[:digit:]]+)\\-([[:digit:]]+).*"
names.form = c("type", "dt", "M", "run")
num.form = c(FALSE, TRUE, TRUE, TRUE)

stat.names = c("rmse", "crps")
subset = NULL
# if(!is.na(args$filter)) subset = c(subset, list(filter = args$filter))
# if(!is.na(args$inf)) subset = c(subset, list(infcode = args$inf))
# if(!is.na(args$B)) subset = c(subset, list(B = args$B))
# if(!is.na(args$M)) subset = c(subset, list(M = args$M))

# get rmses
rmse = get.rmse.batch(run.folder, stat.names, folder.form, names.form, subset = subset, num.form = num.form)
summary(rmse)

# get unique ids
uni = lapply(rmse[,!colnames(rmse) %in% c("p10", "p50", "p90", "run")], unique)
rmse.all = expand.grid(uni)
rmse.all$model = NA
# sprint.form = "%-15s"
# rmse.all$model[rmse.all$type == "right"] = sprintf(sprint.form, "R")
# rmse.all$model[rmse.all$type == "wrong"] = sprintf(sprint.form, "W")
# rmse.all$model[rmse.all$type == "est"] = sprintf(sprint.form, "W + online")
# rmse.all$model[rmse.all$type == "plugin-svd"] = sprintf(sprint.form, "W + truth_SVD")
# rmse.all$model[rmse.all$type == "plugin-est"] = sprintf(sprint.form, "W + batch")
# rmse.all$model[rmse.all$type == "plugin-truth"] = sprintf(sprint.form, "W + truth_param")
rmse.all$stat2 = NA
rmse.all$stat2[rmse.all$stat == "rmse"] = "RMSE"
rmse.all$stat2[rmse.all$stat == "crps"] = "ES"
rmse.all$day = rmse.all$dt / 24

rmse = merge(rmse, rmse.all, all.x = TRUE)

# merge right and wrong with all methods
format = "([[:alpha:]\\-]+)([[:digit:]q]+)"
rmse$type1 = gsub(format, "\\1", rmse$type)
rmse$type2 = gsub(format, "\\2", rmse$type)

temp = strsplit(rmse$type2, "q")
rmse$type2 = sapply(temp, function(x) x[1])
rmse$type3 = sapply(temp, function(x) x[2])

idx = rmse$type %in% c("right", "wrong")
temp = rmse[idx,]
rmse = rmse[!idx,]
temp = temp[,!colnames(temp) %in% c("type1", "type2")]
temp$type2 = temp$type #ifelse(temp$type == "right", -10, 110)
temp = lapply(unique(rmse$type1), function(x) data.frame(temp, type1 = x))
rmse = do.call("rbind", c(list(rmse), temp))

# Plot distribution of rmses --------------------------------------------------------------------
plot.data = subset(rmse, stat2 == "ES")
plot.data = subset(plot.data, !(type1 == "plugin-optL" & type2 == "100"))

p = ggplot(plot.data, aes(dt, p50, linetype= type2, color = type3))
p = p + geom_line() #+ geom_point()
# p = p + geom_ribbon(aes(ymin = p10, ymax = p90), alpha = .5)
p = p + facet_grid(~type1, drop = TRUE, scales = "free")
p = p + ylab("")
# p = p + xlab("day")
p = p + xlab(expression(paste(Delta, t[n])))
# p = p + scale_colour_manual(values = cols) + scale_fill_manual(values = cols)
p = p + coord_cartesian(ylim=c(0, 1.5))

p2 = p + theme_bw() +
  theme(text = element_text(size = 10),
        # axis.title.y = element_text(angle = 0),
        #axis.text.x = element_text(angle = 90, vjust = .5),
        plot.margin=unit(c(0,3,0,0),"mm"),
        legend.position="bottom",
        # legend.key.width=unit(1.5,"line"),
        legend.title=element_blank())

# pdf.dims = c((8.5-3), (11-3)) # width x height
# if(args$long.width) pdf.dims = rev(pdf.dims)
# pdf.dims[1] = args$scale.width*pdf.dims[1]
# pdf.dims[2] = args$scale.height*pdf.dims[2]
pdf.dims = c(11, 8.5)

dir = "~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/lorenz05/"
dir.create(dir, showWarnings = FALSE, recursive = TRUE)
filename = sprintf("%s/rmse.pdf", dir)
pdf(filename, width = pdf.dims[1], height = pdf.dims[2])
print(p2)
dev.off()

