
library(ggplot2)
library(grDevices)
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/image.plot2.r")
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/lorenz05/calc-linear-fun.r")

dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"
load(sprintf("%s/cov.rdata", dr))
dr = "~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/lorenz05/"


filename = sprintf("%s/true-spatial-cor.pdf", dr)
pdf(filename, width = .8*(8.5 - 3), height = .8*(8.5 - 3))
for(type1 in c("cov", "cor")) {
  for(type2 in c("x0", "diff.x0", "diff")) {
    if(type1 == "cov") {
      temp = cov[[type2]]
    } else {
      temp = cor[[type2]]
    }
    par(cex.axis = .75)
    image.plot2(temp, center = TRUE, legend.mar = c(2.5, 2, 2, 1) + .1)
  }  
}
dev.off()

# cmd = sprintf("cd %s; pdf2ps true-spatial-cor.pdf - | ps2pdf - true-spatial-cor-flattened.pdf", dr)
# system(cmd)

plot.data = list(cov = NULL, cor = NULL)
plot.data.mean = list(cov = NULL, cor = NULL)

for(type1 in c("cov", "cor")) {
  for(type2 in c("x0", "diff.x0", "diff")) {
    if(type1 == "cov") {
      temp = cov[[type2]]
    } else {
      temp = cor[[type2]]
    }
    #     temp.svd = svd(temp)
    #     var.perc = cumsum(temp.svd$d) / sum(temp.svd$d)
    #     temp.svd$d[var.perc > .95] = 0
    #     temp.new = temp.svd$u %*% diag(temp.svd$d) %*% t(temp.svd$v)
    #     image.plot2(temp, center = TRUE)
    #     image.plot2(temp.new, center = TRUE)
    
    for(row in 2:nrow(temp)) temp[row,] = temp[row,c(row:960,1:(row-1))]
    plot.data[[type1]][[type2]] = 
      data.frame(loc = rep(1:960, 960), 
                 dist1 = rep(0:959, each = 960), 
                 dist = rep(c(0:480, 479:1), each = 960), 
                 cov = as.numeric(temp))
    plot.data[[type1]][[type2]]$direction = -1
    plot.data[[type1]][[type2]]$direction[plot.data[[type1]][[type2]]$dist1 %in% 0:480] = 1
    plot.data.mean[[type1]][[type2]] = data.frame(aggregate(cov ~ dist, plot.data[[type1]][[type2]], mean), loc = "mean", direction = "mean")
  }
}

xbreaks = seq(0, 480, length.out = 5)
xlabs = expression(0, pi/4, pi/2, 3*pi/4, pi)

filename = sprintf("%s/true-spatial-cor-bydist.pdf", dr)
pdf(filename, width = 0.75*(8.5 - 3), height = 0.75*1/2*(8.5 - 3))
for(type1 in "cov") { #c("cov", "cor")) {
  for(type2 in c("x0", "diff.x0")) { #c("x0", "diff.x0", "diff")) {
    p = ggplot(plot.data[[type1]][[type2]], aes(dist, cov, group = interaction(loc, direction)))
    p = p + geom_hline(yintercept = 0, size = .25) + geom_vline(xintercept = 0, size = .25)
    p = p + geom_line(color = "gray", alpha = .1)
    p = p + geom_line(data = plot.data.mean[[type1]][[type2]], color = "gray50") #, color = "gray50")
    p = p + scale_x_continuous(breaks = xbreaks, labels = xlabs)
    p = p + xlab("distance (h)")
    if(type1 == "cov") {
      p = p + ylab("covariance")
    } else {
      p = p + ylab("correlation")
    }
    
    p2 = p + theme_bw() +
      theme(panel.border = element_blank(),
            text = element_text(size = 10),
            plot.margin=unit(c(0,1.5,0,0),"mm"))
    print(p2)
  }  
}
dev.off()

# pdf2ps true-spatial-cor-bydist.pdf - | ps2pdf - true-spatial-cor-bydist-flattened.pdf

dvec = 0:480
n = 20
wc.fun = function(theta, data, q, n) {
  pred = theta[1] * wendland.cos(dvec/960, theta[2], theta[3], q, theta[4], n)
  sum(abs(pred - data))
}
w.fun = function(theta, data, q, n) {
  pred = theta[1] * wendland(dvec/960, theta[2], theta[3], q)
  sum(abs(pred - data))
}

fit = list(cov = NULL, cor = NULL)
descent.out = list(cov = NULL, cor = NULL)
set.seed(12345)
for(type1 in "cov") { #c("cov", "cor")) {
  for(type2 in c("x0", "diff.x0")) {
    lb = c(0,0,4,0)
    m = ifelse(type1 == "cov", max(abs(range(subset(plot.data[[type1]][[type2]], dist == 0)$cov))), 1)*1.25
    ub = c(m, max(dvec)/960, 50, 1)
    if(type2 == "diff") {
      lb = lb[1:3]
      ub = ub[1:3]
    }
    if(type2 == "diff.x0") lb[1] = -m
    theta1 = c(mean(subset(plot.data[[type1]][[type2]], dist == 0)$cov), .4, 4.01, 0.01)
    lc = bd.to.lc(lb, ub)
    descent.out[[type1]][[type2]] = 
      constrOptim(theta1, if(type2 == "diff") w.fun else wc.fun, 
                  # method = "SANN",
                  control = list(trace = 1, REPORT = 100, maxit = 1e5),
                  grad = NULL, ui = lc$ui, ci = lc$ci,
                  data = plot.data.mean[[type1]][[type2]]$cov, q = 1, n = n)
    descent.out[[type1]][[type2]] = 
      constrOptim(descent.out[[type1]][[type2]]$par, if(type2 == "diff") w.fun else wc.fun, 
                  method = "SANN",
                  control = list(trace = 1, REPORT = 100, maxit = 1e5),
                  grad = NULL, ui = lc$ui, ci = lc$ci,
                  data = plot.data.mean[[type1]][[type2]]$cov, q = 1, n = n)
    print(descent.out[[type1]][[type2]])
    
    theta = descent.out[[type1]][[type2]]$par
    pred = theta[1] * wendland.cos(dvec/960, theta[2], theta[3], 1, if(type2 != "diff") theta[4] else 0, n)
    
    fit[[type1]][[type2]] = rbind(plot.data.mean[[type1]][[type2]], data.frame(dist = dvec, cov = pred, loc = "fitted", direction = "fitted"))
  }
}

xbreaks = seq(0, 480, length.out = 5)
xlabs = expression(0, pi/4, pi/2, 3*pi/4, pi)

filename = sprintf("%s/true-spatial-cor-bydist-fitted.pdf", dr)
pdf(filename, width = 0.5*(8.5 - 3), height = 0.45*(8.5 - 3))
for(type1 in "cov") { #c("cov", "cor")) {
  for(type2 in c("x0", "diff.x0")) {
    p = ggplot(fit[[type1]][[type2]], aes(dist, cov, color = loc, linetype = loc))
    p = p + geom_hline(yintercept = 0) + geom_vline(xintercept = 0)
    p = p + geom_line(size = .75)
    p = p + scale_colour_manual(values = c("mean" = "gray50", "fitted" = "blue"))
    p = p + scale_x_continuous(breaks = xbreaks, labels = xlabs)
    p = p + xlab("distance (h)")
    if(type1 == "cov") {
      p = p + ylab("covariance")
    } else {
      p = p + ylab("correlation")
    }
    
    p2 = p + theme_bw() +
      theme(panel.border = element_blank(),
            legend.key = element_blank(),
            legend.position="bottom",
            legend.title=element_blank(),
            # legend.margin = unit(c(0,0,0,0),"mm"),
            text = element_text(size = 10),
            plot.margin=unit(c(0,1.5,0,0),"mm"))
    print(p2)
  }  
}
dev.off()
