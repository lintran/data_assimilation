#!/usr/bin/env Rscript

library("argparse")
parser = ArgumentParser()

# parser$add_argument("-I", type = "double", default = 12)
# parser$add_argument("-b", type = "double", default = 10)
# parser$add_argument("-c", type = "double", default = 2.5)
# parser$add_argument("-N", type = "double", default = 960)
# parser$add_argument("-K", type = "double", default = 32)
# parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("initfile", type = "character")
parser$add_argument("-header", type = "character", default = "data")
# args = parser$parse_args()
args = parser$parse_args("inits")
print(args)

# load necessary libraries and files
source("~/data_assimilation/model_discrepancy/code/lorenz05/lorenz05.r")
lorenz05.temp = function(y, parms) lorenz05(0, y, parms)[[1]]

# sample from right trajectory
print("Load data from right trajectory...")
dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"
load(sprintf("%s/%s.rdata", dr, args$initfile))

# evaluate derivative at samples
print("Evaluating derivatives...")
data$dR = t(apply(data$x0, 1, lorenz05.temp, parms = parms3))
data$dW = t(apply(data$x0, 1, lorenz05.temp, parms = parms2))
data$diff = data$dR - data$dW

print("Saving...")
args.save = args
filename = sprintf("%s/%s.rdata", dr, args$header)
save(data, args.save, x0, parms3, parms2, file = filename)




