
library(spam)
source("~/data_assimilation/model_discrepancy/code/lorenz05/lorenz05.r")
source("~/data_assimilation/model_discrepancy/code/image.plot2.r")

wendland = function(dist, cutoff, tau, q = 1) {
#   dist: distance matrix / vector
#   cutoff: range parameter
#   tau: shape parameter
#   q: cov fn is 2*q-times continuously differentiable, thus GP is q-times mean-square differentiable

  if(cutoff <= 0) stop("'cutoff' must be greater than 0.")
  if(tau < 2*(q+1)) stop("'tau' must be greater than or equal to 2*('q'+1).")
  dist = dist / cutoff
  a = fplus(1-dist)^tau

  if(q == 0) {
    num = 1
  } else if(q == 1) {
    num = 1 + tau*dist
  } else if(q == 2) {
    num = 3 + 3*tau*dist + (tau^2 - 1) * dist^2
  } else {
    stop("'q' must take integer values between 0 and 2, inclusive.")
  }

  den = 1
  if(q == 2) den = 3

  return(a * num / den)
}

fplus = function(a) a*(a>0)

wendland.cos = function(dist, cutoff, tau, q, period.frac, n, dim = 1) {
  if(period.frac < 0 | period.frac > 1) stop("'period.frac' must be between 0 and 1, inclusive.")
  if(n < 0) stop("'n' must be an integer greater than or equal to 0.")

  w = wendland(dist, cutoff, tau, q)

  dist = 2*pi* n*period.frac * dist/cutoff
  tau = 1/tan(pi/(2*dim))
  c = exp(-tau*dist) * cos(dist)

  return(w*c)
}

rotate.matrix = function(mat, N) {
  blah = sapply(1:N, function(i) wrap(seq(i,N+i-1)+N/2, N))
  idx.rotate = cbind(rep(1:N, each = N), as.numeric(blah))
  matrix(mat[idx.rotate], nrow = N, byrow = TRUE)
}

dist.on.circle = function(N) {
  out = as.matrix(dist(1:N))
  out[out > N/2] = N - out[out > N/2]
  out
}

Sigma.over.folds = function(fold, fold.idx, data, X.name, Y.name, verbose = TRUE) {
  if(verbose) cat(fold, "")
  in.idx = fold.idx != fold
  out =
    list(X.name = X.name, Y.name = Y.name,
         in.idx = which(in.idx), out.idx = which(!in.idx),
         XX = cov.fast(data[[X.name]][in.idx,]),
         XY = cov.fast(data[[X.name]][in.idx,], data[[Y.name]][in.idx,]),
         YY = cov.fast(data[[Y.name]][in.idx,]))
  out$mu = sapply(data[c(X.name, Y.name)], colMeans)
  out
}

mclapply2 = function(...) {
  if(getOption("mc.cores") == 1) {
    lapply(...)
  } else {
    mclapply(...)
  }
}

cov.fast = function(x, y = NULL, cor = FALSE, center = TRUE) {
  n = nrow(x)
  x = scale(x, center = center, scale = cor)

  if(is.null(y)) {
    return(1/(n-1) * crossprod(x))
  } else {
    y = scale(y, center = center, scale = cor)
    return(1/(n-1) * crossprod(x, y))
  }
}

linear.svd = function(theta, Sigma, ss = FALSE, data = NULL) {
  # data: replicates should be stacked in columns

  if(theta < 1) {
    svd.out = svd(Sigma$XX)
    var.perc = cumsum(svd.out$d) / sum(svd.out$d)
    svd.out$d[var.perc > theta] = Inf
    Lt = svd.out$u %*% diag(1/svd.out$d) %*% crossprod(svd.out$v, Sigma$XY) # XX^-1 XY
  } else {
    Lt = solve(Sigma$XX, Sigma$XY)
  }
  L = t(Lt)
  b = drop(Sigma$mu[,Sigma$Y.name] - crossprod(Lt, Sigma$mu[,Sigma$X.name]))

  if(ss) { # just return sum of squares
    Yhat = b + L %*% data[[Sigma$X.name]]
    res = Yhat - data[[Sigma$Y.name]]
    out = sqrt(mean(res^2))
  } else { # return estimates
    out = list(L = L, b = b, theta = theta)
  }

  return(out)
}

linear.param = function(theta, dist.vec, n, Sigma, ss = FALSE, data = NULL, est.b = FALSE,
                        q.XX = 2, q.XY = 2, q.YY = 2, fix.tau = FALSE) {
  # data: replicates should be stacked in columns and normalized inverse FFT already taken
  nm = c("sXY", "cXY", if(!fix.tau) "tXY" else NULL, if(n > 0) "pXY" else NULL,
         "sXX", "cXX", if(!fix.tau) "tXX" else NULL, if(n > 0) "pXX" else NULL)
  nm = c(nm, "sYY", "cYY", if(!fix.tau) "tYY" else NULL)
  names(theta) = nm

  if(fix.tau) {
    tau = c(XY = 2*(q.XY+1), XX = 2*(q.XX+1), YY = 2*(q.YY+1))
  } else {
    tau = c(XY = theta["tXY"], XX = theta["tXX"], YY = theta["tYY"])
  }

  if(n > 0) {
    c.XX = wendland.cos(dist.vec, cutoff = theta["cXX"], tau = tau["XX"], q = q.XX,
                        period.frac = theta["pXX"], n = n, dim = 1)
    c.XY = wendland.cos(dist.vec, cutoff = theta["cXY"], tau = tau["XY"], q = q.XY,
                        period.frac = theta["pXY"], n = n, dim = 1)
  } else {
    c.XX = wendland(dist.vec, cutoff = theta["cXX"], tau = tau["XX"], q = q.XX)
    c.XY = wendland(dist.vec, cutoff = theta["cXY"], tau = tau["XY"], q = q.XY)
  }
  c.XX = theta["sXX"] * c.XX
  c.XY = theta["sXY"] * sqrt(theta["sXX"]) * sqrt(theta["sYY"]) * c.XY
  c.XX.fft = fft(c.XX)
  c.XY.fft = fft(c.XY)
  c.L.fft = c.XY.fft / c.XX.fft
  # how this is applied: Re(fft( c.L.fft * fft(x, TRUE) / length(x) ))

  if(ss) { # return loglik
    c.YY = theta["sYY"] * wendland(dist.vec, cutoff = theta["cYY"], tau = tau["YY"], q = q.YY)
    c.YY.fft = fft(c.YY)
    c.YgivenX.fft = c.YY.fft - c.L.fft * c.XY.fft

    det.YgivenX = logdet.circulant(c.YgivenX.fft)
    if(!is.na(det.YgivenX)) c.YgivenX.fft = abs(c.YgivenX.fft)
    res.sq = Re(data[["YtY"]] - c.L.fft*data[["2YtX"]] + (c.L.fft^2)*data[["XtX"]]) # = crossprod(y - Lx)
    if(any(res.sq < 0)) {
      out.YgivenX = -Inf
    } else {
      res.sq = res.sq / c.YgivenX.fft
      rss = Re(sum(res.sq))
      out.YgivenX = det.YgivenX + rss
    }

    det.XX = logdet.circulant(c.XX.fft)
    if(!is.na(det.XX)) c.XX.fft = abs(c.XX.fft)
    res.sq = data[["XtX"]] / c.XX.fft
    rss = Re(sum(res.sq))
    out.X = det.XX + rss

    out = -.5 * (out.YgivenX + out.X)
    # print(out)
    # print(c(out.YgivenX, out.X))
    # print(c(logdet.circulant(c.XX.fft), logdet.circulant(c.YY.fft), logdet.circulant(c.YgivenX.fft)))
  } else { # return estimates
    d = length(c.L.fft)
    L = circulant(Re(fft(c.L.fft, TRUE)  / d))
    b = rep(0, d)
    if(est.b) b = drop(Sigma$mu[,Sigma$Y.name] - L %*% Sigma$mu[,Sigma$X.name])
    out = list(c.L.fft = c.L.fft, L = L, b = b, theta = theta, n = n, dist.vec = dist.vec,
               q.XY = q.XY, q.XX = q.XX, q.YY = q.YY)
  }

  return(out)
}

circulant = function(x) {
  # Creates a circulant matrix from a vector
  # http://stackoverflow.com/questions/15795318/efficient-way-to-create-a-circulant-matrix-in-r
  n = length(x)
  suppressWarnings(matrix(x[matrix(1:n,n+1,n+1,byrow=T)[c(1,n:2),1:n]],n,n))
}

logdet.circulant = function(c.fft) {
  eigen = Re(c.fft)
  if(sum(eigen < 0) %% 2 == 0) { # even number of negative eigenvalues
    return(sum(log(abs(eigen))))
  } else {
    return(NA)
  }
}

apply.fft = function(c.fft, x, x.ifft = FALSE) {
  if(!is.matrix(x)) {
    if(!x.ifft) x = fft(x, TRUE)
    out = Re(fft( c.fft * x ))
  } else {
    # vectors stacked by columns
    if(!x.ifft) x = mvfft(x, TRUE)
    out = Re(mvfft( c.fft * x ))
  }
  return(out)
}

sprintf2 = function(fmt, x) sapply(x, function(i) sprintf(fmt, i))

bd.to.lc = function(lower, upper) {
  # returns constraint matrix given lower and upper values, where ui %*% theta - ci >= 0
  ui = rbind(diag(1, length(lower)), diag(-1, length(upper)))
  ci = c(lower, -upper)
  list(ui = ui, ci = ci)
}

lcbind = function(a, b) {
  # binds constraint matrix
  ui = rbind(cbind(a$ui, array(0, c(nrow(a$ui), ncol(b$ui)))),
             cbind(array(0, c(nrow(b$ui), ncol(a$ui))), b$ui))
  ci = c(a$ci, b$ci)
  list(ui = ui, ci = ci)
}

# function for fft
data.fft.mean = function(data, Sigma = NULL, center = FALSE, transpose = FALSE) {
  if(transpose) data = lapply(data, t)
  if(center) for(n in names(data)) data[[n]] = data[[n]] - Sigma$mu[,n]
  n = ncol(data[[1]])
  d = nrow(data[[1]])

  out = list("YtY" = 0, "2YtX" = 0, "XtX" = 0, "n" = n, "d" = d)
  for(i in 1:n) {
    Y.fft = fft(data[[Sigma$Y.name]][,i])
    X.fft = fft(data[[Sigma$X.name]][,i])
    Y.ifft = fft(data[[Sigma$Y.name]][,i], inverse = TRUE)
    X.ifft = fft(data[[Sigma$X.name]][,i], inverse = TRUE)
    out[["YtY"]] = out[["YtY"]] + Y.fft * Y.ifft
    out[["2YtX"]] = out[["2YtX"]] + Y.fft * X.ifft + X.fft * Y.ifft
    out[["XtX"]] = out[["XtX"]] + X.fft * X.ifft
  }
  for(i in c("YtY", "2YtX", "XtX")) out[[i]] = out[[i]] / (d*n) # divide by fft scaling and to take the mean
  return(out)
}


