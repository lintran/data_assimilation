#!/bin/bash
ncpus=1
qsub_ncpus=""
if [ "${ncpus}" -gt "1" ]; then
  qsub_ncpus="-pe smp ${ncpus}"
fi
qsub_command="qsub ${qsub_ncpus} -j y" #"-j y -js 1"

mkdir -p outfiles

for n in `seq 0 10 100`
do
  for est in "L" #"Lb"
  do
    for i in `seq 1 5`
    do
      for qXY in 0 1 2
      do
        for qXX in 0 1 2
        do
          for qYY in 0 1 2
          do
            name=$(printf est-opt${est}%03iq${qXY}${qXX}${qYY}-fold$i $n)
            if [ -e "outfiles/${name}.out" ]; then rm outfiles/${name}.out; fi
            if [ -e "errfiles/${name}.err" ]; then rm errfiles/${name}.err; fi
            echo "export OMP_NUM_THREADS=${ncpus}; ./calc-linear-param.r ${est} $n -fold $i -q.XY ${qXY} -q.XX ${qXX} -q.YY ${qYY} -fix.tau" | ${qsub_command} -N ${name} -o outfiles/${name}.out -e errfiles/${name}.err
            sleep 1
          done
        done
      done
    done
  done
done