#!/bin/bash
ncpus=1
qsub_ncpus=""
if [ "${ncpus}" -gt "1" ]; then
  qsub_ncpus="-pe smp ${ncpus}"
fi
qsub_command="qsub ${qsub_ncpus} -j y" #"-j y -js 1"

mkdir -p outfiles

for n in 95 100
do
  for i in `seq 1 5`
  do
    n100=$(echo "${n}/100" | bc -l)
    name=$(printf svd%03i-fold$i $n)
    if [ -e "outfiles/${name}.out" ]; then rm outfiles/${name}.out; fi
    if [ -e "errfiles/${name}.err" ]; then rm errfiles/${name}.err; fi
    echo "export OMP_NUM_THREAD=${ncpus}; ./calc-linear-svd.r ${n100} -ncores ${ncpus} -fold $i" | ${qsub_command} -N ${name} -o outfiles/${name}.out -e errfiles/${name}.err
    sleep .5
  done
done
