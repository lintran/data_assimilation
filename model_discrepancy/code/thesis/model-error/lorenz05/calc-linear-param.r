#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()
parser$add_argument("est", type = "character")
parser$add_argument("n", type = "double")
parser$add_argument("-fix.tau", action = "store_true", default = FALSE)
parser$add_argument("-q.XX", type = "integer", default = 1)
parser$add_argument("-q.YY", type = "integer", default = 1)
parser$add_argument("-q.XY", type = "integer", default = 1)
parser$add_argument("-fold", type = "integer", default = 1)
parser$add_argument("-ncores", type = "double", default = 1)
args = parser$parse_args()
# args = parser$parse_args(c("L", "10", "-fold", "0"))
print(args)

# load necessary libraries and files
library(R.matlab)
if(args$ncores > 1) {
  library(RhpcBLASctl)
  omp_set_num_threads(args$ncores)
}
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/lorenz05/calc-linear-fun.r")

# load data
cat("Loading data...\n")
dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"

if(args$fold == 0) {
  filename = sprintf("%s/cov.rdata", dr)
  load(filename)
  Sigma = list(XX = cov$x0, XY = t(cov$diff.x0), YY = cov$diff, X.name = "x0", Y.name = "diff")
  gc()

  filename = sprintf("%s/data.rdata", dr)
  load(filename)
  data = data[c("x0", "diff")]
  Sigma$mu = sapply(data, colMeans)
  in.data = data.fft.mean(data, Sigma, center = args$est == "Lb", transpose = TRUE)
  rm(data); gc()

  # distance matrix
  dmat = dist.on.circle(parms3$N) / parms3$N # divide dmat by N so it matches up with python code
} else {
  filename = sprintf("%s/cvdata%i-%sfft.rdata", dr, args$fold, ifelse(args$est == "Lb", "center", ""))
  load(filename)
}

# transpose data and take inverse fft ahead of time to speed up calculations
# sort(sapply(objects(), function(x) object.size(get(x))), decreasing = TRUE)
cat("Preparing data...\n")
dvec = dmat[1,]

# set up initialization
cat("Setting up initialization...\n")

Sigma.mean = lapply(Sigma[c("XX", "XY", "YY")], function(x) {
  for(row in 2:nrow(x)) x[row,] = x[row,c(row:960,1:(row-1))]
  colMeans(rbind(x[,1:481], x[,c(1,960:481)]))
})

var.range = lapply(Sigma[c("XX", "XY", "YY")], function(x) max(abs(range(diag(x)))))
lb = list(XX = c(0,0,2*(args$q.XX+1),0), YY = c(0,0,2*(args$q.YY+1)), XY = c(-.5,0,2*(args$q.XY+1),0))
ub = list(XX = c(var.range$XX*1.25, max(dmat), 20, 1),
          YY = c(var.range$YY*1.25, max(dmat), 20),
          XY = c(.5, max(dmat), 20, 1))
Sigma = Sigma[!names(Sigma) %in% c("XX", "XY", "YY")]; gc()

if(args$fix.tau) {
  lb = lapply(lb, function(x) x[-3])
  ub = lapply(ub, function(x) x[-3])
}

if(args$n > 0) {
  lb = c(lb$XY, lb$XX, lb$YY)
  ub = c(ub$XY, ub$XX, ub$YY)
} else {
  lb = c(lb$XY[-length(lb$XY)], lb$XX[-length(lb$XX)], lb$YY)
  ub = c(ub$XY[-length(ub$XY)], ub$XX[-length(ub$XX)], ub$YY)
}
theta1 = rowMeans(cbind(lb, ub))
lc = bd.to.lc(lb, ub)

# run descent for CV
cat("Running descent...\n")

time = system.time(linear.param(theta1, dist.vec = dvec, n = args$n, ss = TRUE, data = in.data,
                                q.XX = args$q.XX, q.YY = args$q.YY, q.XY = args$q.XY, fix.tau = args$fix.tau))[3]
print(sprintf("Time estimate for 10,000 iterations: %.02f min", 1e4*time / 60))

set.seed(12345)
system.time(
  descent.out <-
    constrOptim(theta1, linear.param,
                control = list(trace = 1, fnscale = -1, maxit = 1e5),
                grad = NULL, ui = lc$ui, ci = lc$ci,
                dist.vec = dvec, n = args$n, Sigma = Sigma, ss = TRUE, data = in.data,
                q.XX = args$q.XX, q.YY = args$q.YY, q.XY = args$q.XY, fix.tau = args$fix.tau,
                est.b = args$est == "Lb")
)
print(descent.out)
print(round(descent.out$par, 3))

# Calculate L
cat("Calculating L...\n")
linear = linear.param(descent.out$par, dist.vec = dvec, n = args$n, Sigma = Sigma,
                      q.XX = args$q.XX, q.YY = args$q.YY, q.XY = args$q.XY, fix.tau = args$fix.tau,
                      ss = FALSE, est.b = args$est == "Lb")

# Calculate out-of-sample RSS
if(args$fold != 0) {
  res.sq = out.data[["YtY"]] - linear$c.L.fft*out.data[["2YtX"]] + (linear$c.L.fft^2)*out.data[["XtX"]]
  linear$rmse = sqrt(Re(sum(res.sq)) / parms3$N)
  print(linear$rmse)
}

# save
cat("Saving...\n")
filename = sprintf("%s/opt%s%03iq%i%i%i%s.rdata", dr,
                   args$est, as.integer(args$n),
                   args$q.XY, args$q.XX, args$q.YY,
                   ifelse(args$fold == 0, "", sprintf("-fold%02i", as.integer(args$fold))))
args.save = c(args.save, args)
save(linear, descent.out, args.save, file = filename)

if(args$fold == 0) {
  filename = gsub("rdata", "mat", filename)
  writeMat(filename, c.L.fft = linear$c.L.fft, L = linear$L, b = linear$b, theta = descent.out$par, n = linear$n,
           q.XX = args$q.XX, q.YY = args$q.YY, q.XY = args$q.XY)
}


