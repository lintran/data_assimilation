#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
parser$add_argument("-long.width", action = "store_true")
parser$add_argument("-scale.height", type = "double", default = .5)
parser$add_argument("-scale.width", type = "double", default = 1)
args = parser$parse_args()
# args = parser$parse_args(c("-long.width"))

library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/get-rmse.r")

plot.add = function(p) {
  p = p + geom_line()
  p = p + ylab("ES") + xlab(expression(paste(Delta, t[n] %*% 1000)))
  p = p + scale_color_discrete(breaks = legend.vals, labels = legend.labs)
  p = p + scale_linetype(breaks = legend.vals, labels = legend.labs)

  p2 = p + theme_bw() +
    theme(text = element_text(size = 10),
          # axis.title.y = element_text(angle = 0),
          #axis.text.x = element_text(angle = 90, vjust = .5),
          plot.margin=unit(c(0,3,0,0),"mm"),
          legend.text.align = 0,
          legend.title=element_blank())
  return(p2)
}

# Get rmses -----------------------------------------------------------------------------------
run.folder = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/plugin/"
folder.form = "([[:alnum:]\\-]+)\\-dt([[:digit:]]+)\\-M([[:digit:]]+)\\-([[:digit:]]+).*"
names.form = c("type", "dt", "M", "run")
num.form = c(FALSE, TRUE, TRUE, TRUE)

stat.names = c("rmse", "crps")
subset = NULL
# if(!is.na(args$filter)) subset = c(subset, list(filter = args$filter))
# if(!is.na(args$inf)) subset = c(subset, list(infcode = args$inf))
# if(!is.na(args$B)) subset = c(subset, list(B = args$B))
# if(!is.na(args$M)) subset = c(subset, list(M = args$M))

# get rmses
rmse = get.rmse.batch(run.folder, stat.names, folder.form, names.form, subset = subset, num.form = num.form)
summary(rmse)
rmse = subset(rmse, stat == "crps")

unique(rmse$type)
blah = aggregate(p50 ~ type, rmse, median)
blah[order(blah$p50),]

opt.choose = "plugin-optL000q220"
est.choose = "estL010"
rmse = subset(rmse, type %in% c("right", "wrong", "plugin-svd100", "plugin-svd095", opt.choose, est.choose))

rmse$type1 = "other"
rmse$type1[substring(rmse$type, 1, 6) == "plugin"] = "truth"
rmse$type1[substring(rmse$type, 1, 3) == "est"] = "est"
rmse$type2 = rmse$type

# merge right and wrong with all methods
idx = rmse$type %in% c("right", "wrong")
right.wrong = rmse[idx,]
rmse = rmse[!idx,]
temp = right.wrong[,!colnames(right.wrong) %in% c("type1", "type2")]
temp = lapply(unique(rmse$type1), function(x) data.frame(temp, type1 = x, type2 = temp$type))
rmse = do.call("rbind", c(list(rmse, right.wrong), temp))

# merge others
idx = rmse$type == opt.choose
temp = rmse[idx,]
temp$type1 = "est"
rmse = rbind(rmse, temp)

# Plot distribution of rmses --------------------------------------------------------------------
legend.vals = c("right", "wrong", "plugin-svd100", "plugin-svd095", opt.choose, est.choose)
legend.labs = expression(R, W, W + hat(L), W + hat(hat(L)), "", "")
legend.labs[5] = expression(W + paste("L(", S, ",", S, "; ", hat(theta), ")"))
legend.labs[6] = expression(W + paste(L[t], "(", S, ",", S, "; ", hat(theta)[t], ")"))

p = list()
p[[1]] = ggplot(subset(rmse, type1 == "truth"), aes(dt, p50, linetype = type2, color = type2))
p[[2]] = ggplot(subset(rmse, type1 == "est"), aes(dt, p50, linetype = type2, color = type2))

pdf.dims = c((8.5-3), (11-3)) # width x height
if(args$long.width) pdf.dims = rev(pdf.dims)
pdf.dims[1] = args$scale.width*pdf.dims[1]
pdf.dims[2] = args$scale.height*pdf.dims[2]

dir = "~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/lorenz05/"
dir.create(dir, showWarnings = FALSE, recursive = TRUE)
filename = sprintf("%s/rmse.pdf", dir)
pdf(filename, width = pdf.dims[1], height = pdf.dims[2])
for(i in 1:length(p)) print(plot.add(p[[i]]))
dev.off()

