#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()
parser$add_argument("-data.file", type = "character", default = "data")
parser$add_argument("-nfolds", type = "double", default = 5)
parser$add_argument("-seed", type = "double", default = 12345)
args = parser$parse_args()
# args = parser$parse_args("data")
print(args)

# load libraries
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

# load data
cat("Loading data...\n")
dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"
filename = sprintf("%s/%s.rdata", dr, args$data.file)
load(filename)
data = data[c("x0", "diff")]
gc()

dmat = dist.on.circle(parms3$N) / parms3$N # divide dmat by N so it matches up with python code

# create folds for CV
cat("Create folds...\n")
set.seed(args$seed)
fold.idx = sample(args$nfolds, nrow(data$x0), replace = TRUE)
table(fold.idx)

# save fold data
args.save = c(args.save, args[c("nfolds", "seed")], list(fold = 1))
for(fold in 1:args$nfolds) {
  cat(sprintf("\nFold #%i: Getting data.. ", fold))
  Sigma = Sigma.over.folds(fold, fold.idx = fold.idx, data = data, X.name = "x0", Y.name = "diff", verbose = FALSE)
  out.data = lapply(data, function(d) t(d[fold.idx == fold,]))
  in.data = lapply(data, function(d) t(d[fold.idx != fold,]))
  args.save$fold = fold
  
  # save full dataset
  cat("Saving full dataset.. ")
  fname = sprintf("%s/cvdata%i.rdata", dr, fold)
  save(dmat, Sigma, out.data, in.data, args.save, parms2, parms3, file = fname)
  
  in.data.keep = in.data
  out.data.keep = out.data
  rm(in.data, out.data); gc()
  
  # save dataset after fft scaling
  cat("fft data.. ")
  fname = sprintf("%s/cvdata%i-fft.rdata", dr, fold)
  in.data = data.fft.mean(in.data.keep, Sigma, center = FALSE); gc()
  out.data = data.fft.mean(out.data.keep, Sigma, center = FALSE); gc()
  save(dmat, Sigma, out.data, in.data, args.save, parms2, parms3, file = fname)
  rm(in.data, out.data); gc()
  
  # save dataset after centering and then fft scaling
  cat("center&fft.. ")
  fname = sprintf("%s/cvdata%i-centerfft.rdata", dr, fold)
  in.data = data.fft.mean(in.data.keep, Sigma, center = TRUE); gc()
  out.data = data.fft.mean(out.data.keep, Sigma, center = TRUE); gc()
  save(dmat, Sigma, out.data, in.data, args.save, parms2, parms3, file = fname)
  rm(in.data, out.data); gc()
  
  # memory management
  rm(Sigma, in.data, out.data, in.data.keep, out.data.keep); gc()
}
cat("\n")

