#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("theta", type = "double")
parser$add_argument("-fold", type = "integer", default = 1)
parser$add_argument("-ncores", type = "double", default = 4)
args = parser$parse_args()
# args = parser$parse_args(c("1", "-fold", "0"))
print(args)

# load necessary libraries and files
library(R.matlab)
if(args$ncores > 1) {
  library(RhpcBLASctl)
  omp_set_num_threads(args$ncores)
}
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/lorenz05/calc-linear-fun.r")

# load data
cat("Loading data...\n")
dr = "~/data_assimilation/model_discrepancy/data/thesis/lorenz05/model-error/truth/"
if(args$fold == 0) {
  filename = sprintf("%s/cov.rdata", dr)
  load(filename)
  Sigma = list(XX = cov$x0, XY = t(cov$diff.x0), X.name = "x0", Y.name = "diff")
  gc()

  filename = sprintf("%s/data.rdata", dr)
  load(filename)
  data = data[c("x0", "diff")]
  Sigma$mu = sapply(data, colMeans)
  rm(data); gc()

  # distance matrix
  dmat = dist.on.circle(parms3$N) / parms3$N # divide dmat by N so it matches up with python code
} else {
  filename = sprintf("%s/cvdata%i.rdata", dr, args$fold)
  load(filename)
  rm(in.data); gc()
}

# Calculate L and b
cat("Calculating L and b...\n")
linear = linear.svd(args$theta, Sigma = Sigma)

# Calculate out-of-sample RMSE
if(args$fold != 0) {
  Yhat = linear$b + linear$L %*% out.data[[Sigma$X.name]]
  res = Yhat - out.data[[Sigma$Y.name]]
  linear$rmse = sqrt(mean(res^2))
  print(linear$rmse)
}

# save
cat("Saving...\n")
args.save$theta = args$theta
filename = sprintf("%s/svd%03i%s.rdata", dr,
                   as.integer(args$theta*100),
                   ifelse(args$fold == 0, "", sprintf("-fold%02i", as.integer(args$fold))))
save(linear, args.save, file = filename)

if(args$fold == 0) {
  filename = gsub("rdata", "mat", filename)
  writeMat(filename, L = linear$L, b = linear$b, theta = args$theta)
}



