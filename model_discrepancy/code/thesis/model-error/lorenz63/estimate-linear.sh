#!/bin/bash

for alpha in $(echo 1.1 1.5 `seq 2 9`)
do
    ./generate-data.r ${alpha}
    ./true-estimate-linear.r ${alpha}
done

