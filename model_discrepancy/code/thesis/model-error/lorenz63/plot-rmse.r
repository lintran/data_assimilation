#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(argparse))
parser = ArgumentParser()
# parser$add_argument("savename", type = "character")
# parser$add_argument("-x.facet", type = "character", default = "obs")
# parser$add_argument("-y.facet", type = "character", default = "inf")
# parser$add_argument("-filter", type = "character", default = "")
# parser$add_argument("-inf", type = "character", nargs = "+", default = 100)
# parser$add_argument("-M", type = "integer", nargs = "+", default = 0)
# parser$add_argument("-B", type = "integer", default = 0)
# parser$add_argument("-Nruns", type = "integer", default = 30)
# parser$add_argument("-folder", type = "character", default = "")
# parser$add_argument("-param.min", type = "double", default = 0)
# parser$add_argument("-param.max", type = "double", default = 50)
parser$add_argument("-long.width", action = "store_true")
parser$add_argument("-scale.height", type = "double", default = .35)
parser$add_argument("-scale.width", type = "double", default = 1)
args = parser$parse_args()
# args = parser$parse_args(c("test", "-filter", "etkf", "-inf", "002060", "100000", "150000"))

# if(args$filter == "") args$filter = NA
# if(args$inf == 100) args$inf = NA
# if(args$M == 0) args$M = NA
# if(args$B == 0) args$B = NA

library(grid)
library(ggplot2)
library(scales) # for alpha
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/get-rmse.r")

# Get rmses -----------------------------------------------------------------------------------
run.folder = "~/data_assimilation/model_discrepancy/data/thesis/lorenz63/model-error/"
folder.form = "true-plugin-beta20-alpha([[:digit:]]+)\\-([[:alpha:]]+)\\-dt([[:digit:]]+)\\-([[:digit:]]+).*"
names.form = c("alpha", "type", "dt", "run")
num.form = c(TRUE, FALSE, TRUE, TRUE)

stat.names = c("rmse", "crps")
subset = NULL
# if(!is.na(args$filter)) subset = c(subset, list(filter = args$filter))
# if(!is.na(args$inf)) subset = c(subset, list(infcode = args$inf))
# if(!is.na(args$B)) subset = c(subset, list(B = args$B))
# if(!is.na(args$M)) subset = c(subset, list(M = args$M))

# get rmses
rmse = get.rmse.batch(run.folder, stat.names, folder.form, names.form, subset = subset, num.form = num.form)
rmse$alpha = rmse$alpha/100
summary(rmse)

# make sure each alpha gets the 'right' one merged
rmse.right = subset(rmse, type == "right")
rmse = subset(rmse, type != "right")
rmse.right = merge(rmse.right[,colnames(rmse.right) != "alpha"], unique(rmse[,"alpha",drop = FALSE]), all.x = TRUE)
rmse = rbind(rmse, rmse.right)

# get unique ids
uni = lapply(rmse[,!colnames(rmse) %in% c("p10", "p50", "p90", "run")], unique)
# uni$run = 1:args$Nruns
rmse.all = expand.grid(uni)
rmse.all$model = NA
rmse.all$model[rmse.all$type == "right"] = sprintf("%-20s", "R")
rmse.all$model[rmse.all$type == "wrong"] = sprintf("%-20s", "W")
# rmse.all$model[rmse.all$type == "wrongb"] = sprintf("%-20s", "W + intercept")
rmse.all$model[rmse.all$type == "wrongLb"] = sprintf("%-20s", "W + linear")
rmse.all$stat2 = NA
# rmse.all$stat2[rmse.all$stat == "rmse"] = "RMSE"
rmse.all$stat2[rmse.all$stat == "crps"] = "ES"
rmse.all$day = rmse.all$dt / 24

rmse = merge(rmse, rmse.all, all.x = TRUE)

# # get quantiles
# rmse.summary = aggregate(value ~ ., rmse[,colnames(rmse) != "run"], quantile, c(.1, .5, .9))
# temp = rmse.summary$value
# colnames(temp) = c("lower", "median", "upper")
# rmse.summary = data.frame(rmse.summary[,colnames(rmse.summary) != "value"], temp)

# Plot distribution of rmses --------------------------------------------------------------------
cols = c("darkgreen", "red", "blue")
names(cols) = sprintf("%-20s", c("R", "W", "W + linear"))

p = ggplot(subset(rmse, !is.na(stat2) & alpha %in% c(1.1, 1.5, 2, 3)),
           aes(dt, p50, color = model, fill = model, linetype = model))
p = p + geom_line() #+ geom_point()
# p = p + geom_ribbon(aes(ymin = p10, ymax = p90), alpha = .5)
p = p + facet_grid( ~ alpha, drop = TRUE, scales = "free")
p = p + ylab("ES")
# p = p + xlab("day")
p = p + xlab(expression(paste(Delta, t[n] %*% 10)))
p = p + scale_colour_manual(values = cols) + scale_fill_manual(values = cols)

p2 = p + theme_bw() +
  theme(text = element_text(size = 10),
        # axis.title.y = element_text(angle = 0),
        #axis.text.x = element_text(angle = 90, vjust = .5),
        plot.margin=unit(c(0,0,0,0),"mm"),
        legend.position="bottom",
        legend.key.width=unit(3,"line"),
        legend.title=element_blank())
print(p2)
dev.off()

pdf.dims = c((8.5-3), (11-3)) # width x height
if(args$long.width) pdf.dims = rev(pdf.dims)
pdf.dims[1] = args$scale.width*pdf.dims[1]
pdf.dims[2] = args$scale.height*pdf.dims[2]

dir = "~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/lorenz63/"
dir.create(dir, showWarnings = FALSE, recursive = TRUE)
filename = sprintf("%s/rmse.pdf", dir)
pdf(filename, width = pdf.dims[1], height = pdf.dims[2])
print(p2)
dev.off()

