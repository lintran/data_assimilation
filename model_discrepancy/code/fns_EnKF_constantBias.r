#
# Used algorithm outlined in "Data Assimilation" by Nychka and Anderson,
# published in Gelfand et al., pg. 484
#

require(mvtnorm)
require(spam)

# Update step, analysis distribution

bias.update = function(bias.ensemble, forecast.ensemble, 
                       y, y.perturbed = NULL,
                       H = NULL, Sigma.0, Sigma.0.inv = NULL,
                       method = "ETKF") {
  d = length(drop(y))
  n = ncol(forecast.ensemble)
  if(is.null(H)) H = diag(x = 1, nrow = d, ncol = d)
  
  U.f = t(scale(t(forecast.ensemble), center = TRUE, scale = FALSE))
  mu.f = attr(U.f, "scaled:center")
  Z.f = 1/sqrt(n-1) * U.f # Sigma.f = Z.f %*% t(Z.f) = cov(t(forecast.ensemble))
  H.Zf = H %*% Z.f
  
  U.b = t(scale(t(bias.ensemble), center = TRUE, scale = FALSE))
  mu.b = attr(U.b, "scaled:center")
  Z.b = 1/sqrt(n-1) * U.b # Sigma.b = Z.b %*% t(Z.b) = cov(t(bias.ensemble))
  H.Zb = H %*% Z.b
  
  KG.t = solve(tcrossprod(H.Zf) + tcrossprod(H.Zb) + Sigma.0, tcrossprod(H.Zb, Z.b)) # transpose of Kalman gain = [H Sigma_b H + H Sigma_f H + Sigma_0]^-1 H Sigma_b
  
  # Method 1: perturbed observations
  if(method == "perturbed") {
    if(is.null(y.perturbed)) y.perturbed = t(rmvnorm(n = n, mean = y, sigma = Sigma.0))
    output = sapply(1:n, function(i) bias.ensemble[,i] - crossprod(KG.t, y.perturbed[,i] - H %*% forecast.ensemble[,i]))  # updated bias ensemble, b_a = b_f - KG (y.perturbed - H x_f)
  } else {
    stop("Don't recognize that particular 'method'.")
  }
  
  return(output)
}

data.bias.update = function(bias.ensemble, forecast.ensemble, y, y.perturbed = NULL, dim, args, N, M) {
  # Initialization
  analysis = matrix(nrow = N, ncol = M+2)
  
  # Update: Obtain analysis distribution
  analysis[,1:M] = 
    do.call("bias.update",
            c(list(bias.ensemble = bias.ensemble, forecast.ensemble = forecast.ensemble, y = y, y.perturbed = y.perturbed), args))
  analysis[,M+1] = rowMeans(analysis[,1:M])
  analysis[,M+2] = apply(analysis[,1:M], 1, var)
  
  return(analysis)
}




