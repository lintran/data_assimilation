
dyn.load("~/Desktop/Grad school/Research/data_assimilation/model_discrepancy/code/gaspari_cohn_corr.so")

gaspari.cohn.corr.mat = function(x, c = NULL, l = NULL, nugget = 0) {
  error = (is.null(c) & is.null(l)) | (!is.null(c) & !is.null(l))
  if( error ) stop("Either 'c' or 'l' must be specified. Not both.")
  
  if( is.null(c) ) c = sqrt(10/3) * l
  if( is.null(nugget) ) nugget = 0
  N = max(x)

  output = array(0, dim = rep(N,2))
  out = .Fortran("gaspari_cohn_corr_mat",
               x = as.double(x),
               c = as.double(c),
               N = as.integer(N),
               x_max = as.double(N),
               nugget = as.double(nugget),
               output = as.double(output))
  output = array(out$output, dim = dim(output))
  
  return(output)
}

gaspari.cohn.corr = function(d, c = NULL, l = NULL) {
  error = (is.null(c) & is.null(l)) | (!is.null(c) & !is.null(l))
  if( error ) stop("Either 'c' or 'l' must be specified. Not both.")
  
  if( is.null(c) ) c = sqrt(10/3) * l
  a = d/c
  
  if (d <= c) {
    out = -1/4*a^5 + 1/2*a^4 + 5/8*a^3 - 5/3*a^2 + 1
  } else if (d <= 2*c) {
    out = 1/12*a^5 - 1/2*a^4 + 5/8*a^3 + 5/3*a^2 - 5*a + 4. - 2/3*1/a
  } else {
    out = 0
  }
  
  return(out)
}

circ.dist = function(x1, x2, N) pmin(abs(x1 - x2), abs((N+x1) - x2), abs(x1 - (N+x2)))

