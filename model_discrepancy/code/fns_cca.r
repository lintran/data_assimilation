
library(CCA)

ccalm = function(X, Y) {

  # CCA
  result = cc(X, Y)
  
  # get means
  result$means$X = matrix(colMeans(X, na.rm = TRUE), nrow = 1)
  result$means$Y = matrix(colMeans(Y, na.rm = TRUE), nrow = 1)
  
  # linear regression
  P = length(result$cor)
  result$lm = vector("list", P)
  for(p in 1:P) result$lm[[p]] = lm(v ~ u, data.frame(v = result$scores$yscores[,p], u = result$scores$xscores[,p]))
  
  # V to Y map
  result$VtoY = solve(result$ycoef)
  
  class(result) = "ccalm"
  return(result)
}


predict.ccalm = function(object, newdata) {
  P = length(object$lm)
  Q = ncol(newdata)
  N = nrow(newdata)
  if(Q != nrow(object$xcoef)) stop("Number of columns in 'newdata' does not match up with number of columns in 'X'.")
  
  yscores = array(dim = c(N,P))
  newdata = (newdata - object$means$X[rep(1,N),]) %*% object$xcoef
  for(p in 1:P) yscores[,p] = predict(object$lm[[p]], data.frame(u = newdata[,p]))
  y.pred = yscores %*% object$VtoY + object$means$Y[rep(1,N),]
  
  return(y.pred)
}
