
multiplier.separate = FALSE
rotate = FALSE
K = 100
Nrep = 1
if(!"suffix" %in% objects()) suffix = "inf2"

# Load libraries, functions, and training data ---------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/fns_onlineCCA.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

load(sprintf("model_discrepancy/data/lorenz05/training/%s.rdata", suffix))
if(!multiplier.separate) Y.input$delta.xa = Y.input$delta.xa / diff(times[1:2])

file.header = sprintf("model_discrepancy/data/lorenz05/EnKF_onlineCCA/%s_t%04d_K%03d_sepMult%s_rot%s", 
                      suffix,
                      t.first, K,
                      substr(multiplier.separate,1,1),
                      substr(rotate,1,1))

# BIBFA training ---------------------------------------------------------------
bibfa.opts = c(getDefaultOpts(), K = K, Nrep = Nrep)
bibfa.opts$rotate = rotate

bibfa = GFAexperiment(Y = Y.input, K = bibfa.opts$K, opts = bibfa.opts, Nrep = bibfa.opts$Nrep); for(i in 1:10) alarm()

bibfa$cholW = lapply(bibfa$covW, chol)
bibfa$mean = lapply(Y.input, function(x) attr(x, "scaled:center"))
bibfa$pred = names(Y.input) == "xa" # training data

1/sqrt(bibfa$tau)

if(FALSE) {
  matplot(t(bibfa$alpha), type = "l")
  source("CCAGFA/R/CCAGFAtools.R")
  GFAtrim(bibfa)$K
}

# Save
save.list = c("N", "M", "T", "DARTdir",
              "forecast.length", "t.first", "t.same",
              "bibfa", "bibfa.opts", "multiplier.separate")

data.filename = sprintf("%s_training.rdata", file.header)
save(list = save.list, file = data.filename)

# Sample from the posterior ----------------------------------------------------
parms$bibfa.samp = GFAsample.pred(bibfa$pred, bibfa, nSample = 1000); for(i in 1:10) alarm()
parms$bibfa.samp$params$multiplier = ifelse(multiplier.separate, 1/diff(times[1:2]), 1)

if(multiplier.separate) {
  lorenz05.bias = function(t, y, parms) {
    out = lorenz05(t = t, y = y, parms = parms)
    out[[1]] = 
      out[[1]] + 
      parms$bibfa.samp$params$multiplier *
      GFApred2(list(delta.xa = NULL, xa = matrix(y, nrow = 1)), 
               parms$bibfa.samp$params, 
               parms$bibfa.samp$sam)[[parms$bibfa.samp$params$pr]][1,]
    return(out)
  }
} else {
  lorenz05.bias = function(t, y, parms) {
    out = lorenz05(t = t, y = y, parms = parms)
    out[[1]] = 
      out[[1]] + 
      GFApred2(list(delta.xa = NULL, xa = matrix(y, nrow = 1)), 
               parms$bibfa.samp$params, 
               parms$bibfa.samp$sam)[[parms$bibfa.samp$params$pr]][1,]
    return(out)
  }
}

# Save
save.list = c("N", "M", "T", "DARTdir",
              "forecast.length", "t.first", "t.same",
              "parms", "lorenz05.bias", "init", "multiplier.separate", "rotate")
data.filename = sprintf("%s_postSamp.rdata", file.header)
save(list = save.list, file = data.filename)

# Test -------------------------------------------------------------------------
if(FALSE) {
  load(data.filename)
  
  # prediction with bias
  blah = lorenz05.bias(0, init[,1], parms)
  plot(blah[[1]], type = "l")
  
  # prediction with bias, separate out components
  blah = lorenz05(0, init[,1], parms)
  plot(blah[[1]], type = "l")
  
  error =
  parms$bibfa.samp$params$multiplier *
  GFApred2(list(delta.xa = NULL, xa = matrix(init[,1], nrow = 1)), 
           parms$bibfa.samp$params, 
           parms$bibfa.samp$sam)[[parms$bibfa.samp$params$pr]][1,]
  plot(error, type = "l")
  
  # advance many more steps
  blah = adv.model(y = init[,1], times = times[1:2], func = lorenz05.bias, parms = parms, delta.t = input.parms$time.step, verbose = FALSE)
  plot(blah[2,-1], type = "l")
  
  # prediction for training data
  blah = GFApred2(list(delta.xa = NULL, xa = Y.input$xa), 
           parms$bibfa.samp$params, 
           parms$bibfa.samp$sam)[[parms$bibfa.samp$params$pr]]
  
  plot(Y.input$delta.xa[,1], blah[,1])
  abline(a = 0, b = 1)
  
  plot(Y.input$delta.xa[,1] / diff(times[1:2]), blah[,1] * parms$bibfa.samp$params$multiplier)
  abline(a = 0, b = 1)
}