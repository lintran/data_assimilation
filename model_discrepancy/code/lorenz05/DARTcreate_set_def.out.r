################################################################################
#
# Create "empty notebook" for DART to fill in times and observations
#  https://www.image.ucar.edu/DAReS/DART/DART_Observations.php#obs_seq_overview
#  https://www.image.ucar.edu/DAReS/DART/DART_Observations.php#obs_synthetic
#
# 1) Run "./create_obs_sequence" to create the first state.  Output: set_def.out.
#    "set_def.out" should look like (the numbers before each line is the line
#    number:
#      1 obs_sequence
#      2 obs_kind_definitions
#      3 0
#      4 num_copies:            0  num_qc:            0
#      5 num_obs:            1  max_num_obs:            1
#      6 first:            1  last:            1
#      7 OBS            1
#      8 -1          -1          -1
#      9 obdef
#     10 loc1d
#     11 0.0000000000000000
#     12 kind
#     13 -1
#     14 0          0
#     15 1.0000000000000000     
#
# 2) Run this R file to fill in the other states.
# 3) ./create_fixed_network_sequence
# 4) ./perfect_model_obs
#
################################################################################

# get length of state space from model_mod.nml ---------------------------------
raw.data = readLines("~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/model_mod.nml")
idx = grep("model_size", raw.data)
N = as.numeric(gsub("[[:alpha:][:blank:]_=]{1,}([[:digit:]]{1,}),", "\\1", raw.data[idx]))

# Read in set_def.out ----------------------------------------------------------
raw.data = readLines("~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/set_def.out")
output = raw.data

# Change the number of observations --------------------------------------------

# Make sure the leading zeroes match up
N.txt = formatC(1, format = "d", width = nchar(N))

# Replace
idx = grep("num_obs", raw.data)
output[idx] = gsub(N.txt, N, raw.data[idx])

idx = grep("last", raw.data)
output[idx] = gsub(paste("(*last:[[:blank:]]{1,})", N.txt, sep = ""), paste("\\1", N, sep = ""), raw.data[idx])

# Add observation blocks -------------------------------------------------------
idx = grep("OBS", raw.data)
obs.block = raw.data[idx:length(raw.data)]

output = c(list(output), lapply(1:(N-1), function(x) obs.block))

# Change which observation goes after the first observation
output[[1]][idx+1] = gsub("([[:blank:]]{1,}-1[[:blank:]]{1,})-1*", "\\1 2", output[[1]][idx+1])

for(n in 2:N) {
  # Change observation number
  n.txt = formatC(1, format = "d", width = nchar(n))
  output[[n]][1] = gsub(paste("(*OBS[[:blank:]]{1,})", n.txt, sep = ""), paste("\\1", n, sep = ""), obs.block[1])
  
  # Change which observations go before and after this particular observation
  output[[n]][2] = gsub("([[:blank:]]{1,})-1([[:blank:]]{1,})-1*", paste("\\1", n-1, "\\2", ifelse(n == N, -1, n+1)), obs.block[2])
  
  # Change location of observation
  output[[n]][5] = formatC((n-1)/960, format = "f", width = 19, digits = 16)
  
  # Change H
  n.txt = formatC(-1, format = "d", width = nchar(-n))
  output[[n]][7] = gsub(paste("([[:blank:]]{1,})", n.txt, sep = ""), paste("\\1", -n, sep = ""), obs.block[7])
}

output = unlist(output)
fileCon = file("~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/set_def.out")
writeLines(output, fileCon)
close(fileCon)




