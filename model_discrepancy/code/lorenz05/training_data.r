
suffix = "smoothed"

# Training data ----------------------------------------------------------------
load("model_discrepancy/data/lorenz05/lorenz05.rdata")

# start forecasting at this time point. normally would just be T - forecast.length, but I want to see how the estimates deteoriate with more time.
forecast.length = 7 * 24/6 # 7 days (data comes every 6 hours)
t.first = 5*360*24/6 + 1 # day 1 of the 5th year

# time points that are similar to t.first
t.same = input.parms$T - forecast.length*(floor(input.parms$T/forecast.length):0) + 1
t.same = t.same[t.same < t.first]

# Which timepoints should be used for training data?
#t.use = t.same
#t.use = 2:(t.first-1)
t.use = rev(seq(t.first-1, 100, -1* 24/6))

# Load and obtain training data
load("model_discrepancy/data/lorenz05/EnKF.rdata")

Y.input = list(delta.xa = NULL, xa = t(analysis[,"mean",t.use-1]))
init = analysis[,,t.first-1]

if(suffix == "analysis") {
  Y.input$delta.xa = t(analysis[,"mean",t.use] - forecast[,"mean",t.use])
} else if(suffix == "truth") {
  Y.input$delta.xa = x[t.use,] - t(forecast[,"mean",t.use])
} else if(suffix == "smoothed") {
  forecast.keep = t(forecast[,"mean",t.use])
  load("model_discrepancy/data/lorenz05/EnKS.rdata")
  Y.input$delta.xa = t(smoothed[,"mean",t.use]) - forecast.keep
} else {
  stop("Don't recognize that 'suffix'.")
}
colnames(Y.input$delta.xa) = paste("y", 1:960, sep = "")
colnames(Y.input$xa) = paste("x", 1:960, sep = "")
Y.input = lapply(Y.input, scale, center = TRUE, scale = FALSE)

rm(forecast, analysis); gc()

save.list = c("N", "M", "T", "DARTdir", "Y.input", "init", "t.same", "t.first", "t.use", "forecast.length", "times", "parms")
save(list = save.list, file = sprintf("model_discrepancy/data/lorenz05/training/%s.rdata", suffix))
