################################################################################
#
# Converts DART's ASCII files to obtain observations and the initial conditions
# of the ensemble.  ASCII files of interest:
#   input.nml     : contains Lorenz 05 model parameters
#   obs_seq.out   : contains observations (with measurment error)
#   perfect_ics   : contains the initial conditions
#   True_State.nc : ditto
#   filter_ics    : contains the initial conditions of the ensemble members,
#
################################################################################

DARTdir = "~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/"

# functions and constants ------------------------------------------------------
days.to.sec = 24*60*60
sec.to.days = 1/days.to.sec
DARTtime.regex = "^[[:blank:]]{1,}[[:digit:]]{1,}[[:blank:]]{1,}[[:digit:]]{1,}[[:blank:]]{0,}$"

convert.DARTtime = function(idx, text) {
  temp = lapply(strsplit(text[idx], "[[:blank:]]{1,}"), as.numeric)
  sapply(temp, function(x) x[2]*sec.to.days + x[3])
}

eval.parse = function(text) eval(parse(text = text))

loc.convert = function(loc, N) loc*N+1

extract.number = function(idx, text) {
  # extracts number from raw text. only works for text with one value to be extracted
  as.numeric(gsub("[^[:digit:]\\.-]", "", text[idx]))
}

find.and.extract.number = function(find, text) extract.number(grep(find, text), text)

# get model characteristics: model_mod.nml -------------------------------------
input = readLines(paste(DARTdir, "input.nml", sep = ""))
input.parms = list()

nml.start = grep("^\\&[[:alpha:]_]+", input)
nml.end = c(nml.start[-1]-1, length(input))
names(nml.start) <- names(nml.end) <- input[nml.start]

model.nml = input[(nml.start["&model_nml"]+1):nml.end["&model_nml"]]
perfect.nml = input[(nml.start["&perfect_model_obs_nml"]+1):nml.end["&perfect_model_obs_nml"]]
filter.nml = input[(nml.start["&filter_nml"]+1):nml.end["&filter_nml"]]

# time step
input.parms$time.step = find.and.extract.number("delta_t", model.nml)

# what is the time step equivalent to in real time? in days
temp = find.and.extract.number("time_step_(days|seconds)", model.nml)
input.parms$ts.convert = temp[1] + temp[2]*sec.to.days

# model parameters
input.parms$parms = c(N = find.and.extract.number("model_size", model.nml),
                      K = find.and.extract.number("K", model.nml),
                      I = find.and.extract.number("smooth_steps", model.nml), 
                      F = find.and.extract.number("forcing", model.nml), 
                      b = find.and.extract.number("space_time_scale", model.nml), 
                      c = find.and.extract.number("coupling", model.nml),
                      model.number = find.and.extract.number("model_number", model.nml))
N <- input.parms$parms["N"]

# ensemble size
M <- input.parms$M <- find.and.extract.number("num_output_state_members", filter.nml)

input.parms = c(input.parms, list(days.to.sec = days.to.sec, sec.to.days = sec.to.days))

# get observations: obs_seq.out ------------------------------------------------
raw.data = readLines(paste(DARTdir, "obs_seq.out", sep = ""))

# Get number of variables. Usually observations, truth, quality control.
idx = grep("num_copies", raw.data)
nvar = eval.parse(gsub("[[:blank:]]{1,}num_copies:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}num_qc:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1+\\2", raw.data[idx]))

# Get number of observations
idx = grep("num_obs", raw.data)
nobs = as.numeric(gsub("[[:blank:]]{1,}num_obs:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}max_num_obs:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1", raw.data[idx]))

# Create data matrix
obs = matrix(nrow = nobs, ncol = nvar+6)
colnames(obs) = c("t", "loc.raw", "loc", "H.raw", "H", "var", gsub(" ", "", raw.data[(idx+1):(idx+nvar)]))

# Fill in variables
idx = grep("OBS", raw.data)
if(length(idx) != nobs) stop("Number of 'OBS' do not equal the number of observations as stated by 'num_obs'")
for(i in 1:nvar) obs[,i+6] = as.numeric(gsub(" ", "", raw.data[idx+i]))

# Get locations
idx = grep("loc1d", raw.data)
if(length(idx) != nobs) stop("Number of 'loc1d' do not equal the number of observations as stated by 'num_obs'")
obs[,"loc.raw"] = as.numeric(gsub(" ", "", raw.data[idx+1]))
obs[,"loc"] = loc.convert(obs[,"loc.raw"], N)

# Get time
idx = grep("^kind", raw.data)
if(length(idx) != nobs) stop("Number of 'kind' do not equal the number of observations as stated by 'num_obs'")
obs[,"t"] = convert.DARTtime(idx+2, raw.data)

# Get variance
obs[,"var"] = as.numeric(gsub(" ", "", raw.data[idx+3]))

# Get H
obs[,"H.raw"] = as.numeric(gsub(" ", "", raw.data[idx+1]))
obs[,"H"] = as.numeric(obs[,"H.raw"] < 0)

# get perfect initial conditions: perfect_ics and True_State.nc ----------------
inits = list(time = NULL,
             perfect = NULL,
             perturbed = NULL)

# one source: get it from True_State.nc
library(RNetCDF)
nc = open.nc(paste(DARTdir, "True_State.nc", sep = ""))
truth = read.nc(nc)
close.nc(nc)

# second source: get it from perfect_ics. data is in row-major order.
raw.data = readLines(paste(DARTdir, "perfect_ics", sep = ""))
con = textConnection(gsub("D", "e", raw.data[-1]))
perfect = as.numeric(t(read.fwf(file = con, widths = rep(24,3))))
close(con)

# check if both sources give the same answer
all.equal(truth$state[,1], perfect) # TRUE

# fill in inits$perfect
inits$perfect = perfect

# get time of initial conditions: check &perfect_model_obs_nml > init_time_days
# and init_time_seconds.  If the values are nonnegative, then use those values
# for time of initial conditions, else use the values in perfect_ics.
ics.time = find.and.extract.number("init_time_(days|seconds)", perfect.nml)

if(ics.time[1] < 0) {
  init.time = raw.data[1]
  temp = as.numeric(strsplit(init.time, "[[:blank:]]{1,}")[[1]])
  inits$time = temp[2] + temp[3]*sec.to.days
} else {
  inits$time = ics.time[1] + ics.time[2]*sec.to.days
}

# get initial conditions of each ensemble member: filter_ics -------------------
raw.data = readLines(paste(DARTdir, "filter_ics", sep = ""))
init.time = raw.data[1]

# Get starting index of each ensemble member
start = grep(init.time, raw.data)
end = c(start[-1]-1, length(raw.data))
Mbig = length(start)

# create data
inits$perturbed = matrix(ncol = Mbig, nrow = N)

# get values of ensemble members. data is in row-major order.
file.create("temp.txt")
con = file("temp.txt")
writeLines(gsub("D", "e", raw.data[1:end[Mbig]][-start[1:Mbig]]), con)
close(con)

vals = read.fwf("temp.txt", widths = rep(24,3))
nr = nrow(vals)/Mbig
for(m in 1:Mbig) {
  start = (m-1)*nr+1
  end = m*nr
  inits$perturbed[,m] = as.numeric(t(vals[start:end,]))
}

file.remove("temp.txt")
rm(raw.data); gc()

# save -------------------------------------------------------------------------
save(input.parms, obs, inits, truth, file = "model_discrepancy/data/lorenz05/DARTobsinits.rdata")


