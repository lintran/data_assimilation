
burn.in = 850
thin = 1

t.first = 7301
K = 100
multiplier.separate = FALSE
rotate = FALSE

source("model_discrepancy/code/fns_bibfa_gibbs.r")

# Get files -------------------------------------------------------------------
folder = "model_discrepancy/data/lorenz05/EnKF_onlineCCA"
file.heading = sprintf("t%04d_K%03d_sepMult%s_rot%s_gibbsTraining_Bend", 
                       t.first, K,
                       substr(multiplier.separate,1,1),
                       substr(rotate,1,1))
B.ends = as.numeric(gsub(sprintf("(%s)|(\\.rdata)", file.heading), "", grep(file.heading, dir(folder), value = TRUE)))
B.sizes = diff(c(0,B.ends))

# Combine and thin data ----------------------------------------------------
i.keep = seq(burn.in+1, rev(B.ends)[1], thin)
len = 0

for(b in 1:length(B.ends)) {
  print(B.ends[b])
  
  i.keep.rescaled = i.keep[i.keep > B.ends[b]-B.sizes[b] & i.keep <= B.ends[b]] - B.ends[b] + B.sizes[b]
  len.keep = length(i.keep.rescaled)
  
  if(len.keep > 0) {
    filename = sprintf("%s/%s%05d.rdata", folder, file.heading, B.ends[b])
    load(filename)
    
    if(len == 0) {
      post.params.thinned = list()
      post.params.thinned$W = lapply(consts$D, function(D) array(dim = c(length(i.keep),D,consts$K)))
      post.params.thinned$alpha = array(dim = c(length(i.keep),consts$M,consts$K))
      post.params.thinned$tau = array(dim = c(length(i.keep),consts$M))
      post.params.thinned$y = array(dim = c(length(i.keep),consts$N,consts$K))
    }
    
    for(m in 1:consts$M) post.params.thinned$W[[m]][len+1:len.keep,,] = post.params$W[[m]][i.keep.rescaled,,]
    post.params.thinned$y[len+1:len.keep,,] = post.params$y[i.keep.rescaled,,]
    post.params.thinned$alpha[len+1:len.keep,,] = post.params$alpha[i.keep.rescaled,,]
    post.params.thinned$tau[len+1:len.keep,] = post.params$tau[i.keep.rescaled,]
    
    len = len + len.keep
  }
}

post.params = post.params.thinned
rm(post.params.thinned); gc()

# Trace plots ---------------------------------------------------------
par(mfrow = c(2,3), mar = c(5.1,4.1,1,1))
plot(i.keep, post.params$tau[,1], type = "l", xlab = "", ylab = expression(tau[1]))
plot(i.keep, post.params$alpha[,1,1], type = "l", xlab = "", ylab = expression(alpha[1]^1))
plot(i.keep, post.params$W[[1]][,1,1], type = "l", xlab = "", ylab = expression(W[1,1]^1))
plot(i.keep, post.params$y[,1,1], type = "l", xlab = "", ylab = expression(y[11]))

plot(i.keep, post.params$tau[,2], type = "l", xlab = "", ylab = expression(tau[2]))
plot(i.keep, post.params$alpha[,2,1], type = "l", xlab = "", ylab = expression(alpha[1]^2))
plot(i.keep, post.params$W[[2]][,1,1], type = "l", xlab = "", ylab = expression(W[1,1]^2))
plot(i.keep, post.params$y[,1,2], type = "l", xlab = "", ylab = expression(y[12]))

# Save thinned sample ------------------------------------------------
save.list = c("post.params", "consts", "i.keep")
filename = sprintf("%s/%s_gibbsTraining_thinned.rdata", folder, file.heading)
save(list = save.list, file = filename); for(i in 1:10) alarm()

# Get the posterior sample ready for prediction ------------------------
load("model_discrepancy/data/lorenz05/training_data.rdata")
pred = names(Y.input) == "xa" # training data

parms$post.samp = parmsPostPredSamp(pred = pred, post.params = post.params, consts = consts); for(i in 1:10) alarm()
parms$post.samp$params$multiplier = ifelse(multiplier.separate, 1/diff(times[1:2]), 1)

if(multiplier.separate) {
  lorenz05.bias = function(t, y, parms) {
    out = lorenz05(t = t, y = y, parms = parms)
    out[[1]] = 
      out[[1]] + 
      parms$post.samp$params$multiplier *
      postPredSampler(list(delta.xa = NULL, xa = matrix(y, nrow = 1)), 
                      parms$post.samp$params, 
                      parms$post.samp$sam)[[parms$post.samp$params$pr]][1,]
    return(out)
  }
} else {
  lorenz05.bias = function(t, y, parms) {
    out = lorenz05(t = t, y = y, parms = parms)
    out[[1]] = 
      out[[1]] + 
      postPredSampler(list(delta.xa = NULL, xa = matrix(y, nrow = 1)), 
                      parms$post.samp$params, 
                      parms$post.samp$sam)[[parms$post.samp$params$pr]][1,]
    return(out)
  }
}

source("model_discrepancy/code/lorenz05/lorenz05.r")
blah = adv.model(y = init[,1], times = times[1:2], func = lorenz05.bias, parms = parms, delta.t = 0.001, verbose = FALSE)
plot(blah[2,-1], type = "l")

save.list = c("N", "M", "T", "DARTdir",
              "forecast.length", "t.first", "t.same",
              "parms", "lorenz05.bias", "init", "multiplier.separate", "rotate")

filename = sprintf("%s/%s_gibbsTraining_postSamp.rdata", folder, file.heading)
save(list = save.list, file = filename)

