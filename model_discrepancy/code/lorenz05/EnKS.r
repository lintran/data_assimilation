
t.first = 7201

# Load libraries and functions -------------------------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")
source("model_discrepancy/code/gaspari_cohn_corr.r")

# Initialization ---------------------------------------------------------------
# data and parameters
load("model_discrepancy/data/lorenz05/lorenz05.rdata")
load("model_discrepancy/data/lorenz05/EnKF_DART/inf10.rdata")
parms$Ksq = parms$K^2

# output info
smoothed = array(dim = c(N, M+2, t.first-1))
dimnames(smoothed) = 
  list(state = NULL,
       ensemble = c(sprintf("e%d", 1:M), "mean", "var"),
       time = NULL)
smoothed[,,t.first-1] = analysis[,,t.first-1]

# arguments for smoothing steps
gc.corr.f = gaspari.cohn.corr.mat(1:N, c = 20)
gc.corr.fa = gaspari.cohn.corr.mat(1:N, c = 20)

# args.smoothed = 
#   list(method = "ensemble",
#        taper.Sigma.f = gc.corr.f,
#        taper.Sigma.fa = NULL,
#        #taper.Sigma.fa = gc.corr.fa,
#        error.checking = TRUE)
args.smoothed = 
  list(method = "linearize",
       lin.model = lorenz05.lin,
       parms = parms,
       taper.Sigma.f = gc.corr.f,
       #taper.Sigma.fa = NULL,
       taper.Sigma.fa = gc.corr.fa,
       error.checking = TRUE)

rm(gc.corr.f, gc.corr.fa); gc()

# objects to save
save.list = c("smoothed", "args.smoothed", 
              "t.first", "N", "M", "T", "t")

# Run EnKF using the wrong model, Model 2 --------------------------------------
for(t in seq(t.first-2,1)) {
  print(t)
  
  # Smoother: Obtain smoothed distribution
  smoothed[,,t] = data.smooth(analysis.ensemble = analysis[,1:M,t], 
                              smoothed.ensemble = smoothed[,1:M,t+1],
                              forecast.ensemble = forecast[,1:M,t+1],
                              args = args.smoothed,
                              N = N, M = M, range.tol = 50)
}

save(list = save.list, file = "model_discrepancy/data/lorenz05/EnKS.rdata")

if(FALSE) {
Rprof("Rprof.out")
enks(analysis.ensemble = analysis[,1:M,t], 
     smoothed.ensemble = smoothed[,1:M,t+1],
     forecast.ensemble = forecast[,1:M,t+1],
     lin.model = lorenz05.lin, parms = parms,
     localization = gc.corr)
Rprof(NULL)
summaryRprof("Rprof.out")
  
plot.me = vector("list", 4)
plot.me[[1]] <- Mt <- lorenz05.lin(analysis[,"mean",t], parms)
plot.me[[2]] = Mt %*% cov(t(analysis[,1:M,t]))
plot.me[[3]] = cov(t(forecast[,1:M,t+1]), t(analysis[,1:M,t]))
plot.me[[4]] = var(t(forecast[,1:M,t+1]))

names.plot.me = c("Mt", "Mt.Sigma.a", "Sigma.fa", "Sigma.f")

pdf("model_discrepancy/plots/lorenz05/EnKS.pdf")
for(i in 1:4) {
  maxL = max(abs(plot.me[[i]]))
  image.plot(1:960, 1:960, plot.me[[i]],
             zlim = c(-1,1)*maxL,
             breaks = seq(-maxL, maxL, length.out = 20), 
             col = colorRampPalette(c("red", "white", "blue"))(19),
             xlab = "row", ylab = "col",
             main = names.plot.me[i])
}
dev.off()

n = 1
sd = sqrt(Sigma.0[n,n])
plot.den.assim(t = t, n = n, sd = sd,
               analysis = analysis, forecast = forecast, x = x, y = y, M = M)
lines(density(smoothed[n,1:M,t]), col = "red")

blah = apply(analysis[n,1:M,t:(t.first-1)], 2, quantile, probs = c(.05,.5,.95))
blah2 = apply(smoothed[n,1:M,t:(t.first-1)], 2, quantile, probs = c(.05,.5,.95), na.rm = TRUE)
blah3 = apply(forecast[n,1:M,t:(t.first-1)], 2, quantile, probs = c(.05,.5,.95))
plot(t:(t.first-1), x[t:(t.first-1),n], type = "l", col = "darkgreen", ylim = range(x[t:(t.first-1),n], blah, blah2, na.rm = TRUE))
apply(blah, 1, function(x) lines(t:(t.first-1), x, col = "red"))
apply(blah2, 1, function(x) lines(t:(t.first-1), x, col = "blue"))
apply(blah3, 1, function(x) lines(t:(t.first-1), x, col = "gray"))
}