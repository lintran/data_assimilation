
# Model description ------------------------------------------------------------
#             X_0 ~ MVN(0_N, I_N)
#   X_t | X_{t-1} ~ w_t( X_t-1, parms )
#       Y_t | X_t ~ MVN(X_t, sigma^2 I_N)
# where w_t(cdot, parms) is Lorenz 2005 Model 2.
#

# Load libraries and functions -------------------------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/fns_EnKF_constantBias.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

# Initialization ---------------------------------------------------------------
# data and parameters
load("model_discrepancy/data/lorenz05/lorenz05.rdata")
load("model_discrepancy/data/lorenz05/EnKF.rdata")
rm(analysis); gc()

# output info
bias.analysis <- array(dim = c(N, M+2, T+1))
dimnames(bias.analysis) <-
  list(state = NULL,
       ensemble = c(sprintf("e%d", 1:M), "mean", "var"),
       time = NULL)

bias.analysis[,1:M,1] = rnorm(M*N, mean = 0, sd = 1) # Initialize the particles
bias.analysis[,M+1,1] = rowMeans(bias.analysis[,1:M,1])
bias.analysis[,M+2,1] = apply(bias.analysis[,1:M,1], 1, var)

# arguments for forecast and update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "perturbed")

# objects to save
save.list = c("bias.analysis",
              "args.update",
              "parms", "N", "M", "T", "t")
data.filename = "model_discrepancy/data/lorenz05/EnKF_offlineBias.rdata"

# Run EnKF using the wrong model, Model 2 --------------------------------------
for(t in 2:T) {
  print(t)
  
  # Update: Obtain analysis distribution
  bias.analysis[,,t] = 
    data.bias.update(bias.ensemble = bias.analysis[,1:M,t-1], 
                     forecast.ensemble = forecast[,1:M,t] - bias.analysis[,1:M,t-1], 
                     y = y[t,,drop = FALSE], 
                     args = args.update, N = N, M = M)
  
  if(t %% 500 == 0) save(list = save.list, file = data.filename)
}

if(t %% 500 != 0) save(list = save.list, file = data.filename)


