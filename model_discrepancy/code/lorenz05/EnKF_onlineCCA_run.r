
t.first = 7201
K = 100
multiplier.separate = FALSE
rotate = FALSE
gibbs = FALSE
if(!"suffix" %in% objects()) suffix = "inf2"

# Load libraries and functions -------------------------------------------------
#library(CCAGFA)
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

if(gibbs) {
  source("model_discrepancy/code/fns_bibfa_gibbs.r")
} else {
  source("model_discrepancy/code/fns_onlineCCA.r")
}

# Initialization ---------------------------------------------------------------
# output info
load("model_discrepancy/data/lorenz05/lorenz05.rdata")

file.header = sprintf("model_discrepancy/data/lorenz05/EnKF_onlineCCA/%s_t%04d_K%03d_sepMult%s_rot%s%s", 
          suffix,
          t.first, K,
          substr(multiplier.separate,1,1),
          substr(rotate,1,1),
          ifelse(gibbs, "_gibbsTraining", ""))
data.filename = sprintf("%s_postSamp.rdata", file.header)
load(data.filename)

forecast <- analysis <- array(dim = c(N, M+2, T-t.first+2))
analysis[,,1] = init

# arguments for forecast and update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "ETKF")

# save info
data.filename = sprintf("%s.rdata", file.header)
plot.filename = sprintf("%s.pdf", gsub("data", "plots", file.header, fixed = TRUE))

save.list = c("forecast", "analysis",
              "args.update",
              "N", "M", "T", "t",
              "DARTdir", "plot.filename", "data.filename",
              "forecast.length", "t.first", "t.same",
              "lorenz05.bias", "multiplier.separate", "rotate", "K")

# Ensemble Kalman filter with streaming CCA ------------------------------------
for(t in 2:(T-t.first+2)) {
  t.equiv = t+t.first-2
  print(t.equiv)
  
  # Forecast: Propagate the ensemble members forward
  forecast[,,t] = data.forecast(analysis.ensemble = analysis[,1:M,t-1], 
                                adv.model = adv.model, times = times[1:2], 
                                func = lorenz05.bias, parms = parms,
                                delta.t = input.parms$time.step,
                                inflation.factor = 2,
                                N = N, M = M)
  
  # Update: Obtain analysis distribution
  analysis[,,t] = data.update(forecast.ensemble = forecast[,1:M,t], 
                              y = y[t.equiv,,drop = FALSE], 
                              args = args.update, N = N, M = M)
}

save(list = save.list, file = data.filename)

# Plots -----------------------------------------------------------------------
plot.forecast.ensemble(n = 667, M = M, T = T-t.first+2, 
                       forecast = forecast, x = x[(t.first-1):T,], y = y[(t.first-1):T,], 
                       t.end = 10, t.window.length = 300)

plot.forecast.ensemble(n = 667, M = M, T = T-t.first+2, 
                       forecast = forecast, x = x[(t.first-1):T,], y = y[(t.first-1):T,], 
                       t.end = T-t.first+2, t.window.length = 300)


