
library(deSolve)
library(mvtnorm)
dyn.load("model_discrepancy/code/lorenz05/lorenz05.so")
load("model_discrepancy/data/lorenz05/lorenz05.rdata")
length(objects())

times.high2 = 2*12*30*24/120 + seq(30*24/120, 60*24/120, 1/120)
sol2 = ode(y = sol[nrow(sol),-1], times = times.high2, func = lorenz05.3, parms = parms, 
           method = "ode45", verbose = TRUE)

times2 = 2*12*30*24/120 + seq(30*24/120 + 6/120, 60*24/120, 6/120)
x2 = sol2[sol2[,"time"] %in% times2,-1]
set.seed(12345+1)
y2 = x2 + matrix(rnorm(prod(dim(x2)), sd = sigma), ncol = parms$N) # Observations

sol = rbind(sol, sol2[-1,]); rm(sol2)
times.high = c(times.high, times.high2[-1]); rm(times.high2)
times = c(times, times2); rm(times2)
x = rbind(x, x2); rm(x2)
y = rbind(y, y2); rm(y2)

length(objects())
save.image("model_discrepancy/data/lorenz05/lorenz05.rdata")
