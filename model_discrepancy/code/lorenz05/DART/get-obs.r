################################################################################
#
# Converts DART's ASCII files to obtain observations and the initial conditions
# of the ensemble.  ASCII files of interest:
#   input.nml     : contains Lorenz 05 model parameters
#   obs_seq.out   : contains observations (with measurment error)
#   perfect_ics   : contains the initial conditions
#   True_State.nc : ditto
#   filter_ics    : contains the initial conditions of the ensemble members,
#
################################################################################

DARTdir = "~/data_assimilation/DART/Kodiak/models/lorenz_04/default-identity-other-480/dt12/perfect/"
integral = FALSE

# functions and constants ------------------------------------------------------
days.to.sec = 24*60*60
sec.to.days = 1/days.to.sec
DARTtime.regex = "^[[:blank:]]{0,}([[:digit:]]{1,})[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{0,}$"

convert.DARTtime = function(idx, text) {
  temp = lapply(strsplit(text[idx], "[[:blank:]]{1,}"), as.numeric)
  sapply(temp, function(x) x[2]*sec.to.days + x[3])
}

eval.parse = function(text) eval(parse(text = text))

loc.convert = function(loc, N) loc*N+1

extract.number = function(idx, text) {
  # extracts number from raw text. only works for text with one value to be extracted
  as.numeric(gsub("[^[:digit:]\\.-]", "", text[idx]))
}

find.and.extract.number = function(find, text) extract.number(grep(find, text), text)

# get model characteristics: input.nml ---------------------------------------------
input = readLines(paste(DARTdir, "input.nml", sep = ""))
input.parms = list()

nml.start = grep("^\\&[[:alpha:]_]+", input)
nml.end = c(nml.start[-1]-1, length(input))
names(nml.start) <- names(nml.end) <- input[nml.start]

model.nml = input[(nml.start["&model_nml"]+1):nml.end["&model_nml"]]
filter.nml = input[(nml.start["&filter_nml"]+1):nml.end["&filter_nml"]]

# time step
input.parms$dt = find.and.extract.number("delta_t", model.nml)
temp = find.and.extract.number("time_step_(days|seconds)", model.nml)
input.parms$day = temp[1]
input.parms$seconds = temp[2]

# model parameters
input.parms$parms = c(N = find.and.extract.number("model_size", model.nml),
                      K = find.and.extract.number("K", model.nml),
                      I = find.and.extract.number("smooth_steps", model.nml), 
                      F = find.and.extract.number("forcing", model.nml), 
                      b = find.and.extract.number("space_time_scale", model.nml), 
                      c = find.and.extract.number("coupling", model.nml),
                      model.number = find.and.extract.number("model_number", model.nml))
N <- input.parms$parms["N"]

# ensemble size
M <- input.parms$M <- find.and.extract.number("num_output_state_members", filter.nml)

# get observations: obs_seq.out ------------------------------------------------
raw.data = readLines(paste(DARTdir, "obs_seq.out", sep = ""))

# Get number of variables. Usually observations, truth, quality control.
idx = grep("num_copies", raw.data)
nvar = eval.parse(gsub("[[:blank:]]{1,}num_copies:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}num_qc:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1+\\2", raw.data[idx]))

# Get number of observations
idx = grep("num_obs", raw.data)
nobs = as.numeric(gsub("[[:blank:]]{1,}num_obs:[[:blank:]]{1,}([[:digit:]]{1,})[[:blank:]]{1,}max_num_obs:[[:blank:]]{1,}([[:digit:]]{1,})", "\\1", raw.data[idx]))

# Create data matrix
obs = matrix(nrow = nobs, ncol = nvar+5)
colnames(obs) = c("days", "seconds", "loc.raw", "loc", "var", gsub(" ", "", raw.data[(idx+1):(idx+nvar)]))
obs.idx = 5 + 1:nvar

# Fill in variables
idx = grep("OBS", raw.data)
if(length(idx) != nobs) stop("Number of 'OBS' do not equal the number of observations as stated by 'num_obs'")
for(i in 1:nvar) obs[,obs.idx[i]] = as.numeric(raw.data[idx+i])

# Get locations
idx = grep("loc1d", raw.data)
if(length(idx) != nobs) stop("Number of 'loc1d' do not equal the number of observations as stated by 'num_obs'")
obs[,"loc.raw"] = as.numeric(gsub(" ", "", raw.data[idx+1]))
obs[,"loc"] = loc.convert(obs[,"loc.raw"], N)

# Get time
idx = grep("^kind", raw.data)
if(length(idx) != nobs) stop("Number of 'kind' do not equal the number of observations as stated by 'num_obs'")
if(integral) {
  temp = strsplit(raw.data[idx[1]+2], "[[:blank:]]{1,}")[[1]]
  halfwidth = as.numeric(temp[2])
  num_points = as.numeric(temp[3])
  obs[,"days"] = as.numeric(gsub(DARTtime.regex, "\\2", raw.data[idx+4]))
  obs[,"seconds"] = as.numeric(gsub(DARTtime.regex, "\\1", raw.data[idx+4]))
  obs[,"var"] = as.numeric(raw.data[idx+5])
} else {
  halfwidth = 0
  num_points = 0
  obs[,"days"] = as.numeric(gsub(DARTtime.regex, "\\2", raw.data[idx+2]))
  obs[,"seconds"] = as.numeric(gsub(DARTtime.regex, "\\1", raw.data[idx+2]))
  obs[,"var"] = as.numeric(raw.data[idx+3])
}

# convert obs and truth --------------------------------------------------------
obs = cbind(t = obs[,"seconds"]*sec.to.days + obs[,"days"], obs)
obs = obs[order(obs[,"t"], obs[,"loc.raw"]),]

N = length(unique(obs[,"loc.raw"]))
T = length(unique(obs[,"t"]))
obs.mat = matrix(obs[,"observations"], nrow = N, ncol = T)
truth.mat = matrix(obs[,"truth"], nrow = N, ncol = T)

# save -------------------------------------------------------------------------
library(R.matlab)
save(obs, input.parms, obs.mat, file = paste0(DARTdir, "obs.rdata"))
writeMat(paste0(DARTdir, "obs.mat"), 
         obs = obs.mat,
         truth = truth.mat,
         var = obs[obs[,"t"] == obs[1,"t"],"var"],
         loc = unique(obs[,"loc.raw"]),
         halfwidth = halfwidth,
         num_points = num_points,
         verbose = -1)


