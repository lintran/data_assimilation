################################################################################
#
# Generate assimilation data from the Lorenz 05 model.  Sync up with DART data.
#
################################################################################

library(spam)

source("model_discrepancy/code/lorenz05/lorenz05.r")
load("model_discrepancy/data/lorenz05/DARTobsinits.rdata")

# Calculate the time output, get parameters, get starting values ----------------
times = sort(unique(obs[,"t"])) * input.parms$time.step / input.parms$ts.convert
dt = mean(diff(times))

parms = lorenz05.parms(input.parms$parms)
start = inits$perfect

# The true solution, X_t: Lorenz 2005 Model 3 ----------------------------------

# Integrate the true model
sol = adv.model(y = start, times = times, func = lorenz05, parms = parms, 
                delta.t = input.parms$time.step, verbose = TRUE)
# The above should be equivalent to...
#    ode(y = start, times = times, func = lorenz05, parms = parms,
#        method = "rk4", hini = input.parms$time.step, verbose = TRUE)
#                       or equivalently
#    rk(y = start, times = times, func = lorenz05, parms = parms,
#      method = "rk4", hini = input.parms$time.step, verbose = TRUE)
# but it's not. I'm not sure why.

# Check if matches up with DART
if(!all.equal(sol[,-1], t(truth$state), check.names = FALSE, check.attributes = FALSE)) stop("Solution doesn't match up with DART's!")

plot(truth$state[1,] - sol[,2], type = "l")

# The wrong solution: Lorenz 2005 Model 2 --------------------------------------
parms2 = parms
parms2["model.number"] = 2
parms2["F"] = 10

sol.wrong = 
  adv.model(y = start, times = times, func = lorenz05, parms = parms2, 
            delta.t = input.parms$time.step, verbose = TRUE)

plot(sol[,668] - sol.wrong[,668], type = "l")
plot(sol[1000,-1] - sol.wrong[1000,-1], type = "l")

# Generate the data, Y_t -------------------------------------------------------
x = matrix(obs[,"truth"], byrow = TRUE, ncol = parms$N)
y = matrix(obs[,"observations"], byrow = TRUE, ncol = parms$N)

sigma.sq = obs[obs[,"t"] == obs[,"t"][1], "var"]
Sigma.0 = diag.spam(sigma.sq)
Sigma.0.chol = chol(Sigma.0)
Sigma.0.inv = as.spam(chol2inv(Sigma.0.chol))
sigma = sqrt(sigma.sq)

# Initial conditions of ensemble members ---------------------------------------
ensemble.ics = inits$perturbed[,1:input.parms$M]

# Save -------------------------------------------------------------------------
input.parms$N = parms$N
input.parms$T = length(times)

save(x, y, Sigma.0, Sigma.0.inv, Sigma.0.chol, sigma,
     parms, parms2, times, dt,
     ensemble.ics, input.parms,
     file = "model_discrepancy/data/lorenz05/lorenz05.rdata")

save(sol, sol.wrong,
     file = "model_discrepancy/data/lorenz05/lorenz05_sol.rdata")

