#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-nfolds", type = "double", default = 3)
parser$add_argument("-ncores", type = "double", default = 1)
parser$add_argument("-seed", type = "double", default = 12345)

parser$add_argument("-folder", type = "character", default = "")

args = parser$parse_args()

# load libraries
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

# set up parallelization
library(parallel)
options(mc.cores = min(args$ncores, args$nfolds))

# load data
cat("Loading data...\n")
filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/%s/data-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                   args$folder, 
                   as.integer(args$N), as.integer(args$K), as.integer(args$I), 
                   as.integer(args$b), as.integer(args$c), as.integer(args$F))
load(filename)
data = data[c("x0", "diff")]
gc()

dmat = dist.on.circle(args$N)

# create folds for CV
cat("Create folds...\n")
set.seed(args$seed)
fold.idx = sample(args$nfolds, args.save$runs, replace = TRUE)
table(fold.idx)

# get fold data
cat("Getting data for each fold #")

Sig.folds = mclapply2(1:args$nfolds, Sigma.over.folds, fold.idx = fold.idx, 
                      data = data, X.name = "x0", Y.name = "diff")

out.folds = lapply(1:args$nfolds, function(fold) lapply(data, function(d) d[fold.idx == fold,]))

in.folds = lapply(1:args$nfolds, function(fold) lapply(data, function(d) d[fold.idx != fold,]))

# save fold data
cat("\nSaving data for fold #")
args.save = c(args.save, args[c("nfolds", "seed")], list(fold = 1))
for(fold in 1:args$nfolds) {
  cat(fold, "")
  fname = gsub("data-", paste0("cvdata", fold, "-"), filename)
  Sigma = Sig.folds[[fold]]
  out.data = out.folds[[fold]]
  in.data = in.folds[[fold]]
  args.save$fold = fold
  save(dmat, Sigma, out.data, in.data, args.save, file = fname)
}
cat("\n")

