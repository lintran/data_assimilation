#!/bin/bash
# 1 = cv name
# 2 = first fold
# 3 = last fold
# 4-last = param

#echo "./cv-data.r -${1} ${2}" | qsub -o outfiles/${1}${2}-cvdata.out -e errfiles/${1}${2}-cvdata.err -N ${1}${2}-cvdata
#sleep 15m

header="${*:4}"
header="${header//[ -]/}"

for fold in `seq $2 $3`;
do
    name="${header}-${1}${fold}"
    echo "./calc-linear-${1}-cv.r ${*:4} -fold ${fold}" | qsub -o outfiles/${name}.out -e errfiles/${name}.err -N ${name}
    sleep 1s
done
