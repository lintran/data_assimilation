#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-fold", type = "double", default = 1)
parser$add_argument("-folder", type = "character", default = "")

args = parser$parse_args()
print(args)

# load necessary libraries and files
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

# load data
cat("Loading data...\n")
filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/%s/cvdata%i-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                   args$folder, as.integer(args$fold), 
                   as.integer(args$N), as.integer(args$K), as.integer(args$I), 
                   as.integer(args$b), as.integer(args$c), as.integer(args$F))
load(filename)

# run descent for CV
cat("Running CV...")

lb = rep(0, 2)
ub = rep(args$N/2, 2)
ui = rbind(diag(length(lb)), diag(-1, length(lb)))
ci = c(lb, -ub)

cv.out = 
  constrOptim(ub/10, linear.taper.Sigma,
              control = list(trace = 100, REPORT = 1),
              grad = NULL, ui = ui, ci = ci,
              Sigma = Sigma, dist.mat = dmat, data = out.data, data.out = TRUE)

cat(sprintf2("%.02f", cv.out$par), "\n")
cat(cv.out$value, "\n")

# Calculate L and b
cat("Calculating L and b...\n")
diag.out = linear.taper.Sigma(cv.out$par, Sigma = Sigma, dist.mat = dmat, 
                              data = in.data, data.out = FALSE)

# save
cat("Saving...\n")
filename = gsub("cvdata", "taper-cv", filename)
save(cv.out, diag.out, args.save, file = filename)

