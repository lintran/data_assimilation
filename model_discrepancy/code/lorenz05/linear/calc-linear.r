
library(R.matlab)
source("model_discrepancy/code/image.plot2.r")
source("model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

load("model_discrepancy/data/lorenz05/linear/data-centered.rdata")
load("model_discrepancy/data/lorenz05/linear/cov.rdata")
load("model_discrepancy/data/lorenz05/linear/linear.rdata")
rm(Sig.XX.fold, Sig.YX.fold)
gc()

wXX = wendland(d, 30, 2, 1)
wYX = wendland(d, 15, 2, 1)
Sig.XX = cov$x0 * wXX
Sig.YX = cov$diff.x0 * wYX

image.plot2(cor$x0, center.scale = TRUE)
image.plot2(cor$x0 * wXX, center.scale = TRUE)

image.plot2(cor$diff.x0, center.scale = TRUE)
image.plot2(cor$diff.x0 * wYX, center.scale = TRUE)

image.plot2(rotate.matrix(cor$x0 * wXX, N), center.scale = TRUE)
image.plot2(rotate.matrix(cor$diff.x0 * wYX, N), center.scale = TRUE)

L = t(solve(Sig.XX, t(Sig.YX)))
b = attr(data.centered$diff, "scaled:center") - L %*% attr(data.centered$x0, "scaled:center")
writeMat("model_discrepancy/data/lorenz05/linear/L.mat", L = L, b = b)

pdf("model_discrepancy/plots/lorenz05/linear/L.pdf")
image.plot2(L, center.scale = TRUE)
image.plot2(L[100:300,100:300], center.scale = TRUE)
plot(b, type = "l", xlab = "")
dev.off()

acf(b, 100)
acf(diff(b), 100)

plot(L[,400], type = "l")
matplot(L[,400:401], type = "l")
acf(L[,300], 100)



pred = t(tcrossprod(L, data.centered$x0))
res = pred - data.centered$diff

cov.res = cov(res)

pdf("model_discrepancy/plots/lorenz05/linear/L-var.pdf", width = 7*3)
split.screen(c(1,3))
screen(1)
image.plot2(cov$diff, center.scale = TRUE, main = "total")
screen(2)
image.plot2(cov$diff - cov.res, center.scale = TRUE, z.max = max(abs(cov$diff)), main = "explained")
screen(3)
image.plot2(cov.res, center.scale = TRUE, z.max = max(abs(cov$diff)), main = "unexplained")
close.screen(all.screens = TRUE)

plot(1 - diag(cov.res) / diag(cov$diff), ylab = "% variance explained", type = "l", ylim = c(0, .5))
abline(h = c(0,1))
dev.off()


plot(cov.res[400,], type = "l")

i = 10000
plot(data.centered$diff[i,], type = "l")
points(pred[i,], type = "l", col = "blue")


points(-res[i,], type = "l", col = "red")
acf(res[i,], 100)




load("model_discrepancy/data/lorenz05/linear/data.rdata")
pred2 = tcrossprod(L, data$x0)
pred2 = apply(pred2, 2, function(x) x + b)
pred2 = t(pred2)

res = data$diff - pred


i = 1000
plot(data$dR[i,], type = "l", col = "darkgreen")
points(data$dW[i,], type = "l", col = "red")
points(data$dW[i,] + pred2[i,], type = "l", col = "purple")

