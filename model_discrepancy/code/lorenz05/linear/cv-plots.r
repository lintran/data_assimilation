#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-header", type = "character", default = "taper")
parser$add_argument("-fold1", type = "double", default = 1)
parser$add_argument("-fold2", type = "double", default = 3)

args = parser$parse_args()

# load necessary libraries and files
library(ggplot2)
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")


filename = sprintf("~/data_assimilation/model_discrepancy/plots/lorenz05/linear/%s-N%i-K%i-I%i-b%i-c%i-F%i.pdf", 
                   paste0(args$header, if(args$fold1 >= 10) args$fold1 else ""), 
                   as.integer(args$N), as.integer(args$K), 
                   as.integer(args$I), as.integer(args$b), as.integer(args$c), 
                   as.integer(args$F))

pdf(filename)
for(fold in args$fold1:args$fold2) {
  cat("fold", fold, "\n")
  
  # load data
  filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/%s-cv%i-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                     args$header, fold, as.integer(args$N), as.integer(args$K), as.integer(args$I), as.integer(args$b), as.integer(args$c), as.integer(args$F))
  load(filename)
  
  title = paste("fold", fold, 
                "MAE =", sprintf("%.03f", cv.out$value), "\n",
                "theta =", paste(sprintf2("%.02f", cv.out$par), collapse = " "))
  
  image.plot2(diag.out$L, center.scale = TRUE, main = title)
  image.plot2(diag.out$L[1:floor(args$N/10), 1:floor(args$N/10)], center.scale = TRUE, main = title)
  
  i = as.integer(floor(args$N/2))
  plot.me = data.frame(gridpoint = rep(1:args$N), L = diag.out$L[,i])
  p = ggplot(plot.me, aes(gridpoint, L)) + 
    geom_hline(yintercept = 0, color = "gray") + geom_vline(xintercept = i + args$I*(-1:1), color = "gray") +
    geom_line() + theme_bw() + ylab(sprintf("L[:,%i]", i)) + xlim(i + 100*c(-1,1)) + ggtitle(title)
  print(p)
  
  plot.me = data.frame(gridpoint = rep(1:args$N), L = diag.out$L[i,])
  p = ggplot(plot.me, aes(gridpoint, L)) + 
    geom_hline(yintercept = 0, color = "gray") + geom_vline(xintercept = i + args$I*(-1:1), color = "gray") +
    geom_line() + theme_bw() + ylab(sprintf("L[%i,:]", i)) + xlim(i + 100*c(-1,1)) + ggtitle(title)
  print(p)
  
  plot.me = data.frame(gridpoint = rep(1:args$N), b = diag.out$b)
  p = ggplot(plot.me, aes(gridpoint, b)) + 
    geom_line(color = "darkgray") + stat_smooth(se = FALSE, span = .1, color = "black") + theme_bw() + ylab("b") + ggtitle(title)
  print(p)
  
  plot.me = data.frame(gridpoint = rep(1:args$N), exp.var = diag.out$exp.var*100)
  p = ggplot(plot.me, aes(gridpoint, exp.var)) + 
    geom_line(color = "darkgray") + stat_smooth(se = FALSE, span = .1, color = "black") + theme_bw() + ylab("% explained variance") + ggtitle(title)
  print(p)
}
dev.off()









