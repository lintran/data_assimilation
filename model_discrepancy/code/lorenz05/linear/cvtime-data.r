#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-perc_ho", type = "double", default = 50)

args = parser$parse_args()

# load libraries
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

# load data
cat("Loading data...\n")
filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/data-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                   as.integer(args$N), as.integer(args$K), as.integer(args$I), as.integer(args$b), as.integer(args$c), as.integer(args$F))
load(filename)
data = data[c("x0", "diff")]
gc()

dmat = dist.on.circle(args$N)

# create folds for CV
cat("Creating fold...\n")
n.ho = floor(args.save$run * args$perc_ho/100)
fold.idx = c(rep(1000, args.save$run - n.ho), rep(1, n.ho))
table(fold.idx)

# get fold data
cat("Getting data for fold...\n")

Sigma = Sigma.over.folds(1, fold.idx = fold.idx, data = data, X.name = "x0", Y.name = "diff")
out.data = lapply(data, function(d) d[fold.idx == 1,])
in.data = lapply(data, function(d) d[fold.idx != 1,])

# save fold data
cat("Saving data for each fold... \n")
args.save = c(args.save, args[c("perc_ho")])
fname = gsub("data-", paste0("cvdata", args$perc_ho, "-"), filename)
save(dmat, Sigma, out.data, in.data, args.save, file = fname)

