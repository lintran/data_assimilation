
library(spam)
source("~/data_assimilation/model_discrepancy/code/lorenz05/lorenz05.r")
source("~/data_assimilation/model_discrepancy/code/image.plot2.r")

wendland = function(dist, cutoff, q, dim = 1) {
  if(any(dist<0)) stop("'dist' must be nonnegative")
  if(cutoff<0) stop("'cutoff' must be nonnegative.")
  if(cutoff == 0) cutoff = .Machine$double.eps
    
  dist = dist/cutoff
  j = floor(dim/2) + q + 1
  a = fplus(1-dist)**(j+q)
  
  if(q == 0) {
    num = 1
  } else if(q == 1) {
    num = (j+1) * dist + 1
  } else if(q == 2) {
    num = (j**2 + 4*j + 3) * dist**2 + (3*j + 6) * dist + 3
  } else if(q == 3) {
    num = (j**3 + 9*j**2 + 23*j + 15) * dist**3 + (6*j**2 + 36*j + 45) * dist**2 + (15*j + 45) * dist + 15
  } else stop("'q' is not between 0 and 3, inclusive.")
  
  den = 1
  if(q == 2) den = 3
  if(q == 3) den = 15
  
  as.spam(a * num / den)
}

fplus = function(a) a*(a>0)

wendland.cos = function(dist, period.frac, cutoff, q, dim = 1) {
  wendland(dist, cutoff, q, dim) * cos(dist * 2*pi*period.frac)
}

rotate.matrix = function(mat, N) {
  blah = sapply(1:N, function(i) wrap(seq(i,N+i-1)+N/2, N))
  idx.rotate = cbind(rep(1:N, each = N), as.numeric(blah))
  matrix(mat[idx.rotate], nrow = N, byrow = TRUE)
}

dist.on.circle = function(N) {
  out = as.matrix(dist(1:N))
  out[out > N/2] = N - out[out > N/2]
  out
}

Sigma.over.folds = function(fold, fold.idx, data, X.name, Y.name, verbose = TRUE) {
  if(verbose) cat(fold, "")
  in.idx = fold.idx != fold
  out = 
    list(X.name = X.name, Y.name = Y.name, 
         in.idx = which(in.idx), out.idx = which(!in.idx),
         XX = cov.fast(data[[X.name]][in.idx,]),
         XY = cov.fast(data[[X.name]][in.idx,], data[[Y.name]][in.idx,]),
         YY = cov.fast(data[[Y.name]][in.idx,]))
  out$mu = sapply(data[c(X.name, Y.name)], colMeans)
  out
}

mclapply2 = function(...) {
  if(getOption("mc.cores") == 1) {
    lapply(...)
  } else {
    mclapply(...)
  }
}

cov.fast = function(x, y = NULL, cor = FALSE, center = TRUE) {
  n = nrow(x)
  x = scale(x, center = center, scale = cor)
  
  if(is.null(y)) {
    return(1/(n-1) * crossprod(x))
  } else {
    y = scale(y, center = center, scale = cor)
    return(1/(n-1) * crossprod(x, y))
  }
}

cv.fun = function(Sigma, dist.mat, data, data.out, b = NA, bad.val = Inf, 
                  return.Yhat = FALSE, return.cov.res = FALSE, return.res = FALSE) {
  
  err = try(L <- t(solve(Sigma$XX, Sigma$XY)), silent = TRUE)
  if(class(err) != "try-error") {
    if(is.na(b)) b = drop(Sigma$mu[,Sigma$Y.name] - L %*% Sigma$mu[,Sigma$X.name])
    Yhat = b + tcrossprod(L, data[[Sigma$X.name]])
    Yhat = t(Yhat)
    res = Yhat - data[[Sigma$Y.name]]
  }
  
  if(data.out) { # just return mean out-of-sample prediction error
    out = if(class(err) != "try-error") mean(abs(res)) else bad.val
    
  } else { # return diagnostics
    out = list(L = L, b = b)
    if(return.Yhat) out$Yhat = Yhat
    if(return.res) out$res = res
    
    if(return.cov.res) {
      out$cov.res = cov.fast(res)
      res.var = diag(out$cov.res)
    } else {
      res.var = apply(res, 2, var)
    }
    
    out$unexp.var = res.var / diag(Sigma$YY)
    out$exp.var = 1 - out$unexp.var
  }
  
  return(out)
}

linear.taper.Sigma = function(theta, Sigma, dist.mat, data, data.out, ...) {
  names(theta) = c("kXX", "kXY")
  Sigma$XX = Sigma$XX * wendland(dist.mat, theta["kXX"], 2, 1)
  Sigma$XY = Sigma$XY * wendland(dist.mat, theta["kXY"], 2, 1)
  out = cv.fun(Sigma, dist.mat, data, data.out, ...)
  if(!data.out) out = c(out, list(theta = theta))
  out
}

linear.param = function(theta, Sigma, dist.mat, data, data.out, b.const = FALSE, ...) {
  nm = c("beta", "kXY", "pXY", "kXX", "pXX")
  if(b.const) nm = c("b", nm)
  names(theta) = nm
  
  b = if(b.const) theta["b"] else NA
  Sigma$XX = wendland.cos(dist.mat, theta["pXX"], theta["kXX"], 2, 1)
  Sigma$XY = theta["beta"] * wendland.cos(dist.mat, theta["pXY"], theta["kXY"], 2, 1)
  out = cv.fun(Sigma, dist.mat, data, data.out, b = b, ...)
  if(!data.out) out = c(out, list(theta = theta))
  out
}

sprintf2 = function(fmt, x) sapply(x, function(i) sprintf(fmt, i))

bd.to.lc = function(lower, upper) {
  # returns constraint matrix given lower and upper values, where ui %*% theta - ci >= 0
  ui = rbind(diag(1, length(lower)), diag(-1, length(upper)))
  ci = c(lower, -upper)
  list(ui = ui, ci = ci)
}

lcbind = function(a, b) {
  # binds constraint matrix
  ui = rbind(cbind(a$ui, array(0, c(nrow(a$ui), ncol(b$ui)))),
             cbind(array(0, c(nrow(b$ui), ncol(a$ui))), b$ui))
  ci = c(a$ci, b$ci)
  list(ui = ui, ci = ci)
}



