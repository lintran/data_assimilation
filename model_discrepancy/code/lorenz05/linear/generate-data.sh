#!/bin/bash
# 1-last = param

header="${*:1}"
header="${header//[ -]/}"
echo "./generate-data.r ${*:1}" | qsub -o outfiles/${header}-gendata.out -e errfiles/${header}-gendata.err -N ${header}-gendata
