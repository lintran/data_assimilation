#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-fold", type = "double", default = 1)
parser$add_argument("-folder", type = "character", default = "")
parser$add_argument("-bconst", action = "store_true", default = FALSE)

args = parser$parse_args()
print(args)

# load necessary libraries and files
source("~/data_assimilation/model_discrepancy/code/lorenz05/linear/calc-linear-fun.r")

# load data
cat("Loading data...\n")
filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/%s/cvdata%i-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                   args$folder, as.integer(args$fold), 
                   as.integer(args$N), as.integer(args$K), as.integer(args$I), 
                   as.integer(args$b), as.integer(args$c), as.integer(args$F))
load(filename)

# set up initialization
cat("Setting up initialization...\n")

load(gsub("cvdata", "taper-cv", filename))
theta = list()

optim.cor = function(theta, data, dist.mat) {
  pred = wendland.cos(dist.mat, theta[1], 2, theta[2])
  mean(abs(as.matrix(pred - data)))
}

beta = function(beta, Sigma, dist.mat, data) {
  Sigma$XY = beta * Sigma$XY
  cv.fun(Sigma, dist.mat, data, data.out = TRUE)
}

# initialize covariance parameters
lc = bd.to.lc(rep(0,2), rep(args$N/2,2))
lc$ui[4,] = c(1,-1)
lc$ci[4] = 0

cat("Initializing XX parameters...\n")
data = cov2cor(Sigma$XX) * wendland(dmat, cv.out$par[1], 2)
#data = cov2cor(Sigma$XX)
theta$XX = 
  constrOptim(c(cv.out$par[1], 1), optim.cor,
  #constrOptim(c(ub[1]/10, 1), optim.cor,
              #control = list(trace = 100, REPORT = 100),
              grad = NULL, ui = lc$ui, ci = lc$ci,
              data = data, dist.mat = dmat)$par

cat("Initializing XY parameters...\n")
data = cov.fast(in.data[[Sigma$X.name]], in.data[[Sigma$Y.name]], cor = TRUE) * wendland(dmat, cv.out$par[2], 2)
#data = cov.fast(in.data[[Sigma$X.name]], in.data[[Sigma$Y.name]], cor = TRUE)
theta$XY = 
  constrOptim(c(cv.out$par[2], 1), optim.cor,
  #constrOptim(c(ub[1]/10, 1), optim.cor,
              #control = list(trace = 100, REPORT = 100),
              grad = NULL, ui = lc$ui, ci = lc$ci,
              data = data, dist.mat = dmat)$par

# initialize beta
cat("Initializing beta...\n")
Sigma$XX = wendland.cos(dmat, theta$XX[1], 2, theta$XX[2])
Sigma$XY = wendland.cos(dmat, theta$XY[1], 2, theta$XY[2])
ratio = round(max(abs(in.data$diff)) / max(abs(in.data$x0)), -1)
theta$beta = optimize(beta, lower = -ratio, upper = ratio,
                      Sigma = Sigma, dist.mat = dmat, data = out.data)$minimum

# check initial parameters
theta0 = c(theta$beta, theta$XX, theta$XY)
if(args$bconst) theta0 = c(0, theta0)

linear.param(theta0, Sigma = Sigma, dist.mat = dmat, 
             data = out.data, data.out = TRUE, b.const = args$bconst)

# run descent for CV
cat("Running CV...\n")

lc2 = lcbind(bd.to.lc(-ratio, ratio), lcbind(lc, lc))
if(args$bconst) lc2 = lcbind(bd.to.lc(-ratio, ratio), lc2)

system.time(
cv.out <-
  constrOptim(theta0, linear.param,
              control = list(trace = 100, REPORT = 1),
              grad = NULL, ui = lc2$ui, ci = lc2$ci,
              Sigma = Sigma, dist.mat = dmat, 
              data = out.data, data.out = TRUE, 
              b.const = args$bconst)
)

cat(sprintf2("%.02f", cv.out$par), "\n")
cat(cv.out$value, "\n")

# Calculate L and b
cat("Calculating L and b...\n")
diag.out = linear.param(cv.out$par, Sigma = Sigma, dist.mat = dmat, 
                        data = in.data, data.out = FALSE, 
                        b.const = args$bconst)

# save
cat("Saving...\n")
args.save = c(args.save, args[c("bconst")])
filename = gsub("cvdata", ifelse(args$bconst, "param-bconst-cv", "param-cv"), filename)
save(cv.out, diag.out, args.save, file = filename)
