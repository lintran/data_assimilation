#!/usr/bin/env Rscript

.libPaths("~/R/x86_64-pc-linux-gnu-library/3.2")

library("argparse")
parser = ArgumentParser()

parser$add_argument("-I", type = "double", default = 12)
parser$add_argument("-b", type = "double", default = 10)
parser$add_argument("-c", type = "double", default = 3)

parser$add_argument("-K", type = "double", default = 32)
parser$add_argument("-F", type = "double", default = 15)
parser$add_argument("-N", type = "double", default = 960)

parser$add_argument("-header", type = "character", default = "taper")
parser$add_argument("-fold", type = "double", default = 1)

args = parser$parse_args()

# load necessary libraries and files
library(R.matlab)

filename = sprintf("~/data_assimilation/model_discrepancy/data/lorenz05/linear/%s-cv%i-N%i-K%i-I%i-b%i-c%i-F%i.rdata", 
                   args$header, args$fold, 
                   as.integer(args$N), as.integer(args$K), 
                   as.integer(args$I), as.integer(args$b), as.integer(args$c), 
                   as.integer(args$F))
load(filename)

filename = gsub(".rdata", ".mat", filename)
writeMat(filename, L = diag.out$L, b = diag.out$b)




