
# Load functions and data ------------------------------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")

if(any(!c("data.filename", "plot.filename") %in% objects())) {
  data.filename = "model_discrepancy/data/lorenz05/EnKF.rdata"
  plot.filename = "model_discrepancy/plots/lorenz05/EnKF_snapshots.pdf"
}

load("model_discrepancy/data/lorenz05/lorenz05.rdata")
load(data.filename)

# Plots ------------------------------------------------------------------------
n = 667 # gridpoint that we're keeping track of in the plots
sd = sqrt(Sigma.0[n,n])

Sys.sleep(1); pdf(plot.filename, width = 11, height = 8.5)
for(t in 1:t) {
  print(t)
  par(mfrow = c(2,1))
  
  # ...of the trajectory
  plot.forecast.ensemble(n = n, forecast = forecast, x = x, y = y, 
                         t.end = t, t.window.length = 300)
  
  # ...of the distributions
  plot.den.assim(t = t, n = n, sd = sd,
                 analysis = analysis, forecast = forecast, x = x, y = y)
}
dev.off()
