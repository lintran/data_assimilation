
# Set parameters -------------------------------------------------------
B.start = 1
B.end = 1000
thin = 1

K = 100
multiplier.separate = FALSE
rotate = FALSE

# Load libraries and functions -----------------------------------------
source("model_discrepancy/code/fns_bibfa_gibbs.r")

# load training data ---------------------------------------------------
load("model_discrepancy/data/lorenz05/training_data.rdata")
if(!multiplier.separate) Y.input$delta.xa = Y.input$delta.xa / diff(times[1:2])

# initialize -----------------------------------------------------------
file.header = sprintf("model_discrepancy/data/lorenz05/EnKF_onlineCCA/t%04d_K%03d_sepMult%s_rot%s_gibbsTraining_Bend", 
                      t.first, K,
                      substr(multiplier.separate,1,1),
                      substr(rotate,1,1))

if(B.start > 1) { # Load the previous values

  B.start.save = B.start
  B.end.save = B.end
  thin.save = thin
  
  filename = sprintf("%s%05d.rdata", file.header, B.start-1)
  load(filename)
  
  B.start = B.start.save
  B.end = B.end.save
  thin = thin.save
  
} else { # Initialize everything anew
  
  multiplier.separate.save = multiplier.separate
  data.filename = 
    sprintf("model_discrepancy/data/lorenz05/EnKF_onlineCCA/t%04d_K%03d_sepMultT_rot%s_training.rdata", 
            t.first, K, substr(rotate,1,1))
  load(data.filename)
  multiplier.separate = multiplier.separate.save
  
  # prior
  prior = list()
  prior$tau = list(alpha = 1e-14, beta = 1e-14)
  prior$alpha = list(alpha = 1e-14, beta = 1e-14)
  
  # parameters that will be used constantly
  consts = list()
  consts$K = bibfa$K
  consts$D = bibfa$D
  consts$M = 2
  consts$N = nrow(Y.input[[1]])
  consts$XX = sapply(Y.input, function(x) sum(x^2))
  consts$post.alpha$tau = prior$tau$alpha + 0.5 * consts$N * consts$D
  consts$post.alpha$alpha = prior$alpha$alpha + 0.5 * consts$D
  consts$mean = lapply(Y.input, function(x) attr(x, "scaled:center"))
  
  # initialization of parameters
  old = list()
  for(m in 1:consts$M) old$W[[m]] = bibfa$W[[m]]
  old$y = bibfa$Z
  old$tau = bibfa$tau
  old$alpha = bibfa$alpha
  
  if(!multiplier.separate) {
    old$W[[1]] = old$W[[1]] / diff(times[1:2])
    old$tau[[1]] = old$tau[[1]] * diff(times[1:2])^2
  }
  
  inits = old
}

# posterior sample of parameters
B.size = B.end-B.start+1
post.params = list()
post.params$W = lapply(consts$D, function(D) array(dim = c(B.size,D,consts$K)))
post.params$alpha = array(dim = c(B.size,consts$M,consts$K))
post.params$tau = array(dim = c(B.size,consts$M))
post.params$y = array(dim = c(B.size,consts$N,consts$K))

# save info
save.list = c("post.params", "consts", "old", "prior", "B.size", "B.start", "B.end", "thin")
if(B.start == 1) save.list = c(save.list, "inits")
filename = sprintf("%s%05d.rdata", file.header, B.end)

# Gibbs sampler ----------------------------------------------------------------
for(b in 1:(B.size*thin)) {
  cat("[", b, "] Sampling ")
  
  cat("W ")
  for(m in 1:consts$M) old$W[[m]] = sampleW(m = m, old = old, data = Y.input, consts = consts)
  
  cat("y ")
  old$y = sampleY(old = old, data = Y.input, consts = consts)
  
  cat("tau ")
  for(m in 1:consts$M) old$tau[m] = sampleTau(m = m, old = old, data = Y.input, consts = consts, prior = prior)
  
  cat("alpha ")
  for(m in 1:consts$M) old$alpha[m,] = sampleAlpha(m = m, old = old, prior = prior)
  
  if(b %% thin == 0) {
    for(m in 1:consts$M) post.params$W[[m]][b/thin,,] = old$W[[m]]
    post.params$y[b/thin,,] = old$y
    post.params$tau[b/thin,] = old$tau
    post.params$alpha[b/thin,,] = old$alpha
  }
  
  if(b %% 500 == 0) {
    cat("saving ")
    save(list = save.list, file = filename)
  }
  
  cat("\n")
}

if(b %% 500 != 0) save(list = save.list, file = filename); for(i in 1:10) alarm()

# Plots ------------------------------------------------------------------------
par(mfrow = c(2,4), mar = rep(1,4))

if(B.start == 1) {
  
  plot(0:B.end, c(inits$tau[1], post.params$tau[,1]), type = "l", xlab = "", ylab = expression(tau[1]))
  plot(0:B.end, c(inits$alpha[1,1], post.params$alpha[,1,1]), type = "l", xlab = "", ylab = expression(alpha[1]^1))
  plot(0:B.end, c(inits$W[[1]][1,1], post.params$W[[1]][,1,1]), type = "l", xlab = "", ylab = expression(W[1,1]^1))
  plot(0:B.end, c(inits$y[1,1], post.params$y[,1,1]), type = "l", xlab = "", ylab = expression(y[11]))
  
  plot(0:B.end, c(inits$tau[2], post.params$tau[,2]), type = "l", xlab = "", ylab = expression(tau[2]))
  plot(0:B.end, c(inits$alpha[2,1], post.params$alpha[,2,1]), type = "l", xlab = "", ylab = expression(alpha[1]^2))
  plot(0:B.end, c(inits$W[[2]][1,1], post.params$W[[2]][,1,1]), type = "l", xlab = "", ylab = expression(W[1,1]^2))
  plot(0:B.end, c(inits$y[1,2], post.params$y[,1,2]), type = "l", xlab = "", ylab = expression(y[12]))

} else {
  
  plot(B.start:B.end, post.params$tau[,1], type = "l", xlab = "", ylab = expression(tau[1]))
  plot(B.start:B.end, post.params$alpha[,1,1], type = "l", xlab = "", ylab = expression(alpha[1]^1))
  plot(B.start:B.end, post.params$W[[1]][,1,1], type = "l", xlab = "", ylab = expression(W[1,1]^1))
  plot(B.start:B.end, post.params$y[,1,1], type = "l", xlab = "", ylab = expression(y[11]))
  
  plot(B.start:B.end, post.params$tau[,2], type = "l", xlab = "", ylab = expression(tau[2]))
  plot(B.start:B.end, post.params$alpha[,2,1], type = "l", xlab = "", ylab = expression(alpha[1]^2))
  plot(B.start:B.end, post.params$W[[2]][,1,1], type = "l", xlab = "", ylab = expression(W[1,1]^2))
  plot(B.start:B.end, post.params$y[,1,2], type = "l", xlab = "", ylab = expression(y[12]))

}


