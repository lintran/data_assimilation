
library(ggplot2)
library(animation)
ani.options(interval = .1, outdir = paste(getwd(), "model_discrepancy/plots/lorenz05/", sep = "/"))
library(scatterplot3d)

load("model_discrepancy/data/lorenz05/lorenz05.rdata")


# Plots similar to the paper ---------------------------------------------------
t.plot = seq(1, 1000, length.out = 10)

plot.me = t(x[t.plot,] + 20*array(rep(rev(1:length(t.plot)), ncol(x)), dim = c(length(t.plot), ncol(x))))
matplot(plot.me, type = "l", col = "black", lty = "solid", xaxs = "i", ylab = "")

x.wrong = sol.wrong[sol.wrong[,"time"] %in% times,-1]
plot.me = t(x.wrong[t.plot,] + 10*array(rep(rev(1:length(t.plot)), ncol(x.wrong)), dim = c(length(t.plot), ncol(x.wrong))))
matplot(plot.me, type = "l", col = "black", lty = "solid", xaxs = "i", ylab = "")

# 2d animated plot -------------------------------------------------------------
t.plot = times.high >= 2*12*30*24/120
model.highres = list(NULL, sol.wrong[t.plot, -1], sol[t.plot, -1])

for(model.no in 3:2) {
  saveGIF(
    for(t in 1:nrow(model.highres[[model.no]])) {
      print(t)
      plot = ggplot(data.frame(gridpoint = 1:parms$N, val = model.highres[[model.no]][t,]), aes(x = gridpoint, y = val)) + 
        geom_line(aes(color = val)) +
        ylim(range(model.highres[[model.no]])) + theme(legend.position = "none") + ylab("")
      print(plot)
    },
    movie.name = paste("model", model.no, "_2d.gif", sep = ""))
}

# 3d animated plot -------------------------------------------------------------
# http://stackoverflow.com/questions/13495502/plotting-lines-between-two-points-3d-in-r-project
t.plot = times.high >= 2*12*30*24/120
model.highres = list(NULL, sol.wrong[t.plot, -1], sol[t.plot, -1])

nbcol = 150
col = colorRampPalette(c("cyan", "darkmagenta"))(nbcol)
zrange = range(unlist(model.highres))
int = seq(zrange[1], zrange[2], length.out = nbcol + 1)
zcol = findInterval(model.highres[[model.no]][t,], int, rightmost.closed = TRUE)

for(model.no in 3:2) {
  #saveGIF(
  png(filename = paste(getwd(), "/model_discrepancy/plots/lorenz05/model", model.no, "_3d/", "image%d.png", sep = ""))
    for(t in 1:nrow(model.highres[[model.no]])) {
      print(t)
      
      s = scatterplot3d(x = circle$x, y = circle$y, z = model.highres[[model.no]][t,], 
                        type = "n", #color = col[zcol], pch = ".", cex.symbols = .5,
                        zlim = range(model.highres[[model.no]]), 
                        xlab = "", ylab = "", zlab = "", x.ticklabs = "", y.ticklabs = "", z.ticklabs = "", 
                        scale.y = 1, angle = 60, grid = FALSE, lwd = 2, col.axis = "gray", mar = rep(.1,4))
      
      for(gp in 1:(parms$N-1)) {
        z.mean = mean(model.highres[[model.no]][t,gp:(gp+1)])
        p2 = s$xyz.convert(circle$x[gp], circle$y[gp], model.highres[[model.no]][t,gp])
        p3 = s$xyz.convert(circle$x[gp+1], circle$y[gp+1], model.highres[[model.no]][t,gp+1])
        segments(p2$x, p2$y, p3$x, p3$y, col = col[findInterval(z.mean, int, rightmost.closed = TRUE)], lwd = 3.5)
      }
      
      z.mean = mean(model.highres[[model.no]][t,c(1,parms$N)])
      p2 = s$xyz.convert(circle$x[parms$N], circle$y[parms$N], model.highres[[model.no]][t,parms$N])
      p3 = s$xyz.convert(circle$x[1], circle$y[1], model.highres[[model.no]][t,1])
      segments(p2$x, p2$y, p3$x, p3$y, col = col[findInterval(z.mean, int, rightmost.closed = TRUE)], lwd = 3)
    }#,
    #movie.name = paste("model", model.no, "_3d.gif", sep = ""))
  dev.off()
}



