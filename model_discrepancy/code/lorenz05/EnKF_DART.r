
# Model description ------------------------------------------------------------
#             X_0 ~ MVN(0_N, I_N)
#   X_t | X_{t-1} ~ w_t( X_t-1, parms )
#       Y_t | X_t ~ MVN(X_t, sigma^2 I_N)
# where w_t(cdot, parms) is Lorenz 2005 Model 2.
#

# Load libraries and functions -------------------------------------------------
library(RNetCDF)
library(abind)
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")
load("model_discrepancy/data/lorenz05/lorenz05.rdata")

parms["model.number"] = 2
parms["F"] = 10
N = parms$N; M = input.parms$M; T = input.parms$T

if(!"DARTdir" %in% objects()) {
  folder = ""
  
  DARTdir = paste("~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/", folder, "/", sep = "")
  plot.filename = paste("model_discrepancy/plots/lorenz05/EnKF_DART/", folder, "_snapshots.pdf", sep = "")
  save.filename = paste("model_discrepancy/data/lorenz05/EnKF_DART/", folder, ".rdata", sep = "")
  
  plot.inflation = FALSE
}

# Convert NetCDF to R ----------------------------------------------------------
nc = open.nc(paste(DARTdir, "Prior_Diag.nc", sep = ""))
prior = read.nc(nc)
close.nc(nc)

nc = open.nc(paste(DARTdir, "Posterior_Diag.nc", sep = ""))
post = read.nc(nc)
close.nc(nc)

rm(nc); gc()

ensemble.idx = grep("ensemble member", prior$CopyMetaData)
mean.idx = grep("ensemble mean", prior$CopyMetaData)
var.idx = grep("ensemble spread", prior$CopyMetaData)
inflation.idx = grep("inflation", prior$CopyMetaData)

T = min(length(prior$time), length(post$time))
M = dim(prior$state)[2] - 4

analysis = post$state[,c(ensemble.idx, mean.idx, var.idx), 1:T]
analysis[,M+2,] = analysis[,M+2,]^2

rm(post); gc()

forecast = prior$state[,c(ensemble.idx, mean.idx, var.idx), 1:T]
forecast[,M+2,] = forecast[,M+2,]^2

inflation = prior$state[,inflation.idx,1:T]
inflation[,2,] = inflation[,2,]^2

rm(prior); gc()

dimnames(forecast) <- dimnames(analysis) <-
  list(state = NULL,
       ensemble = c(sprintf("e%d", 1:M), "mean", "var"),
       time = NULL)
dimnames(inflation) = 
  list(value = NULL,
       stat = c("mean", "var"),
       time = NULL)

# Save -------------------------------------------------------------------------
save.list = c("forecast", "analysis", "inflation",
              "parms", "N", "M", "T", "t",
              "folder", "DARTdir", "plot.filename", "save.filename")

if(folder != "") save(list = save.list, file = save.filename)

# Plot -------------------------------------------------------------------------
n = 667 # gridpoint that we're keeping track of in the plots
upper = inflation[n,"mean",] + sqrt(inflation[n,"var",])*qnorm(.975)
lower = pmax(0, inflation[n,"mean",] - sqrt(inflation[n,"var",])*qnorm(.975))
sd = sqrt(Sigma.0[n,n])

Sys.sleep(1); pdf(plot.filename, width = ifelse(plot.inflation, 8.5, 11), height = ifelse(plot.inflation, 11, 8.5))
for(t in 1:T) {
  if(t %% 100 == 0) cat(folder, t, "\n")
  
  # Plots...
  par(mfrow = c(ifelse(plot.inflation,3,2), 1), mar = c(2, 2, 0, 0))
  
  # ...of the trajectory
  temp = plot.forecast.ensemble(n = n, M = M, T = T, 
                                forecast = forecast, x = x, y = y,
                                t.end = t, t.window.length = min(300, T))
                                #date.fn = function(x) as.Date("2009-01-01") + x/4)
  #lines(temp$t.window.short, analysis[n,"mean",temp$t.window.short], col = "blue")

  # ...of the inflation parameter
  if(plot.inflation) {
  plot(temp$t.window.short, inflation[n,"mean",temp$t.window.short], type = "l", ylim = range(lower, upper, na.rm = TRUE), xlim = range(temp$t.window))
  polygon(c(temp$t.window.short, rev(temp$t.window.short)), c(upper[temp$t.window.short], rev(lower[temp$t.window.short])), density = 50, col = "lightgray", border = NA)
  lines(temp$t.window.short, inflation[n,"mean",temp$t.window.short], lwd = 1.2)
  }
  
  # ...of the distributions
  plot.den.assim(n = n, M = M, t = t, sd = sd,
                 analysis = analysis, forecast = forecast, x = x, y = y)
}
dev.off()





