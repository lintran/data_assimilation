
est.b = TRUE
est.L = TRUE
if(!"suffix" %in% objects()) suffix = "inf2"

# Load libraries, functions, and training data --------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

load("model_discrepancy/data/lorenz05/lorenz05.rdata")
load(sprintf("model_discrepancy/data/lorenz05/training/%s.rdata", suffix))

# Estimate empirical correction operator ---------------------------------------
parms$bias = list(b = rep(0, parms$N), z1 = matrix(0, nrow = parms$N), z2 = matrix(0, nrow = parms$N))

# ...state-independent bias
if(est.b) parms$bias$b = 1/diff(times[1:2]) * colMeans(Y.input$delta.xa)

# estimate state-dependent bias
if(est.L) {
  sd = lapply(Y.input, function(x) apply(x, 2, sd))
  
  C.svd.tilde = svd(cor(Y.input$delta.xa, Y.input$xa))
  #K = which(cumsum(C.svd.tilde$d) / sum(C.svd.tilde$d) >= .95)[1]; K
  K = 100
  
  C.svd = C.svd.tilde
  C.svd$u = sd$delta.xa * C.svd.tilde$u
  C.svd$v = sd$delta.xa * C.svd.tilde$v
  
  var.xa = var(Y.input$xa)
  gamma.den = 1/apply(C.svd$v, 2, function(x) crossprod(x, var.xa) %*% x)
  
  parms$bias$z1 = 1/diff(times[1:2]) * C.svd$u[,1:K] %*% diag(C.svd$d[1:K]*gamma.den[1:K])
  parms$bias$z2 = t(C.svd$v[,1:K])
  
  if(FALSE) {
    L = solve(var(Y.input$xa), cov(Y.input$xa, Y.input$delta.xa))
    L = 1/diff(times[1:2]) * C.svd$u %*% tcrossprod(diag(C.svd$d * gamma.den), C.svd$v)
    L.approx = 1/diff(times[1:2]) * C.svd$u[,1:K] %*% tcrossprod(diag(C.svd$d[1:K] * gamma.den[1:K]), C.svd$v[,1:K])
    
    plot(L[1,], type = "l", col = "darkgreen")
    lines(L.approx[1,], col = "blue")
  }
  
  parms$bias$b = parms$bias$b - parms$bias$z1 %*% (parms$bias$z2 %*% attr(Y.input$xa, "scaled:center"))
  
  rm(Y.input, C.svd, C.svd.tilde); gc()
}

forecast <- analysis <- array(dim = c(N, M+2, T-t.first+2))
analysis[,,1] = init

# lorenz05 model adding in bias
lorenz05.bias = function(t, y, parms) {
  out = lorenz05(t = t, y = y, parms = parms)
  out[[1]] = 
    out[[1]] + 
    parms$bias$z1 %*% (parms$bias$z2 %*% y) + 
    parms$bias$b
  return(out)
}

# arguments for update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "ETKF")

# filenames
file.header = sprintf("model_discrepancy/data/lorenz05/EnKF_danforth/%s_t%03d_b%s_L%s", suffix, t.first, substring(est.b,1,1), substring(est.L,1,1))
data.filename = sprintf("%s.rdata", file.header)
plot.filename = sprintf("%s.pdf", gsub("data", "plots", file.header, fixed = TRUE))

save.list = c("forecast", "analysis",
              "args.update", "parms",
              "N", "M", "T", "t",
              "DARTdir", "plot.filename", "data.filename",
              "forecast.length", "t.first", "t.same",
              "lorenz05.bias")

# Ensemble Kalman filter with Danforth's correction ----------------------------
for(t in 2:(T-t.first+2)) {
  t.equiv = t+t.first-2
  print(t.equiv)
  
  # Forecast: Propagate the ensemble members forward
  forecast[,,t] = data.forecast(analysis.ensemble = analysis[,1:M,t-1], 
                                adv.model = adv.model, times = times[1:2], 
                                func = lorenz05.bias, parms = parms,
                                delta.t = input.parms$time.step,
                                inflation.factor = 2,
                                N = N, M = M)
  
  # Update: Obtain analysis distribution
  analysis[,,t] = data.update(forecast.ensemble = forecast[,1:M,t], 
                              y = y[t.equiv,,drop = FALSE], 
                              args = args.update, N = N, M = M)
  
  if(t %% 500 == 0) save(list = save.list, file = data.filename)
}

if(t %% 500 != 0) save(list = save.list, file = data.filename)

# Plots -----------------------------------------------------------------------
if(FALSE) {
  # Concatenate with older data
  load(data.filename)
  forecast.new = forecast
  analysis.new = analysis
  plot.filename.new = plot.filename
  
  load("model_discrepancy/data/lorenz05/EnKF.rdata")
  forecast[,,t.first:T] = forecast.new[,,2:(T-t.first+2)]
  analysis[,,t.first:T] = analysis.new[,,2:(T-t.first+2)]
  plot.filename = plot.filename.new
  
  rm(forecast.new, analysis.new, plot.filename.new); gc()
  
  # Preliminaries before plotting
  n = 667 # gridpoint that we're keeping track of in the plots
  sd = sqrt(args.update$Sigma.0[n,n])
  
  # Plot
  Sys.sleep(1); pdf(plot.filename, width = 11, height = 8.5)
  for(t in t.first:T) {
    if(t %% 100 == 0) print(t)
    par(mfrow = c(2,1))
    
    # ...of the trajectory
    temp = plot.forecast.ensemble(n = n, M = M, T = T, 
                                  forecast = forecast, x = x, y = y, 
                                  t.end = t, t.window.length = 300)
    #lines(temp$t.window.short, analysis[n,"mean",temp$t.window.short], col = "blue")
    abline(v = seq(t.first, t.first, forecast.length), col = "gray")
    
    # ...of the distributions
    plot.den.assim(n = n, M = M, t = t, sd = sd,
                   analysis = analysis, forecast = forecast, x = x, y = y)
  }
  dev.off()
}


