
t.first = 7201

# Load libraries and functions -------------------------------------------------
library(spam)
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")
source("model_discrepancy/code/gaspari_cohn_corr.r")

# Initialization ---------------------------------------------------------------
# data and parameters
load("model_discrepancy/data/lorenz05/lorenz05.rdata")
load("model_discrepancy/data/lorenz05/EnKF.rdata")
rm(analysis); gc()

backsolve.chol = function(X, Y) {
  if(!is.spam(X) & !is.spam(Y)) {
    return(backsolve(X, Y, transpose = TRUE))
  } else {
    return(forwardsolve(X, Y))
  }
}

loglik = function(t, forecast, y, gc.corr, Sigma.0, M) {
  eps = y[t,] - forecast[,"mean",t]
  
  Sigma = gc.corr * (cov(t(forecast[,1:M,t])) + Sigma.0) # tapered covariance: C \circ (H P_f H^T + \Sigma_0)
  Sigma.chol = chol(Sigma)
  logdet = sum(log(diag(Sigma.chol)))
  
  b2 = backsolve.chol(Sigma.chol, eps)
  a = crossprod(b2)
  
  return(drop(-.5*(logdet + a)))
}

if(FALSE) {
Sigma.0.spam = as.spam(Sigma.0)

gc.corr = gaspari.cohn.corr.mat(1:N, l = l)
gc.corr.spam = as.spam(gc.corr)

system.time(sapply(1:10, loglik, forecast = forecast, y = y, gc.corr = gc.corr, Sigma.0 = Sigma.0, M))
system.time(sapply(1:10, loglik, forecast = forecast, y = y, gc.corr = gc.corr.spam, Sigma.0 = Sigma.0.spam, M))

system.time(sapply(1:10, function(x) det(gc.corr * (cov(t(forecast[,1:M,t])) + Sigma.0))))
system.time(sapply(1:10, function(x) det(gc.corr.spam * (cov(t(forecast[,1:M,t])) + Sigma.0))))
system.time(sapply(1:10, function(x) det(gc.corr.spam * (cov(t(forecast[,1:M,t])) + Sigma.0.spam))))
}

set.seed(12345)
#t.sample = seq(1,t.first-1)
t.sample = sort(sample(t.first-1, 500))

c = 1:30
L = rep(NA, length(c))
timings = L
for(i in seq(1,30,5)) {
  print(c[i])
  gc.corr = gaspari.cohn.corr.mat(1:N, c = c[i])
  timings[i] = system.time(L[i] <- sum(sapply(t.sample, loglik, forecast = forecast, y = y, gc.corr = gc.corr, Sigma.0 = Sigma.0, M)))[3]
}

plot(c, L, type = "l")
abline(v = c[which.max(L)])
# max at 17
