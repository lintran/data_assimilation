################################################################################
#
# Get ensemble members after running DART on model of interest
#
################################################################################

library(RNetCDF)

DARTdir = "~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/"

# get prior and posterior distributions ----------------------------------------
nc = open.nc(paste(DARTdir, "Prior_Diag.nc", sep = ""))
prior = read.nc(nc)
close.nc(nc)

nc = open.nc(paste(DARTdir, "Posterior_Diag.nc", sep = ""))
post = read.nc(nc)
close.nc(nc)

save(prior, post, file = "model_discrepancy/data/lorenz05/DARTensemble.rdata")


# check if everything matches up with what we expect ---------------------------
source("model_discrepancy/code/lorenz05/lorenz05.r")
load("model_discrepancy/data/lorenz05/DARTobsinits.rdata")

parms = lorenz05.parms(input.parms$parms)
times = c(0, sort(unique(obs[,"t"]))) * input.parms$time.step / input.parms$ts.convert

sol = ode(y = inits$perfect, times = times[1:2], 
          func = lorenz05.3, parms = parms, 
          method = "ode45", verbose = TRUE)
all.equal(sol[2,-1], obs[obs[,"t"] == .5, "truth"])

# differences should be small
test = rep(NA, input.parms$M)
for(m in 1:20) {
  sol = ode(y = inits$perturbed[,m], times = times[1:2], 
            func = lorenz05.3, parms = parms, 
            method = "ode45", verbose = FALSE)
  test[m] = all.equal(sol[2,-1], prior$state[,m+2,1], check.names = FALSE)
}
test

# differences should be not small
for(m in 1:19) {
  sol = ode(y = inits$perturbed[,m+1], times = times[1:2], 
            func = lorenz05.3, parms = parms, 
            method = "ode45", verbose = FALSE)
  test[m] = all.equal(sol[2,-1], prior$state[,m+2,1], check.names = FALSE)
}
test

