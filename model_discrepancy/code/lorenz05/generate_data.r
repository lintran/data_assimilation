################################################################################
#
# Generate assimilation data from the Lorenz 05 model
#
################################################################################

library(mvtnorm)
source("model_discrepancy/code/lorenz05/lorenz05.r")

input.parms = list(time.step = 1/120,
                   ts.convert = 1/24,
                   parms = c(N = 960, K = 32, I = 12, F = 15, b = 10, c = 2.5, model.number = 3),
                   M = 100,
                   days.to.sec = 24*60*60,
                   sec.to.days = 1/(24*60*60))

# The true solution, X_t: Lorenz 2005 Model 3 ----------------------------------
#  Per Lorenz's suggestion, each variable was chosen randomly from a uniform
#  distribution and integrated forward for two years.  This was then used as
#  the initial state and propagated forward for one month.
#

set.seed(12345)
start = runif(960)
parms = lorenz05.parms(input.parms$parms)

times.high = c(0, 2*12*30*24/120 + seq(0, 30*24/120, 1/120))
#                 2 years        + 30 days in 1 hour intervals
length(times.high)

sol = ode(y = start, times = times.high, func = lorenz05, parms = parms, 
          method = "ode45", verbose = TRUE)

# The wrong solution: Lorenz 2005 Model 2 --------------------------------------
parms2 = parms
parms2["model.number"] = 2

sol.wrong = ode(y = start, times = times.high, func = lorenz05, parms = parms2, 
                method = "ode45", verbose = TRUE)

# Generate the data, Y_t -------------------------------------------------------
#  Data every 6 hours.

sigma = 1 # Standard deviation of the observations in each coordinate
times = 2*12*30*24/120 + seq(0, 30*24/120, 6/120)
x = sol[sol[,"time"] %in% times,-1]
set.seed(12345)
y = x + matrix(rnorm(prod(dim(x)), sd = sigma), ncol = parms$N) # Observations

Sigma.0 = diag(sigma^2, parms$N)
Sigma.0.inv = solve(Sigma.0)
Sigma.0.chol = t(chol(Sigma.0))
sigma = rep(sigma, parms$N)

# Initial conditions of ensemble members ---------------------------------------
ensemble.ics = t(rmvnorm(n = input.parms$M, mean = x[1,]))

# Save -------------------------------------------------------------------------
input.parms$N = parms$N
input.parms$T = length(times)

save(x, y, Sigma.0, Sigma.0.inv, Sigma.0.chol, sigma,
     sol, sol.wrong, parms, parms2, times.high, times, 
     ensemble.ics, input.parms,
     file = "model_discrepancy/data/lorenz05/lorenz05.rdata")

