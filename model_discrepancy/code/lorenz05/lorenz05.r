################################################################################
#
# Functions to run Lorenz 05 models
#
################################################################################

require(deSolve)

sumprime = function(x, K, x.len = length(x)) sum(x) + (K %% 2 - 1) * sum(x[c(1, x.len)]) / 2

wrap = function(i, n) (i-1) %% n + 1

ZtoXY = function(Z, parms) {
  X = sapply(1:parms$N, function(n) sumprime(parms$alpha.minus.betai * Z[wrap((n-parms$I):(n+parms$I), parms$N)], parms$K, parms$len.I) )
  Y = Z - X
  
  return(list(X = X, Y = Y))
}

dyn.load("~/data_assimilation/model_discrepancy/code/lorenz05/lorenz05.so")
lorenz05 = function(t, y, parms = lorenz05.parms(c(N = 960, K = 32, I = 12, b = 10, c = 2.5, F = 15, model.number = 3))) {
  if(!parms$model.number %in% c(2,3)) stop("Invalid 'model.number'.  'model.number' is supposed to be 2 or 3.")
  
  dt = as.double(rep(0, parms$N))
  out = .Fortran("comp_dt",
                 z                 = as.double(y),
                 dt                = dt,
                 model_number      = as.integer(parms$model.number), 
                 model_size        = as.integer(parms$N), 
                 K                 = as.integer(parms$K), 
                 K2                = as.integer(parms$K2), 
                 K4                = as.integer(parms$K4), 
                 H                 = as.integer(parms$J), 
                 forcing           = as.double(parms$F),
                 smooth_steps      = as.integer(parms$I), 
                 ss2               = as.integer(parms$I2), 
                 space_time_scale  = as.double(parms$b), 
                 sts2              = as.double(parms$b.sq), 
                 coupling          = as.double(parms$c),
                 alpha_minus_betai = as.double(parms$alpha.minus.betai),
                 DUP = FALSE)
  
  return(if(parms$model.number == 2) list(dX = out$dt) else list(dZ = out$dt))
}

lorenz05.plus.linear = function(t, y, parms = lorenz05.parms(c(N = 240, K = 8, F = 15, model.number = 3))) {
  dX = lorenz05(t, y, parms)
  dX[[1]] = drop(dX[[1]] + parms$L %*% y + parms$b)
  return(dX)
}

lorenz05.parms = function(parms) {
  model = parms["model.number"]
  
  if(model == 2) {  # just dummy variables
    parms = c(parms, I = 1, b = 1, c = 1)
  } else if(model != 3) {
    stop("Invalid 'model' number.")
  }
  
  if(parms["K"] %% 2 != 0) stop("Code has only been implemented for 'K' even.")
  
  parms = as.list(parms)
  
  parms$J = floor(parms$K/2)
  parms$K2 = 2*parms$K
  parms$K4 = 4*parms$K
  parms$I2 = 2*parms$I
  parms$alpha = (3*parms$I^2 + 3) / (2*parms$I^3 + 4*parms$I)
  parms$beta = (2*parms$I^2 + 1) / (parms$I^4 + 2*parms$I^2)
  parms$alpha.minus.betai = parms$alpha - parms$beta * abs(-parms$I:parms$I)
  parms$b.sq = parms$b^2
  
  return(parms)
}

adv.1step = function(x, func, parms, delta.t) {
  # This is how DART integrates the Lorenz 2005 model, cf subroutine adv_1step in DART/Kodiak/models/lorenz_04/model_mod.f90
  # DART's comment:
  #     Does single time step advance for lorenz 04 model
  #     using four-step rk time step
  
  # Compute the first intermediate step
  dx = func(0, y = x, parms = parms)[[1]]
  x1    = delta.t * dx
  inter = x + x1 / 2
 
  # Compute the second intermediate step
  dx = func(0, y = inter, parms = parms)[[1]]
  x2    = delta.t * dx
  inter = x + x2 / 2
  
  # Compute the third intermediate step
  dx = func(0, y = inter, parms = parms)[[1]]
  x3    = delta.t * dx
  inter = x + x3
  
  # Compute fourth intermediate step
  dx = func(0, y = inter, parms = parms)[[1]]
  x4 = delta.t * dx
  
  # Compute new value for x
  dxt = x1/6 + x2/3 + x3/3 + x4/6
  return(x + dxt)
}

adv.model = function(y, times, func, parms, delta.t, verbose = FALSE) {
  # Wrapper for adv.1step. The output matches the structure of the 'ode' function of the 'deSolve' package
  T = length(times)
  
  # initialize output
  out = matrix(nrow = T, ncol = length(y)+1)
  colnames(out) = c("time", 1:length(y))
  out[,"time"] = times
  out[1,-1] = y
  x = y
  
  # number of integration steps
  int.steps = c(NA, diff(times)/delta.t)
  
  # do the integration
  for(i in 2:T) {
    if(verbose) if(i %% 100 == 0) print(sprintf("%s / %s", i, T))
    for(j in 1:int.steps[i]) 
      x = adv.1step(x = x, func = func, parms = parms, delta.t = delta.t)
    out[i,-1] = x
  }
  
  return(out)
}

# Jacobian of Lorenz 2005 Model 2. Output: M_ij = \partial M_i / partial x_j ( \mu_t^a), where M is Model 2 and \mu_t^a is the analysis mean
lorenz05.lin = function(y, parms) {
  if(parms$model.number != 2) stop("Invalid 'model.number'.  Can only linearize Lorenz 2005 if 'model.number' is 2.")
  
  out = .Fortran("linearize_dt",
                 x           = as.double(y),
                 M           = rep(0, parms$N^2),
                 model_size  = as.integer(parms$N), 
                 K           = as.integer(parms$K), 
                 K2          = as.integer(parms$K2), 
                 K4          = as.integer(parms$K4),
                 Ksq         = as.double(parms$Ksq),
                 H           = as.integer(parms$J))
  return(matrix(out$M, nrow = parms$N, ncol = parms$N))
}

if(FALSE) {
  # test lorenz05.lin
  load("model_discrepancy/data/lorenz05/lorenz05.rdata")
  parms["model.number"] = 2
  parms$Ksq = parms$K^2
  
  N = parms$N
  K = parms$K
  J = parms$J
  
  wrap = function(i, n) (i-1) %% n + 1
  sumprime = function(x, K, x.len = length(x)) sum(x) + (K %% 2 - 1) * sum(x[c(1, x.len)]) / 2
  
  x = rnorm(parms$N, 10, 2)
  dyn.load("model_discrepancy/code/lorenz05/lorenz05.so")
  blah = lorenz05.lin(x, parms)
  blah[1:5, 1:5]
  
  n = 17
  j1 = wrap(n-K-(-J:J),N)
  blah[17,j1]
  sapply(1:length(j1), function(...) sumprime(x[wrap(n-2*K-(-J:J),N)], K))
  
  i = sapply(-J:J, function(j) n-K+j-(-J:J))
  table(i)
  wrap(i, N)
  
  library(fields)
  maxL = max(abs(blah))
  image.plot(1:960, 1:960, blah,
             zlim = c(-1,1)*maxL,
             breaks = seq(-maxL, maxL, length.out = 20), 
             col = colorRampPalette(c("red", "white", "blue"))(19),
             xlab = "M", ylab = "X")
}

