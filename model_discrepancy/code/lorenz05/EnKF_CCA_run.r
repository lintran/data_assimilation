
t.first = 7301
K = 350
rotate = FALSE
gibbs = FALSE

feedback = FALSE

# Load libraries and functions -------------------------------------------------
#library(CCAGFA)
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

if(gibbs) {
  source("model_discrepancy/code/fns_bibfa_gibbs.r")
} else {
  source("model_discrepancy/code/fns_onlineCCA.r")
}

# Initialization ---------------------------------------------------------------
# output info
load("model_discrepancy/data/lorenz05/lorenz05.rdata")

if(!feedback) {
  load("model_discrepancy/data/lorenz05/EnKF.rdata")
  forecast = forecast[,,(t.first-1):T]
  analysis = analysis[,,(t.first-1):T]
  gc()
}

file.header = sprintf("model_discrepancy/data/lorenz05/EnKF_onlineCCA/t%04d_K%03d_sepMultT_rot%s%s", 
                      t.first, K,
                      substr(rotate,1,1),
                      ifelse(gibbs, "_gibbsTraining", ""))
data.filename = sprintf("%s_postSamp.rdata", file.header)
load(data.filename)

bias = array(dim = c(N, M+2, T-t.first+2))
if(feedback) {
  bias <- forecast <- analysis
  analysis[,,1] = init
}

# arguments for forecast and update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "ETKF")

# save info
file.header = sprintf("%s_fbk%s", gsub("(online)|(_sepMultT)", "", file.header), substr(feedback,1,1))
data.filename = sprintf("%s.rdata", file.header)
plot.filename = sprintf("%s.pdf", gsub("data", "plots", file.header, fixed = TRUE))

save.list = c("bias",
              "args.update",
              "N", "M", "T", "t",
              "DARTdir", "plot.filename", "data.filename",
              "forecast.length", "t.first", "t.same",
              "lorenz05.bias", "multiplier.separate", "rotate", "K")
if(feedback) save.list = c(save.list, "forecast", "analysis")

# Ensemble Kalman filter with streaming CCA ------------------------------------
for(t in 2:(T-t.first+2)) {
  t.equiv = t+t.first-2
  print(t.equiv)
  
  # Forecast: Propagate the ensemble members forward
  if(feedback) {
  forecast[,,t] = data.forecast(analysis.ensemble = analysis[,1:M,t-1], 
                                adv.model = adv.model, times = times[1:2], 
                                func = lorenz05, parms = parms,
                                delta.t = input.parms$time.step,
                                inflation.factor = 2,
                                N = N, M = M)
  }
  
  # Estimate bias and add into forecast
  bias[,1:M,t] =
    if(!gibbs) {
      t(GFApred2(list(delta.xa = NULL, xa = t(analysis[,1:M,t-1])), 
                 parms$bibfa.samp$params, 
                 parms$bibfa.samp$sam)[[parms$bibfa.samp$params$pr]])
    } else {
      t(postPredSampler(list(delta.xa = NULL, xa = t(analysis[,1:M,t-1])), 
                        parms$post.samp$params, 
                        parms$post.samp$sam)[[parms$post.samp$params$pr]])
    }
  bias[,M+1,t] = rowMeans(bias[,1:M,t])
  bias[,M+2,t] = apply(bias[,1:M,t], 1, var)
  
  # Update: Obtain analysis distribution
  if(feedback) {
  analysis[,,t] = data.update(forecast.ensemble = forecast[,1:M,t] + bias[,1:M,t],
                              y = y[t.equiv,,drop = FALSE], 
                              args = args.update, N = N, M = M)
  }
}

save(list = save.list, file = data.filename)

# Plots -----------------------------------------------------------------------
if(FALSE) {
plot.forecast.ensemble(n = 667, M = M, T = T-t.first+2, 
                       forecast = forecast + bias, x = x[(t.first-1):T,], y = y[(t.first-1):T,], 
                       t.end = 10, t.window.length = 300)

plot.forecast.ensemble(n = 667, M = M, T = T-t.first+2, 
                       forecast = forecast + bias, x = x[(t.first-1):T,], y = y[(t.first-1):T,], 
                       t.end = 100, t.window.length = 300)


plot.forecast.ensemble(n = 667, M = M, T = T-t.first+2, 
                       forecast = forecast + bias, x = x[(t.first-1):T,], y = y[(t.first-1):T,], 
                       t.end = T-t.first+2, t.window.length = 300)
}