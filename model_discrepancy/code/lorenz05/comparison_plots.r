
load.data = FALSE

library(RColorBrewer)
library(ggplot2)

old.par = par()

if(load.data) {
  load("model_discrepancy/data/lorenz05/EnKF_CCA_plots.rdata")
} else {
  forecasts = vector("list", 6)
  flen = length(forecasts)
  fplot = c(2:6)
  fplotlen = length(fplot)
  
  load("model_discrepancy/data/lorenz05/EnKF_CCA.rdata")
  forecasts[[1]] = forecast$ensemble
  forecasts[[2]] = forecast.bias$ensemble
  rm(list = objects()[!objects() %in% c("old.par", "forecasts", "x", "y", "parms", "T", "M", "flen", "fplot", "fplotlen")]); gc()
  
  runs = read.csv("model_discrepancy/data/lorenz05/DARTruns.csv")
  i = 3
  for(folder in as.character(runs$folder.name)) {
    load(paste("model_discrepancy/data/lorenz05/EnKF_DART_", folder, ".rdata", sep = ""))
    forecasts[[i]] = forecast$ensemble
    rm(list = objects()[!objects() %in% c("i", "runs", "old.par", "forecasts", "x", "y", "parms", "T", "M", "flen", "fplot", "fplotlen")]); gc()
    i = i+1
  }
  
  #load("model_discrepancy/data/lorenz05/EnKF_CCA2.rdata")
  #forecasts[[4]] = forecast.bias.ensemble
  #rm(list = objects()[!objects() %in% c("old.par", "forecasts", "x", "y", "parms", "T", "M", "flen", "fplot", "fplotlen")]); gc()
  
  ylim.point = lapply(1:parms$N, function(n) range(lapply(forecasts, function(f) f[n,,]), na.rm = TRUE))
  ylim.time = lapply(1:T, function(t) range(lapply(forecasts, function(f) f[,,t]), na.rm = TRUE))
  ylim.all = range(unlist(ylim.point), x, y, na.rm = TRUE)
}

if(FALSE) {
#pdf("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_point.pdf", width = 8.5, height = 11)
png("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_point/image%d.png", width = 8.5/11*900, height = 900)
par(mfrow = c(fplotlen,1), mar = c(0,4.1,0,1))
for(n in 1:parms$N) {
  print(n)
  for(f in fplot) {
    plot(forecasts[[f]][n,1,], col = "lightgray", type = "l", ylab = "", xlab = "t", # mean of forecast ensemble
         ylim = ylim.point[[n]])
    for(m in 2:M) lines(forecasts[[f]][n,m,], col = "lightgray")
    lines(x[,n], lwd = 2, col = "green4")                                           # the true solution
    points(y[,n], pch = 19, cex = .5)                                               # observations
  }
}
dev.off()

pdf("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_point/image667.pdf", width = 11, height = 8.5)
par(mfrow = c(fplotlen,1), mar = c(0,4.1,0,1))
n = 667
for(f in fplot) {
  plot(forecasts[[f]][n,1,], col = "lightgray", type = "l", ylab = "", xlab = "t", # mean of forecast ensemble
       ylim = ylim.point[[n]])
  for(m in 2:M) lines(forecasts[[f]][n,m,], col = "lightgray")
  lines(x[,n], lwd = 2, col = "green4")                                         # the true solution
  points(y[,n], pch = 19, cex = .5)                                               # observations
}
dev.off()

pdf("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_point/image667_EnKF.pdf", width = 11, height = 4.5)
par(mar = c(2,4.1,0,1))
n = 667
f = 1
plot(forecasts[[f]][n,1,], col = "lightgray", type = "l", ylab = "", xlab = "t", # mean of forecast ensemble
     ylim = ylim.point[[n]])
for(m in 2:M) lines(forecasts[[f]][n,m,], col = "lightgray")
lines(x[,n], lwd = 2, col = "green4")                                         # the true solution
points(y[,n], pch = 19, cex = .5)                                               # observations
dev.off()


#pdf("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_time.pdf", width = 8.5, height = 11)
png("model_discrepancy/plots/lorenz05/EnKF_CCA_snapshots_time/image%d.png", width = 11/8.5*900, height = 900)
par(mfrow = c(fplotlen,1), mar = c(0,4.1,0,0))
for(t in 2:T) {
  for(f in fplot) {
    plot(forecasts[[f]][,1,t], col = "lightgray", type = "l", ylab = "", xlab = "t", # mean of forecast ensemble
         ylim = ylim.all)
    for(m in 2:M) lines(forecasts[[f]][,m,t], col = "lightgray")
    lines(x[t,], lwd = 1.2, col = "green4")                                         # the true solution
    points(y[t,], pch = 19, cex = .1)                                               # observations
  }
}
dev.off()
}

# Coverage probabilities over time ---------------------------------------------
coverage = array(dim = c(parms$N,T,flen))
for(f in 1:flen) {
  qs = apply(forecasts[[f]], c(1,3), quantile, probs = c(.025, .975), na.rm = TRUE)
  coverage[,,f] = t(x) > qs[1,,] & t(x) < qs[2,,]
}
coverage[,1,] = NA
coverage[,2,3:flen] = NA

plot.labs = c(expression(paste(w^f, " from CCA     ")),
              expression(paste(x^f, " from CCA     ")),
              bquote(paste(x^f, " from ", .(as.character(runs$folder.name)[1]), "     ")),
              bquote(paste(x^f, " from ", .(as.character(runs$folder.name)[2]), "     ")),
              bquote(paste(x^f, " from ", .(as.character(runs$folder.name)[3]), "     ")),
              bquote(paste(x^f, " from ", .(as.character(runs$folder.name)[4]), "     ")))
gg_color_hue <- function(n) {
  #http://stackoverflow.com/questions/8197559/emulate-ggplot2-default-color-palette
  hues = seq(15, 375, length=n+1)
  hcl(h=hues, l=65, c=100)[1:n]
}
#plot.col = brewer.pal(flen, "Set1")
plot.col = gg_color_hue(fplotlen)

plot.me = data.frame(apply(coverage, 3, colMeans))
plot.me = reshape(plot.me, direction = "long", varying = 1:flen, v.names = "coverage", timevar = "forecast", idvar = "t")
plot.me$forecast = factor(plot.me$forecast)
plot.me = subset(plot.me, forecast %in% fplot)

pdf("model_discrepancy/plots/lorenz05/coverage.pdf", width = 11, height = 8.5)
ggplot(plot.me, aes(x = t, y = coverage, group = factor(forecast))) + 
  geom_hline(yintercept = .95, color = "gray", lwd = .5) +
  geom_line(aes(color = forecast), size = .5) + 
  geom_point(aes(color = forecast, shape = forecast)) +
  ylab("percentage of gridpoints covered") + xlab("") +
  scale_colour_manual("forecast", values = plot.col, labels = plot.labs[fplot]) +
  scale_shape_manual("forecast", values = 1:fplotlen, labels = plot.labs[fplot]) +
  theme(legend.position = "bottom", 
        panel.background = element_rect(fill = "white", colour = "white"),
        panel.border = element_rect(colour = "gray50", fill = NA),
        panel.grid.major = element_line(colour = "gray90"))
dev.off()

# RMS --------------------------------------------------------------------------
rms = array(dim = c(T,flen))
for(f in 1:flen) {
  temp = sapply(1:T, function(t) forecasts[[f]][,,t] - x[t,], simplify = "array")
  rms.overM = apply(temp, c(1,3), function(x) sqrt(mean(x^2)))
  rms[,f] = apply(rms.overM, 2, mean)
}

plot.me = reshape(data.frame(rms), direction = "long", varying = 1:flen, v.names = "rms", timevar = "forecast", idvar = "t")
plot.me$forecast = factor(plot.me$forecast)
plot.me = subset(plot.me, forecast %in% fplot)

pdf("model_discrepancy/plots/lorenz05/rms.pdf", width = 11, height = 8.5)
ggplot(plot.me, aes(x = t, y = rms, group = factor(forecast))) + 
  geom_hline(yintercept = 0, color = "gray", lwd = .5) +
  geom_line(aes(color = forecast), size = .5) + 
  geom_point(aes(color = forecast, shape = forecast)) +
  ylab("average RMS") + xlab("") +
  scale_colour_manual("forecast", values = plot.col, labels = plot.labs[fplot]) +
  scale_shape_manual("forecast", values = 1:fplotlen, labels = plot.labs[fplot]) +
  theme(legend.position = "bottom", 
        panel.background = element_rect(fill = "white", colour = "white"),
        panel.border = element_rect(colour = "gray50", fill = NA),
        panel.grid.major = element_line(colour = "gray90"))
dev.off()

save.image("model_discrepancy/data/lorenz05/EnKF_CCA_plots.rdata")





# verification rank histogram
t = 50
hist(sapply(1:960, function(n) rank(c(x[t,n], forecasts[[2]][n,,t]))[1]))





