######################################################################################
#
# Converts all the different DART outputs to work with the system we have here in R
#
######################################################################################

runs = read.csv("model_discrepancy/data/lorenz05/EnKF_DART/DARTruns.csv", as.is = TRUE)
runs$run = as.logical(runs$run)
runs$plot.inflation = as.logical(runs$plot.inflation)

runs = runs[runs$run,]

for(folder in runs$folder.name) {
  print(folder)
  
  DARTdir = paste("~/Desktop/Grad school/Research/data_assimilation/DART/Kodiak/models/lorenz_04/work/", folder, "/", sep = "")
  plot.filename = paste("model_discrepancy/plots/lorenz05/EnKF_DART/", folder, "_snapshots.pdf", sep = "")
  save.filename = paste("model_discrepancy/data/lorenz05/EnKF_DART/", folder, ".rdata", sep = "")
  
  plot.inflation = runs[runs$folder.name == folder, "plot.inflation"]
  
  source("model_discrepancy/code/lorenz05/EnKF_DART.r", verbose = TRUE)
  
  rm(DARTdir)
}


