
# Model description ------------------------------------------------------------
#             X_0 ~ MVN(0_N, I_N)
#   X_t | X_{t-1} ~ w_t( X_t-1, parms )
#       Y_t | X_t ~ MVN(X_t, sigma^2 I_N)
# where w_t(cdot, parms) is Lorenz 2005 Model 2.
#

# Load libraries and functions -------------------------------------------------
source("model_discrepancy/code/fns_EnKF_processdata.r")
source("model_discrepancy/code/lorenz05/lorenz05.r")

# Initialization ---------------------------------------------------------------
# data and parameters
load("model_discrepancy/data/lorenz05/lorenz05.rdata")
parms["model.number"] = 2
parms["F"] = 10
N = parms$N; M = input.parms$M; T = input.parms$T

# output info
forecast <- analysis <- array(dim = c(N, M+2, T))
dimnames(forecast) <- dimnames(analysis) <-
  list(state = NULL,
       ensemble = c(sprintf("e%d", 1:M), "mean", "var"),
       time = NULL)

# arguments for forecast and update steps
args.update = 
  list(Sigma.0 = Sigma.0,
       Sigma.0.inv = Sigma.0.inv,
       method = "ETKF")

# Initialize the particles
forecast[,1:M,1] = ensemble.ics
forecast[,M+1,1] = rowMeans(forecast[,1:M,1])
forecast[,M+2,1] = apply(forecast[,1:M,1], 1, var)

analysis[,,1] = data.update(forecast.ensemble = forecast[,1:M,1], 
                            y = y[1,,drop = FALSE], 
                            args = args.update, N = N, M = M)

# objects to save
save.list = c("forecast", "analysis",
              "args.update",
              "parms", "N", "M", "T", "t")

# Run EnKF using the wrong model, Model 2 --------------------------------------
for(t in 2:T) {
  print(t)
  
  # Forecast: Propagate the ensemble members forward
  forecast[,,t] = data.forecast(analysis.ensemble = analysis[,1:M,t-1], 
                                adv.model = adv.model, times = times[1:2], 
                                func = lorenz05, parms = parms,
                                delta.t = input.parms$time.step,
                                inflation.factor = 2,
                                N = N, M = M)
  
  # Update: Obtain analysis distribution
  analysis[,,t] = data.update(forecast.ensemble = forecast[,1:M,t], 
                              y = y[t,,drop = FALSE], 
                              args = args.update, N = N, M = M)
  
  if(t %% 500 == 0) save(list = save.list,
                         file = "model_discrepancy/data/lorenz05/EnKF.rdata")
}

if(t %% 500 != 0) save(list = save.list,
                       file = "model_discrepancy/data/lorenz05/EnKF.rdata")

