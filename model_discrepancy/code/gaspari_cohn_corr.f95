double precision function gaspari_cohn_corr(x1, x2, c, N, x_max, m)
  ! Calculates an entry in the Gaspari-Cohn correlation matrix
  !
  implicit none ! tells compiler that you won't allow implicit types
  integer, intent(in) :: N
  double precision, intent(in) :: x1, x2, c, x_max, m(10)
  double precision :: output, d, r
  
  !d = sqrt(dot_product(x1 - x2, x1 - x2))
  d = min(abs( x1-x2 ), abs( (x_max+x1)-x2 ), abs( x1-(x_max+x2) ))
  r = d/c

  if (d <= c) then
    output = -m(1)*r**5 + m(2)*r**4 + m(3)*r**3 - m(4)*r**2 + m(5)
  elseif (d <= 2.*c) then
    output =  m(6)*r**5 - m(2)*r**4 + m(3)*r**3 + m(4)*r**2 - m(7)*r + m(8) - m(9)*c/d
  else
    output = m(10)
  endif
  gaspari_cohn_corr = output
return
end function gaspari_cohn_corr

subroutine gaspari_cohn_corr_mat(x, c, N, x_max, nugget, output)
  ! Calculates the Gaspari-Cohn correlation matrix using the function gaspari_cohn_corr

  implicit none ! tells compiler that you won't allow implicit types

  integer, parameter :: rp = kind(0.d0)
  
  integer, intent(in) :: N
  double precision, intent(in) :: x(N), c, x_max, nugget
  double precision, intent(out) :: output(N,N)
  
  integer :: n1, n2
  double precision :: m(10), gaspari_cohn_corr

  m(1) = 1.0_rp / 4.0_rp
  m(2) = 1.0_rp / 2.0_rp
  m(3) = 5.0_rp / 8.0_rp
  m(4) = 5.0_rp / 3.0_rp
  m(5) = 1.0_rp
  m(6) = 1.0_rp / 12.0_rp
  m(7) = 5.0_rp
  m(8) = 4.0_rp
  m(9) = 2.0_rp / 3.0_rp
  m(10) = 0.0_rp
  
  do n1 = 1,(N-1)
    do n2 = (n1+1),N
      output(n1,n2) = gaspari_cohn_corr(x(n1), x(n2), c, N, x_max, m)
    enddo
  enddo
  
  ! symmetrize the matrix
  do n1 = 1,(N-1)
    do n2 = (n1+1),N
      output(n2,n1) = output(n1,n2)
    enddo
  enddo
  
  ! deal with the diagonal
  do n1 = 1,N
    output(n1,n1) = 1. + nugget
  enddo
return
end subroutine gaspari_cohn_corr_mat


