
# Code adapted from CGK's file:
#  ~/Dropbox/Grad school/Research/model_error/toy_models/LorenzModels.particle.R

# Update step, analysis distribution

kf.update = function(forecast, y, 
                       H = NULL, Sigma.0) {
  # Equations 27.5 and 27.6 of Gelfand et al, Handbook of Spatial Statistics
  #   mu_a = mu_f + [Sigma_f H^T (H Sigma_f H^T + Sigma_0)^-1 ] (y_t - H mu_f)
  #                 [------ = Kalman Gain (KG) ---------------]
  
  #   Sigma_a = Sigma_f - KG H Sigma_f
  
  # Error checking
  n = length(drop(y))
  if(n != length(drop(forecast$mu)) | n != nrow(forecast$Sigma)) stop("Dimensions of 'forecast' do not match with 'y'.")
  if(is.null(H)) H = diag(x = 1, nrow = n, ncol = n) # check
  
  A = H %*% forecast$Sigma
  R = chol( tcrossprod(A, H) + Sigma.0 )
  C = backsolve(R, A, transpose = TRUE)
  KG.t = backsolve(R, C) # KG^T: transpose of Kalman gain
  
  list(list(mu = forecast$mu + KG.t %*% (y - H %*% forecast$mu), # mu_a
            Sigma = forecast$Sigma - KG.t %*% A))                # Sigma_a
}

kf.forecast = function(analysis,
                         L = NULL, Sigma.m) {
  # Distribution below Equation 27.7 of Gelfand et al, Handbook of Spatial Statistics
  #   mu_f = L mu_a
  #   Sigma_f = L Sigma_a L^T + Sigma_m
  
  if(is.null(L)) {
    n = length(drop(analysis$mu))
    L = diag(x = 1, nrow = n, ncol = n) # check
  }
  
  return(list(mu = L %*% analysis$mu,                                  # mu_f
              Sigma = L %*% tcrossprod(analysis$Sigma, L) + Sigma.m))  # Sigma_f
}
