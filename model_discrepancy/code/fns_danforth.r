

model.bias = function(t, y, model, parms,
                      z = NULL, b = NULL) {
  if(is.null(b)) b = rep(0, parms$N)
  if(is.null(z)) z = matrix(1, nrow = parms$N)
  out = model(t = t, y = y, parms = parms)
  out[[1]] = out[[1]] + rowSums(z * out[[1]]) + b
  return(out)
}




