
source("CCAGFA/R/CCAGFA.R")

GFAsample.pred = function(pred, model, nSample = 1) {
  # Co-opted from GFApred
  
  tr = which(pred==1)  # The observed data sets
  pr = which(pred==0)  # The data sets that need to be predicted
  sam = lapply(1:nSample, function(sample) list())
  
  for(i in 1:nSample) {
    # Sample W
    sam[[i]]$W = lapply(1:2, function(m) model$W[[m]] + matrix(rnorm(model$K*model$D[[m]],0,1),model$D[m],model$K) %*% model$cholW[[m]])
    
    # Calculate Var(Z | X[tr]) = I - Sum_{i in tr} I - Wi^t (Sigmai + WiWi^t)^-1 Wi
    varZgiventr = diag(model$K)
    for(m in tr) {
      varZgiventr = varZgiventr - 
        crossprod(sam[[i]]$W[[m]],
                  solve(diag(1/model$tau[m], model$D[m]) + tcrossprod(sam[[i]]$W[[m]]),
                        sam[[i]]$W[[m]]))
    }
    sam[[i]]$cholvarZgiventr = chol(varZgiventr)
  }
  
  return(
    list(params = list(nSample = nSample, tr = tr, pr = pr, D = model$D, K = model$K, tau = model$tau, mean = model$mean), 
         sam = sam)
  )
}

GFApred2 = function(Ynew, params, sam) {
  N = nrow(Ynew[[params$tr[1]]])
  idx.sample = sample(length(sam), N, replace = TRUE)

  out = lapply(Ynew, function(m) list())
  for(m in params$pr) out[[m]] = matrix(nrow = N, ncol = params$D[m])

  for(n in 1:N) {
    i = idx.sample[n]
    
    # Sample from Z | X[tr]
    #Z = rnorm(K) %*% sam[[i]]$cholvarZgiventr
    #for(m in tr) Z = Z + (Ynew[[m]][n,] - mean[[m]]) %*% sam[[i]]$W[[m]]
    Z = matrix(0,1,params$K)
    for(m in params$tr) Z = Z + (Ynew[[m]][n,] - params$mean[[m]]) %*% sam[[i]]$W[[m]] * params$tau[m]
    Z = tcrossprod(Z, sam[[i]]$cholvarZgiventr)
    Z = (Z + rnorm(params$K)) %*% sam[[i]]$cholvarZgiventr
    
    # Sample from X[pr] | X[tr]
    #for(m in pr) out[[m]][n,] = drop(mean[[m]] + tcrossprod(sam[[i]]$W[[pr]], Z) + rnorm(D[pr], 0, 1/sart(tau[m])))
    for(m in params$pr) out[[m]][n,] = params$mean[[m]] + tcrossprod(Z, sam[[i]]$W[[m]]) + rnorm(params$D[m], 0, 1/sqrt(params$tau[m]))
  }
  
  return(out)
}
