%!TEX root = quals.tex
\synctex=1

\begin{frame}
  \frametitle{Summary}
  Estimating and predicting model error in the data assimilation framework is naturally a hierarchical modeling problem.
  \begin{align*}
    \alert<1->{\boldsymbol{b}_t}
      &\alert<1->{= \text{?}} \\
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}] \alert<1->{+ \text{?}})
        \alert<1->{+ \text{?}}\\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}

  The key to this research is figuring out the right form to put on the bias. \visible<2->{Possible features to incorporate into the form:
  \begin{itemize}
    \item temporal correlation with previous bias
    \item \alert<3->{state-dependent}
    \item spatial correlation
    \item amenable to sequential updating
  \end{itemize}
  }

  \note<1>{
  Once we recognize that the problem is naturally a hierarchical model, the key to solving the model error problem is to figure out the right form to put on the bias.
  }

  \note<2>{
  \begin{itemize}
    \item We can have the bias be correlated with the previous bias, like in the sequentially correlated bias literature...
    \item ...or it can depend on the state, like the linear correction literature
    \item What I didnt show you in this talk is how the bias is spatially correlated, so we might want to incorporate spatial correlation into the prior
    \item We have also seen both offline and online solutions to the problem. It's desirable to have an online method where we are continuously predicting the bias and updating our prior as more observations come in. Doing it in an online manner will allow for parameters to change as the system changes, such as through the change of seasons, for example.
  \end{itemize}
  }
\end{frame}

\begin{frame}
  \frametitle{Generative model for the linear correction method}
  Problem formulation:

  \vspace{-1em}
  \begin{center}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \dot{\boldsymbol{x}}(t) 
      &= \mathcal{M}[\boldsymbol{x}(t); \boldsymbol{\theta}] + \mathcal{D}[\boldsymbol{x}(t)] \\
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \boldsymbol{f}_t, \boldsymbol{d}_t) \\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  \end{minipage}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \mathcal{D}[\boldsymbol{x}(t)]
      &= L \boldsymbol{x}(t) + \boldsymbol{b} + \boldsymbol{\varepsilon}(t) \\
    \boldsymbol{f}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}]) \\
    \boldsymbol{d}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{D}[\cdot])
  \end{align*}
  \end{minipage}
  \end{center}

  \vspace{1em}
  \begin{minipage}[t][2in][t]{\textwidth}
  \only<1>{
  \begin{figure}
    \centering
    \scalebox{.8}{
    \begin{tikzpicture}[node distance = 5em, auto]
    
    % states
    \node[circ, filled, label=90:$\boldsymbol{x}_{t-1}$] (x_prev) at (0,0) {};
    \node[minimum size=0em,right of=x_prev] (M) {};%{$\boldsymbol{M}$};
    \node[circ, filled, node distance=2em, label=90:$\boldsymbol{f}_t$, above of=M] (x_prevf) {};
    \node[circ, node distance=2em, label=-90:$\boldsymbol{d}_t$, below of=M] (bias) {};
    \node[circ, label=90:$\boldsymbol{x}_t$, right of=M] (xt) {};
    
    % observations    
    \node[circ, filled, label=270:$\boldsymbol{y}_t$, below of=xt, yshift=-2em] (yt) {};
    
    % paths between states
    \path[line] (x_prev) to node [above] {$\mathcal{M}$} (x_prevf);
    \path[line] (x_prev) to node [below] {$\mathcal{D}$} (bias);
    \path[line] (x_prevf) to node {} (xt);
    \path[line] (bias) to node [above,yshift=.25em] {$\boldsymbol{M}$} (xt);
    %\path[line, dotted] (x_prev) -- (M) node {} -- (xt);
    
    % paths between states and observations
    \path[line] (xt) to node [right] {$H_t$} (yt);

    % box around ensemble
    \draw[thick] (-.75,2) rectangle (4.75,-4);
    \node (T) at (4.5,-3.8) {$T_0$};

    \end{tikzpicture}
    }
  \end{figure}
  }

  \only<2->{Approximations that were made in the linear correction literature:}
  \begin{enumerate}
    \item<2,3-> Approximate tendencies via finite differences.
              \only<3->{
              \begin{align*}
                \boldsymbol{d}_t
                  \approx \frac{\boldsymbol{x}_t - \boldsymbol{f}_t}{\Delta t}
              \end{align*}
              }
             \only<3>{\alert<3>{Still need this assumption to estimate bias.}}
    \item<2,4> \only<2-4>{Use the analysis ensemble $\{ \boldsymbol{x}^a_{tn} \}_{n=1}^N$ to approximate $\{\boldsymbol{x}(t)_n\}_{n=1}^N$.}
              \only<4>{\\ \alert<4>{Approximation not needed if recognize that there is a hidden state.}}
    \item<2> Covariance localization
    \item<2> SVD decomposition of the covariance term in $L$
  \end{enumerate}

  \end{minipage}

  \note<1>{
  The linear correction to the dynamical model seemed like a promising line of work in correcting the model error, so my plan is to continue on this path by solving the implementation issues that the previous authors had.\\

  Here's the hierarchical model and its graphical model that we would ideally like to solve.\\

  x t is the hidden state of the system and y is its noisy version that we observe. x t minus Delta t is the previous state, which I treat as known. f is the forecast when we plug in the previous state into the incorrect model, d is the forecast from the model error term. Both f and d added together gives us the true state xt.\\

  My goal here is to estimate these hidden states and any other parameters by conditioning on one to estimate the other.
  }

  \note<2>{
  I'll go through the four approximations that were used in the linear correction literature and discuss how it applies to this framework.
  }

  \note<3>{
  We can eliminate the need for using the analysis ensemble in place of the truth ensemble by treating the problem as a hierarchical model with a hidden state.
  }

  \note<4>{
  We still have to use finite differences to estimate the model error term, but in this new formulation, we don't need to replace truth with the analysis.
  }
\end{frame}

\newcommand\mycommfont[1]{\scriptsize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}

\begin{frame}
  \frametitle{Naive implementation}

  \begin{minipage}{.55\textwidth}
  \begin{itemize}
    \item Initialize hidden state $\boldsymbol{x}_t$ with best guess: analysis ensemble, $\boldsymbol{x}_{tn}^a$.
    \item Iterate until convergence:
          \begin{enumerate}
            \item Calculate bias $\boldsymbol{d}_t$ via finite differences.
            \item Learn form of model error, $\mathcal{D}[\cdot]$. The dynamical model that generates $\boldsymbol{x}_t$ is now $\mathcal{M}[\cdot; \boldsymbol{\theta}] + \mathcal{D}[\cdot]$.
            \item Use new dynamical model to forecast $\boldsymbol{x}_t = \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}] + \mathcal{D}[\cdot])$.
            \item Update hidden state $\boldsymbol{x}_t$ with observation $\boldsymbol{y}_t$.
          \end{enumerate}
  \end{itemize}
  \end{minipage}\hfill
  \begin{minipage}{.43\textwidth}
    \begin{figure}
      \centering
      \scalebox{.8}{
      \begin{tikzpicture}[node distance = 5em, auto]
      
      % states
      \node[circ, filled, label=90:$\boldsymbol{x}_{t-1}$] (x_prev) at (0,0) {};
      \node[minimum size=0em,right of=x_prev] (M) {};%{$\boldsymbol{M}$};
      \node[circ, filled, node distance=2em, label=90:$\boldsymbol{f}_t$, above of=M] (x_prevf) {};
      \node[circ, node distance=2em, label=-90:$\boldsymbol{d}_t$, below of=M] (bias) {};
      \node[circ, label=90:$\boldsymbol{x}_t$, right of=M] (xt) {};
      
      % observations    
      \node[circ, filled, label=270:$\boldsymbol{y}_t$, below of=xt, yshift=-2em] (yt) {};
      
      % paths between states
      \path[line] (x_prev) to node [above] {$\mathcal{M}$} (x_prevf);
      \path[line] (x_prev) to node [below] {$\mathcal{D}$} (bias);
      \path[line] (x_prevf) to node {} (xt);
      \path[line] (bias) to node [above,yshift=.25em] {$\boldsymbol{M}$} (xt);
      %\path[line, dotted] (x_prev) -- (M) node {} -- (xt);
      
      % paths between states and observations
      \path[line] (xt) to node [right] {$H_t$} (yt);

      % box around ensemble
      \draw[thick] (-.75,2) rectangle (4.75,-4);
      \node (T) at (4.5,-3.8) {$T_0$};

      \end{tikzpicture}
      }
    \end{figure}
  \end{minipage}

  \vspace{1em}
  \visible<2->{This ``naive'' implementation can be refined.}

  \note{
  Here's a naive implementation of this hierarchical model.

  \begin{list}
  {\arabic{itemcounter}.}
  {\usecounter{itemcounter}\leftmargin=-1em\parskip=0em\parsep=0em\itemsep=0em\rightmargin=-1em}
    \item First, we initialize the truth ensemble with the analysis ensemble...
    \item ...giving us a good first guess of the model error, using finite differences.
    \item With the model error, we can solve for the parameters of the linear correction term, giving a new form for script D.
    \item Now the true dynamical model should be our (incorrect) dynamical model plus this new model error term we just found. We integrate this model forward to give us a forecast that is closer to the truth.
    \item Then, we update this forecast with its noisy version, giving us a new estimate of the truth.
    \item We repeat this process over and over again until we get convergence.
  \end{list}
  This can either be done in an EM setting or with a Monte Carlo sampler.

  The reason why I'm calling it naive is because it seems like some refinements can be made to this algorithm. In particular, I want to find numerical ways that avoid reintegrating this model forward, since that can be computationally expensive.
  }
\end{frame}

\begin{frame}
  \frametitle{Generative model for the linear correction method}
  Problem formulation:

  \vspace{-1em}
  \begin{center}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \dot{\boldsymbol{x}}(t) 
      &= \mathcal{M}[\boldsymbol{x}(t); \boldsymbol{\theta}] + \mathcal{D}[\boldsymbol{x}(t)] \\
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \boldsymbol{f}_t, \boldsymbol{d}_t) \\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  \end{minipage}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \mathcal{D}[\boldsymbol{x}(t)]
      &= L \boldsymbol{x}(t) + \boldsymbol{b} + \boldsymbol{\varepsilon}(t) \\
    \boldsymbol{f}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}]) \\
    \boldsymbol{d}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{D}[\cdot])
  \end{align*}
  \end{minipage}
  \end{center}

  \vspace{1em}
  \begin{minipage}[t][2.5in][t]{\textwidth}
  Approximations that were made in the linear correction literature:
  \begin{enumerate}
    \item Approximate tendencies via finite differences.
    \item Use the analysis ensemble $\{ \boldsymbol{x}^a_{tn} \}_{n=1}^N$ to approximate $\{\boldsymbol{x}(t)_n\}_{n=1}^N$.
    \item Covariance localization\only<1>{: \alert<1>{problem-specific}}
    \item<2-> SVD decomposition of the covariance term in $L$
              \only<2>{\alert<2>{\\There is no probabilistic interpretation of SVD. But, there is a probabilistic interpretation of canonical correlation analysis (CCA).}}
  \end{enumerate}
  \end{minipage}

  \note<1>{
  Covariance localization will be application specific, so I won't discuss this here.
  }
  
  \note<2>{
  The SVD decomposition was nice because it made the problem more computationally friendly. But, SVD doesn't have a probabilistic interpretation, making it difficult to use in this hierarchical setting. \\

  It turns out that the SVD of the covariance and canonical correlation analysis (CCA), although not quite the same, was shown to have some similarities. Furthemore, very recently some work has been developed on the probabilistic interpretation of CCA, which I will review in the next two slides.

  \hl{review this}
  }
\end{frame}

\begin{frame}
  \frametitle{Probabilistic interpretation of canonical correlation analysis}
  {\bf Inter-battery factor analysis} is a generative model for canonical correlation analysis (CCA) \citep{Klami2013}. For $m \in \{1,2\}$,
  \begin{align*}
    \boldsymbol{z}
      &\sim \mathcal{N}(\boldsymbol{0}, I) \\
    \boldsymbol{z}_m
      &\sim \mathcal{N}(\boldsymbol{0}, I) \\
    \boldsymbol{x}_m
      &\sim \mathcal{N}(\boldsymbol{\mu}_m + A_m \boldsymbol{z} + B_m \boldsymbol{z}_m, \Sigma_m)
  \end{align*}

  \visible<2->{
  After integrating out $\boldsymbol{z}_1$ and $\boldsymbol{z}_2$, 
  \begin{align*}
    \boldsymbol{x}_m &\sim \mathcal{N}(\boldsymbol{\mu}_m + A_m \boldsymbol{z}, \underbrace{B_m B_m^T + \Sigma_m}_{\normalfont \Psi_m}).
  \end{align*}
  The MLE of this model is equivalent to CCA up to a rotation (proved by \cite{Browne1979} and \cite{Bach2005}).
  }

  \note<1>{
  The inter-battery factor analysis model was shown to be a generative model for CCA.  Here is the model. I just want to note that the notation in these slides have no relationship with the other variables seen before, so please bear with me for these next few slides.\\

  Let there be two datasets, x1 and x2.
  \begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
    \item Let z be a latent variable capturing the variation common to both datasets.
    \item zm is called a view-specific variable, which captures the remaining variation in the datasets not captured by z
    \item A and B are linear maps from the latent spaces to the observation space.
    \item Sigma represents the error not captured by the latent variables and is usually diagonal.
  \end{itemize}
  }

  \note<2>{
  When the view specific latent variables, z1 and z2, are integrated out of the model, you get this model. The mapping B is absorbed into the Psi, the covariance of x. The MLEs of this particular model has been proven to be equivalent to CCA by both Browne and Francis Bach and Mike, here.
  }

\end{frame}



\begin{frame}<beamer:0|handout:0>[noframenumbering]
  \frametitle{Bayesian CCA}
  The generative model for CCA can be made into a Bayesian model by putting priors on the parameters.
  \begin{align*}
    \boldsymbol{x}_m &\sim \mathcal{N}(\boldsymbol{\mu}_m + A_m \boldsymbol{z}, \underbrace{B_m B_m^T + \Sigma_m}_{\normalfont \Psi_m}).
  \end{align*}

  {\bf Choice of priors:}
  \begin{itemize}
    \item<1-> Putting an inverse-Wischart prior on $\Psi_m$ \citep{Klami2008,Wang2007} makes the Bayesian model not very scalable.
    \item<2-> Instead, put prior on columns of $B_m$ to make the Bayesian model scalable while maintaining the complicated structure in $\Psi_m$ \citep{Klami2013}.
  \end{itemize}
  
  \note<1>{
  One line of research made this into a Bayesian model by putting priors on Psi and the columns of A.  In particular, they put an inverse-Wischart prior on Psi.  The inverse-Wischart makes the Bayesian model not very scalable, so Klami and his colleagues decided to work with the original IBFA model instead.\\

  They first started off by reparametrizing the model, as shown here.  The reparam consists of concatenating the latent variables together into y, giving this new mapping W between the latent space, y, and the data, x. Sigma is assumed to be diagonal.\\

  This reparametrization essentially moves the interesting parts contained in B, which was absorbed into Psi before, to the columns in W, which is faster to estimate and is more scalable than using an inverse wischart prior.
  }
\end{frame}

\begin{frame}
  \frametitle{CCA is a low-dimensional linear model}
  It can be shown that CCA is a linear model! In particular, with this form:
  \begin{align*}
    \boldsymbol{x}_1 = L \tilde{\boldsymbol{x}}_2 + \boldsymbol{b} + \boldsymbol{\epsilon},
  \end{align*}
  where
  \begin{align*}
    \tilde{\boldsymbol{x}}_2 
      &= \boldsymbol{x}_2 - \boldsymbol{\mu}_2 \\
    L 
      &= A_1 A_2^T (\Sigma_2 + W_2 W_2^T)^{-1} \\
    \boldsymbol{b} 
      &= \boldsymbol{\mu}_1 - L \boldsymbol{\mu}_2 \\
    \boldsymbol{\epsilon} 
      &\sim \normal(\boldsymbol{0}, \Sigma_1 + W_1 W_2^T + L A_2 A_1^T)
  \end{align*}
  CCA is a low-dimensional linear model for learning the model error term, $\mathcal{D}[\boldsymbol{x}(t)]$.

  \note{
  Using MVN conditioning techniques, I can show that CCA is a low-dimensional linear model. \\

  There's an intuitive interpretation of the linear map: CCA essentially extracts the information in one dataset and remaps it to the other data set through the latent space that is common to both datasets, ignoring the variation that is not common to both datasets.
  }

\end{frame}

\begin{frame}
  \frametitle{Generative model for the linear correction method}
  Problem formulation:

  \vspace{-1em}
  \begin{center}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \dot{\boldsymbol{x}}(t) 
      &= \mathcal{M}[\boldsymbol{x}(t); \boldsymbol{\theta}] + \mathcal{D}[\boldsymbol{x}(t)] \\
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \boldsymbol{f}_t, \boldsymbol{d}_t) \\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  \end{minipage}
  \begin{minipage}{.48\textwidth}
  \begin{align*}
    \mathcal{D}[\boldsymbol{x}(t)]
      &= L \boldsymbol{x}(t) + \boldsymbol{b} + \boldsymbol{\varepsilon}(t) \\
    \boldsymbol{f}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}]) \\
    \boldsymbol{d}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{D}[\cdot])
  \end{align*}
  \end{minipage}
  \end{center}

  \vspace{1em}
  \begin{minipage}[t][2.5in][t]{\textwidth}
  Approximations that were made in the linear correction literature:
  \begin{enumerate}
    \item \alert<2->{Approximate tendencies via finite differences.}
    \item Use the analysis ensemble $\{ \boldsymbol{x}^a_{tn} \}_{n=1}^N$ to approximate $\{\boldsymbol{x}(t)_n\}_{n=1}^N$.
    \item Covariance localization
    \item \alert<2->{SVD decomposition of the covariance term in $L$}
  \end{enumerate}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Next steps}

  {\bf Immediate next steps}
  \begin{enumerate}
    \item Implement the naive implementation to estimate the linear correction term to the dynamical model in an offline manner. Can we accurately identify the bias?
  \end{enumerate}
  ~\\

  {\bf Follow up analysis}
  \begin{enumerate}
    \item Speed up naive implementation.
          \begin{itemize}
            \item Find numerical approximations to avoid reintegration.
            \item Incorporate method into the ensemble Kalman filter.
          \end{itemize}
    \item SVD/CCA was not the only way to make the method computationally friendly. Other forms for $L$?
    \item Other forms for the bias? Spatial correlation?
    \item Apply to a realistic model: Weather Research and Forecasting (WRF) Model.
  \end{enumerate}

  \note{
  \begin{enumerate}
    \item My plan is to implement the naive algorithm and determine if we can accurately identify the true bias.
    \item If I determine that I can, then I will attempt to make the slow parts faster, such as finding numerical approximations that avoid the reintegration step or incorporate the method into the sequential Bayesian update setting.
    \item CCA is not the only way to make the Lx operation computationally friendly. For example, I can impose the assumption that only the bias depends only on ``closeby'' points in the state space or perhaps impose some spatial correlation to the bias.
    \item If that all works out, I plan to then apply our methods to a more realistic problem, which will have its own complications.
  \end{enumerate}

  
  }

\end{frame}

