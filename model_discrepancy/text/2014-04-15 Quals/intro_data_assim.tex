%!TEX root = quals.tex
\synctex=1

\begin{frame}
  \frametitle{Data assimilation (DA)}
  A framework to incorporate observations into a physical model as the observations come online.  It allows us to learn and predict the true state of the system.
  \begin{align*}
    \dot{\boldsymbol{x}}(t)
      &= \mathcal{M}[\boldsymbol{x}(t); \boldsymbol{\theta}] \\
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}]) \\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  {\bf Goal}: Learn the hidden state, $\boldsymbol{x}_t$. \only<2->{The framework consists of two main recursive steps:}
  \begin{minipage}[t][2in][t]{\textwidth}
  \only<1>{
  \begin{figure}
    \centering
    \vspace{-.5em}
    \scalebox{.8}{
    \begin{tikzpicture}[node distance = 5em, auto]
    
    % states
    \node[circ, filled, label=90:$\boldsymbol{x}_{0}$] (x0) at (0,0) {};
    \node[circ, label=90:$\boldsymbol{x}_{1}$, right of=x0] (x1) {};
    \node[minimum size=2.5em, right of=x1] (xother) {...};
    \node[circ, label=90:$\boldsymbol{x}_{t}$, right of=xother] (xt) {};
    \node[circ, label=90:$\boldsymbol{x}_{t+1}$, right of=xt] (xt1) {};
    \node[minimum size=2.5em, right of=xt1] (xother2) {...};
    
    % observations    
    \node[circ, filled, label=270:$\boldsymbol{y}_1$, below of=x1] (y1) {};
    \node[circ, filled, label=270:$\boldsymbol{y}_t$, below of=xt] (yt) {};
    \node[circ, label=270:$\boldsymbol{y}_{t+1}$, below of=xt1] (yt1) {};
    
    % paths between states
    \path[line] (x0) to node [above] {$\boldsymbol{M}$} (x1);
    \path[line] (x1) to node [above] {$\boldsymbol{M}$} (xother);
    \path[line] (xother) to node [above] {$\boldsymbol{M}$} (xt);
    \path[line] (xt) to node [above] {$\boldsymbol{M}$} (xt1);
    \path[line] (xt1) to node [above] {$\boldsymbol{M}$} (xother2);
    
    % paths between states and observations
    \path[line] (x1) to node [right] {$H_1$} (y1);
    \path[line] (xt) to node [right] {$H_t$} (yt);
    \path[line] (xt1) to node [right] {$H_{t+1}$} (yt1);

    \end{tikzpicture}
    }
  \end{figure}
  }
  \only<2->{

  \begin{center}
  \begin{tabular}[c]{rl}
    Forecast & $p(\boldsymbol{x}_{t} \,|\, \boldsymbol{y}_{1:t-1})$ \\
    Update / analysis & $p(\boldsymbol{x}_{t} \,|\, \boldsymbol{y}_{1:t})$
  \end{tabular}
  \end{center}
  Many ways to do this!
  }
  \end{minipage}
    
  \note<1>{
  Now, I will review data assimilation. \\
  
  Data assimilation is a framework that allows us to reconcile the differences we see between the physical model and what we actually observe in real life and, in particular, in a setting where we are constantly getting new observations. \\

  Let x be the true state of the system, y be some noisy version of the true state. The state of the system is propagated forward in time through a dynamical model, kind of like the Lorenz system that we saw previously. A graphical model of the process is depicted here. The x's form a Markov chain and the observations are conditional given the state.\\
  
  The goal of DA is to learn the hidden state x given all the data we have.
  }

  \note<2>{
  The DA algorithm has two main steps:
  \begin{enumerate}
    \item a forecast step that uses the dynamical model M to predict what the next state at time t will be given all the data we have up to time t-1
    \item and an analysis step, which corrects the forecast given the observed value at time t. The analysis step is basically a weighted average between the observation and the forecast.
  \end{enumerate}
  From a statistician's perspective, the forecast is just a prior of the state at time t and the analysis is the posterior.
  
  There are many methods to do assimilation... you can pick your favorite among EnKF, particle filter, variational methods, etc.  I will only review the EnKF in this talk, since it's one of the more popular methods used in the atmospheric science community.
  }
\end{frame}

\begin{frame}
  \frametitle{Kalman filter (KF)}
  Problem formulation \citep{Kalman1960}:
  \begin{align*}
    \boldsymbol{x}_t
      &= A_t \boldsymbol{x}_{t-1} + \boldsymbol{\eta}_t \\
    \boldsymbol{y}_t
      &= H_t\boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  with $A_t, H_t$ known;
  $\boldsymbol{x}_0 \sim \normal(\mu_0^a, \Sigma_0^a)$;
  $\boldsymbol{\eta}_t \sim \normal(\boldsymbol{0}, \Sigma_t^\eta)$;
  $\boldsymbol{\epsilon}_t \sim \normal(\boldsymbol{0}, \Sigma_t^\epsilon)$;
  and $\{\boldsymbol{x}_0, \boldsymbol{\epsilon}_1,...,\boldsymbol{\epsilon}_T, \boldsymbol{\eta}_1,...,\boldsymbol{\eta}_T \}$ are mutually independent.
  \\~\\

  \visible<2->{
  There are closed form solutions for the forecast and analysis distributions:
  \begin{align*}
    \boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t-1} 
      &\sim \normal(\boldsymbol{\mu}_t^f, \Sigma_t^f) \\
    \boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t} 
      &\sim \normal(\boldsymbol{\mu}_t^a, \Sigma_t^a)
  \end{align*}
  }

  \note<1>{
  I'm going to start off describing data assimilation in an easier setting, where the forecasting model is linear and everything is Gaussian; this is also known as the Kalman filter.
  \begin{itemize}
    \item Let x be a linear transformation of the previous state with some normal random error. A is the linear mapping between x and its previous state and it is known.
    \item Let y be a linear mapping of the true state with some normal random error. H is the linear mapping between x and the observed value and it is also known. An example of H is where satellite or radar data are not observed all spatial locations, or that there is some averaging in spatial locations.
    \item The random errors here are all normally distributed with known covariances. They are also mutually independent with each other and with the initial condition, x naught.
  \end{itemize}
  }

  \note<2>{
  In this situation, where everything is linear and Gaussian, we can get closed form solutions to the forecast and analysis distributions. I won't give them here, but they can be easily derived from the affine transformation properties and conditioning techniques (Schur complements) of the MVN.
  }
\end{frame}

\begin{frame}
  \frametitle{Modifications to the Kalman filter for nonlinear models}

  Physical models are hardly linear!
  \begin{align*}
    \boldsymbol{x}_t
      &= \boldsymbol{M}(\boldsymbol{x}_{t-1}; \mathcal{M}[\cdot; \boldsymbol{\theta}]) \\
    \boldsymbol{y}_t
      &= H_t \boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t
  \end{align*}
  
  How to deal with the nonlinearities:
  \begin{itemize}
    \item<2-> Extended Kalman filter (EKF) \citep{Epstein1969}: Linearize $\boldsymbol{M}$.
    \item<3-> Ensemble Kalman filter (EnKF) \citep{Evensen1994,Houtekamer1998}: Use a Monte Carlo sample.
              \begin{itemize}
                \item The collection of samples is called the ``ensemble''.
                \item Each sample is called an ``ensemble member''.
              \end{itemize}
  \end{itemize}
  %\visible<4->{The update step of both of these methods assume that the forecast distribution is Gaussian.}

  \note<1>{
  Realistically, the dynamical models that are used are never linear! The KF algorithm were modified in a couple of ways to deal with this nonlinearity.
  }

  \note<2>{
  Originally, the Extended Kalman filter was developed to tackle this problem. The EKF linearizes M by using the Jacobian and Hessian at the previous state to estimate the first and second moments of the forecast distribution. This is then used to obtain the analysis distribution. The linearization was shown to not work well in many applications due to \hl{an unbounded error variance growth from neglecting the higher moments} (discussed in Evensen (1994,2003)). \\

  NOTE TO SELF: Not the same as a Laplace approximation! Laplace approximation is used for approximating a pdf with a Normal pdf by doing a Taylor's expansion around its maxima. \\
  }

  \note<3>{
  So, Evensen developed the Ensemble Kalman filter, which basically uses a collection of random samples to estimate the forecast distribution. We statisticians know this as using a Monte Carlo sample.\\

  I just want to mention here the collection of random samples is called the ``ensemble'' and each sample is called an ``ensemble member''.
  }

\end{frame}

\begin{frame}
  \frametitle{EnKF on Lorenz 1963}
  \begin{center}
    \only<1>{\includegraphics[width=\textwidth,height=.9\textheight,keepaspectratio]{lorenz63_true.png}}
    \only<2>{\includegraphics[width=\textwidth,height=.9\textheight,keepaspectratio]{lorenz63_obs.png}}
    \only<3>{\includegraphics[width=\textwidth,height=.9\textheight,keepaspectratio]{EnKF_right/image1.png}}
    \only<4>{\animateornot{EnKF_right/image}{1}{10}{width=\textwidth}}
    \only<5>{\animateornot{EnKF_right/image}{10}{301}{width=\textwidth}}
  \end{center}

  \note<1>{
  Let's see how the EnKF works on the Lorenz system. \\

  Here's the true trajectory of the system.
  }

  \note<2>{
  Suppose that we can measure the truth with some random error, shown here with the transparent black circles. \\

  For clarity, I will take away all of these circles and show the evolution of EnKF over time. But, remember that they are there.
  }

  \note<3>{
  Here's the start of the system, where I have randomly sampled from the known distribution of the starting values. I started the system at an unreasonable spot to illustrate how data assimilation can get us back on the right trajectory.\\

  On the left, the open square represents the where we are in this trajectory at time t. Each purple dot is a Monte Carlo sample of the forecast ensemble and the big f represents the mean of the small purple dots.\\

  On the topright, I show the x direction of the state. Green is truth, the open circle is the observed value. You'll see some gray lines when I start the animation: those are the forecast trajectories of each Monte Carlo sample over time. \\

  On the bottomleft, I show the bias and its 90\% uncertainty interval. \\
  }

  \note<4>{
  As you can see, incorporating the observations into the dynamical system helped us get closer to the true trajectory and got rid of the bad starting values.
  }
\end{frame}

