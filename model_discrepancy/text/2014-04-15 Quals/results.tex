%!TEX root = quals.tex
\synctex=1

\begin{frame}{The toy model: Lorenz 2005}
  \begin{minipage}[t]{.48\textwidth}
    {\bf ``Reality''}: Model 3
    \vspace{1em}

    \only<1>{
      \animateornot{model3_3d/image}{1}{721}{width=\textwidth,loop}
    }
    \only<2->{
      \includegraphics[width=\textwidth]{model3_3d/image669.png}
    }
  \end{minipage}\hfill
  \begin{minipage}[t]{.48\textwidth}
    \only<2->{
      {\bf Physical model}: Model 2
      \vspace{1em}

      \animateornot{model2_3d/image}{1}{721}{width=\textwidth,loop}
    }
  \end{minipage}

  \note<1>{
  I wanted to see how well these solutions work in practice. In particular, in setting where I know truth. \\

  The Lorenz system that we saw before was too low-dimensional of a toy model. I wanted a toy model that more closely reflects realistic systems. \\

  Fortunately, Lorenz created this particular model in 2005 specifically for the data assimilation community to use as a toy model and test out ideas on model error. The appeal for scientists (and me) of this particular system is that it's simple and fast enough to run computational experiments while preserving many features of a more complicated model that has chaos and structure at multiple spatial scales.\\

  Let's pretend that this Lorenz 2005 model on the left is how weather varies on a latitude band. The circle is the latitude band and the z direction and color represents the value of a variable, such as temperature or pressure.
  }
  
  \note<2>{
  But, we can only get an approximation of the dynamics of our weather system, as seen on the right. Comparing the two, you'll notice that the physical model can't capture the small variations of the real weather system.
  }
\end{frame}

\begin{frame}
  \frametitle{Lorenz 2005 equations}
  \begin{minipage}[t]{.48\textwidth}
  {\bf ``Reality''}: Model 3
  \begin{align*}
    \frac{dz_n}{dt} 
      &= [\boldsymbol{x}, \boldsymbol{x}]_{K,n} + b^2 [\boldsymbol{y}, \boldsymbol{y}]_{1,n}  \\
      &\quad + c [\boldsymbol{y}, \boldsymbol{x}]_{1,n} - x_n - b y_n + F\\
      %&\quad - b y_n + F \\
    x_n 
      &= \sumprime_{i=-I}^{I} (\alpha - \beta|I|) z_{n+i} \\
    y_n 
      &= z_n - x_n \\\\
    \boldsymbol{\theta} &= (N,K,F,I,b,c)
  \end{align*}
  \end{minipage}\hfill
  \begin{minipage}[t]{.48\textwidth}
  {\bf Physical model}: Model 2
  \begin{align*}
    \frac{dx_n}{dt} &= [\boldsymbol{x}, \boldsymbol{x}]_{K,n} - x_n + F \\\\
    \boldsymbol{\theta} &= (N,K,F)
  \end{align*}
  \end{minipage}

  %\begin{align*}
  %  [x,y]_{K,n} 
  %    &= \frac{1}{K^2}\sumprime_{j=-J}^J \sumprime_{i=-J}^J -x_{n-2K-i} y_{n-K-j} + x_{n-K+j-i} y_{n+K+j} \\
  %  \sumprime_{n = 1}^N x_n 
  %    &= \begin{cases}
  %      \sum_{n = 1}^N x_n & K \text{ even} \\
  %      \frac{x_1}{2} + \sum_{n = 2}^{N-1} x_n + \frac{x_N}{2} & K \text{ odd} \\
  %    \end{cases}
  %\end{align*}

  \note{
  Here are the set of equations that describe the Lorenz 2005 system. The details are not important. But, I just want to show you that it's not very different from the 1963 system that you've seen in the previous slides.\\

  On the right is our script M. Bold M is the discrete approximation of this system. \\
  }
\end{frame}

\begin{frame}
  \frametitle{Solutions applied to the Lorenz 2005 system}
  \litreview{}{}{}
  
  \note{
  I will now show how these three lines of literature applied to the Lorenz 2005 system.
  }  
\end{frame}

\begin{frame}
  \frametitle{Lorenz 2005 and variance inflation}
  Results from:
  \begin{enumerate}
    \item<1-> Ensemble Kalman filter with non-adaptive inflation, $\lambda = 2$
    \item<2-> EnKF with non-adaptive inflation, $\lambda = 10$
    \item<3-> EnKF with adaptive inflation
  \end{enumerate}

  \begin{minipage}[t][2in][t]{\textwidth}
    \only<1>{\includegraphics[width=\textwidth]{inf2.pdf}}
    \only<2>{\includegraphics[width=\textwidth]{inf10.pdf}}
    \only<3>{\includegraphics[width=\textwidth]{adinf.pdf}}
  \end{minipage}

  \note<1>{
  This graph shows the trajectory for one point in that circle that we saw before. Green is truth. Open circles are the noisy version of the truth. Each gray line is a forecast trajectory from the ensemble. \\

  Here are the results from doing EnKF with non-adaptive inflation with an arbitrary value of 2.\\
  }

  \note<2>{
  ...and a value of 10.\\

  We get closer to the truth and cover it sometimes, but there is still some bias.
  }

  \note<3>{
  ...and here are the results from the adaptive variance inflation. \\

  The forecast ensemble captures the truth better than the non-adaptive variance inflation at the expense of its precision. The inflation value is increased by a lot to cover the bias that the forecasts couldn't capture.
  }
\end{frame}

\begin{frame}
  \frametitle{Lorenz 2005 and sequentially correlated bias}
  Results from:
  \begin{enumerate}
    \item<1-> EnKF with non-adaptive inflation, $\lambda = 2$
    \item<2-> Ditto + sequentially correlated bias
  \end{enumerate}
  \begin{minipage}[t][2in][t]{\textwidth}
    \only<1>{\includegraphics[width=\textwidth]{inf2.pdf}}
    \only<2>{\includegraphics[width=\textwidth]{constantbias2.pdf}}
  \end{minipage}

  \note<1>{
  Let's see if we can do better with a sequentially correlated bias.\\

  Again, here are the results of the EnKF with non-adaptive inflation of 2.
  }

  \note<2>{
  And, here it is when we sequentially predict the bias and add it to the forecast. \\

  That's not your imagination. You're seeing that right. Nothing really changed here. \\

  This illustrates the problem with the form of the bias used in the sequentially correlated bias literature. Recall that the prior put on the bias was that it is exactly the same as the previous bias with some random error. The form of the bias needs more structure to predict the bias well.
  }
\end{frame}


\begin{frame}
  \frametitle{Lorenz 2005 and linear correction}

  Results from:
  \begin{enumerate}
    \item<1-> Ensemble Kalman filter with non-adaptive inflation, $\lambda = 2$
    \item<2-> Ditto + linear correction to the dynamical model
    %\item<3-> Thought experiment: What if we accounted for the uncertainty of the estimates of $L$ and $b$? Can we do better?
    \item<3-> Thought experiment: What if we had the true bias? Does the method work?
  \end{enumerate}

  \begin{minipage}[t][2in][t]{\textwidth}
    \only<1>{\includegraphics[width=\textwidth]{inf2_line.pdf}}
    \only<2>{\includegraphics[width=\textwidth]{danforth.pdf}}
    %\only<3>{\includegraphics[width=\textwidth]{bibfa.pdf}}
    \only<3->{\includegraphics[width=\textwidth]{bibfa_truth.pdf}}
  \end{minipage}

  \note<1>{
  So, let's see how it performs when the form on the bias is more structured, in particular linear in the dynamical model.\\

  Here is the EnKF again with an arbitrary inflation value of 2. I use an equivalent of 5 years of data for training the linear correction method, indicated by the vertical black line. You're seeing the tail end of the data here on this graph. \\

  The training data has not seen everything to the right of the vertical line. The right side is about half a year of data.
  }

  \note<2>{
  Here's the linear correction applied to the right end of the graph.\\

  It does better in some places, but does worse in others.\\

  So, this made me wonder... does the formulation of the method work if we had the true bias? Well, since I know truth in my toy model, I can actually answer this question.
  }

  \note<3>{
  Here it is.. if we knew the true bias. The linear correction method does predict the bias pretty well. \\

  What I can conclude here is that the linear correction to the dynamical model does work well, but it just has implementation issues.
  }
\end{frame}

\begin{frame}
  \frametitle{What happened with the linear correction?}
  Recall that these two approximations were made:
  \begin{enumerate}
    \item Approximate tendencies via finite differences:
    \begin{align*}
        \dot{\boldsymbol{x}}(t)_n
            \approx \frac{\boldsymbol{x}_{t + \Delta t, n} - \boldsymbol{x}_{tn}}{\Delta t}
        \hspace{.25in} \text{and} \hspace{.25in}
        \mathcal{M}[\boldsymbol{x}(t)_n; \boldsymbol{\theta}]
            \approx \frac{\boldsymbol{x}^f_{t + \Delta t,n} - \boldsymbol{x}_{tn}}{\Delta t}.
    \end{align*}
    This approximation requires that $\Delta t$ to be short enough to resolve the linear growth of the errors.

    \item \alert<1->{Use the analysis ensemble $\{ \boldsymbol{x}^a_{tn} \}_{n=1}^N$ to approximate $\{\boldsymbol{x}(t)_n\}_{n=1}^N$.}
  \end{enumerate}

  \visible<1->{
  \alert<1->{The analysis ensemble was generated using the forecast ensemble from the BIASED model! Therefore, the estimate of the model error is also biased.}
  }

  \note<1>{
  In particular, there was a problem with the second approximation of using the analysis ensemble as an estimate of the truth ensemble. Since the analysis ensemble was generated using forecasts from the biased model, the analysis ensemble is also biased and therefore the estimate of the model error is biased. This can easily be solved by recognizing that there is a hierarchy to the problem, where there's a hidden state and that we observe some noisy version of it.
  }
\end{frame}

