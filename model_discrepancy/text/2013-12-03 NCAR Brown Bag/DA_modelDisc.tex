\documentclass[xcolor=svgnames]{beamer}

% Load packages ---------------------------------------------------------------
\usepackage{pgfpages}
\usepackage{amsmath,amsfonts,amsthm,amssymb}
\usepackage{color}
\usepackage{ulem} % for \sout (strikeout)
\usepackage{tikz}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{animate}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage[style=authoryear]{biblatex}
\addbibresource{../refs.bib}

% tikz stuffs ------------------------------------------------------------------
\usetikzlibrary{shapes,arrows,fit,calc}
\tikzstyle{line} = [draw, thick, ->, >=stealth']
\tikzstyle{circ} = [draw, circle, thick, node distance=5em, minimum size=2.5em]
\tikzstyle{filled} = [fill=gray]
\newcommand{\tikzmark}[1]{%
  \tikz[overlay,remember picture,baseline] \node [anchor=base] (#1) {};}

% Beamer options ---------------------------------------------------------------
%\usetheme{Malmoe}
%\usetheme{split}
%\usecolortheme[named=DarkBlue]{structure}
\usetheme{Boadilla}
\usecolortheme{whale}

\useoutertheme{infolines}
\beamertemplatenavigationsymbolsempty
%\setbeameroption{show notes on second screen=bottom}

% Fix note margins. https://bitbucket.org/rivanvx/beamer/issue/57/margins-set-up-wrong-in-note-itemize
\let\orignote\note
\renewcommand{\note}[1]{\orignote{
\begin{minipage}{\textwidth}
	#1
\end{minipage}
}}

\setbeamertemplate{note page}{
  \vspace{.1in}
  \insertnote
}


% new commands -----------------------------------------------------------------
\newcommand{\hl}[1]{\colorbox{yellow}{#1}}
\definecolor{lightgray}{gray}{0.75}
\newcommand{\g}[1]{\textcolor{lightgray}{#1}}
\def\newblock{\hskip .11em plus .33em minus .07em}
\newcommand{\themecolor}{DarkRed}
\defbeamertemplate{itemize item}{checkbox}{\Square}
\defbeamertemplate{itemize item}{checked}{\Square\llap\CheckmarkBold}
\newcommand*{\Scale}[2][4]{\scalebox{#1}{$#2$}}%

% end new commands

% math stuffs ------------------------------------------------------------------
\DeclareMathOperator{\pois}{Pois}
\DeclareMathOperator{\bivpois}{BivPois}
\DeclareMathOperator{\cov}{\mathbb{C}ov}
\DeclareMathOperator{\prob}{\mathbb{P}}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\sumprime}{\sum'}


% title stuffs ------------------------------------------------------------------
\title[Model discrepancy in DA]{Incorporating model discrepancy into a data assimilation framework}
\author[Linda N. Tran, Cari G. Kaufman]{Linda N. Tran \\ Cari G. Kaufman}
\institute[]{Statistics, UC Berkeley}
\date[NCAR Brown Bag, Dec 2013]{NCAR Brown Bag \\ Tuesday, December 3, 2013}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Data assimilation (DA)}
  A framework to incorporate observations into a physical model as the observations come online.  It allows us to learn and predict the true state of the system.
  \begin{align*}
    \underset{\text{obs}}{\boldsymbol{y}_t} 
      &= H_t\boldsymbol{x}_t
        + \underset{\substack{\text{measurement}\\ \text{error}}}{\boldsymbol{\epsilon}_t} \\
    \boldsymbol{x}_t
      &= \boldsymbol{r}_t(\boldsymbol{x}_{t-1}; \boldsymbol{\theta})
  \end{align*}
  
  The framework consists of two main recursive steps:
  \begin{itemize}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item Forecast: $\boldsymbol{x}_{t} \,|\, \boldsymbol{y}_{1:t-1}$
    \item Update / analysis: $\boldsymbol{x}_{t} \,|\, \boldsymbol{y}_{1:t}$
  \end{itemize}  
  
  \note{
  \footnotesize
  We often represent our understanding of the dynamics of a system through a physical model.  DA is a framework that allows us to reconcile the differences we see between the physical model and what we actually observe in real life and, in particular, in a setting where we are constantly getting new observations. \\
  \hfill\\
  Let x be the true state of the system, y be some noisy version of the true state, and r represent the physical model that propagates the state of the system to the next time step. \\
  \hfill \\
  The DA algorithm has two main steps:
  \begin{enumerate}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item a forecast step that uses the physical model r to predict what the the next state at time t will be given all the data we have up to time t-1
    \item and an analysis step, which corrects the forecast given the observed value at time t. The analysis step is basically a weighted average between the observation and the forecast, or what the physical model thinks the true state is.
  \end{enumerate}
  Typically you will see this algorithm used in weather forecasting.  There are many methods to does assimilation... you can pick your favorite among EnKF, particle filter, variational assimilation, etc.  We won't be focusing on a particular method, but focus on the general data assimilation framework instead.
  }
\end{frame}

\begin{frame}
  \setbeamercolor{alerted text}{fg=blue}
  \setcounter{algocf}{0}
  
  \frametitle{Data assimilation algorithm}
  \begin{center}
  \begin{minipage}{.48\textwidth}
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
    \alert<2>{\Input{$x_0^a$}}
    \Output{$x_{1:T}^f, x_{1:T}^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      \alert<3,6,9>{$x_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;}
      \alert<4,7>{collect $y_t$\;}
      \alert<5,8>{$x_t^a \leftarrow$ \Update($x_t^f, y_t$)\;}
    }
    \caption{Data assimilation}
  \end{algorithm}
  \end{minipage}
  \end{center}
  
  \begin{figure}[hb]
    \centering
    \scalebox{.8}{
    \begin{tikzpicture}[node distance = 5em, auto]
    
    \tikzstyle{hline} = [draw=blue, ultra thick, ->, >=stealth', text=blue, font=\LARGE]
    \tikzstyle{hcirc} = [draw=blue, circle, ultra thick, node distance=5em, minimum size=2.5em, text=blue]
    \tikzstyle{hfilled} = [fill=blue!50!gray]
    
    % states
    \visible<1,4->{\node[circ, filled, label=90:$\boldsymbol{x}_0$] (x0) at (0,0) {};}
    \visible<1-2,4,7->{\node[circ, label=90:$\boldsymbol{x}_1$, right of=x0] (x1) {};}
    \visible<1-5,7>{\node[circ, label=90:$\boldsymbol{x}_2$, right of=x1] (x2) {};}
    \visible<1-8>{\node[minimum size=2.5em, right of=x2] (xother) {...};}
    \node[circ, label=90:$\boldsymbol{x}_t$, right of=xother] (xt) {};
    \node[circ, label=90:$\boldsymbol{x}_{t+1}$, right of=xt] (xt1) {};
    \node[minimum size=2.5em, right of=xt1] (xother2) {...};
    
    \visible<2-3>{\node[hcirc, hfilled, label=90:\textcolor{blue}{\LARGE $\boldsymbol{x}_0$}] (x0) at (0,0) {};}
    \visible<3,5-6>{\node[hcirc, label=90:\textcolor{blue}{\LARGE $\boldsymbol{x}_1$}, right of=x0] (x1) {};}
    \visible<6,8-9>{\node[hcirc, label=90:\textcolor{blue}{\LARGE $\boldsymbol{x}_2$}, right of=x1] (x2) {};}
    \visible<9>{\node[minimum size=2.5em, right of=x2] (xother) {\color{blue} \LARGE ...};}

    % observations    
    \visible<1-3>{\node[circ, label=270:$\boldsymbol{y}_1$, below of=x1] (y1) {};}
    \visible<1-6>{\node[circ, label=270:$\boldsymbol{y}_2$, below of=x2] (y2) {};}
    \node[circ, label=270:$\boldsymbol{y}_t$, below of=xt] (yt) {};
    \node[circ, label=270:$\boldsymbol{y}_{t+1}$, below of=xt1] (yt1) {};
    
    \visible<4-5>{\node[hcirc, hfilled, label=270:\textcolor{blue}{\LARGE $\boldsymbol{y}_1$}, below of=x1] (y1) {};}
    \visible<7-8>{\node[hcirc, hfilled, label=270:\textcolor{blue}{\LARGE $\boldsymbol{y}_2$}, below of=x2] (y2) {};}
    
    \visible<1,6->{\node[circ, filled, label=270:$\boldsymbol{y}_1$, below of=x1] (y1) {};}
    \visible<1,9->{\node[circ, filled, label=270:$\boldsymbol{y}_2$, below of=x2] (y2) {};}
    \visible<1>{\node[circ, filled, label=270:$\boldsymbol{y}_t$, below of=xt] (yt) {};}
    
    % paths between states
    \visible<1-2,4->{\path[line] (x0) to node [above] {$\boldsymbol{r}_1$} (x1);}
    \visible<1-5,7->{\path[line] (x1) to node [above] {$\boldsymbol{r}_2$} (x2);}
    \visible<1-8>{\path[line] (x2) to node [above] {$\boldsymbol{r}_3$} (xother);}
    \path[line] (xother) to node [above] {$\boldsymbol{r}_{t-1}$} (xt);
    \path[line] (xt) to node [above] {$\boldsymbol{r}_{t}$} (xt1);
    \path[line] (xt1) to node [above] {$\boldsymbol{r}_{t+1}$} (xother2);
    
    \visible<3>{\path[hline] (x0) to node [above] {$\boldsymbol{r}_1$} (x1);}
    \visible<6>{\path[hline] (x1) to node [above] {$\boldsymbol{r}_2$} (x2);}
    \visible<9>{\path[hline] (x2) to node [above] {$\boldsymbol{r}_3$} (xother);}
    
    % paths between states and observations
    \visible<1-4,6->{\path[line] (x1) to node [right] {$H_1$} (y1);}
    \visible<1-7,9->{\path[line] (x2) to node [right] {$H_2$} (y2);}
    \path[line] (xt) to node [right] {$H_t$} (yt);
    \path[line] (xt1) to node [right] {$H_{t+1}$} (yt1);
    
    \visible<5>{\path[hline] (x1) to node [right] {$H_1$} (y1);}
    \visible<8>{\path[hline] (x2) to node [right] {$H_2$} (y2);}
    
    \end{tikzpicture}
    }
  \end{figure}
  
  \note{
  Here is the DA algorithm.
  \begin{itemize}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item First, you start off with some initial condition.
    \item Then, you use the physical model to forecast what the next state, x1, would be.
    \item You collect the observation at the first time step and use this to correct, or update, the forecast given by the physical system.
    \item Then, this corrected state of the system is put into the physical model to forecast the next step.
    \item You repeat these steps as more data comes available.
  \end{itemize}
  }
\end{frame}

\begin{frame}{Lorenz 2005}
  \begin{minipage}{.48\textwidth}
    \only<1>{
    \begin{center}
      ``Reality'': Model 3
      \animategraphics[width=\textwidth,controls,loop,autoplay,buttonsize=1em]{200}{../../plots/lorenz05/model3_3d/image}{1}{721}
      %\includegraphics[width=\textwidth]{../../plots/lorenz05/model3_3d/image669.png}
    \end{center}
    }
    \only<2->{
    \begin{center}
      ``Reality'': Model 3
      \includegraphics[width=\textwidth]{../../plots/lorenz05/model3_3d/image669.png}
      \vspace{1em}
    \end{center}
    }
  \end{minipage}\hfill
  \begin{minipage}{.48\textwidth}
    \only<2->{
    \begin{center}
      Physical model: Model 2
      \animategraphics[width=\textwidth,controls,loop,autoplay,buttonsize=1em]{200}{../../plots/lorenz05/model2_3d/image}{1}{721}
      %\includegraphics[width=\textwidth]{../../plots/lorenz05/model2_3d/image669.png}
    \end{center}
    }
  \end{minipage}
  
  \note{
  To make this a bit more concrete, pretend that this Lorenz 2005 model on the left is how our weather system actually works, if we lived in some weird world where we're all 2d and the earth is a circle instead of a sphere. \\
  \hfill\\
  But our weather model is just an approximation of the weather system, as seen on the right.  Our physical model can't capture the small variations of the real weather system.
  }
\end{frame}

\begin{frame}
  \frametitle{Data assimilation assumption: not always true!}
  The data assimilation framework assumes that the physical model correctly represents reality -- not necessarily true!
  
  \begin{center}
    \includegraphics[width=\textwidth]{figures/EnKF_CCA_snapshots_point/image667_EnKF.pdf}
  \end{center}
  
  \note{
  Well, the DA framework assumes that our weather model is exactly the same as the true weather system, but it's not!  This graph shows how ``off'' our forecasts are when we use this ``wrong'' physical model in the DA framework.  The graph shows the trajectory of one of the points of that circle.
  \begin{itemize}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item x-axis: time
    \item green: true trajectory
    \item black dots: noisy observations of the true trajectory
    \item gray lines: forecast trajectories
  \end{itemize}
  One way to correct this is to do something very simple -- artificially inflate the variance of the forecasts so that it covers the true state.  This might do well for some states but not do so well in others, so we want a better way to inflate this variance so that it is more state dependent.  Cari will talk about a particular vein in the literature that attempts to do this.
  }
\end{frame}

\begin{frame}
  \frametitle{A Framework for State-Dependent Error}
  
  See Leith (1978), DelSole and Hou (1999), Danforth, Kalnay, and Miyoshi (2007), Danforth and Kalnay (2008).
  
  \vspace{1em}
  The goal of this approach is to correct the model tendency (time derivative) equation, as opposed to just the forecasts.  Following Leith(1978), assume

$$ \underset{\text{true system tendency}}{\dot{x}} = 
\underset{\text{model tendency}}{M(x)} + 
\underset{\text{error, linear in state}}{Lx + b} $$

\vspace{1em}
Let $e = \dot{x} - M(x) - Lx - b$ and consider choosing $L$ and $b$ to minimize $E[e^Te]$, assuming that $x$ is a random vector with a stationary distribution.

  \end{frame}

\begin{frame}
  \frametitle{Solution for L and b}
The solution is
\begin{eqnarray*}
L &=& Cov(\dot{x} - M(x), x) Cov^{-1}(x)\\
b &=& E[\dot{x} - M(x)] - L E(x)
\end{eqnarray*}

\begin{itemize}
\item If $L\equiv 0$, we have only a state-independent error correction $b = E[\dot{x} - M(x)].$
\item If $M(x) \equiv 0$ (no physical model), we have something akin to linear regression of $\dot{x}$ on $x$.
\end{itemize}

\vspace{1em}
BUT we don't know the distributional quantities above.

\note{Note that $L$ here is potentially very informative about the sources of model error.}

\end{frame}

\begin{frame}
\frametitle{Estimating L and b}

Relaxation Step 1: Suppose we have a long time series of true states $x_t, x_{t+\Delta t}, x_{t+2 \Delta t}, \ldots x_{t+B \Delta t}$. For each one, we can produce a forecast.

  \begin{figure}[hb]
    \centering
    \scalebox{.8}{
    \begin{tikzpicture}[node distance = 5em, auto]
    % Place nodes
    \node[circ, filled, label=90:$\boldsymbol{x}_t$] (x0) at (0,0) {};
    \node[circ, filled, label=90:$\boldsymbol{x}_{t+\Delta t}$, right of=x0] (x1) {};
    \node[circ, filled, label=90:$\boldsymbol{x}_{t + 2\Delta t}$, right of=x1] (x2) {};
    \node[minimum size=2.5em, right of=x2] (xother) {...};
    \node[circ, filled, label=90:$\boldsymbol{x}^f_{t + \Delta t}$, below of=x1] (xf1) {};     
    \node[circ, filled, label=90:$\boldsymbol{x}^f_{t + \Delta t}$, below of=x1] (xf1) {};  
    \node[circ, filled, label=90:$\boldsymbol{x}^f_{t + 2\Delta t}$, below of=x2] (xf2) {}; 
    % Place errors
    \path[line] (x0) to node [above] {} (x1); 
    \path[line] (x1) to node [above] {} (x2); 
    \path[line] (x2) to node [above] {} (xother);
    \path[line] (x0) to node [above] {M} (xf1);
    \path[line] (x1) to node [above] {M} (xf2);
    \end{tikzpicture}
    }
  \end{figure}
  
  \vspace{1em}
  Let $$\delta_{t+k\Delta t} = x_{t+k\Delta t} - x^f_{t+k\Delta t}.$$

\end{frame}

\begin{frame}
\frametitle{Estimating L and b}

Recall that we need the covariance between $\dot{x} - M(x)$ and $x$. We have a sample of $x$. Using finite differences, we can calculate

\begin{eqnarray*}
\dot{x}_t - M(x_t) &\approx& \frac{x_{t+\Delta t} - x_t}{\Delta t} - \frac{x^f_{t+\Delta t} - x_t}{\Delta t}\\
&=& \frac{x_{t+\Delta t} - x^f_{t+\Delta t}}{\Delta t}\\
&=& \frac{\delta_{t+\Delta t}}{\Delta t}
\end{eqnarray*}

\vspace{1em}
To compute $L$ and $b$ on a model with $p=160$, DelSole and Hou (1999)
\begin{itemize}
\item substitute analyses $x^a_t$ for true states $x_t$
\item replace expectations and covariances with sample-based estimates
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Modifications when the state is large}

PROBLEM: the computation of $Lx$ requires $O(p^3)$ operations. Danforth et al. (2007) and Danforth and Kalnay (2008) propose two modifications when $p$ is large.

\vspace{1em}
Modification 1: Covariance localization
\begin{itemize}
\item in variables, levels $\Rightarrow$ block diagonal matrix
\item in space $\Rightarrow$ sparse, banded blocks
\end{itemize}

\vspace{1em}
Modification 2: Perform SVD on the localized cross-covariance matrix $\widehat{Cov}(\delta, x)$ and retain only $K$ SVD modes. This reduces the computation of $Lx$ even further.

\end{frame}


\begin{frame}
  \frametitle{How does the discrepancy affect the data assimilation framework?}
  Assume
  \begin{align*}
    \boldsymbol{y}_t
        &= H_t\boldsymbol{x}_t
          + \boldsymbol{\epsilon}_t \\
    \boldsymbol{x}_t
      &= \underset{\text{`right' model}}{\boldsymbol{r}_t(\boldsymbol{x}_{t-1}; \boldsymbol{\theta})}
      = \underset{\text{`wrong' model}}{\boldsymbol{w}_t(\boldsymbol{x}_{t-1}; \boldsymbol{\theta}_w)} 
        + \textcolor{red}{\underset{\text{discrepancy}}{\boldsymbol{d}_t(\boldsymbol{x}_{t-1}; \boldsymbol{\theta}_d)}}
  \end{align*}
  and $\boldsymbol{w}_t$ is independent of $\boldsymbol{d}_t$ conditional on $\boldsymbol{x}_{t-1}$. Then...\\
  \vspace{1em}
  The update/analysis equation doesn't change: $$p(\boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t}) \propto p(\boldsymbol{y}_t \,|\, \boldsymbol{x}_t) p(\boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t-1})$$
  
  \note{
  To effectively adjust the DA algorithm, we want to understand how the discrepancy actually affect the DA framework.  If we assume that the discrepancy is additive as shown here with the same model for the observation, then we can figure out how this additive discrepancy term affects the DA framework. \\
  \hfill\\
  We notice that the analysis equation doesn't change...
  }
\end{frame}

\begin{frame}
  \frametitle{How does the discrepancy affect the data assimilation framework?}
  But, the forecast equation does:
  \begin{align*}
    p(&\boldsymbol{x}_{t+1} \,|\, \boldsymbol{y}_{1:t}) \\
      &= \int \textcolor{red}{\int p(\boldsymbol{d}_{t+1} = \boldsymbol{x}_{t+1} - \boldsymbol{w}_{t+1} \,|\, \boldsymbol{x}_t) 
                              p(\boldsymbol{w}_{t+1} \,|\, \boldsymbol{x}_t)} 
                              p(\boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t}) 
                              \textcolor{red}{d \boldsymbol{w}_{t+1}} d \boldsymbol{x}_t
  \end{align*}
  
  Compare this to the forecast equation without a discrepancy:
  \begin{align*}
    p(&\boldsymbol{x}_{t+1} \,|\, \boldsymbol{y}_{1:t})
      = \int p(\boldsymbol{x}_{t+1} \,|\, \boldsymbol{x}_t) p(\boldsymbol{x}_t \,|\, \boldsymbol{y}_{1:t}) d \boldsymbol{x}_t
  \end{align*}
  The discrepancy term needs to be integrated into the forecast step! \\
  \vspace{1em}
  
  \note{
  ...but the forecast equation does! Basically, this shows us that the discrepancy term somehow needs to be integrated into the forecast step. This gives us an idea of how the DA algorithm should be adjusted $\rightarrow$ next slide.
  }
\end{frame}


\begin{frame}
  \frametitle{Data assimilation algorithm with discrepancy}
  \setcounter{algocf}{0}
  
  \scalebox{.78}{
  \begin{minipage}{.6\textwidth}
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
    \Input{$x_0^a$}
    \Output{$x_{1:T}^f, x_{1:T}^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      $x_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;
      collect $y_t$\;
      $x_t^a \leftarrow$ \Update($x_t^f, y_t$)\;
    }
    \caption{Data assimilation}
  \end{algorithm}
  \vspace{3.9em}
  \end{minipage}
  }\hfill
  \scalebox{.78}{
  \begin{minipage}{.6\textwidth}
  \visible<2->{
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwFunction{Predict}{ForecastDisc}
    \SetKwFunction{UpdateModel}{UpdateDisc}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
    \Input{$x_0^a$}
    \Output{$x_{1:T}^f, x_{1:T}^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      $w_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;
      \textcolor{red}{$d_t^f \leftarrow$ \Predict($x_{t-1}^a, \mathcal{D}_{t-1}$)\;}
      \textcolor{red}{$x_t^f \leftarrow w_t^f + d_t^f$\;}
      collect $y_t$\;
      $x_t^a \leftarrow$ \Update($x_t^f, y_t$)\;
      \textcolor{red}{$\mathcal{D}_t \leftarrow$ \UpdateModel($\,\cdot\,, \mathcal{D}_{t-1}$)\;}
    }
    \caption{DA with model discrepancy}
  \end{algorithm}
  }
  \end{minipage}
  }
  \note{
  On the left is the original algorithm.  On the right we show how the data assimilation algorithm which allows us to correct for the fact that the physical model doesn't represent the true dynamics of the system.  In addition to forecasting using the physical model, we also want to predict the discrepancy and correct the forecast by adding a prediction of the discrepancy to the output of the physical model.  After we collect new observations, we update our prediction model for the discrepancy. \\
  \hfill\\
  So, we want a low dimensional method that accurately predicts model discrepancy.  We choose canonical correlation analysis, or CCA, to do this.
  }
\end{frame}

\begin{frame}
  \frametitle{DA and Canonical correlation analysis (CCA)}
  Let
  \begin{alignat*}{2}
      \boldsymbol{c}_1 &= \boldsymbol{x}_{t-1} \approx \boldsymbol{x}_{t-1}^a \qquad & \qquad
      \boldsymbol{c}_2 &= \boldsymbol{d}_t = \boldsymbol{x}_t - \boldsymbol{w}_t \approx \boldsymbol{y}_t - \boldsymbol{w}_t^f \\
      u_k &= \boldsymbol{a}_k^T \boldsymbol{c}_1 &
      v_k &= \boldsymbol{b}_k^T \boldsymbol{c}_2
  \end{alignat*}
  {\bf Canonical correlation analysis (CCA)} is an algorithm that finds $\arg \max_{\boldsymbol{a}_k, \boldsymbol{b}_k} Corr(u_k, v_k)$
  such that $u_k, v_k$ are uncorrelated with the previous $u_j, v_j$, respectively, for all $j < k \leq p$. \\
  \hfill\\
  Let $A = \begin{bmatrix} \boldsymbol{a}_1 & \cdots & \boldsymbol{a}_p \end{bmatrix}$ and $B = \begin{bmatrix} \boldsymbol{b}_1 & \cdots & \boldsymbol{b}_p \end{bmatrix}$. Then, the DA framework incorporating this CCA model of discrepancy becomes
  \vspace{-.1in}
  \begin{align*}
    \boldsymbol{y}_t
      &= H_t\boldsymbol{x}_t
        + \boldsymbol{\epsilon}_t \\
    \boldsymbol{x}_t
      &= \boldsymbol{w}_t(\boldsymbol{x}_{t-1}; \boldsymbol{\theta})
        + \textcolor{red}{B^{-T} \boldsymbol{v}_{t-1}} \\
    \textcolor{red}{\boldsymbol{v}_{t}}
      &\textcolor{red}{\;= A^T \boldsymbol{x}_{t} + \boldsymbol{\eta}_t}
  \end{align*}
  
  \note{
  Let c1 be the previous time step, estimated by the analysis and c2 be the discrepancy, estimated by the observation minus the forecast from the wrong model. CCA is a sequential algorithm that finds linear combinations of c1 and c2 such that correlation between the linear combos are maximized subject to the constraint stated here. CCA gives us these a's and b's and how these linear combos, u and v, are correlated. Now looking at these equations, we see this gives us a mapping between the analysis and our estimate of the discrepancy.  Practically, that means we can predict discrepancy from the analysis of the previous time step.  This is formalized by the equations shown here at the bottom of the slide. \\
  \hfill\\
  But, CCA is not amenable to being updated in a streaming manner, meaning that we have to recompute these As and Bs every time new observations come available -- this is very computationally expensive.  Furthermore, CCA doesn't provide us a distribution on the discrepancy.  To solve these problems, we want a Bayesian generative model of CCA, which luckily has been figured out in a paper that came out this year.
  }
\end{frame}

\begin{frame}
  \frametitle{Generative model for CCA: inter-battery factor analysis}
  {\bf Inter-battery factor analysis (IBFA)} is a generative model for CCA \parencite{Klami2013}. For $m \in \{1,2\}$,
  \begin{align*}
    \boldsymbol{z} &\sim \mathcal{N}(\boldsymbol{0}_K, I_K) \\
    \boldsymbol{z}_m &\sim \mathcal{N}(\boldsymbol{0}_{K_m}, I_{K_m}) \\
    \boldsymbol{c}_m &\sim \mathcal{N}(F_m \boldsymbol{z} + G_m \boldsymbol{z}_m, \Sigma_m)
  \end{align*}
  \visible<2->{
  After integrating out $\boldsymbol{z}_1$ and $\boldsymbol{z}_2$, 
  \begin{align*}
    \boldsymbol{z} &\sim \mathcal{N}(\boldsymbol{0}_K, I_K) \\
    \boldsymbol{x}_m &\sim \mathcal{N}(F_m \boldsymbol{z}, \underbrace{G_m G_m^T + \Sigma_m}_{\normalfont \Psi_m}),
  \end{align*}
  the MLE of this model is equivalent to CCA up to a rotation (proved by \cite{Browne1979} and \cite{Bach2005}).
  }
  \note{
  Klami and co very recently dug up a connection b/w IBFA model and CCA that has been established in the 70s.  Here is the IBFA model.
  \begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
    \item The z is a latent variable capturing the variation common to both datasets.
    \item zm is called a view-specific variable, which captures the remaining variation in the datasets not captured by z
    \item F and G are linear maps from the latent space to the observation space.
    \item Sigma usually is diagonal, representing independence of noise over features.
  \end{itemize}
  When you integrate out the view specific latent variables, zm, you get this model.  The MLEs of this model has been proven to be equivalent to CCA by both Browne and Bach/Jordan. \\
  \hfill\\
  Some people have made this particular model into a Bayesian model by putting priors on the columns of A and Psi.  In particular, they put an inverse-Wischart prior on Psi.  The inverse-Wischart makes the Bayesian model not very scalable, so Klami and co decided to work with this original IBFA model, coming up with a more efficient BIBFA model.
  }
\end{frame}

\begin{frame}
  \frametitle{Bayesian inter-battery factor analysis (BIBFA)}
  \cite{Klami2013} came up with an efficient {\bf Bayesian} inter-battery factor analysis {\bf (BIBFA)} model.  Let
  \begin{align*}
    \tilde{\boldsymbol{z}}^T = 
      \begin{bmatrix} \boldsymbol{z}^T \\ \boldsymbol{z}_1^T \\ \boldsymbol{z}_2^T \end{bmatrix} \quad
    W =
      \begin{bmatrix}
        F_1 & G_1 & \O \\
        F_2 & \O      & G_2
      \end{bmatrix} \quad
    \Sigma =
      \begin{bmatrix}
        \Sigma_1 & \O \\
        \O       & \Sigma_2
      \end{bmatrix}
  \end{align*}
  Assume $\Sigma_m = \tau_m I$.  Then, the model can be reparametrized as
  \begin{align*}
    \tilde{\boldsymbol{z}} &\sim \mathcal{N}(\boldsymbol{0}, I) \\
    \boldsymbol{x} \,|\, W, \tilde{\boldsymbol{z}}, \tau_m &\sim \mathcal{N}(W \tilde{\boldsymbol{z}}, \Sigma)
  \end{align*}
  with priors
  \begin{align*}
    \tau_m &\sim Gamma(\alpha_\tau, \beta_\tau) \\
    W_k \,|\, \alpha_k &\sim \mathcal{N}\left(0, \tfrac{1}{\alpha_k} I \right) \\
    \alpha_k &\sim Gamma(\alpha_0, \beta_0)
  \end{align*}
  Posterior of the parameters are obtained via variational approximation.
  
  \note{
  Klami and co basically reparametrized the IBFA model so that they can avoid the inverse Wischart prior, as shown here.  They reparametrized the IBFA model by concatenating the latent variables together into y, giving this mapping between y and  and let Sigma be a diagonal matrix -- this avoids the inverse-wischart prior on Psi and makes it scalable.  They used these particular priors for this model.  I just want to point out that this particular prior that they chose for W is one that makes W sparse, and sparse is good -- sparse means that we can represent the high dimensional state space with a lower dimensional space. \\
  \hfill\\
  %Once we estimate the posterior distribution of these parameters, we can then obtain a predictive distribution the discrepancy given the analysis from the previous time step.
  This solves the problem of getting a distribution on the discrepancy, but still doesn't solve the problem of online updating.
  }
\end{frame}

\begin{frame}
  \frametitle{Streaming BIBFA}
  Joint likelihood of BIBFA:
  \begin{align*}
    p(&\boldsymbol{x}, \boldsymbol{y}, \text{parameters}) \\
      &= \prod_{m=1}^{2} p(W_m \,|\, \boldsymbol{\alpha}_m) p(\boldsymbol{\alpha}_m) p(\tau_m) \prod_{n=1}^{N} p(\tilde{z}_n) p(c_{mn} \,|\, W_m, \tilde{z}_n, \tau_m)
  \end{align*}

  Some manipulation gives us a streaming update of BIBFA!
  %http://tex.stackexchange.com/questions/52598/beamer-highlighting-aligned-math-with-overlay
  \begin{align*}
    p(&\text{parameters} \,|\, \boldsymbol{c}_{\text{old}}, \boldsymbol{c}_{\text{new}}) \\
      &\propto \tikzmark{a} \prod_{m=1}^{2} p(W_m \,|\, \boldsymbol{\alpha}_m) p(\boldsymbol{\alpha}_m) p(\tau_m) \prod_{n \in \text{old}}^{N} p(c_{mn} \,|\, W_m, \tau_m) \tikzmark{b} \\
          &\qquad\times \tikzmark{c} \prod_{n \in \text{new}}^{N} p(c_{mn} \,|\, W_m, \tau_m) \tikzmark{d} \\
      &\propto \tikzmark{e} p(\text{parameters} \,|\, \boldsymbol{c}_{\text{old}}) \tikzmark{f}
          \times \tikzmark{g} p(\boldsymbol{c}_{\text{new}} \,|\, \text{parameters}) \tikzmark{h}
      \visible<2->{= \tikzmark{i} \text{prior} \tikzmark{j} \times \tikzmark{k} \text{likelihood} \tikzmark{l}}
  \end{align*}
  
  \begin{tikzpicture}[remember picture,overlay]
    \coordinate (aa) at ($(a)+(0,0.75)$);
    \coordinate (bb) at ($(b)+(0,-.5)$);
    \node<2>[fill=blue!20,opacity=0.4,rectangle,rounded corners,fit=(aa) (bb)] {};
    \coordinate (cc) at ($(c)+(0,0.75)$);
    \coordinate (dd) at ($(d)+(0,-.5)$);
    \node<2>[fill=red!20,opacity=0.4,rectangle,rounded corners,fit=(cc) (dd)] {};
    \coordinate (ee) at ($(e)+(0,0.25)$);
    \coordinate (ff) at ($(f)+(0,0)$);
    \node<2>[fill=blue!20,opacity=0.4,rectangle,rounded corners,fit=(ee) (ff)] {};
    \coordinate (gg) at ($(g)+(0,0.25)$);
    \coordinate (hh) at ($(h)+(0,0)$);
    \node<2>[fill=red!20,opacity=0.4,rectangle,rounded corners,fit=(gg) (hh)] {};
    \coordinate (ii) at ($(i)+(0,0.25)$);
    \coordinate (jj) at ($(j)+(0,0)$);
    \node<2>[fill=blue!20,opacity=0.4,rectangle,rounded corners,fit=(ii) (jj)] {};
    \coordinate (kk) at ($(k)+(0,0.25)$);
    \coordinate (ll) at ($(l)+(0,0)$);
    \node<2>[fill=red!20,opacity=0.4,rectangle,rounded corners,fit=(kk) (ll)] {};
  \end{tikzpicture}
  
  \note{
  This BIBFA model can be manipulated to get a streaming update of BIBFA, where we can use the posterior using the old data, shown in blue, as a prior for the new data that comes in, shown in red.  This is very reminiscent of the analysis step in the DA framework, where the forecast we used as the prior and the observation was used as the likelihood.
  }
\end{frame}

\begin{frame}
  \frametitle{How does this all fit together?}
  \begin{itemize}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item CCA gave us a low dimensional way to predict discrepancy from analysis, but CCA...
    \begin{itemize}
    %\setlength{\itemsep}{0pt}
    %\setlength{\parskip}{0pt}
      \item Has to be recomputed every time new observations come available -- this is computationally expensive!
      \item Doesn't give a predictive distribution for the discrepancy.
    \end{itemize}
    \item BIBFA is a Bayesian generative model of CCA that can be updated as new observations come available and can thus be incorporated into the data assimilation framework.
  \end{itemize}
  \note{ }
\end{frame}

\begin{frame}
  \frametitle{Data assimilation algorithm with BIBFA}
  %\setbeamercolor{alerted text}{fg=blue}
  \setcounter{algocf}{0}

  \scalebox{.78}{
  \begin{minipage}{.6\textwidth}
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}

    %\Input{$x_{t-1}^a$}
    %\Output{$x_t^f, x_t^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      \alert<2->{$x_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;}
      collect $y_t$\;
      $x_t^a \leftarrow$ \Update($x_t^f, y_t$)\;
    }
    \caption{Data assimilation}
  \end{algorithm}
  \vspace{3.9em}
  \end{minipage}
  } \hfill
  \setcounter{algocf}{2}
  \scalebox{.78}{
  \begin{minipage}{.6\textwidth}
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwFunction{BIBFA}{BIBFAUpdate}
    \SetKwFunction{Predict}{ForecastDisc}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
    %\Input{$x_{t-1}^a$}
    %\Output{$x_t^f, x_t^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      $w_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;
      $d_t^f \leftarrow$ \Predict($x_{t-1}^a, \mathcal{D}_{t-1}$)\;
      \alert<2->{$x_t^f \leftarrow w_t^f + d_t^f$\;}
      collect $y_t$\;
      $x_t^a \leftarrow$ \Update($x_t^f, y_t$)\;
      $\mathcal{D}_t \leftarrow$ \BIBFA($y_{t} - w_{t}^f, x_{t}^a, \mathcal{D}_{t-1}$)\;
    }
    \caption{DA with BIBFA}
  \end{algorithm}
  \end{minipage}
  }
  \iffalse
  \scalebox{.78}{
  \begin{minipage}{.6\textwidth}
  \begin{algorithm}[H]
    \SetKwFunction{Update}{Update}
    \SetKwFunction{Forecast}{Forecast}
    \SetKwFunction{BIBFA}{BIBFAUpdate}
    \SetKwFunction{Predict}{ForecastDisc}
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
    %\Input{$x_{t-1}^a$}
    %\Output{$x_t^f, x_t^a$}
    
    %\BlankLine
    \For{$t \leftarrow 1$ \KwTo $T$}{
      $w_t^f \leftarrow$ \Forecast($x_{t-1}^a$)\;
      $d_t^f \leftarrow$ \Predict($x_{t-1}^a, \mathcal{D}_{t-1}$)\;
      \alert<2->{$x_t^f \leftarrow w_t^f + d_t^f$\;}
      collect $y_t$\;
      $x_t^a \leftarrow$ \Update($\textcolor{red}{w_t^f}, y_t$)\;
      $\mathcal{D}_t \leftarrow$ \BIBFA($y_{t} - w_{t}^f, x_{t}^a, \mathcal{D}_{t-1}$)\;
    }
    \caption{DA with post-hoc BIBFA}
  \end{algorithm}
  \end{minipage}
  }
  \fi
  
  \note{
  On the left is the original DA framework.  The right shows how the algorithm changes with the BIBFA.  There are two main changes here:
  \begin{enumerate}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item Use the old BIBFA model to predict the discrepancy
    \item Then update the old BIBFA model with new data using the streaming update equation from the previous slide.
  \end{enumerate}
  Now I will show you some results, comparing the forecasts from both of these algorithms, as shown in red.  Unfortunately, for these results, I didn't have a chance to implement this streaming update of BIBFA, so I just did a batch update of BIBFA, meaning that I had to run this BIBFA model using the whole dataset instead of updating the old BIBFA with a new model, which is more memory friendly.
  }
  
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{center}
    \includegraphics[width=.85\textwidth]{figures/EnKF_CCA_snapshots_point/image667.pdf}
  \end{center}
  
  \note{
  Here is the same gridpoint in the circle from the Lorenz 2005 model that we were looking at before.  The top panel shows the forecasts after running DA using the wrong model, Algorithm 1.  The bottom panel shows the DA using BIBFA to predict the discrepancy, Algorithm 3.
  \begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
    \item At first, there is not enough data in the BIBFA model to properly inform what the discrepancy is.
    \item With more data, the BIBFA gets better at predicting the discrepancy and we see that incorporating BIBFA inflates the forecasts so that the uncertainty captures the true value pretty well.
  \end{itemize}
  }
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{center}
    \includegraphics[width=.85\textwidth]{figures/rms.pdf}
  \end{center}
  
  \note{
  Here's the average RMS over time, where the x-axis is time.  The coral line is the forecasts from the original DA framework and the blue line is the forecast when we account for the discrepancy.  This shows errors from both forecasts are comparable.
  }
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{center}
    \includegraphics[width=.85\textwidth]{figures/coverage.pdf}
  \end{center}
  
  \note{
  However, the coverage is better with the algorithm that incorporates the discrepancy, as seen here.  The coverage rates approaches the optimal coverage as time goes by, i.e. with more data.
  }
\end{frame}

\begin{frame}
  \frametitle{There's a lot more to do!}
  \begin{itemize}
  %\setlength{\itemsep}{0pt}
  %\setlength{\parskip}{0pt}
    \item Incorporate the streaming update of BIBFA into the DA framework.
    \item Implement the algorithm on a more complex model.
    \item Compare the BIBFA algorithm with the other 
  \end{itemize}
  \note{ }
\end{frame}

\end{document}
