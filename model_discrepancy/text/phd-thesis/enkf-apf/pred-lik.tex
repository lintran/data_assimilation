%!TEX root = ../thesis.tex
\synctex=1

\subsection{Predictive and conditional predictive likelihood} \label{sec2:pred-lik}
Before deriving the estimators of the analysis distribution, we derive estimators of two different forms of the predictive likelihood: one that has been introduced before---the predictive likelihood $p(y_t \,|\, y_{0:t-1})$ from \Cref{eqn2:pred-lik}---and one that has not been seen before---the conditional predictive likelihood $p(y_t \,|\, \theta_t, y_{0:t-1})$. These two forms of the predictive likelihood are important in the construction of the updating algorithms.

The predictive likelihood from \Cref{eqn2:pred-lik} is repeated here for convenience:
\begin{align*}
  \pi_{0:t-1}(y_t)
    \equiv p(y_t \,|\, y_{0:t-1})
    &= \int \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t, \theta_t \,|\, y_{0:t-1}) dx_t d\theta_t
\end{align*}
An estimator of the predictive likelihood is derived by plugging the estimator of the joint forecast distribution (\Cref{eqn2:joint-forecast}) into the above equation:
\begin{align*}
  \pf_{0:t-1}(y_t)
    &\equiv \int \int \phi[y_t; H_t(\theta_{t,y}) x_t , V_t(\theta_{t,y})] \pf_{0:t-1}(x_t, \theta_t) dx_t d\theta_t \\
    &= \frac{1}{M} \sum_{m=1}^M \int \phi[y_t; H_t(\theta_{t,y}^{f(m)}) x_t, V_t(\theta_{t,y}^{f(m)})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) dx_t
\end{align*}
The summand of the predictive likelihood is important in the derivation of the estimator to the analysis distribution, so we denote the summand by the function
\begin{align}
  l_t(x_t^{f(m)},\theta_t^{f(m)},\Sigma)
    &\equiv \int \phi[y_t; H_t(\theta_t^{f(m)}) x_t^{f(m)}, V_t(\theta_t^{f(m)})] \phi(x_t; x_t^{f(m)}, \Sigma) dx_t \notag \\
    &= \phi[y_t; H_t(\theta_t^{f(m)}) x_t^{f(m)}, V_t(\theta_t^{f(m)}) + H_t(\theta_t^{f(m)}) \Sigma H_t^T(\theta_t^{f(m)})],
\label{eqn2:cond-pred-lik}
\end{align}
where $(x_t^{f(m)}, \theta_t^{f(m)}) \sim \pf_{0:t-1}(x_t, \theta_t)$.
Therefore, the estimator of the predictive likelihood is
\begin{align*}
  \pf_{0:t-1}(y_t)
    &= \frac{1}{M} \sum_{m=1}^M l_t(x_t^{f(m)}, \theta_{t,y}^{f(m)}, \hat{\Sigma}_t^f).
\end{align*}

We introduce the conditional predictive likelihood $p(y_t \,|\, \theta_t, y_{0:t-1})$ that will be important in the construction of the sequential updating algorithm. By the axiom of conditional probability,
\begin{align*}
  \pi_{0:t-1}(y_t \,|\, \theta_t)
    \equiv p(y_t \,|\, \theta_t, y_{0:t})
    &= \int p(y_t \,|\, x_t, \theta_t, y_{0:t-1}) p(x_t \,|\, \theta_t, y_{0:t-1}) dx_t \\
    &= \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t \,|\, \theta_t, y_{0:t-1}) dx_t, %\label{eqn2:cond-pred-lik}
\end{align*}
where the last equality comes from the measurement independence assumption:
\begin{align}
  p(y_t \,|\, x_t, \theta_t, y_{0:t-1})
    = g(y_t \,|\, x_t, \theta_{t,y})
    % = g(y_t \,|\, x_t)
    = \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})].
\label{eqn2:obs-indep}
\end{align}
An estimator of the conditional predictive likelihood is constructed by plugging in the estimator of the conditional forecast distribution of the state (\Cref{eqn2:forecast-cond-state}):
\begin{align*}
  \pf_{0:t-1}(y_t \,|\, \theta_t)
    &\equiv \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] \pf_{0:t-1}(x_t \,|\, \theta_t) dx_t \\
    &= \frac{1}{M} \sum_{m=1}^M \left[ \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)  dx_t \right] \\
    %&= \frac{1}{M} \sum_{m=1}^M \phi[y_t; H_t(\theta_t) x_t^{f(m)}, V_t(\theta_t) + H_t(\theta_t) \hat{\Sigma}_t^f H_t^T(\theta_t)]
    &= \frac{1}{M} \sum_{m=1}^M l_t(x_t^{f(m)}, \theta_{t,y}, \hat{\Sigma}_t^f).
\end{align*}

