%!TEX root = ../thesis.tex
\synctex=1

\section{Update step: when estimating the measurement parameter $\Theta_{t,y}$} \label{sec2:update-thetay-est}
Now that we have an algorithm to update the state parameter $\Theta_{t,x}$, it is easy to construct an algorithm to update both the state and measurement parameter. The algorithm constructed in this section is a slight modification of \Cref{alg:enkf-apf-thetay-known}, which updates $\Theta_{t,x}$ when $\Theta_{t,y}$ is fixed. The estimation of the measurement parameter introduces computational expenses that are undesirable, e.g., the calculation of different Kalman gains. We briefly discuss this situation before constructing an algorithm that reduces that computational expense. %As mentioned earlier, the key to reducing the computational expense is to use a point estimator of the measurement parameter in place of the parameter samples.

\subsection{With different Kalman gains} \label{sec2:update-diffKG}
Using a similar factorization of the joint analysis distribution as in \Cref{eqn2:analysis-sequential}, we have
\begin{align}
  \pi_{0:t}(x_t, \theta_t)
    &= \frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})]}{p(y_t \,|\, \theta_t, y_{0:t-1})} \times \frac{p(y_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} p(x_t, \theta_t \,|\, y_{0:t}).
\label{eqn2:update-diffkg}
\end{align}
This factorization is quite similar to \Cref{eqn2:analysis-sequential} and suggests that \Cref{alg:enkf-apf-thetay-known} is similarly applicable. However, since the measurement parameter is being sampled at every timestep, each state sample $x_t^{f(m)}$ is updated with a different parameter sample $\theta_{t,y}^{(m)}$ as opposed to the same parameter $\bar{\theta}_{t,y}$ as in \Cref{sec2:update-sequential}. This consequently results in $M$ evaluations of $H_t(\theta_{t,y}^{(m)})$ and $V_t(\theta_{t,y}^{(m)})$, leading to other computationally expensive modifications to \Cref{alg:enkf-apf-thetay-known}. After deriving estimators in a similar manner as \Cref{sec2:update-sequential}, there are two important changes to the estimators that come with undesirable modifications to the algorithm:
\begin{enumerate}[leftmargin=*]
  \item The parameters are resampled with importance weights proportional to $l_t(x_t^{f(m)}, \theta_{t,y}^{f(m)}, \hat{\Sigma}_t^f)$ as opposed to $l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)$ (see Step \ref{item2:fixed-thetay-param-weights} of \Cref{alg:enkf-apf-thetay-known}). Examining the equation of $l_t(\cdot)$ (\Cref{eqn2:cond-pred-lik}), the evaluation of the weight for each parameter sample requires $M$ calculations of the variance $V_t(\theta_{t,y}^{f(m)}) + H_t(\theta_{t,y}^{f(m)}) \hat{\Sigma}_t^f H_t^T(\theta_{t,y}^{f(m)})$ for each $m=1,..,M$ as opposed to the calculation of one calculation of the variance $V_t(\bar{\theta}_{t,y}) + H_t(\bar{\theta}_{t,y}) \hat{\Sigma}_t^f H_t^T(\bar{\theta}_{t,y})$ for all samples.

  \item %Plugging in the necessary estimators, the measurement density in the term to the left of the ``$\times$'' symbol of \Cref{eqn2:update-diffkg} leads to a different measurement mapping $H_t(\theta_{t,y}^{a(m)}) \cdot$ and measurement variance $V_t(\theta_{t,y}^{a(m)})$ for each sample and thus
  After resampling the forecast parameter particles to generate analysis parameter particles $\{\theta_t^{a(m)}\}_{m=1}^M$, each state sample is updated as
  \begin{align*}
    x_t^{a(m)}
      &= \tilde{x}_t^{f(m)} + K_t(\theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f) [y_t - H_t(\theta_{t,y}^{a(m)}) \tilde{x}_t^{f(m)} ].
    % \hat{\Sigma}_t^a
    %   &= \hat{\tilde{\Sigma}}_t^f - K_t(\theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f) \hat{\tilde{\Sigma}}_t^f H_t^T(\theta_{t,y}^{a(m)}), \\
    % w_t^{(m)}
    %   &= \frac{l_t(\tilde{x}_t^{f(m)}, \theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f)}{\sum_{n=1}^M l_t(\tilde{x}_t^{f(n)}, \theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f)}.
  \end{align*}
  Compare this equation with \Cref{eqn2:state-sample} from \Cref{sec2:update-sequential}: instead of updating each state sample with the same Kalman gain $K_t(\bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f)$, each sample is updated with a different Kalman gain $K_t(\theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f)$, requiring $M$ calculations of the Kalman gain. Not only is the calculation of the Kalman gain computationally expensive, it is not ideal for plug-and-play, since existing implementations of the \gls{enkf} use the same Kalman gain for each state sample.

\end{enumerate}
% Not only is the algorithm more computationally expensive due to the calculation of different Kalman gains, it is worsened by the computational expense of the calculation of the covariances required by the weights.

\citet{Frei2012} faced a similar issue in their development of a hybrid filter that uses the \gls{enkf} to filter the states and the \gls{pf} to estimate a measurement noise parameter. They circumvented the problem by replacing the relevant parts of the Kalman gain with a point estimator. With parameter samples $\{(\theta_t^{(m)}, w_t^{(m)})\}_{m=1}^M$ from $p(\theta_t \,|\, \cdot)$, they suggested replacing $V_t(\theta_t^{(m)})$ of the Kalman gain $K_t(\theta_t^{(m)}, \cdot)$ (see \Cref{eqn2:kg2}) with a point estimator\footnote{See discussion at the end of Section 4b in their paper.}, i.e.,
\begin{align*}
  V_t(\overline{\theta}_t) \equiv V_t \left( \sum_{m=1}^M w_t^{(m)} \theta_t^{(m)} \right)
  \quad \text{or} \quad
  \overline{V_t(\cdot)} \equiv \sum_{m=1}^M w_t^{(m)} V_t(\theta_t^{(m)}).
\end{align*}
%, where $\bar{x}$ denotes the sample mean calculated with the samples $\{x_i\}_{i=1}^n$.
While a practical solution to the problem, a bias is introduced when updating with only a point estimator, which they do not correct for in their algorithm. We do think their approach is promising and develop a similar algorithm that uses a point estimator, which allows for the usage of the same Kalman gain, \textit{and} corrects for the bias in the next section.

\subsection{With the same Kalman gain} \label{sec2:update-sameKG}
The computational expense is reduced by replacing posterior samples of $\Theta_{t,y}$ with a point estimator $\bar{\theta}_{t,y}^f$. Consequently, the updated samples are biased towards the point estimator, so the bias is corrected similar to the sequential updating algorithm of \Cref{sec2:update-sequential}. Let $\bar{\theta}_{t,y}^f$ denote a point estimator of $p(\theta_{t,y} \,|\, y_{0:t-1})$. Before delving into the details, we introduce notation to distinguish between when the point estimator is used and when it is not. Denote the parameter with the point estimator as $\tilde{\Theta}_t = (\Theta_{t,x}, \bar{\theta}_{t,y}^f)$. Consequently, $\tilde{\theta}_t \equiv (\theta_{t,x}, \bar{\theta}_{t,y}^f)$ and the forecast samples are $\tilde{\theta}_t^{f(m)} \equiv (\theta_{t,x}^{f(m)}, \bar{\theta}_{t,y}^f)$.

As mentioned in \Cref{sec2:update-sequential}, one way to view the equation that summarizes the sequential update algorithm is the joint analysis distribution multiplied by an special identity. Instead of using the identity based on the conditional predictive likelihood $p(y_t \,|\, \theta_t, y_{0:t-1})$ as in \Cref{sec2:update-sequential}, the identity is based on the predictive likelihood conditioned on the point estimator of the measurement parameter, i.e., $p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})$. Thus, the joint analysis distribution is
\begin{align*}
  \pi_{0:t}(x_t, \theta_t)
    &= \frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})]}{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})} \times \frac{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} p(x_t, \theta_t \,|\, y_{0:t}).
\end{align*}
However, like the algorithm in \Cref{sec2:update-sequential}, updating with an estimator of this equation will only update the state parameter and not update the measurement parameter since the predictive likelihood is conditioned on the point estimator $\bar{\theta}_{t,y}^f$. To update the measurement parameter, the equation is multiplied by another identity: an identity constructed with the measurement density conditioned on the point estimator $\bar{\theta}_{t,y}^f$, i.e., $g(y_t \,|\, x_t, \bar{\theta}_{t,y}) = \phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t, V_t(\bar{\theta}_{t,y}^f) ]$. There are a couple of ways to reshuffle the identity in a way that is illustrative to constructing the algorithm.

One way to update the measurement parameter is via this particular factorization of the joint analysis distribution:
\begin{align}
  \pi_{0:t}(x_t, \theta_t)
    &= \underbrace{\frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})]}{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t, V_t(\bar{\theta}_{t,y}^f)]}} \notag \\
      &\qquad \times
      \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t, V_t(\bar{\theta}_{t,y}^f)]}{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})}
      \times
      \frac{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})}
      p(x_t, \theta_t \,|\, y_{0:t}). \label{eqn2:update-thetay-resample-twice}
\end{align}
Ignoring the term above the underbrace, notice that the factorization is the same as \Cref{eqn2:analysis-sequential} from the sequential update of the state and parameters when $\Theta_{t,y}$ is fixed. For this reason, this particular factorization suggests a \textit{three}-stage algorithm: the two-stage algorithm of \Cref{alg:enkf-apf-thetay-known} with an additional third stage. Specifically, sample pairs of state and parameter samples via \Cref{alg:enkf-apf-thetay-known}, thus updating only the state and the state parameter, and then the additional third stage resamples those samples with importance weights proportional to the term above the underbrace to update the measurement parameter. Although resampling is a necessary step in \gls{pf} algorithms, it is an undesirable step that can lead to particle collapse, so adding \textit{another} resampling step to the algorithm will only excarbate the problem. Furthermore, our goal is to \textit{avoid} resampling the state samples, which this particular modification of the algorithm fails to achieve.

\begin{alg}[h]
\fbox{
\begin{minipage}{\linewidth}
  \textbf{Input}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$
    \item $\{(x_t^{f(m)}, \theta_t^{f(m)})\}_{m=1}^M \approxdist p(x_t, \theta_t \,|\, y_{0:t-1})$ from \Cref{alg:forecast}
    % \item $H_t \equiv H_t(\bar{\theta}_{t,y}^f)$: mapping between state and observations
    % \item $V_t \equiv V_t(\bar{\theta}_{t,y}^f)$: measurement covariance
    % \item $y_t$: observation
  \end{itemize}
  ~\\
  \textbf{Output}: $\{(x_t^{a(m)}, \theta_t^{a(m)})\}_{m=1}^M \approxdist p(x_t, \theta_t \,|\, y_{0:t})$

  \begin{enumerate}[leftmargin=*]
    \item Update $\Theta_t$:
    \begin{enumerate}[leftmargin=*,noitemsep]
      \item Calculate the point estimator $\bar{\theta}_{t,y}^f$ from samples $\{\theta_{t,y}^{f(m)}\}_{m=1}^M$.
      \item Calculate unnormalized weights\footnote{The $l_t(\cdot)$ term is defined in \Cref{eqn2:cond-pred-lik}.}:
      \begin{align*}
        \tilde{w}_t^{(m)}
          &= \frac{\phi[y_t; H_t(\theta_{t,y}^{f(m)}) x_t^{f(m)}, V_t(\theta_{t,y}^{f(m)})]}{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t^{f(m)}, V_t(\bar{\theta}_{t,y}^f)]}
            l_t(x_t^{f(m)}, \bar{\theta}_{t,y}^f, \hat{\Sigma}_t^f),
            %\phi(y_t; H_t(\theta_{t,y}^{f(m)}) x_t^{f(m)}, V_t(\theta_{t,y}^{f(m)}) + H_t(\theta_{t,y}^{f(m)}) \hat{\Sigma}_t^f H_t(\theta_{t,y}^{f(m)})^T)
      \end{align*}
      for each $m=1,...,M$, where $\hat{\Sigma}_t^f$ is the (tapered) sample covariance calculated from $\{x_t^{f(m)}\}_{m=1}^M$. Normalize weights: $w_t^{(m)} = \tilde{w}_t^{(m)} / \sum_{n=1}^M \tilde{w}_t^{(n)}$ for each $m=1,...,M$.
      % \item Resample $\{\theta_t^{a(m)}\}_{m=1}^M$ from $\{(\theta_t^{f(m)}, w_t^{(m)})\}_{m=1}^M$.
      \item Sample
      \begin{align*}
        \theta_t^{a(m)}
          \sim %\pft_{0:t}(\theta_t) =
          \sum_{m=1}^M w_t^{(m)} \delta_{\theta_t^{f(m)}}(\theta_t)
      \end{align*}
      for each $m=1,...,M$.
    \end{enumerate}
    \item Update $(X_t, \Theta_t)$:
    \begin{enumerate}[leftmargin=*,noitemsep]
      \item Sample $\tilde{x}_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{a(m)})$.
      \item Sample $\{x_t^{a(m)}\}_{m=1}^M$ using Step 3 of \Cref{alg:enkf} with forecast state samples $\{\tilde{x}_t^{f(m)}\}_{m=1}^M$, measurement mapping $H_t = H_t(\bar{\theta}_{t,y}^f)$, and measurement variance $V_t = V_t(\bar{\theta}_{t,y}^f)$.
    \end{enumerate}
  \end{enumerate}
\end{minipage}
}
\captionnew{EnKF-APF: Update step}{}
\label{alg:enkf-apf-thetay-est}
\end{alg}


Let's examine another factorization of the joint analysis distribution:
\begin{align}
  \pi_{0:t}(x_t, \theta_t)
    &= \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t, V_t(\bar{\theta}_{t,y}^f)]}{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})} \notag\\
      &\qquad \times
      \underbrace{\frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})]}{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t, V_t(\bar{\theta}_{t,y}^f)]}}
      \frac{p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})}
      p(x_t, \theta_t \,|\, y_{0:t}). \label{eqn2:update-thetay-resample-once}
\end{align}
Again, ignoring the term above the underbrace, the factorization is the same as \Cref{eqn2:analysis-sequential} when $\Theta_{t,y}$ is fixed. This particular factorization suggests resampling the parameter particles with weights proportional to the conditional predictive likelihood $p(y_t \,|\, \tilde{\theta}_t, y_{0:t-1})$---as in the first-stage of \Cref{alg:enkf-apf-thetay-known}---\textit{in addition to} the term above the underbrace. Specifically, each parameter sample is updated with importance weights
\begin{align*}
  w_t^{(m)}
    \propto \frac{\phi[y_t; H_t(\theta_{t,y}^{f(m)}) x_t^{f(m)}, V_t(\theta_{t,y}^{f(m)})]}{\phi[y_t; H_t(\bar{\theta}_{t,y}^f) x_t^{f(m)}, V_t(\bar{\theta}_{t,y}^f)]}
            l_t(x_t^{f(m)}, \bar{\theta}_{t,y}^f, \hat{\Sigma}_t^f).
\end{align*}
%The additional weight above the underbrace is the key to updating the observation parameter.
Not only does the term above the underbrace update the measurement parameter, it allows for the construction of an algorithm that uses the same Kalman gain $K_t(\bar{\theta}_{t,y}^f, \cdot)$ in the \gls{enkf} update of the state samples, i.e., %: instead of applying the \gls{enkf} update with a different observation mapping $H_t(\theta_{t,y}^{a(m)})$ and measurement covariance $V_t(\theta_{t,y}^{a(m)})$ to each state sample as in \Cref{sec2:update-diffKG}, each state sample uses the same observation mapping $H_t(\bar{\theta}_{t,y}^f)$ and measurement covariance $V_t(\bar{\theta}_{t,y}^f)$:
\begin{align*}
    x_t^{a(m)}
      &= \tilde{x}_t^{f(m)} + K_t(\bar{\theta}_{t,y}^f, \hat{\tilde{\Sigma}}_t^f) [y_t - H_t(\bar{\theta}_{t,y}^f) \tilde{x}_t^{f(m)}].
    % \hat{\Sigma}_t^a
    %   &= \hat{\tilde{\Sigma}}_t^f - K_t(\theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f) \hat{\tilde{\Sigma}}_t^f H_t^T(\theta_{t,y}^{a(m)}), \\
    % w_t^{(m)}
    %   &= \frac{l_t(\tilde{x}_t^{f(m)}, \theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f)}{\sum_{n=1}^M l_t(\tilde{x}_t^{f(n)}, \theta_{t,y}^{a(m)}, \hat{\tilde{\Sigma}}_t^f)}.
  \end{align*}
We do not derive the estimators of the filtering densities since they are similarly derived as in \Cref{sec2:update-sequential}. \Cref{alg:enkf-apf-thetay-est} summarizes the algorithm. Notice that the algorithm is exactly the same as \Cref{alg:enkf-apf-thetay-known}, except that the parameters are resampled with different weights (Step 1b).

