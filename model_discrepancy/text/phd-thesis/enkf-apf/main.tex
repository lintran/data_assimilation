%!TEX root = ../thesis.tex
\synctex=1

\chapter{EnKF-APF: ensemble Kalman filter to update states, particle filter to update parameters} \label{sec2:enkf-apf}
\todo{Pieter: Connect to Rao-Blackwellised PFs \citep{Doucet2000b}}

In the last chapter, we demonstrated on a low-dimensional model that artificial evolution of parameters with the \gls{enkf} algorithm is not generally applicable, except when the practitioner knows that the parameter being estimated is conditionally correlated with the state. Outside of fully observed systems, these situations are not straightforward to analyze a priori, especially with chaotic systems like the Lorenz 1963 system. For example, when examining the set of equations governing the Lorenz 1963 system, we thought that the $x$-direction is the most important direction to measure when estimating $\sigma$, but the results did not corrobate our theory---it turns out the $y$-direction is the most important. Practitioners could alternatively use a \gls{pf} approach to estimate the parameters, but the \gls{pf} has been proven to be impractical for high-dimensional state-spaces \citep{Snyder2008}.

In this chapter, we propose an algorithm that combines the best qualities of both the \gls{enkf} and \gls{pf} algorithms: a hybrid approach that uses the \gls{enkf} to filter the high-dimensional states and the \gls{pf} to filter low-dimensional parameters. There are many ways to combine the two filters, but we choose to combine the two filters in such a way that allows for plug-and-play into existing systems such as \gls{dart}. We fulfill this goal by constructing an updating algorithm that doesn't affect the implementation of the \gls{enkf} to update the state. %Specifically, this means using the same Kalman gain for \textit{all} ensemble members.
As the reader will learn, our particular approach is reminiscent of the widely-used \gls{pf} algorithm: the \gls{apf} proposed by \citet{Pitt1999}. For this reason, we name our algorithm EnKF-APF to remind the reader that the \gls{enkf} is applied to the states and a \gls{pf} approach similar to the \gls{apf} is applied to the parameters. %To distinguish our algorithm from the \gls{enkf} parameter estimation approach, we henceforth refer to the algorithm of using \gls{enkf} to update \textit{both} the state and parameter as the \gls{enkf}-\gls{enkf} algorithm. This particular algorithm was applied to the Lorenz 1963 system in \Cref{sec:l63-param-est-fails} .
We further note that our algorithm is specifically developed to be used with artificial evolution of parameters. There is no need to use our algorithm with parameter estimation algorithms such as \acrlong{pmcmc} \citep{Andrieu2010}.

Similar to the derivations of the sampling algorithms in \Cref{sec:filtering-review}, we begin with an estimator of the analysis distribution at time $t-1$ and derive estimators to the filtering densities at time $t$, which are divided into sections in this chapter. Suppose we have simple random samples from the approximate analysis distribution at time $t-1$: $\{(x_{t-1}^{a(m)}, \theta_{t-1}^{a(m)})\}_{m=1}^M \approxdist p(x_{t-1}, \theta_{t-1} \,|\, y_{0:t-1})$. An estimator of the joint analysis distribution at time $t-1$, i.e., $\pi_{0:t-1}(x_{t-1}, \theta_{t-1})$, is
\begin{align*}
  \pf_{0:t-1}(x_{t-1}, \theta_{t-1})
    \equiv \frac{1}{M} \sum_{m=1}^M \delta_{(x_{t-1}^{a(m)}, \theta_{t-1}^{a(m)})}(x_{t-1}, \theta_{t-1}).
%\label{eqn2:analysis-prev}
\end{align*}
We now derive estimators of the filtering densities and thus construct the EnKF-APF algorithm.

\input{enkf-apf/forecast}
\input{enkf-apf/update-main}
\input{enkf-apf/pred-lik}
\input{enkf-apf/update-thetay-fixed-joint}
\input{enkf-apf/update-thetay-fixed-sequential}
\input{enkf-apf/update-thetay-est}
\input{param-est-case-studies/main}
\input{enkf-apf/limitations}


% \section{Integrating EnKF-PF into IFs}
% The various EnKF-PF algorithms outlined in this chapter can easily be applied for general parameter estimation with \glspl{if}. Simply replace the forecast step (Steps 2a and 2b) of \Cref{alg:if1,alg:if2} with \Cref{alg:forecast} and the update step (Step 2c) of \Cref{alg:if1,alg:if2} with \Cref{alg:enkf-pf-thetay-known}, \ref{alg:enkf-apf-thetay-known}, or \ref{alg:enkf-apf}.

% Furthermore, if the \gls{enkf} is later proven to converge to the optimal filter, the parameter convergence theorems proven by \citet{Ionides2006,Ionides2011,Doucet2013,Ionides2015} can easily be adapted to the algorithms outlined in this chapter.

