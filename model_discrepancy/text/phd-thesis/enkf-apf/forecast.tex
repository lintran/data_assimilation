%!TEX root = ../thesis.tex
\synctex=1

\section{Forecast step} \label{sec2:forecast}
By conditional probability, the joint forecast distribution is factorized as
\begin{align}
  p(x_t, \theta_t \,|\, y_{0:t-1})
    = p(x_t \,|\, \theta_t, y_{0:t-1}) p(\theta_t \,|\, y_{0:t-1}),
\label{eqn2:forecast-true}
\end{align}
Notice that the state is conditionally dependent on the parameter; this suggests a sequential sampling scheme to first sample from the forecast distribution of the parameter and then use the newly sampled parameter to then sample from the state distribution. \Cref{alg:forecast} outlines the forecasting algorithm. Not only does the algorithm provide samples from the joint forecast distribution $p(x_t, \theta_t \,|\, y_{0:t-1})$, it also provides samples from the conditional distributions $p(x_t \,|\, \theta_t, y_{0:t-1})$ and $p(\theta_t \,|\, y_{0:t-1})$. Furthermore, this forecasting algorithm is the same whether the \gls{enkf} or \gls{pf} algorithm is applied; nothing new is proposed here. The new contribution is the estimators used to approximate the joint forecast distribution, which consequently affects the construction of the updating algorithms.

% In this section, we will go through the derivations to make this clear.
% We have
% \begin{align*}
%   p(x_t, \theta_t \,|\, y_{0:t-1})
%     &= \int \int p(x_t, \theta_t \,|\, x_{t-1}, \theta_{t-1}, y_{0:t-1}) p(x_{t-1}, \theta_{t-1} \,|\, y_{0:t-1}) dx_{t-1} d\theta_{t-1}
% \end{align*}
% By the Markov assumption,
% \begin{align*}
%   p(x_t, \theta_t \,|\, x_{t-1}, \theta_{t-1}, y_{0:t-1})
%     &= p(x_t \,|\, x_{t-1}, \theta_t, \theta_{t-1}, y_{0:t-1}) p(\theta_t \,|\, x_{t-1}, \theta_{t-1}, y_{0:t-1}) \\
%     &= f(x_t \,|\, x_{t-1}, \theta_t) p(\theta_t \,|\, \theta_{t-1}),
% \end{align*}
% thus
% \begin{align*}
%   p(x_t, \theta_t \,|\, y_{0:t-1})
%     &= \int \int f(x_t \,|\, x_{t-1}, \theta_t) p(\theta_t \,|\, \theta_{t-1}) p(x_{t-1}, \theta_{t-1} \,|\, y_{0:t-1}) dx_{t-1} d\theta_{t-1}
% \end{align*}
% This suggests a sequential sampling scheme.

% First, sample the parameter:
% \begin{align*}
%   (x_{t-1}^{a(m)}, \theta_t^{f(m)})
%     &\sim \int p(\theta_t \,|\, \theta_{t-1}) \pf_{t-1|t-1}(x_{t-1}, \theta_{t-1}) d\theta_{t-1} \\
%     &= \frac{1}{M} \sum_{m=1}^M p(\theta_t \,|\, \theta_{t-1}^{a(m)}) \delta_{x_{t-1}^{a(m)}}(x_{t-1})
% \end{align*}
% providing the following approximations:
% \begin{align*}
%   p(x_{t-1}, \theta_t \,|\, y_{0:t-1})
%     &\approx \frac{1}{M} \sum_{m=1}^M \delta_{(x_{t-1}^{a(m)}, \theta_t^{f(m)})}(x_{t-1}, \theta_t)
% \end{align*}
% Marginalizing over $X_{t-1}$,
% \begin{align*}
%   p(\theta_t \,|\, y_{0:t-1})
%     &\approx \frac{1}{M} \sum_{m=1}^M \delta_{\theta_t^{f(m)}}(\theta_t)
% \end{align*}
% Now that we have an approximation of $p(x_{t-1}, \theta_t \,|\, y_{0:t-1})$, we can sample the state:
% \begin{align*}
%   x_t^{f(m)}
%     &\sim \int \int f(x_t \,|\, x_{t-1}, \theta_t) \tilde{p}(x_{t-1}, \theta_t \,|\, y_{0:t-1}) dx_{t-1} d\theta_t \\
%     &= \frac{1}{M} \sum_{m=1}^M f(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{f(m)})
% \end{align*}


\begin{alg}[h!]
\fbox{
\begin{minipage}{\linewidth}
  \textbf{Input}: $\{(x_{t-1}^{a(m)}, \theta_{t-1}^{a(m)})\}_{m=1}^M \approxdist p(x_{t-1}, \theta_{t-1} \,|\, y_{0:t-1})$

  \textbf{Output}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{\theta_t^{f(m)}\}_{m=1}^M \approxdist p(\theta_t \,|\, y_{0:t-1})$
    \item $\{x_t^{f(m)}\}_{m=1}^M \approxdist p(x_t \,|\, \theta_t, y_{0:t-1})$
    \item $\{(x_t^{f(m)}, \theta_t^{f(m)})\}_{m=1}^M \approxdist p(x_t, \theta_t \,|\, y_{0:t-1})$\\
  \end{itemize}

  For each $m = 1,...,M$,
  \begin{enumerate}[leftmargin=*,noitemsep]
    \item Sample $\theta_t^{f(m)} \sim q(\theta_t \,|\, \theta_{t-1}^{a(m)})$.
    \item Sample $x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{f(m)})$.
  \end{enumerate}
\end{minipage}
}
\captionnew{EnKF-APF: Forecast step}{}
\label{alg:forecast}
\end{alg}

% Since our goal is to use \glss{enkf} to filter the states and \gls{pf} to filter the parameters, we will have to choose different estimators for the conditional forecast distributions of the states and parameters. For the parameters, the choice is easy: like the \gls{pf}, we use the empirical density with point masses centered at the parameter samples. For the states, we have two choices: approximate the distribution either with
% \begin{enumerate}[leftmargin=*]
%   \item a normal distribution, like the estimator derived in the original interpretation of the \gls{enkf} (see \Cref{eqn1:enkf-forecast}), or
%   \item a mixture of normal distributions, like the estimator derived in the re-interpretation of the \gls{enkf} (see \Cref{eqn1:enkf2-forecast}).
% \end{enumerate}
% As discussed in \Cref{sect:enkf-reinterpretation}, we have a preference for the second estimator derived under the re-interpretation, since it makes it clear how the estimators are used recursively. Furthermore, we believe that a mixture of normal distributions will better approximate the likelihood than one normal distribution. With these two components, we can now derive an estimator of the joint distribution of the state and parameters:
Recall that the key defining difference between the \gls{enkf} and the \gls{pf} is the estimator used to approximate the forecast distribution: the \gls{pf} uses the empirical density and the \gls{enkf} uses a \gls{kde} with Gaussian kernels centered at each sample with a bandwidth equal to the sample covariance (see discussion in \Cref{sect:enkf-reinterpretation}). We use similar estimators for the conditional forecast distributions:
\begin{subequations}
\begin{align}
  \pf_{0:t}(\theta_t)
    &\equiv \frac{1}{M} \sum_{m=1}^M \delta_{\theta_t^{f(m)}}(\theta_t),\\
  \pf_{0:t}(x_t \,|\, \theta_t)
    &\equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f), \label{eqn2:forecast-cond-state}
\end{align}
\label{eqn2:forecast-conditionals}%
\end{subequations}
where $\hat{\Sigma}_t^f$ is the sample covariance calculated from the state samples $\{x_t^{f(m)}\}_{m=1}^M$ and the superscript $M$ is to remind the reader that $\pf_{0:t}$ is an estimator of $\pi_{0:t}$ that depends on the sample size $M$. These estimators will play a role in the derivation of the estimator to the analysis distribution in the next section, but will not be as fundamental as the estimator of the joint forecast distribution.

At first glance, \Cref{eqn2:forecast-true} seems to indicate that an estimator of the joint forecast distribution is simply the product of the conditional forecast distributions of \Cref{eqn2:forecast-conditionals}. However, multiplying the conditional forecast distributions in that way is fundamentally incorrect: this results in an estimator that implies that the forecast state samples are independent of the forecast parameter samples and consequently artificially increases the sample size. Let's see how that happens. Multiplying the estimators of the conditional forecast distributions from \Cref{eqn2:forecast-conditionals} together, we have
\begin{align*}
  \frac{1}{M^2} \sum_{n=1}^M \sum_{m=1}^M \phi(x_t; x_t^{f(n)}, \hat{\Sigma}_t^f) \delta_{\theta_t^{f(m)}}(\theta_t),
\end{align*}
implying that $\{x_t^{f(n)}\}_{n=1}^M$ is a simple random sample from $\pi_{0:t-1}(x_t \,|\, \theta_t^{f(m)})$ for each $m$ or, in other words, $X_t^{f}$ is independent of $\Theta_t^{f}$. In actuality, the state sample $x_t^{f(n)}$ is generated by $\theta_t^{f(m)}$ for $m = n$ and \textit{together} they are a sample from the joint distribution. With this in mind, we propose a hybrid estimator of the joint forecast distribution: a mixture distribution where each component is a product of a Gaussian kernel centered at a state sample and a point mass at a parameter sample:
\begin{align}
  \pf_{0:t}(x_t, \theta_t)
    \equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) \delta_{\theta_t^{f(m)}}(\theta_t).
\label{eqn2:joint-forecast}
\end{align}
Equipped with estimators of the conditional and joint forecast distributions, we now derive an estimator for the analysis distribution.
