%!TEX root = ../thesis.tex
\synctex=1

\section{Update step: when the measurement parameter $\Theta_{t,y}$ is fixed} \label{sec2:thetay-fixed}
Suppose the measurement parameter $\Theta_{t,y}$ is fixed at some vector $\bar{\theta}_{t,y}$ for each $t$. Then, the parameter is $\Theta_t = (\Theta_{t,x}, \bar{\theta}_{t,y})$ and consequently samples from its forecast and analysis distributions are $\theta_t^{f(m)} = (\theta_{t,x}^{f(m)}, \bar{\theta}_{t,y})$ and $\theta_t^{a(m)} = (\theta_{t,x}^{a(m)}, \bar{\theta}_{t,y})$, respectively. Since the difference between the predictive and conditional predictive likelihoods comes from the observation parameter $\theta_{t,y}$, they are equivalent when $\Theta_{t,y}$ is fixed at $\bar{\theta}_{t,y}$ and thus
\begin{align*}
  \pf_{1:t-1}(y_t) = \pf_{1:t-1}(y_t \,|\, \theta_t) = \frac{1}{M} \sum_{m=1}^M l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f).
\end{align*}
Because the measurement parameter $\Theta_{t,y}$ is fixed, the algorithm in this section only updates the state parameter $\Theta_{t,x}$.

\subsection{Joint update of state and parameter} \label{sec2:update-joint}
The fixed value of the measurement parameter $\bar{\theta}_{t,y}$ is plugged into \Cref{eqn2:analysis} to derive the joint analysis distribution as follows:
\begin{align}
  \pi_{1:t}(x_t, \theta_t)
    \equiv p(x_t, \theta_t \,|\, y_{1:t})
    = \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] p(x_t, \theta_t \,|\, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})}
    % = \frac{g(y_t \,|\, x_t, \bar{\theta}_{t,y}) \pi_{1:t}(x_t, \theta_t)}{\pi_{1:t}(y_t)}
\label{eqn2:update-thetay-fixed-joint}
\end{align}
A direct application of the above factorization suggests a joint update of the state and parameter by weighting each sample proportionally to the observation density $\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})]$. We show why this particular approach is not ideal for plug-and-play.

% \begin{alg}
% \fbox{
% \begin{minipage}{\linewidth}
%   \textbf{Input}: $\{(x_t^{f(m)}, \theta_t^{f(m)})\}_{m=1}^M \approxdist X_t, \Theta_t \,|\, y_{1:t-1}$

%   \textbf{Output}:
%   \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
%     \item $\{x_t^{a(m)}\}_{m=1}^M \approxdist X_t \,|\, \Theta_t, y_{1:t}$
%     \item $\{\theta_t^{a(m)}\}_{m=1}^M \approxdist \Theta_t \,|\, y_{1:t}$
%     \item $\{(x_t^{a(m)}, \theta_t^{a(m)})\}_{m=1}^M \approxdist X_t, \Theta_t \,|\, y_{1:t}$
%   \end{itemize}

%   \begin{enumerate}[leftmargin=*]
%     \item Update:
%     \begin{enumerate}[leftmargin=*,noitemsep]
%       \item Calculate and normalize weights: $w_t^{(m)} = l_t(x_t^{f(m)}, \bar{\theta}_{t,y}) / \sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y})$ for each $m=1,...,M$.
%       \item Sample $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ with an \gls{enkf} update on $\{x_t^{f(m)}\}_{m=1}^M$.
%     \end{enumerate}
%     \item Resample: Sample $\{(x_t^{a(m)}, \theta_t^{a(m)})\}_{m=1}^M$ from $\{(\tilde{x}_t^{a(m)},\theta_t^{f(m)},w_t^{(m)})\}_{m=1}^M$.
%   \end{enumerate}
% \end{minipage}
% }
% \caption{\textit{EnKF-PF when $\Theta_{t,y}$ is known.}}
% \label{alg:enkf-pf-thetay-known}
% \end{alg}

Plugging in the estimator of the joint forecast distribution from \Cref{eqn2:joint-forecast}, we derive an estimator of the joint analysis distribution:
\begin{align*}
  \pf_{1:t}(x_t, \theta_t)
    &\equiv \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \pf_{1:t-1}(x_t, \theta_t)}{\pf_{1:t-1}(y_t)} \\
    &= \frac{\frac{1}{M}\sum_{m=1}^M \phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) \delta_{\theta_t^{f(m)}}(\theta_t)}{\frac{1}{M} \sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
    % &= \sum_{m=1}^M
    %     \frac{l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}{\sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
    %     \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)}{l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
    %     \delta_{\theta_t^{f(m)}}(\theta_t)\\
    % &= \sum_{m=1}^M
    %     \frac{l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}{\sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
    %     \underbrace{\frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) }{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t^{f(m)}, V_t(\bar{\theta}_{t,y}) + H_t(\bar{\theta}_{t,y}) \hat{\Sigma}_t^f H_t^T(\bar{\theta}_{t,y})]}}
    %     \delta_{\theta_t^{f(m)}}(\theta_t)
\end{align*}
Multiplying the summands of the numerator by the identity, $l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f) / l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)$, we have
\begin{align*}
  l_t(&x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)
  \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)}{l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
  \delta_{\theta_t^{f(m)}}(\theta_t) \\
    &= l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)
      \underbrace{\frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) }{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y}) + H_t(\bar{\theta}_{t,y}) \hat{\Sigma}_t^f H_t^T(\bar{\theta}_{t,y})]}}
      \delta_{\theta_t^{f(m)}}(\theta_t).
\end{align*}
As in the re-interpretation of the \gls{enkf} in \Cref{sect:enkf-reinterpretation}, the term above the underbrace should remind the reader of the analysis distribution of the Kalman filter (cf. \Cref{eqn1:kf-analysis2}) and is thus equal to
\begin{align*}
  \phi(x_t; x_t^{a(m)}, \hat{\Sigma}_t^a)
\end{align*}
with
\begin{align*}
  x_t^{a(m)}
    &= x_t^{f(m)} + K_t(\bar{\theta}_{t,y}, \hat{\Sigma}_t^f) [y_t - H_t(\bar{\theta}_{t,y}) x_t^{f(m)}], \\
  \hat{\Sigma}_t^a
    &= \hat{\Sigma}_t^f - K_t(\bar{\theta}_{t,y}, \hat{\Sigma}_t^f) \hat{\Sigma}_t^f H_t^T(\bar{\theta}_{t,y}).
\end{align*}
Therefore, the estimator is
\begin{align*}
  \pf_{1:t}(x_t, \theta_t)
    &= \sum_{m=1}^M w_t^{(m)} \phi(x_t; x_t^{a(m)}, \hat{\Sigma}_t^a) \delta_{\theta_t^{f(m)}}(\theta_t),
\end{align*}
with importance weights
\begin{align*}
  % x_t^{a(m)}
  %   &= x_t^{f(m)} + K_t(\bar{\theta}_{t,y}, \hat{\Sigma}_t^f) [y_t - H_t(\bar{\theta}_{t,y}) x_t^{f(m)}] \\
  % \hat{\Sigma}_t^a
  %   &= \hat{\Sigma}_t^f - K_t(\bar{\theta}_{t,y}, \hat{\Sigma}_t^f) \hat{\Sigma}_t^f H_t^T(\bar{\theta}_{t,y}), \\
  w_t^{(m)}
    &= \frac{l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}{\sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}.
\end{align*}

The samples $\{(x_t^{f(m)}, \theta_t^{f(m)})\}_{m=1}^M$ do not generally have uniform weights, i.e., $w_t^{(m)} \neq 1/M$, and, since our goal is to develop an algorithm that more closely resembles the \gls{enkf}, uniform weights are needed. To achieve uniform weights, there are two ways forward at this point:
\begin{enumerate}[leftmargin=*]
  \item Resample the samples as pairs: $(x_t^{a(m)}, \theta_t^{a(m)}) \sim \pf_{1:t}(x_t, \theta_t)$. This particular approach is akin to updating with a \gls{pf} and does not fully take advantage of \gls{enkf}'s ability of not requiring resampling, thus particle collapse is inevitable. %Furthermore, the resampling prevents the direct application of the \gls{enkf} update algorithm and is thus not ideal for plug-and-play. % perturbed observations: need to add stratified or systematic sampling. square-root filters: need to consider weighted averages of the mean and covariance of the updated state, which could be problematic in the case of particle collapse --- effectively one sample, so can't calculate sample covariance.

  \item Approximate the weights $w_t^{(m)}$ to be uniform as done with the \gls{enkf} (see \Cref{eqn1:enkf2-weightapprox}). Recall from the discussion of the \gls{enkf} in \Cref{sect:enkf-reinterpretation} that this approximation consequently does not require resampling and thus avoids particle collapse. However, since the weights are a crucial component of the \gls{pf}'s updating algorithm, this approach will not update the parameter.
\end{enumerate}
In fact, these approaches are what is performed when directly artificial evolution of parameters with the \gls{pf} and \gls{enkf}, respectively. These approaches are unsatisfactory for the reasons discussed in \Cref{chap:param-est}. Since the state and parameter particles are tightly coupled in this particular estimator, we cannot separately use \gls{pf} to update the parameters and \gls{enkf} to update the states. Another approach is required.

