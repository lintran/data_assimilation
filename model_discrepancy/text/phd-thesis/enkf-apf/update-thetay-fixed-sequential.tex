%!TEX root = ../thesis.tex
\synctex=1

\subsection{Sequential update of the state and parameter} \label{sec2:update-sequential}
The approach in this section circumvents the tightly coupled update of the state and parameters by first updating the parameter and then jointly updating the state and parameter. This approach is based on another factorization of the joint analysis distribution, derived by the axiom of conditional probability:
\begin{align}
  \pi_{0:t}(x_t, \theta_t)
    \equiv p(x_t, \theta_t \,|\, y_{0:t})
    &= p(x_t \,|\, \theta_t, y_{0:t}) p(\theta_t \,|\, y_{0:t}).
    % &= \underbrace{\frac{g(y_t \,|\, x_t, \theta_t) p(x_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, \theta_t, y_{0:t-1})}}_{p(x_t \,|\, \theta_t, y_{0:t})}
    % \times
    % \underbrace{\frac{p(y_t \,|\, \theta_t, y_{0:t-1}) p(\theta_t \,|\, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})}}_{p(\theta_t \,|\, y_{0:t})}
\label{eqn2:analysis3}
\end{align}
Similar to the factorization of the joint forecast distribution in \Cref{sec2:forecast}, the conditional dependence of the state on the parameter in the above factorization suggests a two-stage sequential updating algorithm. In this section, we first summarize the algorithm with a single equation and then elaborate on each stage of the algorithm further by deriving estimators relevant to that particular stage.

The single equation that summarizes the algorithm is
\begin{align}
  \pi_{0:t}(x_t, \theta_t)
    &= \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})]}{p(y_t \,|\, \theta_t, y_{0:t-1})} \times \frac{p(y_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} p(x_t, \theta_t \,|\, y_{0:t}),
\label{eqn2:analysis-sequential}
\end{align}
which is derived by applying Bayes theorem to the conditional distributions on the right hand side of \Cref{eqn2:analysis3} or, even more simply, multiplying the joint analysis distribution from \Cref{eqn2:update-thetay-fixed-joint} by the identity constructed by dividing the conditional predictive likelihood $p(y_t \,|\, \theta_t, y_{0:t-1})$ by itself. For clarity, we have strategically separated parts of the above equation with the product symbol (``$\times$'') to symbolize the separation of the two stages of the algorithm:
\begin{enumerate}[leftmargin=*]
  \item %First, an estimator for $p(\theta_t \,|\, y_{0:t})$ is constructed via an update procedure similar to the \gls{pf} (see \Cref{sec:bf} for a review). The update introduces nonuniform weights and thus requires resampling of the parameter particles to avoid resampling the state particles in the second stage of the algorithm. Resampling, however, decouples the pairing of the parameter particles from the state particles. Consequently, new state particles are sampled with the updated parameter particles (from resampling), creating another, but biased, estimator of the joint forecast distribution with an updated parameter. In other words,
  The first-stage of the algorithm updates the parameter via a \gls{pf} update with weights proportional to the conditional predictive likelihood $p(y_t \,|\, \theta_t, y_{0:t-1})$, which is effectively equivalent to deriving a biased estimator of the joint forecast distribution, i.e., the term to the right of the ``$\times$'' symbol. %The sampling in this stage effectively updates the parameter without updating the state by creating another, but biased, estimator of the joint forecast distribution.

  \item %Then, the new pairs of state and parameter particles are updated with an \gls{enkf} update that also corrects for the biasness introduced in the first-stage. In other words,
  The second-stage corrects for the bias introduced in the first-stage via an \gls{enkf} update with weights proportional to the term to the left of the ``$\times$'' symbol. %, i.e., $\phi[y_t; H_t(V_t(\theta_{t,y}) \theta_{t,y})x_t] / p(y_t \,|\, \theta_t, y_{0:t-1})$.
  Not only does this stage correct for the bias, it also jointly updates the state and parameter.
\end{enumerate}

As mentioned earlier, our two-stage algorithm is reminiscent of the \gls{apf} \citep{Pitt1999}, hence we name our algorithm the EnKF-APF algorithm. With the \gls{apf}, the updating and resampling steps of the \gls{bf} are interchanged: the first-stage (the resampling step) uses the measurement to pre-sample particles that are more likely to survive and the second-stage (the updating step) updates the state samples in a manner that corrects for the bias introduced by the pre-sampling step in the first-stage. Like the first-stage of the \gls{apf}, the first-stage of our algorithm pre-samples pairs of state and parameter samples with the conditional predictive likelihood, effectively updating \textit{only} the parameter. %It is a crucial step that provides equally-weighted samples from the joint forecast distribution $p(x_t, \theta_{t} \,|\, y_{0:t-1})$.
The new samples provide a biased estimator of the joint forecast distribution. Though biased, it is crucial to avoid resampling when applying the \gls{enkf} update to the pre-sampled particles in the second-stage of the algorithm. The second-stage then performs an \gls{enkf} update, which serves two purposes: (1) it updates the state particles with existing implementations of the \gls{enkf} and (2) it corrects for the bias introduced in the first-stage. Consequently, the algorithm provides simple random samples from an estimator of the joint analysis distribution $p(x_t, \theta_t \,|\, y_{0:t})$. %\footnote{This assumes that the empirical distribution constructed with analysis samples from \gls{enkf} converges to the optimal distribution as the number of samples goes to infinity. This has only been proven for linear systems \citep{LeGland2009,Mandel2009,Kwiatkowski2014} and has not been proven for nonlinear systems.}.
The algorithm, however, requires two evaluations of the state transition per sample, increasing the computational time of the algorithm, but it is a necessary expense to avoid the issues faced by applying artificial evolution of parameters with the \gls{enkf}. We further elaborate on each step by deriving the estimators necessary for each stage of the algorithm.

Let's begin the development of the first-stage of the algorithm by deriving an estimator for the conditional distribution of the parameter. Applying Bayes theorem, the conditional distribution of the parameter is
\begin{align}
  \pi_{0:t}(\theta_t)
    &\equiv p(\theta_t \,|\, y_{0:t}) \notag\\
    &= \frac{p(y_t \,|\, \theta_t, y_{0:t-1}) p(\theta_t \,|\, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} \label{eqn2:update-cond-param}\\
    % &= \frac{\left[ \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t \,|\, \theta_t, y_{0:t-1}) dx_t \right] p(\theta_t \,|\, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} \\
    &= \frac{\int \phi[y_t; H_t(\theta_{t,y}) x_t , V_t(\theta_{t,y})] p(x_t, \theta_t \,|\, y_{0:t-1}) dx_t}{p(y_t \,|\, y_{0:t-1})}.  \notag
\end{align}
The conditional distribution suggests updating the parameter with weights proportional to the conditional predictive likelihood\index{conditional predictive likelihood} $p(y_t \,|\, \theta_t, y_{0:t-1})$. Plugging in our estimators, we derive an estimator of the conditional parameter distribution:
\begin{align*}
  \pft_{0:t}(\theta_t)
    &\equiv \frac{\int \phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \pf_{0:t-1}(x_t, \theta_t) dx_t}{\pf_{0:t-1}(y_t)} \\
    &= \frac{\frac{1}{M} \sum_{m=1}^M \left[ \int \phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) dx_t \right] \delta_{\theta_t^{f(m)}}(\theta_t)}{\frac{1}{M}\sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)} \\
    &= \frac{\sum_{m=1}^M l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f) \delta_{\theta_t^{f(m)}}(\theta_t)}{\sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)}
\end{align*}
Let $w_t^{(m)} = l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f) / \sum_{n=1}^M l_t(x_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)$. Then,
\begin{align*}
  \pft_{0:t}(\theta_t)
    &= \sum_{m=1}^M w_t^{(m)} \delta_{\theta_t^{f(m)}}(\theta_t)
\end{align*}
Each parameter sample has a nonuniform weight---a weight that is undesirable to carry over to the state update in the second-stage of the algorithm. Therefore, we resample the parameter samples \textit{before} updating the state: $\theta_t^{a(m)} \sim \pft_{0:t}(\theta_t)$ for each $m = 1,...,M$. These samples provide a new estimator of the conditional parameter distribution:
\begin{align*}
  \pf_{0:t}(\theta_t)
    &= \frac{1}{M} \sum_{m=1}^M \delta_{\theta_t^{a(m)}}(\theta_t).
\end{align*}

Though resampling parameter samples avoids resampling the state samples in the second-stage of the algorithm, it introduces a new problem: as with forecasting, the state and parameter samples should be considered as a sample from the joint forecast distribution \textit{as pairs}. However, since only the parameter samples are resampled, the resampling disentangled the pairings between the parameter and the state samples. The problem is remedied by sampling from the conditional forecast distribution of the state again---this time with the resampled parameter samples:
\begin{align*}
  \tilde{x}_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{a(m)})
\end{align*}
for each $m = 1,...,M$. This step is equivalent to sampling from an estimator of the following distribution:
\begin{align*}
  %p(x_t \,|\, \theta_t, y_{0:t-1}) p(\theta_t \,|\, y_{0:t-1})
  \frac{p(y_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} p(x_t, \theta_t \,|\, y_{0:t-1}),
\end{align*}
i.e., the term to the right of the ``$\times$'' symbol in the single equation that describes the algorithm (\Cref{eqn2:analysis-sequential}). Effectively, the samples are biased samples from the forecast distribution $p(x_t, \theta_t \,|\, y_{0:t-1})$, biased by the weight $p(y_t \,|\, \theta_t, y_{0:t-1})$ used to update the parameter. As in \Cref{sec2:forecast}, we construct a hybrid estimator for the joint forecast distribution with these new samples:
\begin{align}
  \pft_{0:t-1}(x_t, \theta_t)
    = \frac{1}{M} \sum_{m=1}^M \phi(x_t; \tilde{x}_t^{f(m)}, \hat{\tilde{\Sigma}}_t^f) \delta_{\theta_t^{a(m)}}(\theta_t), \label{eqn2:forecast-biased}
\end{align}
where $\hat{\tilde{\Sigma}}_t^f$ is the sample covariance calculated from the state particles $\{\tilde{x}_t^{f(m)}\}_{m=1}^M$. Similarly, a new estimator of the conditional predictive distribution is derived:
\begin{align*}
  \pft_{0:t-1}(y_t \,|\, \theta_t)
    &\equiv \frac{1}{M} \sum_{m=1}^M l_t(\tilde{x}_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f).
\end{align*}

\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
  \textbf{Input}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$
    \item $\{(x_t^{f(m)}, \theta_t^{f(m)})\}_{m=1}^M \approxdist p(x_t, \theta_t \,|\, y_{0:t-1})$ from \Cref{alg:forecast}
    % \item $H_t \equiv H_t(\bar{\theta}_{t,y})$: mapping between state and observations
    % \item $V_t \equiv V_t(\bar{\theta}_{t,y})$: measurement covariance
    % \item $y_t$: observation
  \end{itemize}
  ~\\
  \textbf{Output}: $\{(x_t^{a(m)}, \theta_t^{a(m)})\}_{m=1}^M \approxdist p(x_t, \theta_t \,|\, y_{0:t})$

  \begin{enumerate}[leftmargin=*]
    \item Update $\Theta_t$:
    \begin{enumerate}[leftmargin=*,noitemsep]
      \item \label{item2:fixed-thetay-param-weights} Calculate unnormalized weights\footnote{The $l_t(\cdot)$ term is defined in \Cref{eqn2:cond-pred-lik}.}:
      \begin{align*}
        \tilde{w}_t^{(m)}
          &= l_t(x_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\Sigma}_t^f)
      \end{align*}
      for each $m=1,...,M$, where $\hat{\Sigma}_t^f$ is the (tapered) sample covariance calculated from $\{x_t^{f(m)}\}_{m=1}^M$. Normalize weights: $w_t^{(m)} = \tilde{w}_t^{(m)} / \sum_{n=1}^M \tilde{w}_t^{(n)}$ for each $m=1,...,M$.
      % \item Resample $\{\theta_t^{a(m)}\}_{m=1}^M$ from $\{(\theta_t^{f(m)}, w_t^{(m)})\}_{m=1}^M$.
      \item Sample
      \begin{align*}
        \theta_t^{a(m)}
          \sim %\pft_{0:t}(\theta_t) =
          \sum_{m=1}^M w_t^{(m)} \delta_{\theta_t^{f(m)}}(\theta_t)
      \end{align*}
      for each $m=1,...,M$.
    \end{enumerate}
    \item Update $(X_t, \Theta_t)$:
    \begin{enumerate}[leftmargin=*,noitemsep]
      \item Sample $\tilde{x}_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{a(m)})$.
      \item Sample $\{x_t^{a(m)}\}_{m=1}^M$ using Step 3 of \Cref{alg:enkf} with forecast state samples $\{\tilde{x}_t^{f(m)}\}_{m=1}^M$, measurement mapping $H_t = H_t(\bar{\theta}_{t,y})$, and measurement variance $V_t = V_t(\bar{\theta}_{t,y})$.
    \end{enumerate}
  \end{enumerate}
\end{minipage}
}
\captionnew{EnKF-APF: Update step with fixed measurement parameter $\Theta_{t,y}$}{}
\label{alg:enkf-apf-thetay-known}
\end{alg}

In the second-stage of the algorithm, the pairs of state and parameter samples are jointly updated in a fashion similar to the joint update discussed in \Cref{sec2:update-joint}. The joint update also corrects for the bias introduced by the first-stage of the algorithm. Applying Bayes' theorem to the conditional state distribution, we have
\begin{align}
  \pi_{0:t}(x_t \,|\, \theta_t)
    \equiv p(x_t \,|\, \theta_t, y_{0:t})
    &= \frac{p(y_t \,|\, x_t, \theta_t, y_{0:t-1}) p(x_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, \theta_t, y_{0:t-1})} \notag \\
    &= \frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, \theta_t, y_{0:t-1})}, \label{eqn2:update-cond-state}
\end{align}
where the last equality comes from the measurement independence assumption from \Cref{eqn2:obs-indep}. Plugging in estimators of the conditional parameter and state distributions from \Cref{eqn2:update-cond-state,eqn2:update-cond-param}, respectively, into the joint analysis distribution of \Cref{eqn2:analysis3}, \Cref{eqn2:analysis-sequential} that summarized our algorithm is derived. We repeat it here for convenience:
\begin{align*}
  \pi_{0:t}(x_t, \theta_t)
    &= \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})]}{p(y_t \,|\, \theta_t, y_{0:t-1})} \times
      \frac{p(y_t \,|\, \theta_t, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})} p(x_t, \theta_t \,|\, y_{0:t}).
\end{align*}
The estimator in \Cref{eqn2:forecast-biased} is an estimator of the term to the right of the ``$\times$'' symbol, thus an estimator for the joint analysis distribution is derived in a similar manner as in \Cref{sec2:update-joint}:
\begin{align*}
  \pft_{0:t}(x_t, \theta_t)
    &\equiv \frac{\phi[y_t; H_t(\bar{\theta}_{t,y}) x_t, V_t(\bar{\theta}_{t,y})] \pft_{0:t-1}(x_t, \theta_t)}{\pft_{0:t-1}(y_t \,|\, \theta)} \\
    &= \sum_{m=1}^M w_t^{(m)} \phi(x_t; x_t^{a(m)}, \hat{\Sigma}_t^a) \delta_{\theta_t^{a(m)}}(\theta_t),
\end{align*}
where
\begin{align}
  x_t^{a(m)}
    &= \tilde{x}_t^{f(m)} + K_t(\bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f) [y_t - H_t(\bar{\theta}_{t,y}) \tilde{x}_t^{f(m)}] \label{eqn2:state-sample}\\
  \hat{\Sigma}_t^a
    &= \hat{\tilde{\Sigma}}_t^f - K_t(\bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f) \hat{\tilde{\Sigma}}_t^f H_t^T(\bar{\theta}_{t,y}), \notag\\
  w_t^{(m)}
    &= \frac{l_t(\tilde{x}_t^{f(m)}, \bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f)}{\sum_{n=1}^M l_t(\tilde{x}_t^{f(n)}, \bar{\theta}_{t,y}, \hat{\tilde{\Sigma}}_t^f)}. \notag
\end{align}
Like in \Cref{sec2:update-joint}, the pairs of state and parameter samples have unequal weights and we suggested two ways forward: either jointly update the pairs of state and parameter samples via a \gls{pf} or \gls{enkf} update. We hesitated to move forward with either approach because the \gls{pf} update would lead to particle collapse and the \gls{enkf} update requires approximations that were not ideal for updating the parameter samples. However, at this stage of the sequential updating algorithm, we do not share the same hesitation about the \gls{enkf} update as before: the parameter has already been updated in the construction of the new estimator of the forecast distribution, $\pft_{0:t-1}(x_t, \theta_t)$. Therefore, introducing the weight approximation from the \gls{enkf} at this stage updates the state in a manner faithful to an \gls{enkf} update. Therefore, pairs of state and parameter samples are jointly updated by directly applying the \gls{enkf} update (Step 3 of \Cref{alg:enkf}). \Cref{alg:enkf-apf-thetay-known} summarizes the sequential updating algorithm.

\subsubsection{Another connection: Rao-Blackwellised particle filtering}
Up to this point, we only focused on the relationship of the EnKF-APF algorithm to the \gls{apf}. We make a connection to another filtering algorithm: Rao-Blackwellised particle filtering. Suppose that the hidden state $X_t$ is divided into two groups $X_t^{(1)}$ and $X_t^{(2)}$ with $p(x_t \,|\, x_{t-1}) = p(x_t^{(1)} \,|\, x_{t-1}^{(1)}, x_{t-1:t}^{(2)}) p(x_t^{(2)} \,|\, x_{t-1}^{(2)})$. Further suppose that the posterior distribution $p(x_{0:t}^{(1)} \,|\, x_{0:t}^{(2)}, y_{1:t})$ is analytically tractable for collected measurements $y_{1:t}$. Rao-Blackwellised particle filtering is an algorithm to marginalize out the part of the state that is more analytically tractable, $X_t^{(1)}$, and then apply a particle filtering algorithm to sample and then calculate a lower variance estimator of the state with reduced dimensions, $X_t^{(2)}$ \citep{Doucet2000b,Chen2003}. The algorithm is based on the Rao-Blackwell theorem, which states that the expected value of an unbiased estimator conditioned on its sufficient statistic has a lower variance than the original estimator (see \citet[Theorem 7.3.17]{Casella2002} for a formal statement of the theorem). With the EnKF-APF, there is a natural division of the hidden variables: the parameter $\Theta_t$ and the state $X_t$. The first-stage of the algorithm is Rao-Blackwellised particle filtering of the parameter $\Theta_t$. However, unlike Rao-Blackwellised particle filtering, the posterior distribution of the marginalized hidden variable, the often high-dimensional state $X_t$, is not analytically tractable and thus the more practically effective \gls{enkf} is used to sample $X_t$ in the second-stage of the algorithm.



