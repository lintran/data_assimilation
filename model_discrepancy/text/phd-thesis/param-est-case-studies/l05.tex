%!TEX root = ../thesis.tex
\subsection{Lorenz 2005} \label{sec:l05-enkf-apf}

While the EnKF-APF has performed well for the Lorenz 1963 system, good results are not observed until an ensemble size of 48 is used for many measurement models---an ensemble size 16 times larger than the state dimensions! If we extrapolated this to high-dimensional systems, the ensemble size needed for good results quickly becomes computationally infeasible, thus one may conclude that the EnKF-APF is not a practical method for parameter estimation. In this section, we demonstrate that the extrapolation is unfounded and show that the EnKF-APF is perfectly capable of capturing true parameter values of the Lorenz 2005-II system for small ensemble sizes, oftentimes better than the \gls{enkf}. Furthermore, the previous demonstration with the Lorenz 1963 system only illustrates the ability of EnKF-APF to estimate state parameters, i.e., \Cref{alg:enkf-apf-thetay-known}. We additionally estimate a measurement parameter with the Lorenz 2005 system, thus demonstrating the full capabilities of \Cref{alg:enkf-apf-thetay-est}.

Before discussing the results, we detail the settings used in the parameter estimation algorithm. Like the Lorenz 1963 demonstration, all parameter estimation runs have the same underlying data generation process; only samples from the initial condition and the initial parameter values to start the algorithm are different between runs.

\subsubsection{State-space model and data generation}
Examining the Lorenz 2005-II equations (\Cref{eqn:lorenz05-2}), there are three state parameters, i.e., $N$, $K$, and $F$. The parameter $N$ controls resolution of the mathematical grid. In practice, the resolution is chosen based on computational resources. For this reason, we set $N$ to be the default value of 960 (and thus the state dimension is $d_x = 960$) and aim to estimate the parameters $K$ and $F$. Let $R = (r_1, ..., r_{d_x})$ be the vector of the state locations equispaced on a spherical latitude band as illustrated in \Cref{fig1:latitude-grid}, where $r_i$ is the location of the $i$th state located at an angle of $\frac{(i-1)}{d_x} \times 2\pi$ for all $i = 1,..., d_x$.% and the shortest arc length between two nearby locations is $\Delta r$, i.e., $|r_i - r_j| = \Delta r$ for all $|i-j|=1$.

When not estimating an inflation value, the state transition model is
\begin{align*}
  x_{t_n}
    &= m_{t_n} \left( x_{t_{n-1}}; K, F \right);
\end{align*}
and when estimating an inflation value, the state transition model is
\begin{align*}
  x_{t_n}
    &= \mu_{t_n} + \lambda \left[m_{t_n} \left( x_{t_{n-1}}; K,F \right) - \mu_{t_n} \right].
\end{align*}
The discrete state transition model $m_{t_n}$ is defined to be
\begin{align*}
  %\begin{bmatrix} \tilde{x}_{t_n} \\ \tilde{y}_{t_n} \\ \tilde{z}_{t_n} \end{bmatrix}
    % &= \mathcal{I}_W \left( \begin{bmatrix} x_{t_{n-1}} \\ y_{t_{n-1}} \\ z_{t_{n-1}} \end{bmatrix}, \sigma \right) + O(e(dt)),
  m_{t_n}(\cdot; K, F, dt) = \mathcal{I}_W ( \cdot; K,F ) + O(e(dt))
\end{align*}
where $W$ is the continuous state transition model for the Lorenz 2005-II system, i.e., \Cref{eqn1:l05-cont-state-transition-model}, and $e(\cdot)$ and $dt$ are defined in \Cref{sec1:state-transition-examples}. Depending on whether the inflation value is estimated or not, the state parameter being estimated is either $\theta_x = (K, F, \lambda)$ or $\theta_x = (K,F)$, respectively.

The measurement model is
\begin{gather*}
  w_{t_n} = H(S_i, \gamma_i) x_{t_n} + \epsilon_{t_n}, \quad
  \epsilon_{t_n} \sim \normal(0, U_i),
\end{gather*}
where the measurement mapping $H(s_i, \gamma_i)$ depends on the locations $S_i$ where the measurements are collected and the halfwidth parameter $\gamma_i$. Recall that the linear measurement mapping is a matrix of weights that interpolates the state values to the measurement values: state locations that are closer to the measurement location are assigned higher weights than those further away. The halfwidth parameter $\gamma_i$ controls the distance at which the weights are zero: specifically, the weights are zero for distances beyond $2\gamma_i$. The weights of the measurement mappings are constructed with the Gaspari-Cohn function \citep{Gaspari1999} with five interpolating points.

We choose three measurement mappings. The first measurement mapping is the fully observed system, where the measurement mapping is simply the identity matrix $I_{d_x}$. In this case, the measurement locations exactly match the state locations, i.e., $S_1 = R$, and there is no interpolation between the state and measurement locations, i.e., $\gamma_1 = 0$. The next two measurement mappings are partially observed systems. The first partially observed system measures every other state location, i.e., $S_2 = (r_1, r_3, r_5, ..., r_{d_x-1})$, without interpolation, i.e., $\gamma_2 = 0$. Specifically, its measurement mapping is a $(d_x/2) \times d_x$ matrix that takes the following form:
\begin{align*}
  H(s_2, \gamma_2) &\equiv \begin{bmatrix}
    1 & 0 & 0 & 0 & 0 & \cdots & 0 \\
    0 & 0 & 1 & 0 & 0 & \cdots & 0 \\
    0 & 0 & 0 & 0 & 1 & \cdots & 0 \\
    \vdots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots\\
    0 & 0 & 0 & 0 & 0 & \cdots & 1
  \end{bmatrix}.
\end{align*}
The second partially observed model has $d_x/2$ measurement locations randomly chosen on the latitude band via a uniform distribution, i.e., the $i$th element of $S_3$ is located at an angle of $U_i \times 2\pi$ where $U_i \sim \unif(0,1)$ for all $i = 1,...,d_x/2$, and the halfwidth is chosen to be $\gamma_3=\frac{5}{960} \times 2\pi$. The variance of the measurement noise is taken to be the identity, i.e., $U_1 = I_{d_x}$ and $U_2 = U_3 = I_{d_x/2}$.

The initial condition $x_{t_0}$ is generated by taking a random state from a uniform distribution and integrating it forward with a large $\Delta t$. The state $x_{t_n}$ is generated by taking the initial condition $x_{t_0}$ and integrating it sequentially to collect the equivalent of one year's worth of measurements with a measurement collected once per day, i.e., $N=365$ times with a forecast lead time of $\Delta t_n = 0.24$ for all $n$. Samples from the initial condition are generated from a standard multivariate normal distribution with mean $x_{t_0}$.

\subsubsection{EnKF settings}
We apply the square-root filter with localization. The localization halfwidth is $\frac{5}{960} \times 2\pi$ using the Gaspari-Cohn function to construct the tapering matrix. Ensemble sizes are set to be 51, 101, and 201. Parameter estimation is run both with and without inflation. When inflating, the forecast ensemble is inflated in two ways: with a fixed inflation of $\lambda = 1.5$ and with inflation values estimated with the default \gls{dart} settings as described in \Cref{sec:l63-param-est-fails}.

\subsubsection{Parameter estimation settings}
We apply the iterated filter from \citet{Ionides2015} with both the EnKF-APF and \gls{enkf} to estimate parameters. We are interested in estimating the state parameters $K$ and $F$ and the measurement parameter $\gamma_3$. However, the parameter $\gamma_3$ is only estimated with the EnKF-APF and is fixed with the \gls{enkf}. We choose the perturbation density $q(\theta_{t_n} \,|\, \theta_{t_{n-1}})$ to be a multivariate truncated normal distribution with mean $\theta_{t_{n-1}}$. \Cref{tab2:l05-if-settings} lists the initial standard deviations and the constraints. The correlation between the parameters is set to be zero, thus the multivariate truncated normal distribution is simply a product of univariate truncated normal distributions. To respect the integer-valued nature of $K$, proposals are rounded to the nearest integer. %The settings for the inflation value is taken to be the default \gls{dart} settings as described in \Cref{sec:l63-param-est-fails}.
\begin{table}[h!]%\begin{wraptable}{r}{.5\textwidth}
\centering
\renewcommand{\arraystretch}{1.75}
\begin{tabular}{ccccc}
  \hline
  parameter & truth & initial sd & minimum & maximum \\ \hline
  $K$ & 32 & 0.5 & 1 & 100 \\
  $F$ & 15 & 0.5 & -50 & 50 \\
  $\lambda$ & 1 & 0.6 & 1 & 20 \\
  $\gamma_3$ & $\frac{5}{d_x} \pi$ & $\frac{1}{d_x} \pi$ & 0 & $\pi$ \\ \hline
            % &   & 0.2 & 1 & 20 \\
            % &   & 0.6 & 1 & 5 \\
            % &   & 0.2 & 1 & 5 \\
            % &   & 0.1 & 1 & 5 \\ \hline
\end{tabular}
\captionnew{Lorenz 2005-II parameter estimation settings.}{The perturbation density $q(\theta_{t_n} \,|\, \theta_{t_{n-1}})$ is a product of truncated normal distributions with the above parameters.}
\label{tab2:l05-if-settings}
\end{table}

Each run of the iterated filter is initialized with the true value perturbed by the initial standard deviation multiplied by two and an inflation value of one. The iterated filter is run with 25 iterations and the \gls{mle} is reported to be the median of the final ensemble.

\subsubsection{Results}
\begin{figure}[b]
  \begin{subfigure}[t]{\textwidth}
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-apf-inf15-zoomed}
  \captionnew{Fixed inflation, $\lambda=1.5$}{}
  \label{fig2:l05-enkfapf-mle-fix}
  \end{subfigure}
  \vspace{1em}

  \begin{subfigure}[b]{\textwidth}
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-apf-inf00-zoomed}
  \captionnew{Estimated inflation}{}
  \label{fig2:l05-enkfapf-mle-est}
  \end{subfigure}
  \captionnew{Lorenz 2005-II: distribution of MLEs as estimated by EnKF-APF.}{Each violin plot illustrates the distribution of the MLEs of the parameters labelled to the right of each row. The estimated values come from 20 runs of the iterated filter varied by ensemble size and measurement model. %The \glspl{mle} are estimated by applying the iterated filter \citep{Ionides2015} with the \gls{enkf} to estimate $\theta_x$. The number of iterations per iterated filter is 50.
  Ensemble size is varied across the $x$-axis of each plot and the measurement model are varied across columns of the plots. Each figure has a different inflation setting as indicated by the caption. The green horizontal line indicates the true parameter value and the red lines, if shown, indicate the constraints of the perturbation density as specified in \Cref{tab2:l05-if-settings}.  A red dot, if shown, is a jittered value of an MLE that is estimated to be outside of the constraints specified.}
  \label{fig2:l05-enkfapf-mle}
\end{figure}

\begin{figure}[b]
  \begin{subfigure}[t]{\textwidth}
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-etkf-inf15-zoomed}
  \captionnew{Fixed inflation, $\lambda=1.5$}{}
  \label{fig2:l05-enkf-mle-fix}
  \end{subfigure}
  \vspace{1em}

  \begin{subfigure}[b]{\textwidth}
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-etkf-inf00-zoomed}
  \captionnew{Estimated inflation}{}
  \label{fig2:l05-enkf-mle-est}
  \end{subfigure}
  \captionnew{Lorenz 2005-II: distribution of MLEs as estimated by EnKF.}{The settings here are the same as in \Cref{fig2:l05-enkfapf-mle}.}
  \label{fig2:l05-enkf-mle}
\end{figure}

\begin{figure}[b]
  \begin{subfigure}[t]{\textwidth}
  \centering
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-K-inf00-zoomed}
  \captionnew{$K$}{}
  \label{fig2:l05-compare-mle-K}
  \end{subfigure}
  \vspace{1em}

  \begin{subfigure}[b]{\textwidth}
  \centering
  \includegraphics{param-est-case-studies/figures/lorenz05/mle-F-inf00-zoomed}
  \captionnew{$F$}{}
  \label{fig2:l05-compare-mle-F}
  \end{subfigure}
  \captionnew{Lorenz 2005-II: comparison of MLEs as estimated by EnKF-APF and EnKF.}{The plots are zoomed-in versions of \Cref{fig2:l05-enkfapf-mle-est,fig2:l05-enkf-mle-est} (with estimated inflation). Measurement model $i=3$ is omitted because the results are not comparable between filters: $\gamma_3$ is estimated with EnKF-APF and is fixed with \gls{enkf}.}
  \label{fig2:l05-compare-mle}
\end{figure}

% \begin{figure}[b]
%   \begin{subfigure}[t]{\textwidth}
%   \centering
%   \includegraphics{param-est-case-studies/figures/lorenz05/mle-K-inf15-zoomed}
%   \captionnew{$K$}{}
%   \label{fig2:l05-compare-mle-K}
%   \end{subfigure}
%   \vspace{1em}

%   \begin{subfigure}[b]{\textwidth}
%   \centering
%   \includegraphics{param-est-case-studies/figures/lorenz05/mle-F-inf15-zoomed}
%   \captionnew{$F$}{}
%   \label{fig2:l05-compare-mle-F}
%   \end{subfigure}
%   \captionnew{Lorenz 2005-II: comparison of MLEs as estimated by EnKF-APF and EnKF.}{The settings here are the same as in \Cref{fig2:l05-enkfapf-mle}. The plots are zoomed-in versions of \Cref{fig2:l05-enkfapf-mle-est,fig2:l05-enkf-mle-est} (with estimated inflation). Measurement model $i=3$ is omitted because the results are not comparable between filters, i.e., $\gamma_3$ is estimated with EnKF-APF and is fixed with \gls{enkf}.}
%   % \label{fig2:l05-enkf-mle}
% \end{figure}

The results without inflation are omitted because the parameter estimation procedure failed to capture the true values with both filters. This is as we suspected, even before running the parameter estimation algorithm, since the \gls{enkf} requires inflation for good performance when filtering high-dimensional state-spaces. \Cref{fig2:l05-enkfapf-mle,fig2:l05-enkf-mle} show the results with inflation from both the EnKF-APF and \gls{enkf}, respectively.

With the EnKF-APF, the results show that the EnKF-APF consistently captures the true parameter values well for all measurement models and ensemble sizes, including the small ensemble size of 51! The state parameter $K$ is captured almost perfectly (top rows of both \Cref{fig2:l05-enkfapf-mle-fix} and \Cref{fig2:l05-enkfapf-mle-est}): most MLEs are exactly the true value of 32 with a few MLEs with values of 31, 33, or 34---values very close to the true value. The state parameter $F$ is often captured well (middle rows of both \Cref{fig2:l05-enkfapf-mle-fix} and \Cref{fig2:l05-enkfapf-mle-est}): the range of MLEs cover the the true value. The range of estimated values of $F$ tend to be larger with fixed inflation than with estimated inflation. Similarly, the measurement parameter $\gamma_3$ is captured better with estimated inflation than with fixed inflation (bottom rows of both \Cref{fig2:l05-enkfapf-mle-fix} and \Cref{fig2:l05-enkfapf-mle-est}). In fact, with estimated inflation, the distribution of the estimated values of $\gamma_3$ show the characteristic decrease in the range of estimated values with larger ensemble sizes, indicating that parameter estimation for the Lorenz 2005-II system is better performed when the inflation value is estimated. The ability of the EnKF-APF to estimate the halfwidth parameter $\gamma_3$ demonstrates its full capabilities: the algorithm is able to successfully estimate both state \textit{and} measurement parameters.

On the other hand, the results show that the \gls{enkf} is generally able to capture the true parameter values but it does sometimes have trouble. Oddly, the \gls{enkf} is able to better capture the true parameter values in partially observed systems (measurement models $i=2,3$ in the latter two columns of \Cref{fig2:l05-enkf-mle}) than the fully observed system ($i=1$, first column of \Cref{fig2:l05-enkf-mle}). We suspect this oddity is due to the sensitivity of the \gls{enkf} linear update: the parameter is updated by the conditional correlations between the state and the parameter that are perhaps unimportant. While there were no failures in the parameter estimation procedure, a few runs estimated values of $K$ to be outside of its pre-specified constraints.

\Cref{fig2:l63-mle-compare} compares the results from the EnKF-APF and EnKF. The plots in this figure are zoomed-in versions of the previously examined figures, i.e., \Cref{fig2:l05-enkfapf-mle-est,fig2:l05-enkf-mle-est}, omitting the results for the measurement model $i=3$. The last measurement model is omitted because the results are not comparable between the two filters: $\gamma_3$ is estimated with the EnKF-APF and is fixed with the EnKF. For the fully observed system ($i=1$, first column of \Cref{fig2:l63-mle-compare}), the EnKF-APF clearly captures the true parameter values of $K$ and $F$ better than the EnKF. For the partially observed system ($i=2$, second column of \Cref{fig2:l63-mle-compare}), the EnKF-APF better captures the true parameter value of $K$ and both filters capture $F$ similarly. Overall, the EnKF-APF is better at capturing the true parameter values than the \gls{enkf}.

