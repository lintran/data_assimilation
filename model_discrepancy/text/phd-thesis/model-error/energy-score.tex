%!TEX root = ../thesis.tex
% Suppose $x$ is a prediction of the observed value $y$. A simple evaluation metric is the \textit{root mean square error}\index{root mean square error}\index{RMSE}:
% \begin{align*}
%   \text{RMSE}(x,y) = \sqrt{ \frac{1}{n} \norm{y - x}_2^2 },
% \end{align*}
% or the \textit{mean absolute error}\index{mean absolute error}\index{MAE}:
% \begin{align*}
%   \text{MAE}(x,y) = \frac{1}{n} \norm{y - x}_1,
% \end{align*}
% where $\norm{x}_d$ is the $d$-norm and $n$ is the size of the vector $y$ and $x$. While great metrics for nonprobabilistic forecasts, the forecasts coming from \gls{enkf} are probabilistic, in the sense that samples from the forecast distribution are being drawn, thus giving providing uncertainty.

Since the \gls{enkf} provides samples from the forecast distribution, it is desirable to evaluate forecasts on both how close the forecast is to the observation (\textit{accuracy}\index{accuracy!versus precision}) and the uncertainty in the forecasts (\textit{precision}\index{precision!versus accuracy}). The \gls{crps}\index{continuous ranked probability score}\index{CRPS}, first introduced by \citet{Matheson1976}\footnote{See \citet{Wilks2011a} for a textbook review.}, is one such metric for scalar forecasts and is widely used in the atmospheric sciences. Though the forecasts in our demonstrations are multidimensional, we first introduce the \gls{crps} and provide some intuition behind how the metric measures forecast skill before discussing its generalization to multidimensional forecasts.

Let $y$ be the observed value and $P$ be the predictive distribution. The \gls{crps} is defined as
\begin{align*}
  \text{CRPS}(P, y) = \int_{-\infty}^{\infty} [F(x) - \mathbb{I}(y \geq x)]^2 dx,
\end{align*}
where $F$ is the \gls{cdf} of the distribution $P$ and $\mathbb{I}$ is the indicator function. When the predictive distribution comes from iid samples $x_i \stackrel{iid}{\sim} P, i = 1,...,n$, denoted as $P_{ens}$, the \gls{cdf} $F$ is either replaced with the empirical \gls{cdf}, $\hat{F}_n(x) = \frac{1}{n} \sum_{i=1}^n \mathbb{I}(x_i \leq x)$, or approximated with the \gls{cdf} of a Gaussian with the first two moments given by the sample mean and variance. \Gls{crps} is a negatively oriented metric, meaning that a lower value indicates a better prediction.

\Cref{fig3:crps-illustration} provides some intuition for the \gls{crps} metric: it illustrates three different predictions $X_i, i = 1,2,3$, for the observed value $y=0$ and their corresponding \gls{crps} statistic. The distribution of all three predictions are Gaussian with different means and variances. Let's examine the first prediction $X_1$ (first row of plots) with a mean that matches the observation exactly and a spread of one standard deviation around the observation, i.e., $X_1 \sim \normal(0,1^2)$. The prediction has a \gls{crps} score is 0.234 and is represented by the shaded region in the topright figure. The second prediction (green in the second row of plots) represents a less accurate prediction with equal precision, i.e., the mean prediction is no longer zero but has the same variance: $X_2 \sim \normal(1, 1^2)$. As expected, the \gls{crps} score increases to 0.602, penalizing the prediction for being less accurate. On the other hand, the third prediction (blue in the second row of plots) represents a prediction with the same accuracy as the first but with lower precision, i.e., the mean prediction is zero but has a larger variance of 2.5 times the spread of the original prediction: $X_3 \sim \normal(0, 2.5^2)$. The \gls{crps} score (0.584) also increases in this case, penalizing the prediction for being less precise.

\begin{figure}[p]
  \definecolor{color1}{HTML}{F8766D}
  \definecolor{color2}{HTML}{00BA38}
  \definecolor{color3}{HTML}{619CFF}

  % http://tex.stackexchange.com/questions/148681/how-to-draw-legend-lines-in-caption-text-without-including-original-plots-tikz
  \scalebox{0}{%
  \begin{tikzpicture}
    \begin{axis}[hide axis]
      \addplot[color=color1,solid,line width=2pt,forget plot](0,0);\label{line1}
      \addplot[color=color2,solid,line width=2pt,forget plot](0,0);\label{line2}
      \addplot[color=color3,solid,line width=2pt,forget plot](0,0);\label{line3}
      \end{axis}
  \end{tikzpicture}%
  }

  \begin{center}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{|ccccc|}
      \hline
      i & $X_i$              & legend      & $\text{CRPS}(F_{X_i}, y)$ & \\ \hline
      1 & $\normal(0,1^2)$   & \ref{line1} & 0.234 & \\
      2 & $\normal(1,1^2)$   & \ref{line2} & 0.602 & less accurate than $X_1$\\
      3 & $\normal(0,2.5^2)$ & \ref{line3} & 0.584 & less precise than $X_1$ \\ \hline
    \end{tabular}
  \end{center}
  \vspace{3em}

  \includegraphics[page=1]{model-error/figures/crps/crps}
  ~
  \includegraphics[page=2]{model-error/figures/crps/crps}
  \vspace{3em}

  \includegraphics[page=3]{model-error/figures/crps/crps}
  ~
  \includegraphics[page=4]{model-error/figures/crps/crps}

  \captionnew{CRPS: an illustration.}{Three different predictions $X_i, i = 1,2,3$, of the observed value $y=0$ and their corresponding \gls{crps} score. The first prediction $X_1$ has a mean that matches the observation exactly with a spread of one. The other two predictions perturbs the first prediction's accuracy ($X_2$) and precision ($X_3$)---both undesirable and is thus penalized with higher \gls{crps} scores.}
  \label{fig3:crps-illustration}
\end{figure}

\citet{Gneiting2007a} showed that the \gls{crps} is equivalently written as
\begin{align*}
  \text{CRPS}(P,y) = \e_P|X-y| - \frac{1}{2} \e_P |X-X'|,
\end{align*}
where $X'$ is an independent random variable with the same distribution as $X$. With this insight, \citet{Gneiting2008} proposed the \gls{es} as a generalization of the \gls{crps} for multidimensional vectors:
\begin{align*}
  \text{ES}(P,y) = \e_P \norm{X - y} - \frac{1}{2} \e_P \norm{X - X'},
\end{align*}
where $\norm{\cdot}$ is the Euclidean norm. When the predictive distribution comes from a sample of size $n$, i.e., $P_{ens}$, the \gls{es} reduces to
\begin{align*}
  \text{ES}(P_{ens},y) = \frac{1}{n} \sum_{j=1}^n \norm{x_j - x} - \frac{1}{2n^2} \sum_{i=1}^n \sum_{j=1}^n \norm{x_i - x_j}.
\end{align*}
We use this particular metric to evaluate the improvement in forecast skill of the improved model $\widetilde{W}$. %In the demonstrations in the next chapter, the true value at time $t$ is known and thus the forecasts are evaluated against the true value instead of the measurement as discussed in this section.

% Cramer-von Mises criterion, Kolmogorov–Smirnov test

