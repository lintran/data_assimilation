%!TEX root = ../thesis.tex

Not all covariance functions are valid for spherical domains\index{spherical domains}, like the domain of the Lorenz 2005 system. Fortunately, \citet{Gneiting2013} reviews necessary and sufficient conditions for positive definite functions on spherical domains. Before discussing how to make our newly constructed covariance function valid for spherical domains, we describe notation necessary for spherical domains. Let $\mathbb{S}^d = \{s \in \mathbb{R}^{d+1}: \norm{s} = 1\}$ denote the unit sphere in $\mathbb{R}^{d+1}$ for integer $d \geq 1$. Define the distance function $\theta(s,s') = \arccos(\left< s,s' \right>)$ to be the great circle, spherical, or geodesic distance on $\mathbb{S}^d$ where $\left< s,s' \right>$ denotes the inner product in $\mathbb{R}^{d+1}$. The covariance function now depends on $h = \theta(s,s')$, which have values in $[0,\pi]$. For example, the domain of the Lorenz 2005 system is $\mathbb{S}^1$ and one distance metric is the shortest arc length between two spatial locations. Now, we discuss how to make the Wendland exponentially damped cosine function of \Cref{eqn3:wendlandcos} valid for spherical domains: by Theorem 3 in \citet{Gneiting2013}, compactly supported positive definite functions are easily made valid by restricting the range parameter $c$ to be in $(0,\pi]$. Similar to how the Wendland exponentially damped cosine function on $\mathcal{D} \in \mathbb{R}^d$ is valid for only $d=1,2,3$, the Wendland exponentially damped cosine function on $\mathcal{D} \in \mathbb{S}^d$ is also valid for only $d=1,2,3$.

\begin{figure}
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[page=1]{model-error/figures/lorenz05/true-spatial-cor-bydist-fitted}
    \captionnew{$\var(Z(t))$.}{Fitted line has parameter value $(\nu, c, \tau, p) = (15.20, \pi, 4, 0.18)$. }
    % sigma^2 = 15.0932237, c = 0.5000000, tau = 4.0000001, q = 1, p = 0.1410043, n = 100
  \end{subfigure} \hfill
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[page=2]{model-error/figures/lorenz05/true-spatial-cor-bydist-fitted}
    \captionnew{$\cov(D(Z(t)), Z(t))$.}{Fitted line has parameter value $(\nu, c, \tau, p) = (-7.21, \pi, 4, 0.18)$. }
    % sigma^2 = -7.2646787, c = 0.4978083, tau = 4.0063848, q = 1, p = 0.1412532, n = 100
  \end{subfigure}
  \captionnew{Lorenz 2005: the components of $\hat{L}$ by distance fitted with the Wendland exponentially damped cosine function.}{The Wendland exponentially damped cosine function $C^{WC}_{1,20}$ fit to the mean line of \Cref{fig3:l05-covbydist}.}
  \label{fig3:l05-covbydist-fitted}
\end{figure}

Now that the new constructed covariance function is guaranteed to be valid on spherical domains, the Wendland exponentially damped cosine function is fitted to the mean covariances represented by thick gray lines in \Cref{fig3:l05-covbydist}; \Cref{fig3:l05-covbydist-fitted} plots the results. Notice how well the covariance function fits the covariance estimated from data. Though the fit is not perfect, the function has only four parameters, which is sufficiently estimated with much fewer samples than the number required to estimate the full variance or covariance matrix. In \Cref{sec3:low-rank-truth}, this spatial structure is exploited in the construction of a low-rank linear correction. We first introduce notation useful for the construction of the model and discuss computational gains that can be exploited for random processes on spherical domains.

With Gaussian processes, the largest computational expense is the inversion of the covariance matrix computed at the spatial locations $S = (s_1, ..., s_n) \in \mathcal{D}$, an $O(n^3)$ operation. When random process is assumed to be second-order isotropic stationary, the variance of the equispaced observations on a spherical domain is a \textit{circulant matrix}\index{circulant matrix}. For these types of matrices, \glspl{fft}\index{FFT}\index{fast Fourier transform} can be exploited to quickly invert the matrix in $O(n \log n)$ time. \citet[Section 2.6.1]{Rue2005} is a great resource on computational gains to be exploited with circulant matrices. To illustrate the circulant nature of the matrices, we use the Lorenz 2005 system as an example. We first generalize the notation used to describe model error, which is also useful in the next section. In addition to indexing the state by time $t$ as previously done, i.e., $X(t)$, let the state also be indexed by space: $X(t,s)$ for a spatial location $s \in \mathbb{S}^d$. Furthermore, we similarly index the model error $D$ by space and further introduce a shorthand: $D(t,s) \equiv D[X(t,s);\cdot]$.

Recall that the state of the Lorenz 2005 system is denoted $Z$ instead of $X$. Let $d_z$ denote the resolution of the system, where $d_z = 960$ in our demonstrations. Suppose the states are observed at $s_i = \frac{i-1}{d_z} \times 2\pi$ for each $i \in 1,...,d_z$. Then, the distance between two nearby locations $s_i$ and $s_j$ for $|i-j| = 1$ is $\Delta s \equiv \frac{2\pi}{d_z}$. If the true state $Z(t,\cdot)$ for a particular time $t$ is assumed to be a second-order isotropic stationary random process with covariance function $C(\cdot; \theta_z)$, then the variance of $Z(t,\cdot)$ on $S$ is a $d_z \times d_z$ circulant matrix with the following form:
\begin{align*}
  &\var[Z(t,S)] \\
    &\quad= \begin{bmatrix}
      C(0; \theta_z) & C(\Delta s; \theta_z) & C(2\Delta s; \theta_z) & \cdots & C(2\Delta s; \theta_z) & C(\Delta s; \theta_z)\\
      C(\Delta s; \theta_z) & C(0; \theta_z) & C(\Delta s; \theta_z) & \cdots & C(3\Delta s; \theta_z) & C(2\Delta s; \theta_z)\\
      \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
      C(\Delta s; \theta_z) & C(2 \Delta s; \theta_z) & C(3\Delta s; \theta_z) & \cdots & C(\Delta s; \theta_z) & C(0; \theta_z)\\
    \end{bmatrix}.
    % = [C((j-i) \mod d_x) \Delta s)]
\end{align*}
Similarly, if the covariance between the model error $D(t,\cdot)$ and the state $Z(t,\cdot)$ for a particular time $t$ is assumed to be second-order isotropic with covariance function $C(\cdot; \theta_{dz})$, then the covariance between the model error and the state on $S$ is also a $d_z \times d_z$ circulant matrix with the following form:
\begin{align*}
  &\cov[D(t,S), Z(t,S)] \\
    &\quad= \begin{bmatrix}
      C(0; \theta_{dz}) & C(\Delta s; \theta_{dz}) & C(2\Delta s; \theta_{dz}) & \cdots & C(2\Delta s; \theta_{dz}) & C(\Delta s; \theta_{dz})\\
      C(\Delta s; \theta_{dz}) & C(0; \theta_{dz}) & C(\Delta s; \theta_{dz}) & \cdots & C(3\Delta s; \theta_{dz}) & C(2\Delta s; \theta_{dz})\\
      \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
      C(\Delta s; \theta_{dz}) & C(2 \Delta s; \theta_{dz}) & C(3\Delta s; \theta_{dz}) & \cdots & C(\Delta s; \theta_{dz}) & C(0; \theta_{dz})\\
    \end{bmatrix}.
    % = [C((j-i) \mod d_x) \Delta s)]
\end{align*}
Since circulant matrices\index{circulant matrix} are defined by the first row of its matrix (called the \textit{base}\index{circulant matrix!base} of the matrix), another computational gain is that $d_z$-vectors instead of $d_z \times d_z$ matrices are computed. Furthermore, the vector is sparse if a compactly supported covariance function is used. %Since we are only interested in the model error $D(s)$ at the same spatial locations $S$, the covariance between the model error and the states $\cov[D(S), X(S)]$ is similarly a circulant matrix. Though it need not be inverted,



