%!TEX root = ../thesis.tex

One popular stationary assumption is \textit{second-order stationarity}\index{stationarity!second-order}, meaning that the covariance is based only on the relative locations $s - s'$:
\begin{align*}
  \cov[\eta(s), \eta(s')] = C(s,s'; \theta) \text{ for all } s, s' \in \mathcal{D}
\end{align*}
for some covariance function $C$ with parameter $\theta$.
% The \textit{semivariogram}\index{semivariogram} function $\gamma$ is a more general representation of stationarity defined as
% \begin{align*}
%   \gamma(s-s';\theta) = C(0;\theta) - C(s-s';\theta)
% \end{align*}
% for second-order stationary random processes\footnote{The semivariogram is used to study intrinsically stationary random process, in which $\gamma(s-s') = \frac{1}{2} \var[\eta(s) - \eta(s')]$. A second-order stationary random process is intrinsically stationary, but not the other way around.}.
Of course, to construct \textit{symmetric} matrices, it must satisfy an evenness property: $C(s,s';\theta) = C(s',s;\theta)$ for all $s,s' \in \mathcal{D}$. Furthermore, to construct symmetric \textit{positive semidefinite} matrices, the function must be a positive semidefinite function:
\begin{align}
  \sum_{i=1}^n \sum_{j=1}^n a_i a_j C(s_i, s_j) > 0
\label{eqn3:posdef-fn-valid}
\end{align}
for all finite sets $s_1, ..., s_n \in \mathcal{D}$ and constants $a_i, a_j \in \mathbb{R}$. For the function to be \textit{positive definite}, the greater than sign ``$>$'' is replaced with the greater than and equal sign ``$\geq$'' in the above expression. The function is called \textit{valid} if it satisfies the condition in \Cref{eqn3:posdef-fn-valid}. It is generally not easy to check the validity of a function $C$. Since validity is preserved under sums, products, convex mixtures, and convolutions, new covariance functions are often constructed from existing covariance functions that have already been proven to be positive (semi)definite. In fact, we construct a symmetric positive definite function in this section that is used in the linear correction for the Lorenz 2005 system. For this reason, we restrict our attention in this section to the two covariance functions used in our construction.

If the covariance function $C$ is further assumed to be a function of distance between two spatial locations $h = \norm{s-s'}$, the random process is assumed to be second-order \textit{isotropic}\index{isotropic} stationary. The two covariance functions used in the construction of the new covariance function are both isotropic covariance functions. These functions is reduced to be dependent only on the distance $h$: $C(s,s';\theta) = C(h;\theta)$. There are a few features of isotropic covariance functions worth mentioning. The \textit{sill}\index{sill} is defined as $C(0;\theta) - \lim_{h \to \infty} C(h;\theta)$ and will be denoted by $\nu$. If the sill $\nu$ is one, then the function $C$ is called the \textit{correlation function}\index{correlation function}. The \textit{range}\index{range} is the smallest $h$ for which $C(h;\theta)$ equals the sill. Not all covariance functions have a range and thus a related notion called the \textit{effective range}\index{range!effective} is defined as the the smallest $h$ for which $C(h;\theta)$ is 95\% of the sill, which is a function of the \textit{range parameter}. For those functions with a range, the range parameter is the range. The range parameter will be denoted by $c$ in the following discussion.

Isotropic covariance functions that have a range are compactly supported and is often desirable for computational reasons, since it allows for the construction of sparse covariance matrices. The Wendland function is an example of such a function for the domain $\mathcal{D} = \mathbb{R}^d$, $d = 1,2,3$. Let the function $a_{+} = \text{max}(a,0)$. %and $\lfloor \cdot \rfloor$ denotes the floor function.
The \textit{Wendland function}\index{Wendland function}\index{covariance function!Wendland}\index{positive definite function!Wendland} $C^W_q$ is defined as:
\begin{align}
  C^W_q(h;\nu, c, \tau) =
  \begin{cases}
    \nu \left( 1 - \tfrac{h}{c} \right)_{+}^\tau & \text{ if } q = 0\\
    \nu \left( 1 - \tfrac{h}{c} \right)_{+}^\tau \left[1 + \tau \frac{h}{c}\right] & \text{ if } q = 1\\
    \nu \left( 1 - \tfrac{h}{c} \right)_{+}^\tau \left[1 + \tau \frac{h}{c} + \frac{\tau^2 -1}{3} \left(\frac{h}{c}\right)^2 \right] & \text{ if } q = 2
  \end{cases}
\label{eqn3:wendland}
\end{align}
with sill parameter $\nu$, range parameter $c > 0$, differentiability parameter $q = 0,1,2$, and shape parameter $\tau \geq 2(q+1) \equiv \tau_0$. \Cref{fig3:wendland} depicts the Wendland function varying each parameter while keeping the others fixed at a certain value. Though not obvious from the figure, the differentiability parameter $q$ controls the smoothness of the spatial process\footnote{In particular, the covariance function is $2q$-times continuously differentiable and thus the random process $Y(s)$ is $q$-times mean-square differentiable. See both recommended references for more information.}. To illustrate the effect of the differentiability parameter, \Cref{fig3:wendland-sampleprior} depicts random functions sampled from a Gaussian process with the same parameter values as in \Cref{fig3:wendland-diff}. In addition to depicting differentiability, \Cref{fig3:wendland-sampleprior} illustrates the ability of a Gaussian process to model complicated phenomena beyond linearity, an idea that was alluded to earlier.

\begin{figure}
  \centering
  \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[page=4]{model-error/figures/lorenz05/wendland}
    \captionnew{Varying sill parameter $\nu$.}{The parameters $c, q$, and $\tau$ are set to $\pi/2$, 1, and 4, respectively.}
    \label{fig3:wendland-sigma}
  \end{subfigure} \hfill
  \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[page=1]{model-error/figures/lorenz05/wendland}
    \captionnew{Varying range parameter $c$.}{The parameters $\nu, q$, and $\tau$ are set to 1, 1, and 4, respectively.}
    \label{fig3:wendland-cutoff}
  \end{subfigure}
  \vspace{5mm}

  \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[page=2]{model-error/figures/lorenz05/wendland}
    \captionnew{Varying differentiability parameter $q$.}{The parameters $\nu, c$, and $\tau$ are set to 1, $\pi/2$, and 6, respectively.}
    \label{fig3:wendland-diff}
  \end{subfigure} \hfill
  \begin{subfigure}[t]{.49\textwidth}
    \centering
    \includegraphics[page=3]{model-error/figures/lorenz05/wendland}
    \captionnew{Varying shape parameter $\tau$.}{The parameters $\nu, c$, and $q$ are set to 1, $\pi/2$, and 1, respectively.}
    \label{fig3:wendland-tau}
  \end{subfigure}
  \captionnew{Illustrations of the Wendland function.}{Each plot depicts the Wendland function by varying one parameter while keeping the other parameters fixed. The Wendland function is defined in \Cref{eqn3:wendland}. }
  \label{fig3:wendland}
  \vspace{5mm}

  \includegraphics{model-error/figures/lorenz05/wendland-diff}
  \captionnew{Illustration of the effect of the differentiability parameter $q$.}{Random functions drawn from a Gaussian process with mean zero and covariance given by the Wendland function (\Cref{eqn3:wendland}): $Y(\cdot) \sim \gp(0, C^W_q(\cdot, \cdot; 1, \pi/2, 6))$.}
  \label{fig3:wendland-sampleprior}
\end{figure}

The Wendland function doesn't quite capture the form of the covariance that we're looking for: since the Wendland function is positive for all distances, it cannot properly capture the hole effect present in the Lorenz 2005 system, seen in \Cref{fig3:l05-covbydist}. The \textit{hole effect}\index{hole effect} is the presence of fluctuations (or ``wiggles'') in the covariance function that results in negative covariances. \citet{Chiles1999} advise against the consideration of a hole effect unless there is a physical explanation. In their setting of analyzing one spatial realization, their advice should be heeded  because statistically insignificant fluctuations may appear during exploratory data analysis. However, there is good reason to believe that the states of the Lorenz 2005 system have a strong hole effect for two reasons:
\begin{enumerate*}[label=(\arabic*)]
  \item there are prominent spatial waves in the state and
  \item \Cref{fig3:l05-covbydist} is constructed with many spatial realizations (302,400, in fact!) due to the temporal stationarity assumption.
\end{enumerate*}

The \textit{exponentially damped cosine function}\index{exponentially damped cosine function}\index{covariance function!exponentially damped cosine}\index{positive definite function!exponentially damped cosine} is an example of a valid positive definite function for the domain $\mathcal{D} = \mathbb{R}^d$ and integer $d \geq 1$ that allows for negative covariances. The function is defined as:
\begin{align*}
  \widetilde{C}^C(h;\nu,c,\tau) = \nu \exp \left( -\tau \frac{h}{c} \right) \cos \left( \frac{h}{c} \right)
\end{align*}
with sill parameter $\nu$, range parameter $c > 0$, and shape parameter $\tau \geq 1/\tan(\frac{\pi}{2d}) \equiv \tau_1$. The cosine component of the function allows for negative covariances. The exponential component has the effect of ``damping'' the cosine component so that the cosine function and thus $\widetilde{C}^C(h;\cdot)$ has a limit as $h \to \infty$, hence its name. The restriction on $\tau$ is important in maintaining the function's positive definiteness \citep{Zastavnyi2000}. This particular covariance function does not have a range and thus only has an effective range.

Since we are interested in compactly supported positive definite functions that allow for negative covariances, one such covariance function is easily constructed by simply multiplying the Wendland and exponentially damped cosine functions. The Wendland function contains all features needed of a covariance function except for a hole effect, so the exponentially damped cosine function is appealing only for its cosine component. In its current parametrization, the cosine component is not interpretable. Therefore, the function $\widetilde{C}^C$ is reparametrized so that it is a function of a parameter that controls the number of periods of the cosine function. Let $c = c'/(2 \pi n p)$, where $np$ controls the number of periods within the distance $c'$. Since $c$ is allowed to be any value strictly greater than zero, the ratio $c'/(np)$ must also be strictly greater than zero. %Since $c > 0$, $np > 0$
The reason for choosing two parameters to represent the period parameter will be explained after reparametrizing the exponentially damped cosine function\index{exponentially damped cosine function!reparametrization}\index{covariance function!exponentially damped cosine!reparametrization}\index{positive definite function!exponentially damped cosine!reparametrization} as follows:
\begin{align*}
  C^C_{n}(h;\nu,c',\tau,p) = \nu \exp \left( -\tau \times 2\pi n p \times \frac{h}{c'} \right) \cos \left( 2\pi n p \times \frac{h}{c'} \right)
\end{align*}
with sill parameter $\nu$, range parameter $c' > 0$, shape parameter $\tau \geq 1/\tan(\frac{\pi}{2d}) \equiv \tau_1$, maximum period parameter $n$, and period fraction parameter $p$. Though both $n$ and $p$ can both take any positive, finite value, it is advantageous to restrict their ranges to avoid aliasing and thus identifiability issues when estimating these parameters. When two signals are indistinguishable from each other, it is called \textit{aliasing}\index{aliasing} and is particularly an issue for sinusoidal functions, such as the cosine function. Specifically, input values that are separated by $2 \pi$ are evaluated to the same value: %\footnote{For more information, see lectures notes from Wickert offered \href{http://www.eas.uccs.edu/~mwickert/ece2610/lecture_notes/ece2610_chap4.pdf}{ECE 210}.}:
for all $x \in \mathbb{R}$ and integer values $i$, $\cos(x) = \cos(x + 2 \pi i)$ and, because cosine is an even function, $\cos(x) = (2\pi i - x)$. For the reparametrized exponentially damped cosine function, aliasing is avoided by choosing an integer $n \geq 0$ and restricting $p \in [0,1]$ and thus $np$ is the maximum number of periods within a distance of $c'$.

\begin{figure}[t]
  \centering
  \includegraphics[page=1]{model-error/figures/lorenz05/wendland-cos}
  \captionnew{Illustration of the Wendland exponentially damped function.}{Since \Cref{fig3:wendland} already provides intuition for many of the parameters, this figure only varies the maximum period parameter $n$. The parameters $\nu, c, q, \tau$, $p$, and $d$ are set to 1, $\pi/2$, 1, 4, 1, and 1, respectively. The function is defined in \Cref{eqn3:wendlandcos}.}
  \label{fig3:wendlandcos}
\end{figure}

With the reparametrization, a compactly supported covariance function that allows for negative covariances is constructed. The following parameters in the reparametrized exponentially damped cosine function $C^C_{n}$ already have equivalent functions in the Wendland function and are fixed to the following values: the sill parameter $\nu$ is set to one; the shape parameter $\tau$ is set be the minimum value $\tau_1$ that makes it a valid positive definite function; and since the Wendland function has a natural maximum distance, i.e., the range, the range parameter $c'$ is set to equal the range parameter of the Wendland function. With these modifications, a new covariance function is constructed by multiplying it with the Wendland function:
\begin{align}
  C^{WC}_{q,n}(h;\theta) = C^W_q(h;\nu,c,\tau) C_n^C(h;1,c,\tau_1,p),
\label{eqn3:wendlandcos}
\end{align}
with differentiability parameter $q = 0,1,2$, maximum period parameter $n \in \{1,2,...\}$, and parameter vector $\theta \equiv (\nu, c, \tau, p)$ with elements comprised of the sill parameter $\nu$, range parameter $c > 0$, shape parameter $\tau \geq 2(q+1)$, and period fraction parameter $p \in [0,1]$. We call this function the \textit{Wendland exponentially damped cosine function}\index{Wendland exponentially damped cosine function}\index{covariance function!Wendland exponentially damped cosine}\index{positive definite function!Wendland exponentially damped cosine}. The function is valid for the domain $\mathcal{D} = \mathbb{R}^d$ with $d=1,2,3$ because of the restriction imposed by the Wendland function. When $p = 0$, the $C_n^C$ component is exactly one and thus the Wendland exponentially damped cosine function is equal to the Wendland function. Since \Cref{fig3:wendland} already provides intuition for many of the parameters, \Cref{fig3:wendlandcos} depicts the function for different period parameter values.


% \hl{Fit GP to true states?}

