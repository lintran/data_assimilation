%!TEX root = ../thesis.tex

Under the same assumptions made by \citeauthor{Leith1978}, we construct a low-rank linear model for the model error $D$, which provides intuition for building a similar model for state-space models. %Recall that \citeauthor{Leith1978} assumed that the model error $D$ and the true state $X$ can be evaluated at any time $t$.
Suppose the model error $D$ at time $t$ is a random process that is a function of the continuous spatial locations $s \in \mathcal{D}$. Further suppose it is a linear function of another spatial random process $X$:
\begin{align}
  D(t,s) = L(s,\cdot) X(t,\cdot) + e(t,s),
\label{eqn3:low-rank-model-truth}
\end{align}
where $L(s,\cdot)$ is a function that maps the state $X$ to the model error $D$ and $e(t,s)$ is the model error not captured by $L(s, \cdot) X(t,\cdot)$. If the joint distribution of the model error $D$ and state $X$ is assumed to be a Gaussian process, the linear model is easily derived as the conditional distribution of $D$ given $X$. We detail the assumptions that lead to this derivation.

Suppose that the model error $D$ and the true state $X$ are both second-order stationary with covariances
\begin{align*}
  \cov[D(t,s), D(t,s')] &= \sigma_d^2 C(s, s'; \theta_d), \\
  \cov[X(t,s), X(t,s')] &= \sigma_x^2 C(s, s'; \theta_x).
\end{align*}
The function $C$ is a correlation function\footnote{Recall from the previous section that correlation functions are covariance functions with a sill parameter of one.} with parameters $\theta_d$ and $\theta_x$ for the model error and state processes, respectively. Similarly, the sill parameters are $\sigma_d^2$ and $\sigma_x^2$, respectively. There is no need to assume isotropy, so that assumption is not made here. The mean of both processes is assumed to be zero. Further assume that their relationship is also second-order stationary with covariance
\begin{align*}
  \cov[D(t,s), X(t,s')] &= \rho \sigma_x \sigma_d C(s, s'; \theta_{dx})
\end{align*}
with correlation function $C$ with parameter $\theta_{dx}$. The sill parameter is a function of the square root of the sill parameters from both processes and a correlation parameter $\rho \in [-1,1]$. Lastly, assume that the two processes are jointly distributed as a Gaussian process:
\begin{align*}
  \begin{bmatrix}
    D(t,\cdot) \\ X(t,\cdot)
  \end{bmatrix} \sim
  \gp \left(
  \begin{bmatrix}
    0 \\ 0
    %\mu_d(s) \\ \mu_x(s')
  \end{bmatrix},
  \begin{bmatrix}
    \sigma_d^2 C(\cdot, \cdot; \theta_d) & \rho \sigma_d \sigma_x C(\cdot,\cdot; \theta_{dx}) \\
    \rho \sigma_d \sigma_x C(\cdot, \cdot; \theta_{dx}) & \sigma_x^2 C(\cdot, \cdot; \theta_x)
  \end{bmatrix}
  \right).
\end{align*}
Let $\theta$ be comprised of all parameters of the model: $\theta = (\sigma_x^2, \sigma_d^2, \rho, \theta_x, \theta_d, \theta_{dx})$. We first derive the components of the model from \Cref{eqn3:low-rank-model-truth} and then the likelihood.%, which is useful for estimating the form of the linear correction for prediction purposes.

The linear correction is easily derived by examining the conditional distribution of the model error $D$. Suppose the model error process $D$ is observed at finite spatial locations $S_d \in \mathcal{D}$ and the state process $X$ is observed at finite spatial locations $S_x \in \mathcal{D}$. Let the number of components in $S_d$ and $S_x$ be denoted by $d_d$ and $d_x$, respectively. Then, the joint distribution of the observed processes is
\begin{align*}
  \begin{bmatrix}
    D(t,S_d) \\ X(t,S_x)
  \end{bmatrix} \sim
  \normal \left(
  \begin{bmatrix}
    0 \\ 0
    %\mu_d(s) \\ \mu_x(s')
  \end{bmatrix},
  \begin{bmatrix}
    \sigma_d^2 C(S_d, S_d; \theta_d) & \rho \sigma_d \sigma_x C(S_d,S_x; \theta_{dx}) \\
    \rho \sigma_d \sigma_x C(S_x, S_d; \theta_{dx}) & \sigma_x^2 C(S_x, S_x; \theta_x).
  \end{bmatrix}
  \right)
\end{align*}
The conditional distribution of the model error $D$ is also multivariate normal with moments
\begin{align}
  &\e[D(t,S_d) \,|\, X(t,S_x)] \notag \\
    &\quad= \rho \frac{\sigma_d}{\sigma_x} C(S_d, S_x; \theta_{dx}) C(S_x, S_x; \theta_x)^{-1} X(t,S_x), \notag \\
  &\var[D(t,S_d) \,|\, X(t,S_x)] \notag \\
    &\quad= \sigma_d^2 C(S_d,S_d; \theta_d) - \rho^2 \sigma_d^2 C(S_d, S_x; \theta_{dx}) C(S_x, S_x; \theta_x)^{-1} C(S_x, S_d; \theta_{dx}). \label{eqn3:D-cond-var}
\end{align}
Both moments of the conditional distribution provide the form of the components in \Cref{eqn3:low-rank-model-truth}.

The first moment provides the form of the linear mapping $L$:
\begin{align}
  L(S_d, S_x;\theta_L)
    &= \rho \frac{\sigma_d}{\sigma_x} C(S_d, S_x; \theta_{dx}) C(S_x, S_x; \theta_x)^{-1}
  % b(s) &= \mu_d(s) - L(s,\cdot) \mu_x(\cdot)
\label{eqn3:low-rank-truth-L}
\end{align}
with parameter $\theta_L = (\rho, \sigma_d, \sigma_x, \theta_x, \theta_{dx})$. The linear mapping is a $d_d \times d_x$ matrix that is a function of the covariance between the model error $D(t,\cdot)$ observed at $S_d$ and state $X(t,\cdot)$ observed at $S_x$, standardized by the variance of the state. Notice that the form is very similar to the linear mapping derived by Leith (see \Cref{eqn3:leith-L}): it is a function of the variance of the state $X$ and its covariance with the model error $D$. However, because of the considerable structure imposed on the random processes, the linear mapping comes from a continuous function of the spatial locations parametrized by a few parameters and hence we call it a \textit{low-rank linear correction}. This particular form of the linear mapping will be important later when modeling the linear correction in state-space models.

The second moment provides the distribution for the error $e(t,S)$ that is not captured by the state $X$:
\begin{align*}
  e(t,S_d) \sim \normal ( 0, C(S_d,S_d;\theta_{d|x})).
\end{align*}
with variance equal to the variance given by \Cref{eqn3:D-cond-var} with parameter $\theta_{d|x} = (\rho, \sigma_d^2, \theta_d, \theta_x, \theta_{dx})$:
\begin{align*}
  &\overbrace{C(S_d,S_d;\theta_{d|x})}^{\text{variance unexplained}} \\
    &\quad= \underbrace{\sigma_d^2 C(S_d,S_d; \theta_d)}_{\text{total variance}}
    - \underbrace{\rho^2 \sigma_d^2 C(S_d, S_x; \theta_{dx}) C(S_x, S_x; \theta_x)^{-1} C(S_x, S_d; \theta_{dx})}_{\text{variance explained}}.
\end{align*}
This is exactly the variance of the model error that is not captured by the linear correction. The first term is the total variance of the model error $D$ evaluated at $S_d$ and the second term is the variance of the model error $D$ evaluated at $S_d$ explained by the state $X$ evaluated at $S_x$. The difference between the two terms is the remaining variance in the model error unexplained by the state $X$.

The model we have presented is often used to model multivariate spatial processes in the spatial statistics literature \citep{Gelfand2010a}. The underlying assumption behind the relationship between the two spatial processes that we have presented here is called \textit{separable}\index{separable} and is the most simplifying assumption for these types of models. The model is called separable because it separates the local covariance from the correlation based on distance. Specifically, for all $s,s' \in \mathcal{D}$, the local covariance is
\begin{align*}
  \begin{bmatrix}
    D(t,s) \\ X(t,s)
  \end{bmatrix} \sim
  \normal \left(
  \begin{bmatrix}
    0 \\ 0
    %\mu_d(s) \\ \mu_x(s')
  \end{bmatrix},
  \begin{bmatrix}
    \sigma_d^2 & \rho \sigma_d \sigma_x \\
    \rho \sigma_d \sigma_x & \sigma_x^2
  \end{bmatrix}
  \right)
\end{align*}
and the correlation based on distance is
\begin{align*}
  \begin{bmatrix}
    D(t,s) \\ X(t,s')
  \end{bmatrix} \sim
  \normal \left(
  \begin{bmatrix}
    0 \\ 0
    %\mu_d(s) \\ \mu_x(s')
  \end{bmatrix},
  \begin{bmatrix}
    \sigma_d^2 & \rho \sigma_d \sigma_x C(s,s';\theta_{dx}) \\
    \rho \sigma_d \sigma_x C(s',s;\theta_{dx}) & \sigma_x^2
  \end{bmatrix}
  \right).
\end{align*}
This is a strict assumption that assumes that the relationship between two spatial processes is the same for all locations in the domain. Furthermore, because of the spatial structure imposed on the variances and covariance of the random processes, this model is a least-squares linear regression with regularization similar to the \gls{svd} proposed by \citet{Danforth2007} (see \Cref{eqn3:danforth-L}). Unlike \gls{svd}, however, this particular regularization respects the spatial structure of the random processes being studied.

To complete the model, the likelihood is derived for \Cref{eqn3:low-rank-model-truth}. Let the prediction of $D(t, S_d)$ be denoted as $\hat{D}(t,S_d) = L(S_d,S_x;\theta) X(t,S_x)$. Then, the log likelihood at time $t$ is
\begin{align*}
  \log p&[D(t,S_d),X(t,S_x) \,|\, \theta]
    = \log p[D(t,S_d) \,|\, X(t,S_x), \theta] + \log p[X(t,S_x) \,|\, \theta],
    %&= \phi \left[ D(t,S); L(S,S';\theta) X(t,S'), C(S,S;\theta_{d|x}) \right] \phi \left[ X(t,S'); C(S', S'; \theta_x) \right].
\end{align*}
with
\begin{align*}
  &\log p[D(t,S_d) \,|\, X(t,S_x), \theta] \\
    &\quad= -\frac{d_d}{2} \log (2\pi)  -\frac{1}{2} \log |C(S_d,S_d;\theta_{d|x})| \\
      &\qquad\quad- \frac{1}{2} \left[D(t,S_d) - \hat{D}(t,S_d) \right]^T C(S_d,S_d;\theta_{d|x})^{-1} \left[D(t,S_d) - \hat{D}(t,S_d) \right]
\end{align*}
and
\begin{align*}
  &\log p[X(t,S_x) \,|\, \theta]\\
    &\quad= -\frac{d_x}{2} \log (2\pi) -\frac{1}{2} \log |C(S_x,S_x;\theta_x)|
      - \frac{1}{2} X(t,S_x)^T C(S_x,S_x;\theta_x)^{-1} X(t,S_x).
\end{align*}
Since the model error $D$, the state $X$, and their relationship is assumed to be temporally stationary, the log likelihood for all observations evaluated at times $t = 1,..., T$ is
\begin{align}
  \log p(\text{data} \,|\, \theta)
    &= \sum_{t=1}^T \log p[D(t,S_d) \,|\, X(t,S_x), \theta]
    + \sum_{t=1}^T \log p[X(t,S_x) \,|\, \theta].
\label{eqn3:low-rank-likelihood}
\end{align}
With the log likelihood, any applicable method can be used to estimate the parameters of the model. % and thus the form of the linear mapping to predict model error.
We end this section with a demonstration of the feasibility of a low-rank linear correction in correcting model error for the Lorenz 2005 system from \Cref{ex3:l05-model-error}.



\subsection{Demonstration: Lorenz 2005} \label{sec3:l05-demo-truth}
By making the same assumptions as \citeauthor{Leith1978}'s thought experiment, we continue \Cref{ex3:l05-model-error} and examine the improved model:
\begin{align*}
  \widetilde{W}(z(t); \theta_{\widetilde{W}})
    = W(z(t); \theta_W) + L z(t).
\end{align*}
where the linear correction $L$ is replaced with the following optimal linear corrections:
\begin{itemize}[leftmargin=*]
  \item Leith's linear correction $\hat{L}$ from \Cref{eqn3:leith-L};
  \item \citeauthor{Danforth2007}'s linear correction $\tilde{L}$ from \Cref{eqn3:danforth-L} with no tapering; and
  \item the low-rank linear correction $L(\cdot, \cdot; \hat{\theta}_L)$, where $\hat{\theta}_L$ is the subset of the vector $\theta$ that maximizes the likelihood in \Cref{eqn3:low-rank-likelihood}.
\end{itemize}
There are two stages in the demonstration:
\begin{enumerate*}[label=(\arabic*)]
  \item under the assumptions made by \citeauthor{Leith1978}, generate data to estimate the above optimal linear corrections and
  \item plug in the estimated optimal linear correction into $\widetilde{W}$ and, with measurements generated from the true model $R$, filter the state-space model with $\widetilde{W}$ as its state transition.
\end{enumerate*}
The process to generate the data in the first-stage is described at the beginning of \Cref{sec3:motivate-spatial-covfns}. The process generated 302,400 realizations from the Lorenz 2005 system, which is enough for a full-rank sample estimate of $\var[Z(t)]$ needed to calculate $\hat{L}$. We emphasize that the linear correction is \textit{not} estimated \textit{while} filtering the state-space model, as done in \Cref{part:param-est}. For that to happen, modifications need to be made to the model in \Cref{eqn3:low-rank-model-truth}, which is deferred to the next section.

In addition to the assumptions behind the model in \Cref{eqn3:low-rank-model-truth}, the model error $D$, the true state $Z$, and their correlation are further assumed to be isotropic. From the strong hole effect seen in \Cref{fig3:l05-covbydist}, the correlation of the true state $Z$ and its correlation with model error $D$ is chosen to be the Wendland exponentially damped cosine function. The model error $D$ is not observed to have a hole effect and is simply chosen to be the Wendland function. Therefore,
\begin{align*}
  \cov[D(t,s), D(t,s')] &= \sigma_d^2 C_{q_d, n}^W(s, s'; \theta_d), \\
  \cov[Z(t,s), Z(t,s')] &= \sigma_z^2 C_{q_z, n}^{WC}(s, s'; \theta_z), \\
  \cov[D(t,s), Z(t,s')] &= \rho \sigma_z \sigma_d C_{q_{dz}, n}^{WC}(s, s'; \theta_{dz}),
\end{align*}
with $\theta_d = (c_d, \tau_d)$, $\theta_z = (c_z, \tau_z, p_z)$, and $\theta_{dz} = (c_{dz}, \tau_{dz}, p_{dz})$, where $q_\cdot$, $n$, $c_\cdot$, $\tau_\cdot$, and $p_\cdot$ are the differentiability, maximum wavelength, range, shape, and period fraction parameters, respectively. The locations of the model error are coincident with the locations of the state: $S \equiv S_d = S_z = (0, \frac{1}{d_z} \times 2\pi, \frac{2}{d_z}\times 2\pi, \frac{3}{d_z}\times 2\pi, ..., \frac{d_z-1}{d_z} \times 2\pi)$.

We compare two sets of results: the best-performing set of parameter values chosen from a few parameter settings guided by exploratory data analysis and from cross-validation. From our exploratory data analysis, the state $Z$ appeared to be a smooth spatial process, similar to $q=2$ of \Cref{fig3:wendland-sampleprior}, and the model error $D$ was not very smooth, similar to $q=0$ of the same figure. Furthermore, both plots in \Cref{fig3:l05-covbydist} showed the presence of at most four ``wiggles''. Therefore, we choose $q_z = 2$, $q_d = 0$, vary $q_{dz}$ to be 0, 1, and 2, and $n$ to be 0, 10, and 20, and choose the rest of the parameters by maximizing the likelihood with Nelder-Mead constrained optimization. The following settings gave the best overall \gls{es} in the state-space model demonstration that will be discussed later:
% plugin-optL000q220:
\begin{gather*}
  q_z = q_{dz} = 2,
  q_d = 0,
  n = 0, \\
  \hat{\sigma}_d^2 = 142.58, \hat{\theta}_d = (\tfrac{31.00}{960} \times 2\pi, 46.82). \\
  \hat{\sigma}_z^2 = 16.03, \hat{\theta}_z = (\tfrac{87.00}{960} \times 2\pi, 49.98), \text{ and}\\
  \hat{\rho} = -0.30, \hat{\theta}_{dz} = (\tfrac{7.63}{960} \times 2\pi, 6.00);
\end{gather*}
these parameters are denoted as $\hat{\theta}_L^{EDA}$. We also used five-fold cross-validation to choose the differentiability parameters $q_d$, $q_z$, and $q_{dz}$ (varied as 0, 1, and 2) and the maximum period parameter $n$ (varied as multiples of 10 between 0 and 100, inclusive). The rest of the parameters are chosen by maximizing the likelihood with Nelder-Mead constrained optimization. The following set of parameter values minimized the out-of-sample \gls{rmse}:
% plugin-optL000q002:
\begin{gather*}
  q_z = q_{dz} = 0,
  q_d = 2,
  % \text{ and }
  n = 0, \\
  \hat{\sigma}_d^2 = 133.09, \hat{\theta}_d = (\tfrac{11.97}{960} \times 2\pi, 49.99). \\
  \hat{\sigma}_z^2 = 2.34, \hat{\theta}_z = (\tfrac{480.00}{960} \times 2\pi, 19.81), \text{ and} \\
  \hat{\rho} = -0.068, \hat{\theta}_{dz} = (\tfrac{478.13}{960} \times 2\pi, 50.00);
\end{gather*}
these parameters are denoted as $\hat{\theta}_L^{CV}$. The out-of-sample \gls{rmse} is the smallest for the optimal linear correction given by $\hat{L}$, followed by $\tilde{L}$, and then $L(S,S; \cdot)$ for all combinations of settings. Oddly, even though a strong hole effect is observed (see \Cref{fig3:l05-covbydist}), %the best out-of-sample error is obtained
both sets of parameter values do not include the exponentially damped cosine part, i.e., $n = 0$. Upon closer inspection, this result is explained by the results from \Cref{fig3:l05-covbydist-fitted}: both have the same fitted period parameter value of 0.18 when $n$ is chosen to be 20. Extrapolating this result to the calculation of $L(S,S; \cdot)$, the same fitted value is effectively canceling out the cosine components present in both correlation functions, since the inverse of $C_{q_z, n}^{WC}$ is multiplied by $C_{q_{dz}, n}^{WC}$.


\begin{figure}
  \centerfloat
  \begin{subfigure}[t]{.49\textwidth}
    \centerfloat
    \includegraphics[page=1,width=1.2\textwidth]{model-error/figures/lorenz05/true-L-zoomed}
    \captionnew{$\hat{L}$}{}
  \end{subfigure} \hfill
  \begin{subfigure}[t]{.49\textwidth}
    \centerfloat
    \includegraphics[page=2,width=1.2\textwidth]{model-error/figures/lorenz05/true-L-zoomed}
    \captionnew{$\tilde{L}$}{}
  \end{subfigure}

  \begin{subfigure}[t]{.49\textwidth}
    \centerfloat
    \includegraphics[page=4,width=1.2\textwidth]{model-error/figures/lorenz05/true-L-zoomed}
    \captionnew{$L(S,S;\hat{\theta}_L^{EDA})$}{}
  \end{subfigure} \hfill
  \begin{subfigure}[t]{.49\textwidth}
    \centerfloat
    \includegraphics[page=3,width=1.2\textwidth]{model-error/figures/lorenz05/true-L-zoomed}
    \captionnew{$L(S,S;\hat{\theta}_L^{CV})$}{}
  \end{subfigure}

  \captionnew{Lorenz 2005: first $50 \times 50$ elements of the optimal linear corrections}{}
  \label{fig3:l05-optimal-L}
\end{figure}

\Cref{fig3:l05-optimal-L} plots the optimal linear corrections estimated from the generated data: $\hat{L}$, $\tilde{L}$, $L(S,S; \hat{\theta}_L^{EDA})$, and $L(S,S; \hat{\theta}_L^{CV})$. The linear corrections are $960 \times 960$ matrices and the figure only shows the first $50 \times 50$ elements. Compared to $\tilde{L}$, the low-rank linear correction $L(S,S; \cdot)$ better captures the general shape and magnitude of $\hat{L}$. Further comparing $\tilde{L}$ to $L(S,S; \cdot)$, notice that $L(S,S; \cdot)$ better captures the structure of $\hat{L}$. The difference is explained by the choice of regularization: the regularization to obtain $L(S,S; \cdot)$ is cognizant of the spatial structure of the underlying processes and therefore correctly puts higher weights on parts of the state-space that are more important in predicting the model error $D$. In particular, the correlation between the model error $D(t,s)$ and $Z(t,s')$ is large for nearby spatial locations $s,s'$ and therefore nearby states have a larger influence in predicting model error. On the other hand, the regularization to obtain $\tilde{L}$ is oblivious to the spatial structure of the correlation, diluting the weights to all parts of the space. The low-rank linear correction $L(S,S; \cdot)$, however, does not quite correctly capture the sign of the weights in $\hat{L}$, whose superdiagonal entries have large positive weights while the subdiagonal entries have large negative weights. This is not captured by $L(S,S; \cdot)$ because the correlation is assumed to be isotropic and hence depends only on distance, i.e., $\norm{s-s'}$ as opposed to $s-s'$.

Finally, we describe the state-space model used to test our improvements to the state transition model. From states generated from the true model $R$, measurements from a partially observed system in which every other state location are generated with a standard deviation of one. The forecast lead time $\Delta t_n$ is varied between $6 \times 0.001$ and $120 \times 0.001$ to observe the model improvements as a function of increasing nonlinearities of the state transition model. The deterministic square-root \gls{enkf} with localization is applied to state-space models with the following state transition models: the true model $R$, the wrong model $W$, and the improved model $\widetilde{W}$ with linear corrections $\hat{L}$, $\tilde{L}$, $L(S,S; \hat{\theta}_L^{EDA})$, and $L(S,S; \hat{\theta}_L^{CV})$. The localization halfwidth is $\frac{5}{960} \times \pi$ using the Gaspari-Cohn function to construct the tapering matrix and the ensemble size is chosen to be $M = 200$. The filter included an inflation parameter estimated by artificial evolution of parameters with default \gls{dart} settings as described in \Cref{sec:l05-enkf-apf}.

\begin{figure}
  \centering
  \includegraphics[page=1]{model-error/figures/lorenz05/rmse}
  \captionnew{Lorenz 2005: model improvements from optimal linear corrections.}{}
  \label{fig3:l05-compare-truth}
\end{figure}

\Cref{fig3:l05-compare-truth} reports the median \gls{es} during the time period $13,140 \times 0.001 < \sum_n \Delta t_n \leq 17,520 \times 0.01$. All model improvements $\widetilde{W}$ perform better than without ($W$) for all forecast lead times $\Delta t_n$. For almost all forecast lead times, \citeauthor{Leith1978}'s linear correction $\hat{L}$ performed the best, \citeauthor{Danforth2007}'s linear correction $\tilde{L}$ performed the worst, and the low-rank linear correction $L(S,S;\cdot)$ performed somewhere in between. This is surprising since the low-rank linear corrections $L(S,S; \cdot)$ had larger out-of-sample \gls{rmse} than $\tilde{L}$. Even more surprisingly, the linear correction with $L(S,S; \hat{\theta}_L^{LT})$ performed better the one chosen via cross-validation ($\hat{\theta}_L^{CV}$) for all forecast lead times and better than $\hat{L}$ for forecast lead times $6 \times 0.001$, $12 \times 0.001$, and $18 \times 0.001$.

There are many possible reasons for the conflicting result. %Perhaps the assumption of temporal stationarity is unreasonable.
The out-of-sample \gls{rmse} is calculated under the assumption of temporal stationarity, an assumption that need not apply to state-space models, thus the best predictor under \citeauthor{Leith1978}'s assumptions does not necessarily mean that it is the best predictor in state-space models. Since the existence of a unique \acrlong{mle} is not guaranteed for stationary Gaussian processes, the parameter value found could be a local maxima instead of a global maxima \citep{Zimmerman2010}. Another possibility is the choice of the maximization procedure used: the Nelder-Mead method is known to converge to a nonstationary point \citep{McKinnon1998}. Regardless of the reason, the forecasts from the low-rank linear correction are not sensitive to the differentiability parameter $q$ and, even, the maximum period parameter $n$. To test our hypothesis, we filtered with other $\hat{\theta}_L$ estimated with different $q$s and $n$s. These different parameter values also improved model error, provided that the value of $n$ is ``reasonable'', e.g., values of up to 50. Most resulted in lower \gls{es} values than without the linear correction ($W$) and many performed better than the linear correction $\tilde{L}$. Therefore, even though a practitioner will be unable to choose their parameters via cross-validation or exploratory data analysis like we did here, he can be reassured that a low-rank linear correction is promising, even when the model error is nonlinear.

% Regardless of the reason, this demonstrates that the low-rank linear correction is promising, even when the model error is nonlinear.
