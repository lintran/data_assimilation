%!TEX root = ../thesis.tex
In the last section, the low-rank linear correction under a temporal stationarity assumption is demonstrated to improve model error. Unfortunately, that low-rank linear correction cannot be applied because \citeauthor{Leith1978}'s assumptions are not applicable in real applications: the true state cannot measured without error and it is impossible to evaluate the true model $R$. The form of the linear correction from the last section, however, provides some guidance.

Instead of approximating the continuous state transition model $R[x(t); \theta_R] \approx W[x(t);\theta_W]$, suppose that a better approximation can be made with a linear correction:
\begin{align*}
  R[x(t); \theta_R] \approx W[x(t);\theta_W] + L(\theta_t) x(t),
\end{align*}
where $L(\cdot)$ is a $d_x \times d_x$ matrix that depends on the parameter $\theta_t$. Unlike in the last section, the parameter of the linear mapping is allowed to vary in time and thus temporal stationarity is not assumed. It is possible to assume temporal stationarity by replacing the time-varying parameter $\theta_t$ with a static parameter $\theta$, but that assumption is not made here for more flexibility. To parametrize a low-rank linear correction $L(\cdot)$, let's examine the form of the low-rank linear correction from the previous section (see \Cref{eqn3:low-rank-truth-L}). In a state-space model, the model error is to be corrected at the same locations of the state, so $S_d = S_x$ can be replaced with a single vector of spatial locations $S$. \Cref{eqn3:low-rank-truth-L} provides one possible form for $L(\cdot)$:
\begin{align*}
  L(\theta_t)
    &= \rho_t \frac{\sigma_{t,d}}{\sigma_{t,x}} C(S, S; \theta_{t,dx}) C(S, S; \theta_{t,x})^{-1}
\end{align*}
with parameter $\theta_t = (\rho_t, \sigma_{t,d}, \sigma_{t,x}, \theta_{t,x}, \theta_{t,dx})$ and a chosen correlation function $C$. The problem with this particular linear correction is identifiability of the three parameters $\rho_t, \sigma_{t,d}$, and $\sigma_{t,x}$. %Though filtering provides an estimate of an approximate true state $X$, it does not provide an estimate of the model error $D$ and thus both parameters $\rho_t$ and $\sigma_{t,d}$ are not estimable.
This is easily remedied by replacing the scalar term $\rho_t \frac{\sigma_{t,d}}{\sigma_{t,x}}$ with one parameter $\beta_t$, providing another form for $L(\cdot)$:
\begin{align*}
  L(\theta_t)
    &= \beta_t C(S, S; \theta_{t,dx}) C(S, S; \theta_{t,x})^{-1}
\end{align*}
with parameter $\theta_t = (\beta_t, \theta_{t,x}, \theta_{t,dx})$. Not only does this form eliminate identifiability issues, it also reduces the number of parameters to be estimated. We end this section with another demonstration of the Lorenz 2005 system from \Cref{ex3:l05-model-error}.

\subsection{Demonstration: Lorenz 2005}
For the same reasons discussed in \Cref{sec3:l05-demo-truth}, the state and its correlation to model error are assumed to be second-order isotropic stationary with Wendland exponentially damped cosine function and thus the linear correction takes the form
\begin{align*}
  L(\theta_t)
    &= \beta_t C_{q_{t,dx},n}^{WC}(S, S; \theta_{t,dx}) C_{q_{t,x},n}^{WC}(S, S; \theta_{t,x})^{-1}
\end{align*}
with differentiability parameters $q_{t,dx}$ and $q_{t,x}$; maximum period parameter $n$; and parameters $\theta_t = (\beta_t, \theta_{t,dx}, \theta_{t,x})$; $\theta_{t,dx} = (c_{t,dx}, \tau_{t,dx}, p_{t,dx})$, and $\theta_{t,x} = (c_{t,x}, \tau_{t,x}, p_{t,x})$, where $c_{t,\cdot}$ is a range parameter, $\tau_{t,\cdot}$ is a shape parameter, and $p_{t,\cdot}$ is the period fraction parameter. Fixing $n$, the maximum number of parameters to be estimated is 9, which can be further decreased by fixing parameters by fixing the differentiability parameters $q_{t,\cdot}$ or shape parameters $\tau_{t,\cdot}$ or choosing $n = 0$, which eliminates the need to estimate the period fraction parameters $p_{t,dx}$ and $p_{t,x}$. Whether or not the parameters are fixed, the number of parameters to be estimated for this low-rank linear correction is a lot fewer than $O(d_z^2)$ required by Leith's linear correction. The same state-space model settings are used as outlined in \Cref{sec3:l05-demo-truth}.

The robustness of the linear correction is tested using different parameter settings. The parameters of the linear correction are estimated with and without the assumption of temporal stationarity; the latter assumption assumes that the parameters are static. Under both sets of assumptions, the maximum period parameter $n$ is fixed and varied among values of 0, 10, and 20, and 100. The differentiability parameters $q_{t,\cdot}$ are both estimated and fixed, guided by the results from \Cref{sec3:l05-demo-truth}. In the last section, the shape parameter $\tau_{t,\cdot}$ was generally difficult to estimate and thus we test both estimating it and fixing it be the minimum value that maintains the validity of the Wendland exponentially damped cosine function. The rest of the parameters are estimated in addition to the inflation parameter $\lambda_t$ with artificial evolution of parameters. For the parameters that are estimated, the perturbation density is chosen to be a product of truncated normal distributions with initial standard deviations and constraints as listed in \Cref{tab3:l05-if-settings}. When assuming that the parameters are static, the parameters are estimated with 25 runs of the \acrlong{if} \citep{Ionides2015} with the same initial perturbation density settings outlined in \Cref{tab3:l05-if-settings}.

\begin{table}[h!]%\begin{wraptable}{r}{.5\textwidth}
\centering
\renewcommand{\arraystretch}{1.75}
\begin{tabular}{ccccc}
  \hline
  parameter & initial sd & minimum & maximum \\ \hline
  $\beta_t$ & 0.1 & -50 & 50 \\
  $c_{t,dz}, c_{t,z}$ & $\frac{1}{d_z}\pi$ & 0 & $\pi$ \\
  $\tau_{t,dz}, \tau_{t,z}$ & 0.1 & $2(q_{t,\cdot}+1)$ & 10 \\
  $q_{t,dz}, q_{t,z}$ & 0.1 & 0 & 2 \\
  $p_{t,dz}, p_{t,z}$ & 0.01 & 0 & 1 \\
  $\lambda_t$ & 0.6 & 1 & 20 \\ \hline
\end{tabular}
\captionnew{Lorenz 2005 parameter estimation settings.}{The perturbation density $q(\theta_{t_n} \,|\, \theta_{t_{n-1}})$ is a product of truncated normal distributions with the above parameters.}
\label{tab3:l05-if-settings}
\end{table}

% \begin{figure}
%   \centering
%   \includegraphics[page=2]{model-error/figures/lorenz05/rmse}
%   \captionnew{Lorenz 2005: model improvements from estimated linear correction.}{}
%   \label{fig3:l05-compare-est}
% \end{figure}

% \Cref{fig3:l05-compare-est} reports the median \gls{es} during the time period $13,140 \times 0.001 < \sum_n \Delta t_n \leq 17,520 \times 0.01$ for the best two parameter settings: $\hat{\theta}_t^{(1)}$ has fixed parameter values $n=0$, $q_{dz} = q_z = 0$, and fixed $\tau_{dz}$ and $\tau_z$ and $\hat{\theta}_t^{(2)}$ has fixed $n=10$. The figure also includes Leith's optimal linear correction $\hat{L}$ from \Cref{fig3:l05-compare-truth} for comparison. The improved model $\widetilde{W}$ performs better than without ($W$) for all forecast lead times $\Delta t_n$. Furthermore, they are also better than the improved model with $\hat{L}$ for forecast lead times $6 \times 0.001$ to $36 \times 0.001$ and for $48 \times 0.001$ with $\hat{\theta}_t^{(2)}$. A possible explanation for the better performance is the difference in assumptions behind the two linear models: the model with $\hat{L}$ assumes stationarity whereas the model with $L(\cdot)$ does not. Though these results look promising for these particular parameter settings, we did obtain poor results when the differentiability parameters $q_{dz}$ and $q_z$ are fixed to be 2; in fact, their \gls{es} values are higher than the unimproved model $W$ for all forecast lead times. Perhaps the translation over to state-space models does not apply or our exploratory data analysis from the last section was misguided, since the cross-validation procedure did find that the best out-of-sample error is given by differentiability parameters fixed at zero. Therefore, it seems that the low-rank linear correction in state-space models is promising but the practitioner should be careful about how the parameter values are fixed. In fact, it is safer to estimate the parameters when possible.

\begin{figure}
  \centering
  \includegraphics[page=2]{model-error/figures/lorenz05/rmse-both}
  \captionnew{Lorenz 2005: model improvements from estimated linear correction.}{}
  \label{fig3:l05-compare-est}
\end{figure}

\Cref{fig3:l05-compare-est} reports the median \gls{es} during the time period $13,140 \times 0.001 < \sum_n \Delta t_n \leq 17,520 \times 0.01$ for the best parameter setting under both sets of assumptions. Allowing the parameters to vary with time, the best performing set of parameters (denoted as $\hat{\theta}_t$) has values $n=0$, $q_{dz} = q_z = 0$, and fixed $\tau_{dz}$ and $\tau_z$. When assuming temporal stationarity, the best performing set of parameters (denoted as $\hat{\theta}$) has values $n=0$ and $q_{dz} = q_z = 0$. Notice that both sets have similar fixed values. The figure also includes Leith's optimal linear correction $\hat{L}$ from \Cref{fig3:l05-compare-truth} for comparison.

The improved models $\widetilde{W}$ perform better than without ($W$) for all forecast lead times $\Delta t_n$. In fact, the model under both sets of assumptions ($\hat{\theta}_t$ and $\hat{\theta}$) perform similarly to each other and to Leith's optimal linear correction $\hat{L}$. Considering that both sets have similar fixed values, we suspect the similar performance is due to the insensitivity of the forecasts to relatively small changes in the values of the parameters that govern the small-scale dynamics of the system. This suspicion is corroborated by our preliminary studies on the EnKF-APF: when estimating the parameters of the Lorenz 2005-III system, the parameters that govern the large-scale dynamics of the system (i.e., $K$ and $F$) showed clear convergence while the ones that govern the smaller-scale dynamics (i.e., $I$, $b$, and $c$) did not. Similarly, the low-rank linear correction is supposed to capture the missing small-scale dynamics of the system. Both of these results indicate that better forecasts are obtained when the small-scale dynamics of the system are captured but are not sensitive to the value of the parameters. Therefore, to reduce the computational expense of estimating parameters, a practitioner could assume temporal stationarity, estimate the parameters under this assumption with existing data, and subsequently plug in the estimated parameters to future forecasts.

Though these results look promising for these particular parameter settings, we did obtain poor results when fixing the parameters based on the exploratory data analysis from the last section (\Cref{sec3:low-rank-truth}). When fixing the differentiability parameters $q_{dz}$ and $q_z$ to be 2, the \gls{es} values are consistently higher than the unimproved model $W$ for all forecast lead times. Since the cross-validation procedure did find that the best out-of-sample error is when the differentiability parameters are fixed at zero, perhaps our exploratory data analysis from the last section was misguided. Though this is the case, the low-rank linear correction is promising but the practitioner should be careful about how the parameter values are fixed. In fact, it is safer to estimate the parameters.

\section{Future work}
In this section, we demonstrated with the high-dimensional Lorenz 2005 system that a low-rank linear correction, constructed with spatial covariance functions, improved forecasts. Even though the correction is called ``linear'', the correction has the ability to capture more complicated phenomenon such as the quadratic nature of the model error of the Lorenz 2005 system. However, even though the Lorenz 2005 system has characteristics of real atmospheric models, it is not clear that the principles learned from the Lorenz 2005 system carry over to real high-dimensional applications. For example, the exploratory data analysis performed in this chapter is with the true model $R$, which is not possible with real atmospheric models. We believe that the set of approximations introduced by \citet{DelSole1999} (discussed in \Cref{sec3:leith-delsole-limitations}) is worth further exploring as a tool to determine if correcting model error is even a worthwhile endeavor and, if it is, to investigate the structure of the model error. The tool would also help an investigator parametrize the low-rank linear correction.

Nevertheless, we believe the form of the linear correction is flexible to be applied to real atmospheric models, especially if it can be assumed that the model error is small relative to the dynamics of the system. This is not an unreasonable assumption for many atmospheric models, since it is typical that many atmospheric models capture the large-scale dynamics of the system and miss the small-scale dynamics. Even though the Wendland exponentially damped cosine function is constructed from the exploratory data analysis of the Lorenz 2005 system, it provides the practitioner the flexibility to include a hole effect ($n > 0$) or not ($n=0$). From a prediction standpoint, there may be no need to include the hole effect even if the system shows a strong hole effect, as seen with the Lorenz 2005 system. Without further investigation, we strongly advise the practitioner to evaluate the model error improvement with \gls{es} before choosing to use the improved model with the low-rank linear correction.

\todo{something about batch estimation if not including it in the results?}

