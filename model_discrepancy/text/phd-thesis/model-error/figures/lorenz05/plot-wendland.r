

library(ggplot2)
library(scales)
source("~/data_assimilation/model_discrepancy/code/thesis/model-error/lorenz05/calc-linear-fun.r")

# setup -------------------------------------------------------------------------
dr = "~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/lorenz05/"

multiplier = pi / 480
dvec = 0:300

xbreaks = seq(0, pi, pi/8)
sprintf("%i pi/8", 0:8)
xlabs = expression(0, pi/8, pi/4, 3*pi/8, pi/2, 5*pi/8, 3*pi/4, 7*pi/8, pi)

ybreaks = seq(-1, 1, .5)
ylabs = expression(-1, textstyle(-frac(1,2)), 0, textstyle(frac(1,2)), 1)
# ylabs = c(0, "1/4", "1/2", "3/4", 1)
# ylabs = expression(0, scriptstyle(1/4), scriptstyle(1/2), scriptstyle(3/4), 1)

plot.data = function(data, vary) {
  data = do.call("rbind", data)
  data$dist = data$dist * multiplier
  data$sigma = factor(data$sigma)
  data$cutoff = factor(data$cutoff)
  data$q = factor(data$q)
  data$period = factor(data$period)
  data$tau = factor(data$tau)
  p = ggplot(data, aes_string("dist", "cov", color = vary, linetype = vary))
  p = p + geom_hline(yintercept = 0, size = .25) + geom_vline(xintercept = 0, size = .25)
  p = p + geom_line(size = .75)
  p = p + xlab("distance (h)")
  p = p + scale_x_continuous(breaks = xbreaks, labels = xlabs)
  p = p + scale_y_continuous(breaks = ybreaks, labels = ylabs)
  p = p + theme_bw() +
    theme(text = element_text(size = 10),
          axis.title.y = element_text(angle = 0),
          #axis.text.x = element_text(angle = 90, vjust = .5),
          plot.margin=unit(c(0,3,0,0),"mm"),
          # legend.position="bottom",
          legend.key = element_blank(),
          legend.text.align = 0,
          legend.position = c(1.1,1.2),
          legend.justification = c(1,1),
          legend.background = element_rect(fill = alpha("white", .5)),
          panel.border = element_blank(),
          # legend.key.width=unit(1.5,"line"),
          legend.title=element_blank())
  return(p)
}

# illustration of the wendland function -------------------------------------------
pw = list()

data = lapply(c(60, 120, 240), function(c) data.frame(sigma = 1, cutoff = c, tau = 2*(1+1), q = 1, period = 0, dist = dvec, cov = wendland(dvec, c, 4, 1)))
pw[[1]] = plot.data(data, "cutoff")

data = lapply(0:2, function(q) data.frame(sigma = 1, cutoff = 240, tau = 2*(2+1), q = q, period = 0, dist = dvec, cov = wendland(dvec, 240, 2*(2+1), q)))
pw[[2]] = plot.data(data, "q")

data = lapply(c(4,8,16), function(t) data.frame(sigma = 1, cutoff = 240, tau = t, q = 1, period = 0, dist = dvec, cov = wendland(dvec, 240, t, 1)))
pw[[3]] = plot.data(data, "tau")

data = lapply(c(.25, .5, 1), function(s) data.frame(sigma = s, cutoff = 240, tau = 4, q = 1, period = 0, dist = dvec, cov = s*wendland(dvec, 240, 4, 1)))
pw[[4]] = plot.data(data, "sigma")

filename = sprintf("%s/wendland.pdf", dr)
pdf(filename, width = .45*(8.5 - 3), height = .35*(8.5 - 3))
for(i in 1:length(pw)) {
  grouping = as.character(pw[[i]]$mapping$colour)
  if(grouping == "cutoff") {
    legend.vals = c(60, 120, 240)
    legend.labs = expression(paste("c = ", pi/8), paste("c = ", pi/4), paste("c = ", pi/2))
  } else if(grouping == "q") {
    legend.vals = as.numeric(levels(pw[[i]]$data$q))
    legend.labs = sprintf("q = %i", legend.vals)
  } else if(grouping == "period") {
    legend.vals = as.numeric(levels(pw[[i]]$data$period))
    legend.labs = sprintf("n = %i", legend.vals)
  } else if(grouping == "tau") {
    legend.vals = as.numeric(levels(pw[[i]]$data$tau))
    legend.labs = sapply(legend.vals, function(x) bquote(tau == .(x)))
    legend.labs = do.call("expression", legend.labs)
  } else if(grouping == "sigma") {
    legend.vals = as.numeric(levels(pw[[i]]$data$sigma))
    # legend.labs = expression(nu == textstyle(frac(1,4)), nu == textstyle(frac(1,2)), nu == 1)
    legend.labs = expression(nu == 1/4, nu == 1/2, nu == 1)
  }
  
  p = pw[[i]] 
  p = p + scale_color_discrete(breaks = legend.vals, labels = legend.labs)
  p = p + scale_linetype(breaks = legend.vals, labels = legend.labs)
  p = p + ylab(expression(C^W))
  print(p)
}
dev.off()

# illustration of the wendland exponentially damped cosine function ----------------
pwc = list()

data = lapply(c(1,2,4), function(p) data.frame(sigma = 1, cutoff = 480, tau = 4, q = 1, period = p, dist = dvec, cov = wendland.cos(dvec/960, 480/960, 4, 1, 1, n = p)))
pwc[[1]] = plot.data(data, "period")

filename = sprintf("%s/wendland-cos.pdf", dr)
pdf(filename, width = .45*(8.5 - 3), height = .35*(8.5 - 3))
for(i in 1:length(pwc)) {
  grouping = as.character(pwc[[i]]$mapping$colour)
  if(grouping == "cutoff") {
    legend.vals = c(60, 120, 240)
    legend.labs = expression(paste("c = ", pi/8), paste("c = ", pi/4), paste("c = ", pi/2))
  } else if(grouping == "q") {
    legend.vals = as.numeric(levels(pwc[[i]]$data$q))
    legend.labs = sprintf("q = %i", legend.vals)
  } else if(grouping == "period") {
    legend.vals = as.numeric(levels(pwc[[i]]$data$period))
    legend.labs = sprintf("n = %i", legend.vals)
  } else if(grouping == "tau") {
    legend.vals = as.numeric(levels(unique(pwc[[i]]$data$tau)))
    legend.labs = sapply(legend.vals, function(x) bquote(tau == .(x)))
    legend.labs = do.call("expression", legend.labs)
  } else if(grouping == "sigma") {
    legend.vals = as.numeric(levels(pwc[[i]]$data$sigma))
    legend.labs = expression(nu == textstyle(frac(1,4)), nu == textstyle(frac(1,2)), nu == 1)
  }
  
  p = pwc[[i]] 
  p = p + scale_color_discrete(breaks = legend.vals, labels = legend.labs)
  p = p + scale_linetype(breaks = legend.vals, labels = legend.labs)
  p = p + ylab(expression(C^{WC}))
  print(p)
}
dev.off()

# illustration of the differentiability parameter -------------------------------------
s = seq(0, 959)
dvec = c(0:480, 479:1)
qs = 0:2
c = sapply(qs, function(q) wendland(dvec, 240, 2*(2+1), q))
c.fft = mvfft(c)
apply(c.fft, 2, function(f) sum(Re(f) < 0))
C.sq = lapply(1:length(qs), function(i) chol(circulant(c[,i])))
set.seed(12345)
x = rnorm(length(s))
x = sapply(1:length(qs), function(i) crossprod(C.sq[[i]], x))
# x = sapply(1:length(qs), function(i) crossprod(C.sq[[i]], rnorm(length(s))))

data = data.frame(s = rep(s, 3), x = as.numeric(x), q = rep(qs, each = length(s)))

xbreaks = seq(0, 960, length.out = 5)
xlabs = expression(0, pi/2, pi, 3*pi/2, 2*pi)

legend.vals = qs
legend.labs = sprintf("q = %i", legend.vals)

p = ggplot(data, aes(s, x, color = factor(q))) 
p = p + geom_hline(yintercept = 0, size = .1)
p = p + geom_line(size = .4)
p = p + ylab("Y(s)") + xlab("s")
p = p + scale_x_continuous(breaks = xbreaks, labels = xlabs)
p = p + scale_color_discrete(breaks = legend.vals, labels = legend.labs)
p = p + ylim(max(abs(range(x))) * c(-1,1))
p = p + theme_bw() +
  theme(text = element_text(size = 10),
        axis.text.x = element_text(vjust = .5),
        axis.title.y = element_text(angle = 0),
        plot.margin=unit(c(0,3,0,0),"mm"),
        legend.key = element_blank(),
        panel.border = element_blank(),
        legend.title = element_blank())

filename = sprintf("%s/wendland-diff.pdf", dr)
pdf(filename, width = .75*(8.5 - 3), height = .4*(8.5 - 3))
print(p)
dev.off()


