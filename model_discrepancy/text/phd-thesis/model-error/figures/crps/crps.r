



library(verification)
library(reshape2)
library(ggplot2)

cdf.plot = function(p, col, legend = FALSE) {
  p = p + xlab("x") + ylab(expression(F[x]))
  p = p + scale_y_continuous(expand = c(.005, 0), breaks = c(0,1)) + scale_x_continuous(expand = c(0, 0), breaks = 0)
  p = p + scale_colour_manual(values = col)
  p = p + theme_bw() +
    theme(text = element_text(size = 10),
          panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          # axis.text.x = element_blank(),
          # axis.ticks.x = element_blank(),
          axis.title.x = element_text(hjust=1),
          axis.title.y = element_text(vjust=1,angle = 0),
          axis.line = element_line(colour = "black"),
          #axis.text.x = element_text(angle = 90, vjust = .5),
          plot.margin=unit(c(1,0,0,0),"mm"))
  if(legend) {
    p = p + theme(legend.position="bottom",
                  legend.key.width=unit(1.5,"line"),
                  legend.title=element_blank())
  } else {
    p = p + theme(legend.position="none")
  }
          
  return(p)
}

pdf.plot = function(p, col, legend = FALSE) {
  p = p + xlab("x") + ylab(expression(f[x]))
  p = p + scale_y_continuous(expand = c(.005, 0), breaks = c(0,100)) + scale_x_continuous(expand = c(0, 0), breaks = 0)
  p = p + scale_colour_manual(values = col)
  p = p + theme_bw() +
    theme(text = element_text(size = 10),
          panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          # axis.text.x = element_blank(),
          # axis.ticks.x = element_blank(),
          axis.title.x = element_text(hjust=1),
          axis.title.y = element_text(vjust=1,angle = 0),
          axis.line = element_line(colour = "black"),
          #axis.text.x = element_text(angle = 90, vjust = .5),
          plot.margin=unit(c(1,0,0,0),"mm"))
  if(legend) {
    p = p + theme(legend.position="bottom",
                  legend.key.width=unit(1.5,"line"),
                  legend.title=element_blank())
  } else {
    p = p + theme(legend.position="none")
  }
  return(p)
}

gg.color.hue = function(n) {
  # emulates ggplot's colors
  # http://stackoverflow.com/questions/8197559/emulate-ggplot2-default-color-palette
  hues = seq(15, 375, length=n+1)
  hcl(h=hues, l=65, c=100)[1:n]
}

x = 0
crps(x, c(0, 1))$crps
crps(x, c(1, 1))$crps
crps(x, c(0, 2.5))$crps

x = seq(-5, 5, .01)
sprint.form = "%-10s"
data = data.frame(x = x, pdf = dnorm(x, 0, 1), cdf = pnorm(x, 0, 1), type = sprintf(sprint.form, "N(0,1)"))
data = rbind(data, data.frame(x = x, pdf = dnorm(x, 1, 1), cdf = pnorm(x, 1, 1), type = sprintf(sprint.form, "N(1,1)")))
data = rbind(data, data.frame(x = x, pdf = dnorm(x, 0, 2.5), cdf = pnorm(x, 0, 2.5), type = sprintf(sprint.form, "N(0,2.5)")))
data = rbind(data, data.frame(x = x, pdf = dnorm(x, 0, 0), cdf = pnorm(x, 0, 0), type = sprintf(sprint.form, "observation")))
dist.types = unique(data$type)

data2 = vector("list", 3)
for(i in 1:3) {
  data2[[i]] = subset(data, type %in% dist.types[c(i,4)])
  cnames = colnames(data2[[i]])
  data2[[i]] = reshape(data2[[i]][,cnames != "pdf"], direction = "wide", idvar = "x", timevar = "type", v.names = "cdf")
  colnames(data2[[i]]) = c("x", "y0", "y1")
  data2[[i]]$cdf = 1
  data2[[i]]$type = dist.types[i]
}

col = c(gg.color.hue(3), "#000000")
names(col) = unique(data$type)

pdf("~/data_assimilation/model_discrepancy/text/phd-thesis/model-error/figures/crps/crps.pdf", width = (8.5-3)/2.25, height = (8.5-3)/3)

p = ggplot(subset(data, type == sprintf(sprint.form, "N(0,1)")), aes(x, pdf, color = factor(type)))
p = p + geom_vline(xintercept = 0, size = 1.25)
p = p + geom_line(size = .75)
p = pdf.plot(p, col)
print(p)

p = ggplot(subset(data, type == sprintf(sprint.form, "N(0,1)")), aes(x, cdf, color = factor(type)))
p = p + geom_ribbon(data = data2[[1]], aes(ymin = y0, ymax = y1), color = "gray", alpha = .5)
p = p + geom_line(data = subset(data, type == sprintf(sprint.form, "observation")), size = 1.25, color = "black")
p = p + geom_line(size = .75)
p = cdf.plot(p, col)
print(p)

p = ggplot(subset(data, !type %in% sprintf(sprint.form, c("observation", "N(0,1)"))), aes(x, pdf, color = factor(type))) 
p = p + geom_vline(xintercept = 0, size = 1.25)
p = p + geom_line(size = .75)
p = pdf.plot(p, col)
print(p)

p = ggplot(subset(data, !type %in% sprintf(sprint.form, c("observation", "N(0,1)"))), aes(x, cdf, color = factor(type)))
# p = p + geom_ribbon(data = data2[[2]], aes(ymin = y0, ymax = y1), alpha = .5)
# p = p + geom_ribbon(data = data2[[3]], aes(ymin = y0, ymax = y1), alpha = .5)
p = p + geom_line(data = subset(data, type == sprintf(sprint.form, "observation")), size = 1.25, color = "black")
p = p + geom_line(size = .75)
p = cdf.plot(p, col)
print(p)

p = ggplot(subset(data, !type %in% sprintf(sprint.form, c("observation"))), aes(x, cdf, color = factor(type))) 
p = p + geom_line(data = subset(data, type == sprintf(sprint.form, "observation")), size = 1.25, color = "black")
p = p + geom_line(size = .75)
p = cdf.plot(p, col, TRUE)
print(p)

dev.off()
