%!TEX root = ../thesis.tex
%Suppose that it has been determined that a linear correction makes sense for a high-dimensional system such as the Lorenz 2005 system presented in \Cref{ex3:l05-model-error}.
In this section, the Lorenz 2005 system in \Cref{ex3:l05-model-error} is used to motivate spatial covariance functions. If the optimal linear correction $\hat{L}$ is applied to this example, the covariance and variance components in $\hat{L}$ both have a dimension of $960 \times 960$, indicating that samples on the order of $O(960^2)$ are needed to estimate these components well. In reality, that sample size is impossible to obtain. If the state is measured every 6 hours, then it would take on the order of hundreds of years to obtain a sufficient amount of samples! Fortunately, atmospheric models describe phenomena over geographic regions and thus the states are spatially correlated. This correlation in space is advantageous: correlation can be made to be a function of distance and thus be described by a few parameters, which is demonstrated at the end of \Cref{sec3:spatial-covfns-spherical}.

\begin{figure}[b]
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[page=1,trim=.25in .25in .25in .25in,clip,width=\textwidth]{model-error/figures/lorenz05/true-spatial-cor}
    \captionnew{$\var(Z(t))$}{}
  \end{subfigure}
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[page=2,trim=.25in .25in .25in .25in,clip,width=\textwidth]{model-error/figures/lorenz05/true-spatial-cor}
    \captionnew{$\cov(D(Z(t)), Z(t))$}{}
  \end{subfigure}
  \captionnew{Lorenz 2005: sample estimates of the components in $\hat{L}$.}{The axes indicate the indices of the specified matrix with a total of 960 indices. The (co)variances are indicated by the colors.}
  \label{fig3:l05-covmat}
\end{figure}

In a similar manner to the Lorenz 1963 system in the previous chapter, the true states $z(t_n)$ are generated for $n = 1,...,302,400$ by integrating the true model $R$ with a forecast lead time of $\Delta t_n = 0.001$. The model error $D$ is evaluated at the states $z(t_n)$: $D[z(t_n);\cdot] = R[z(t_n);\cdot] - W[z(t_n);\cdot]$. \Cref{fig3:l05-covmat} depicts sample estimates of the components of the linear correction $\hat{L}$ from \Cref{eqn3:leith-L}: $\var[Z(t_n)]$ and $\cov\{D[Z(t_n);\cdot], Z(t_n)\}$. Both matrices are highly structured with the covariance largest in magnitude for nearby states and decreases as the spatial distance increases. To illustrate how the covariance varies by distance, the elements of the matrices are plotted as a function of distance in \Cref{fig3:l05-covbydist}. The figure makes clear that the covariance is a smooth function of distance that can certainly be expressed with few parameters---much less than the number required to properly estimate the matrices in $\hat{L}$. In fact, we will show that the mean covariance (thick gray line in the figure) is approximated by a function with only \textit{four} parameters.

\begin{figure}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[page=1]{model-error/figures/lorenz05/true-spatial-cor-bydist-flattened}
    \captionnew{$\var(Z(t))$}{}
  \end{subfigure}

  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[page=2]{model-error/figures/lorenz05/true-spatial-cor-bydist-flattened}
    \captionnew{$\cov(D(Z(t)), Z(t))$}{}
  \end{subfigure}
  \captionnew{Lorenz 2005: sample estimates of the components in $\hat{L}$ by distance.}{The covariances from \Cref{fig3:l05-covmat} plotted by distance. There are a total of 1,920 light gray lines in each plot with the mean of those lines represented by the thick gray line. Because the locations of the elements of the state are equally-spaced on a circle, the distance are functions of the arc length of two nearby points $\Delta s$.}
  \label{fig3:l05-covbydist}
\end{figure}

Of course, not all functions of distance are symmetric positive semidefinite, a requirement of covariance functions in order to construct symmetric positive semidefinite covariance matrices. Fortunately, this is a well-studied problem in spatial statistics. We provide a short review that is relevant to our discussion. Our review is principally based on \citet[Chapters 2 and 3]{Gneiting2010} and \citet[Chapters 2 and 4]{Rasmussen2006}; we refer the reader to the original sources for more comprehensive reviews. Suppose there is a spatial stochastic process $Y$ that is a function of the continuous spatial locations $s$: $Y(\cdot) \equiv \{Y(s): s \in \mathcal{D}\}$ for the spatial domain $\mathcal{D} \in \mathbb{R}^d$ with $d \geq 1$. A model for the spatial stochastic process is
\begin{align*}
  Y(s) = \mu(s) + e(s),
\end{align*}
where $\mu(s) = \e[Y(s)]$ is a deterministic mean function and $e(s)$ is the spatial variation not captured by the mean function. Two examples of the mean function are simply zero, i.e., $\mu(s) = 0$, and linear in a vector of covariates $X(s)$, i.e., $\mu(s) = \beta X(s)$ where $X(s)$ is a vector of covariates observed at $s$ and $\beta$ is the parameter. If there is measurement error from the data collection process, the spatial component $e(s)$ is often divided into two independent components:
\begin{align*}
  e(s) = \eta(s) + \epsilon(s),
\end{align*}
where $\eta(s)$ is the component describing the spatial variation and $\epsilon(s)$ is the measurement error that has no spatial structure. The measurement error is often assumed to be \gls{iid} error with variance $\tau^2$ while the remaining spatial process $\eta(s)$ is assumed to satisfy a stationarity assumption, a discussion that is deferred to the next section.

Since Gaussian processes are defined by their first two moments, the spatial function $\eta$ is often assumed to be distributed as a Gaussian process: $\eta(\cdot) \sim \gp(0, C(\cdot,\cdot; \theta))$. Any finite subset of a Gaussian process is distributed as a multivariate normal distribution. Let $S$ denote a finite subset in $\mathcal{D}$: $S = (s_1, ..., s_n)$ with $s_i \in \mathcal{D}$ for all $i = 1,...,n$. Furthermore, let $\eta(S)$ be the column vector denoting the function $\eta$ evaluated at spatial locations $S$: $\eta(S) = (\eta(s_1), ...,\eta(s_n))^T$. Then, $\eta(S)$ has a multivariate normal distribution with mean zero and covariance $C(S, S;\theta)$, where $C(S, S; \theta)$ denotes the $n \times n$ covariance matrix evaluated at all pairs of $S$:
\begin{align*}
  C(S, S; \theta) =
  \begin{bmatrix}
    C(s_1, s_1; \theta) & C(s_1, s_2; \theta) & \cdots & C(s_1, s_n; \theta) \\
    C(s_2, s_1; \theta) & C(s_2, s_2; \theta) & \cdots & C(s_2, s_n; \theta) \\
    \vdots & \vdots & \ddots & \vdots \\
    C(s_n, s_1; \theta) & C(s_n, s_2; \theta) & \cdots & C(s_n, s_n; \theta) \\
  \end{bmatrix}.
\end{align*}
Similarly, the measurement error $\epsilon(S)$ is often assumed to be distributed as a multivariate normal distribution and, since the convolution of two normally distributed variables is also normally distributed, $Y(S)$ is distributed as a multivariate normal distribution. These assumptions will be important in the construction of a low-rank linear correction in \Cref{sec3:low-rank-truth} using spatial covariance functions. First, we provide relevant background on assumptions of stationarity and covariance functions.

