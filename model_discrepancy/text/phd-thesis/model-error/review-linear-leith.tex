%!TEX root = ../thesis.tex
A linear model is the next simplest form for model error that additionally allows model error to vary with the state. Like the state-constant correction, the linear model is added either to the continuous or discrete state transition models. Although it is easier to estimate a linear model in the discrete state transition model, the linear correction is difficult to interpret. By the assumptions of model error laid out in \Cref{sec3:model-error-notation}, the continuous model $W$ is incorrect and should thus be fixed at the source of that error: in the continuous-time model. We believe this line of work is the most promising, so we begin by reviewing the original idea behind this work, first introduced by \citet{Leith1978}.%; we defer the rest of the review to \Cref{sec3:delsole} for reasons that will become clear later.

\citeauthor{Leith1978} considered approximating the model error $D$ with a linear term and derived the linear correction under a few assumptions, some of which are impractical. First, he assumed that the true continuous state transition model $R$ can be evaluated and the true state $X(t)$ can be obtained without measurement error. With these two assumptions, the model error $D$ is evaluated as follows:
\begin{align*}
  D[x(t); \theta_D] = R[x(t); \theta_R] - W[x(t); \theta_W].
\end{align*}
He further assumed temporal stationarity of the model error $D$, the states $X$, and the relationship between the two. Then, he considered approximating the model error $D$ with a linear model:
\begin{align*}
  D[x(t); \theta_D] \approx L x(t) + b + \epsilon(t)
\end{align*}
where the parameter $\theta_D$ contains the $d_x \times d_x$ matrix $L$ and $d_x$-vector $b$ and $\epsilon(t)$ is the model error that is not captured by the linear term and assumed to have mean zero. Then, the least-squares solution is:
\begin{subequations}
\begin{align}
  \hat{L} &= \cov\{D[X(t); \cdot], X(t)\} \var[X(t)]^{-1}, \label{eqn3:leith-L}\\
  \hat{b} &= \e\{D[X(t); \cdot]\} - \hat{L} \e[X(t)]. \label{eqn3:leith-b}
\end{align}
\label{eqn3:leith-soln}%
\end{subequations}
We call this the \textit{optimal linear correction}.

Unfortunately, the optimal linear correction is only a thought experiment: the least-squares solution cannot be evaluated because neither does one have the perfect model $R$ to evaluate $D$ nor can the true state $X$ be measured exactly. The first problem is intractable: the investigator does not have the true model $R$ and even if he did, he should use the perfect model $R$ as the state transition model. The second problem, however, is tractable: while the true state $X$ cannot be measured exactly, its moments can be approximated with filtering algorithms. Unfortunately, nonlinear filtering algorithms, such as the \gls{enkf} or \gls{pf}, were not developed at the time. Once nonlinear filtering algorithms had been established, \citet{DelSole1999} proposed several solutions to overcome the issues that prevented the evaluation of the optimal linear correction; we elaborate on their methods in the next section.



