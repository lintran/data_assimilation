%!TEX root = ../thesis.tex
Under the same assumptions laid out by Leith, \citet{DelSole1999} proposed solutions to the problems that prevented the evaluation of the optimal linear correction:
\begin{enumerate*}[label=(\arabic*)]
  \item employ a forward Euler approximation to estimate the model error $D$ and
  \item approximate the state-space model with the imperfect model $W$ and apply filtering algorithms to estimate the true state.
\end{enumerate*}

A forward Euler approximation enables the approximation of the true state $X$ and the approximate state $\hat{X}$ as:
\begin{gather*}
  x(t_n)
    = \frac{x_{t_{n+1}} - x_{t_n}}{\Delta t_{n+1}} + O(\Delta t_{n+1}),\\
  \hat{x}(t_n)
    % \equiv \widehat{m}_{t_n}(x_{t_{n-1}}; \theta_W)
    = \frac{\hat{x}_{t_{n+1}} - x_{t_n}}{\Delta t_{n+1}} + O(\Delta t_{n+1}),
\end{gather*}
respectively. Subtracting the two expressions provides an approximation of the model error at $t_n$:
\begin{align}
  D[x(t_n);\theta_D]
    % = R[x(t_n);\theta_R] - W[x(t_n);\theta_W]
    = x(t_n) - \hat{x}(t_n)
    = \frac{x_{t_{n+1}} - \hat{x}_{t_{n+1}}}{\Delta t_{n+1}} + O(\Delta t_{n+1}).
\label{eqn3:model-error-euler}
\end{align}
While the forward Euler approximation provides a seemingly tractable way to evaluate the model error $D$ at time $t_n$, it is a function of the true state $X$ that cannot be measured. Not only is it a function of the true state $X_{t_{n+1}}$, it contains the approximate state $\hat{X}_{t_{n+1}}$ that is also a function of the true state $\hat{X}_{t_n}$ (see \Cref{eqn3:ss-discrete}). One more approximation is needed to obtain the model error $D$, required in the evaluation of the optimal linear correction (see \Cref{eqn3:leith-soln}).

The authors additionally proposed approximating the true state $X$ with the best estimate of the imperfect state $\hat{X}$ from a filtering algorithm. Specifically, approximate $X_{t_{n+1}}$ with the forecasted state $\hat{X}_{t_{n+1}}^f = \widehat{m}_{t_{n+1}}(\hat{X}_{t_n}^a; \cdot)$ and $X_{t_{n+1}}$ with the updated state $\hat{X}_{t_{n+1}}^a$, where $\hat{X}_{t_{n+1}}^f \overset{d}{=} \hat{X}_{t_{n+1}} \,|\, y_{0:t_n}$, $\hat{X}_{t_n}^a \overset{d}{=} \hat{X}_{t_n} \,|\, y_{0:t_n}$, and $\hat{X}_{t_{n+1}}^a = \hat{X}_{t_{n+1}} \,|\, y_{0:t_{n+1}}$. Plugging these approximations into \Cref{eqn3:model-error-euler} and assuming that the numerical error from the Euler approximation is negligible, the model error is approximated as
\begin{align*}
  D[x(t_n); \theta_D]
    \approx \frac{\hat{x}_{t_{n+1}}^a - \hat{x}_{t_{n+1}}^f}{\Delta t_{n+1}}.
\end{align*}
and the least-squares solutions in \Cref{eqn3:leith-soln} is approximated as
\begin{subequations}
\begin{align}
  \hat{L} &\approx \frac{1}{\Delta t_{n+1}} \cov ( \hat{X}_{t_{n+1}}^a - \hat{X}_{t_{n+1}}^f, \hat{X}_{t_{n+1}}^a ) \var(\hat{X}_{t_{n+1}}^a)^{-1},\label{eqn3:delsolehou-L} \\
  \hat{b} &\approx \frac{1}{\Delta t_{n+1}} \e(\hat{X}_{t_{n+1}}^a - \hat{X}_{t_{n+1}}^f) - \hat{L} \e(\hat{X}_{t_{n+1}}^a). \label{eqn3:delsolehou-b}
\end{align}
\label{eqn3:delsolehou}%
\end{subequations}
Of course, the above moments cannot be evaluated analytically and are replaced by their sample estimates. The authors demonstrated that the linear correction improved forecasts for a relatively low-dimensional state-space ($d_x = 160$).

