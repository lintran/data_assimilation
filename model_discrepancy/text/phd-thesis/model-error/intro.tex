%!TEX root = ../thesis.tex



% There are many ways a model can be incorrect. In this section, we be specific about the type of model error we aim to improve. Much of the notation henceforth is similar to \Cref{sec:cont-state-transition}, so we refer the reader to that section for a detailed discussion.

Suppose we are interested in studying the state $X$. The true model of the state's rate of change at time $t$ is:
\stepcounter{equation}
\begin{equation}
  \dot{x}(t) = R[x(t); \theta_R], \\
\tag{{\theequation}a}
\label{eqn3:ss-continuous}
\end{equation}
where $\theta_R$ is the parameter of the system and the dot above $x$ represents the first time-derivative, i.e., $\dot{x}(t) = \frac{dx(t)}{dt}$. The model is named $R$ to remind the reader that it is the ``right'' model. Measurements of the state $y_{t_n}$ are collected at discrete timepoints $t_n$.  Let $\Delta t_n$ be the forecast lead time: the time between the $(n-1)$-th and $n$th measurements, i.e., $\Delta t_n = t_n - t_{n-1}$. To evaluate the state at the same temporal resolution as the measurements, the model $R$ is integrated as follows:
% \addtocounter{equation}{1}
\begin{equation}
  x_{t_n} = m_{t_n}(x_{t_{n-1}}; \theta_R, dt) \label{eqn3:R-discrete-state-model}
\end{equation}
with
\begin{equation*}
  m_{t_n}(x_{t_{n-1}}; \theta)
    = x_{t_{n-1}} + \int_{t_{n-1}}^{t_n} \dot{x}(\tau) d\tau.
    % = x_{t_{n-1}} + \int_{t_{n-1}}^{t_n} W[x(\tau);\theta] d\tau.
  % m_{t_n}(x_{t_{n-1}}; \theta_R, dt) = \mathcal{I}_R(x_{t_{n-1}}) + O(e(dt)) \tag{{\theequation}c} \label{eqn3:ss-discrete-mean}
\end{equation*}
The true model is assumed to be deterministic and thus the state disturbance $\eta_{t_n} = 0$ does not appear in \Cref{eqn3:R-discrete-state-model}. The state $x_{t_n}$ is interpolated to the spatial resolution of the measurements as
\begin{gather}
  y_{t_n} = H_{t_n} x_{t_n} + \epsilon_{t_n},\quad
  \epsilon_{t_n} \sim \normal(0, V_{t_n}),
\label{eqn3:ss-obs}
\end{gather}
with a linear mapping $H_{t_n}$ of size $d_y \times d_x$ and measurement noise $V_{t_n}$, both assumed to be known. Recall that \Cref{eqn3:ss-obs} is called the \textit{measurement model}. %The assumptions of the model are the same as the state-space model introduced in \Cref{sec:intro}.

In reality, the true model $R$ is not known, but there is expert knowledge of the evolution of the system. Specifically, the following is a model of the evolution of the approximate state $\hat{X}$:
\begin{equation}
  \dot{\hat{x}}(t) = W[x(t); \theta_W] \label{eqn3:ss-continuous}
\end{equation}
where $\theta_W$ is the parameter of the continuous state transition model $W$. This model is denoted $W$ to remind the reader that it is the ``wrong'' model. The approximate state $\hat{X}$ is integrated to the same temporal resolution as the measurements as follows:
\begin{equation}
  \hat{x}_{t_n} = \widehat{m}_{t_n}(x_{t_{n-1}}; \theta_W) \label{eqn3:ss-discrete}
\end{equation}
with
\begin{equation*}
  \widehat{m}_{t_n}(x_{t_{n-1}}; \theta)
    = x_{t_{n-1}} + \int_{t_{n-1}}^{t_n} \dot{\hat{x}}(\tau) d\tau.
\end{equation*}
Oftentimes, the integral in the above expression does not have an analytic form and is instead numerically integrated with an appropriate numerical integrator $\mathcal{I}$:
\begin{gather}
  \widehat{m}_{t_n}(x_{t_{n-1}}; \theta_W, dt) = \mathcal{I}_W(x_{t_{n-1}}) + O(e(dt)), \label{eqn3:ss-discrete-mean}
\end{gather}
where $dt$ denotes the timestep used by the numerical integrator and $e$ is a function mapping the order of the numerical error, e.g., $e(x) = x$ for forward Euler and $e(x) = x^4$ for fourth-order Runge Kutta. The numerical integrator $\mathcal{I}$ has a subscript $W$ to remind the reader that it depends on the continuous state transition model $W$. Recall that \Cref{eqn3:ss-continuous,eqn3:ss-discrete-mean} are called the \textit{continuous} and \textit{discrete state transition models}, respectively, and they both make up the \textit{state transition model} of \Cref{eqn3:ss-discrete}; see \Cref{sec:cont-state-transition} for a detailed discussion.

Since the true model $R$ is not known, the true state cannot be evaluated via \Cref{eqn3:R-discrete-state-model}. Instead, the true state $X$ is approximated with the approximate state $\hat{X}$: $x_{t_n} \approx \hat{x}_{t_n}$. This may or may not be a good approximation. We examine the situation when it is not and aim to improve the model $W$ with an improved model $\widetilde{W}$. Assume the error between the true model and the imperfect model is additive:
\begin{align*}
  R[x(t); \theta_R] = W[x(t); \theta_W] + D[x(t); \theta_D],
\end{align*}
where the function $D$ represents the unknown dynamics of the true system not captured by the model $W$, i.e., the \textit{model error}\index{model error}, and $\theta_D$ is the parameter of the continuous state transition model $D$. It is possible to integrate just the continuous state transition model $D$, i.e.,
\begin{gather*}
  d_{t_n}(x_{t_{n-1}}; \theta_D, dt) = \mathcal{I}_D(x_{t_{n-1}}) + O(e(dt)),
\end{gather*}
but the additivity in the continuous state transition model does not necessarily translate to additivity in the discrete state transition model, i.e.,
\begin{align*}
  m_{t_n}(x_{t_{n-1}}; \theta_R, dt) \neq \widehat{m}_{t_n}(x_{t_{n-1}}; \theta_W, dt) + d_{t_n}(x_{t_{n-1}}; \theta_D, dt),
\end{align*}
particularly for nonlinear $R$ and $W$. The discretization error of the integrator $O(e(dt))$ is assumed to be negligible with respect to the model error and thus we henceforth omit the input $dt$ from the state transition models. %Furthermore, when it is not important to refer to the parameters, we use the shorthand $\widehat{m}_{t_n}(\cdot)$ for $\widehat{m}_{t_n}(\cdot; \theta_W)$ and $d_{t_n}(\cdot)$ for $d_{t_n}(\cdot; \theta_D)$. We are interested in the case where $d_{t_n}(\cdot)$ is small compared to $\widehat{m}_{t_n}(\cdot)$.
To fix ideas, we examine a few examples of model error.

\begin{example}[Lorenz 2005: model error] \label{ex3:l05-model-error}
Recall that \citeauthor{Lorenz2005} specifically constructed the models in his \citeyear{Lorenz2005} paper for atmospheric scientists to test their ideas on model error, thus providing an explicit example of model error. Let $z(t)$ be the state of the system with a resolution of $d_z$, i.e., $z(t) \equiv (z_1(t), ..., z_{d_z}(t))$. The true continuous state transition model $R$ is represented by Model III (\Cref{eqn:lorenz05-3}), i.e.,
\begin{align*}
  \dot{z}(t)
    &= R[z(t); \theta_R] \\
    &= \begin{bmatrix}
        [x(t),x(t)]_{K_R,1} + b^2[y(t),y(t)]_{1,1} + c[y(t),x(t)]_{1,1} \\
        \vdots\\
        [x(t),x(t)]_{K_R,d_z} + b^2[y(t),y(t)]_{1,n} + c[y(t),x(t)]_{1,d_z} \\
       \end{bmatrix}
       - x(t) - by(t) + F_R,
\end{align*}
where $x(t)$ and $y(t)$ are functions of $z(t)$; the parameter $I$ is defined by \Cref{eqn:lorenz05-3b}; and the parameter $\theta_R$ is $(K_R, F_R, I, b, c)$. Model II (\Cref{eqn:lorenz05-2}) captures the large-scale dynamics of Model III, but misses the small-scale dynamics of the system and thus serves as an imperfect, but good, model $W$:
\begin{align*}
  W(z(t); \theta_W) =
    \begin{bmatrix}
      [z(t),z(t)]_{K_W,1}\\
      \vdots\\
      [z(t),z(t)]_{K_W,d_z} \\
    \end{bmatrix}
    - z(t) + F_W
\end{align*}
with $\theta_W = (K_W, F_W)$. The imperfect model $W$ is equivalent to $R$ only when $I = 1$ and the parameters $K_W = K_R$ and $F_W = F_R$. The model error $D$ is quadratic in the state. While there is an expression for $D$, the expression is not simple and does not provide elucidating information beyond its quadratic nature, so we do not show it.
\end{example}

While the Lorenz 2005 system provides an explicit example of model error, model error could alternatively arise from fixing a parameter to an incorrect value. We show an example with the Lorenz 1963 system.

\begin{example}[Lorenz 1963: model error] \label{ex3:l63-model-error}
Let $(x(t), y(t), z(t))$ be the state of the system. The true continuous state transition model $R$ is represented by \Cref{eqn:lorenz63}, i.e.,
\begin{align*}
  \begin{bmatrix} \dot{x}(t) \\ \dot{y}(t) \\ \dot{z}(t) \end{bmatrix}
    = R \left( \begin{bmatrix} x(t) \\ y(t) \\ z(t) \end{bmatrix}; \theta_R \right)
    = \begin{bmatrix}
        \sigma_R[y(t)-x(t)] \\
        x(t)[\rho_R-z(t)] - y(t) \\
        x(t)y(t) - \beta_R z(t)
      \end{bmatrix}.
\end{align*}
with parameter $\theta_R = (\sigma_R, \rho_R, \beta_R)$. An imperfect model can simply come from setting a parameter to an incorrect value or getting one of the terms wrong in the above equation, e.g., accidentally multiplying the interactive term $x(t)y(t)$ by a value of $\alpha$:
\begin{align*}
  W\left(\begin{bmatrix} x(t) \\ y(t) \\ z(t) \end{bmatrix}; \theta_W\right)
    = \begin{bmatrix}
        \sigma_W[y(t)-x(t)] \\
        x(t)[\rho_W-z(t)] - y(t) \\
        \alpha x(t)y(t) - \beta_W z(t)
      \end{bmatrix}.
\end{align*}
with $\theta_W = (\sigma_W, \rho_W, \beta_W, \alpha)$. In contrast to the previous example, this model has a simple expression for the model error:
\begin{align*}
  D\left(\begin{bmatrix} x(t) \\ y(t) \\ z(t) \end{bmatrix}; \theta_D\right)
    = \begin{bmatrix}
        \sigma_D[y(t)-x(t)] \\
        \rho_D x(t) \\
        (1-\alpha) x(t)y(t) - \beta_D z(t)
      \end{bmatrix}
\end{align*}
with parameter $\theta_D = (\sigma_D, \rho_D, \beta_D, \alpha)$, where $\sigma_D = \sigma_R - \sigma_W$, $\rho_D = \rho_R - \rho_W$, and $\beta_D = \beta_R - \beta_W$. There is no model error when the parameters $\sigma_D, \rho_D$ and $\beta_D$ are zero and $\alpha = 1$. Otherwise, the model error is linear in the state when any of the parameters $\sigma_D, \rho_D$, or $\beta_D$ is nonzero and quadratic when $\alpha \neq 1$.
\end{example}
In this particular example, model error is simply corrected by better estimating the parameters using an algorithm such as the one developed in \Cref{sec2:enkf-apf}. However, it is not always known a priori when the parameters are fixed to incorrect values. Large and complex codes often accompany state transition models of high-dimensional state-space models, especially those studied in the atmospheric sciences. A seemingly innocent error, such as incorrectly modifying a term, could easily be missed, leading to an imperfect model that produce seemingly realistic results but affect the precision and accuracy of forecasts due to the chaotic nature of the model.

Without knowing the true model $R$, there is often no way to discern whether model error arises from missing the model dynamics, such as \Cref{ex3:l05-model-error}, or from setting incorrect parameter values, such as \Cref{ex3:l63-model-error}. Therefore, an investigator may desire to parametrize $D$ in such a way that reflects her hypothesis of the form of the model error. We continue a line of work that examines one of the simplest parametrizations of model error: linear in the state. While simple, the linear correction comes with a complication. The high-dimensional nature of many atmospheric models mean that the parameters of the linear correction is also high-dimensional. For this reason, we formulate a low-rank linear correction by leveraging the spatial correlation often found in atmospheric models. Furthermore, we emphasize that the main goal is to improve forecasts and not to learn the form of the model error. While the linear correction could potentially help locate and diagnose parts of the model that are wrong, it is only an auxiliary benefit to our main goal.

Before reviewing the linear correction and its low-rank approximation, we briefly review even simpler ways to correct model error. %For the discussion, we distinguish between the improved, but still imperfect, model $\tilde{W}$ from $W$ by denoting its discrete model by $\widetilde{m}_{t_n}(x_{t_{n-1}}; \theta_{\widetilde{W}})$.
In the following section, the true state $X$ is approximated with an approximate state $\tilde{X}$ with state transition model
\begin{align*}
  \tilde{x}_{t_n} = \widetilde{m}_{t_n}(x_{t_{n-1}}; \theta_{\widetilde{W}})
\end{align*}
and parameter $\theta_{\widetilde{W}}$, where $\widetilde{m}_{t_n}$ is some function of the imperfect model $\widehat{m}_{t_n}$. All the improved models $\widetilde{m}_{t_n}$, except the model with the linear correction, modify the discrete state transition model and thus an improved continuous state transition model $\widetilde{W}$ is not proposed; only the model with the linear correction has an improved continuous state transition model $\widetilde{W}$.

% For much of the review in the next section, the modifications to $W$ are made in the discrete state transition model and thus a continuous state transition model $\widetilde{W}$ is not proposed. Even though this is the case, we refer to the improved discrete state transition model as $\widetilde{m}_{t_n}(x_{t_{n-1}}; \theta_{\widetilde{W}})$ and the random variable evaluated by that model as $\widetilde{X}$ to remind the reader that is an improved version of the discrete state transition model $\widehat{m}_{t_n}(x_{t_{n-1}}; \theta_{W})$ arising from the imperfect continuous state transition model $W$.








