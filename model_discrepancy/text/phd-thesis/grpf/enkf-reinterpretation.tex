%!TEX root = ../thesis.tex
\synctex=1


Like the \gls{bf}, we begin with simple random samples from the approximate analysis distribution, $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$, that form the empirical density estimator
\begin{align*}
  \enkf_{0:t-1}(x_{t-1})
    \equiv \frac{1}{M} \sum_{m=1}^M \delta_{x_{t-1}^{a(m)}}(x_{t-1}).
\end{align*}
This estimator is then used to generate samples from the forecast distribution:
\begin{align*}
  x_t^{f(m)}
    \sim \int f(x_t \,|\, x_{t-1}) \enkf_{0:t-1}(x_{t-1}) dx_{t-1}
    = \frac{1}{M} \sum_{m=1}^M \phi(x_t; m_t(x_{t-1}^{a(m)}), U_t).
\end{align*}
In the usual presentation of the \gls{bf}, this last sampling step is instead $x_t^{f(m)} \sim \phi(x_t; m_t(x_{t-1}^{a(m)}), U_t)$, which exactly matches the sampling mechanism in the original presentation of \gls{enkf} (cf. \Cref{eqn1:enkf-forecast-sample}). Up to this point, the \gls{enkf} and the \gls{bf} do not differ: both generate forecast samples $\{x_t^{f(m)}\}_{m=1}^M$ in the same way. The difference begins with the choice of estimator to approximate the forecast distribution, which consequently changes the updating algorithm.

Recall that the \gls{bf} uses the empirical density as an estimator of the forecast distribution (see \Cref{eqn1:pf-forecast}). Instead of the empirical density, the \gls{enkf} approximates the forecast distribution with a mixture of Gaussian densities:
\begin{align}
  \enkf_{0:t-1}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f).
\label{eqn1:enkf2-forecast}
\end{align}
Each Gaussian density is centered at the forecast samples $\{x_t^{f(m)}\}_{m=1}^M$ and has a variance equal to the sample variance calculated from the forecast samples, denoted by $\hat{\Sigma}_t^f$.
In the \gls{kde} literature, a mixture of Gaussians, such as the estimator above, is proposed as an alternative estimator to the empirical density estimator for probability densities. We make this connection and further elaborate in \Cref{sect:grpf-algorithm}. We introduce terminology from the \gls{kde} literature to facilitate the rest of our discussion. The Gaussian in the mixture is a particular choice of a \textit{kernel density} (or, \textit{kernel} for short). The mean and variance of the Gaussian, e.g., $x_t^{f(m)}$ and $\hat{\Sigma}_t^f$, respectively, are called the \textit{center} and \textit{bandwidth} of the kernel.

After observing $y_t$, the state is updated using the estimator of the forecast distribution in \Cref{eqn1:enkf-forecast}. Plugging the estimator of the forecast distribution ($\enkf_{0:t-1}$) into the optimal predictive likelihood of \Cref{eqn1:cond-lik} provides an estimator to the predictive likelihood:
\begin{align}
  \enkf_{0:t-1}(y_t)
    &\equiv \int g(y_t \,|\, x_t) \enkf_{0:t-1}(x_t) dx_t \notag \\
    &= \frac{1}{M} \sum_{m=1}^M \int \phi(y_t; H_t x_t, V_t) \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f) dx_t \notag \\
    &= \frac{1}{M} \sum_{m=1}^M \phi(y_t; H_t x_t^{f(m)}, V_t + H_t \hat{\Sigma}_t^f H_t^T) \label{eqn1:enkf2-condlik2}\\
    &= \frac{1}{M} \sum_{m=1}^M l_t^{(m)}, \label{eqn1:enkf2-condlik}
\end{align}
where $l_t^{(m)} = \phi(y_t; H_t x_t^{f(m)}, V_t + H_t \hat{\Sigma}_t^f H_t^T)$. The summand in \Cref{eqn1:enkf2-condlik2} should remind the reader of the predictive likelihood of the Kalman filter (see \Cref{eqn1:kf-condlik}). The estimator of the analysis distribution is then derived by plugging approximations of the forecast distribution and the predictive likelihood ($\enkf_{0:t-1}(x_t)$ and $\enkf_{0:t-1}(y_t)$, respectively) into the optimal analysis density in \Cref{eqn1:analysis}:
\begin{align*}
  \enkfttt_{0:t}(x_t)
    &\equiv \frac{\phi(y_t; H_t x_t, V_t) \enkf_{0:t-1}(x_t)}{\enkf_{0:t-1}(y_t)} \\
    &= \frac{\frac{1}{M}\sum_{m=1}^M \phi(y_t; H_t x_t, V_t) \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)}{\frac{1}{M}\sum_{n=1}^M l_t^{(n)}} \\
    &= \sum_{m=1}^M \frac{l_t^{(m)}}{\sum_{n=1}^M l_t^{(n)}} \frac{\phi(y_t; H_t x_t, V_t) \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)}{l_t^{(m)}} \\
    &= \sum_{m=1}^M \frac{l_t^{(m)}}{\sum_{n=1}^M l_t^{(n)}} \underbrace{\frac{\phi(y_t; H_t x_t, V_t) \phi(x_t; x_t^{f(m)}, \hat{\Sigma}_t^f)}{\phi(y_t; H_t x_t^{f(m)}, V_t + H_t \hat{\Sigma}_t^f H_t^T)}}
\end{align*}
The term denoted by the underbrace should remind the reader of the analysis distribution of the Kalman filter (see \Cref{eqn1:kf-analysis2}). Applying the conditioning technique of multivariate normal random variables to the term above the underbrace, an analytic form for the mixture distribution is derived:
\begin{align}
  \enkfttt_{0:t}(x_t)
    &= \sum_{m=1}^M \tilde{w}_t^{(m)} \phi(x_t; \tilde{x}_t^{a(m)}, \hat{\Sigma}_t^a)
\label{eqn2:enkf-analysis-before-approx}
\end{align}
with
\begin{subequations}
\begin{align}
  \tilde{x}_t^{a(m)}
    &= x_t^{f(m)} + \hat{\Sigma}_t^{f} H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1} (y_t - H_t x_t^{f(m)}), \label{eqn1:enkf2-analysis-sample}\\
  \hat{\Sigma}_t^a
    &= \hat{\Sigma}_t^{f} - \hat{\Sigma}_t^{f} H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1} H_t \hat{\Sigma}_t^{f}, \label{eqn1:enkf2-analysis-var}
\end{align}
\label{eqn1:enkf2-analysis-moments}%
\end{subequations}
and importance weights $\tilde{w}_t^{(m)} = l_t^{(m)} / \sum_{n=1}^M l_t^{(n)}$ for each $m = 1,...,M$. Notice that each kernel in the estimator of the forecast distribution is updated with the measurement $y_t$ and each kernel's weight is updated with the measurement density evaluated at the measurement $y_t$ consequently changing the uniform weight to a nonuniform weight $\tilde{w}_t^{(m)}$. In other words, there are two components to the update: the kernel itself and its weight. This particular approximation, however, is not used in the \gls{enkf}. Two approximations are introduced:
\begin{enumerate*}[label=(\arabic*)]
  \item the importance weights are approximated to be uniform and
  \item the bandwidth $\hat{\Sigma}_t^a$ is replaced by another bandwidth depending on the \gls{enkf} algorithm.
\end{enumerate*}

We discuss the first approximation. The importance weights are approximated to be uniform:
\begin{align}
  \tilde{w}_t^{(m)} \approx 1/M,
\label{eqn1:enkf2-weightapprox}
\end{align}
providing another estimator to the analysis distribution ($\pi_{0:t}(x_t)$):
\begin{align}
  \enkftt_{0:t}(x_t)
    &\equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; \tilde{x}_t^{a(m)}, \hat{\Sigma}_t^a). \label{eqn1:enkf2-analysis}
\end{align}
%where the approximation in \Cref{eqn1:enkf2-weightapprox} is equivalent to proposing that $\enkfttt_{0:t}(x_t) \approx \enkftt_{0:t}(x_t).$
We make a few remarks about this estimator:
\begin{itemize}[leftmargin=*]
  \item Notice that this approximation effectively removes one of the two modes to update the state: the weight.

  \item Contrast this estimator to the estimator of the bootstrap filter in \Cref{eqn1:bf-analysis1}. After a similar step in the bootstrap filter, the estimator is a mixture of point masses centered at the analysis samples $\tilde{x}_t^{a(m)}$ that do \textit{not} differ from the forecast samples $x_t^{f(m)}$; only their weights differ. With the \gls{enkf}, the samples $x_t^{f(m)}$ and $\tilde{x}_t^{a(m)}$ \textit{do} differ because of the Kalman gain term, i.e., $K_t \equiv \hat{\Sigma}_t^{f} H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1}$, but their weights do not change because of the approximation made in \Cref{eqn1:enkf2-weightapprox}. Therefore, the updating mechanism of the \gls{enkf} solely relies on the Kalman gain when it should additionally depend on the importance weight. On the other hand, the updating mechanism of the \gls{bf} only relies on the importance weights.


  \item Compare each kernel's moments $\tilde{x}_t^{a(m)}$ and $\hat{\Sigma}_t^a$ to the analysis samples derived in the initial exposition of \gls{enkf} (\Cref{sec:enkf}). Specifically, compare \Cref{eqn1:enkf-analysis-sample} with (\ref{eqn1:enkf2-analysis-sample}) and \Cref{eqn1:enkf-analysis-var} with (\ref{eqn1:enkf2-analysis-var}). Both pairs of equations are equal.

\end{itemize}

At a similar point in the \gls{bf} algorithm, the user chooses whether or not to resample. If the user chooses not to resample, the estimator with nonuniform weights, $\pft_{0:t}(x_t)$, is used to sample from the forecast distribution of the next timestep; otherwise, the estimator with uniform weights, $\pf_{0:t}(x_t)$, is used. The same principle does not apply to the \gls{enkf} estimator $\enkftt_{0:t}(x_t)$: the convolution of the nonlinear mapping of the state transition model, i.e., $m_{t+1}$, with a Gaussian density does not have an analytic expression. Specifically, when using $\enkftt_{0:t}(x_{t-1})$ to generate samples from the forecast distribution at time $t+1$, we have
\begin{align*}
  x_{t+1}^{f(m)}
    &\sim \int f(x_{t+1} \,|\, x_t) \enkftt_{0:t}(x_{t-1}) dx_t \\
    &= \frac{1}{M} \sum_{m=1}^M \int \phi(x_{t+1}; m_t(x_t), U_{t+1}) \phi(x_t; \tilde{x}_t^{a(m)}, \hat{\Sigma}_t^a) dx_t.
\end{align*}
The integral in each summand generally does not have an analytic expression. A solution is to approximate the integral with a Monte Carlo sample, %, i.e., sample $\tilde{\tilde{x}}_t^{a(n)} \sim \phi(x_t; \tilde{x}_t^{a(m)}, \hat{\Sigma}_t^a)$ for $n=1,...,N_m$ and approximate the integral as
% \begin{align*}
%   \int f(x_{t+1} \,|\, x_t) \enkft_{0:t}(x_{t-1}) dx_t
%     \approx \frac{1}{MN_m} \sum_{m=1}^M \sum_{n=1}^{N_m} \phi(x_{t+1}; m_t(\tilde{\tilde{x}}_t^{a(n)}), U_{t+1}) dx_t,
% \end{align*}
which is equivalent to resampling from $\enkftt_{0:t}(x_{t-1})$ to form an empirical density estimator of the analysis distribution.
%An empirical density estimate is instead required for forecasting, which is easily obtained by sampling from $\pft_{0:t}(x_t)$; in other words, a resampling step is required.

Both the original \gls{enkf} and \gls{enkf} with perturbed observations have a step that is equivalent to a resampling step, using stratified or systematic sampling. Since the weights on each Gaussian in the estimator $\enkftt_{0:t}(x_t)$ are uniform, \Cref{thm:sys-sampling-unif-weights} implies that stratified/systematic sampling generates one sample from each Gaussian density in the mixture. However, instead of resampling from each Gaussian density with a variance of $\hat{\Sigma}_t^a$, the following approximations to $\hat{\Sigma}_t^a$ are made:
\begin{itemize}[leftmargin=*]
  \item \textbf{Original \gls{enkf}}: The bandwidth is approximated as $\hat{\Sigma}_t^a \approx 0$, thus the estimator of the analysis distribution is approximated as
  \begin{align*}
    \enkftt_{0:t}(x_t) \approx \enkft_{0:t}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \delta_{\tilde{x}_t^{a(m)}}(x_t).
  \end{align*}
  Stratified or systematic random sampling is used to generate a simple random sample from $\enkft_{0:t}(x_t)$, obtaining the analysis samples $x_t^{a(m)} = \tilde{x}_t^{a(m)}$ for each $m = 1,...,M$.

  \item \textbf{\gls{enkf} with perturbed observations}: Recall that this particular method adds noise to the measurement $y_t$ (\Cref{eqn1:enkf-analysis-sample2}), generating analysis samples:
  \begin{align*}
    x_t^{a(m)}
      % &= x_t^{f(m)} + \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1}[y_t^{(m)} - H_tx_t^{f(m)}] \\
      &= x_t^{f(m)} + K_t(y_t - H_tx_t^{f(m)}) + K_t \epsilon_t^{(m)},
  \end{align*}
  where %$y_t^{(m)} = y_t + \epsilon_t^{(m)}$ with
  $\epsilon_t^{(m)} \sim \normal(0, V_t)$. This is equivalent to stratified or systematic resampling with uniform weights and approximating the analysis bandwidth $\hat{\Sigma}_t^a$ to be $\cov(K_t \epsilon_t) = K_t V_t K_t^T$ (see \Cref{eqn1:enkf-analysis-sample2}). Therefore, the estimator of the analysis distribution is approximated as
  \begin{align*}
    \enkftt_{0:t}(x_t) \approx \enkft_{0:t}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; \tilde{x}_t^{a(m)}, K_t V_t K_t^T).
  \end{align*}
  Again, stratified or systematic random sampling is used to generate a simple random sample from $\enkft_{0:t}(x_t)$, obtaining the analysis samples $x_t^{a(m)} \sim \normal(\tilde{x}_t^{a(m)}, K_t V_t K_t^T)$ for each $m = 1,...,M$.
\end{itemize}
%With the original \gls{enkf}, $\hat{\Sigma}_t^a \approx 0$, thus the resampled particles are set to be equal to the centers of the mixture distribution in $\pft_{t|t}$, i.e., $x_t^{a(m)} = \tilde{x}_t^{a(m)}$. With perturbed observations, let $\hat{\Sigma}_t^a \approx K_t V_t K_t^T$, then the particles are resampled as $x_t^{a(m)} \,|\, \tilde{x}_t^{a(m)} \sim \normal(\tilde{x}_t^{a(m)}, B_t^a)$.
Another estimator of the analysis distribution is constructed from analysis samples generated from either estimator:
\begin{align}
  \enkf_{0:t}(x_t)
    &= \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{a(m)}}(x_t).
\label{eqn1:enkf2-analysis2}
\end{align}
Since square-root filters deterministically scale the forecast samples $\{x_t^{f(m)}\}_{m=1}^M$, there is no analogous \gls{bf} interpretation for square-root filters\footnote{The update step with the deterministic square-root filter is more closely related to the update step in sigma-point Kalman filters, where the samples are scaled to have the proper mean and variance \citep{VanderMerwe2004}.}, but the same estimator $\enkf_{0:t}(x_t)$ with point masses centered at the resulting analysis samples $\{x_t^{a(m)}\}_{m=1}^M$ is used to sample from the approximate forecast distribution of the next timestep\footnote{We are not the first to recognize that the perturbed observations algorithm use both $\enkft_{0:t}(x_t)$ and $\enkf_{0:t}(x_t)$ as estimators of the analysis distribution \citep{Frei2012,Frei2013a}. We independently discovered these estimators through our probabilistic re-interpretation of the \gls{enkf}.}. As mentioned in \Cref{sec:enkf}, even though the \gls{enkf} algorithm was inspired by the \gls{ekf}, the sample mean and variance $\hat{\mu}_t^a$ and $\hat{\Sigma}_t^a$ are not used when filtering the next timestep. Only the samples $\{x_t^{a(m)}\}_{m=1}^M$ are used and our re-interpretation makes it clear how. %With this re-interpretation, it is now clear what estimators (and the approximations being made to derive them!) are behind the samples generated by the \gls{enkf} algorithm. %The estimator $\enkf_{0:t}(x_t)$ is used to sample from the approximate forecast distribution of time $t+1$, thus continuing the iterative procedure of sampling from the approximate filtering densities.



