%!TEX root = ../thesis.tex
\synctex=1

% \subsubsection{Forecast step: connection to kernel density estimation} \label{sec:kde}
The first difference between the \gls{bf} and the \gls{enkf} is the estimators used to approximate the forecast distribution: the \gls{bf} uses an empirical density estimator and the \gls{enkf} uses a \gls{kde} with Gaussian kernels. If the variance of a Gaussian density is taken to zero, it becomes a point mass centered at the mean. Therefore, the \gls{enkf} estimator for the forecast distribution is equivalent to the \gls{bf} estimator when the bandwidth is taken to be zero instead of the sample variance. Considering how well the \gls{enkf} performs in practice for high-dimensional state-spaces, it seems beneficial to have nonzero covariances.

To better understand this benefit, we draw intuition from the \gls{kde} literature (see \citet{Silverman1986} and \citet{Wand1995} for textbook reviews). Instead of using an empirical density as an estimator of a probability density, \gls{kde} uses a mixture of kernels centered at \gls{iid} samples $x_1, ..., x_n \sim p(x)$:
\begin{align*}
  \hat{p}_B(x) \equiv \frac{1}{n} \sum_{i=1}^n |B|^{-1/2} K\left[B^{-1/2}(x-x_i)\right],
\end{align*}
where $B$ is a smoothing parameter, often called a \textit{bandwidth}\index{bandwidth}. For $\hat{p}_B(x)$ to be an appropriate estimator of a probability density, the kernel $K$ must satisfy the following constraints:
\begin{enumerate*}[label=(\arabic*)]
  \item $K(u) \geq 0$ for all $u$,
  \item $\int K(u) du = 1$, and
  \item $K(-u) = K(u)$.
\end{enumerate*}
It is easy to show that the standard Gaussian density satisfies the constraints of a kernel.

As the bandwidths approach zero, \glspl{kder} are well-known to be a biased, but asymptotically optimal, estimator of $p(x)$ \citep{Wand1992}. Optimality in the \gls{kde} literature is often defined in terms of asymptotic properties of the integrated mean squared error, $\e[\int (\hat{p}_B(x) - p(x))^2 dx]$.
If $x_1, ..., x_n$ are \gls{iid} samples from $\normal(\mu, \Sigma)$, the asymptotic integrated mean squared error is minimized by the following bandwidth:
\begin{align*}
  B_n = c_{n,d} \Sigma \qquad \text{ with } \qquad c_{n,d} = \left( \frac{4}{n(d + 2)} \right)^{\frac{2}{d+4}},
\end{align*}
where $d$ is the dimension of $x_i$. Notice that the bandwidth is a function of the variance of the density, which is not known in reality. The sample variance $\hat{\Sigma}$ is a reasonable estimator and is in fact the suggested bandwidth, after scaling it by $c_{n,d}$, for unimodal densities \citep{Wand1995}. Furthermore, notice that the bandwidth is scaled by both the dimension of $x$ ($d$) and the sample size $n$ ($c_{n,d}$). In fact, $c_{n,d} \to 0$ as $n \to \infty$, thus $B_n \to 0$. Therefore, the bandwidths should be scaled by the sample size in such a way that the kernels shrink to point masses as the number of samples increases.

With this intuition, it seems reasonable to approximate the forecast distribution with a mixture distribution as the \gls{enkf} does. The bandwidth choice of the sample variance is also reasonable but with one caveat: the bandwidth should scale with the ensemble size, similar to the scalar $c_{n,d}$ as in the \gls{kde} literature. In fact, the idea of using \glspl{kde} with the \gls{pf} has been proposed numerous times in the literature to overcome particle collapse; these filters are known as \textit{\glspl{rpf}}. When \citet{Gordon1993} first introduced the \gls{bf}, they suggested jittering the forecast samples when filtering state-space models with a deterministic state transition model to overcome particle degeneracy. \citett{Hurzeler1998} then suggested a more principled approach to jittering: use a \gls{kder} as an estimator of the forecast distribution, leading to the introduction of \textit{pre-regularized particle filters} (pre-\gls{rpf})\footnote{On the other hand, post-regularized particle filters use a \gls{kder} to approximate the \textit{analysis} distribution.}. %Since the introduction by \citet{Hurzeler1998}, \glspl{rpf} has been the subject of a few large sample asymptotic analyses \citep{Musso2001,LeGland2004,LeGland1998}.
Therefore, using a mixture of Gaussians as an estimator to the forecast distribution, as the \gls{enkf} does, is a pre-\gls{rpf} with Gaussian kernels. %; this is the subject of exploration in the next section.


% \begin{conjecture}
%   \begin{align*}
%     \enkf_{0:t-1}(x_t) \to \pf_{0:t-1}(x_t)
%   \end{align*}
% \end{conjecture}

