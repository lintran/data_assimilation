%!TEX root = ../thesis.tex
\synctex=1

\section{Past and future exploration} \label{sec1:grpf-future-work}

In this section, we introduced a re-interpretation of the \gls{enkf}. Not only is the re-interpretation useful in the development of new parameter estimation methodology that will be introduced in \Cref{part:param-est}, it clarifies its connection with the \gls{pf} and thus some of the theoretical underpinnings of \gls{pf} can be borrowed to more rigorously study the \gls{enkf}. In particular, the pre-\gls{rpf} with Gaussian kernels is a step in that direction. Like the \gls{enkf}, the pre-\gls{rpf} also uses a mixture of Gaussian kernels as an estimator of the forecast distribution. %We hope this connection will help future studies the \gls{enkf} in a more rigorous manner.
We conclude this section with a few hypotheses that we think are critical to the practical success of the \gls{enkf} and are worth exploring more rigorously in the future.%: we separate the hypotheses into the natural delineations given by the forecast, analysis, and resampling steps of the pre-\gls{rpf} algorithm.

% \subsubsection{Forecast step: bandwidth selection}
Two special cases of the pre-\gls{rpf} with Gaussian kernels have appeared in the atmospheric science literature; both propose two different forecast bandwidths $B_t^f$. The kernel filter \citep{Anderson1999} chooses the bandwidth $B_t^f$ to be the (tapered) sample variance $\hat{\Sigma}_t^f$ calculated via the forecast samples $\{x_t^{f(m)}\}_{m=1}^M$. The nonlinear ensemble filter \citep{Bengtsson2003} uses a nearest neighbor approach to estimate a different bandwidth for each kernel in the mixture, which also uses the forecast samples $\{x_t^{f(m)}\}_{m=1}^M$ to construct the bandwidths. Neither scaled the covariance by the ensemble size. Furthermore, both methods were demonstrated to perform better than the \gls{enkf} when applied to low-dimensional state-spaces ($d_x \leq 40$), but had trouble with particle collapse when filtering higher-dimensional state-spaces (verbal discussion with authors). In our own explorations with the Lorenz 2005 system as a state transition model and $d_x = 960$, we found somewhat similar results: the pre-\gls{rpf} with a simple fixed bandwidth (\Cref{eqn1:grpf-bandwidth-recommendation} with $\Sigma = I_{d_x}$ and multiplied by a fixed scalar $c$) is just as effective as or better than the \gls{enkf} when filtering fully observed systems, but suffered from particle collapse when filtering partially observed systems.

Our explorations of the forecast bandwidth lead us to believe that the \gls{kder} of the forecast distribution is important but not critical to the success of the \gls{enkf}. Consequently, we believe that the combination of the variance inflation in the forecast step and the weight approximation in the update step is crucial to the success of the \gls{enkf}. As its name suggests, variance inflation increases the variance of the ensemble to better explore the state space. However, the exploration is stunted because of particle collapse and thus the weight approximation introduced in the update step of the \gls{enkf} prevents particle collapse from being a problem.
% \subsubsection{Update step: weight approximation and variance inflation}
After substituting the \gls{kder} of the forecast distribution into the optimal distribution, the estimator $\enkfttt_{0:t}(x_t)$ of \Cref{eqn2:enkf-analysis-before-approx}\footnote{This estimator corresponds to the pre-\gls{rpf} estimator $\nft_{0:t}(x_t)$ of \Cref{eqn1:grpf-analysis}.} is derived. Recall that this estimator has two modes to update the particles: the weight $\tilde{w}_t^{(m)}$ and the conditional update of the Gaussian kernel with the measurement\footnote{This corresponds to the pre-\gls{rpf}'s \Cref{eqn1:grpf-analysis-sample}.} $y_t$. However, since the \gls{enkf} approximates the weights $\tilde{w}_t^{(m)}$ to be uniform (see \Cref{eqn1:enkf2-weightapprox}), the algorithm only partially updates particles through one of the two update modes. In other words, the weight approximation increases the effective sample size in exchange for the introduction of biasedness by removing one of the update modes. Without the approximation, like the \gls{pf}, particle collapse is inevitable when applying the \gls{enkf} to high-dimensional state-spaces, thus rendering variance inflation ineffective. Since large sample asymptotics of the \gls{enkf} with linear state transitions have been previously studied \citep{LeGland2009,Mandel2009}, we have good reason to believe that the weight approximation may be appropriate for near linear state transitions and therefore deviations from linearity should be studied along with variance inflation and the weight approximation. Furthermore, many theoretical analyses of the \gls{enkf} do not consider variance inflation as part of the algorithm, even though it is practically used almost every time \gls{enkf} is applied, and thus we believe these analyses are missing a crucial part of the puzzle.

% \subsubsection{Resample step: bandwidth approximation}
For completeness, we discuss one last difference between the pre-\gls{rpf} and the \gls{enkf} that we don't believe to be important in the success of the \gls{enkf}. Recall that the pre-\gls{rpf} derives the analysis bandwidth $B_t^a$ (see \Cref{eqn1:grpf-analysis-var}) to be
\begin{align*}
  B_t^a
    = B_t^{f} - B_t^{f} H_t^T ( V_t + H_t B_t^f H_t^T )^{-1} H_t B_t^{f}
    = (I - K_t H_t) B_t^f,
\end{align*}
where the Kalman gain $K_t = B_t^{f} H_t^T ( V_t + H_t B_t^f H_t^T )^{-1}$ is substituted in the second equality. Since the \gls{enkf} uses the sample variance $\hat{\Sigma}_t^f$ as the bandwidth, the analysis bandwidth $B_t^a$ becomes
\begin{align*}
  B_t^a = (I - K_t H_t) \hat{\Sigma}_t^f.
\end{align*}
However, the \gls{enkf} with perturbed observations uses the following analysis bandwidth instead of $B_t^a$:
\begin{align*}
  K_t V_t K_t^T = K_t (I - K_t^T H_t^T) \hat{\Sigma}_t^f,
\end{align*}
where the Woodbury matrix identity is applied to obtain the equality. The difference between the two expressions is
\begin{align*}
  B_t^a - K_t V_t K_t^T = [\underbrace{(I - K_t H_t) - K_t (I - K_t^T H_t^T)}] \hat{\Sigma}_t^f
\end{align*}
and thus the two expressions are (asymptotically) equivalent if the expression above the underbrace is (asymptotically) zero. A secondary study may be to explore the large sample asymptotics that particular term.

% \begin{conjecture}
%   \begin{align*}
%     \enkft_{0:t}(x_t) \to \enkftt_{0:t}(x_t)
%   \end{align*}
% \end{conjecture}
