%!TEX root = main.tex
\synctex=1

\subsection{Almost sure convergence of the Gaussian perturbations to the point mass perturbations}
We start by proving almost sure convergence of the mixture of Gaussian kernels to the empirical distribution. The proof will be very similar in spirit to \cref{lem:dist-delta-true}. In the proof of \cref{lem:dist-delta-true}, the cross terms in \cref{eqn:as-condition} were exactly zero due to independence and the unbiasedness of the empirical distribution. This will not be the case when approximating the distribution using a mixture of Gaussian kernels---it will be biased for finite $n$. The biasedness is no longer an issue when the bandwidth matrix approaches zero. In other words, the Gaussian kernels will approach the point masses, making the biasedness approach zero, so the cross terms also approach zero. \cref{lem:dist-delta-true} was proven using the test functions in $C_b(\mathbb{R}^d)$. The following lemma is proven using the test functions in $L_b(\mathbb{R}^d)$, but as mentioned before, the topologies are equivalent with the same distance metric due to the portmanteau theorem.

\input{lemma-distance}

A simple application of the triangle inquality gives us the following corollary.
\begin{corollary}
\label{cor:dist-gaussian-true}
Given the assumptions in \cref{lem:dist-gaussian-delta}, $\mathbb{Q}_n(\mu_n) \to \mu$ almost surely.
\end{corollary}

\subsection{Bandwidth matrices}
\label{subsect:bandwidth}
\cref{eqn:bandwidth-assumption} provides the conditions on the rate that the bandwidth matrices should go to zero. In this section, we derive equivalent definitions of \cref{eqn:bandwidth-assumption} and the conditions that would satisfy \cref{eqn:bandwidth-assumption} when choosing a particular form for the bandwith matrices. Define $\lambda_{max}(\Sigma) = \max_i \lambda_i$ and $\lambda_{min}(\Sigma) = \min_i \lambda_i$, where $\{\lambda_i: 1 \leq i \leq d\}$ are the eigenvalues of $\Sigma$. $\tr(\Sigma)$ denotes the trace of $\Sigma$.

The assumption on the sequence of bandwidth matrices is obscured by \cref{eqn:bandwidth-assumption}, so we present an equivalent definition that makes it clear that the sequence of bandwidth matrices are important.
\begin{corollary}
\label{cor:bandwidth-assumption2}
Let $Z \sim \normal(0, I)$ and $D_n$ be a diagonal matrix with the eigenvalues of $B_n$ on its diagonal. The following statements are equivalent:
\begin{enumerate}[(1)]
  \item \cref{eqn:bandwidth-assumption}.
  \item
    \begin{equation}
      \e\left[\prob\left(Z^T B_n Z > \frac{1}{n} \,\bigg|\, V_{1:n} \right) \right]
        = O\left( \frac{1}{\sqrt{n}} \right). \label{eqn:bandwidth-assumption2a}
    \end{equation}
  \item
    \begin{equation}
      \e\left[\prob\left(Z^T D_n Z > \frac{1}{n} \,\bigg|\, V_{1:n} \right) \right]
        = O\left( \frac{1}{\sqrt{n}} \right). \label{eqn:bandwidth-assumption2b}
    \end{equation}
\end{enumerate}
\end{corollary}

\begin{proof}
Since $B_n$ is a positive symmetric definite matrix, it can be eigendecomposed as $B_n = Q_n D_n Q_n^T$. Let $Z = D_n^{-1/2} Q_n^T W$. Then, $Z \sim \normal(0, I)$ and
\begin{align*}
  W^T W = Z^T D_n^{1/2} Q_n^T Q_n D_n^{1/2} Z = Z^T D_n Z.
\end{align*}
Therefore,
\begin{align*}
  \prob \left(W^T W > \frac{1}{n} \, \bigg|\, V_{1:n} \right)
    &= \prob \left(Z^T B_n Z > \frac{1}{n} \,\bigg|\, V_{1:n}\right)
\end{align*}
Taking expectations of both sides, we have \cref{eqn:bandwidth-assumption2b}. \cref{eqn:bandwidth-assumption2a} can be proven similarly if we let $Z = Q_n D_n^{-1/2} W$.
\end{proof}
For a fixed sequence of bandwidth matrices $B_n$, the corollary gives conditions that the probability outside of a $d$-sphere with radii given by the inverse eigenvalues of $B_n$ goes to zero as $n$ increases, or in other words, how fast the maximum eigenvalue should go to zero for the probability outside of the $d$-sphere to go to zero. Though the corollary makes \cref{eqn:bandwidth-assumption} less abstract, the following corollary explicitly provides a rate based on the trace of the matrix.

\begin{corollary}
\label{cor:bandwidth-assumption3}
If $p \geq 3/2$ and
\begin{equation}
  \tr[\e(B_n)] = O\left(\frac{1}{n^p}\right), \label{eqn:bandwidth-assumption3}
\end{equation}
then \cref{eqn:bandwidth-assumption} holds.
\end{corollary}

\begin{proof}
By Markov's inequality,
\begin{align*}
  \prob \left(Z^T B_n Z > \frac{1}{n} \,\bigg|\, V_{1:n}\right)
    &\leq n \e(Z^T B_n Z)
    = n \tr(B_n)
\end{align*}
Taking expectations of both sides, we have
\begin{align*}
  \e\left[\prob \left(Z^T B_n Z > \frac{1}{n} \,\bigg|\, V_{1:n}\right) \right]
    &\leq n \e[\tr(B_n)]
    = n \tr[\e(B_n)].
\end{align*}
By the assumptions, we have our result.
\end{proof}

This corollary says that $B_n = \frac{1}{n^{3/2}} \Sigma$ is a good choice for the bandwidth, where $\Sigma$ can be as simple a fixed diagonal matrix or as complicated as the covariance matrix of the underlying distribution. Oftentimes, the covariance matrix is a better choice because it is indicative of the shape of the density, as seen in the kernel density literature: when the underlying distribution is Gaussian, the optimal bandwidth matrix is a scaled version of the covariance (\cref{eqn:kde-bandwidth}). For most applications, the covariance is unknown and the sample covariance $\hat{\Sigma}_n$ is used instead. It is clear from \cref{cor:bandwidth-assumption3} that $B_n = \frac{1}{n^{3/2}} \hat{\Sigma}_n$ is a good choice as long as $\e[\hat{\Sigma}_n]$ is finite, which can be ensured as long as the underlying distribution has finite variance.

Therefore, the sample variance used in the standard ensemble Kalman filter (\cref{eqn:std-enkf-bandwidth}) needs to be scaled by the ensemble size:
\begin{equation*}
  B_t^f = \frac{C}{n^p} \hat{\Sigma}_t^f \text{ with } C \text{ finite and }  p \geq 3/2. \label{eqn:std-enkf-bandwidth-scaled}
\end{equation*}
Furthermore, the state transitions are required to have finite variance for $\e[\hat{\Sigma}_t^f]$ to be finite. Covariance tapering (or, localization, as it is commonly called in the ensemble Kalman filter literature) should continued to be used to ensure that the sample covariance is full-rank.

\hl{why linearization of $h_t$ doesn't matter asymptotically:}
\begin{align*}
  \tr(K_t)^2
    &= \tr[B_t^{f} H_t^T ( \Sigma_o + H_t B_t^f H_t^T )^{-1}]^2 \\
    &\leq \tr[B_t^{f}]^2 \tr[H_t^T ( \Sigma_o + H_t B_t^f H_t^T )^{-1}]^2 \to 0
\end{align*}
since $\tr[B_t^{f}] \to 0$.

\begin{comment}
\begin{sidewaysfigure}
    \resizebox{\textwidth}{!}{\input{figures/illustration/R/density.tex}}
    %\includegraphics[width=\textwidth]{figures/illustration/R/illustration.tikz}
    \caption{\emph{Depicting the filter algorithms.}}
    \label{fig:illustration}
\end{sidewaysfigure}

\begin{sidewaysfigure}
    \resizebox{\textwidth}{!}{\input{figures/illustration/R/cdf.tex}}
    %\includegraphics[width=\textwidth]{figures/illustration/R/illustration.tikz}
    \caption{\emph{Depicting the filter algorithms.}}
    \label{fig:illustration2}
\end{sidewaysfigure}
\end{comment}

\subsection{Almost sure convergence}
Before proving almost sure convergence of the ensemble Kalman filter, we draw parallels between the particle and ensemble Kalman filters to provide intuition behind the proof. Assume that the set of particles from the previous analysis step $\{(x_{t-1}^{a(i)}, w_{t-1}^{a(i)} = \frac{1}{n})\}_{i=1}^n$ are the same for both the particle and ensemble Kalman filters and therefore $\pi_{t-1|1:t-1}^n = \psi_{t-1|1:t-1}^n \to \pi_{t-1|1:t-1}$ by \cref{thm:pf-almostsure}.

As in the particle filter, the forecast particles $\{(x_t^{f(i)}, w_t^{f(i)} = \frac{1}{n})\}$ are asymptotically guaranteed to be from the optimal forecast distribution. The particle filter then approximates the forecast distribution with point masses centered at the forecast particles ($\pi_{t|1:t-1}^n$), while the ensemble Kalman filter uses Gaussian kernels instead ($\psi_{t|1:t-1}^n$). Intuitively, if the bandwidths of the Gaussian kernels go to zero at the rate stated in \cref{lem:dist-gaussian-delta}, the particle filter's approximation of the forecast distribution is recovered, thereby providing convergence of the ensemble Kalman filter's approximation of the forecast distribution to the optimal forecast distribution. As in the proof of \cref{thm:pf-almostsure}, continuity of $u_t$ ensures that the approximation of the analysis distribution in the update step ($\tilde{\psi}_{t|1:t-1}^n$) converges to the optimal analysis distribution. However, instead of using that distribution to approximate the analysis distribution, the mixture of Gaussian kernels is replaced by the empirical distribution ($\psi_{t|1:t}^n$) instead---convergence is ensured with another application of \cref{lem:dist-gaussian-delta}.

\begin{theorem}
\label{thm:enkf-almostsure}
Suppose the transition density $f$ is Feller, $\Sigma_t^o$ is full rank, and $B_t^f$ satisfies \cref{eqn:bandwidth-assumption}. Then,
\begin{gather*}
  \lim_{n\to\infty} \psi_{t|1:t-1}^n = \pi_{t|1:t-1}, \\
  \lim_{n\to\infty} \tilde{\psi}_{t|1:t}^n = \pi_{t|1:t}, \\
  \lim_{n\to\infty} \psi_{t|1:t}^n = \pi_{t|1:t},
\end{gather*}
almost surely.
\end{theorem}

\begin{proof}
We prove the theorem by induction. Assume $\psi_{t-1|1:t-1}^n \to \pi_{t-1|1:t-1}$. By the Feller property of $f$, $\psi_{t-1|1:t-1}^n f = \frac{1}{n} \sum_{i=1}^n f(dx_t \,|\, x_{t-1}^{a(i)}) \to \pi_{t-1|1:t-1} f = \pi_{t|1:t-1}$. Since $B_t^f$ satisfies \cref{eqn:bandwidth-assumption}, $\psi_{t|1:t-1}^n = \mathbb{Q}_n(\psi_{t-1|1:t-1}^n f) \to \pi_{t|1:t-1}$ by \cref{cor:dist-gaussian-true}. Continuity in $u_t$ ensures $\tilde{\psi}_{t|1:t}^n = u_t(\psi_{t|1:t-1}^n) \to u_t(\pi_{t|1:t-1}) = \pi_{t|1:t}$.

Once it is shown that $B_t^a$ fulfills the assumptions of \cref{eqn:bandwidth-assumption}, we have $d(\psi_{t|1:t}^n, \tilde{\psi}_{t|1:t}^n) \to 0$ by \cref{lem:dist-gaussian-delta} and therefore $\psi_{t|1:t}^n \to \pi_{t|1:t}$ by \cref{cor:dist-gaussian-true}. Let $Z \sim \normal(0, I)$. For any vector $c$,
\begin{align*}
  c^T B_t^a c
    &\leq c^T (B_t^{f} - B_t^{f} H_t^T ( \Sigma_t^o + H_t B_t^f H_t^T )^{-1} H_t B_t^{f}) c
    \leq c^T B_t^{f} c,
\end{align*}
thus
\begin{align*}
  \prob(Z^T B_t^a Z > \tfrac{1}{n} \,|\, \cdot)
    &\leq \prob(Z^T B_t^f Z > \tfrac{1}{n} \,|\, \cdot).
\end{align*}
and we have our result by assumption.
\end{proof}

\subsection{Bias-variance tradeoff}

