%!TEX root = ../thesis.tex
\synctex=1

% The idea of using a mixture of Gaussians as part of a filtering algorithm is not new \citep{Alspach1972,Chen2000,Hurzeler1998,Musso2001}; this algorithm was developed independently through the connection found with the \gls{enkf}. In particular, the \gls{grpf} is a special case of a pre-regularized particle filter, hence we chose to name the filter after the pre-regularized particle filter even though our discovery was after we made the connection.

%Regularized particle filters uses a mixture of kernels instead of point masses to either approximate the forecast distribution (pre-regularized particle filters) or the analysis distribution (post-regularized particle filters) \citep{Musso2001}.

The unique combination of applying pre-\gls{rpf} with Gaussian kernels to linear measurement models with Gaussian errors is amenable to closed-form expressions to the estimators, which is not true for many applications of the \gls{rpf}. Like the \gls{pf}, the \gls{rpf} was developed for general state-space models such as the one outlined in \Cref{eqn1:general-filter}, thus analytic forms could not be derived for the resulting estimators and rejection sampling is performed instead. In the special case that we have presented, an analytic solution to the estimator is easily derived, e.g., $\enkfttt_{1:t}(x_t)$, and thus allows for straightforward and efficient sampling from the estimator, important for the resampling step.

In this section, we explore the pre-\gls{rpf} with Gaussian kernels applied to the state-space model outlined at the beginning of the chapter.
%In this section, we will harness the intuition drawn in \Cref{sec:enkf-connection} and develop a particle filter algorithm that hopefully borrows the \gls{enkf}'s ability to deal with high-dimensional states.
Like the \gls{enkf}, the forecast distribution is approximated with a mixture of Gaussian kernels with a bandwidth that scales by the number of particles. We first present the estimators of the optimal filtering densities, deferring the bandwidth choice to \Cref{sec:bandwidth-selection}. The filter does not include the approximations made in the update and resampling steps of the \gls{enkf}. We make some hypotheses regarding the roles of these approximations in the success of the \gls{enkf} and discuss further exploration in \Cref{sec1:grpf-future-work}. Since the derivation of the estimators to the optimal filtering densities is quite similar to the derivations in the last section on re-interpretating the \gls{enkf}, we directly state the estimators without deriving them; for more detailed derivations, refer to \Cref{sect:enkf-reinterpretation}. %After describing the algorithm, we relate the algorithm to other filtering algorithms in the literature. One of these relationships will help choose the bandwidth matrix, so we will defer the discussion of the bandwidth matrix to \Cref{sec:bandwidth-selection}.


% by using a mixture of Gaussians instead of point masses. The bandwidth matrix for these Gaussians will need to be at least scaled by the sample size for the algorithm to be asymptotically optimal. Even though there is a parallel between kernel density estimators and the approximations used in this new algorithm, the conditions on the bandwidths will be different because optimality is defined differently in the two literatures.

%After describing the algorithm, we will answer how fast the bandwidths should go to zero and provide suggestions on how to choose the bandwidth matrices. We will discuss a scaled version of the full-rank estimator of the variance as a special case on how to choose bandwidth matrices. We can then prove almost sure convergence for the \hl{new} filter.


\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
  \small
  \textbf{Inputs}: $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{1:t-1})$
  ~\\
  \textbf{Output}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{x_t^{f(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{1:t-1})$
    \item $\{x_t^{a(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{1:t})$
  \end{itemize}

  \begin{enumerate}[leftmargin=*]
    \item \ul{Forecast step}: Sample $x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)})$ for each $m = 1, ..., M$.

    \item \ul{Update step}:
      \begin{enumerate}[leftmargin=*,noitemsep]
      \item Calculate the Kalman gain:
            \begin{gather*}
              K_t = B_t^f H_t^T ( V_t + H_t B_t^f H_t^T )^{-1}.
            \end{gather*}
            and
            \begin{align*}
              l_t^{(m)} = \phi(y_t; H_t x_t^{f(m)}, V_t + H_t B_t^f H_t^T)
            \end{align*}
            for each $m = 1,...,M$.

      \item Calculate
            \begin{gather*}
              \tilde{x}_t^{a(m)}
                = x_t^{f(m)} + K_t[y_t - H_t x_t^{f(m)}], \\
              \tilde{w}_t^{(m)}
                = l_t^{(m)} / \textstyle{\sum}_{n=1}^M l_t^{(n)},
            \end{gather*}
            for each $m = 1,...,M$.
    \end{enumerate}

    \item \ul{Resampling step}:
      \begin{enumerate}[leftmargin=*,noitemsep]
      \item Calculate $B_t^a = B_t^f - K_t H_t B_t^f$.
      \item Resample
            \begin{gather*}
              j_m = i \text{ with probability } \tilde{w}_t^{(i)}, \\
              x_t^{a(m)} \sim \normal(\tilde{x}_t^{(j_m)}, B_t^a),
            \end{gather*}
            for each $m = 1,...,M$.
    \end{enumerate}
  \end{enumerate}
\end{minipage}
}
\captionnew{Pre-regularized particle filter with Gaussian kernels.}{}
\label{alg:grpf}
\end{alg}

Suppose we have generated simple random samples from the approximate forecast distribution: $\{x_t^{f(m)}\}_{m=1}^M \approxdist X_t \,|\, y_{1:t-1}$. As with the \gls{enkf}, the forecast distribution is approximated by a \gls{kde} with Gaussian kernels centered at the forecast samples and forecast bandwidth $B_t^f$:
\begin{align*}
  \nf_{1:t-1}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \phi(x_t; x_t^{f(m)}, B_t^f).
\end{align*}
After observing $y_t$, this estimator is plugged into the optimal predictive likelihood and analysis distribution, deriving estimators to those densities. The estimator to the predictive likelihood is
\begin{align*}
  \nf_{1:t-1}(y_t)
    \equiv \int g(y_t \,|\, x_t) \nf_{1:t-1}(x_t) dx_t
    = \frac{1}{M} \sum_{m=1}^M l_t^{(m)}
\end{align*}
with $l_t^{(m)} \equiv \frac{1}{M} \phi(y_t; H_t x_t^{f(m)}, V_t + H_t B_t^f H_t^T)$. Then, an estimator of the analysis distribution is derived:
\begin{align}
  \nft_{1:t}(x_t)
    \equiv \frac{\phi(y_t; H_t x_t, V_t) \nf_{1:t-1}(x_t)}{\nf_{1:t-1}(y_t)}
    = \sum_{m=1}^M \tilde{w}_t^{(m)} \phi(x_t; \tilde{x}_t^{a(m)}, B_t^a),
\label{eqn1:grpf-analysis}
\end{align}
with updated weights $\tilde{w}_t^{(m)} = l_t^{(m)} / \sum_{n=1}^M l_t^{(n)}$ and updated centers and analysis bandwidths as follows:
\begin{subequations}
\begin{align}
  \tilde{x}_t^{a(m)}
    &= x_t^{f(m)} + \hat{\Sigma}_t^{f} H_t^T ( V_t + H_t B_t^f H_t^T )^{-1} (y_t - H_t x_t^{f(m)}), \label{eqn1:grpf-analysis-sample}\\
  B_t^a
    &= B_t^{f} - B_t^{f} H_t^T ( V_t + H_t B_t^f H_t^T )^{-1} H_t B_t^{f}, \label{eqn1:grpf-analysis-var}
\end{align}
\label{eqn1:grpf-analysis-moments}%
\end{subequations}
for each $m = 1,..., M$.

Like the \gls{enkf}, resampling must be performed to obtain an empirical density estimator required in the next forecast step. In particular, a simple random sample $\{x_t^{a(m)}\}_{m=1}^M \sim \nft_{1:t}(x_t)$ is generated %as follows:
% \begin{gather*}
%   j_m \sim \cat(\tilde{w}_1, ..., \tilde{w}_M), \\
%   x_t^{a(m)} \sim \normal(\tilde{x}_t^{a(j_m)}, B_t^a),
% \end{gather*}
via any resampling algorithm outlined in \Cref{sec:resampling}. These new samples provide another approximation to the analysis distribution:
\begin{align*}
  \nf_{1:t}(x_t)
    &= \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{a(m)}}(x_t).
%\label{eqn1:grpf-analysis2}
\end{align*}
\Cref{alg:grpf} summarizes the algorithm. %, which we call the \gls{grpf}. The source of its name will become clear in the next section.
In \Cref{fig1:smc-algorithms}, we repeat \Cref{fig1:smc-algorithms2} and add the pre-\gls{rpf} between the \gls{bf} and \gls{enkf} algorithms to provide a side-by-side comparison of the three algorithms applied to the state-space model outlined at the beginning of this chapter.  \Cref{fig1:smc-algorithms-pic} illustrates the comparisons.

\begin{sidewaysfigure}
  \centering
  \resizebox{.9\textwidth}{!}{\input{grpf/algorithm-compare-table}}
  \captionnew{Comparison of filtering algorithms.}{This figure is the same as \Cref{fig1:smc-algorithms2} with the pre-\gls{rpf} algorithm added between the bootstrap and ensemble Kalman filter for comparison. For each time $t$, these algorithms require samples $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$. To directly compare the algorithms, resampling after every filtering step is required of the bootstrap filter and the variance inflation step has been removed from the ensemble Kalman filter.}
  \label{fig1:smc-algorithms}
\end{sidewaysfigure}

\begin{sidewaysfigure}
  \centering
  \includegraphics[width=.9\textwidth, trim=0 0 0 1.25in, clip]{grpf/figures/illustration/comparison-densities}
  \captionnew{Illustration of filtering algorithms.}{The ``optimal'' column represents the optimal filtering densities. The optimal forecast and analysis distributions are shown in the first and last two rows, respectively. The measurement density is represented by the green line with the measurement represented by the green square. Black points represent the forecast or analysis samples and the size of the black point represents the number of samples. The vertical line above the black points represents a point mass centered at a sample; its height represents its weight. The gray line above the black points represents the Gaussian kernels. The black lines in the ``pre-RPF'' and ``EnKF'' columns represent the estimators of the forecast or analysis distribution.}
  \label{fig1:smc-algorithms-pic}
\end{sidewaysfigure}

