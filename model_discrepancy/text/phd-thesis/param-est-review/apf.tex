%!TEX root = ../thesis.tex
\section{Auxiliary particle filter (APF)} \label{sec:apf}
\todo[inline]{Introduce the auxiliary particle filter algorithm.}
%In APF, the first-stage pre-samples state trajectories that are more likely to survive, which introduces a bias. The bias is corrected in the second-stage by adjusting the weights and then resampling. (Refer to the appendix on APF for more details.)

Suppose we have approximately random samples from the joint analysis distribution, $x_{1:t-1}^{a(m)} \approxdist X_{1:t-1} \,|\, y_{1:t-1}$ for $m = 1,...,M$. Approximate the joint analysis density as
\begin{align*}
  p(x_{1:t-1})
    \approx \frac{1}{M} \sum_{m=1}^M \delta_{x_{1:t-1}^{a(m)}}(x_{1:t-1})
\end{align*}
Plug in this approximation into the optimal forecast distribution (\Cref{eqn1:forecast}) to sample the forecast particles at time $t$: $x_{1:t}^{f(m)} \sim f(x_t \,|\, x_{1:t-1}^{a(m)}) = f(x_t \,|\, x_{t-1}^{a(m)})$ for each $m=1,...,M$. These samples provide an approximation to the joint forecast distribution:
\begin{align}
  p(x_{1:t} \,|\, y_{1:t-1}) \approx \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{f(m)}}(x_t). \label{eqn2:apf-forecast}
\end{align}
After observing $y_t$, the state can be updated using this estimator of the forecast distribution. Up to this point, the filter is the same as the bootstrap filter. The auxiliary particle filter differs from the bootstrap filter in the update step.

Recall that the joint analysis distribution is
\begin{align*}
  p(x_{1:t} \,|\, y_{1:t})
    = \frac{g(y_t \,|\, x_t)p(x_{1:t} \,|\, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})}
\end{align*}
\todo[inline]{Explain why we like $p(y_t \,|\, x_{t-1})$. Something about particles are more likely to survive.}
\begin{align*}
  p(y_t \,|\, x_{t-1})
    = \int g(y_t \,|\, x_t) p(x_t \,|\, x_{t-1}) dx_t
\end{align*}
There is usually no analytic form for $p(y_t \,|\, x_{t-1})$. Multiplying the numerator and denominator by $\hat{p}(y_t \,|\, x_{t-1})$, we have
\begin{align*}
  p(x_{1:t} \,|\, y_{1:t})
    = \frac{g(y_t \,|\, x_t)}{\hat{p}(y_t \,|\, x_{1:t-1})p(y_t \,|\, y_{1:t-1})} \times \hat{p}(y_t \,|\, x_{t-1}) p(x_{1:t} \,|\, y_{1:t-1})
\end{align*}
The idea of \gls{apf} is to switch the resampling and update steps of the filter, where the particles are first resampled with $\hat{p}(y_t \,|\, x_{t-1})$, choosing particles that are more likely to survive.

Let's resample to get an estimator for $\hat{p}(y_t \,|\, x_{t-1}) p(x_{1:t} \,|\, y_{1:t-1})$:
\begin{align*}
  \tilde{x}_{1:t}^{f(m)}
    \sim \sum_{m=1}^M \hat{p}(y_t \,|\, x_{t-1}^{a(m)}) \delta_{x_{1:t}^{f(m)}}(x_{1:t}).
\end{align*}
Let $v_t^{a(m)} = \hat{p}(y_t \,|\, x_{t-1}^{a(m)})$
giving us the empirical density:
\begin{align}
  \hat{p}(y_t \,|\, x_{t-1}) p(x_{1:t} \,|\, y_{1:t-1})
    \approx \frac{1}{M} \sum_{m=1}^M \delta_{\tilde{x}_{1:t}^{f(m)}} (x_{1:t})
\label{eqn2:apf-forecast}
\end{align}
Now, we can get an approximation of the analysis distribution. Let $\tilde{w}_t^{(m)} = g(y_t \,|\, \tilde{x}_t^{f(m)}) / \hat{p}(y_t \,|\, \tilde{x}_{t-1}^{a(m)})$ and $w_t^{(m)} = \tilde{w}_t^{(m)}/\sum_{n=1}^M \tilde{w}_t^{(n)}$ for each $m=1,...,M$. Plugging in the above approximation into joint analysis distribution, we have:
\begin{align*}
  p(x_{1:t} \,|\, y_{1:t})
    &\approx \sum_{m=1}^M w_t^{(m)} \delta_{\tilde{x}_t^{f(m)}} (x_t)
\end{align*}
At this point, the above approximation can be used to forecast for time $t+1$ or the particles can be resampled again and then used to forecast.

Since the introduction of the \gls{apf} by \citet{Pitt1999}, rigorous analysis of the \gls{apf} have been published about the \gls{apf}. These analyses reformulated \gls{apf} as a \gls{sirs} algorithm (see \citet{Whiteley} for a review), which we have loosely presented here. We make a few remarks that contrasts the original formulation by \citeauthor{Pitt1999} to its current formulation in the literature:
\begin{itemize}[leftmargin=*]
  \item There is usually no analytic form for $p(y_t \,|\, x_{t-1})$, so \citeauthor{Pitt1999} recommended approximating $p(y_t \,|\, x_{t-1})$ with $g(y_t \,|\, \mu(x_{t-1}))$, where $\mu(x_{t-1})$ is a point estimate of $f(x_t \,|\, x_{t-1})$. \citet{Johansen2008} advise against using $g(y_t \,|\, \mu(x_{t-1}))$ because it may yield estimators with larger variance than if using \gls{sirs}.
  \item In the original formulation of the algorithm, the particles were resampled a second time with weights $w_t^{(m)}$, but \citet{Whiteley} recommend not to resample a second time, since resampling increases Monte Carlo variance.
  \item In the original formulation, the estimator in \Cref{eqn2:apf-forecast} can be viewed as a biased estimator of $p(x_{1:t} \,|\, y_{1:t-1})$, which later gets corrected by the weights $w_t^{(m)}$.
\end{itemize}







