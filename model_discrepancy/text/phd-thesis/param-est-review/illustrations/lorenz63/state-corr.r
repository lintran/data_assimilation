


source("~/data_assimilation/model_discrepancy/code/lorenz63/lorenz63.r")
library(ggplot2)
library(grid)
library(gridExtra)
library(extrafont)

parms = c(sigma = 10, rho = 28, beta = 8/3) # theta
start = rep(.01,3)
times = seq(0, 300, by = 0.01)
length(times)

starts1 = lsode(start, times, deriv, c(sigma = 10, rho = 28, beta = 8/3, alpha = 1))
starts1 = starts1[1000:length(times), -1]

# starts2 = expand.grid(x = seq(-20,20,.5), y = seq(-25,25,.5), z = seq(0, 50, .5))
# starts2 = as.matrix(starts2)

starts = starts1
nstarts = nrow(starts)

dts = sort(unique(c(seq(.02, .5, .04), c(.18, .19, .2))))
cors = lapply(dts, function(dt) {
  print(dt)
  data = sapply(1:5000, function(i) {
    start = starts[sample(nstarts, 1),]
    #sigma = runif(1, 5, 15)
    sigma = 10
    out = rk(start, c(0, dt), deriv, c(sigma = sigma, rho = 28, beta = 8/3, alpha = 1), method = "rk2", hini = .01)
    c(start, sigma, out[2,-1])
  })
  data = t(data)
  out = cor(data)
  colnames(out) = c(paste0(c("x", "y", "z"), 0), "sigma0", paste0(c("x", "y", "z"), 1))
  rownames(out) = c(paste0(c("x", "y", "z"), 0), "sigma0", paste0(c("x", "y", "z"), 1))
  out
})
names(cors) = dts[length(cors)]
cors

cors2 = lapply(cors, function(l) l[-4,5:7])
# cors2 = lapply(cors, function(l) l[5:7,5:7])
blah = lapply(1:length(cors2), function(i) 
  data.frame(state0 = rep(rownames(cors2[[i]]), ncol(cors2[[i]])),
             state1 = rep(colnames(cors2[[i]]), each = nrow(cors2[[i]])),
             cor = as.numeric(cors2[[i]]),
             dt = dts[i]))
plot.me = do.call(rbind, blah)
plot.me = subset(plot.me, cor != 1)
plot.me$state0.type1 = substr(plot.me$state0, 1, 1)
plot.me$state0.type2 = as.numeric(substr(plot.me$state0, 2, 2) == 1)
levels = paste0(rep(c("x", "y", "z"), 2), rep(0:1, each = 3))
labels = paste0(rep(c("x", "y", "z"), 2), "[t[n", rep(c("-1", ""), each = 3), "]]")
plot.me$state1 = factor(plot.me$state1, levels = levels, labels = labels)
plot.me$state0 = factor(plot.me$state0, levels = levels, labels = labels)
plot.me$state0.type1 = factor(plot.me$state0.type1)
plot.me$state0.type2 = factor(plot.me$state0.type2, levels = 1:0, labels = rev(c("t[n-1]", "t[n]")))

p = ggplot(plot.me, aes(dt, cor, color = state0.type1)) +  
  geom_hline(yintercept = 0, size = .25) + 
  geom_line(size = .5) + geom_point(size = 1.25) +
  facet_grid(state0.type2 ~ state1, labeller = label_parsed, scales = "free_y", space = "free_y")
p = p + xlab(expression(paste(Delta, t[n]))) + ylab("correlation")
p = p + scale_colour_discrete(name = "direction") + 
  scale_shape_discrete(name = "time", solid = FALSE,
                       breaks = levels(plot.me$state0.type2), 
                       labels = c(expression(t[n-1]), expression(t[n])))
p2 = p + theme_bw() +
  theme(text = element_text(size = 10),
        axis.text.x = element_text(angle = 90, vjust = .5),
        plot.margin=unit(c(0,0,0,0),"mm"),
        legend.key.size = unit(3, "mm"),
        legend.margin = unit(0, "mm"),
        #legend.direction = "horizontal",
        legend.box = "horizontal",
        #legend.box.just = "bottom",
        legend.position = "bottom")
# print(p2)

pdf("~/data_assimilation/model_discrepancy/text/phd-thesis/param-est-review/illustrations/lorenz63/state-corr.pdf", width = 5.5, height = 4)
print(p2)
dev.off()

