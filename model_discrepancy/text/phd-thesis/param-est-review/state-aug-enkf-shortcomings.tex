%!TEX root = ../thesis.tex

Before describing the shortcomings, we examine how the \gls{enkf} updates the parameter. Suppose we have samples of the augmented state: $z_{t-1}^{a(m)} \equiv (x_{t-1}^{a(m)}, \theta_{t-1}^{a(m)}) \approxdist p(z_{t-1} \,|\, y_{0:t-1})$. At time $t$, generate augmented state samples $z_t^{f(m)} \equiv (x_t^{f(m)}, \theta_t^{f(m)})$ from the forecast distribution:
\begin{gather*}
  \theta_t^{f(m)} \sim q(\theta_t \,|\, \theta_{t-1}^{a(m)}), \\
  x_t^{f(m)} \sim g(x_t \,|\, x_{t-1}^{a(m)}, \theta_t^{f(m)}).
\end{gather*}
%where $\eta_t^{(m)} \sim \normal(0, U_t(\theta_{t-1}^{a(m)}))$ and $\zeta_t^{(m)} \sim \zeta_t$ for $m = 1,...,M$.
Calculate the sample covariance with the forecast samples:
\begin{align*}
  \hat{\Sigma}_t^f =
  \begin{bmatrix}
    \hat{\Sigma}_{t,xx}^f & \hat{\Sigma}_{t,x\theta}^f \\
    \hat{\Sigma}_{t,\theta x}^f & \hat{\Sigma}_{t,\theta\theta}^f
  \end{bmatrix},
\end{align*}
where $\hat{\Sigma}_{t,xx}^f$ and $\hat{\Sigma}_{t,\theta\theta}^f$ are the sample variances of the forecast state and parameter, respectively, and $\hat{\Sigma}_{t,x\theta}^f = (\hat{\Sigma}_{t,\theta x}^f)^T$ is the sample covariance between the forecast state and parameter.

After observing $y_t$, the analysis samples are calculated by applying \Cref{eqn1:enkf-analysis-sample} to the state-space model in \Cref{eqn2:state-space-timevarying}. The particular choice of \gls{enkf} update (i.e., original \gls{enkf}, perturbed observations, or square-root filter) is not important to the discussion, thus for simplicity, we apply the update of the original \gls{enkf} (c.f., \Cref{eqn1:enkf-analysis-sample}) to each Gaussian kernel with center $z_t^{f(m)}$ and bandwidth $\hat{\Sigma}_t^f$:
\begin{align}
  z_t^{a(m)} = z_t^{f(m)} + K_t [y_t - H_t(\theta_t^{f(m)}) z_t^{f(m)} ],
\label{eqn2:augmented-state-update}
\end{align}
where $K_t$ is the Kalman gain:
\begin{gather}
  K_t \equiv \cov(Z_t, Y_t \,|\, y_{0:t-1}) \var(Y_t \,|\, y_{0:t-1})^{-1},
\label{eqn2:kg}
\end{gather}
where the expression is taken from \Cref{eqn1:kf-kg}.
% and
% \begin{align*}
%   \cov(Y_t, Z_t \,|\, y_{1:t-1})
%     &= \tilde{H}_t(\theta_t^{f(m)}) \hat{\Sigma}_t^f
%     = \begin{bmatrix} H_t(\theta_t^{f(m)}) \hat{\Sigma}_{t,xx}^f \\ H_t(\theta_t^{f(m)}) \hat{\Sigma}_{t,x\theta}^f \end{bmatrix} \\
%   \var(Y_t \,|\, y_{1:t-1})
%     &= V_t(\theta_t^{f(m)}) + \tilde{H}_t(\theta_t^{f(m)}) \hat{\Sigma}_t \tilde{H}_t^T(\theta_t^{f(m)})
% \end{align*}

Notice that there are three parts to the update: the measurement residual $y_t - H_t(\theta_t^{f(m)}) z_t^{f(m)}$; the conditional variance of the measurement, $\var(Y_t \,|\, y_{0:t-1})$; and the conditional covariance between the measurement and the state, $\cov(Y_t, Z_t \,|\, y_{0:t-1})$. While the measurement residual provides important information about the magnitiude that the forecast state sample differs from the measurement, the last term $\cov(Y_t, Z_t \,|\, y_{0:t-1})$ is the most important because it \textit{controls the influence} of the measurement residual. For example, if $\cov(Y_t, Z_t \,|\, y_{1:t-1})$ is zero, then the state does not get updated. Let's examine the term more closely:
\begin{align*}
  \cov(Y_t, Z_t \,|\, y_{1:t-1})
    % &= \cov(\tilde{H}_t(\theta_t^{f(m)}) Z_t + \epsilon_t(\theta_t^{f(m)}), Z_t \,|\, y_{1:t-1}) \\
    % &= \cov(H_t(\theta_t^{f(m)}) X_t, Z_t \,|\, y_{1:t-1})\\
    % &= H_t(\theta_t^{f(m)}) \cov(X_t, Z_t \,|\, y_{1:t-1})\\
    &= \tilde{H}_t(\theta_t^{f(m)}) \hat{\Sigma}_t^f
    = \begin{bmatrix} H_t(\theta_t^{f(m)}) \hat{\Sigma}_{t,xx}^f \\ H_t(\theta_t^{f(m)}) \hat{\Sigma}_{t,x\theta}^f \end{bmatrix}
\end{align*}
The first and second rows of this conditional covariance---and thus the Kalman gain---updates the state $X_t$ and parameter $\theta_t$, respectively.
% For ease of discussion, we will use the following shorthand for the Kalman gain term:
% \begin{align}
%   K_t(\theta, \Sigma) \equiv \underbrace{\Sigma H_t^T(\theta)}_{(1)} \underbrace{[V_t(\theta) + H_t(\theta) \hat{\Sigma}_{t,xx}^f H_t^T(\theta) ]^{-1}}_{(2)}.
% \label{eqn2:kg}
% \end{align}
% \begin{equation*}
% \begin{aligned}
%   x_t^{a(m)}
%     &= x_t^{f(m)} + K_t(\theta_{t-1}^{a(m)}, \hat{\Sigma}_{t,xx}^f, \hat{\Sigma}_{t,xx}^f)[y_t - H_t(\theta_{t-1}^{a(m)})x_t^{f(m)}],\\
%   \theta_t^{a(m)}
%     &= \theta_t^{f(m)} + K_t(\theta_{t-1}^{a(m)}, \hat{\Sigma}_{t,\theta x}^f, \hat{\Sigma}_{t,xx}^f) [y_t - H_t(\theta_{t-1}^{a(m)}) x_t^{f(m)}].
% \end{aligned}
% \end{equation*}
This shows us that there are two important components to the \gls{enkf} \textbf{linear update} of the parameters: the covariance between the state and parameter, $\hat{\Sigma}_{t,\theta x}^f$, as mentioned by previous authors\footnote{\citett{Stroud2007,Delsole2010,Frei2012} are the authors in reference.}, \textit{and} the observation mapping $H_t(\cdot)$. Therefore, the parameter gets updated \textit{only if} there is correlation between the forecasted state and parameter ($\hat{\Sigma}_{t,\theta x}^f$) \textit{and} the part of the state that is correlated with the parameter is observed ($H_t(\cdot)$). Otherwise, the parameter diverges randomly or stays the same as its proposed initial state when applying artificial evolution of parameters or state augmentation, respectively. In the next section, we demonstrate with the Lorenz 1963 system that the observation mapping $H_t(\cdot)$ is just as important as the covariance between the parameter and the state, $\hat{\Sigma}_{t,\theta x}^f$. %in some situations.
% \citeauthor{Stroud2007} estimate the parameter $\sigma$ of the following state space model:
% \begin{gather*}
%   x_t = a_t(x_{t-1}) + \eta_t, \quad
%     \eta_t \sim \normal(0, U_t) \\
%   y_t = H_t x_t + \sigma \epsilon_t, \quad
%     \epsilon_t \sim \normal(0, V_t)
% \end{gather*}
% They noted that $\cov(Y_t, \sigma) = 0$, thus the linear update rule does not apply. While it is certainly true that $\cov(Y_t, \sigma) = 0$, a marginal covariance $\cov(Y_t, \sigma) = 0$ of zero \textit{does not} generally imply that the conditional covariance $\cov(Y_t, \sigma \,|\, y_{1:t-1})$ is also zero. In this particular case, $\cov(Y_t, \sigma \,|\, y_{1:t-1})$ happens to be zero and so therefore $\hat{\Sigma}_{t,\theta x}^f = 0$ and the linear update rule truly does not apply\footnote{The linear update rule is not a problem for the \acrlong{grpf} of \Cref{sec:grpf}, since its update step consists of linearly updating the state \textit{in addition to} updating the weight of each sample.}.

Besides the linear update issue, we remark on a few other issues not mentioned by previous authors:
\begin{itemize}[leftmargin=*]
  \item \textbf{Unbounded update}. Often, parameters, especially those governing deterministic atmospheric models, have a range of values in which the dynamics of the system are realistic and/or numerically stable. Notice that the measurement residual in the update equation in \Cref{eqn2:augmented-state-update} is unbounded, hence a user cannot restrict the parameter update to be within pre-specified bounds. %without ad hoc interventions.
  %\item \textbf{Localization cannot be simply applied}. Recall that localization---the tapering of the sample variance of the forecasted state ($\hat{\Sigma}_{t,xx}^f$) to alleviate its rank deficiency with small ensemble sizes---is crucial to the success of filtering high-dimensional state spaces with the \gls{enkf}. The rank deficiency issue will only be excarbated since the small ensemble size will additionally be used to estimate the sample covariance of the state and parameter ($\hat{\Sigma}_{t,x\theta}^f$). This problem cannot be straightforwardly be alleviated by localization because there is no sense of distance between the state and parameter, thus larger ensemble sizes will be needed to better estimate $\hat{\Sigma}_{t,x\theta}^f$ and hence parameter estimators.
  \item \textbf{Different Kalman gain for each update}. When estimating the measurement parameter $\theta_y$, the update of the analysis distribution requires the calculation of a different Kalman gain for each kernel in the mixture of Gaussians---a computationally expensive procedure. \citet{Frei2012} faced this issue in their development of a combined \gls{enkf} and \gls{pf} algorithm to estimate the parameter $\rho$ in the measurement error $V_t(\rho)$. They circumvented the problem by replacing the samples $V_t(\theta_t^{f(m)})$ in \Cref{eqn2:kg} with a point estimator, e.g., $V_t(\bar{\theta}_t^{f(m)})$ or $\overline{V_t(\cdot)}$ (see Algorithm 2 in their paper), where $\bar{x}$ denotes the sample mean calculated with the samples $\{x_i\}_{i=1}^n$. While a promising approach, a bias is introduced when sampling with a point estimator, which they do not correct for in their algorithm. We use a similar approach in our algorithm, but we also add another step to correct for the bias from using a point estimator. %Lastly, there are no guarantees of convergence to the optimal parameters, even if the \gls{enkf} was later proven \todo{Cari didn't understand} to converge to the optimal filtering densities.\todo{move this last sentence to the discussion of artificial evoluation of parameters}
\end{itemize}

In \Cref{sec2:demonstrate-shortcomings}, we use these principles to demonstrate that the \gls{enkf} can poorly estimate parameters of interest. Before doing that, we briefly divert our attention to parametrizing variance inflation, a technique applied with the \gls{enkf} to avoid filter divergence (see \Cref{sec:enkf} for a review).

