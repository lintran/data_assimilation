%!TEX root = ../thesis.tex


\section{Iterated filters (IF)}
\acrshortpl{if} are a class of algorithms that allow for the general estimation of static parameters. When \glspl{if} are used with the particle filter, it has been proven that the algorithms are guaranteed to converge \todo{how?} to the optimal parameter under certain conditions \citep{Ionides2011,Doucet2013}. \acrshortpl{if} were not developed to be applied with only particle filters; the developers of the algorithms mention that they can be used with any filter, including the \gls{enkf}! However, we have not seen \glspl{if} applied with the \gls{enkf} yet, and for good reason---\glspl{if} cannot be directly used with the \gls{enkf} to estimate general parameters, which will be discussed later after introducing the algorithms. %The introduction will be important for the next chapter, where we will develop algorithms to use \gls{enkf} to estimate the state and \acrlong{pf} for the parameters.

% \Acrshortpl{if} can be thought of as a generalization of state augmentation. Like state augmentation, \gls{if} replaces the parameter $\theta$ with a closely related time-varying parameter $\theta_t$. Specifically, the densities\footnote{The algorithm in \citet{Ionides2006} can also be used to estimate parameters of the initial condition, $p(x_0 \,|\, \theta)$. That is not the focus of the dissertation, so we will omit it from the discussion.}
% \begin{equation*}
%     f(x_t \,|\, x_{t-1}, \theta) \text{ and } g(y_t \,|\, x_t, \theta)
% \end{equation*}
% are replaced with closely related densities
% \begin{equation*}
%     f(x_t \,|\, x_{t-1}, \theta_t) \text{ and } g(y_t \,|\, x_t, \theta_t)
%     %\footnote{The densities could also be replaced with $f(x_t \,|\, x_{t-1}, \theta_{t-1}), g(y_t \,|\, x_t, \theta_{t-1}), \text{ and } f(x_0 \,|\, \theta_0)$. I will focus on the above formulation for this dissertation.}
% \end{equation*}
% with $\theta_t \equiv (\theta_{t,x}, \theta_{t,y})$. However, $\theta_t$ is not specified to be constant in time as in \Cref{eqn2:param-constant}. Instead, $\Theta_t$ is parametrized to be a random walk with
% \begin{equation}
% \begin{array}{l@{\hspace{.5in}}l}
%     \e(\Theta_t \,|\, \theta_{t-1}) = \theta_{t-1},&
%     \var(\Theta_t \,|\, \theta_{t-1}) = \sigma^2 \Psi, \\
%     \e(\Theta_0) = \theta,&
%     \var(\Theta_0) = \sigma^2 c^2 \Psi,
% \end{array}
% \label{eqn2:param-random-walk}
% \end{equation}
% where $\Psi$ is pre-specified by the user and $\theta$ is the optimal, but unknown, parameter. Any distribution that satisfies the above constraints can be used for the random walk. The multivariate normal distribution is often used, but there are other distributions that satisfy the above constraints, such as the truncated normal distribution. Alternatively, parameters can be transformed to allow the usage of a multivariate normal distribution.

There are a few reasons to perturb the parameters as in \Cref{eqn2:param-random-walk}. The perturbations enable the exploration of the parameter space to find the optimal parameter $\theta$ and for the parameter search itself to escape local minima. However, the random walk will not converge to $\theta$ if it is allowed to explore the parameter space unconstrained---a convergence mechanism is required. The convergence mechanism used in both \citet{Ionides2006,Ionides2015} is to decrease the variance of the random walk over multiple runs of the filter on the same, pre-collected data $y_{1:T}$. We will be specific later when delineating the algorithms.

\subsection{Maximum likelihood via iterated filtering (IF1)}
The first \gls{if} algorithm (\acrshort{if}1) was developed to perform maximum likelihood estimation \citep{Ionides2006}. In this algorithm, the random walk allows for local exploration of the likelihood surface to search for the maximum of the likelihood function; specifically, to find the \gls{mle} $\hat{\theta} = \argmax_\theta \ell(\theta)$. To do this, choose $\theta_0$ to be an initial guess of the parameter $\theta$, let $b$ be one \gls{if} iteration, i.e., one filter run using $y_{1:T}$, and replace $\sigma^2$ with $\sigma_b^2$. The convergence mechanism is to update the \gls{mle} with every \gls{if} iteration $b$ while decreasing the variance $\sigma_b^2 \to 0$ as $b \to \infty$. The full algorithm is outlined in \Cref{alg:if1}.

\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
\textbf{Input}:
\begin{itemize}[leftmargin=*,noitemsep]
  \item $M$: number of samples
  \item $B$: number of \gls{if} iterations (often, $B \approx 100$)
  \item $y_{1:T}$: observations up to time $T$
  \item $\theta_0$: an initial guess of the parameter $\theta$
  \item perturbation density for $\Theta$ with constraints outlined in \Cref{eqn2:param-random-walk}
  \item cooling factor $0 < \alpha < 1$ (often, $\alpha = 0.95$ or $0.98$)
  \item initial variance multiplier $c^2$
\end{itemize}
\textbf{Output}: Maximum likelihood estimate $\hat{\theta} = \theta_{B+1}$ \\

For each $b = 1,...,B$,
\begin{enumerate}[leftmargin=*]
  \item Initialization:
  \begin{enumerate}[leftmargin=*,noitemsep]
    \item Set $\sigma_b^2 = \alpha^{b-1}$.
    \item Sample $\theta_0^{a(m)} \sim \Theta_0$ with mean $\theta_b$ and variance $\sigma_b^2 c^2 \Psi$ for each $m = 1,...,M$.
    \item Sample $x_0^{a(m)} \sim X_0$ for each $m = 1,...,M$.
    \item Calculate $\bar{\theta}_0$, the sample mean of $\{\theta_0^{a(m)}\}_{m=1}^M$.
  \end{enumerate}

  \item For each $t = 1,...,T$,
  \begin{enumerate}[leftmargin=*,noitemsep]
    \item Forecast $\theta_t^{f(m)} \sim \Theta_t \,|\, \theta_{t-1}^{a(m)}$ with variance $\sigma_b^2 \Psi$ for each $m = 1,...,M$.
    \item Forecast $x_t^{f(m)} \sim X_t \,|\, y_{1:t-1}, \theta_t^{f(m)}$ for each $m = 1,...,M$.
    \item Update $x_t^{a(m)}, \theta_t^{a(m)} \sim X_t, \Theta_t \,|\, y_{1:t}, \theta_t^{f(m)}$ for each $m = 1,...,M$.
    \item Calculate $\bar{\theta}_t$ and $\hat{\Gamma}_t$, the sample mean and variance of $\{\theta_t^{a(m)}\}_{m=1}^M$, respectively.
  \end{enumerate}

  \item Update maximum likelihood estimate:
  \begin{align}
    \theta_{b+1} = \theta_b + \hat{\Gamma}_1 \sum_{t=1}^T \hat{\Gamma}_t^{-1} (\bar{\theta}_t - \bar{\theta}_{t-1})
  \label{eqn2:mle}
  \end{align}
\end{enumerate}
\end{minipage}
}
\caption{\textit{Maximum likelihood via iterated filtering (IF1)}.}
\label{alg:if1}
\end{alg}

Remarkably, \citet{Ionides2011} proved that expectation of the second term in \Cref{eqn2:mle} converges \todo{how} to the score vector $\ell^{(1)}(\theta)$ with a sufficient number of particles and \gls{if} iterations, where $\ell^{(1)}$ is Jacobian of the log-likelihood $\ell$. With this result, they were able to prove that $\theta_b$ in \Cref{eqn2:mle} converges to the MLE. \citet{Doucet2013} published a result that a similar algorithm with additional calculations guarantees the convergence to the observed information matrix $-\ell^{(2)}(\theta)$, where $\ell^{(2)}$ is the Hessian of the log-likelihood $\ell$; this is important in obtaining variance estimates of the \gls{mle}.

\subsection{Iterated Bayes MAP (IF2)}
Very recently, \citet{Ionides2015} proposed a slight modification to the IF1 algorithm that estimates a Bayes \gls{map} estimate (IF2; \Cref{alg:if2}). \todo[inline]{Need to figure out if it estimates Bayes MAP or if it's a data cloning technique (uses MCMC to get MLEs).} They also demonstrated that the algorithm is more efficient at finding the global maximum than IF1.

\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
\textbf{Input}:
\begin{itemize}[leftmargin=*,noitemsep]
  \item $M$: number of samples
  \item $B$: number of \gls{if} iterations (often, $B \approx 100$)
  \item $y_{1:T}$: observations up to time $T$
  \item $\{\theta_0^{(m)}\}_{m=1}^M$: $M$ initial guesses of the parameter $\theta$
  \item the perturbation density for $\Theta$ with constraints outlined in \Cref{eqn2:param-random-walk}, setting $c=1$
  \item a cooling factor $0 < \alpha < 1$ (often, $\alpha = 0.95$ or $0.98$)
\end{itemize}
\textbf{Output}: Bayes \acrlong{map} estimates $\{\theta_B^{(m)}\}_{m=1}^M$ \\

For each $b = 1,...,B$,
\begin{enumerate}[leftmargin=*]
  \item Initialization:
  \begin{enumerate}[leftmargin=*,noitemsep]
    \item Set $\sigma_b^2 = \alpha^{b-1}$.
    \item Sample $\theta_0^{a(m)} \sim \Theta_0$ with mean $\theta_b^{(m)}$ and variance $\sigma_b^2 \Psi$ for each $m = 1,...,M$.
    \item Sample $x_0^{a(m)} \sim X_0$ for each $m = 1,...,M$.
  \end{enumerate}

  \item For each $t = 1,...,T$,
  \begin{enumerate}[leftmargin=*,noitemsep]
    \item Forecast $\theta_t^{f(m)} \sim \Theta_t \,|\, \theta_{t-1}^{a(m)}$ with variance $\sigma_b^2 \Psi$ for each $m = 1,...,M$.
    \item Forecast $x_t^{f(m)} \sim X_t \,|\, y_{1:t-1}, \theta_t^{f(m)}$ for each $m = 1,...,M$.
    \item Update $x_t^{a(m)}, \theta_t^{a(m)} \sim X_t, \Theta_t \,|\, y_{1:t}, \theta_t^{f(m)}$ for each $m = 1,...,M$.
  \end{enumerate}

  \item Update Bayes \gls{map}: $\theta_b^{(m)} = \theta_T^{a(m)}$ for each $m = 1,...,.M$.
\end{enumerate}
\end{minipage}
}
\caption{\textit{Iterated Bayes MAP estimate (IF2)}.}
\label{alg:if2}
\end{alg}

\subsection{Limitations of applying EnKF with IFs}
As mentioned earlier, \gls{if} can be thought of as a generalization of state augmentation. Given the limitations of state augmentation as outlined in \Cref{sec:state-aug}, \glspl{if} cannot simply be applied with \gls{enkf} to estimate any static parameter. In the next chapter, we will resolve this issue by developing algorithms that filter the (often) high-dimensional states with the \gls{enkf} and the parameters with a \gls{pf}. We will first introduce a particular \gls{pf} that will be a crucial component in the later development our algorithms.

