%!TEX root = ../thesis.tex

%The idea behind state augmentation is to augment the state variable with the parameter so that data can be used to update both the state and parameter. As mentioned earlier, both \citet{Stroud2007} and \citet{Frei2012} mentioned that state augmentation is not sufficient to deal with parameters that are not linearly correlated with the state due to linear update rule of the \gls{enkf}. We elaborate on this further.

With artificial evolution of parameters, the parameter $\theta$ is replaced with a closely related time-varying parameter $\theta_t \equiv (\theta_{t,x}, \theta_{t,y})$ and the state transition and measurement densities are replaced with closely related densities
\begin{equation*}
    f(x_t \,|\, x_{t-1}, \theta_{t,x}) \text{ and } g(y_t \,|\, x_t, \theta_{t,y})
\end{equation*}
with $\theta_t = (\theta_{t,x}, \theta_{t,y})$. The parameter $\Theta_t$ is a random walk as follows
\begin{subequations}
\begin{gather}
  \theta_t = \theta_{t-1} + \zeta_t \text{ with density } q(\theta_t \,|\, \theta_{t-1}) \\
  \var(\zeta_0) = \tau^2 \Psi \text{ and } \var(\zeta_t) = \sigma^2 \Psi \text{ for } t > 0
\end{gather}
\label{eqn2:param-random-walk}%
\end{subequations}
% \begin{equation}
% \begin{array}{l@{\hspace{.5in}}l}
%     \e(\Theta_t \,|\, \theta_{t-1}) = \theta_{t-1},&
%     \var(\Theta_t \,|\, \theta_{t-1}) = \sigma^2 \Psi, \\
%     \e(\Theta_0) = \theta_0,&
%     \var(\Theta_0) = \sigma^2 c^2 \Psi,
% \end{array}
% \label{eqn2:param-random-walk}
% \end{equation}
where $\tau^2$ is a positive scalar and $\Psi$ is a $d_\theta \times d_\theta$ matrix, both pre-specified by the user, and $\theta_0$ is an initial guess of the parameter. Of course, the static parameter is not truly time-varying, hence the ``artificial'' descriptor in its name\footnote{In fact, \citet{Kantas2015} mention that the artificial perturbation may introduce biases into the estimation of parameters that is difficult to quantify in real-world examples.}. In fact, $\sigma^2$ is zero in state augmentation\index{state augmentation}---the idea being that the parameter is static, thus should be a priori constant in time.  When artificial evolution of parameters is used to estimate an \gls{mle} \citep{Ionides2006,Ionides2015}, $\sigma^2$ is a scalar that exponentially decreases over multiple iterations of the filter. If artificial evolution of parameters is used directly, $\sigma^2$ is a scalar chosen by the user. The density is called the \textit{perturbation density}\index{perturbation density} and is denoted by $q(\theta_t \,|\, \theta_{t-1})$. Typically the density $q$ is a multivarite normal distribution, but any distribution that satisfies the above constraints can be used, such as the truncated normal distribution. Alternatively, parameters can be transformed to allow the usage of a multivariate normal distribution.

\begin{comment}
Not only are the parameters time-varying, the parameters are treated in a Bayesian context automatically by the random walk. The parameters will have similar assumptions as the Bayesian treatment of the parameters outlined in the introduction, except that the conditional independence assumptions will depend on the time-varying parameter $\Theta_t$. We will be specific about the assumptions. The parameters from the state and observation models are conditionally independent:
\begin{gather*}
  \Theta_{t,x} \indep \Theta_{t,y} \,|\, \Theta_{t-1}.
\end{gather*}
The parameters follow a Markov process, where the future parameter is conditionally independent of the past parameters given the current parameter:
\begin{gather*}
  \Theta_{t,x} \indep \Theta_{t',x} \,|\, \Theta_{t-1,x} \text{ and }
  \Theta_{t,y} \indep \Theta_{t',y} \,|\, \Theta_{t-1,y} \text{ for all } t' < t-1,
\end{gather*}
The states also follow a Markov process, where the future state of the system is conditionally independent of the past states and parameters given the current state \textit{and} the state transition parameter:
\begin{align*}
  X_t \indep (X_{t'}, \Theta_{t'}, \Theta_{t-1}) \,|\, X_{t-1}, \Theta_{t,x} \text{ for all } t' < t-1.
\end{align*}
Furthermore, the observations are conditionally independent of everything else given the state \textit{and} the observation parameter:
\begin{align*}
  Y_t \independent (X_{t'}, Y_{t'}, \Theta_{t'}) \,|\, X_t, \Theta_{t,y} \text{ for all } t' \neq t.
\end{align*}
% Lastly, the initial state, state disturbances, and measurement errors %, $\{X_0, \eta_t, \eta_{t'}, \epsilon_t, \epsilon_{t'}, t \neq t'\}$,
% are mutually independent \textit{given the parameter}:
% \begin{gather*}
%   X_0 \indep \eta_t \,|\, \Theta, \quad
%   X_0 \indep \epsilon_t \,|\, \Theta, \\
%   \eta_t \indep \eta_{t'} \,|\, \Theta, \quad
%   \epsilon_t \indep \epsilon_{t'} \,|\, \Theta, \\
%   \eta_t \indep \epsilon_t \,|\, \Theta, \quad
%   \eta_t \indep \epsilon_{t'}  \,|\, \Theta\\
%   \text{ for all } t \neq t'.
% \end{gather*}
\Cref{fig2:artificial-evolution} shows two equivalent graphical models of this state-space model with a time-varying parameter.
\end{comment}

\begin{figure}
\centering
\begin{subfigure}[b]{\textwidth}
\centerfloat
\begin{tikzpicture}[node distance = 5em, auto]

% states
\node[circ, filled, label=90:$X_{0}$] (x0) at (0,0) {};
\node[circ, label=85:$X_{1}$, right of=x0] (x1) {};
\node[minimum size=2.5em, right of=x1] (xother) {...};
\node[circ, label=85:$X_{t-1}$, right of=xother] (xtprev) {};
\node[circ, label=85:$X_{t}$, right of=xtprev] (xt) {};
\node[circ, label=85:$X_{t+1}$, right of=xt] (xt1) {};
\node[minimum size=2.5em, right of=xt1] (xother2) {...};
\node[circ, label=85:$X_T$, right of=xother2] (xT) {};

% observations
\node[circ, label=270:$Y_1$, below of=x1] (y1) {};
\node[circ, label=270:$Y_{t-1}$, below of=xtprev] (ytprev) {};
\node[circ, label=270:$Y_t$, below of=xt] (yt) {};
\node[circ, label=270:$Y_{t+1}$, below of=xt1] (yt1) {};
\node[circ, label=270:$Y_T$, below of=xT] (yT) {};

% parameters
\node[circ, filled, label=90:$\theta_{0}$, above of=x0] (theta0) at (0,0) {};
\node[circ, label=90:$\theta_{1}$, right of=theta0] (theta1) {};
\node[minimum size=2.5em, right of=theta1] (thetaother) {...};
\node[circ, label=90:$\theta_{t-1}$, right of=thetaother] (thetatprev) {};
\node[circ, label=90:$\theta_{t}$, right of=thetatprev] (thetat) {};
\node[circ, label=90:$\theta_{t+1}$, right of=thetat] (thetat1) {};
\node[minimum size=2.5em, right of=thetat1] (thetaother2) {...};
\node[circ, label=90:$\theta_T$, right of=thetaother2] (thetaT) {};

% paths between states
\path[line] (x0) to node [above] {} (x1);
\path[line] (x1) to node [above] {} (xother);
\path[line] (xother) to node [above] {} (xtprev);
\path[line] (xtprev) to node [above] {} (xt);
\path[line] (xt) to node [above] {} (xt1);
\path[line] (xt1) to node [above] {} (xother2);
\path[line] (xother2) to node [above] {} (xT);

% paths between states and observations
\path[line] (x1) to node [right] {} (y1);
\path[line] (xtprev) to node [right] {} (ytprev);
\path[line] (xt) to node [right] {} (yt);
\path[line] (xt1) to node [right] {} (yt1);
\path[line] (xT) to node [right] {} (yT);

% paths between parameters
\path[line] (theta0) to node [above] {} (theta1);
\path[line] (theta1) to node [above] {} (thetaother);
\path[line] (thetaother) to node [above] {} (thetatprev);
\path[line] (thetatprev) to node [above] {} (thetat);
\path[line] (thetat) to node [above] {} (thetat1);
\path[line] (thetat1) to node [above] {} (thetaother2);
\path[line] (thetaother2) to node [above] {} (thetaT);

% paths between parameters and states
\path[line] (theta1) to node [right] {} (x1);
\path[line] (thetatprev) to node [right] {} (xtprev);
\path[line] (thetat) to node [right] {} (xt);
\path[line] (thetat1) to node [right] {} (xt1);
\path[line] (thetaT) to node [right] {} (xT);

% paths between parameters and observation
\path[line] (theta1) to [bend right=45] node {} (y1);
\path[line] (thetatprev) to [bend right=45] node {} (ytprev);
\path[line] (thetat) to [bend right=45] node {} (yt);
\path[line] (thetat1) to [bend right=45] node {} (yt1);
\path[line] (thetaT) to [bend right=45] node {} (yT);

\end{tikzpicture}
\caption{\textit{Single node for the parameters.}}
\end{subfigure}

\vspace{.5in}

\begin{subfigure}[t]{\textwidth}
\centerfloat
\begin{tikzpicture}[node distance = 5em, auto]

% states
\node[circ, filled, label=90:$X_{0}$] (x0) at (0,0) {};
\node[circ, label=85:$X_{1}$, right of=x0] (x1) {};
\node[minimum size=2.5em, right of=x1] (xother) {...};
\node[circ, label=85:$X_{t-1}$, right of=xother] (xtprev) {};
\node[circ, label=85:$X_{t}$, right of=xtprev] (xt) {};
\node[circ, label=85:$X_{t+1}$, right of=xt] (xt1) {};
\node[minimum size=2.5em, right of=xt1] (xother2) {...};
\node[circ, label=85:$X_T$, right of=xother2] (xT) {};

% observations
\node[below of=x0] (y0) {};
\node[circ, label=275:$Y_1$, below of=x1] (y1) {};
\node[circ, label=275:$Y_{t-1}$, below of=xtprev] (ytprev) {};
\node[circ, label=275:$Y_t$, below of=xt] (yt) {};
\node[circ, label=275:$Y_{t+1}$, below of=xt1] (yt1) {};
\node[circ, label=275:$Y_T$, below of=xT] (yT) {};

% parameters
\node[circ, filled, label=90:$\theta_{0,x}$, above of=x0] (tx0) {};
\node[circ, label=90:$\theta_{1,x}$, right of=tx0] (tx1) {};
\node[minimum size=2.5em, right of=tx1] (txother) {...};
\node[circ, label=90:$\theta_{t-1,x}$, right of=txother] (txtprev) {};
\node[circ, label=90:$\theta_{t,x}$, right of=txtprev] (txt) {};
\node[circ, label=90:$\theta_{t+1,x}$, right of=txt] (txt1) {};
\node[minimum size=2.5em, right of=txt1] (txother2) {...};
\node[circ, label=90:$\theta_{T,x}$, right of=txother2] (txT) {};

\node[circ, filled, label=270:$\theta_{0,y}$, below of=y0] (ty0) {};
\node[circ, label=270:$\theta_{1,y}$, right of=ty0] (ty1) {};
\node[minimum size=2.5em, right of=ty1] (tyother) {...};
\node[circ, label=270:$\theta_{t-1,y}$, right of=tyother] (tytprev) {};
\node[circ, label=270:$\theta_{t,y}$, right of=tytprev] (tyt) {};
\node[circ, label=270:$\theta_{t+1,y}$, right of=tyt] (tyt1) {};
\node[minimum size=2.5em, right of=tyt1] (tyother2) {...};
\node[circ, label=270:$\theta_{T,y}$, right of=tyother2] (tyT) {};

% paths between states
\path[line] (x0) to node [above] {} (x1);
\path[line] (x1) to node [above] {} (xother);
\path[line] (xother) to node [above] {} (xtprev);
\path[line] (xtprev) to node [above] {} (xt);
\path[line] (xt) to node [above] {} (xt1);
\path[line] (xt1) to node [above] {} (xother2);
\path[line] (xother2) to node [above] {} (xT);

% paths between states and observations
\path[line] (x1) to node [right] {} (y1);
\path[line] (xtprev) to node [right] {} (ytprev);
\path[line] (xt) to node [right] {} (yt);
\path[line] (xt1) to node [right] {} (yt1);
\path[line] (xT) to node [right] {} (yT);

% paths between parameters
\path[line] (tx0) to node [above] {} (tx1);
\path[line] (tx1) to node [above] {} (txother);
\path[line] (txother) to node [above] {} (txtprev);
\path[line] (txtprev) to node [above] {} (txt);
\path[line] (txt) to node [above] {} (txt1);
\path[line] (txt1) to node [above] {} (txother2);
\path[line] (txother2) to node [above] {} (txT);

\path[line] (ty0) to node [above] {} (ty1);
\path[line] (ty1) to node [above] {} (tyother);
\path[line] (tyother) to node [above] {} (tytprev);
\path[line] (tytprev) to node [above] {} (tyt);
\path[line] (tyt) to node [above] {} (tyt1);
\path[line] (tyt1) to node [above] {} (tyother2);
\path[line] (tyother2) to node [above] {} (tyT);

% paths between parameters and states
\path[line] (tx1) to node [right] {} (x1);
\path[line] (txtprev) to node [right] {} (xtprev);
\path[line] (txt) to node [right] {} (xt);
\path[line] (txt1) to node [right] {} (xt1);
\path[line] (txT) to node [right] {} (xT);

% paths between parameters and observation
\path[line] (ty1) to node {} (y1);
\path[line] (tytprev) to node [right] {} (ytprev);
\path[line] (tyt) to node {} (yt);
\path[line] (tyt1) to node {} (yt1);
\path[line] (tyT) to node {} (yT);

\end{tikzpicture}
\caption{\textit{Two separate nodes for the state and observation parameters.}}
\end{subfigure}
\captionnew{Graphical model for a state-space model under artificial evolution of parameters}{}
\label{fig2:artificial-evolution}
\end{figure}

Since the time-varying parameters are also assumed to follow a Markov process like the state, we can obtain an equivalent reparametrization of the state-space model by creating a new state variable $Z_t$ that contains both the state $X_t$ and parameter $\theta_t$:
\begin{align*}
  z_t = \begin{bmatrix} x_t \\ \theta_t \end{bmatrix};
\end{align*}
this is called ``augmenting'' the state.
This leads to the augmented state-space model:
\begin{equation}
\begin{array}{r@{\;}l@{\;}l}% r@{\;}l@{\;}l}
  z_t &=& \begin{bmatrix} m(x_{t-1}, \theta_t) \\ \theta_{t-1} \end{bmatrix} + \begin{bmatrix} \eta_t \\ \zeta_t \end{bmatrix}, \\
    %&\eta_t &\sim& \normal(0, U_t(\theta_t)),\\
  y_t &=& \tilde{H}_t(\theta_t) z_t + \epsilon_t,
    %&\epsilon_t &\sim& \normal(0, V_t(\theta_t)).
\end{array}
\label{eqn2:state-space-timevarying}
\end{equation}
where $\tilde{H}_t(\theta_t) = \begin{bmatrix} H_t(\theta_t) & 0_{d_{\theta}} \end{bmatrix}$, $0_{d_{\theta}}$ is the zero $d_{\theta}$-vector, and $\eta_t$, $\zeta_t$, and $\epsilon_t$ are distributed as before. The assumptions behind the state-space model is the same as the state-space model introduced in the previous part of the dissertation, hence the same graphical model in \Cref{fig1:graphical-model} applies after replacing the nodes with $X_t$ with $Z_t$. For this reason, many developments in the previous part of the dissertation are directly applicable to the augmented state-space model---a nice feature called \textit{plug-and-play}\index{plug-and-play} by \citet{Ionides2006,Ionides2011} that is maintained in our proposed methodology. For completeness, we provide two graphical models for this state space model in \Cref{fig2:artificial-evolution}.

For the augmented state-space model to be complete, we derive the state transition and measurement densities of the model, which are used to obtain the optimal filtering densities and predictive likelihood. By the axiom of conditional probability, the state transition density is
\begin{align*}
  f(z_t \,|\, z_{t-1})
    = p(x_t, \theta_t \,|\, x_{t-1}, \theta_{t-1})
    = p(x_t \,|\, x_{t-1}, \theta_t, \theta_{t-1}) p(\theta_t \,|\, x_{t-1}, \theta_{t-1}).
\end{align*}
By the Markov assumptions on both the state and parameter,
\begin{align*}
  f(z_t \,|\, z_{t-1})
    &= f(x_t \,|\, x_{t-1}, \theta_{t,x}) q(\theta_t \,|\, \theta_{t-1}) \\
    &= \phi[x_t; m_t(x_{t-1}, \theta_{t,x}), U_t(\theta_{t,x})] q(\theta_t \,|\, \theta_{t-1}).
\end{align*}
The measurement density is
\begin{align}
  g(y_t \,|\, z_t)
%    &= p(y_t \,|\, x_t, \theta_t)
    = g(y_t \,|\, x_t, \theta_{t,y})
    = \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})]
\label{eqn2:obs-density}
\end{align}
Equipped with the state transition and measurement densities, the optimal filtering densities are easily derived by plugging them into the optimal filtering densities of \Cref{eqn1:filtering-densities}:
% Before introducing the optimal filtering densities, we introduce a similar shorthand as in the previous part of the dissertation:
% \begin{align*}
%   \pi_{s:t}(a|b) \equiv p(a \,|\, b, y_{s:t}).
% \end{align*}
% Then, the optimal filtering densities are
% \begin{subequations}
% \begin{align}
%   \pi_{1:t-1}(x_t, \theta_t)
%     &= \int \int \phi[x_t; a_t(x_{t-1}, \theta_{t,x}), U_t(\theta_{t,x})] q(\theta_t \,|\, \theta_{t-1}) \pi_{1:t-1}(x_{t-1}, \theta_{t-1}) dx_t d\theta_t, \\
%   \pi_{1:t}(x_t, \theta_t)
%     &= \frac{\phi[y_t; H_t(\theta_{t,y}), V_t(\theta_{t,y})] \pi_{1:t-1}(x_t, \theta_t)}{\pi_{1:t-1}(y_t)}, \label{eqn2:analysis}\\
%   \pi_{1:t-1}(y_t)
%     &= \int \int \phi[y_t; H_t(\theta_{t,y}), V_t(\theta_{t,y})] \pi_{1:t-1}(x_t, \theta_t) dx_t d\theta_t.
% \end{align}
% \label{eqn2:filtering-densities}%
% \end{subequations}
\begin{subequations}
\begin{align}
  p(z_t \,|\, y_{0:t-1})
    &= \int \int \phi[x_t; m_t(x_{t-1}, \theta_{t,x}), U_t(\theta_{t,x})] q(\theta_t \,|\, \theta_{t-1}) p(x_{t-1}, \theta_{t-1} \,|\, y_{0:t-1}) dx_t d\theta_t, \\
  p(z_t \,|\, y_{0:t})
    &= \frac{g(y_t \,|\, x_t, \theta_{t,y}) p(x_t, \theta_t \,|\, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})}, \notag \\
    &= \frac{\phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t, \theta_t \,|\, y_{0:t-1})}{p(y_t \,|\, y_{0:t-1})}, \label{eqn2:analysis}\\
  p(y_t \,|\, y_{0:t-1})
    &= \int \int \phi[y_t; H_t(\theta_{t,y}) x_t, V_t(\theta_{t,y})] p(x_t, \theta_t \,|\, y_{0:t-1}) dx_t d\theta_t. \label{eqn2:pred-lik}
\end{align}
\label{eqn2:filtering-densities}%
\end{subequations}
When convenient, we use a similar shorthand for the optimal filtering densities as in the previous part of the dissertation:
\begin{align*}
  \pi_{s:t}(a) &\equiv p(a \,|\, y_{s:t}), \\
  \pi_{s:t}(a|b) &\equiv p(a \,|\, b, y_{s:t}),
\end{align*}
where $a,b \in \{x_1,...,x_T, y_0, ..., y_T\}$ and $s,t \in \{0,1,..,T\}$.
Therefore, the optimal filtering densities from \Cref{eqn2:filtering-densities} are denoted as
\begin{gather*}
  \pi_{0:t-1}(z_t) = p(z_t \,|\, y_{0:t-1}), \\
  \pi_{0:t}(z_t) = p(z_t \,|\, y_{0:t}), \\
  \pi_{0:t-1}(y_t) = p(y_t \,|\, y_{0:t-1}),
\end{gather*}
respectively.

% Lastly, we derive the joint likelihood. By a recursive application of the axiom of conditional probability, the joint likelihood is:
% \begin{align*}
%   p(y_{1:T} \,|\, \theta_{1:T})
%     = \prod_{t=1}^T p(y_t \,|\, \theta_{1:T}, y_{1:t-1})
%     = \prod_{t=1}^T \pi_{1:t-1}(y_t \,|\, \theta_{1:T}).
% \end{align*}
% By the Markov assumption of the parameters,
% \begin{align*}
%   p(y_{1:T} \,|\, \theta_{1:T})
%     = \prod_{t=1}^T \pi_{1:t-1}(y_t \,|\, \theta_t).
% \end{align*}

Not only are the optimal filtering densities easily derived for the augmented state-space model, parameters are estimated using almost any filtering algorithm that can filter the particular state-space model in \Cref{eqn2:state-space-timevarying}. Of the filtering algorithms presented in the dissertation, the \gls{pf} is the most flexible filtering algorithm and can easily be applied to augmented state-space model. In fact, many parameter estimation algorithms take this approach with the \gls{pf} \citep{Liu2001,Ionides2006,Ionides2011}. At first glance, it seems that the \gls{enkf} is also applicable, but upon closer inspection, $\theta_t$ is not always updated by measurements for a few reasons described in the next section.

%\todo[inline]{Talk about how there is no guarantee of obtaining the MLE under the frequentist treatment of the parameters or sampling from the posterior $\theta \,|\, y_{1:T}$ under the Bayesian treatment. Since its introduction, there have been many parameter estimation methods developed; some of which are based on this algorithm (like the IF), so it's worth discussing the shortcomings of this algorithm with the EnKF. }

