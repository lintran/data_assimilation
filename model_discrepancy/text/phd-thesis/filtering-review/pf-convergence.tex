%!TEX root = main.tex
\synctex=1

\subsection{Almost sure convergence of the particle filter}
\cite{Crisan2002} show almost sure convergence of the approximate distributions under the weak topology. The proof is done assuming that the observations are fixed, and we maintain that assumption throughout the paper. Before stating the theorem, we briefly describe the topology and define a condition of the theorem called the Feller property.

Let $\mathcal{P}(\mathbb{R}^d)$ be the space of probability measures over the $d$-dimensional Euclidean space $\mathbb{R}^d$, $C_b(\mathbb{R}^d)$ be the set of continuous, bounded functions on $\mathbb{R}^{d}$, and $L_b(\mathbb{R}^d)$ be the set of Lipschitz, bounded functions on $\mathbb{R}^{d}$. Define $\norm{\cdot}_\infty$ to be the supremum norm, i.e., $\norm{\varphi}_\infty \equiv \sup_{x \in \mathbb{R}^d} |\varphi(x)|$, and $\norm{\cdot}_{lip}$ to be the Lipshitz constant: $\varphi$ is Lipshitz if there exists a constant $L > 0$ such that $|\varphi(x) - \varphi(y)| \leq L |x-y|$ for every $x$ and $y$; $\norm{\varphi}_{lip}$ is the least such $L$.

In the topology outlined by the authors, a sequence of probability measures $P_n$ is said to converge weakly to $P \in \mathcal{P}(\mathbb{R}^d)$, if for any $\varphi \in C_b(\mathbb{R}^d)$,
\begin{align*}
    \lim_{n\to\infty} P_n\varphi = P\varphi \quad \text{weakly},
\end{align*}
where $P\varphi = \e_P(\varphi)$ and similarly for $P_n$ (we will use $\e_P(\cdot)$ when there is confusion); we write $P_n \to P$ when this happens. The topology is metrizable with the following distance function
\begin{align}
    d(P, Q) = \sum_{j=1}^\infty \frac{|P\varphi_j - Q\varphi_j|}{2^j \norm{\varphi_j}_\infty}. \label{eqn:distance-metric}
\end{align}
With this distance function, we have
\begin{align*}
    \lim_{n \to \infty} d(P_n, P) = 0 \qquad \Longleftrightarrow \qquad \lim_{n\to\infty} P_n\varphi = P\varphi \quad \text{weakly}
\end{align*}

Note that proving convergence on the space of test functions $L_b(\mathbb{R}^{d_x})$ is equivalent to proving convergence on the space of test functions $C_b(\mathbb{R}^d)$ because of the portmanteau theorem \cite[Lemma 18.9, pg 259]{VanderVaart1998}. Therefore, the topologies are equivalent and have the same distance metric as in \cref{eqn:distance-metric}.

The Feller property is defined as follows:
\begin{defn}
\label{def:feller}
$P$ is Feller if and only if for all $\varphi \in C_b(\mathbb{R}^d)$ implies that $P\varphi \in C_b(\mathbb{R}^d)$.
\end{defn}

Now, we state the almost sure convergence of the empirical distributions from particle filter:
\begin{theorem}
\label{thm:pf-almostsure}
Suppose that the transition density $f$ is Feller and the likelihood function $g$ is bounded, continuous, and strictly positive. Then,
\begin{gather*}
  \lim_{n\to\infty} \pi_{t|1:t-1}^n = \pi_{t|1:t-1}, \\
  \lim_{n\to\infty} \tilde{\pi}_{t|1:t}^n = \pi_{t|1:t}, \\
  \lim_{n\to\infty} \pi_{t|1:t}^n = \pi_{t|1:t},
\end{gather*}
almost surely.
\end{theorem}

Since the proof of almost sure convergence for the ensemble Kalman filter is inspired by this theorem, we briefly discuss the proof here; interested readers should refer to \citet[Section IV]{Crisan2002} for details. Induction is used to prove \cref{thm:pf-almostsure}. Two key elements are needed to make the proof possible: (1) continuity and (2) Lemma 2 of \cite{Crisan2002} (repeated in this paper as \cref{lem:dist-delta-true}).

Continuity ensures that particles that start off ``close'' remain ``close'' after forecast and update steps. The continuity of the forecast step is ensured by the Feller property of $f$ and the continuity of the analysis step is ensured by the continuity of the following function
\begin{align*}
    u_t(P)(\cdot) \equiv \frac{\e_{P}[g(y_t \,|\, \cdot) \varphi(\cdot)]}{\e_{P}[g(y_t \,|\, \cdot)]} \quad \text{ for any } \quad \varphi \in C_b(R^d),
\end{align*}
which is continuous because $g$ is also continuous. Boundedness also plays an important role but to a lesser extent---it ensures that the quantities which convergence is proven are still bounded, ensuring that the quantities are still in the set of test functions after applying $f$ or $u_t$. Note that $g$ is required to be strictly positive so that the denominator in $u$ is nonzero.

In the optimal filter outlined in this paper, the likelihood $g(\cdot \,|\, x_t)$ is the normal density centered at $h_t(x_t)$. The normal density is bounded, continuous, and strictly positive, so it is required that $h_t$ is also continuous to ensure that $g$ satisfies the conditions of the theorem.

Lemma 2 of \cite{Crisan2002} is repeated here:
\begin{lemma}
\label{lem:dist-delta-true}
Suppose $\mu_n \to \mu$ and $V_j$ be i.i.d. random variables with common distribution P. Define
\begin{align*}
  \mathbb{P}_n(\cdot) = \frac{1}{n} \sum_{j=1}^n \delta_{V_j}(\cdot).
\end{align*}
Then,
\begin{align*}
    \mathbb{P}_n (\mu_n) \to \mu
\end{align*}
almost surely.
\end{lemma}

\begin{proof}[Proof sketch]
It is shown that
\begin{align}
    \e[ (\mathbb{P}_n(\mu_n) \varphi_i - \mu_n \varphi_i)^4 ]
        \leq \frac{C}{n^2} \label{eqn:as-condition}
\end{align}
for some finite $C$. Therefore,
\begin{align*}
    \e \left[ \sum_{n=1}^\infty (\mathbb{P}_n(\mu_n) \varphi_i - \mu_n \varphi_i)^4 \right]
        &= \sum_{n=1}^\infty  \e \left[ (\mathbb{P}_n(\mu_n) \varphi_i - \mu_n \varphi_i)^4 \right]
        < \infty
\end{align*}
By Borel-Cantelli lemma,
\begin{align*}
    \sum_{n=1}^\infty (\mathbb{P}_n(\mu_n) \varphi_i - \mu_n \varphi_i)^4 < \infty, \quad \text{almost surely}
\end{align*}
implying that $|\mathbb{P}_n(\mu_n) \varphi_i - \mu_n \varphi_i| \to 0$ almost surely for all $i$. Therefore, $d(\mathbb{P}_n(\mu_n), \mu_n) \to 0$ almost surely and $d(\mathbb{P}_n(\mu_n), \mu) \to 0$ almost surely.
\end{proof}

Define the following sigma algbras:
\begin{align*}
  \mathcal{F}_t^a &= \sigma(X_s^{f(i)}, X_s^{a(i)}, s \leq t, i = 1,...,n) \\
  \mathcal{F}_t^f &= \sigma(X_s^{f(i)}, X_s^{a(i)}, s < t, X_t^{f(i)},  i = 1,...,n).
\end{align*}
The expectation in the proof of \cref{lem:dist-delta-true} corresponds to
\begin{inparaenum}[(1)]
  \item $\e[\cdot | \mathcal{F}_{t-1}^a]$ when proving $\pi_{t|t-1}^n \to \pi_{t|t-1}$ and
  \item $\e[\cdot | \mathcal{F}_t^f]$ when proving $\pi_{t|t}^n \to \pi_{t|t}$.
\end{inparaenum}

The lemma ensures that the empirical distribution constructed with the forecast particles converges to the optimal forecast distribution and that the resampling step does not change the distribution of the particles. Since most particle filters differ only by the resampling scheme, the authors note that \cref{lem:dist-delta-true} holds as long as the condition in \cref{eqn:as-condition} is satisfied for any new resampling scheme.
