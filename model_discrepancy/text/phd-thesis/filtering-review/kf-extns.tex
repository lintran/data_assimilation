%!TEX root = ../thesis.tex
\synctex=1

In this section, we discuss two extensions of the Kalman filter that loosen the requirement of linearity. Consider the following state-space model:
\begin{equation*}
\begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l}
  x_t &=& m_t(x_{t-1}) + \eta_t,
    &\eta_t &\sim& \normal(0, U_t), \\
  y_t &=& h_t(x_t) + \epsilon_t,
    &\epsilon_t &\sim& \normal(0, V_t),
\end{array}
% \label{eqn1:kf-extn}
\end{equation*}
where $m_t: \mathbb{R}^{d_x} \to \mathbb{R}^{d_x}$ and $h_t: \mathbb{R}^{d_x} \to \mathbb{R}^{d_y}$ are nonlinear mappings of the state. The state transition and observation densities are then:
\begin{align*}
  f(x_t \,|\, x_{t-1}) &= \phi(x_t; m_t(x_{t-1}), U_t), \\
  g(y_t \,|\, x_{t-1}) &= \phi(y_t; h_t(x_t), V_t).
\end{align*}
The initial state is also distributed normally with mean $\mu_0^a$ and variance $\Sigma_0^a$, i.e., $X_0 \sim \normal(\mu_0^a, \Sigma_0^a)$. Notice that the state-space model is the same as the model under the Kalman filter, except that the first moments of both the state transition and measurement models are nonlinear with respect to the state because of the mappings $m_t$ and $h_t$, respectively.

Even if both the analysis random variable $X_{t-1}^a \overset{d}{=} X_{t-1} \,|\, y_{0:t-1}$ and the forecast random variable $X_t^f \overset{d}{=} X_t \,|\, y_{0:t-1}$ are normally distributed, $m_t(X_{t-1}^a)$ and $h_t(X_t^f)$ need not be normally distributed and therefore the distributions of $X_t \,|\, y_{0:t-1}$ and $X_t \,|\, y_{0:t}$, respectively, generally do not have analytic forms. The idea behind the extensions in this section is to approximate $m_t(X_{t-1}^a)$ and $h_t(X_t^f)$ with normal random variables, thus allowing the distributions of $X_t \,|\, y_{0:t-1}$ and $X_t \,|\, y_{0:t}$ (and hence the optimal filtering densities) to be approximated with normal distributions as well. Since normally distributed random variables are defined by their first two moments, these algorithms outline how to approximate their first two moments.

\subsection[Extended Kalman filter]{\Acrlong{ekf}}
\input{filtering-review/ekf}

\subsection[Ensemble Kalman filter]{\Acrfull{enkf}} \label{sec:enkf}
\input{filtering-review/enkf}

% \subsection{Limitations}
% The benefit of both the extended and ensemble Kalman filters is that they allow for easy usage of the Kalman filter results and approximations with normal random variables are sometimes reasonable. However, even if the approximation error at one time step is small, the errors will accumulate and can eventually lead to filter divergence. Methods have been developed to overcome filter divergence, e.g., using higher-order \acrlong{ekf} or the \gls{enkf} as an alternative to first-order \acrlong{ekf} or performing variance inflation and localization with the \gls{enkf}. However, even with these modifications, there is no guarantee that the approximations will converge to the optimal filtering densities, since small errors can accumulate \citep[see Section IV-A][for an example]{Crisan2002}. Furthermore, these filters can only solve for the filtering problem outlined in \Cref{eqn1:kf-extn}, which can be quite limited if, for example, the measurements are categorical.
