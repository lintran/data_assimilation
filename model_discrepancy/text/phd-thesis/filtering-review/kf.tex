%!TEX root = ../thesis.tex
\synctex=1

The state-space model under the assumptions of the \acrlong{kf} is one of the rare models with analytic solutions to the optimal filtering densities (see \citett{Kalman1960,Bucy1961} for the original papers and Section 13.2 of \citet{Bishop2006} for a textbook review). We briefly derive the forecast and analysis distributions because they will be important later in the discussion of extensions of the Kalman filter.

Consider the following state-space model:
\begin{equation*}
\begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l}
  x_t &=& M_t x_{t-1} + \eta_t,
    &\eta_t &\sim& \normal(0, U_t), \\
  y_t &=& H_t x_t + \epsilon_t,
    &\epsilon_t &\sim& \normal(0, V_t),
\end{array}
\end{equation*}
where $M_t \in \mathbb{R}^{d_x \times d_x}$ is a $d_x \times d_x$ matrix, $H_t \in \mathbb{R}^{d_y \times d_x}$ is a $d_y \times d_x$ matrix. The transition and observation densities are thus
\begin{gather*}
  f(x_t \,|\, x_{t-1}) = \phi(x_t; M_t x_{t-1}, U_t), \\
  g(y_t \,|\, x_t) = \phi(y_t; H_t x_t, V_t),
\end{gather*}
respectively. The initial state is also normally distributed with mean $\mu_0^a$ and variance $\Sigma_0^a$, i.e., $X_0 \sim \normal(\mu_0^a, \Sigma_0^a)$.

Suppose the analysis random variable at time $t-1$ is normally distributed with mean $\mu_{t-1}^a$ and variance $\Sigma_{t-1}^a$: $X_{t-1} \,|\, y_{0:t-1} \sim \normal(\mu_{t-1}^a, \Sigma_{t-1}^a)$. Since the convolution of two Gaussian random variables is also a Gaussian random variable, the forecast random variable at time $t$ is normally distributed with mean and variance equal to
\begin{equation*}
\begin{aligned}
  \mu_t^f &= M_t \mu_t^a, \\
  \Sigma_t^f &= U_t + M_t \Sigma_{t-1}^a M_t^T,
\end{aligned}
%\label{eqn1:kf-forecast}
\end{equation*}
respectively. Therefore, the forecast distribution at time $t$ is
\begin{align}
  \pi_{0:t-1}(x_t) = \phi(x_t; \mu_t^f, \Sigma_t^f).
\label{eqn1:kf-forecast}
\end{align}

Because normal random variables are defined by their first two moments, the following joint distribution is derived using the laws of total expectation, variance, and covariance:
\begin{align}
  \begin{bmatrix} X_t \\ Y_t \end{bmatrix} \,\bigg|\, y_{0:t-1}
    \sim \normal \left(
    \begin{bmatrix} \mu_t^f \\ H_t\mu_t^f \end{bmatrix},
    \begin{bmatrix}
      \Sigma_t^f & \Sigma_t^f H_t^T \\
      H_t \Sigma_t^f & V_t + H_t \Sigma_t^f H_t^T
    \end{bmatrix}
    \right).
\label{eqn1:kf-joint}
\end{align}
After observing $y_t$, the predictive likelihood is
\begin{align}
  \pi_{0:t}(y_t) = \phi(y_t; H_t\mu_t^f, V_t + H_t \Sigma_t^f H_t^T),
\label{eqn1:kf-condlik}
\end{align}
obtained by marginalizing $X_t, Y_t \,|\, y_{0:t-1}$ over $X_t$. Plugging in \Cref{eqn1:kf-forecast,eqn1:kf-condlik} into the optimal analysis distribution of \Cref{eqn1:analysis}, we have
\begin{align}
  \pi_{0:t}(x_t)
    &= \frac{\phi(y_t; H_tx_t, V_t)\phi(x_t; \mu_t^f, \Sigma_t^f)}{\phi(y_t; H_t\mu_t^f, V_t + H_t \Sigma_t^f H_t^T)}.
\label{eqn1:kf-analysis2}
\end{align}
The first two moments of the analysis random variable $X_t \,|\, y_{0:t}$ (and thus the analysis distribution) are calculated using the conditioning techniques of multivariate normal random variables, derived using Schur complements:
\begin{equation*}
\begin{aligned}
  \mu_t^a
    &\equiv \e(X_t \,|\, y_{0:t}) \\
    &= \e(X_t \,|\, y_{0:t-1}) + \cov(X_t, Y_t \,|\, y_{0:t-1}) \var(Y_t \,|\, y_{0:t-1})^{-1} [y_t - \e(Y_t \,|\, y_{0:t-1})], \\
  \Sigma_t^a
    &\equiv \var(X_t \,|\, y_{0:t}) \\
    &= \var(X_t \,|\, y_{0:t-1}) - \cov(X_t, Y_t \,|\, y_{0:t-1}) \var(Y_t \,|\, y_{0:t-1})^{-1} \cov(Y_t, X_t \,|\, y_{0:t-1})), \\
\end{aligned}
\end{equation*}
thus $X_t \,|\, y_{1:t} \sim \normal(\mu_t^a, \Sigma_t^a)$ and
\begin{align}
  \pi_{1:t}(x_t)
    = \phi(x_t; \mu_t^a, \Sigma_t^a)
\label{eqn1:kf-analysis}
\end{align}
with mean and variance equal to
\begin{equation*}
\begin{aligned}
  \mu_t^a
      &= \mu_t^f + \Sigma_t^f H_t^T ( V_t + H_t \Sigma_t^f H_t^T )^{-1}(y_t - H_t \mu_t^f), \\
  \Sigma_t^a
      &= \Sigma_t^f - \underbrace{\Sigma_t^f H_t^T ( V_t + H_t \Sigma_t^f H_t^T )^{-1}}_{\text{Kalman gain}} H_t \Sigma_t^f,
\end{aligned}
% \label{eqn1:kf-analysis-moments}
\end{equation*}
respectively. The term above the underbrace is called the \textit{Kalman gain}\index{Kalman gain} and is denoted as
\begin{align}
  K_t = \cov(X_t, Y_t \,|\, y_{0:t-1}) \var(Y_t \,|\, y_{0:t-1})^{-1}
      = \Sigma_t^f H_t^T ( V_t + H_t \Sigma_t^f H_t^T )^{-1}.
\label{eqn1:kf-kg}
\end{align}
The Kalman gain is a crucial component to update the state in the Kalman filter and its extensions. By induction, the filtering densities in \Cref{eqn1:kf-forecast,eqn1:kf-analysis,eqn1:kf-condlik} are the optimal filtering densities for all $t$.

