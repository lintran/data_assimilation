%!TEX root = ../thesis.tex
\synctex=1

The \acrlong{ekf} circumvents the nonlinearity issue by linearizing the mappings around the current mean and variance estimates. Specifically, if $m_t$ and $h_t$ are sufficiently differentiable, their Taylor expansions are used to approximate the first two moments of their distributions. In this section, we outline the first-order \acrfull{ekf}, which uses only the first-order approximation of the Taylor expansion in the estimation of the means and variances. Interested readers can read more about extended Kalman filters in \citet{Einicke2012a}.

%Suppose $X_{t-1} \,|\, y_{1:t-1}$ has mean $\mu_{t-1}^a$ and variance $\Sigma_{t-1}^a$. This assumption will not be true in general and approximations to the moments will be used in practice, but we will consider it to derive the optimal filtering densities for time $t$ and discuss the assumption afterwards.
Suppose the mean and variance of $X_{t-1}^a$ are approximated by $\tilde{\mu}_{t-1}^a$ and $\tilde{\Sigma}_{t-1}^a$, respectively. Further suppose $m_t$ is at least twice differentiable. Then, by the multivariate Taylor's theorem,
\begin{align*}
  m_t(x) = m_t(\mu) + D^{(1)} m_t(\mu) (x-\mu) + O[(x-\mu)^T D^{(2)} m_t(\mu) (x-\mu)],
\end{align*}
where $D^{(1)} m_t(\mu)$ and $D^{(2)} m_t(\mu)$ are the Jacobian and Hessian of $m_t$ evaluated at $\mu$, respectively. Let $\tilde{M}_t \equiv D^{(1)} m_t(\tilde{\mu}_{t-1}^a)$. Then, the mean and variance of $X_t^f$ are approximated as
\begin{equation*}
\begin{array}{r@{\;}l@{\;}l@{\;}r@{\;}l}
  \mu_t^f
    &\approx& m_t(\tilde{\mu}_{t-1}^a)
    &\equiv& \tilde{\mu}_t^f,\\
  \Sigma_t^f
    &\approx& U_t + \tilde{M}_t \tilde{\Sigma}_t^a \tilde{M}_t^T
    &\equiv& \tilde{\Sigma}_t^f,
\end{array}
\end{equation*}
respectively. Notice that the truncation error of $\tilde{\mu}_t^f$ and $\tilde{\Sigma}_t^f$ are on the order of the variance and fourth central moment of $X_{t-1}^a$, respectively, scaled by the Hessian evaluated at $\mu_{t-1}^a$. The forecast distribution is then approximated as
\begin{gather}
  \pi_{0:t-1}(x_t) \approx \phi(x_t; \tilde{\mu}_t^f, \tilde{\Sigma}_t^f).
\label{eqn1:ekf-forecast}
\end{gather}

Similarly, suppose $h_t$ is at least twice differentiable. Let $\tilde{H}_t$ be the Jacobian of $h_t$ evaluated at $\tilde{\mu}_t^f$. After observing $y_t$, the $\mu_t^f, \Sigma_t^f$, and $H_t$ terms that appear in the calculation of the Kalman filter's predictive likelihood and analysis distribution (\Cref{eqn1:kf-condlik,eqn1:kf-analysis}, respectively) are replaced by the approximations calculated here. Specifically, the predictive likelihood is approximated as
\begin{align}
  \pi_{0:t-1}(y_t) \approx \phi(y_t; \tilde{H}_t^T \tilde{\mu}_t^f, V_t + \tilde{H}_t \tilde{\Sigma}_t^f \tilde{H}_t^T),
\label{eqn1:ekf-condlik}
\end{align}
and the analysis mean and variance are approximated as:
\begin{equation*}
\begin{array}{r@{\;}l@{\;}l@{\;}r@{\;}l}
  \mu_t^a
    &\approx& \tilde{\mu}_t^f + \tilde{\Sigma}_t^f \tilde{H}_t^T ( V_t + \tilde{H}_t \tilde{\Sigma}_t^f \tilde{H}_t^T )^{-1}[y_t - h_t(\tilde{\mu}_t^f)]
    &\equiv& \tilde{\mu}_t^a,\\
  \Sigma_t^a
    &\approx& \tilde{\Sigma}_t^f - \tilde{\Sigma}_t^f \tilde{H}_t^T ( V_t + \tilde{H}_t \tilde{\Sigma}_t^f \tilde{H}_t^T )^{-1} \tilde{H}_t \tilde{\Sigma}_t^f
    &\equiv& \tilde{\Sigma}_t^a,
\end{array}
\end{equation*}
thus providing an approximation to the analysis distribution:
\begin{gather}
  \pi_{0:t}(x_t) \approx \phi(x_t; \tilde{\mu}_t^a, \tilde{\Sigma}_t^a).
\label{eqn1:ekf-analysis}
\end{gather}

Notice that three approximations are made to derive the filtering densities for each time $t$. Two approximations come from the Taylor expansions of $m_t$ and $h_t$ used to derive the first two moments of the forecast and analysis distributions, respectively. The last approximation comes from the estimator of the analysis distribution at time $t-1$ used to derive the filtering densities at time $t$. This last approximation contains all the errors accumulated from the Taylor expansions up to time $t-1$ and are then propagated into estimations of future forecast and analysis distributions. The accumulation of errors is a common theme in all filtering algorithms with the exception of the Kalman filter.

With the \gls{ekf}, the accumulation of errors can lead to unbounded error growth \citep{Evensen1994,Evensen2003}. This error is part of a filtering problem called \textit{filter divergence}\index{filter divergence}, where the filter is no longer able to capture the true dynamics of the system \citep{Evensen1992,Miller1994,Harlim2010,Gottwald2013}. One solution is to use a higher-ordered \gls{ekf}, which uses higher order terms in the Taylor expansion of $m_t$ and $h_t$ and thus provides better approximations to the means and covariances. An alternative (but partial) solution is to replace the mean and variance of the forecast with their sample estimates; this is the idea behind the \gls{enkf} in the next section. It is only a partial solution because the measurement model is required to be linear in the state, like the Kalman filter: $h_t(x_t) = H_tx_t$. Although solutions have been developed to overcome this requirement, we will not discuss them in this dissertation. For more information, interested readers can refer to \citet[Section A1]{Evensen2009}, \citet{Livings2008}, \citet{Luo2014}, and references therein.




