%!TEX root = ../thesis.tex
\synctex=1

\section{Optimal filtering} \label{sec:optimal-filter}
Filtering is an iterative procedure to forecast and then update the forecast with collected measurements. Though their distributions do not always have an analytic solution, we describe how the optimal filtering densities are derived in this section. Interested readers can refer to \citett{Doucet2001,Crisan2002} for a similar treatment.

\begin{figure}
\centering
\begin{tikzpicture}[node distance = 5em, auto]

% states
\node[circ, filled, label=90:$X_{0}$] (x0) at (0,0) {};
\node[circ, label=90:$X_{1}$, right of=x0] (x1) {};
\node[minimum size=2.5em, right of=x1] (xother) {...};
\node[circ, label=90:$X_{t-1}$, right of=xother] (xtm1) {};
\node[circ, label=90:$X_{t}$, right of=xtm1] (xt) {};
\node[circ, label=90:$X_{t+1}$, right of=xt] (xtp1) {};
\node[minimum size=2.5em, right of=xtp1] (xother2) {...};

% observations
\node[circ, filled, label=270:$Y_1$, below of=x1] (y1) {};
\node[circ, filled, label=270:$Y_{t-1}$, below of=xtm1] (ytm1) {};
\node[circ, filled, label=270:$Y_t$, below of=xt] (yt) {};
\node[circ, label=270:$Y_{t+1}$, below of=xtp1] (ytp1) {};

% paths between states
\path[line] (x0) to node [above] {} (x1);
\path[line] (x1) to node [above] {} (xother);
\path[line] (xother) to node [above] {} (xtm1);
\path[line] (xtm1) to node [above] {} (xt);
\path[line] (xt) to node [above] {} (xtp1);
\path[line] (xtp1) to node [above] {} (xother2);

% paths between states and observations
\path[line] (x1) to node [right] {} (y1);
\path[line] (xtm1) to node [right] {} (ytm1);
\path[line] (xt) to node [right] {} (yt);
\path[line] (xtp1) to node [right] {} (ytp1);

\end{tikzpicture}
\captionnew{Graphical model of a state space model with measurements collected up to time $t$.}{Observed values are represented by filled circles.}
\label{fig1:graphical-model-obst}
\end{figure}

%The distribution of the forecast state is defined as the posterior distribution of the state given all the observations collected up to previous timepoint. Let's be specific.
Since the main goal of filtering is to forecast, we first discuss the optimal forecast distribution. Let $y_{1:t}$ be the set of measurements collected up to time $t$, where $y_{m:n} \equiv (y_m, ..., y_n)$.  \Cref{fig1:graphical-model-obst} is similar to the graphical model from \Cref{fig1:graphical-model} but with nodes filled in to represent that the measurements $y_{1:t}$ are observed. By the definition of conditional probability, the forecast\index{forecast} distribution is:
\begin{align*}
  p(x_{t+1} \,|\, y_{1:t}, x_0)
    = \int p(x_{t+1} \,|\, x_{t}, y_{1:t}, x_0) p(x_t \,|\, y_{1:t}, x_0) dx_t.
\end{align*}
By the Markov assumption, the state $X_{t+1}$ is conditionally independent of the past states and their measurements ($X_{t'}$ and $Y_{t'}$ for $t' < t$) given its previous state $X_t$, thus $p(x_{t+1} \,|\, x_{t}, y_{1:t}, x_0) = f(x_{t+1} \,|\, x_{t})$ and
\stepcounter{equation}
\begin{align}
  p(x_{t+1} \,|\, y_{1:t}, x_0)
    &= \int f(x_{t+1} \,|\, x_{t}) p(x_t \,|\, y_{1:t}, x_0) dx_t.
\tag{{\theequation}a}
\label{eqn1:forecast}
\end{align}
\addtocounter{equation}{-1}%
The forecast random variable at time $t+1$ is denoted as $X_{t+1}^f \overset{d}{=} X_{t+1} \,|\, y_{1:t}, x_0$, where ``$\overset{d}{=}$'' denotes equal in distribution. The first two moments of $X_{t+1}^f$ are denoted as $\mu_{t+1}^f$ and $\Sigma_{t+1}^f$, where the superscript ``$f$'' is to remind the reader that they are moments of the $f$orecast distribution.

Notice that the forecast distribution requires the posterior distribution of the previous state, $p(x_t \,|\, y_{1:t}, x_0)$: called the \textit{analysis}\index{analysis} distribution. An application of Bayes' theorem provides the analysis distribution:
\begin{subequations}
\begin{align}
  p(x_t \,|\, y_{1:t}, x_0)
    &= \frac{g(y_t \,|\, x_t) p(x_t \,|\, y_{1:t-1}, x_0)}{p(y_t \,|\, y_{1:t-1}, x_0)}.
\tag{{\theequation}b}
\label{eqn1:analysis}
\end{align}
\label{eqn1:filtering-densities}%
\end{subequations}
The analysis random variable at time $t$ is denoted as $X_t^a \overset{d}{=} X_t \,|\, y_{1:t}, x_0$ and its mean and variance are $\mu_t^a$ and $\Sigma_t^a$, respectively. Again, the superscript ``$a$'' is to remind the reader that they are moments of the $a$nalysis distribution. The procedure to find the analysis distribution at time $t$ is called \textit{updating}\index{updating} and is performed only after observing $y_t$. The analysis distribution is a function of the measurement density, the forecast distribution of the previous time, and the \textit{predictive likelihood}\index{predictive likelihood}:
\begin{equation*}
  p(y_t \,|\, y_{1:t-1}, x_0)
    = \int p(y_t \,|\, x_t, y_{1:t-1}, x_0) p(x_t \,|\, y_{1:t-1}, x_0) dx_t.
\end{equation*}
Since the measurement is conditionally independent of the previous measurements given its state, $p(y_t \,|\, x_t, y_{1:t-1}, x_0) = g(y_t \,|\, x_t)$ and thus the predictive likelihood is
\begin{equation}
  p(y_t \,|\, y_{1:t-1}, x_0)
    = \int g(y_t \,|\, x_t) p(x_t \,|\, y_{1:t-1}, x_0) dx_t.
%\tag{{\theequation}c}
\label{eqn1:cond-lik}
\end{equation}
Many of the filtering algorithms need not calculate or sample from the predictive likelihood, but it is an important component to derive or estimate the likelihood, a crucial component for parameter estimation.

The set of equations in \Cref{eqn1:filtering-densities} is called the \textit{optimal filtering densities}\index{optimal filtering densities}. There are rarely analytic solutions for the densities. The rest of the chapter is dedicated to reviewing algorithms that derive or approximate the filtering densities in \Cref{eqn1:filtering-densities}. In preparation for \Cref{part:param-est} on parameter estimation, we also derive or approximate the predictive likelihood of \Cref{eqn1:cond-lik} when discussing the algorithms. \todo{Deb: Optimality is defined by a loss function. Why are these optimal?}

\Cref{sec:kf} discusses the Kalman filter, an algorithm to analytically solve the filtering densities when both the state transition and measurement models are linear and have additive and Gaussian errors. \Cref{sec:kf-extns} discusses two extensions of the Kalman filter that loosen the requirement of linearity required in the Kalman filter: the \gls{ekf} and the \acrfull{enkf}. These extensions still require that the state disturbances and measurement errors be additive and Gaussian and thus cannot be used to solve for general state-space models. On the other hand, \acrfullpl{pf} are a class of \acrfull{smc} algorithms to approximately sample from the optimal filtering densities for the general state space model of \Cref{eqn1:general-filter}. In \Cref{sec:bf}, we discuss the simplest \gls{pf} algorithm: the \acrfull{bf}. We end the chapter with a short discussion comparing the \gls{enkf} and \glspl{pf}.

We first end this section by defining some notational conveniences and a note about the derivation of the algorithms.

\subsection{Notation}
The filtering densities up to time $t$ are all conditionally dependent on the initial condition $x_0$ and the measurements that have been collected, e.g., either $y_{1:t-1}$ or $y_{1:t}$. For notational succinctness, let $Y_0 = X_0$. For example, the succinct notation $y_{0:t} = \{x_0, y_{1:t}\}$ and the filtering densities $p(x_{t} \,|\, y_{1:t-1}, x_0)$, $p(x_{t} \,|\, y_{1:t}, x_0)$, and $p(y_t \,|\, y_{1:t-1}, x_0)$ are denoted as $p(x_{t} \,|\, y_{0:t-1})$, $p(x_{t} \,|\, y_{0:t})$, and $p(y_t \,|\, y_{0:t-1})$, respectively.

%After introducing the ensemble Kalman filter and the bootstrap filter, we will demonstrate the filtering algorithms on the case studies outlined in the introduction. We will conclude this chapter by re-interpreting the ensemble Kalman filter---a re-interpretation that will elucidate its connection to the bootstrap filter. This connection will be important later on in the next chapter, when we introduce a new filtering algorithm that attempts to combine the best qualities of both the ensemble Kalman filter and the bootstrap filter.

To distinguish the optimal filtering densities from their estimators in the following sections, we use the following shorthand when convenient:
\begin{align*}
  \pi_{s:t}(z) \equiv p(z \,|\, y_{s:t}),
\end{align*}
where $z \in \{x_1, x_2, ..., y_1, y_2, ...\}$ and $s,t \in \{1,2,...\}$. For example, the forecast density, analysis density, and predictive likelihood from \Cref{eqn1:filtering-densities} are denoted as
\begin{gather*}
  \pi_{0:t}(x_{t+1}) = p(x_{t+1} \,|\, y_{0:t}), \\%\tag{forecast}\\
  \pi_{0:t}(x_t) = p(x_t \,|\, y_{0:t}), \\%\tag{analysis}\\
  \pi_{0:t-1}(y_t) = p(y_t \,|\, y_{0:t-1}), %\tag{predictive likelihood}
\end{gather*}
respectively. The distribution of the initial state\index{initial state} $X_0$ is denoted as $\pi_{0}$. For notational succinctness, the initial state is considered an $a$nalysis random variable when no measurements $y_t$ have been collected, thus define $\pi_{1:0}(x_0) \equiv p(x_0) = \pi_{0}(x_0)$ and $\pi_{1:0}(x_1) \equiv p(x_1 \,|\, x_0)$. Let the first two moments of the initial condition be denoted as $\mu_0^a = \e[X_0]$ and $\Sigma_0^a = \var[X_0]$.

When discussing estimators constructed with Monte Carlo samples, such as the \gls{enkf} and the \gls{pf}, the estimators are denoted with a superscript $M$ (e.g., $\pi_{s:t}^M$) to indicate the sample size $M$ to construct the estimator. The sampling algorithms are initialized with simple random samples $x_0^{a(m)} \sim \pi_0$ for $m = 1,...,M$. The superscript ``$(m)$'' denotes the sample number, e.g., $x_t^{f(m)}$ and $x_t^{a(m)}$ denote the $m$th sample from the forecast and analysis distributions at time $t$, respectively. Let $\{x^{(m)}\}_{m=1}^M$ be a shorthand for a sample of size $M$, i.e., $\{x^{(1)}, ..., x^{(M)}\}$. Furthermore, simple random samples cannot always be generated, thus each sample may have a nonuniform weight associated with it. A sample $x^{(m)}$ with weight $w^{(m)}$ may be denoted as the sample $(x^{(m)}, w^{(m)})$. If the sample does not include a weight, it is assumed to be a simple random sample is generated with uniform weights $w^{(m)} = 1/M$ for $m = 1,...,M$.

%\todo{Deb: Would it help to use boldface for vectors? How about saying more often what the dimenmsion is, e.g., $\Sigma_0^a$ is $d\times d$ initial covariance matrix?}

\subsection{A note about the derivations of estimators}

Due to the inductive nature of the filtering densities, in each following section, we derive estimators of the filtering densities by beginning with an estimator of the analysis distribution at time $t-1$ and plugging that particular estimator into the filtering densities at time $t$ to derive their estimators. This is true for all filtering algorithms presented with one exception: the Kalman filter. The state-space model under the assumptions of the Kalman filter has analytical solutions for the filtering densities. %We will be clear when estimators of the filtering densities are being derived, but an analytic solution only exists for the state space model under the assumptions of the Kalman filter.
