%!TEX root = ../thesis.tex
\synctex=1

\citet{Evensen1994} developed the \gls{enkf} in response to the filter divergence issues of the \gls{ekf}. The \gls{enkf} uses Monte Carlo samples instead of a Taylor approximation to estimate the first two moments of the forecast distribution. In this literature, each sample is called an \textit{ensemble member}\index{ensemble member} and the set of samples is called the \textit{ensemble}\index{ensemble}. For a textbook review of the \gls{enkf}, interested readers can refer to \citet{Evensen2009}.

Consider the following state-space model:
\begin{equation}
\begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l}
  x_t &=& m_t(x_{t-1}) + \eta_t,
    &\eta_t &\sim& \normal(0, U_t), \\
  y_t &=& H_t x_t + \epsilon_t,
    &\epsilon_t &\sim& \normal(0, V_t),
\end{array}
\label{eqn1:kf-extn2}
\end{equation}
where $m_t: \mathbb{R}^{d_x} \to \mathbb{R}^{d_x}$ is a nonlinear mapping of the state. Like the state-space model under the Kalman filter, the measurement is linear in the state with a $d_y \times d_x$ matrix $H_t$ that maps the state to the measurements. The state transition and measurement densities are:
\begin{align*}
  f(x_t \,|\, x_{t-1}) &= \phi(x_t; m_t(x_{t-1}), U_t), \\
  g(y_t \,|\, x_{t-1}) &= \phi(y_t; H_t x_t, V_t),
\end{align*}
respectively.

Suppose we have random samples from the approximate analysis distribution, $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist X_{t-1} \,|\, y_{0:t-1}$, where $X \approxdist Y$ means that $X$ is approximately distributed as $Y$. Samples cannot be obtained from the optimal analysis distribution, so samples are generated from its estimator instead. At time $t$, samples from the forecast distribution are generated as follows:
\begin{align}
  x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)}) = \phi(x_t; m_t(x_{t-1}^{a(m)}), U_t),
\label{eqn1:enkf-forecast-sample}
\end{align}
for each $m = 1,...,M$. The sample mean and variance are calculated from the samples $\{x_t^{f(m)}\}_{m=1}^M$ and are denoted by $\hat{\mu}_t^f$ and $\hat{\Sigma}_t^f$, respectively. After observing $y_t$, samples from the analysis distribution are generated as a function of the forecast samples as follows:
\begin{equation}
  \tilde{x}_t^{a(m)}
      = x_t^{f(m)} + \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1}[y_t - H_tx_t^{f(m)}].
\label{eqn1:enkf-analysis-sample}
\end{equation}
The sample mean and covariance from samples $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ should equal
\begin{subequations}
\begin{align}
  \hat{\mu}_t^a
      &= \hat{\mu}_t^f + \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1}[y_t - H_t \hat{\mu}_t^f], \label{eqn1:enkf-analysis-mean}\\
  \hat{\Sigma}_t^a
      &= \hat{\Sigma}_t^f - \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1} H_t \hat{\Sigma}_t^f, \label{eqn1:enkf-analysis-var}
\end{align}
\label{eqn1:enkf-analysis-moments}%
\end{subequations}
respectively. %This is true for the mean $\hat{\mu}_t^a$ but not for the covariance $\hat{\Sigma}_t^a$.

% In practice, this version of the \gls{enkf} is often not used because the sample covariance calculated with $\{x_t^{a(m)}\}_{m=1}^M$ will not converge to .
% With \Cref{eqn1:enkf-analysis-sample},
% \begin{align*}
%   \cov(\tilde{X}_t^a)
%     &= \cov(X_t^f) + K_t \cov(y_t - H_t X_t^f) K_t^T.
% \end{align*}
% Since $y_t$ is fixed,
% \begin{align*}
%   \cov(\tilde{X}_t^a)
%     &= \cov(X_t^f) + K_t \cov(H_t X_t^f) K_t^T \\
%     &= \Sigma_t^f + K_t H_t \Sigma_t^f H_t^T K_t^T.
% \end{align*}
% The idea of perturbed observations is to pretend that we can sample $M$ samples of $y_t$. Since we do not have truth, cannot get multiple samples $Y_t$, so approximate $y_t^{(m)} \approx y_t + \epsilon_t^{(m)}$. With perturbed observations,
% \begin{align*}
%   \cov(\tilde{X}_t^a)
%     &= \cov(X_t^f) + K_t \cov(Y_t - H_t X_t^f) K_t^T.
% \end{align*}
%The sample mean of $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ matches up with \Cref{eqn1:enkf-analysis-mean} but the sample covariance gives an incorrect covariance in the sense that it does not properly approximate $\hat{\Sigma}_t^a$\todo{Cari confused by this sentence. Will attempt to clarify.}, so it was proposed to replace $y_t$ in \Cref{eqn1:enkf-analysis-sample} with its perturbed version: $y_t^{(m)} = y_t + \epsilon_t^{(m)}$, where $\epsilon_t^{(m)} \sim \normal(0, V_t)$, for $m = 1,...,M$ \citep{Houtekamer1998,Burgers1998}, giving us another approximate sample from the analysis distribution:

%In practice, the original \gls{enkf} algorithm with samples $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ proposed by \citet{Evensen1994} is often not used.
Both \citet{Houtekamer1998} and \citet{Burgers1998} discovered that the sample mean calculated from the samples $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ equals to $\hat{\mu}_t^a$, but the sample covariance did not equal to $\hat{\Sigma}_t^a$.
%In practice, the original \gls{enkf} algorithm proposed by \citet{Evensen1994} is often not used because it was found that the sample mean and covariance constructed with the samples $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$ would not converge to the first two moments of the ; two modifications to the algorithm are used instead.
To generate samples with a sample covariance that match $\hat{\Sigma}_t^a$, they both independently proposed to replace $y_t$ in \Cref{eqn1:enkf-analysis-sample} with a closely related perturbed version $y_t^{(m)} = y_t + \epsilon_t^{(m)}$ where $\epsilon_t^{(m)} \sim \normal(0, V_t)$, leading to the generation of analysis samples:%; the idea behind this method is to inflate the variance of the approximate samples from the analysis distribution by associating each forecast sample $x_t^{f(m)}$ with another possible observation $y_t$.
\begin{equation}
  x_t^{a(m)}
    = x_t^{f(m)} + \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1}[y_t^{(m)} - H_tx_t^{f(m)}].
\label{eqn1:enkf-analysis-sample2}
\end{equation}
This algorithm is called \textit{\gls{enkf} with perturbed observations}. An alternative procedure is to deterministically scale $\{x_t^{f(m)}\}_{m=1}^M$ so that the resulting samples $\{x_t^{a(m)}\}_{m=1}^M$ have the proper mean and variance via
\begin{gather*}
  x_t^{a(m)} = \hat{\mu}_t^a + \hat{A}_t[x_t^{f(m)} - \hat{\mu}_t^f],
\end{gather*}
where $\hat{A}_t$ has the following property: $\hat{\Sigma}_t^a = \hat{A}_t \hat{\Sigma}_t^f \hat{A}_t$. There is no unique $\hat{A}_t$. \citet{Tippett2003} summarizes different algorithms to find $\hat{A}_t$; this set of algorithms is called \textit{deterministic square-root filters} (or \textit{square-root filters}, for short)\index{square-root filters}. Though rarely used in practice, we call the original formulation of the \gls{enkf} developed by \citet{Evensen2003} with resulting samples
\begin{align*}
  x_t^{a(m)} = \tilde{x}_t^{a(m)}
\end{align*}
the \textit{original \gls{enkf}}\index{\gls{enkf}!original} to distinguish it from the two other \gls{enkf} algorithms. %Under the assumptions, the forecast distribution for time $t+1$ requires samples from the distribution of $X_t \,|\, y_{1:t}$, but approximate samples generated by any of these three algorithms are used instead.

\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
  \small
  \textbf{Inputs}: $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$
  ~\\
  \textbf{Output}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{x_t^{f(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{0:t-1})$
    \item $\{x_t^{a(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{0:t})$
  \end{itemize}

  \begin{enumerate}[leftmargin=*]
    \item \ul{Forecast step}: Sample $x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)})$ for each $m = 1, ..., M$.

    \item Observe $y_t$.

    \item If performing inflation, calculate sample mean $\hat{\mu}_t^f$ from forecast samples $\{x_t^{f(m)}\}_{m=1}^M$, choose or estimate\footnote{See \citet{Anderson2007,Anderson2009} for more information on how to apply adaptive inflation.} inflation factor $\lambda_t \geq 1$, and set
      \begin{align*}
        \tilde{x}_t^{f(m)} = \hat{\mu}_t^f + \lambda_t(x_t^{f(m)} - \hat{\mu}_t^f).
      \end{align*}
      Otherwise, set
      % \begin{align*}
        $\tilde{x}_t^{f(m)} = x_t^{f(m)}.$
      % \end{align*}

    \item \label{item:enkf-update} \ul{Update step}:
    \begin{enumerate}[leftmargin=*,noitemsep]
      \item Calculate the Kalman gain
      \begin{gather*}
        \hat{K}_t = \hat{\Sigma}_t^f H_t^T ( V_t + H_t \hat{\Sigma}_t^f H_t^T )^{-1},
      \end{gather*}
      where $\hat{\Sigma}_t^f$ is the sample covariance calculated from $\{x_t^{f(m)}\}_{m=1}^M$, including tapering if performing localization.
      \item \label{item1:enkf-update} Perform initial update:
      \begin{align*}
        \tilde{x}_t^{a(m)}
          = x_t^{f(m)} + \hat{K}_t[y_t - H_t \tilde{x}_t^{f(m)}] \text{ for each } m = 1, ..., M.
      \end{align*}
      \item Perform final update:
            \begin{itemize}[leftmargin=*]
            \item With {original \gls{enkf}}, set $x_t^{a(m)} = \tilde{x}_t^{a(m)}$ for each $m=1,...,M$.
                  % \begin{align*}
                    % x_t^{a(m)} = \tilde{x}_t^{a(m)} \text{ for each } m = 1, ..., M.
                  % \end{align*}
            \item With {perturbed observations}\footnote{In practice, this step is combined with Step \ref{item1:enkf-update} for computational efficiency, but we outline the algorithm in this way to contrast the various \gls{enkf} updating algorithms.}, sample $\epsilon_t^{(m)} \sim \normal(0, V_t)$ and calculate
                  \begin{gather*}
                    x_t^{a(m)} = \tilde{x}_t^{a(m)} + \hat{K}_t \epsilon_t^{(m)} \text{ for each } m = 1, ..., M.
                  \end{gather*}
            \item With {square-root filters}\footnote{See \citet{Tippett2003} for more information on finding such an $\hat{A}_t$.}, calculate the sample mean of $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$, denoted as $\hat{\mu}_t^a$. Find $\hat{A}_t$ such that $\hat{\Sigma}_t^a = \hat{A}_t \hat{\Sigma}_t^f \hat{A}_t$, where $\hat{\Sigma}_t^a$ is the sample covariance calculated with $\{\tilde{x}_t^{a(m)}\}_{m=1}^M$. Then,
                  \begin{gather*}
                    x_t^{a(m)} = \hat{\mu}_t^a + \hat{A}_t[\tilde{x}_t^{f(m)} - \hat{\mu}_t^f] \text{ for all } m = 1, ..., M.
                  \end{gather*}
            \end{itemize}
      \end{enumerate}
  \end{enumerate}

\end{minipage}
}
\captionnew{Ensemble Kalman filter.}{}%For perturbed observations, Step 2e is normally combined with Step \ref{item1:enkf-update} for computational efficiency, but we choose to outline the algorithm in this way to contrast the various \gls{enkf} updating algorithms. For square-root filters, see \citet{Tippett2003} for more information on finding such an $\hat{A}_t$.}
\label{alg:enkf}
\end{alg}

Due to the \gls{enkf}'s close connection with the \gls{ekf}, the optimal filtering densities of \Cref{eqn1:filtering-densities} and predictive likelihood of \Cref{eqn1:cond-lik} are approximated similarly to the extended Kalman filter: instead of approximating the moments with their Taylor expansions, the moments are replaced with sample moments. Specifically, the forecast and analysis distributions are approximated as
\begin{subequations}
\begin{gather}
  \pi_{0:t-1}(x_t) \approx \phi(x_t; \hat{\mu}_t^f, \hat{\Sigma}_t^f), \label{eqn1:enkf-forecast} \\
  \pi_{0:t}(x_t) \approx \phi(x_t; \hat{\mu}_t^a, \hat{\Sigma}_t^a),
\end{gather}
\end{subequations}
respectively (cf. \Cref{eqn1:ekf-forecast,eqn1:ekf-analysis}). Furthermore, the predictive likelihood is approximated as
\begin{align}
  \pi_{0:t-1}(y_t) \approx \phi(y_t; H_t^T \hat{\mu}_t^f, V_t + H_t \hat{\Sigma}_t^f H_t^T)
\tag{{\theequation}c}
\label{eqn1:enkf-condlik}
\end{align}
(cf. \Cref{eqn1:ekf-condlik}).

%There are a few remarks worth making about the above algorithm that will be important later in \Cref{sect:enkf-reinterpretation}, when we re-interpret the \gls{enkf} algorithm.
%\begin{itemize}[leftmargin=*]
%\item Let $\delta_x$ denote the point mass at $x$. If the samples $\{x_{t-1}^{a(m)}\}_{m=1}^M$ are truly random samples from $X_{t-1} \,|\, y_{1:t-1}$, it is straightforward to show that the empirical distribution $\enkf_{1:t-1}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{f(m)}}(x_t)$ converges weakly to $\pi_{1:t}(x_t)$. On the other hand, even if we had samples from the distribution of $X_t \,|\, y_{1:t-1}$, the same statement cannot be made with the empirical distribution of $\enkf_{1:t}(x_t) \equiv \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{a(m)}}(x_t)$, where $\{x_t^{a(m)}\}_{m=1}^M$ were generated by perturbed observations or square-root filters \citep{Lei2010}.

%\item
Even though the development of the \gls{enkf} was inspired by the \gls{ekf}, unlike the \gls{ekf}, the sample mean $\hat{\mu}_t^a$ and variance $\hat{\Sigma}_t^a$ are not used to forecast the next set of samples. In practice, the samples $\{x_t^{a(m)}\}_{m=1}^M$ are used in the next step of the algorithm to generate samples from the forecast distribution at time $t+1$. Therefore, the errors associated with the generation of the analysis samples are propagated to the next step of the algorithm, also leading to the possibility of filter divergence. %\citep{Harlim2010,Gottwald2013}.
To overcome filter divergence, the following two adjustments are highly recommended by \gls{enkf} experts:
\begin{itemize}[leftmargin=*]
  \item \textbf{Variance inflation}. Deterministically scale the forecast samples $\{x_t^{f(m)}\}_{m=1}^M$ to increase its sample variance via:
  \begin{align}
    \tilde{x}_t^{f(m)} = \hat{\mu}_t^f + \lambda_t(x_t^{f(m)} - \hat{\mu}_t^f),
  \label{eqn1:var-inf}
  \end{align}
  where $\lambda_t > 1$ is chosen by the user \citep{Hamill2001} or estimated with data \citep{Anderson2007,Anderson2009} (called \textit{adaptive inflation}\index{adaptive inflation}\index{inflation!adaptive} in the literature). %\citep{Hamill2001,Anderson2007,Anderson2009,Miyoshi2011,Altaf2013}
  \item \textbf{Localization}. Taper the sample forecast covariance matrix via:
  \begin{align*}
    \hat{\Sigma}_t^f \circ T(d),
  \end{align*}
  where ``$\circ$'' denotes the Schur product and $T(d)$ is a tapering covariance matrix that sets covariances to 0 when the distance between two spatial locations of the state are greater than $d$. We refer the reader to \citet{Bickel2008} for more information on tapering and to \citet{Furrer2007} for more information on tapering in the context of \gls{enkf}.
\end{itemize}
%\end{itemize}
In practice, the above two adjustments are crucial to prevent particle collapse when applying the \gls{enkf} to high-dimensional state spaces. For this reason, we include the two adjustments in \Cref{alg:enkf}, which summarizes the \gls{enkf} algorithm.

% Goal: get samples $\{x_t^{a(m)}\}_{m=1}^M$ so that its sample covariance converges to the right covariance.
% Perturbed observations:
% \begin{align*}
%   Cov(x_t^a) = Cov(x_t^f) + K Cov(y - H x_t^f) K^T,
% \end{align*}
% $y$ is not random, so generate approximate samples.

% Square root filters:
% \begin{align*}
%   x_t^{a(m)} = \hat{\mu}_t^a + \hat{A}[x_t^{f(m)} - \hat{\mu}_t^f],
% \end{align*}
% where $\hat{A} = \hat{A}\hat{\Sigma}_t^f \hat{A}^T = \hat{\Sigma}_t^a$



