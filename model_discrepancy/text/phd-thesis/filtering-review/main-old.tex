\synctex=1
\documentclass{article}

\usepackage{amsmath,amsfonts,amsthm,amssymb,amsthm}
\usepackage{bbm}
\usepackage{cancel}
%\usepackage{color}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage[authoryear, round]{natbib}
\usepackage{comment}
\usepackage{subcaption}
\usepackage{abstract}
\usepackage[hyphens]{url}
%\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\usepackage[noabbrev,capitalise]{cleveref}
\usepackage{soul}
\usepackage[toc,page]{appendix}
\usepackage{enumerate}
\usepackage{paralist}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{nopageno}

%\newcommand{\hl}[1]{\colorbox{yellow}{#1}}
\newcommand{\noi}{\noindent}
\newcommand{\spacingsmall}{2.5em}
\newcommand{\spacingbig}{5em}

% math stuffs
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\trace}{Tr}
\newcommand{\norm}[1]{\lVert #1 \rVert}
\DeclareMathOperator{\one}{\mathbbm{1}}
\DeclareMathOperator{\tr}{Tr}
%\DeclareMathOperator{\det}{det}
\DeclareMathOperator{\rank}{rank}

\DeclareMathOperator{\e}{\mathbb{E}}
\DeclareMathOperator{\var}{\mathbb{V}ar}
\DeclareMathOperator{\cov}{\mathbb{C}ov}
\DeclareMathOperator{\prob}{\mathbb{P}r}
\DeclareMathOperator{\se}{\widetilde{\mathbb{E}}}
\DeclareMathOperator{\svar}{\widetilde{\mathbb{V}ar}}
\DeclareMathOperator{\scov}{\widetilde{\mathbb{C}ov}}
\DeclareMathOperator{\sprob}{\widetilde{\mathbb{P}}}
\DeclareMathOperator{\normal}{\mathcal{N}}


\newcommand{\pf}{\pi^n}
\newcommand{\enkf}{\psi^n}
\newcommand{\nf}{\nu^n}
\newcommand{\pft}{\tilde{\pi}^n}
\newcommand{\enkft}{\tilde{\psi}^n}
\newcommand{\nft}{\tilde{\nu}^n}


\newcommand\indep{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\newtheorem{defn}{Definition}

% box equations in "align" environment
\newlength\dlf
\newcommand\alignedbox[2]{
  % #1 = before alignment
  % #2 = after alignment
  &
  \begingroup
  \settowidth\dlf{$\displaystyle #1$}
  \addtolength\dlf{\fboxsep+\fboxrule}
  \hspace{-\dlf}
  \boxed{#1 #2}
  \endgroup
}

\newcommand{\creflastconjunction}{, and\nobreakspace} % oxford comma

\title{Ensemble Kalman filter: Convergence to the Particle Filter}
\author{Linda N. Tran and Cari G. Kaufman}

\date{November 1, 2014}

\begin{document}
\maketitle

\begin{abstract}
The Ensemble Kalman filter (EnKF) is an algorithm to sample from a state space model with nonlinear transitions and is widely used in the atmospheric science community for weather forecasting. We will demonstrate that, with a minor modification, the EnKF converges almost surely to the true distribution. In addition, we will show that the EnKF is guaranteed to be more asymptotically efficient than the particle filter, a competing algorithm. This analysis sheds light on the reason why the EnKF tends to work well in practice for high-dimensional state spaces, while the particle filter typically fails. Our results provide theoretical guarantees for the many atmospheric science applications that use EnKF and open up a new perspective to the sequential Monte Carlo community to focus on biased, but asymptotically consistent, estimators.

Many interesting forecasting problems can be cast as a hidden Markov model with non-linear state transitions. Particle filters are a suite of sampling algorithms that have been developed to estimate the hidden states in these models, but they have issues with high-dimensional state spaces: the number of samples required is more than the dimensions of the state, thus increasing the computational complexity of the algorithm. To resolve this issue, we introduce a simple tweak to the algorithm and demonstrate that better forecasts can be made using fewer samples than the dimensions of the state.
\end{abstract}

\tableofcontents
\vspace{5em}

\begin{itemize}
  \item Related work:
        \begin{itemize}
          \item convergence of the linear EnKF
          \item convergence of the LM-EnKS
          \item mixture filters: Ensemble Kalman particle filter (Frei et al), particle Kalman filter (Hoteit et al), adaptive Gaussian mixture filter (Stordal; Rezaie)
          \item Kernel filters
          \item Kernel density estimators
        \end{itemize}
  \item Other:
        \begin{itemize}
          \item Emphasize the reason why the papers that try to prove $L^p$ convergence of the non-linear EnKF says it's impossible: they view the EnKF incorrectly. The bandwidth needs to change.
          \item KDE literature advises to use the sample covariance matrix but there are no theoretical guarantees.
          \item Why is there $L^p$ convergence of the linear EnKF when the bandwidths don't change with $n$?
          \item talk about the misconception that EnKF is one normal distribution, as in with extended kalman filter?
        \end{itemize}
\end{itemize}

\input{intro}
\section{Review}
  \input{optimal-filter}
  \input{particle-filter}
  \input{enkf}
  \input{diffs}
\input{new-filter}
\input{lemma-distance}
\input{lemma-bandwidth}

\begin{appendices}
\input{long-proof}

\bibliographystyle{plainnat}
\bibliography{refs}

\end{appendices}
\end{document}
