%!TEX root = ../thesis.tex
\synctex=1

\Acrfullpl{pf} are a class of algorithms to approximate the filtering densities of the general state space model outlined in \Cref{eqn1:general-filter}. This state-space model need not have additive and Gaussian errors as with the extensions of the Kalman filter. The idea behind these methods is similar to the \gls{enkf}: a Monte Carlo sample is used to estimate the optimal filtering distributions (or point estimates of them). The samples are called \textit{particles}\index{particles} instead of ensemble members. The sampling mechanism behind the particle filter is importance sampling applied sequentially in time, hence its other name in the literature: \textit{\acrfull{smc}}\index{sequential Monte Carlo} or \textit{\gls{sirs}}\index{sequential importance resampling}\index{sequential importance sampling}. %However, unlike the \gls{enkf}, particle filters have been proven to converge in a sense that will be discussed after the introducing the filter.

In this section, we present the simplest particle filter: the bootstrap filter. Most presentations of the \gls{bf} present it as a \gls{sirs} algorithm, where the prior transition density $f(x_t \,|\, x_{t-1})$ is used as the proposal distribution for the importance sampling\footnote{For a more general overview of particle filtering, we refer interested readers to \citet{Cappe2007}, \citet{Doucet2012}, and references therein.}. Instead, our presentation follows the slight modification of the bootstrap filter presented in \citet[][Section III-A]{Crisan2002}. This particular presentation is often used in the probabilistic analysis of particle filters \citep{Cappe2006,DelMoral2004,DelMoral2013}, where the bootstrap filter is presented as approximating the optimal filtering densities in \Cref{eqn1:filtering-densities} with empirical densities\footnote{The approximations are referred to as \textit{mean field approximations}\index{mean field approximation}.}. The reason for choosing this particular presentation over its usual presentation is to make clear how the \gls{enkf} is connected to the bootstrap filter: \gls{enkf} uses a different estimator to the optimal filtering densities than the bootstrap filter; we elaborate on this further in \Cref{sect:enkf-reinterpretation}.%\todo{Make a note somewhere in this section that the PF actually samples from the joint distribution of states... not just the marginal. But it's easy to get the marginal from the joint. Do this so I can refer to it in the APF chapter.}

Suppose we have random samples from an approximation of the analysis distribution, $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist X_{t-1} \,|\, y_{0:t-1}$. The empirical density
\begin{align*}
  \pf_{0:t-1}(x_{t-1})
    \equiv \frac{1}{M} \sum_{m=1}^M \delta_{x_{t-1}^{a(m)}}(x_{t-1})
\end{align*}
% \todo{Deb: why not put hats on $\pf$}
is an approximation of $\pi_{0:t-1}(x_{t-1})$, the optimal density of $X_{t-1}^a$; the superscript $M$ is to remind the reader that $\pf_{0:t-1}$ is an estimator of $\pi_{0:t-1}$ and that it depends on the sample size $M$. Plug this approximation into the optimal forecast distribution of \Cref{eqn1:forecast} to generate forecast particles at time $t$:
\begin{align*}
  x_t^{f(m)}
    \sim \int f(x_t \,|\, x_{t-1}) \pf_{0:t-1}(x_{t-1}) dx_{t-1}
    = \frac{1}{M}\sum_{m=1}^M f(x_t \,|\, x_{t-1}^{a(m)}),
\end{align*}
for each $m = 1,...,M$. In the usual presentation of the bootstrap filter\footnote{This technicality is not important in the mathematical analysis of \gls{sirs} algorithms, because \gls{sirs} is considered to be an equivalent interpretation of Feynman-Kac mean field Interacting Particle System models. See \citet{DelMoral2013} for a comprehensive review.}, $x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)})$ for each $m=1,...,M$. Note that these are not samples from $\pi_{0:t-1}(x_t)$; they are samples from its estimator, since an estimator of the analysis distribution $\pi_{0:t-1}^M(x_{t-1})$ is used to sample from the forecast distribution instead of the optimal analysis distribution $\pi_{0:t-1}(x_{t-1})$. These samples are used to provide an estimator to the forecast distribution:
\begin{align}
  \pf_{0:t-1}(x_t) = \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{f(m)}}(x_t).
\label{eqn1:pf-forecast}
\end{align}
After observing $y_t$, the state is updated using this estimator of the forecast distribution.

Recall that the predictive likelihood is required in the calculation of the analysis distribution. Plugging the approximation of the forecast distribution ($\pf_{0:t-1}(x_t)$) into the optimal predictive likelihood of \Cref{eqn1:cond-lik} provides an approximation to the predictive likelihood:
\begin{align}
  \pf_{0:t-1}(y_t)
    \equiv \int g(y_t \,|\, x_t) \pf_{0:t-1}(x_t) dx_t
    = \frac{1}{M} \sum_{m=1}^M g(y_t \,|\, x_t^{f(m)})
    = \frac{1}{M} \sum_{m=1}^M l_t^{(m)},
\label{eqn2:bf-predlik}
\end{align}
where $l_t^{(m)} \equiv g(y_t \,|\, x_t^{f(m)})$. Now, the analysis distribution is approximated by plugging the estimators of the forecast distribution and the predictive likelihood into the optimal analysis density of \Cref{eqn1:analysis}:
\begin{align*}
  \pft_{0:t}(x_t)
    &\equiv \frac{g(y_t \,|\, x_t) \pf_{0:t-1}(x_t)}{\pf_{0:t-1}(y_t)} \\
    %&\equiv \frac{g(y_t \,|\, x_t) \pf_{t|t-1}(x_t)}{\sum_{n=1}^M l_t^{(n)}} \\
    &= \sum_{m=1}^M \frac{g(y_t \,|\, x_t^{f(m)})}{\sum_{n=1}^M l_t^{(n)}} \delta_{x_t^{f(m)}}(x_t) \\
    &= \sum_{m=1}^M \frac{l_t^{(m)}}{\sum_{n=1}^M l_t^{(n)}} \delta_{x_t^{f(m)}}(x_t).
\end{align*}
Let $\tilde{x}_t^{a(m)} = x_t^{f(m)}$ and $\tilde{w}_t^{(m)} = l_t^{(m)} / \sum_{n=1}^M l_t^{(n)}$ for each $m = 1,...,M$. The weights $\{\tilde{w}_t^{(m)}\}_{m=1}^M$ are called \textit{importance weights}\index{importance weights}. Then,
\begin{align}
  \pft_{0:t}(x_t)
    &= \sum_{m=1}^M \tilde{w}_t^{(m)} \delta_{\tilde{x}_t^{a(m)}}(x_t)
\label{eqn1:bf-analysis1}
\end{align}
is an estimator of the analysis distribution $\pi_{0:t}(x_t)$. We make a few remarks about the update step:
\begin{itemize}[leftmargin=*]
  \item Technically, the predictive likelihood need not be calculated to sample from the analysis distribution. Only the measurement density needs to be evaluated with the forecast samples, i.e., $l_t^{(m)} = g(y_t \,|\, x_t^{f(m)})$, which are normalized in the calculation of the importance weights without evaluating the estimator of the predictive likelihood.
  \item The particles $x_t^{f(i)}$ from the forecast step and $\tilde{x}_t^{a(i)}$ from the analysis step do not differ; only their weights have changed.
\end{itemize}

With high probability, samples with low importance weights will become irrelevant at future filtering steps, causing \textit{particle collapse}\index{particle collapse} (or \textit{particle degeneracy}\index{particle degeneracy}), where all samples but one have an importance weight of zero. Resampling helps remove those samples with low probability mass. There are many resampling algorithms proposed in the literature; see \Cref{sec:resampling} for a selected review. Let $\{x_t^{a(m)}\}_{m=1}^M$ be a simple random sample from $\pft_{0:t}(x_t)$. These samples provide another estimator of the analysis distribution:
\begin{align*}
  \pf_{0:t}(x_t)
    &\equiv \frac{1}{M} \sum_{m=1}^M \delta_{x_t^{a(m)}}(x_t).
\end{align*}
Since resampling increases the Monte Carlo variance, the resampling step is, in practice, only performed when the effective sample size\index{effective sample size}
\begin{align*}
  ESS = \left( \sum_{m=1}^M (\tilde{w}_t^{(m)})^2 \right)^{-1}
\end{align*}
is below some threshold\footnote{For more information on effective sample sizes, refer to \citet[Section 3.5]{Doucet2012} and references therein.}. Either estimator of the analysis distribution, $\pft_{0:t}(x_t)$ or $\pf_{0:t}(x_t)$, can be used in the next filtering step, repeating the algorithm outlined in this section. \Cref{alg:bf} summarizes the \gls{bf} algorithm, where the user chooses whether or not to resample after every time step. Without loss of generality, we assume that resampling is performed at each time $t$ in our presentation.

\begin{alg}
\fbox{
\begin{minipage}{\linewidth}
  \small
  \textbf{Inputs}: $\{x_{t-1}^{a(m)}\}_{m=1}^M \approxdist p(x_{t-1} \,|\, y_{0:t-1})$
  ~\\
  \textbf{Output}:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt]
    \item $\{x_t^{f(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{0:t-1})$
    \item $\{x_t^{a(m)}\}_{m=1}^M \approxdist p(x_t \,|\, y_{0:t})$
  \end{itemize}

  \begin{enumerate}[leftmargin=*]
    \item \ul{Forecast step}: Sample $x_t^{f(m)} \sim f(x_t \,|\, x_{t-1}^{a(m)})$ for each $m = 1, ..., M$.

    \item Observe $y_t$.

    \item \ul{Update step}:
      \begin{enumerate}[leftmargin=*,noitemsep]
      \item Calculate
            \begin{align*}
              l_t^{(m)} = w_{t-1}^{(m)} g(y_t \,|\, x_t^{f(m)})
            \end{align*}
            for each $m = 1,...,M$.

      \item Calculate
            \begin{gather*}
              \tilde{x}_t^{a(m)}
                = x_t^{f(m)}, \\
              \tilde{w}_t^{(m)}
                = l_t^{(m)} / \textstyle{\sum}_{n=1}^M l_t^{(n)}
            \end{gather*}
            for each $m = 1,...,M$.
    \end{enumerate}

    \item \ul{Resample step}: If resampling,
          \begin{gather*}
            j_m = i \text{ with probability } \tilde{w}_t^{(i)}, \\
            x_t^{a(m)} = \tilde{x}_t^{(j_m)}, \\
            w_t^{(m)} = 1/M,
          \end{gather*}
          for each $m = 1,...,M$. Otherwise,
          \begin{gather*}
            x_t^{a(m)} = \tilde{x}_t^{(m)}, \\
            w_t^{(m)} = \tilde{w}_t^{(m)},
          \end{gather*}
          for each $m = 1,...,M$.
  \end{enumerate}
\end{minipage}
}
\captionnew{Bootstrap filter.}{}
\label{alg:bf}
\end{alg}

\subsection{Resampling algorithms} \label{sec:resampling}
In this section, we discuss resampling broadly, outside of the context of filtering. The goal in this section is to obtain a simple random sample $\{x_m\}_{m=1}^M$ from $p(x)$ when we have a random, but not necessarily simple, sample $\{(\tilde{x}_m, \tilde{w}_m)\}_{m=1}^M$ from the density $p(x)$. Each sample $\tilde{x}_m$ has a weight $\tilde{w}_m$ for each $m = 1,...,M$ and $\sum_{m=1}^M \tilde{w}_m = 1$. These samples form the empirical density estimator $\tilde{p}(x) \equiv \sum_{m=1}^M \tilde{w}_m \delta_{\tilde{x}_m}(x)$. Resampling is performed by sampling indices with weights $\{\tilde{w}_m\}_{m=1}^M$:
\begin{gather}
  j_m = i \text{ with probability } \tilde{w}_i
\label{eqn1:resample-indices}
\end{gather}
for each $m=1,...,M$ and using the resampled indices $\{j_m\}_{m=1}^M$ to obtain simple random samples $\{x_m\}_{m=1}^M$ from $\tilde{p}(x)$:
\begin{gather*}
  x_m = \tilde{x}_{j_m}
  \text{ with probability }
  w_m = 1/M.
\end{gather*}
%The samples $\{x_m\}_{m=1}^M$ is then a simple random sample from $\tilde{p}(x)$.
We discuss a few resampling algorithms to sample the indices $\{j_m\}_{m=1}^M$ as per \Cref{eqn1:resample-indices}:

\begin{itemize}[leftmargin=*]
  \item \textbf{Multinomial sampling}\index{multinomial sampling}: Sampling indices as in \Cref{eqn1:resample-indices} is performed using an inverse transform sampling algorithm. %, which will be helpful in explaining the other resampling algorithms. %The idea of inverse transform sampling is to randomly sample \hl{what}%where auxiliary random samples $u_m \sim U[0,1]$ and $U[0,1]$ denotes the standard uniform distribution on the interval $[0,1]$. Another way to reformulate this problem is to sample an auxiliary variables
  Sample auxiliary random variables
  \begin{align*}
    u_m \sim \unif(0,1)
    \text{ for each } m = 1,...,M,
  \end{align*}
  where $\unif(a,b)$ is the continuous uniform random variable on the interval $[a,b]$. These auxiliary random variables are used to select indices as follows:
  \begin{align}
    j_m = \sum_{n=1}^M n\mathbb{I}\bigg[u_n \in \big[\textstyle{\sum}_{i=1}^{n-1} \tilde{w}_i, \textstyle{\sum}_{i=1}^n \tilde{w}_i \big) \bigg]
    \text{ for each } m = 1,...,M,
  \label{eqn1:cat-dist}
  \end{align}
  where $\mathbb{I}$ is the indicator function (i.e., $\mathbb{I}(x \in A) = 1$ when $x \in A$ and zero otherwise) and $\sum_{i=1}^0 \tilde{w}_i \equiv 0$.

  \item \textbf{Stratified sampling}\index{stratified sampling}: Sample $\tilde{u}_m \sim \unif(0,1)$ for each $m = 1,...,M$. Calculate auxiliary random variables
  \begin{equation*}
    u_m = \frac{(m-1) + \tilde{u}_m}{M} \quad
    \text{ for each } m = 1,...,M
  \end{equation*}
  and then select indices according to \Cref{eqn1:cat-dist}.
  \item \textbf{Systematic sampling}\index{stratified sampling}: Sample $\tilde{u} \sim U(0,1)$. Calculate
  \begin{equation*}
    u_m = \frac{(m-1) + \tilde{u}}{M} \quad
    \text{ for each } m = 1,...,M
  \end{equation*}
  and then select indices according to \Cref{eqn1:cat-dist}.
  %\item \textbf{Residual sampling}\index{residual sampling}: Another popularly used resampling algorithm that we will not discuss; see \hl{ref} \todoref for a review.
\end{itemize}
According to \citet[Section 3.5]{Doucet2012}, the sampling algorithms in decreasing order of popularity and efficiency are: systematic sampling, residual sampling (not discussed), and multinomial sampling. For a more thorough discussion of resampling in the context of \glspl{pf}, we refer the reader to \citet[Sections 3.4 and 3.5]{Doucet2012}, \citet{Douc2005}, and \citet{Hol2006}. We conclude this section with a theorem that will be important in a later discussion on the connection between the \gls{enkf} and the \gls{bf}.


\begin{theorem}
  If $\tilde{w}_m = 1/M$ for all $m = 1,...,M$, stratified and systematic random sampling returns the simple random samples $x_m = \tilde{x}_m$.
\label{thm:sys-sampling-unif-weights}
\end{theorem}
\begin{proof}
  By \Cref{eqn1:cat-dist},
  \begin{align*}
    j_m = \sum_{n=1}^M n \mathbb{I}\bigg[u_n \in \big[\tfrac{n-1}{M}, \tfrac{n}{M} \big) \bigg] \text{ for each } m = 1,...,M.
  \end{align*}
  With stratified or systematic sampling, %$u_m \sim U(\frac{m-1}{M}, \frac{m}{M})$. We have
  $\int_{(n-1)/M}^{n/M} p(u_k) = 1$ if $k = n$ and zero otherwise, therefore $\mathbb{I}[u_n \in [\tfrac{n-1}{M}, \tfrac{n}{M} )] = 1$ if $n=m$ and zero otherwise. Therefore, $j_m = m$ and $x_m = \tilde{x}_m$.
\end{proof}

%Remarkably, it has been shown that $\pf_{t|t-1} \to \pi_{t-1|t-1}$ and $\pft_{t|t},\pf_{t|t} \to \pi_{t|t}$ weakly as $M \to \infty$ for all time $t$\todo{refs. might need to be a theorem if it's referred to in the next chapter}.

%Figure \ref{fig:smc-algorithms}(a) summarizes the bootstrap filter. \hl{talk about how there are many improvements done to the bootstrap filter, e.g., APF. Point to review paper.}


%Even though point estimates generated by the importance sampling algorithms are well known to converge almost surely to the parameter being estimated \citep[][and references therein]{Geyer2011}, it is not straightforward that the same is true when the algorithm is applied sequentially, as with the particle filter. Specifically, for a particular time $t$, it is usually easy to show that errors converge to zero as the number of samples increases, but when the algorithms are applied sequentially,small perturbations at each time step can accumulate over time leading to nonconvergence \citep[see Section IV-A][for an example]{Crisan2002}. Fortunately, it has been proven that particle filters do converge\todo{converge how?} as the number of samples increases under some conditions\todoref.

