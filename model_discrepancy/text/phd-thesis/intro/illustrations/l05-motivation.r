
setwd("~/data_assimilation/")
source("model_discrepancy/code/lorenz05/lorenz05.r")
image.folder = "~/data_assimilation/model_discrepancy/text/phd-thesis/intro/illustrations/lorenz05"
library(ggplot2)
library(grid)
library(gridExtra)
library(extrafont)
library(reshape2)
loadfonts()



N = 960
dt = .001
parms2 = lorenz05.parms(c(N = N, K = 32, I = 12, F = 15, b = 10, c = 2.5, model.number = 2))
parms3 = lorenz05.parms(c(N = N, K = 32, I = 12, F = 15, b = 10, c = 2.5, model.number = 3))
load("model_discrepancy/data/lorenz05/DARTobsinits.rdata")

ggplot.setup = function(p) {
  p = p + theme_bw() +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          axis.ticks.y = element_blank(),
          axis.text.x = element_text(size = 12),
          axis.text.y = element_blank(),
          #axis.title.x = element_text(size = 12),
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          strip.text.y = element_text(size = 12, angle = 0),
          strip.background = element_rect(color = "white", fill = "white"),
          plot.margin=unit(c(0,0,0,0),"mm"),
          panel.margin = unit(0, "lines"))
  return(p)
}

start = ode(y = inits$perfect[1:N], times = c(0, 6*30*24*dt), func = lorenz05, parms = parms3, 
            method = "ode45", verbose = TRUE)[2,-1]
plot(start, type = "l")

# illustrate parameters of model ------------------------------------------------------------
params = list(K = 2^(1:6),
              F = 2^(1:6))

data = vector("list", length(params))
for(i in 1:length(params)) {
  parm.name = names(params)[i]
  parms = lapply(params[[i]], function(value) {
    parms = c(N = N, K = 32, F = 15, model.number = 2)
    parms[parm.name] = value
    lorenz05.parms(parms)
  })
  z = lapply(parms, function(p) {
    ode(y = start, times = c(0, 6*30*24*dt), func = lorenz05, parms = p, 
        method = "ode45", verbose = TRUE)[2,-1]
  })
  
  data[[i]] = data.frame(loc = rep(1:N, length(z)), 
                         z = unlist(z),
                         param.name = parm.name,
                         param.value = rep(params[[i]], each = N))
}

data2 = lapply(data, function(l) aggregate(z ~ param.value, l, mean))

for(i in 1:length(params)) {
  parm.name = names(params)[i]
  pdf(sprintf("%s/model2-%s.pdf", image.folder, parm.name), 
      width = 5.5, height = 4.5, family = "CM Roman")
  p = ggplot(data[[i]], aes(loc, z)) + 
    facet_grid(param.value ~ ., scale = "free", space = "free") +
    #facet_grid(param.value ~ ., scale = "free") +
    geom_hline(data = data2[[i]], aes(yintercept = z), color = "gray") + 
    geom_line()
  p = ggplot.setup(p) + theme(panel.border = element_blank())
  print(p)
  dev.off()
}

# illustrate parameters of model 3 ------------------------------------------------------------
# params = list(K = 2^(1:5),
#               I = seq(5,25,5),
#               b = seq(3,15,3),
#               c = seq(1,5,1)+.5,
#               F = 2^(1:5))
# 
# data = vector("list", length(params))
# for(i in 1:length(params)) {
#   parm.name = names(params)[i]
#   parms = lapply(params[[i]], function(value) {
#     parms = c(N = N, K = 32, I = 12, F = 15, b = 10, c = 2.5, model.number = 3)
#     parms[parm.name] = value
#     lorenz05.parms(parms)
#   })
#   z = lapply(parms, function(p) {
#     ode(y = start, times = c(0, 6*30*24*dt), func = lorenz05, parms = p, 
#         method = "ode45", verbose = TRUE)[2,-1]
#   })
#   xy = mapply(ZtoXY, z, parms, SIMPLIFY = FALSE)
#   
#   data[[i]] = data.frame(loc = rep(1:N, length(z)), 
#                          z = unlist(z),
#                          param.name = parm.name,
#                          param.value = rep(params[[i]], each = N))
#   data[[i]]$x = unlist(lapply(xy, function(l) l$X))
#   data[[i]]$y = unlist(lapply(xy, function(l) l$Y))
# }
# 
# data2 = lapply(data, function(l) aggregate(cbind(x,y,z) ~ param.value, l, mean))
# 
# for(i in 1:length(params)) {
#   for(state.type in c("x", "y", "z")) {
#     parm.name = names(params)[i]
#     pdf(sprintf("%s/model3-%s-%s.pdf", image.folder, state.type, parm.name), 
#         width = 5.5, height = 4.5, family = "CM Roman")
#     p = ggplot(data[[i]], aes_string(x = "loc", y = state.type)) + 
#       facet_grid(param.value ~ ., scale = "free", space = "free") +
#       #facet_grid(param.value ~ ., scale = "free") +
#       geom_hline(data = data2[[i]], aes_string(yintercept = state.type), color = "gray") + 
#       geom_line()
#     p = ggplot.setup(p)
#     print(p)
#     dev.off()
#   }
# }
# 
# # data3 = melt(do.call(rbind, data), id.vars = c("loc", "param.name", "param.value"),
# #              variable.name = "state")
# # p = ggplot(data3, aes(loc, value)) + 
# #   facet_grid(param.name ~ param.value, scale = "free", space = "free") +
# #   #geom_hline(data = data2[[i]], aes_string(yintercept = state.type), color = "gray") + 
# #   geom_line()
# # p = ggplot.setup(p)
# # print(p)
# 

# illustrate large- vs small-scale dynamics --------------------------------------
z = ode(y = start, times = c(0, 6*30*24*dt), func = lorenz05, parms = parms3, 
      method = "ode45", verbose = TRUE)[2,-1]
xy = ZtoXY(z, parms3)

data = data.frame(loc = rep(1:N, 3), 
                  value = c(xy$X, xy$Y, z), 
                  state.type = rep(c("x", "y", "z"), each = N))

p = ggplot(data, aes(loc, value, color = state.type)) + 
  geom_line() #+ facet_grid(state.type ~ ., scales = "free", space = "free")
p = ggplot.setup(p) + theme(panel.border = element_blank())
print(p)


# depict chaos with model 2 vs model 3 --------------------------------------------
#dt = (1/40)/3
#dt = .001
#dt = .005
model2 = ode(y = inits$perfect[1:N], times = seq(0, 6*30*24*dt, dt), func = lorenz05, parms = parms2, 
             method = "rk4", verbose = TRUE, hini = dt)

model3 = ode(y = inits$perfect[1:N], times = seq(0, 6*30*24*dt, dt), func = lorenz05, parms = parms3, 
             method = "rk4", verbose = TRUE, hini = dt)

t = 7*24
data = data.frame(hours = rep(0:t, each = N),
                  loc = rep(1:N, length(t)),
                  model2 = as.numeric(t(model2[1:(t+1),-1])),
                  model3 = as.numeric(t(model3[1:(t+1),-1])))
data$diff = data$model3 - data$model2
data$hour.label = NA
#data$hour.label[data$hours == 1] = "1 hour"
data$hour.label[data$hours == 6] = "6 hours"
data$hour.label[data$hours == 12] = "12 hours"
data$hour.label[data$hours == 24] = "1 day"
data$hour.label[data$hours == 48] = "2 days"
data$hour.label[data$hours == 3*24] = "3 days"
#data$hour.label[data$hours == 24*7] = "1 week"
data$hour.label = factor(data$hour.label, levels = na.omit(unique(data$hour.label)))
data = subset(data, !is.na(hour.label))

pdf(sprintf("%s/diff-model2-vs-model3.pdf", image.folder), 
    width = 5.5, height = 4.5, family = "CM Roman")
p = ggplot(data, aes(loc, diff)) +
      geom_hline(yintercept = 0, color = "lightgray") + geom_line() + 
      facet_grid(hour.label ~ ., scales = "free", space = "free") +
      #facet_grid(hour.label ~ .) +
      #ylim(range(model2, model3, model3-model2)) +
      scale_x_continuous(breaks = seq(0,960,60))
p = ggplot.setup(p) + theme(panel.border = element_blank(),
                            axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))
print(p)
dev.off()

# illustrate small- vs large-scale dynamics -----------------------------------
y.multiplier = 4
data = lapply(1:t, function(ti) {
  z = model3[ti,-1]
  xy = ZtoXY(z, parms3)
  data.frame(hours = ti-1, 
             loc = rep(1:N, 3),
             state = rep(c("x", "y", "z"), each = N),
             value = c(xy$X, xy$Y, z))
})
data = do.call(rbind, data)
data$hour.label = NA
#data$hour.label[data$hours == 1] = "1 hour"
data$hour.label[data$hours == 6] = "6 hours"
data$hour.label[data$hours == 12] = "12 hours"
data$hour.label[data$hours == 24] = "1 day"
data$hour.label[data$hours == 48] = "2 days"
data$hour.label[data$hours == 3*24] = "3 days"
#data$hour.label[data$hours == 24*7] = "1 week"
data$hour.label = factor(data$hour.label, levels = na.omit(unique(data$hour.label)))
data = subset(data, !is.na(hour.label))
data$state2 = data$state != "z"
if(y.multiplier != 1) {
  data$value[data$state == "y"] = y.multiplier*data$value[data$state == "y"]
  levels(data$state)[levels(data$state) == "y"] = "4y"
}

pdf(sprintf("%s/small-vs-large-dynamics.pdf", image.folder), 
    width = 11, height = 4.5, family = "CM Roman")
p = ggplot(data, aes(loc, value, color = state)) +
  geom_hline(yintercept = 0, color = "lightgray") + geom_line() + 
  facet_grid(hour.label ~ state2, scales = "free", space = "free") +
  #facet_grid(hour.label ~ .) +
  #ylim(range(model2, model3, model3-model2)) +
  scale_x_continuous(breaks = seq(0,960,60))
p = ggplot.setup(p) + theme(panel.border = element_blank(),
                            axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5),
                            strip.text.x = element_blank(),
                            legend.key = element_blank(),
                            legend.title.align = 1)
print(p)
dev.off()

# depict chaos with diff inits --------------------------------------------
#dt = (1/40)/3
#dt = .001
#dt = .005
i1 = ode(y = inits$perfect[1:N], times = seq(0, 10*24*dt, dt), func = lorenz05, parms = parms2, 
         method = "rk4", verbose = TRUE, hini = dt)

i2 = ode(y = inits$perfect[1:N] + runif(N), times = seq(0, 10*24*dt, dt), func = lorenz05, parms = parms2, 
         method = "rk4", verbose = TRUE, hini = dt)

t = 7*24
data = data.frame(hours = rep(0:t, each = N),
                  loc = rep(1:N, length(t)),
                  i1 = as.numeric(t(i1[1:(t+1),-1])),
                  i2 = as.numeric(t(i2[1:(t+1),-1])))
data$diff = data$i1 - data$i2
data$hour.label = NA
#data$hour.label[data$hours == 1] = "1 hour"
data$hour.label[data$hours == 6] = "6 hours"
data$hour.label[data$hours == 12] = "12 hours"
data$hour.label[data$hours == 24] = "1 day"
data$hour.label[data$hours == 48] = "2 days"
data$hour.label[data$hours == 3*24] = "3 days"
#data$hour.label[data$hours == 24*7] = "1 week"
data$hour.label = factor(data$hour.label, levels = na.omit(unique(data$hour.label)))
data = subset(data, !is.na(hour.label))

pdf(sprintf("%s/diff-ics.pdf", image.folder), 
    width = 5.5, height = 4.5, family = "CM Roman")
p = ggplot(data, aes(loc, diff)) +
  geom_hline(yintercept = 0, color = "lightgray") + geom_line() + 
  facet_grid(hour.label ~ ., scales = "free", space = "free") +
  #facet_grid(hour.label ~ .) +
  #ylim(range(model2, model3, model3-model2)) +
  scale_x_continuous(breaks = seq(0,960,40))
p = ggplot.setup(p) + theme(panel.border = element_blank(),
                            axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))
print(p)
dev.off()


t = 6
plot(i1[t+1,-1], type = "l", col = "darkgreen")
points(i2[t+1,-1], type = "l", col = "red")


