
setwd("~/data_assimilation/")
source("model_discrepancy/code/lorenz63/lorenz63.r")
image.folder = "~/data_assimilation/model_discrepancy/text/phd-thesis/intro/illustrations/lorenz63"
library(ggplot2)
library(grid)
library(gridExtra)
library(extrafont)
loadfonts()

multiplier = 3
parms = c(sigma = 10, rho = 28, beta = 8/3) # theta
times = seq(0, 150, by = 0.01)
labels = c(expression(italic(x)), expression(italic(y)), expression(italic(z)))
#labels = c(expression(italic(x)[1]), expression(italic(x)[2]), expression(italic(x)[3]))

plot.setup <- function(mat, theta = 40, phi = -5){
  par(mar = rep(0, 4))
  persp(z = matrix(0, nrow = 2, ncol = 2), xlim = range(mat[,1]),
        ylim = range(mat[,2]), zlim = range(mat[,3]),
        theta = theta, phi = phi, zlab = "Z") 
        #xlab = labels[1], ylab = labels[2], zlab = labels[3])
}

col.transparent = function(col, alpha = 100) {
  col.rgb = col2rgb(col)
  apply(col.rgb, 2, function(x) rgb(x[1], x[2], x[3], alpha, maxColorValue = 255))
}

ggplot.setup = function(p) {
  p = p + theme_bw() +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(), 
          panel.border = element_blank(),
          axis.line = element_line(colour = "black"),
          axis.ticks = element_blank(),
          axis.text = element_blank(),
          axis.title.x = element_text(size = 12),
          axis.title.y = element_text(size = 12, angle=0),
          plot.margin=unit(c(0,0,0,0),"mm"))
  return(p)
}

library(RColorBrewer)
col = brewer.pal(4, "Set1")[c(1,2,4)]
col = col.transparent(col, 90)
col.trans = col.transparent(col, 75)

# solutions --------------------------------------------------------------------
start1 = adv.model(y = rep(1,3), times = c(0,11), func = deriv, parms = parms)[2,2:4]
#start2 = start1
#start2[1] = round(start1[1], 10)
start2 = round(start1, 10)

sol1 = adv.model(y = start1, times = times, func = deriv, parms = parms)[,2:4]
sol2 = adv.model(y = start2, times = times, func = deriv, parms = parms)[,2:4]
solw = adv.model(y = start1, times = times, func = deriv2, parms = parms)[,2:4]
colnames(sol1) = c("x", "y", "z")
colnames(sol2) = c("x", "y", "z")
colnames(solw) = c("x", "y", "z")

# 3d illustration --------------------------------------------------------------
pmat <- plot.setup(rbind(sol1, sol2))
newcoords <- with(list(pmat = pmat), function(mat) { # For adding to the plot
  trans3d(x = mat[,1], y = mat[,2], z = mat[,3], pmat = pmat)
})

png(sprintf("%s/3d.png", image.folder), width = 3, height = 3, units = "in", res = 300)
plot.setup(sol1)
lines(newcoords(sol1), col = rgb(0,0,0,alpha=.25))
dev.off()

# 2d illustration --------------------------------------------------------------
combos = rbind(c(1,2,3), c(1,3,2), c(2,3,1))
combos = matrix(colnames(sol1)[combos], nrow = 3)
gt = vector("list", 3)

for(i in 1:3) {
  xlab.name = switch(combos[i,1], "x" = labels[1], "y" = labels[2], "z" = labels[3])
  ylab.name = switch(combos[i,2], "x" = labels[1], "y" = labels[2], "z" = labels[3])
  p = ggplot(data.frame(sol1[1:3000,]), aes_string(x = combos[i,1], y = combos[i,2], color = combos[i,3])) + 
    geom_path() + scale_colour_gradient(low = "cyan", high = "darkmagenta", guide = FALSE) +
    xlab(xlab.name) + ylab(ylab.name)
  p = ggplot.setup(p)
  gt[[i]] <- ggplot_gtable(ggplot_build(p))
}

pdf(sprintf("%s/2d.pdf", image.folder), width = 5.5, height = 5.5/3, family = "CM Roman")
grid.arrange(gt[[1]], gt[[2]], gt[[3]], ncol = 3)
dev.off()

# 1d illustration --------------------------------------------------------------
# http://stackoverflow.com/questions/27140058/add-colored-arrow-to-axis-of-ggplot2-partially-outside-plot-region
gt = vector("list", 3)
names(gt) = colnames(sol1)
ti = 3000

for(i in colnames(sol1)) {
  ylab.name = switch(i, "x" = labels[1], "y" = labels[2], "z" = labels[3])
  p = ggplot(data.frame(sol1[1:ti,], time = 1:ti), aes_string(x = "time", y = i)) + 
    geom_line() + xlim(1, ti) + ylab(ylab.name)
  p = p + annotate("segment", x=-Inf,xend=Inf,y=-Inf,yend=-Inf, arrow = arrow(angle = 10, length = unit(.1,"in")))
  if(i != "z") p = p + xlab("")
  p = ggplot.setup(p)
  #print(p)
  
  gt[[i]] <- ggplot_gtable(ggplot_build(p))
  gt[[i]][["heights"]][1] <- list(unit(0, "lines"))
  if(i != "z") gt[[i]][["heights"]][5] <- list(unit(0, "lines"))
  gt[[i]]$layout$clip[gt[[i]]$layout$name=="panel"] <- "off"
}

pdf(sprintf("%s/1d.pdf", image.folder), width = 2.5, height = 2.5, family = "CM Roman")
grid.arrange(gt[[1]], gt[[2]], gt[[3]], nrow = 3)
dev.off()

# different initial conditions: 3d illustration ------------------------------------------
# par(mar = rep(0, 4))
# pmat <- plot.setup(rbind(sol1, sol2))
# newcoords <- with(list(pmat = pmat), function(mat) { # For adding to the plot
#   trans3d(x = mat[,1], y = mat[,2], z = mat[,3], pmat = pmat)
# })
# 
# png(sprintf("%s/diff-3d.png")
# t = length(times)
# plot.setup(rbind(sol1,sol2))
# lines(newcoords(sol1[1:t,,drop = FALSE]), col = col.trans[1])
# #points(newcoords(sol1[t,,drop = FALSE]), col = col[1], pch = 15, cex = multiplier)
# lines(newcoords(sol2[1:t,,drop = FALSE]), col = col.trans[2])
# #points(newcoords(sol2[t,,drop = FALSE]), col = col[2], pch = 16, cex = multiplier)
# dev.off()

# diff initial conditions: 1d -------------------------------------------------------
gt = vector("list", 3)
names(gt) = colnames(sol1)
ti = 3000

for(i in colnames(sol1)) {
  ylab.name = switch(i, "x" = labels[1], "y" = labels[2], "z" = labels[3])
  p = ggplot(NULL, aes_string(x = "time", y = i)) + 
    geom_line(data = data.frame(sol2[1:ti,], time = 1:ti), col = "red") +
    geom_line(data = data.frame(sol1[1:ti,], time = 1:ti), col = "black") +
    xlim(1, ti) + ylab(ylab.name)
  p = p + annotate("segment", x=-Inf,xend=Inf,y=-Inf,yend=-Inf, arrow = arrow(angle = 10, length = unit(.1,"in")))
  if(i != "z") p = p + xlab("")
  p = ggplot.setup(p)
  #print(p)
  
  gt[[i]] <- ggplot_gtable(ggplot_build(p))
  gt[[i]][["heights"]][1] <- list(unit(0, "lines"))
  if(i != "z") gt[[i]][["heights"]][5] <- list(unit(0, "lines"))
  gt[[i]]$layout$clip[gt[[i]]$layout$name=="panel"] <- "off"
}

pdf(sprintf("%s/diff-1d.pdf", image.folder), width = 2.5, height = 2.5, family = "CM Roman")
grid.arrange(gt[[1]], gt[[2]], gt[[3]], nrow = 3)
dev.off()

