%!TEX root = ../thesis.tex
\synctex=1

%Suppose we wanted to predict temperature. First, we need a model of how the system evolves over time:
Suppose we want to learn about the \textit{state}\index{state} $X_t$ at time $t$. Here, we use the standard convention of denoting random variables with upper-case letters and fixed values with lower-case letters. We have that the state $X_t$ evolves in time as follows:
\stepcounter{equation}
\begin{equation}
  x_t = l_t(x_{t-1}, \eta_t)
    \text{ with density }
    f(x_t \,|\, x_{t-1}).
    \tag{{\theequation}a}
\label{eqn1:general-filter-state}
\end{equation}
\addtocounter{equation}{-1}%
This evolution of the system is called the \textit{state transition model}\index{state transition model}. The function $l_t: \mathbb{R}^{d_x} \to \mathbb{R}^{d_x}$ describes how the state $x_t$ of the system propagates forward in time: it is a function of the previous state $x_{t-1}$ and \textit{state disturbance}\index{state disturbance} $\eta_t$, where $d_x$ is the dimension\footnote{The state dimensions can vary with time. Without loss of generality, we assume that the dimensions do not change over time.} of the state $x_t$ for all $t$. Typically, the state disturbance $\eta_t$ will be additive\footnote{For many applications in this dissertation, the state disturbance is zero.} with respect to the previous state, e.g., $x_t = m_t(x_{t-1}) + \eta_t$. The form in \Cref{eqn1:general-filter-state} is used for greater generality, allowing $\eta_t$ be multiplicative with the previous state, for example. The probability density of the state transition model is called the \textit{state transition density} \index{transition density} (or transition density, for short) and is denoted by $f(x_{t} \,|\, x_{t-1})$. The \textit{initial state}\index{initial state} $X_0$ is sometimes called the \textit{initial condition(s)}\index{initial condition}. The states\index{state} $\{X_t: t = 1,...,T\}$ are assumed to form a \textit{Markov process}\index{Markov process}: the future state of the system is conditionally independent of the past states given the current state:
\begin{align*}
  X_t \indep X_{t'} \,|\, X_{t-1} \text{ for all } t' < t-1.
\end{align*}
This assumption is called the \textit{Markov assumption}\index{Markov assumption}. To help fix these ideas, we present an example.

\begin{example}[A simple temperature model]
Suppose we are interested in forecasting temperature at a particular location. A simple state transition model is to assume that the current day's temperature is a function of the previous day's temperature:
\begin{align*}
  x_t = x_{t-1} + \eta_t, \quad
  \eta_t \sim \normal(0, u_t^2),
\end{align*}
where $\eta_t$ is a random variable representing our theory that temperature increases or decreases relative to the temperature from the previous day $x_{t-1}$ with a certain magnitude (its distribution is called a \textit{prior}\index{prior}). Here $\normal(\mu, \sigma^2)$ denotes a normal random variable with mean $\mu$ and variance $\sigma^2$. The normal prior  says that we believe that the temperature can increase or decrease with equal probability (indicated by the mean zero) and with an increase or decrease by an amount less than $2 u_t$ with 95\% probability (indicated by the magnitude of the variance). Notice that our model satisfies the Markov assumption: the current day's temperature only depends on the previous day's temperature and is not a function of other days' temperatures. The transition density is:
\begin{align*}
  f(x_t \,|\, x_{t-1}) = \phi(x_t; x_{t-1}, u_t^2),
\end{align*}
where $\phi(x; \mu, \sigma^2)$ is the probability density of a normal random variable, with mean $\mu$ and variance $\sigma^2$, evaluated at $x$.
\label{ex:simple-temp}
\end{example}

Usually, the state cannot be measured directly\footnote{For this reason, the state is sometimes called a \textit{latent}\index{latent variable} or \textit{hidden variable}\index{hidden variable}.} and, even if it can be, the measurement has error. Let $Y_t$ be the \textit{measurement}\index{measurement} at time $t$ with the following model:
\begin{subequations}
\begin{equation}
  y_t = o_t(x_t, \epsilon_t)
    \text{ with density }
    g(y_t \,|\, x_t).
    \tag{{\theequation}b}
\label{eqn1:general-filter-obs}
\end{equation}
\label{eqn1:general-filter}%
\end{subequations}
This is called the \textit{measurement model}\index{measurement model}. The function $o_t: \mathbb{R}^{d_x} \to \mathbb{R}^{d_y}$ describes the mapping between the state and what is observed: it is a function of the current state $x_t$ and \textit{measurement error}\index{measurement error} $\epsilon_t$, where $d_y$ is the dimension\footnote{As with the state, the dimensions of the measurements can vary in time. Without loss of generality, we assume that the dimensions do not change over time.} of the measurement $y_t$ for all $t$. The probability density of the measurement model is called the \textit{measurement density}\index{measurement density} and denoted by $g(y_{t} \,|\, x_{t})$. The measurements depend only on the state, i.e., the measurements are conditionally independent of everything else given the state:
\begin{align*}
  Y_t \independent (X_{t'}, Y_{t'}) \,|\, X_t \text{ for all } t' \neq t;
\end{align*}
we call this the \textit{measurement independence assumption}\index{measurement independence assumption}. The set of equations in \Cref{eqn1:general-filter} forms a \textit{state-space model}\index{state-space model}. \Cref{fig1:graphical-model} is a graphical model that describes the conditional independence relationships in the state-space model we've just outlined. %Let's fix these ideas with a couple of examples.

% The initial state, state disturbances, and measurement errors %, $\{X_0, \eta_t, \eta_{t'}, \epsilon_t, \epsilon_{t'}, t \neq t'\}$,
% are mutually independent:
% \begin{gather*}
%   X_0 \indep \eta_t, \quad
%   X_0 \indep \epsilon_t, \\
%   \eta_t \indep \eta_{t'}, \quad
%   \epsilon_t \indep \epsilon_{t'}, \\
%   \eta_t \indep \epsilon_t, \quad
%   \eta_t \indep \epsilon_{t'} \\
%   \text{ for all } t \neq t'.
% \end{gather*}

\begin{figure}
\centering
\begin{tikzpicture}[node distance = 5em, auto]

% states
\node[circ, filled, label=90:$X_{0}$] (x0) at (0,0) {};
\node[circ, label=90:$X_{1}$, right of=x0] (x1) {};
\node[minimum size=2.5em, right of=x1] (xother) {...};
\node[circ, label=90:$X_{t-1}$, right of=xother] (xtm1) {};
\node[circ, label=90:$X_{t}$, right of=xtm1] (xt) {};
\node[circ, label=90:$X_{t+1}$, right of=xt] (xtp1) {};
\node[minimum size=2.5em, right of=xtp1] (xother2) {...};

% observations
\node[circ, label=270:$Y_1$, below of=x1] (y1) {};
\node[circ, label=270:$Y_{t-1}$, below of=xtm1] (ytm1) {};
\node[circ, label=270:$Y_t$, below of=xt] (yt) {};
\node[circ, label=270:$Y_{t+1}$, below of=xtp1] (ytp1) {};

% paths between states
\path[line] (x0) to node [above] {} (x1);
\path[line] (x1) to node [above] {} (xother);
\path[line] (xother) to node [above] {} (xtm1);
\path[line] (xtm1) to node [above] {} (xt);
\path[line] (xt) to node [above] {} (xtp1);
\path[line] (xtp1) to node [above] {} (xother2);

% paths between states and observations
\path[line] (x1) to node [right] {} (y1);
\path[line] (xtm1) to node [right] {} (ytm1);
\path[line] (xt) to node [right] {} (yt);
\path[line] (xtp1) to node [right] {} (ytp1);

\end{tikzpicture}
\captionnew{Graphical model of a state-space model with no measurements collected.}{The nodes $\{X_t: t \geq 1\}$ represent the states. The node $X_0$ is the initial condition and is filled in to represent that it is observed. The nodes $\{Y_t: t \geq 1\}$ represent the measurements and are not filled in because they have not yet been collected.}
\label{fig1:graphical-model}
\end{figure}

\addtocounter{example}{-1}
\begin{example}[A simple temperature model, continued]
The temperature can be measured with a thermometer but not without measurement error:
\begin{align*}
  y_t = x_t + \epsilon_t, \quad
  \epsilon_t \sim \normal(0, v_t^2),
\end{align*}
where $v_t^2$ quantifies the accuracy of the thermometer. Then, the observation density is:
\begin{align*}
  g(y_t \,|\, x_t) = \phi(y_t; x_t, v_t^2).
\end{align*}
\end{example}

\begin{example}[A simple precipitation model] \label{ex:simple-precipitation}
Consider modeling a slightly more complicated phenomenon: precipitation. Like temperature, a simple model of precipitation is that today's precipitation depends on yesterday's precipitation. However, unlike temperature, precipitation cannot take any value on the real line. Two measurements of precipitation are total inches rained rounded to the nearest tenth:
\begin{align*}
  y_t \sim \text{Pois}(\lambda_t),
\end{align*}
or more simply as whether or not it rained:
\begin{align*}
  y_t \sim \text{Bern}(p_t),
\end{align*}
where $\text{Pois}(\lambda)$ is a Poisson random variable with mean $\lambda$ and $\text{Bern}(p)$ is a Bernoulli random variable with mean $p$. These are measurement models and, unfortunately, do not describe how precipitation evolves over time.

A latent process can be used to describe an abstract notion of precipitation---a notion that is measured on a continuous scale and can be specified to be temporally correlated. For example, a latent process %\footnote{These particular latent processes have loose connection with generalized linear models (GLMs); see \citett{McCullagh1989,Dobson2008} for comprehensive reviews.}
for total inches rained is
\begin{align*}
  \log(\lambda_t) = \log(\lambda_{t-1}) + \eta_t, \quad
  \eta_t \sim \normal(0, u_t^2),
\end{align*}
and for whether or not it rained is
\begin{align*}
  \log \left( \frac{p_t}{1-p_t} \right) = \log \left( \frac{p_{t-1}}{1-p_{t-1}} \right) + \eta_t, \quad
  \eta_t \sim \normal(0, u_t^2),
\end{align*}
where $\eta_t$ is a random variable that describes our theory that the latent state of precipitation increases or decreases relative to the previous latent state, similar to the simple temperature model of \Cref{ex:simple-temp}.

Let's define the state-space model. For total inches rained, let $x_t \equiv \log(\lambda_t)$. Then, the state-space model is defined as
\begin{gather*}
  x_t = x_{t-1} + \eta_t, \\
  y_t \sim \text{Pois}(\exp(x_t)),
\end{gather*}
with state transition and observation densities
\begin{gather*}
  f(x_t \,|\, x_{t-1}) = \phi(x_t; x_{t-1}, u_t^2), \\
  g(y_t \,|\, x_t) = \frac{\exp(x_ty_t) \exp\exp(x_t)}{y_t!},
\end{gather*}
respectively, where $x!$ is the factorial of $x$. The state-space model for whether or not it rained can be similarly defined.
\end{example}

%Though these models are
Though the temperature and precipitation models discussed are simple and easy to understand, it is difficult to believe that weather dynamics %, such as temperature and precipitation,
%are simply a linear function of dynamics from a previous time \todo{
are simply an unbiased random walk. Weather is a complex phenomenon that depends on many factors, such as the amount of sun that reaches the atmosphere or the amount of aerosols and carbon dioxides in the system, and often these relationships are nonlinear. Furthermore, these simple models cannot be used to describe global, or even local, dynamics: weather is temporally \textit{and} spatially related, e.g., the temperature at one location is highly correlated with the temperature at nearby locations.

Scientists and engineers from a wide variety of fields, including but not limited to the atmospheric sciences, often describe and test their understanding of scientific phenomena with mathematical models. %\todo{Give examples of how mathematical models describe scientific phenomena? Robotics, ecology, ?}.
These mathematical models describe how scientific phenomena (the state) evolve over time and, in turn, can be used to predict (or, \textit{forecast}\index{forecast}) the state at a future timepoint. Even though many mathematical models are deterministic, these deterministic models may not be predictable---a quality called \textit{chaos}\index{chaos}. A system is called chaotic when two trajectories that started infinitesimally close to each other diverge quickly over a short amount of time. %Many nonlinear models are choatic\todo{Stein?}.

The consequence of chaos is that these mathematical models cannot forecast phenomenon far in the future. When initializing  mathematical models with two different, but close, initial conditions that represent reality, chaos will drive the two trajectories to two very different states. There needs to be a mechanism to correct the trajectories predicted by the mathematical models with reality and that mechanism is to \textit{update}\index{update} these imperfect forecasts with measurements. Measurements quantify the scientific phenomena that scientists or engineers are interested in capturing from reality, such as measuring temperature using thermometers or measuring rainfall with a ruler. Not only are these measurements subject to measurement error, it is impossible to measure desired quantities at every spatial location and timepoint that is desired. For example, the National Oceanic and Atmospheric Administration (NOAA) ingests meteorological data at approximately 36,000 locations in the contiguous United States \citep{MADIS2014}---that is, each station covers more than 80 square miles if the stations were evenly distributed\footnote{Value derived from dividing the area of the contiguous United States by the number of stations.}! This iterative procedure of \textit{forecasting}\index{forecasting} with a mathematical model and \textit{updating}\index{updating} as an observation is collected is called \textit{filtering}\index{filtering} (or, \textit{data assimilation}\index{data assimilation} within the atmospheric science community).

The Kalman filter \citep{Kalman1960,Bucy1961} is the first major contribution to the filtering literature: it is used for linear state transitions and measurement models with additive Gaussian errors. Since then, many filtering algorithms have been introduced to solve a wide variety of state-space models. In particular, to overcome some of the strict assumptions of linearity and Gaussian errors, linear approximations have been proposed to extend the usefulness of the Kalman filter. However, these approximations can introduce large errors that further contribute to the chaos problem when filtering. Thanks to the pioneering work of \citet{Gordon1993} on \glspl{pf}\index{particle filter}, the field of \gls{smc}\index{sequential Monte Carlo} was developed in response: these methods employ importance sampling to approximate interested quantities of the states. Though there are many filtering methods to choose from, filtering remains difficult for high-dimensional systems, such as the weather models that atmospheric scientists are interested in studying. For example, a computationally infeasible amount of samples is required by \gls{smc} methods to properly capture the dynamics of the system.

%Not only is filtering with high-dimensional state spaces difficult, the mathematical models are often able to capture large-scale dynamics but have a more difficult time capturing smaller-scale dynamics.
Not only is filtering with high-dimensional state-spaces difficult, we are seldom able to perfectly capture the true dynamics of a system, especially complex systems such as the ones governing the atmosphere.
%This is especially true in the atmospheric sciences.
We call this inability to capture the true dynamics of a system \textit{model error}\index{model error}. Sometimes, model error is due to imperfect knowledge of the system. Other times, it comes from introducing approximations that are not necessarily believed to be true but are required for computational reasons. Model error is a common problem that can further contribute to the issues posed by chaos. For example, the inability to capture certain dynamics of a system can lead to two very different forecasts. In fact, model error is the problem we initially sought to improve that led to our other contributions to the filtering literature that are described in this dissertation.

Before discussing filtering and model error, we introduce a few state-space models. Although the algorithms discussed and developed in this dissertation can be applied to any model that satisfy the constraints and assumptions of the state-space models outlined, we focus on atmospheric science applications. For this reason, we introduce state transition and measurement models often used in the atmospheric science literature to motivate the filtering algorithms introduced in this dissertation. As we discuss these models, we motivate some of the difficulties of filtering with atmospheric models. In \Cref{sec:outline}, we summarize these difficulties and introduce how we address them in the dissertation.

