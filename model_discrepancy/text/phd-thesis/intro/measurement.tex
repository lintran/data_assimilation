%!TEX root = ../thesis.tex
\synctex=1

\begin{figure}
  %\vspace{-.25in}
  \centering
  \begin{subfigure}{0.4\textwidth}
    \includegraphics{intro/illustrations/latitude-band-obs-all}
  \captionnew{Fully observed system.}{}
  \label{fig1:latitude-grid-obs-all}
  \end{subfigure}
  ~
  \begin{subfigure}{0.4\textwidth}
    \includegraphics{intro/illustrations/latitude-band-obs-rand}
  \captionnew{Partially observed system.}{}
  \label{fig1:latitude-grid-obs-rand}
  \end{subfigure}
\captionnew{Mathematical grid on a latitude band with the locations of the measurements.}{Similar to \Cref{fig1:latitude-grid}, let $r_i$ and $s_j$ be the locations of the state and measurement of the $i$th and $j$th elements, respectively, of the $d_x$-vector $x_n$ and $d_y$-vector $y_n$. In this figure, the state has size $d_x = 10$ and the measurement has sizes $d_y = 10$ in the fully observed system (a) and $d_y = 5$ in the partially observed system (b). The tickmarks represent locations of the state and the dots represent the locations of the measurements.}
\label{fig1:latitude-grid-obs}
\end{figure}
There are a wide variety of measurement models, as simple as the temperature model from \Cref{ex:simple-temp} or as complicated as the precipitation model from \Cref{ex:simple-precipitation}. In the atmospheric sciences, measurement models  tend to be a linear interpolation from the mathematical grid to the spatial resolution of the measurements, usually with Gaussian errors. For this reason, we focus on this particular measurement model in this dissertation.

Suppose that measurements of the systems described in \Cref{sec1:state-transition-examples} are collected at discrete timepoints $t_n$. Let $y_{t_n}$ be the vector of observations at timepoint $t_n$. Then, a linear measurement model\index{measurement model!linear} is
\begin{align*}
  y_{t_n} = H_{t_n} x_{t_n} + \epsilon_{t_n}, \quad \epsilon_{t_n} \sim \normal(0, V_{t_n}),
%\tag{{\theequation}c}
\end{align*}
where $\epsilon_{t_n}$ represents the measurement error with mean zero and $d_y \times d_y$ variance matrix $V_{t_n}$.
The measurement density is then $g(y_{t_n} \,|\, x_{t_n}) = \phi[y_{t_n}; H_{t_n} x_{t_n}, V_{t_n}]$. If measurements of the state variable $x_{t_n}$ are exactly collected at locations on the mathematical grid, $H_{t_n}$ is simply the $d_x \times d_x$ identity matrix\index{identity matrix} $I_{d_x}$. We call this system \textit{fully observed}\index{fully observed}. This is rarely the case---measurements are collected where equipment is placed, which has no bearing on the mathematical grid used to numerically integrate the state transition model. In this case, $H_{t_n}$ is a $d_y \times d_x$ matrix that linearly interpolates the state to the measurements. In other words, $H_{t_n}$ is a weighting matrix that places higher weights to parts of the state that are spatially closer to the measurement. We call this system \textit{partially observed}\index{partially observed}.

\begin{example}[Lorenz 1963: measurement models]
  If the state in the Lorenz 1963 model is fully observed, then all parts of the state, i.e., the $x$-, $y$-, and $z$-directions, are observed. If only the $x$- and $y$-directions are observed, then the system is partially observed and the linear mapping is:
  \begin{align*}
    H_{t_n} = \begin{bmatrix}
      1 & 0 & 0 \\
      0 & 1 & 0
    \end{bmatrix}.
  \end{align*}
  If the location at which the measurement is collected is exactly halfway between the locations of the $x$- and $y$-directions by some distance metric, then this system is also partially observed with the following linear mapping:
  \begin{align*}
    H_{t_n} = \begin{bmatrix}
      \frac{1}{2} & \frac{1}{2} & 0
    \end{bmatrix}.
  \end{align*}
\end{example}

\begin{example}[Lorenz 2005: measurement models]
  With the same latitude band from before, we illustrate fully observed and partially observed systems in \Cref{fig1:latitude-grid-obs}. In a fully observed system, similar to \Cref{fig1:latitude-grid-obs-all}, $H_{t_n}$ is the identity matrix $I_{d_x}$. In a partially observed system, similar to \Cref{fig1:latitude-grid-obs-rand}, each row of $H_{t_n}$ sums to one with higher weights places on parts of the state that are closer to the measurement.
\end{example}

\begin{comment}
If the system is partially observed, $H_{t_n}$ is a weighting matrix to linear interpolate values from the locations of the mathematical grid to the locations of the observations. Let $r_{t_n,i}$ be the coordinates associated with the $i$th index of the vector $x_{t_n}$, i.e., $x_{t_n,i}$, for $i = 1,...,d_{t_n}^x$ and, similarly, $s_{t_n,j}$ be the coordinates associated with the $j$th index of $y_{t_n,j}$ for $j = 1,...,d_{t_n}^y$.
% \begin{align*}
%   y_{t_n,i} = \frac{\sum_{j=1}^{d_x} K_\lambda(|s_i - r_j|) x_{t_n,j}}{\sum_{j=1}^{d_x} K_\lambda(|s_i - r_j|)},
% \end{align*}
Then, the $ij$th component of the matrix $H_{t_n}$ is calculated as
\begin{align*}
  H_{t_n,ij} = \frac{K_h(|s_{t_n,i} - r_{t_n,j}|)}{\sum_{k=1}^{d_{t_n}^x} K_h(|s_{t_n,i} - r_{t_n,k}|)},
\end{align*}
where $K_h$ is a function of distance $d$ with parameter $h$ and $K_h(d) \to 0$ as $d \to \infty$. For its prevalence in the atmopsheric science literature, we will use the Gaspari-Cohn 5th-order polynomial function with halfwidth $h$ \citep[Equation 4.10]{Gaspari1999}:
\begin{align*}
  K_{h}(d) =
  \begin{cases}
    -\frac{1}{4}(\frac{d}{h})^5 + \frac{1}{2}(\frac{d}{h})^4 + \frac{5}{8} (\frac{d}{h})^3 - \frac{5}{3} (\frac{d}{h})^2 + 1, \quad
      & 0 \leq d \leq h, \\
    \frac{1}{12}(\frac{d}{h})^5 - \frac{1}{2}(\frac{d}{h})^4 + \frac{5}{8}(\frac{d}{h})^3 + \frac{5}{3}(\frac{d}{h})^2 - 5(\frac{d}{h}) + 4 - \frac{2}{3} \frac{h}{d},
      & h \leq d \leq 2h, \\
    0
      & d \geq 2h.
  \end{cases}
\end{align*}
\end{comment}
