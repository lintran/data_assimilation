\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amsfonts,amsthm,amssymb,amsthm}
\usepackage[authoryear, round]{natbib}
\usepackage{outlines}
\usepackage{enumitem}
\usepackage{soul,color}
\usepackage{cleveref}
\usepackage{ulem}
\usepackage[textsize=footnotesize,textwidth=1.25in]{todonotes}
%\usepackage[disable]{todonotes}
\reversemarginpar
\setlength{\marginparwidth}{2cm}

\setenumerate[1]{label=\Roman*.}
\setenumerate[2]{label=\arabic*.}
\setenumerate[3]{label*=\arabic*.}
\setenumerate[4]{label*=\arabic*.}
\setenumerate[5]{label*=\arabic*.}

\DeclareMathOperator{\normal}{\mathcal{N}}
\DeclareMathOperator{\e}{\mathbb{E}}
\DeclareMathOperator{\cov}{\mathbb{C}ov}
\DeclareMathOperator{\var}{\mathbb{V}ar}
\DeclareMathOperator{\prob}{\mathbb{P}r}
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\newcounter{update}
\newenvironment{myenum}{
  \begin{list}{(\arabic{update})}{
    \usecounter{update}
    %\setlength{\leftmargin}{0pt}
    \setlength{\itemsep}{0pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}
    %\setlength{\partopsep}{0pt}
    \setlength{\topsep}{0pt}
    }
}{
  \end{list}
  \vspace{.25em}
}

\title{Forecasting in high-dimensional state-spaces with model error}
\author{Linda N. Tran}

\begin{document}
\maketitle
\begin{outline}[enumerate]
  \1 {\bf Introduction}:
  %Why data assimilation is interesting to the atmospheric science community? \hl{ help with applications?} (no equations!) Discuss why it's a difficult problem (high-dimensional, model error, etc.). Schematic (graphical model) of data assimilation.
  %ontological model (or physical model) \\
  %epistematic model (or numerical model)
  I will introduce data assimilation applications and discuss why data assimilation is particularly difficult in the atmospheric sciences, i.e.:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
    \item atmospheric models are chaotic
    \item the state space is high-dimensional
    \item state transition density is usually degenerate
    \item don't have the ontological model, i.e. there's model error
  \end{itemize}
  Thesis:
  \begin{itemize}[leftmargin=*,noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
    \item Part II: I will introduce the audience to data assimilation, focusing on the ensemble Kalman filter and the bootstrap filter. I will discuss the problems with both and introduce a filter that attempts to combine the best qualities of both filters. In the case study, I will show what happens when one filters with the ``wrong'' model, which will provide foreshadowing for Part IV.
    \item Part III: I will introduce methodology to estimate static parameters in state-space models that uses EnKF to update the states and PF to update the parameters. I will motivate why this is better than state augmentation. The methodology can be used for the next part.
    \item Part IV: I will introduce a low-rank linear correction of the numerical model to correct for model error. I will motivate that the linear correction works better when ``truth'' can be evaluated and show that it improves forecasts.
  \end{itemize}

    \2 {\bf Case studies}:
    Introduce the case studies being used in the dissertation.
      \3 \citet{Lorenz2005}: This model was specifically developed for studying model error in data assimilation -- there is a ``wrong'' model that only captures the large-scale dynamics of the ``right'' model.
      \3 If time permits, B-grid model; else, Ikeda model.
    \2 {\bf Evaluation metrics}:
    Since we mostly care about forecasting, I will evaluate the improvements with metrics that capture prediction error. Introduce the metrics here.
      % \3 MAE, RMSE
      % \3 Coverage
      % \3 Other

  \1 {\bf Filtering} \label{item:filtering}
    \2 {\bf Optimal filtering / What is filtering?}
    \begin{equation}
    \begin{array}{r@{\;}l@{\;}l c c}
      x_t &=& m_t(x_{t-1}, \eta_t)
        &\text{with transition density}
        &f(x_t \,|\, x_{t-1}) \\
      y_t &=& o_t(x_t, \epsilon_t)
        &\text{with observation density}
        & g(y_t \,|\, x_t)
    \end{array}
    \label{eqn:filter-general}
    \end{equation}
    (Discuss independence assumptions: $x_t \independent (x_{t'}, y_{t'}) \,|\, x_{t-1}$ for all $t' < t-1$, $y_t \independent (x_{t'}, y_{t'}) \,|\, x_t$ for all $t' \neq t$, $\{x_0, \eta_1, ..., \eta_T, \epsilon_1, ..., \epsilon_T\}$ are mutually independent.) For this dissertation, we focus on forecasting:
    \begin{align*}
      p(x_t \,|\, y_{1:t-1}) = \int f(x_t \,|\, x_{t-1}) p(x_{t-1} \,|\, y_{1:t-1}) dx_{t-1}
    \end{align*}
    To forecast, the analysis distribution is needed:
    \begin{align}
      p(x_t \,|\, y_{1:t})
        = \frac{g(y_t \,|\, x_t) p(x_t \,|\, y_{1:t-1})}{\int g(y_t \,|\, x_t) p(x_t \,|\, y_{1:t-1}) dx_t}
        = \frac{g(y_t \,|\, x_t) p(x_t \,|\, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})}
      \label{eqn:update}
    \end{align}

      \3 {\bf Kalman filter (KF)}:
      When the state transition and measurement models are linear in the state and the noise is additive and Gaussian, there is an analytic solution.
      \begin{equation*}
      \begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l c@{\quad\text{with}\quad} r@{\;}l@{\;}l}
        x_t &=& A_t x_{t-1} + \eta_t,
          &\eta_t &\sim& \normal(0, \Gamma_t) &
          &f(x_t \,|\, x_{t-1}) &=& \phi(dx_t; A_t x_{t-1}, \Gamma_t)\\
        y_t &=& B_t x_t + \epsilon_t,
          &\epsilon_t &\sim& \normal(0, \Sigma_t) &
          &g(y_t \,|\, x_t) &=& \phi(dy_t; H_t x_t, \Sigma_t)
      \end{array}
      \end{equation*}
      This filtering problem is called the Kalman filter \todo{ref} and is one of the rare situations where there is an analytic solution.

    \2 {\bf Filtering with nonlinear systems}
      \3 {\bf Extensions of the Kalman filter}:
        The following class of algorithms solves for
        \begin{equation*}
        \begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l}
          x_t &=& a_t(x_{t-1}) + \eta_t,
            &\eta_t &\sim& \normal(0, \Gamma_t) \\
          y_t &=& b_t(x_t) + \epsilon_t,
            &\epsilon_t &\sim& \normal(0, \Sigma_t)
        \end{array}
        \end{equation*}
        where $a_t(\cdot), b_t(\cdot)$ are nonlinear.
        \4 {\bf Extended Kalman filter (EKF)}:
        Uses a linear approximation of $a_t(\cdot)$ and $b_t(\cdot)$ ($a_t(\cdot) \approx A_t, b_t(\cdot) \approx B_t$), e.g. the Jacobian (second term of its Taylor expansion). \todo{refs}
        \4 {\bf Ensemble Kalman filter (EnKF)}:
        The linearization was shown to not work well in many applications due to an unbounded variance growth from neglecting higher terms \citep{Evensen1994,Evensen2003}, so the EnKF was developed in response \citep{Evensen1994,Houtekamer1998,Tippett2003}. Since the Gaussian distribution is defined by its first two moments, the EnKF uses samples to estimate the first two moments.

      \3 {\bf Particle filters (PF)}:
      Particle filters are a general class of algorithms that can be used to directly solve \cref{eqn:filter-general} via sequential importance sampling.
      %Bootstrap filters (BF) is the original particle filter; other filters stem from the BF.
      Follow derivation of the bootstrap filter (BF) from \citet{Crisan2002}.

      \3 {\bf BF vs EnKF}:
      I will summarize the BF and EnKF with a table comparing the algorithms and discuss the pros and cons of each algorithm. BF (and its cousins) has theoretical underpinnings, but has problems with high-dimensional state spaces \citep{Bengtsson2008} and when the state transition density is degenerate. EnKF performs well with high-dimensional state spaces, but is not guaranteed to converge to the optimal distribution. \citet{Lei2010} proved that the EnKF is not robust for non-Gaussian systems. In the next chapter, I introduce a filter that attempts to combine the best qualities of both filters. \label{item:bf-vs-enkf}

    \2 {\bf Gaussian regularized particle filter (gRPF)}\\
    In this chapter, we will focus on the following filtering problem:
    \begin{equation*}
      \begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l}
        x_t &=& a_t(x_{t-1}) + \eta_t,
          &\eta_t &\sim& \normal(0, \Gamma_t) \\
        y_t &=& B_t x_t + \epsilon_t,
          &\epsilon_t &\sim& \normal(0, \Sigma_t)
      \end{array}
      \end{equation*}
      \3 {\bf A re-interpretation of EnKF}:
      Rewrite the EnKF algorithm to make it clear how it's related to the BF. Discuss how EnKF is related to the BF and the theorem behind the idea (proof in appendix).
      \3 {\bf gRPF}:
      What needs to change in the EnKF to make it a proper PF? This gives the gRPF. Show table from previous chapter with new algorithm in the middle for comparison. Discuss the effects of variance inflation and perturbed observations. \todo{discuss your hypothesis as to why both are needed}
      \3 {\bf Related filters}
        \4 The gRPF is a special case of the regularized particle filter \citep{Hurzeler1998,Musso2001} when the measurement model the same as in the Kalman filter and Gaussian bandwidths are used. RPF has theoretical underpinnings and is known to be biased.
        \4 Kernel filter \citep{Anderson1999} is a special case of the gRPF: bandwidth is a tapered sample covariance matrix. Doesn't work well with high-dimesional state spaces. \todo{Discuss how it's probably because the bandwidth was ``too small'', reverting back to the BF.}
        \4 Nonlinear ensemble filter \citep{Bengtsson2003} is also a special case of the gRPF: each sample has its own distinct bandwidth using a nearest neighbor approach.

    \2 {\bf Case studies}\todo{expand}: In this chapter, I will explore and compare the gRPF against the other filters in the literature using Lorenz 2005 and another high-dimensional state-space model. In particular, I will show what happens with the various filters when filtering with the right vs wrong model, which will foreshadow the model error part of thesis (Part IV). With Lorenz 2005, I expect the following to happen: (1) there will be filter divergence with EnKF if no variance inflation is used; (2) gRPF will be robust to model error. \label{item:filtering-with-wrong-model}
      % \3 APF vs EnKF vs \hl{new filter} vs related filters
      % \3 Increasing $\Delta t_n$ (more nonlinear)
      % \3 Explore where the \hl{new filter} breaks (examine MAE against dimension and particle size)
      % \3 Variance of the log likelihoods by number of particles
      % \3 Wrong vs right model. This foreshadows the model error part of thesis. With Lorenz 2005, I expect the following to happen: (1) there will be filter divergence with EnKF if no variance inflation is used; (2) RPF will be robust to model error. \label{item:filtering-with-wrong-model}

  \1 {\bf Parameter estimation with the EnKF}:
  In the EnKF literature, methods have been developed to estimate an unknown parameter in the variance of the measurement model \citep{Stroud2007,Delsole2010,Frei2012}. Though these methods work well, they cannot be used to estimate other parameters, such as parameters of the state transition model. State augmentation \todo{refs} can be used to estimate other parameters but the update of the parameters relies on the correlation of the state and parameters, which is problematic if the states and parameters are not linearly related. I will develop methods to estimate static parameters in a state-space model that allows one to use the familiar EnKF to filter the (often) high-dimensional states and PF to filter the parameters.

  Consider the following filtering problem:
  \label{item:param-enkf}
  \begin{equation*}
  \begin{array}{r@{\;}l@{\;}l r@{\;}l@{\;}l@{\quad}c@{\quad}c}
    x_t &=& a_t(x_{t-1}, \Theta_x) + \eta_t,
      &\eta_t &\sim& \normal(0, \Gamma_t(\Theta_x))
      &\text{with transition density}
      &f(x_t \,|\, x_{t-1}, \theta_x)\\
    y_t &=& b_t(x_t, \Theta_y) + \epsilon_t,
      &\epsilon_t &\sim& \normal(0, \Sigma_t(\Theta_y))
      &\text{with observation density}
      & g(y_t \,|\, x_t, \theta_y)
  \end{array}
  \end{equation*}
  (Explain notation.) Define $\Theta = (\Theta_x, \Theta_y)$. The goal is to estimate $\Theta$.

    \2 {\bf State augmentation}: In this section, I will discuss (and demonstrate?) why state augmentation is not sufficient to estimate parameters. First, I will derive the parameter update under the Kalman filter and show that the parameter update relies on the forecast covariance of the state and parameter, which can be problematic if the state and parameter are not linearly related.

    \2 {\bf Iterated filtering}:
    There are a host of algorithms to estimate parameters with the PF (see \citet{Kantas2015} for a comprehensive review), but most of them require the user to have a closed-form expression for the state transition density, which is often unattainable in the atmospheric sciences (discuss). Two algorithms in the PF literature that do not require the state transition density are: particle marginal Metropolis-Hastings (PMMH) \citep{Andrieu2010} and iterated filtering (IF) \citep{Ionides2006,Ionides2015}. IF is less computationally expensive than PMMH\todo{ref}, so we will focus on IF to estimate parameters.

    Iterated filtering is a class of algorithms to estimate a static parameter $\Theta$ by replacing
    \begin{equation*}
      f(x_t \,|\, x_{t-1}, \theta), g(y_t \,|\, x_t, \theta), \text{ and } f(x_0 \,|\, \theta)
    \end{equation*}
    with closely related densities
    \begin{equation}
        f(x_t \,|\, x_{t-1}, \theta_t), g(y_t \,|\, x_t, \theta_t), \text{ and } f(x_0 \,|\, \theta_0), \label{eqn:new-dens}
    \end{equation}
    where $\Theta_t$ is parametrized as a random walk with
    \begin{equation*}
    \begin{array}{ll}
        \e(\Theta_t \,|\, \Theta_{t-1}) = \Theta_{t-1}&
        \var(\Theta_t \,|\, \Theta_{t-1}) = \sigma_b^2 \Psi \\
        \e(\Theta_0) = \Theta&
        \var(\Theta_0) = \tau_b^2 \Psi,
    \end{array}
    \end{equation*}
    $b$ is an IF iteration, and $\sigma_b^2, \tau_b^2 \to 0$ as $b \to 0$. Gaussian distributions satisfies the constraints of the random walk, but there are other distributions that satisfy the above constraints, such as the truncated normal distribution. Alternatively, parameters can be transformed to allow the usage of a Gaussian distribution.

    The first iterated IF algorithm (IF1) was developed to estimate the maximum likelihood estimator \citep{Ionides2006}. Since then, it has been proven that the mean and covariance of the posterior samples of $\Theta_t$ converge to the score vector and information matrix \todo{figure out if it converges as the number of particles or IF iterations goes to infinity, or both} \citep{Ionides2011,Doucet2013}. Very recently, \citet{Ionides2015} proposed a slight modification to IF1 (IF2) that estimates a Bayes maximum a posteriori probability (MAP) estimate. They also demonstrated that the algorithm is more efficient at finding the global maximum than IF1. For its efficiency, we will focus on reviewing IF2. (Review IF2.)

    \2 {\bf EnKF for states, PF for parameters}: Before discussing how to integrate EnKF into IF algorithms, we develop algorithms that use EnKF to update states and PF to update the parameters with the densities in \cref{eqn:new-dens}.
      \3 {\bf Forecasting}: The joint forecast distribution can be factorized as:
      \begin{align}
        p(x_t, \theta_t \,|\, y_{1:t-1}) = p(x_t \,|\, \theta_t, y_{1:t-1}) p(\theta_t \,|\, y_{1:t-1})
        \label{eqn:forecast}
      \end{align}

      The above factorization suggests a sequential forecasting algorithm:

      \ul{Forecasting algorithm}: Have samples $(x_{t-1}^{a(i)}, \theta_{t-1}^{a(i)}) \sim X_{t-1}, \Theta_{t-1} \,|\, y_{1:t-1}$
      \begin{myenum}%[label=(\arabic*),leftmargin=*,noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
        \item Sampling from $\Theta_t \,|\, y_{1:t-1}$: Sample $\theta_t^{f(i)} \sim \Theta_t \,|\, \theta_{t-1}^{a(i)}$
        \item Sampling from $X_t \,|\, \Theta_t, y_{1:t-1}$: Sample $x_t^{f(i)} \sim X_t \,|\, x_{t-1}^{a(i)}, \theta_t^{f(i)}$
      \end{myenum}
      The above algorithm provides conditional samples from $X_t \,|\, \Theta_t, y_{1:t-1}$ and $\Theta_t \,|\, y_{1:t-1}$; together, they are also samples from the joint distribution $X_t, \Theta_t \,|\, y_{1:t-1}$.

      \3 {\bf Updating}: With \cref{eqn:update,eqn:forecast}, the analysis distribution is
      \begin{align*}
        p(x_t, \theta_t \,|\, y_{1:t})
          &= \frac{g(y_t \,|\, x_t) p(x_t \,|\, \theta_t, y_{1:t-1}) p(\theta_t \,|\, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})}
          %\label{eqn:update}
      \end{align*}
      In this chapter, we explore equivalent forms of this factorization and develop multiple algorithms to sample from $X_t, \Theta_t \,|\, y_{1:t}$.

      \4 {\bf When $\Theta_t^y$ is known}:
      Suppose $\Theta_t^y$ is known. Then, the likelihood does not involve $\Theta_t$. We have
      \begin{align*}
        p(x_t, \theta_t \,|\, y_{1:t})
          &= \underbrace{\frac{g(y_t \,|\, x_t) p(x_t \,|\, \theta_t, y_{1:t-1})}{p(y_t \,|\, \theta_t, y_{1:t-1})}}_{p(x_t \,|\, \theta_t, y_{1:t})}
            \times
            \underbrace{\frac{p(y_t \,|\, \theta_t, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})} p(\theta_t \,|\, y_{1:t-1})}_{p(\theta_t \,|\, y_{1:t})}
      \end{align*}
      The above factorization shows that the first term can be used to update the state and the second term to update the parameters, as long as $p(y_t \,|\, \theta_t, y_{1:t-1})$ can be evaluated. Fortunately, $p(y_t \,|\, \theta_t, y_{1:t-1})$ does have an analytic form under the EnKF formulation.

      One algorithm that respects the above factorization is as follows:

      \ul{Algorithm}: Have samples $(x_{t-1}^{a(i)}, \theta_{t-1}^{a(i)}) \sim X_{t-1}, \Theta_{t-1} \,|\, y_{1:t-1}$
      \begin{myenum}
        \item Forecast: Sample $(x_t^{f(i)}, \theta_t^{f(i)}) \sim X_t, \Theta_t \,|\, y_{1:t-1}$.
        \item Update $\Theta_t^x$: Calculate and normalize weights, $w_t^{(i)} \propto p(y_t \,|\, \theta_t^{f(i)}, y_{1:t-1})$.
        \item Update $X_t$: Use EnKF with $\{x_t^{f(i)}\}_{n=1}^N$ to update the state, obtaining samples $\{\tilde{x}_t^{a(i)}\}_{n=1}^N$.
        \item Resample: Sample $(x_t^{a(i)}, \theta_t^{a(i)})$ from $\{(w_t^{(i)}, \tilde{x}_t^{a(i)},\theta_t^{f(i)})\}_{i=1}^N$.
      \end{myenum}

      The above algorithm is inefficient due to the resampling step introduced by the PF update of the parameters. The EnKF provides equally-weighted samples of the state after its update step but, in the above algorithm, the state samples have to be resampled due to the unequal weights introduced by the PF update of the parameters. (Show equation that demonstrates this.) Ideally, resampling should be avoided after an EnKF update and we formulate another algorithm that does just that:

      \ul{Algorithm}: Have samples $(x_{t-1}^{a(i)}, \theta_{t-1}^{a(i)}) \sim X_{t-1}, \Theta_{t-1} \,|\, y_{1:t-1}$
      \begin{myenum}
        \item First-stage update: Sample $(\tilde{x}_t^{f(i)}, \theta_t^{f(i)}) \sim X_t, \Theta_t \,|\, y_{1:t-1}$. Calculate and normalize weights, $w_t^{(i)} \propto p(y_t \,|\, \theta_t^{f(i)}, y_{1:t-1})$. Resample $\theta_t^{a(i)}$ from $\{(w_t^{(i)}, \theta_t^{f(i)})\}_{i=1}^N$.
        \item Second-stage update: Sample $x_t^{f(i)} \sim X_t \,|\, \theta_t^{a(i)}, y_{1:t-1}$. Use EnKF with $\{x_t^{f(i)}\}_{n=1}^N$ to update the state, obtaining samples $\{(x_t^{a(i)}, \theta_t^{a(i)})\}_{n=1}^N$.
      \end{myenum}

      Notice that the above algorithm does a two-stage update reminiscent of APF. In APF, the first-stage pre-samples state trajectories that are more likely to survive, which introduces a bias. The bias is corrected in the second-stage by adjusting the weights and then resampling. (Refer to the appendix on APF for more details.) Like APF, the first-stage of the above algorithm pre-samples parameters that are more likely to survive. Furthermore, it obtains an equally-weighted, but biased, sample from $\Theta_{t} \,|\, y_{1:t}$ and this is a key step in avoiding resampling after the EnKF update. The EnKF in the second-stage serves two purposes: (1) it updates the state and (2) it corrects for the bias introduced in the first-stage, therefore providing equally-weighted samples from $X_t, \Theta_t \,|\, y_{1:t}$\footnote{This assumes that the empirical distribution constructed with samples from EnKF converges to the optimal distribution as the number of ensemble members goes to infinity. This has only been proven for linear systems \citep{LeGland2009,Mandel2009,Kwiatkowski2014} and has not been proven for nonlinear systems.}. Notice that the algorithm requires two evaluations of the state transition per ensemble member, increasing the computational time of the algorithm.

      \4 {\bf With different Kalman gains}:
      We have
      \begin{align*}
        p(x_t, \theta_t \,|\, y_{1:t})
          &= \frac{g(y_t \,|\, x_t, \theta_t^y) p(x_t \,|\, \theta_t^x, y_{1:t-1})}{p(y_t \,|\, \theta_t, y_{1:t-1})}
            \times
            \frac{p(y_t \,|\, \theta_t, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})} p(\theta_t \,|\, y_{1:t-1})
      \end{align*}
      Using this factorization of the update equation, the two-stage algorithm from the previous section can also be used to provide samples from $X_t, \Theta_t \,|\, y_{1:t}$. However, in the second-stage update, each state will have a different Kalman gain; this can be computationally expensive. In the next section, another algorithm is developed based on a different factorization of the update equation that uses the same Kalman gain for the update of each state sample.

      \4 {\bf With the same Kalman gain}:
      In this section, different Kalman gains for each state sample is avoided by replacing posterior samples of $\Theta_t^y$ with a point estimator. This, of course, provides biased samples, so the bias will be corrected with an additonal resampling step. Let $\bar{\theta}_t^y$ be a point estimate of $\Theta_t^y \,|\, y_{1:t-1}$. We have
      \begin{align*}
        p(x_t, \theta_t \,|\, y_{1:t})
          &= \frac{g(y_t \,|\, x_t, \theta_t^y)}{g(y_t \,|\, x_t, \bar{\theta}_t^y)}
            \times
            \frac{g(y_t \,|\, x_t, \bar{\theta}_t^y) p(x_t \,|\, \theta_t^x, y_{1:t-1})}{p(y_t \,|\, \theta_t^x, \bar{\theta}_t^y, y_{1:t-1})}
            \times
            \frac{p(y_t \,|\, \theta_t^x, \bar{\theta}_t^y, y_{1:t-1})}{p(y_t \,|\, y_{1:t-1})} p(\theta_t \,|\, y_{1:t-1})
            %}_{p(x_t, \tilde{\theta}_t \,|\, y_{1:t})}
      \end{align*}
      Ignoring the first term, the factorization is similar to the update equation in Section 2.3 where $\Theta_t^y$ was fixed. This suggests applying the algorithm from Section 2.3 with the point estimate $\bar{\theta}_t^y$ to sample from $X_t, \Theta_t \,|\, y_{1:t}$ and then having a resampling step that corrects for the bias introduced in using only the point estimator for sampling.

      \ul{Algorithm}: Have samples $(x_{t-1}^{a(i)}, \theta_{t-1}^{a(i)}) \sim X_{t-1}, \Theta_{t-1} \,|\, y_{1:t-1}$
      \begin{myenum}%[label=(\arabic*),leftmargin=*,noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
        \item First-stage update: Sample $(\tilde{x}_t^{f(i)}, \theta_t^{f(i)}) \sim X_t, \Theta_t \,|\, y_{1:t-1}$. Calculate $\bar{\theta}_t^y = \frac{1}{N} \sum_{n=1}^N \theta_t^{y,f(i)}$. Calculate and normalize weights, $v_t^{(i)} \propto p(y_t \,|\, \theta_t^{x,f(i)}, \bar{\theta}_t^y, y_{1:t-1})$. Resample $\tilde{\theta}_t^{a(i)}$ from $\{(v_t^{(i)}, \theta_t^{f(i)})\}_{i=1}^N$.
        \item Second-stage update: Sample $x_t^{f(i)} \sim X_t \,|\, \tilde{\theta}_t^{x,a(i)}, y_{1:t-1}$. Use EnKF with $\{x_t^{f(i)}\}_{n=1}^N$ to update the state, obtaining samples $\{(\tilde{x}_t^{a(i)}, \tilde{\theta}_t^{a(i)})\}_{n=1}^N$.
        \item Third-stage update: Calculate and normalize weights, $w_t^{(i)} \propto g(y_t \,|\, \tilde{x}_t^{a(i)}, \tilde{\theta}_t^{y,a(i)}) / g(y_t \,|\, \tilde{x}_t^{a(i)}, \bar{\theta}_t^y)$. Resample $(x_t^{a(i)},\theta_t^{a(i)})$ from $\{(w_t^{(i)}, \tilde{x}_t^{a(i)}, \tilde{\theta}_t^{a(i)})\}_{i=1}^N$.
      \end{myenum}
      The unfortunate consequence of the above algorithm is that resampling is performed twice (resampling is an undesirable, but necessary, step in PF algorithms)\todo{refs}.

    \2 {\bf IF with EnKF}: I will discuss how to integrate the above algorithms with the IF. Discuss how this method will do only as well as the EnKF, i.e. the parameters will converge only if the state converges under the EnKF.

    \2 {\bf Case study}\todo{expand}%: Do a case study with Lorenz 2005 with simple parameters.


  \1 {\bf Correcting for error in a continous-time model}
    \2 {\bf What is model error?}
    Suppose we have the ``true'' continuous-time model $R$:
    \begin{align}
      \dot{x}(t) &= R[x(t)], \label{eqn:ctm-right}
    \end{align}
    where $R$ is assumed to be deterministic. Further suppose that the observations are a linear mapping of the true state of the system with measurement error, observed at discrete times $t_n$:
    \begin{align*}
      y_{t_n} &= H_{t_n} x_{t_n} + \epsilon_{t_n}, \epsilon_{t_n} \sim \normal(0, \Sigma_{t_n})
    \end{align*}
    The measurement noise $\Sigma_{t_n}$ is assumed to be known. A numerical integrator, $\mathcal{I}$, is used to solve \cref{eqn:ctm-right} since it cannot be solved analytically. The values $x_{t_n}$ are approximated using an appropriate numerical scheme as:
    \begin{align*}
      x_{t_n} = \mathcal{I}_R(x_{t_{n-1}}) + O(e(dt)),
    \end{align*}
    where $dt$ is the timestep used by the numerical integrator and $e$ is a function mapping the order of the error, e.g. $e(x) = x$ for forward Euler and $e(x) = x^4$ for fourth-order Runge Kutta.

    In reality, we do not have $R$, but we have some knowledge of how the system evolves in time (epistemic knowledge); call it $W$. Then,
    \begin{align*}
      R[x(t)] = W[x(t)] + D[x(t)],
    \end{align*}
    where $D(\cdot) \equiv R(\cdot) - W(\cdot)$ is the unknown dynamics not captured by our model $W$. We are interested in the case where the discretization error of the integrator $O(e(\cdot))$ is negligible compared to the model error $D(\cdot)$.

    What happens when one filters with the ``wrong'' model $W$, i.e. $D(\cdot) = 0$? Refer back to \ref{item:filtering}\ref{item:filtering-with-wrong-model}

    \2 {\bf Correcting the wrong model}
      \3 {\bf Increasing the variance}:
      One way to represent model error is to increase the uncertainty of the state transition model, i.e.
      \begin{align*}
        x_{t_n} &\approx \mathcal{I}_W(x_{t_{n-1}}) + \eta_{t_n}, \hspace{.5in} \eta_{t_n} \sim \normal(0, \Gamma_{t_n})
      \end{align*}
      where $\Gamma_{t_n} \succ 0$. With the EnKF, two methods that implement this are: (1) inflating the forecast variance \citep{Hamill2001,Anderson2007,Anderson2009} and (2) re-estimating the observation variance (which is equivalent to having the measurement model absorb $\eta_{t_n}$ term) \citep{Stroud2007,Delsole2010,Frei2012}. These methods do alleviate the problem of using $W$ in place of $R$ but at the expense of forecast precision\todo{support mathematically? demonstration?}. A potentially better solution is to estimate the structural form of the model error.

      \3 {\bf Estimating a constant bias}:
      Adding a constant is the simplest structural form of model error \citep{Friedland1969,Friedland1978,Ignagni1981,Ignagni1990,Dee1998,Zupanski2006}, i.e.
      \begin{align*}
        x_{t_n} &\approx \mathcal{I}_W(x_{t_{n-1}}) + b_{t_n} + \eta_{t_n}
      \end{align*}

      This line of work does not address the very likely scenario that the model error is different for different parts of the state space.

      \3 {\bf Linear correction to the dynamical model}:
      A linear correction is the simplest form that allows model error to vary with the state space. One way to add a linear term is to add it to the discrete state transition model, as done with the constant error term in the previous section. Although this is relatively easy, it produces results that are difficult to interpret. If we believe that the numerical model $W$ is incorrect, then it should be fixed {\it at the source of that error}, in the continuous-time model. There has been a line of work that does just this by adding the linear correction to the continuous-time model. We will follow this line of work and suggest various improvements that can be made.

      \citet{Leith1978} considered adding a linear correction to $W$ and studied what the optimal correction would be under the idealized scenario where the ``true'' model $R$ can be evaluated and the state $x(t)$ can be obtained without measurement error. In this case, $D(\cdot)$ can be calculated exactly and a linear approximation can be estimated:
      \begin{align}
        D[x(t)] &\approx L x(t) + b + \epsilon(t)
        \label{eqn:leith}
      \end{align}
      where $\epsilon(t)$ represents what could not be captured by the linear term and is assumed to have mean zero. Then, the least-squares solution is
      \begin{equation}
      \begin{aligned}
        \hat{L} &= \cov\{D[x(t)], x(t)\} \var[x(t)]^{-1} \\
        \hat{b} &= \e\{D[x(t)]\} - \hat{L} \e[x(t)]
      \end{aligned}
      \label{eqn:leith-soln}
      \end{equation}
      In reality, this exercise is impossible because neither $D(\cdot)$ can be evaluated nor can the true state $x(t)$ be measured exactly.

      \citet{DelSole1999} proposed using a forward Euler approximation with filtered data to approximate Leith's solutions. They showed that the linear correction improved forecasts, but their methods were not scalable because the variance term in $\hat{L}$ is often rank-deficient for high-dimensional state spaces (need samples on the order of $D^2$). \citet{Danforth2007} and \citet{Danforth2008} proposed using the first $K$ singular vectors of the SVD decomposition in the approximation of $\var[x(t)]$ to find a full-rank representation that can be inverted. They also demonstrated that it improved forecasts with a simple weather model \citep{Danforth2008a}. This line of work is promising, but here are some issues of these approaches that we will resolve in the next section:
      \begin{itemize}
        \item Our applications of interest have spatial correlation that should be taken advantage of in the estimation of $L$.
        \item Does not explicitly account for the leftover error not captured by the linear correction.
        \item A forward Euler approximation with filtered data introduces numerical error in the approximation of $D$, diluting the signal of the model error, and therefore into the estimation of $L,b$.
      \end{itemize}

    \2 {\bf Low-rank linear correction}:
    We follow Leith's approach of correcting the continuous-time model with a linear term. For our applications of interest, the state $x(t)$ is correlated in space and its variance can often be well approximated using fewer parameters than directly using the sample covariance. Furthermore, we assume that the model error and the state are also spatially correlated (and will motivate that this is true). This suggests that the covariance and variance terms in \cref{eqn:leith-soln} can be replaced with parametric functions; for instance, consider using isotropic covariance functions:
    \begin{align*}
      L \approx \widetilde{L}(s_i, \cdot; \lambda, \theta_{dx}, \theta_{xx}) \equiv \lambda C_{dx}(s_i, \cdot; \rho_{dx}) C_{xx}(\cdot, \cdot; \rho_{xx})^{-1},
    \end{align*}
    where $\lambda$ is the magnitude of the linear term, $C_{dx}(s_i, s_j; \rho_{dx})$ is the correlation between the model error at spatial point $i$ and state at spatial point $j$ with parameter $\rho_{dx}$, and $C_{xx}(s_i, s_j; \rho_{xx})$ is the correlation between the states at spatial points $i$ and $j$ with parameter $\rho_{dx}$. Let $\mu_d$ and $\mu_x$ be the mean of the model error and state respectively, then
    \begin{align*}
      b &\approx \widetilde{b}(s_i; \mu_d, \mu_x) \equiv \mu_d - \widetilde{L}(s_i,\cdot) \mu_x.
    \end{align*}
    These two approximations provide an approximation for the model error:
    \begin{align*}
      D[x_i(t)]
        \approx \widetilde{D}[x_i(t); \lambda, \rho_{dx}, \rho_{xx}, \mu_d, \mu_x]
        \equiv \widetilde{L}(s_i, \cdot) x(t) + \widetilde{b}(s_i).
    \end{align*}
    Define $\widetilde{W}(\cdot) \equiv W(\cdot) + \widetilde{D}(\cdot)$. It remains to account for the error that could not be captured by $\widetilde{D}$. The true system $R$ was assumed to be deterministic, so it's odd \todo{need to argue better} to inject stochasticity into the continuous-time model as Leith did. Instead, we describe the leftover error with a random term in the discrete state transition model:
    \begin{align*}
      x_{t_n} \approx \mathcal{I}_{\widetilde{W}}(x_{t_{n-1}}) + \eta_{t_n}, \eta_{t_n} \sim \normal(0, \gamma^2 \Delta t_n I),
    \end{align*}
    where $\Delta t_n$ is the time between $t_{n-1}$ and $t_n$. Specifically, the model error that was not captured by $\widetilde{D}$ is approximated with a random variable that is normally distributed with mean zero. Our goal is to estimate the parameter $\theta = (\lambda, \theta_{dx}, \theta_{xx}, \mu_d, \mu_x, \gamma^2)$. Using methods from Part \ref{item:param-enkf}, $\theta$ can be estimated without introducing numerical approximations as with \citet{DelSole1999,Danforth2007,Danforth2008}.

    \2 {\bf Case study}\todo{expand}: With Lorenz 2005, there is a true model $R$ (Model III) and a simplified version of the true model $W$ (Model II). Both can be evaluated, allowing direct estimation of Leith's solutions. We will generate data to solve for Leith's solutions and motivate that the low-rank linear correction proposed in the last section is reasonable. Then, methods from Part \ref{item:param-enkf} will be applied to estimate the parameter $\theta$ in a filtering context.

      \3 {\bf Motivating the low-rank linear correction}: In this section, I will generate samples from $X(t)$ and evaluate $D[X(t)]$. I will show that the reparametrization does just as well or better in prediction error (cross-validation). Model hierarchy: (a) intercept only; (b) sample covariance matrix (can't be done); (c) tapered sample covariance matrix \citep{Bickel2008}; (d) parametrized covariance matrix. Plugging in estimates of $\widetilde{W}$ into the filter certainly improves forecasts. It remains to see the same can be done in a filtering context.
      \3 {\bf Estimating $\theta$ in a filtering context}: Estimate the parameters for two models: (1) R = Model II + linear. How well can the linear term be recovered? (2) R = Model III.

  \1 {\bf Conclusion}: Discuss future work.

  \1 {\bf Appendix}
    \2 Proof of theorem leading up to gRPF.
    \2 Auxiliary particle filter (APF)

\end{outline}

\bibliographystyle{plainnat}
\bibliography{refs}
\end{document}
